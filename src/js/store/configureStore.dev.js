// 如果是开发模式，store 采用此配置

import { createStore, applyMiddleware, compose, combineReducers} from 'redux'
import createSagaMiddleware, { END } from 'redux-saga'
import createLogger from 'redux-logger'
import thunk from 'redux-thunk';
import Reducer from '../reducers/index.js'
import ZhCn from '../../imports/zh_cn.js';
import { loadTranslations, setLocale, syncTranslationWithStore, i18nReducer} from 'react-redux-i18n';

const configureStore = preloadedState => {
  const sagaMiddleware = createSagaMiddleware()

  const store = createStore(
    combineReducers({
    Reducer,
    i18n: i18nReducer
  }),
    // rootReducer,
    preloadedState,
    compose(
      applyMiddleware(thunk,sagaMiddleware, createLogger())
      // applyMiddleware 是redux的原生方法，它将所有中间件组成一个数组，依次执行。
    )
  )


  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').default
      store.replaceReducer(nextRootReducer)
    })
  }
  store.runSaga = sagaMiddleware.run
  store.close = () => store.dispatch(END)
    syncTranslationWithStore(store);
    store.dispatch(loadTranslations(ZhCn));
    store.dispatch(setLocale('zh'));
  return store
}

export default configureStore

