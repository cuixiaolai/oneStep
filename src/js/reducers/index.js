import { combineReducers } from 'redux'
// import counter from './counter'
// import todos from './todos'
// import showCongratulation from './showCongratulation'
// import counterAsync from './counterAsync'
// import timer from './timer'
// import posts from './posts'
import category from '../../imports/reducer/category.js'
import payment from '../../imports/reducer/payment.js'
import stock from '../../imports/reducer/stock.js'

const Reducer = combineReducers({
  // counter,
  // counterAsync,
  // todos,
  // showCongratulation,
  // timer,
  // posts
  stock,
  category
});

export default Reducer;
