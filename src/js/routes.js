import React from 'react'
import { Route, IndexRoute } from 'react-router'
import ProductDetails from '../icxyz/containers/ProductDetails.jsx'
import stockJson from '../icxyz/stock.json';
import app from '../imports/ui/App.jsx'
import home from '../imports/ui/Home.jsx'
import search from '../imports/ui/Search.jsx'
import login from '../imports/ui/Login.jsx'
import register from '../imports/ui/Register.jsx'
import perRegister from '../imports/ui/perRegister.jsx';
import comRegister from '../imports/ui/comRegister.jsx';
import UserFeedback from '../imports/ui/UserFeedback.jsx';
import Forget from '../imports/ui/Forget.jsx';
import PersonalCenter from '../imports/ui/PersonalCenterList.jsx';
import PersonalCenterIndex from '../imports/ui/PersonalCenterIndex.jsx';
import PersonalCenterInfo from '../imports/ui/PersonalCenterInfo.jsx';
import PersonalCenterAddress from '../imports/ui/PersonalCenterAddress.jsx';
import PersonalCenterOrder from '../imports/ui/PersonalCenterOrder.jsx'; 
import ReturnNote from '../imports/ui/ReturnNote.jsx';
import PersonalCenterSecurity from '../imports/ui/PersonalCenterSecurity.jsx';
import CompantInvoics from '../imports/ui/CompantInvoics.jsx';
import PersonalCenterSecurityAuthentication from '../imports/ui/PersonalCenterSecurityAuthentication.jsx';
import PersonalCenterSecurityNewAuthentication from '../imports/ui/PersonalCenterSecurityNewAuthentication.jsx';
import PersonalCenterIntegral from '../imports/ui/PersonalCenterIntegral.jsx';
import PersonalCenterSecurityPassword from '../imports/ui/PersonalCenterSecurityPassword.jsx';
import PersonalCenterSecurityPhone from '../imports/ui/PersonalCenterSecurityPhone.jsx';
import PersonalCenterSecurityEmail from '../imports/ui/PersonalCenterSecurityEmail.jsx';
import PersonalCenterSecurityNewPhone from '../imports/ui/PersonalCenterSecurityNewPhone.jsx';
import PersonalCenterSecurityNewEmail from '../imports/ui/PersonalCenterSecurityNewEmail.jsx';
import PersonalCenterSecurityPay from '../imports/ui/PersonalCenterSecurityPay.jsx';
import PersonalCenterBuyer from '../imports/ui/PersonalCenterBuyer.jsx';
// import ProductDetails from '../imports/ui/ProductDetails.jsx';
import Cart from '../imports/ui/Cart.jsx';
import Order from '../imports/ui/Order.jsx';
import ConfirmOrder from '../imports/ui/ConfirmOrder.jsx';
import Payment from '../imports/ui/Payment.jsx';
import Evaluate from '../imports/ui/Evaluate.jsx';
import AdditionalComment from '../imports/ui/PersonalAdditionalComment.jsx';
import SellerEvaluate from '../imports/ui/SellerEvaluate.jsx';
import Return from '../imports/ui/Return.jsx';




// import {
//   App,
//   Home,
//   Coupon,
//   CouponDetail,
//   Tour,
//   User,
//   NotFoundPage,
//   Shop,
//   GoodDetail 
// } from './containers'

export default (
  <Route path="/" component={app}>
   <IndexRoute component={home}/>
   <Route path="user_feedback" component={UserFeedback} ></Route>
   <Route path="search" component={search}/>
   <Route path="login" component={login}/>
   <Route path="register" component={register}/>
   <Route path="perRegister" component={perRegister}/> 
   <Route path="perRegister/:id" component={perRegister}/>
   <Route path="comRegister" component={comRegister}/>
    <Route path="forget" component={Forget}  />

                        <Route path="personal_center" component={PersonalCenter} >
                        <IndexRoute component={PersonalCenterIndex} />
                        <Route path="index" component={PersonalCenterIndex} />
                        <Route path="info" component={PersonalCenterInfo} />
                        <Route path="address" component={PersonalCenterAddress} /> 
                        <Route path="order" component={PersonalCenterOrder} />
                         <Route path="return_note" component={ReturnNote}/>
                        <Route path="security" component={PersonalCenterSecurity} />
                        <Route path="invoics" component={CompantInvoics} />
                        <Route path="authentication" component={PersonalCenterSecurityAuthentication} />
                        <Route path="new_authentication" component={PersonalCenterSecurityNewAuthentication} />
                        <Route path="integral" component={PersonalCenterIntegral} />
                        <Route path="password" component={PersonalCenterSecurityPassword} />
                        <Route path="phone" component={PersonalCenterSecurityPhone} />
                        <Route path="email" component={PersonalCenterSecurityEmail} />
                        <Route path="new_phone" component={PersonalCenterSecurityNewPhone} />
                        <Route path="new_email" component={PersonalCenterSecurityNewEmail} />
                        <Route path="pay" component={PersonalCenterSecurityPay} />
                        <Route path="buyer" component={PersonalCenterBuyer} />             
                    </Route>
     // <Route path="cart" component={Cart} />
     <Route path="order" component={Order} />
     <Route path="ConfirmOrder" component={ConfirmOrder} /> 
     <Route path="Payment" component={Payment} />
     <Route path="evaluate" component={Evaluate} />
     <Route path="additional_comment" component={AdditionalComment} />
     <Route path="sellert_evaluate" component={SellerEvaluate} />
     <Route path="return/:id" component={Return} />     
















     <Route path="product_details/:id" component={ProductDetails}/>               

  </Route> 
);
    // 
    // <Route path="coupon">
    //   <IndexRoute component={Coupon}/>
    //   <Route path=":id" component={CouponDetail}/>
    // </Route>
    // <Route path="tour" component={Tour}/>
    // <Route path="user" component={User}/>
    // <Route path="shop" component={Shop}/>
    // <Route path="good" >
    // <IndexRoute component={NotFoundPage}/>
    // <Route path=":id" component={GoodDetail}/>
    // </Route>
    // <Route path="*" component={NotFoundPage}/>