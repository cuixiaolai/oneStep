import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Users = Mongo.Collection.get('users');

Meteor.methods({
    'Roles.addUsersToRoles'(userId, role) {
        check(userId, String);
        check(role, String);
        Roles.addUsersToRoles(userId, role);
    },

    'Roles.removeUsersToRoles'(userId, role) {
        check(userId, String);
        check(role, String);
        Roles.removeUsersFromRoles(userId, role);
    },

    'Roles.checkRoles'(username){
        check(username, String);
        var user = Users.findOne({username: username});
        if (user && user.freeze == true) {
            return 'freeze';
        }
        if (user && Roles.userIsInRole(user, 'person')) {
            return 'person';
        }
        else if (user && Roles.userIsInRole(user, 'company')){
            return 'company';
        }
    },
});
