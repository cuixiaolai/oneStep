import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';


export const Warranty = new Mongo.Collection('warranty_agency');
const Users = Mongo.Collection.get('users');

if (Meteor.isServer) {
    Meteor.publish('warranty_agency', function () {
        return Warranty.find({});
    });
}

Meteor.methods({

});