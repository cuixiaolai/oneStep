import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Authentication = new Mongo.Collection('authentication');
const Buyer = Mongo.Collection.get('buyer');
const Seller = Mongo.Collection.get('seller');
const Users = Mongo.Collection.get('users');

Meteor.methods({
    'authentication.addIdentityAuthentication'(user_id, name, id, begin, end, front, back) {
        check(user_id, String);
        check(name, String);
        check(id, String);
        check(begin, String);
        check(end, String);
        check(front, String);
        check(back, String);
        let usersInfo = Users.findOne({_id: user_id});

        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 1实名认证
        Authentication.insert({user_id: user_id, user_name:usersInfo.username, type: 1, name: name, id: id, state: 1, date: new Date()});
    },

    'authentication.cancelIdentityAuthentication'(user_id) {
        check(user_id, String);

        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 1实名认证
        Authentication.update({user_id:user_id, type: 1, state: {$in:[2,3]}}, {$set:{state: 4}});
    },

    'authentication.addCompanyAuthentication'(user_id) {
        check(user_id, String);
        let buyerInfo = Buyer.findOne({_id: user_id});

        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 2公司认证
        Authentication.insert({user_id:user_id, type: 2, company: buyerInfo.authentication.company, name: buyerInfo.authentication.name, id: buyerInfo.authentication.id, state: 1, date: new Date()});
    },

    'authentication.cancelCompanyAuthentication'(user_id) {
        check(user_id, String);

        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 2公司认证
        Authentication.update({user_id:user_id, type: 2, state: {$in:[2,3]}}, {$set:{state: 4}});
    },

    'authentication.addInvoiceAuthentication'(user_id) {
        check(user_id, String);
        let buyerInfo = Buyer.findOne({_id: user_id});
        let usersInfo = Users.findOne({_id: user_id});

        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 3发票认证
        Authentication.insert({user_id:user_id, user_name:usersInfo.username, name: buyerInfo.invoice.name, user_tel: buyerInfo.invoice.tel, type: 3, state: 1, date: new Date()});
    },

    'authentication.cancelInvoiceAuthentication'(user_id) {
        check(user_id, String);

        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 3发票认证
        Authentication.update({user_id:user_id, type: 3, state: {$in:[2,3]}}, {$set:{state: 4}});
    },

    'authentication.addSellerAuthentication'(user_id, agent, manufacturer) {
        check(user_id, String);
        check(agent, Array);
        check(manufacturer, Array);

        let usersInfo = Users.findOne({_id: user_id});
        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 4卖家认证
        Authentication.insert({user_id:user_id, user_name: usersInfo.username, company_name: usersInfo.company, type: 4, retailer: {}, agent: {files: agent}, manufacturer: {files: manufacturer}, state: 1, date: new Date()});
    },

    'authentication.cancelSellerAuthentication'(user_id) {
        check(user_id, String);

        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 4卖家认证
        Authentication.update({user_id:user_id, type: 4, state: {$in:[2,3]}}, {$set:{state: 4}});
    },

    'authentication.addAgentAuthentication'(user_id, agent) {
        check(user_id, String);
        check(agent, Array);

        let usersInfo = Users.findOne({_id: user_id});
        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 5代理商认证
        Authentication.insert({user_id:user_id, user_name: usersInfo.username, company_name: usersInfo.company, type: 5, agent: {files: agent}, state: 1, date: new Date()});
    },

    'authentication.cancelAgentAuthentication'(user_id, del_file) {
        check(user_id, String);
        check(del_file, Array);

        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 5代理商认证
        Authentication.update({user_id:user_id, type: 5, state: {$in:[2,3]}}, {$set:{state: 4, del_file: del_file}});
    },

    'authentication.addManufacturerAuthentication'(user_id, manufacturer) {
        check(user_id, String);
        check(manufacturer, Array);

        let usersInfo = Users.findOne({_id: user_id});
        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 6厂商认证
        Authentication.insert({user_id:user_id, user_name: usersInfo.username, company_name: usersInfo.company, type: 6, manufacturer: {files: manufacturer}, state: 1, date: new Date()});
    },

    'authentication.cancelManufacturerAuthentication'(user_id, del_file) {
        check(user_id, String);
        check(del_file, Array);

        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 6厂商认证
        Authentication.update({user_id:user_id, type: 6, state: {$in:[2,3]}}, {$set:{state: 4, del_file: del_file}});
    },

    'authentication.addDoubleAmount'(amount) {
        check(amount, Number);

        let usersInfo = Users.findOne({_id: Meteor.userId()});
        let sellerInfo = Seller.findOne({_id: Meteor.userId()});
        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 7双倍赔付认证
        Authentication.insert({user_id:Meteor.userId(), user_name: usersInfo.username, company_name: usersInfo.company, phone: usersInfo.phone, type: 7, amount: amount, total: sellerInfo.double.total, state: 1, date: new Date()});
    },

    'authentication.addGuaranteeAmount'(amount) {
        check(amount, Number);

        let usersInfo = Users.findOne({_id: Meteor.userId()});
        let sellerInfo = Seller.findOne({_id: Meteor.userId()});
        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 8店铺保证金
        Authentication.insert({user_id:Meteor.userId(), user_name: usersInfo.username, company_name: usersInfo.company, phone: usersInfo.phone, type: 8, amount: amount, total: sellerInfo.guarantee.total, state: 1, date: new Date()});
    },

    'authentication.addStockVerify'(username, phone, address, describe) {
        check(username, String);
        check(phone, String);
        check(address, String);
        check(describe, String);

        let usersInfo = Users.findOne({_id: Meteor.userId()});
        //state: 1未审核， 2审核成功，3审核失败，4取消；type: 9库存验证
        Authentication.insert({user_id:Meteor.userId(), user_name: usersInfo.username, company_name: usersInfo.company, type: 9, username: username, phone: phone, address: address, describe: describe, state: 1, date: new Date()});
    },
});