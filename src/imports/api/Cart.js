import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Cart = new Mongo.Collection('cart');

if (Meteor.isServer) {
    Meteor.publish('cart', function () {
        return Cart.find({});
    });
}

Meteor.methods({
    'cart.create'() {
        Cart.insert({_id: Meteor.userId(), size: 0});
    },

    'cart.updateRecord'(stockId, quantity, company, delivery_address, stock_type) {
        check(stockId, String);
        check(quantity, Number);
        check(company, String);
        check(delivery_address, Number);
        check(stock_type, String);
        if (Cart.findOne({'_id': Meteor.userId(),'cart': { $elemMatch: {'stockId': stockId ,'delivery_address': delivery_address}}}) != null) {
            Cart.update({'_id': Meteor.userId(),
                'cart': {
                    $elemMatch: {
                        'stockId': stockId,
                        'delivery_address': delivery_address,
                    }
                }
            }, {$inc: {'cart.$.quantity': quantity}});
        }
        else {
            Cart.update({'_id': Meteor.userId()}, {
                $addToSet: {
                    'cart': {
                        'stockId': stockId,
                        'quantity': quantity,
                        'company': company,
                        'delivery_address': delivery_address,
                        'stock_type': stock_type,
                    }
                }, $inc: {size: 1}
            });
        }
    },

    'cart.deleteRecord'(stockId, delivery_address, quantity) {
        check(stockId, String);
        check(delivery_address, Number);
        check(quantity, Number);

        Cart.update({'_id': Meteor.userId()}, {
            $pull: {
                'cart': {
                    'stockId': stockId,
                    'delivery_address': delivery_address
                }
            }
        });
        Cart.update({'_id': Meteor.userId()}, {$inc: {size: -1}});
    },

    'cart.changeDeliveryAddress'(stockId, quantity, delivery_address, new_delivery_address) {
        check(stockId, String);
        check(quantity, Number);
        check(delivery_address, Number);
        check(new_delivery_address, Number);

        if (Cart.findOne({'_id': Meteor.userId(),'cart': { $elemMatch: {'stockId': stockId ,'delivery_address': new_delivery_address}}}) != null) {
            Cart.update({
                '_id': Meteor.userId(),
                'cart': {
                    $elemMatch: {
                        'stockId': stockId,
                        'delivery_address': new_delivery_address
                    }
                }
            }, {$inc: {'cart.$.quantity': quantity, size: -1}});
            Cart.update({'_id': Meteor.userId()}, {
                $pull: {
                    'cart': {
                        'stockId': stockId,
                        'delivery_address': delivery_address
                    }
                }
            });
        }
        else {
            Cart.update({'_id': Meteor.userId(),
                'cart': {
                    $elemMatch: {
                        'stockId': stockId,
                        'delivery_address': delivery_address
                    }
                }
            }, {$set: {'cart.$.delivery_address': new_delivery_address}});
        }
    },

    'cart.updateQuantity'(stockId, quantity, delivery_address) {
        check(stockId, String);
        check(quantity, Number);
        check(delivery_address, Number);

        Cart.update({'_id': Meteor.userId(),
            'cart': {
                $elemMatch: {
                    'stockId': stockId,
                    'delivery_address': delivery_address,
                }
            }
        }, {$inc: {'cart.$.quantity': quantity}});
    },
});