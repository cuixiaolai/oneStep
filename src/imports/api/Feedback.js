import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Feedback = new Mongo.Collection('feedback');
const Users = Mongo.Collection.get('users');

Meteor.methods({
    'feedback.addItem'(type, suggestion, satisfaction, contact, phone) {
        check(suggestion, String);
        check(type, String);
        check(satisfaction, String);
        check(contact, String);
        check(phone, String);

        userInfo = Users.findOne({_id: Meteor.userId()});
        Feedback.insert({username: userInfo.username, type: type, suggestion: suggestion, satisfaction: satisfaction, contact: contact, phone: phone, solve: false, date: new Date()});
    }
});