import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
var verifyCode = require('./VerifyCode.js');

Meteor.methods({
    'generate.verify_code'(code) {
        check(code, String);
        var result = verifyCode.Generate(code);
        var imgDataURL = result.dataURL;
        return imgDataURL;
    },
});