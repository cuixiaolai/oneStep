import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Buyer = new Mongo.Collection('buyer');
const Users = Mongo.Collection.get('users');

if (Meteor.isServer) {
    Meteor.publish('buyer', function () {
        return Buyer.find({});
    });
}

Meteor.methods({
    'buyer.create'() {
        Buyer.insert({_id: Meteor.userId()});
    },

    'buyer.addAddress'(name, address, detailAddress, phone, tel, code) {
        check(name, String);
        check(address, String);
        check(detailAddress, String);
        check(phone, String);
        if (code != null)
            check(code, String);

        let info = Buyer.findOne({_id: Meteor.userId()});
        let id = 1;
        if (info.address != null && info.address.length > 0) {
            id = info.address[0].id + 1;
        }
        if (info.defaultAddress == null || Buyer.findOne({_id: Meteor.userId(), 'address.id': info.defaultAddress}) == null) {
            Buyer.update(Meteor.userId(), {$set: {defaultAddress: id}});
        }
        if (tel == null) {
            Buyer.update(Meteor.userId(), {$push: {address: {
                $each: [{id: id, name: name, address: address, detailAddress: detailAddress, phone: phone, code: code}],
                $sort: {id: -1},
                $slice: -20}}});
        }
        else {
            check(tel, String);
            Buyer.update(Meteor.userId(), {$push: {address: {
                $each: [{id: id, name: name, address: address, detailAddress: detailAddress, phone: phone, tel: tel, code: code}],
                $sort: {id: -1},
                $slice: -20}}});
        }
    },

    'buyer.updateAddress'(id, name, address, detailAddress, phone, tel, code) {
        check(id, Number);
        check(name, String);
        check(address, String);
        check(detailAddress, String);
        check(phone, String);
        if (code != null)
            check(code, String);

        Buyer.update(Meteor.userId(), {$pull: {address: {id: id}}});
        if (tel == null) {
            Buyer.update(Meteor.userId(), {$push: {address: {
                $each: [{id: id, name: name, address: address, detailAddress: detailAddress, phone: phone, code: code}],
                $sort: {id: -1},
                $slice: -20}}});
        }
        else {
            check(tel, String);
            Buyer.update(Meteor.userId(), {$push: {address: {
                $each: [{id: id, name: name, address: address, detailAddress: detailAddress, phone: phone, tel: tel, code: code}],
                $sort: {id: -1},
                $slice: -20}}});
        }
    },

    'buyer.deleteAddress'(id) {
        check(id, Number);

        let info = Buyer.findOne({_id: Meteor.userId()});
        if (info.defaultAddress == id) {
            Buyer.update(Meteor.userId(),{$unset: {defaultAddress: 1}});
        }
        Buyer.update(Meteor.userId(), {$pull: {address: {id: id}}});
    },

    'buyer.setDefaultAddress'(id) {
        check(id, Number);

        Buyer.update(Meteor.userId(), {$set: {defaultAddress: id}});
    },

    'buyer.updatePersonalInfo'(nickname, sex, birthday, company, career, industry) {
        if (nickname != null) {
            check(nickname, String);
            Buyer.update(Meteor.userId(), {$set: {nickname: nickname}});
        }
        if (sex != null) {
            check(sex, Number);
            Buyer.update(Meteor.userId(), {$set: {sex: sex}});
        }
        if (birthday != null) {
            check(birthday, String);
            Buyer.update(Meteor.userId(), {$set: {birthday: birthday}});
        }
        if (company != null) {
            check(company, String);
            Buyer.update(Meteor.userId(), {$set: {company: company}});
        }
        if (career != null) {
            check(career, String);
            Buyer.update(Meteor.userId(), {$set: {career: career}});
        }
        if (industry != null) {
            check(industry, String);
            Buyer.update(Meteor.userId(), {$set: {industry: industry}});
        }
    },

    'buyer.updatePersonalIcon'(icon) {
        check(icon, String);
        Buyer.update(Meteor.userId(), {$set: {icon: icon}});
    },

    'buyer.addIdentityAuthentication'(name, id, begin, end, front, back) {
        check(name, String);
        check(id, String);
        check(begin, String);
        check(end, String);
        check(front, String);
        check(back, String);

        Buyer.update(Meteor.userId(), {$set: {authentication: {name: name, id: id, begin: begin, end: end, front: front, back: back, verify: false, date: new Date()}}});
    },

    'buyer.deleteIdentityAuthentication'() {
        Buyer.update(Meteor.userId(), {$unset: {authentication: 1}});
    },

    'buyer.addInvoice'(name, address, tel, id, bank_name, bank_account) {
        check(name, String);
        check(address, String);
        check(tel, String);
        check(id, String);
        check(bank_name, String);
        check(bank_account, String);

        Buyer.update(Meteor.userId(), {$set: {invoice: {name: name, address: address, tel: tel, id: id, bank_name: bank_name, bank_account: bank_account}}});
    },

    'buyer.updateInvoice'(path1, path2, path3) {
        check(path1, String);
        check(path2, String);
        check(path3, String);

        Buyer.update(Meteor.userId(), {$set: {'invoice.tax': path1, 'invoice.tax_payer': path2, 'invoice.attorney': path3, 'invoice.verify': false, 'invoice.date': new Date()}});
    },

    'buyer.deleteInvoice'() {
        Buyer.update(Meteor.userId(), {$unset: {invoice: 1}});
    },

    'buyer.addCompanyAuthentication'(company, address, industry, name, id) {
        check(company, String);
        check(address, String);
        check(industry, String);
        check(name, String);
        check(id, String);

        Buyer.update(Meteor.userId(), {$set: {authentication: {company: company, address: address, industry: industry, name: name, id: id}, company: company}});
        Users.update(Meteor.userId(), {$set: {company: company}});
    },

    'buyer.updateCompanyAuthentication'(path1, path2, begin1, end1, path3, begin2, end2) {
        check(path1, String);
        check(path2, String);
        check(begin1, String);
        check(end1, String);
        check(path3, String);
        check(begin2, String);
        check(end2, String);

        Buyer.update(Meteor.userId(), {$set: {'authentication.front': path1, 'authentication.back': path2, 'authentication.begin1': begin1, 'authentication.end1': end1,
            'authentication.license': path3, 'authentication.begin2': begin2, 'authentication.end2': end2, 'authentication.verify': false, 'authentication.date': new Date()}});
    },

    'buyer.updateCompanyUrl'(str) {
        check(str, String);

        if (str == '') {
            Buyer.update(Meteor.userId(), {$unset: {'company_url': 1}});
        }
        else {
            Buyer.update(Meteor.userId(), {$set: {'company_url': str}});
        }
    },

    'buyer.updateCompanyDetail'(str) {
        check(str, String);

        if (str == '') {
            Buyer.update(Meteor.userId(), {$unset: {'company_detail': 1}});
        }
        else {
            Buyer.update(Meteor.userId(), {$set: {'company_detail': str}});
        }
    },

    'buyer.ckeckSuccess'() {
        Buyer.update(Meteor.userId(), {$set: {'authentication.verify': true, 'authentication.success': true}});
    },

    'buyer.invoiceSuccess'() {
        Buyer.update(Meteor.userId(), {$set: {'invoice.verify': true, 'invoice.success': true}});
    },

    'buyer.getIcon'(id) {
        check(id, String);

        let buy = Buyer.findOne({_id: id});
        return buy.icon;
    }
});
