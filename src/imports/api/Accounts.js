import { Accounts } from 'meteor/accounts-base'
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Email } from 'meteor/email';
import ZhCn from '../zh_cn.js';
import config from '../config.json';
import schedule from 'node-schedule';

const Users = Mongo.Collection.get('users');
const Emails = new Mongo.Collection('emails');

Meteor.methods({
    // 'accounts.VerificationEmail'(userId) {
    //     check(userId, String);
    //     Accounts.emailTemplates.siteName = "AlphaBeta";
    //     Accounts.emailTemplates.from = config.email;
    //     Accounts.emailTemplates.verifyEmail.text = function (user, url) {
    //         url = url.replace('/#', '');
    //         return ZhCn.zh.email.verification.title + " " + user.username + `:
    // ` + ZhCn.zh.email.verification.hello + `
    // ` + ZhCn.zh.email.verification.welcome + `
    // ` + ZhCn.zh.email.verification.content + `:
    // ` + url;
    //     };
    //     Accounts.sendVerificationEmail(userId);
    // },

    'accounts.perVerificationEmail'(email) {
        check(email, String);
        let stampedToken = Accounts._generateStampedLoginToken();
        let hashStampedToken = Accounts._hashStampedToken(stampedToken);
        hashStampedToken.hashedToken = hashStampedToken.hashedToken.replace(/\//g, '-');
        hashStampedToken.hashedToken = hashStampedToken.hashedToken.replace(/%/g, '-');
        let emailinfo = Emails.findOne({email: email});
        if (emailinfo == null) {
            Emails.insert({email: email, Token: hashStampedToken});
        }
        else {
            Emails.update(emailinfo._id, {$set: {Token: hashStampedToken}});
        }
        let date_schedule = new Date();
        date_schedule.setTime(date_schedule.getTime() + 86400000); //24小时
        let schedule_action = schedule.scheduleJob(date_schedule, Meteor.bindEnvironment(function(){
            let schedule_email=Emails.findOne({email: email, Token: hashStampedToken});
            if(schedule_email != null){
                Emails.remove({email: email, Token: hashStampedToken});
            }
        }));

        let url = Meteor.absoluteUrl.defaultOptions.rootUrl + 'perRegister/' + hashStampedToken.hashedToken;

        let subject = ZhCn.zh.email.verification.subject;
        let text = ZhCn.zh.email.verification.title + `:
       ` + ZhCn.zh.email.verification.hello + `
       ` + ZhCn.zh.email.verification.welcome + `
       ` + ZhCn.zh.email.verification.content + `:
       ` + url+`
       ` + ZhCn.zh.email.verification.tips;
        Email.send({
            to: email,
            from: config.email,
            subject: subject,
            text: text,
        });
    },

    'accounts.comVerificationEmail'(email) {
        check(email, String);
        let stampedToken = Accounts._generateStampedLoginToken();
        let hashStampedToken = Accounts._hashStampedToken(stampedToken);
        hashStampedToken.hashedToken = hashStampedToken.hashedToken.replace(/\//g, '-');
        hashStampedToken.hashedToken = hashStampedToken.hashedToken.replace(/%/g, '-');
        let emailinfo = Emails.findOne({email: email});
        if (emailinfo == null) {
            Emails.insert({email: email, Token: hashStampedToken});
        }
        else {
            Emails.update(emailinfo._id, {$set: {Token: hashStampedToken}});
        }
        let date_schedule = new Date();
        date_schedule.setTime(date_schedule.getTime() + 86400000); //24小时
        let schedule_action = schedule.scheduleJob(date_schedule, Meteor.bindEnvironment(function(){
            let schedule_email=Emails.findOne({email: email, Token: hashStampedToken});
            if(schedule_email != null){
                Emails.remove({email: email, Token: hashStampedToken});
            }
        }));

        let url = Meteor.absoluteUrl.defaultOptions.rootUrl + 'comRegister/' + hashStampedToken.hashedToken;

        let subject = ZhCn.zh.email.verification.subject;
        let text = ZhCn.zh.email.verification.title + `:
       ` + ZhCn.zh.email.verification.hello + `
       ` + ZhCn.zh.email.verification.welcome + `
       ` + ZhCn.zh.email.verification.content + `:
       ` + url+`
       ` + ZhCn.zh.email.verification.tips;;
        Email.send({
            to: email,
            from: config.email,
            subject: subject,
            text: text,
        });
    },

    'accounts.CheckVerificationEmail'(token) {
        check(token, String);
        let emailinfo = Emails.findOne({"Token.hashedToken": token});

        if (emailinfo == null) {
            throw new Meteor.Error("token error");
        }
        else {
            let email = emailinfo.email;
            Emails.remove(emailinfo._id);
            return email;
        }
    },

    'accounts.ResetPasswordEmail'(userId, username, email) {
        check(username, String);
        check(email, String);
        let items = 'abcdefghijkmlnopqrstuvwxyz0123456789'.split('');
        let randInt = function(start, end) {
            return Math.floor(Math.random() * (end - start)) + start;
        };
        let code = '';
        for (let i = 0; i < 4; ++i)
        {
            code += items[randInt(0, items.length)];
        }
        Users.update(userId, {$set: {email_resetpassword: {code: code, verify: false}}});

        let subject = ZhCn.zh.email.resetPassword.subject;
        let text = ZhCn.zh.email.resetPassword.title + " " + username + `:
    ` + ZhCn.zh.email.resetPassword.hello + `
    ` + ZhCn.zh.email.resetPassword.content + `:
    ` + code;
        Email.send({
            to: email,
            from: config.email,
            subject: subject,
            text: text,
        });
    },

    'accounts.SetPassword'(userId, newPassword) {
        check(userId, String);
        check(newPassword, String);
        Accounts.setPassword(userId, newPassword);
    },

    'accounts.ResetPassword'(userId, newPassword) {
        check(userId, String);
        check(newPassword, String);
        Accounts.setPassword(userId, newPassword, {logout:false});
    },
});
