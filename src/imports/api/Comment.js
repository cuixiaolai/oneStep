import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Comment = new Mongo.Collection('comment');
const Stock = Mongo.Collection.get('stock');
const Order = Mongo.Collection.get('order');
const Buyer = Mongo.Collection.get('buyer');


if (Meteor.isServer) {
    Meteor.publish('comment', function () {
        return Comment.find({});
    });
}

Meteor.methods({
    'comment.addComment'(order_id, stock_id, comment, customer_anonymous, customer_comment_images, customer_name) {
        //should called by order's function ,now reversed
        check(order_id, String);
        check(stock_id, Array);
        check(comment, Array);
        check(customer_anonymous, Boolean);
        check(customer_comment_images, Array);
        let customer_portrait = '';
        let order = Order.findOne({_id:order_id});
        if(order != null){
            let buyer = Buyer.findOne({_id:order.customer_id});
            if(buyer != null && typeof buyer.icon != 'undefined'){
                customer_portrait = buyer.icon
            }
        }
        let comment_id = [];
        for(let i in stock_id){
            let customer_has_images = false;
            if(customer_comment_images[i].length > 0){
                customer_has_images = true;
            }
            let comment_id_temp = Comment.insert({
                order_id:order_id,
                stock_id:stock_id[i],
                comment_five_stars:comment[i].arr, //socle speed service label package
                customer_comment_product:comment[i].goods,
                customer_comment_service:comment[i].server,
                customer_anonymous:customer_anonymous,
                customer_comment_images:customer_comment_images[i],
                customer_has_images:customer_has_images,
                comment_createAt:new Date(),
                customer_additional_comment:'',
                customer_has_addition:false,
                seller_product_comment:'',
                customer_name:customer_name,
                customer_portrait:customer_portrait
            });
            comment_id.push(comment_id_temp);
            //shoud in stock
            let stock = Stock.findOne({_id: stock_id[i]});
            if(stock != null){
                if(typeof stock.order_count == 'undefined' || stock.order_count == 0){
                    Stock.update({_id: stock_id[i]},{$set:{order_count: 1, comment_five_stars: comment[i].arr, comment:[comment_id_temp]}});
                }else{
                    let comment_five_stars = {'socle':0,'speed':0,'package':0,'service':0,'label':0};
                    comment_five_stars.socle = (stock.comment_five_stars.socle + comment[i].arr.socle)/(stock.order_count+1);
                    comment_five_stars.speed = (stock.comment_five_stars.speed + comment[i].arr.speed)/(stock.order_count+1);
                    comment_five_stars.package = (stock.comment_five_stars.package + comment[i].arr.package)/(stock.order_count+1);
                    comment_five_stars.service = (stock.comment_five_stars.service + comment[i].arr.service)/(stock.order_count+1);
                    comment_five_stars.label = (stock.comment_five_stars.label + comment[i].arr.label)/(stock.order_count+1);
                    Stock.update({_id: stock_id[i]},{$inc:{order_count: 1}, $set:{comment_five_stars: comment_five_stars}, $push:{comment:comment_id_temp}});
                }
            }
        }
        Meteor.call('order.evaluateProduct', order_id, comment_id, (err) => {
            if (err) {

            } else {

            }
        });
    },
    'comment.addAdditionalComment'(_id, comment) {
        check(_id, String);
        check(comment, String);
        let customer_has_addition = false;
        if(comment != ''){
            customer_has_addition = true;
        }
        if(Comment.findOne({_id: _id}) != null){
            Comment.update({_id: _id}, {$set: {customer_additional_comment: comment, customer_has_addition: customer_has_addition}});
        }else{
            throw new Meteor.Error("comment id not found");
        }
    },
    'comment.addSellerComment'(_id, comment, seller_comment_star) {
        check(_id, String);
        check(comment, String);
        if(Comment.findOne({_id: _id}) != null){
            Comment.update({_id: _id}, {$set: {seller_product_comment: comment, seller_comment_star: seller_comment_star}});
        }else{
            throw new Meteor.Error("comment id not found");
        }
    }
});