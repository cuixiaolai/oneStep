import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import config from '../config.json';

export const Quote = new Mongo.Collection('quote');
export const Ids = new Mongo.Collection('ids');
const Order = Mongo.Collection.get('order');
const Cart = Mongo.Collection.get('cart');
const Buyer = Mongo.Collection.get('buyer');
const Seller = Mongo.Collection.get('seller');
const Stock = Mongo.Collection.get('stock');
const Product = Mongo.Collection.get('product');
const Users = Mongo.Collection.get('users');


if (Meteor.isServer) {
    Meteor.publish('quote', function () {
        return Quote.find({});
    });
    
//    Meteor.subscribe("stock");  
}

/* pylon */
if (Meteor.isClient) {
	Meteor.subscribe("stock");  
	Meteor.subscribe("seller"); 
}

Meteor.methods({
    'quote.insertProduct'(cartInfo, deliveryAddress) {
        check(cartInfo, Array);
        check(deliveryAddress, Number);
        let data = [];
        for (let i = 0; i < cartInfo.length; ++ i) {
            let key = {company: cartInfo[i].company, type: cartInfo[i].stockType};
            let j = 0;
            for (; j < data.length; ++ j) {
                if (data[j].key.company == key.company && data[j].key.type == key.type) {
                    let value = {
                        stockId: cartInfo[i].stockId,
                        quantity: cartInfo[i].quantity,
                    };
                    data[j].value.push(value);
                    break;
                }
            }
            if (j == data.length) {
                let value = {
                    stockId: cartInfo[i].stockId,
                    quantity: cartInfo[i].quantity,
                };
                let item = {
                    key: key,
                    value: [value],
                }
                data.push(item);
            }
        }
        if (Quote.findOne({_id: Meteor.userId()}) == null) {
            Quote.insert({_id: Meteor.userId(), product: data, delivery_address: deliveryAddress});
        }
        else {
            Quote.update({_id: Meteor.userId()}, {$set: {product: data, delivery_address: deliveryAddress}});
        }
    },

    'quote.updateRecord'(stockId, quantity, company, delivery_address, stock_type) {
        check(stockId, String);
        check(quantity, Number);
        check(company, String);
        check(delivery_address, Number);
        check(stock_type, String);

        let key = {company: company, type: stock_type};
        let value = {stockId: stockId, quantity: quantity};
        let item = {key: key, value: [value]};
        if (Quote.findOne({_id: Meteor.userId()}) == null) {
            Quote.insert({_id: Meteor.userId(), product: [item], delivery_address: delivery_address});
        }
        else {
            Quote.update({_id: Meteor.userId()}, {$set: {product: [item], delivery_address: delivery_address}});
        }
    },

    'quote.createOrder'(addressId, totalPrice, totalQuantity, postage, packagePrice, labelPrice, listPrice, InvoiceType, InvoiceTitle, InvoiceWay, InvoiceEmail, InvoiceAddressType, InvoiceAddress, InvoiceName, options, optionsfile, order, remark,rate) {
        check(addressId, Number);
        check(totalPrice, Number);
        check(totalQuantity, Number);
        check(postage, Array);
        check(packagePrice, Number);
        check(labelPrice, Number);
        check(listPrice, Number);
        check(InvoiceType, Number);
        check(InvoiceTitle, String);
        check(InvoiceWay, Number);
        check(InvoiceEmail, String);
        check(InvoiceAddressType, Number);
        check(InvoiceAddress, Number);
        check(InvoiceName, String);
        check(options, Array);
        check(optionsfile, Array);
        check(order, String);
        check(remark, String);

        let stockInfo2 = Stock.findOne({_id: 'Tmi7mwppXkikSqyDi'});
        let Productrr555 = Seller.find({_id: 'aYXqJCe6CPQoLBYaX'}).fetch()[0];
        let Productrr55566 = Seller.findOne({_id: 'aYXqJCe6CPQoLBYaX'});
        
        let Productrr55566555 = Cart.findOne({_id: 'aq7P5i4GCEQxv2nQr'});
        
        
        let Productrr = Product.find({_id: "DoQavSimpWfJypqD4"}).fetch()[0];
        let Productrr222 = Product.findOne({_id: "DoQavSimpWfJypqD4"});
        let Productrr5dfdf55 = Quote.findOne({_id: "fRhCRfNimF6cSS9zA"});

//        debugger;
        
        let address;
        let invoiceAddress;
        let userInfo = Buyer.findOne({_id: Meteor.userId()}, { fields: {'address': 1, 'nickname': 1, 'company': 1, 'invoice': 1}});
        for (let i = 0; i < userInfo.address.length; ++ i) {
            if (userInfo.address[i].id == addressId) {
                address = userInfo.address[i];
                if (InvoiceAddressType == 1) {
                    invoiceAddress = userInfo.address[i];
                }
            }
            else if (userInfo.address[i].id == InvoiceAddress && InvoiceAddressType == 2) {
                invoiceAddress = userInfo.address[i];
            }
        }
        let company_invoice = null;
        if(InvoiceType == 2){
            company_invoice = userInfo.invoice;
        }
        let name = userInfo.nickname != null ? userInfo.nickname : (userInfo.company != null ? userInfo.company : Meteor.user().username);
        let quoteInfo = Quote.findOne({_id: Meteor.userId()});
        let currency = quoteInfo.delivery_address == 0 ? 1 : 2;
        let orderInfo = [];
        let sellerInfo = [];
        let orderId = [];
        
        // pylon
        let i = 0;
        for (i = 0; i < quoteInfo.product.length; i ++ /* pylon */) {
            let sellerId = '';
            let product = [];
            for (let j = 0; j < quoteInfo.product[i].value.length;  j ++ /* pylon */) {
                if (Cart.findOne({_id: Meteor.userId(), 'cart.stockId': quoteInfo.product[i].value[j].stockId}) != null) {
                    Cart.update({_id: Meteor.userId()}, {
                        $pull: {
                            'cart': {
                                'stockId': quoteInfo.product[i].value[j].stockId,
                                'delivery_address': quoteInfo.delivery_address
                            }
                        }, $inc: {size: -1}
                    });
                }

                let stockId2  = quoteInfo.product[i].value[j].stockId;
                let stockInfo  = Stock.findOne({_id: stockId2});
                
//                debugger;
//                Meteor.call('stock.findOneStock', stockId2, function(err, data) {
//                    if (err) {
//                        console.log(err);
//                    }
//                    else {
//                    	stockInfo = data;
//                    	debugger;
//                    }
//                }.bind(this));
                

                if( stockInfo != undefined)  // pylon
                	{
		                let productInfo = Product.findOne({'Manufacturer Part Number': stockInfo.product_id});
		                let item = {
		                    stock_id: quoteInfo.product[i].value[j].stockId,
		                    product_id: stockInfo.product_id,
		                    manufacturer: stockInfo.manufacturer,
		                    package: stockInfo.package,
		                    price: stockInfo.price,
		                    quantity: quoteInfo.product[i].value[j].quantity,
		                    img: productInfo.Image,
		                    describe: productInfo.Description
		                }
		                product.push(item);
		                sellerId = stockInfo.user_id;
                	}else{ // pylon
                		message.warning('不能提交订单!');
                		return {seller: "", order: ""};
                	}
            }
            let Id = Ids.findAndModify({"query": { "name": 'order' },
                "update": { "$inc": {
                    "id": 1
                }},
                "options": { "new": true, "upsert": true }});
            let seller_info = null;
            let seller_aut = Buyer.findOne({_id: sellerId}, { fields: {'authentication': 1}});
            if(typeof seller_aut.authentication != 'undefinded'){
                seller_info = seller_aut.authentication;
            }
            let seller_contact = null;
            let seller_con = Seller.findOne({_id: sellerId}, { fields: {'contact': 1}});
//            debugger;
            if( seller_con.contact != undefined /* pylon */){
                seller_contact = seller_con.contact;
            }
            let id2 = Meteor.uuid();
// hide by pylon            
//            let item = Order.insert({id: id2 /* Id.id pylon */, order_status: 1, customer_id: Meteor.userId(), customer_name: name, seller_id: sellerId, company_name: quoteInfo.product[i].key.company,
//                company_type: quoteInfo.product[i].key.type, currency: currency, postage: postage[i], packagePrice: packagePrice, labelPrice: labelPrice, listPrice: listPrice, product: product,
//                address: address, invoice_address: invoiceAddress, invoice_name: InvoiceName,invoice_type: InvoiceType, invoice_title: InvoiceTitle, invoice_way: InvoiceWay, invoice_email: InvoiceEmail,
//                options: options, optionsfile: optionsfile, order: order, remark: remark, createAt: new Date(),
//                reminder_delivery: 0, reminder_date: new Date(), delay_receive: 0, update_time: new Date() ,company_invoice: company_invoice, seller_info: seller_info, seller_contact: seller_contact,
//                problem_order: 1, operation: {customer_receive:false, customer_comment:false, customer_addition:false, seller_comment:false}, additional_comment: false,
//                return_goods_status: 0, return_fund_status: 0, quality_assurance_status: 0,
//                payment_application: 0
//            });
            
            let item = Order.insert({id: id2 /* Id.id pylon */, 
  	          order_status: 1, customer_id: Meteor.userId(), customer_name: name, seller_id: sellerId, /* 用户*/
  	          company_name: quoteInfo.product[i].key.company,   company_type: quoteInfo.product[i].key.type,  /* company */
  	          currency: currency, postage: postage[i], packagePrice: packagePrice, labelPrice: labelPrice, listPrice: listPrice, 
  	          product: product,
  	          address: address, 
  	          /* invoice 发票 */
  	          invoice_address: invoiceAddress, invoice_name: InvoiceName,invoice_type: InvoiceType, invoice_title: InvoiceTitle, invoice_way: InvoiceWay, invoice_email: InvoiceEmail,
  	          options: options, optionsfile: optionsfile, /* 三个包装要求  */
  	          order: order,/*  备注订单号 */
  	          remark: remark,/* 订单备注  */ 
  	          createAt: new Date(), /*  创建时间  */
  	          reminder_delivery: 0, /*  提醒发货次数 */
  	          reminder_date: new Date(), /* 为判断每24小时提醒发货次数所记录的第一次提醒时间  */
  	          delay_receive: 0,/*  0无延长发货 1延长发货  */ 
  	          update_time: new Date() , 
  	          company_invoice: company_invoice, seller_info: seller_info, seller_contact: seller_contact, /* 推测是订单联系人  */
  	          problem_order: 1,  /* 1非问题订单 2问题订单  */
  	          operation: {customer_receive:false, customer_comment:false, customer_addition:false, seller_comment:false}, /*  订单操作状态，用于页面跳转，例如确认货页面显示该用户所有订单中customer_receive为true的订单 */
  	          additional_comment: false, /*  是否追评 */
  	          return_goods_status: 0, 
	  		  return_goods:{
	          		"product_choice" : "product_choice",   /* 订单中具体哪一款产品需要退货 */
	        		        "amount" : "", 
	         			 "return_reason" : "", 
	          		"explain" : "", 
	         			 "agreed" : 1, 
	         			 "refused_reason" : "", 
	          		"addition" : "", 
	         			 "delivery_company" : "", 
	          		"delivery_number" : "", 
	         			 "createAt_problem" : "", 
	         			 "update_time_problem" : ""
	     			 },
	  		  return_fund_status: 0,   /*   */
	  		  return_fund : {
	          		"amount" : "amount", 
	          		"return_reason" : "return_reason", 
	          		"explain" : "explain", 
	          		"agreed" : "", 
	          		"refused_reason" : "refused_reason", 
	          		"addition" : "", 
	          		"createAt_problem" : "", 
	          		"update_time_problem" : ""
	      			}, 
	  		  quality_assurance_status: 0, /*   */
	  		  quality_assurance : {
	          		"product_choice" : "", 
	          		"agency" : "", 
	          		"delivery_company" : "", 
	          		"delivery_number" : "", 
	          		"createAt_problem" : "", 
	          		"update_time_problem" : ""
	      			},
  	          payment_application: 0, /*   */
	  		  closed_reason:"质量问题",/* 交易关闭原因 */
	  		  comment_time:"2017-03-17",
	  		  delivery : { 		 /* 快递  */
	          		"delivery_type" : "", 
	          		"delivery_stock" : [
	              		{
	                  		"stock_id" : 1, 
	                  		"stock_address" : [
	                      			100
	                  			], 
	                  		"origin_address" : [
	                      			1000
	                  			], 
	                  		"batch" : [
	                      		10000
	                  		], 
	                  		"package" : [
	                      		500
	                  		]
	              		}
	          		], 
	          		"logistics_company" : "sf", 
	          		"logistics_number" : 110988757012
	      		} 
              },function(err,data){
                if(!rate){
                   if(!Meteor.isServer){
                        $.ajax({
                            type: 'GET',
                            url: config.file_server+id2,
                            success: function (data) {
                                console.log(data);
                            },
                            error: function (data) {
                                console.log(data);
                            }
                        });    
                   }

                }
              });            
            
            
            
            orderInfo.push(item);
            sellerInfo.push(sellerId);
            orderId.push(id2 /* Id.id pylon */ );
            //customer info
            let customer = Users.findOne({_id: Meteor.userId()});
            if (quoteInfo.delivery_address == 0) {
                if (Roles.userIsInRole(Meteor.userId(), 'company')) {
                    if (Seller.findOne({_id: sellerId, 'company_customer.customer_id': Meteor.userId()}) == null) {
                        Seller.update({_id: sellerId}, {$addToSet: {company_customer: {customer_id: Meteor.userId(), totalRMB: totalPrice, totalDollar: 0}}});
                    }
                    else {
                        Seller.update({_id: sellerId, 'person_customer.customer_id': Meteor.userId()}, {$inc: {'company_customer.$.totalRMB': totalPrice}});
                    }
                    Seller.update({_id: sellerId, 'company_customer.customer_id': Meteor.userId()},{$set: {
                        'company_customer.$.user_name': customer.username,
                        'company_customer.$.company_name': typeof userInfo.company == 'undefined'?'':userInfo.company,
                        'company_customer.$.phone': typeof customer.phone == 'undefined'?'':customer.phone,
                        'company_customer.$.emails':  typeof customer.emails == 'undefined'?'':customer.emails[0].address
                    }});
                }
                else if (Roles.userIsInRole(Meteor.userId(), 'person')) {
                    if (Seller.findOne({_id: sellerId, 'person_customer.customer_id': Meteor.userId()}) == null) {
                        Seller.update({_id: sellerId}, {$addToSet: {person_customer: {customer_id: Meteor.userId(), totalRMB: totalPrice, totalDollar: 0}}});
                    }
                    else {
                        Seller.update({_id: sellerId, 'person_customer.customer_id': Meteor.userId()}, {$inc: {'person_customer.$.totalRMB': totalPrice}});
                    }
                    Seller.update({_id: sellerId, 'person_customer.customer_id': Meteor.userId()},{$set: {
                        'person_customer.$.user_name': customer.username,
                        'person_customer.$.contact_name': name,
                        'person_customer.$.phone': typeof customer.phone == 'undefined'?'':customer.phone,
                        'person_customer.$.emails':  typeof customer.emails == 'undefined'?'':customer.emails[0].address
                    }});
                }
            }
            else {
                if (Roles.userIsInRole(Meteor.userId(), 'company')) {
                    if (Seller.findOne({_id: sellerId, 'company_customer.customer_id': Meteor.userId()}) == null) {
                        Seller.update({_id: sellerId}, {$addToSet: {company_customer: {customer_id: Meteor.userId(), totalRMB: 0, totalDollar: totalPrice}}});
                    }
                    else {
                        Seller.update({_id: sellerId, 'person_customer.customer_id': Meteor.userId()}, {$inc: {'company_customer.$.totalDollar': totalPrice}});
                    }
                    Seller.update({_id: sellerId, 'company_customer.customer_id': Meteor.userId()},{$set: {
                        'company_customer.$.user_name': customer.username,
                        'company_customer.$.company_name': typeof userInfo.company == 'undefined'?'':userInfo.company,
                        'company_customer.$.phone': typeof customer.phone == 'undefined'?'':customer.phone,
                        'company_customer.$.emails':  typeof customer.emails == 'undefined'?'':customer.emails[0].address
                    }});
                }
                else if (Roles.userIsInRole(Meteor.userId(), 'person')) {
                    if (Seller.findOne({_id: sellerId, 'person_customer.customer_id': Meteor.userId()}) == null) {
                        Seller.update({_id: sellerId}, {$addToSet: {person_customer: {customer_id: Meteor.userId(), totalRMB: 0, totalDollar: totalPrice}}});
                    }
                    else {
                        Seller.update({_id: sellerId, 'person_customer.customer_id': Meteor.userId()}, {$inc: {'person_customer.$.totalDollar': totalPrice}});
                    }
                    Seller.update({_id: sellerId, 'person_customer.customer_id': Meteor.userId()},{$set: {
                        'person_customer.$.user_name': customer.username,
                        'person_customer.$.contact_name': name,
                        'person_customer.$.phone': typeof customer.phone == 'undefined'?'':customer.phone,
                        'person_customer.$.emails':  typeof customer.emails == 'undefined'?'':customer.emails[0].address
                    }});
                }
            }

            //need to delete data in seller ?
            let date_schedule = new Date();
            date_schedule.setTime(date_schedule.getTime() + 7200000);
            let schedule = require("node-schedule");
            let schedule_action = schedule.scheduleJob(date_schedule, Meteor.bindEnvironment(function(){
                let schedule_order=Order.findOne({'id': Id.id}); //Id?
                if(schedule_order != null && schedule_order.order_status == 1){
                    Order.update({'id': Id.id}, {$set: {order_status:7, update_time: new Date(), closed_reason:'overtime'}});


                }
            }));
        }
        Quote.update({_id: Meteor.userId()}, {$set: {product: [], orders: orderInfo, currency: currency}});
        return {seller: sellerInfo, order: orderId};
    },

    'quote.payOrder'( order_id ){
        check(order_id, String);

        let order = Order.findOne({_id: order_id});
        Quote.update({_id: Meteor.userId()}, {$set: {orders: [order_id], currency: order.currency}});
    }
});
