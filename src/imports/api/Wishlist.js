import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Wishlist = new Mongo.Collection('wishlist');

Meteor.methods({
    'wishlist.addItem'(manufacturer, product_id, quantity, currency, price, company, publish, phone) {
        check(manufacturer, String);
        check(product_id, String);
        check(quantity, Number);
        check(currency, Number);
        check(price, Number);
        check(company, String);
        check(publish, String);
        check(phone, String);

        Wishlist.insert({manufacturer: manufacturer,product_id:product_id,model: product_id, quantity: quantity, currency: currency, price: price, company: company, publish: publish, phone: phone,releaseTime:new Date()});
    }
});