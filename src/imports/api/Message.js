import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Message = new Mongo.Collection('message');

if (Meteor.isServer) {
    Meteor.publish('message', function () {
        return Message.find({});
    });
}

Meteor.methods({
    'message.createMessage'(from, to, type, string) {
        check(from, String);
        check(to, String);
        check(type, Number);
        check(string, String);

        Message.insert({from: from, to: to, type: type, string: string, sign: 1, date: new Date()});
    },

    'message.readMessage'() {
        let messageInfo = Message.find({sign: 1}).fetch();
        for (let i = 0; i < messageInfo.length; ++ i) {
            Message.update({_id: messageInfo[i]._id}, {$set: {sign: 0}});
        }
    },

    'message.deleteMessage'(id) {
        check(id, String);

        Message.remove({_id: id});
    },

    'message.deleteAllnoReadMessage'() {
        let messageInfo = Message.find({sign: 1}).fetch();
        for (let i = 0; i < messageInfo.length; ++ i) {
            Message.remove({_id: messageInfo[i]._id});
        }
    },

    'message.deleteAllReadMessage'() {
        let messageInfo = Message.find({sign: 0}).fetch();
        for (let i = 0; i < messageInfo.length; ++ i) {
            Message.remove({_id: messageInfo[i]._id});
        }
    },
});