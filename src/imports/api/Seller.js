import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Seller = new Mongo.Collection('seller');
const Buyer = Mongo.Collection.get('buyer');
const Users = Mongo.Collection.get('users');

if (Meteor.isServer) {
    Meteor.publish('seller', function () {
        return Seller.find({});
    });
}

Meteor.methods({
    'seller.create'() {
        Seller.insert({_id: Meteor.userId()});
    },

    'seller.addAuthentication'(agent, manufacturer) {
        check(agent, Array);
        check(manufacturer, Array);

        Seller.update(Meteor.userId(), {$set: {authentication: {retailer: {}, agent: {files: agent}, manufacturer: {files: manufacturer}, verify: false}}});
    },

    'seller.ckeckSuccess'() {
        Seller.update(Meteor.userId(), {$set: {'authentication.verify': true, 'authentication.success': true}});
    },

    'seller.deleteAuthentication'() {
        Seller.update(Meteor.userId(), {$unset: {authentication: 1, agent: 1, manufacturer: 1}});
    },

    'seller.updateAgentAuthentication'(agent) {
        check(agent, Array);

        Seller.update(Meteor.userId(), {$set: {'authentication.agent': {files: agent, verify: false}}});
    },

    'seller.updateAgentSuccess'() {
        Seller.update(Meteor.userId(), {$unset: {'authentication.agent.verify': 1}});
    },

    'seller.updateAgentFail'() {
        Seller.update(Meteor.userId(), {$set: {'authentication.agent.verify': true}});
    },

    'seller.deleteAgentAuthentication'() {
        Seller.update(Meteor.userId(), {$set: {'authentication.agent': {files: []}, 'agent': []}});
    },

    'seller.updateManufacturerSuccess'() {
        Seller.update(Meteor.userId(), {$unset: {'authentication.manufacturer.verify': 1}});
    },

    'seller.updateManufacturerFail'() {
        Seller.update(Meteor.userId(), {$set: {'authentication.manufacturer.verify': true}});
    },

    'seller.deleteManufacturerAuthentication'() {
        Seller.update(Meteor.userId(), {$set: {'authentication.manufacturer': {files: []}, 'manufacturer': []}});
    },

    'seller.updateManufacturerAuthentication'(manufacturer) {
        check(manufacturer, Array);

        Seller.update(Meteor.userId(), {$set: {'authentication.manufacturer': {files: manufacturer, verify: false}}});
    },

    'seller.updateContactInfo'(contact_name,contact_phone,contact_telephone,contact_email) {
        check(contact_name, String);
        if (contact_phone != null)
            check(contact_phone, String);
        if (contact_telephone != null)
            check(contact_telephone, String);
        if (contact_email != null)
            check(contact_email, String);
        Seller.update(Meteor.userId(), {$set: {'contact.name':contact_name, 'contact.phone':contact_phone,'contact.telephone':contact_telephone,'contact.email':contact_email}});
    },

    'seller.getSellerInfo'(userId) {
        check(userId, String);

        let buyerInfo = Buyer.findOne({_id: userId});
        let sellerInfo = Seller.findOne({_id: userId});
        let userInfo = Users.findOne({_id: userId});

        if (buyerInfo != null && sellerInfo !=  null && userInfo != null) {
            let phone = '';
            if (sellerInfo.contact != null && sellerInfo.contact.phone != '') {
                phone = sellerInfo.contact.phone;
            }
            if (sellerInfo.contact != null && sellerInfo.contact.telephone != '') {
                if (phone == '') {
                    phone = sellerInfo.contact.telephone;
                }
                else {
                    phone += ', ' + sellerInfo.contact.telephone;
                }
            }
            let item = {
                company: buyerInfo.authentication == null ? '' : buyerInfo.authentication.company,
                address: buyerInfo.authentication == null ? '' : buyerInfo.authentication.address,
                company_url: buyerInfo.company_url,
                company_email: userInfo.emails[0].address,
                company_detail: buyerInfo.company_detail,
                contact_name: sellerInfo.contact == null ? '' : sellerInfo.contact.name,
                contact_phone: phone,
                contact_email: sellerInfo.contact == null ? '' : sellerInfo.contact.email,
                agent: sellerInfo.agent == null ? [] : sellerInfo.agent,
                manufacturer: sellerInfo.manufacturer == null ? [] : sellerInfo.manufacturer,
            };
            return item;
        }
    },

    'seller.addDouble'(amount) {
        check(amount, Number);

        let sellerInfo = Seller.findOne({_id: Meteor.userId()});
        if (sellerInfo.double != null && sellerInfo.double.total != 0) {
            Seller.update({_id: Meteor.userId()}, {$set: {'double.last': amount, 'double.verify': false}, $inc: {'double.total': amount}});
        }
        else {
            Seller.update({_id: Meteor.userId()}, {$set: {'double.last': amount, 'double.verify': false, 'double.total': amount}});
        }
    },

    'seller.againDouble'() {
        let sellerInfo = Seller.findOne({_id: Meteor.userId()});
        let amount = sellerInfo.double.last;
        Seller.update({_id: Meteor.userId()}, {$set: {'double.last': 0, 'double.verify': true, 'double.success': true}, $inc: {'double.total': -amount}});
    },

    'seller.paybackDouble'() {
        Seller.update({_id: Meteor.userId()}, {$unset: {'double': 1}});
    },

    'seller.addGuarantee'(amount) {
        check(amount, Number);

        let sellerInfo = Seller.findOne({_id: Meteor.userId()});
        if (sellerInfo.guarantee != null && sellerInfo.guarantee.total != 0) {
            Seller.update({_id: Meteor.userId()}, {$set: {'guarantee.last': amount, 'guarantee.verify': false}, $inc: {'guarantee.total': amount}});
        }
        else {
            Seller.update({_id: Meteor.userId()}, {$set: {'guarantee.last': amount, 'guarantee.verify': false, 'guarantee.total': amount}});
        }
    },

    'seller.againGuarantee'() {
        let sellerInfo = Seller.findOne({_id: Meteor.userId()});
        let amount = sellerInfo.guarantee.last;
        Seller.update({_id: Meteor.userId()}, {
            $set: {
                'guarantee.last': 0,
                'guarantee.verify': true,
                'guarantee.success': true
            }, $inc: {'guarantee.total': -amount}
        });
    },

    'seller.paybackGuarantee'() {
        Seller.update({_id: Meteor.userId()}, {$unset: {'guarantee': 1}});
    },

    'seller.addStockVerify'(username, phone, address, describe) {
        check(username, String);
        check(phone, String);
        check(address, String);
        check(describe, String);

        Seller.update({_id: Meteor.userId()}, {
            $addToSet: {
                stock_verify: {
                    username: username,
                    phone: phone,
                    address: address,
                    describe: describe,
                    verify: false,
                    price: 0,
                    pay: false,
                    date: new Date()
                }
            }
        });
    },

    'seller.cancelStockVerify'() {
        Seller.update({_id: Meteor.userId()}, {$pull: {
            'stock_verify': {
                'pay': false,
            }
        }});
    },

    'seller.again'() {
        Seller.update({_id: Meteor.userId()}, {$pull: {
            'stock_verify': {
                'verify': true,
                'success': false,
            }
        }});
    },

    'seller.payStockVerify'() {
        Seller.update({_id: Meteor.userId(), 'stock_verify.pay': false}, {$set: {'stock_verify.$.pay': true}});
        Roles.addUsersToRoles(Meteor.userId(), 'stock_verify');
    },

    'seller.addCustomerRemarks'(customer_id, remarks, type) {
        check(customer_id, String);
        check(remarks, String);
        check(type, String);
        if(type == 'company'){
            Seller.update({_id: Meteor.userId(), 'company_customer.customer_id':customer_id}, {$set: {'company_customer.$.remarks': remarks}});
        }else if(type == 'person'){
            Seller.update({_id: Meteor.userId(), 'person_customer.customer_id':customer_id}, {$set: {'person_customer.$.remarks': remarks}});
        }
    },
});
