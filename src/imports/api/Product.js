import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Product = new Mongo.Collection('product');
export const NewProduct = new Mongo.Collection('new_product');
const Users = Mongo.Collection.get('users');
import config from '../config.json';

if (Meteor.isServer) {
    Meteor.publish('product.part_number', function () {
        return Product.find();
    });

    Meteor.publish('new_product.part_number', function () {
        return NewProduct.find();
        check(manufacturer, String);
        check(datasheet, String);
    });
}

Meteor.methods({
    // todo
    'product.addNewProduct'(id, manufacturer, datasheet) {
        check(id, String);
        check(manufacturer, String);
        check(datasheet, String);

        if (NewProduct.findOne({"product_id": id.toUpperCase()}) != null || Product.findOne({"Manufacturer Part Number": id.toUpperCase()}) != null){
            throw new Meteor.Error("product id used");
        } else {
            userInfo = Users.findOne({_id: Meteor.userId()});
            var product_id = '';
            if (userInfo != null) {
               product_id = NewProduct.insert({user_id: Meteor.userId(), product_id: id.toUpperCase(), manufacturer: manufacturer, datasheet: datasheet, contact: userInfo.username, telphone: userInfo.phone, submit_date: new Date(),refused: false ,varified:false ,refused_reason:''});
            }
            else {
               product_id = NewProduct.insert({user_id: Meteor.userId(), product_id: id.toUpperCase(), manufacturer: manufacturer, datasheet: datasheet, submit_date: new Date(),refused: false ,varified:false ,refused_reason:''});
            }


                    // luckcui
        // if(!Meteor.isServer){
        //             const url = config.file_server + "addProduct?product_id=" + id.toUpperCase();
        //             var xhr_rate = new XMLHttpRequest();
        //             xhr_rate.open('get', url, true);
        //             xhr_rate.send();
        //             xhr_rate.onload = function (event) {
        //                 const rate_ret = JSON.parse(xhr_rate.responseText)
        //                 if (rate_ret.code == 200) {
        //                     if(rate_ret.productList.length==0){
        //                         console.log("0000");
        //                          Meteor.call('product.updaterate', "00099000",product_id);
        //                         // Product.update({_id: product}, {$set:{"rate": "000000"}});

        //                     }else{
        //                         Meteor.call('product.updaterate', "0009900090",product_id);
        //                     }
        //                 }
        //             }

        //          }else{
        //              console.log("shifuwuqi");
        //          }

// luckcui
        }



    },

    //  'product.updaterate'(value,product_id){
    //     console.log("dddd");
    //     NewProduct.update({_id: product_id}, {$set:{"rate": value}});
    // },

    'product.checkNewProduct'(id) {
        check(id, String);

        let item = NewProduct.findOne({"product_id": id.toUpperCase()});
        if (Product.findOne({"Manufacturer Part Number": id.toUpperCase()}) != null) {
            throw new Meteor.Error("product id used");
        }
        else if (item != null && item.varified==false && item.refused==false){
            throw new Meteor.Error("product id auditing");
        }

    },

    'product.deleteNewProduct'(id) {
        check(id, String);

        if (NewProduct.findOne({_id: id.toUpperCase()}) != null){
            NewProduct.remove({_id: id.toUpperCase()});
        } else {
            throw new Meteor.Error("product id not found");
        }
    },

    'product.findDetailAndImg'(productId) {
        check(productId, String);
        let productInfo = Product.findOne({'Manufacturer Part Number': productId.toUpperCase()});

        let item = {
            img: "",//productInfo.Image,
            detail: "",//productInfo.Description,
        };
        
        if(typeof productInfo != 'undefined'){
        	item = {
                    img: productInfo.Image,
                    detail: productInfo.Description,
                };
        }
        
        return item;
    },

    'product.findProductInfo'(productId) {
        check(productId, String);
        let productInfo = Product.findOne({'Manufacturer Part Number': productId.toUpperCase()});
        return productInfo;
    },
});