import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Order = new Mongo.Collection('order');
const Comment = Mongo.Collection.get('comment');

if (Meteor.isServer) {
    Meteor.publish('order', function () {
        return Order.find({});
    });
}

Meteor.methods({
    'order.findPaymentInfo'(orderId) {
        check(orderId, String);
        let orderInfo = Order.findOne({_id: orderId});
        let price = 0;
        let name = '';
        for (let j = 0; j < orderInfo.product.length; ++ j) {
            let i = 0;
            for (; i < orderInfo.product[j].price.length; ++ i) {
                if (orderInfo.product[j].quantity < orderInfo.product[j].price[i].min) {
                    break;
                }
            }
            price += orderInfo.product[j].quantity * orderInfo.product[j].price[i-1].price;
            if (name == '')  {
                name = orderInfo.product[j].product_id;
            }
            else {
                name += ',' + orderInfo.product[j].product_id;
            }
        }
        price += orderInfo.postage;
        price += orderInfo.packagePrice;
        price += orderInfo.labelPrice;
        price += orderInfo.listPrice;
        let item = {
            id: orderInfo.id,
            seller_id: orderInfo.seller_id,
            name: name,
            company: orderInfo.company_name,
            price: price,
            payment_application:orderInfo.payment_application
        };
        return item;
    },
    'order.findBy_Id'(id){
        check(id,String);
        let orderinfo = Order.findOne({_id:id});
        return orderinfo;
    },
    'order.forwardToOperation'(orderId,type) {
        check(orderId, String);
        check(type, String);
        if (Order.findOne({'_id': orderId}) != null) {
            if(type == 'customer_pay'){
                Order.update({'_id':orderId }, {$set: {'operation.customer_pay': true} });
                Order.update({'_id':{ $ne : orderId },customer_id:Meteor.userId()}, {$set: {'operation.customer_pay': false}},{multi: true});
            }else if(type == 'customer_receive'){
                Order.update({'_id':orderId }, {$set: {'operation.customer_receive': true} });
                Order.update({'_id':{ $ne : orderId },customer_id:Meteor.userId()}, {$set: {'operation.customer_receive': false}},{multi: true});
            }else if(type == 'customer_comment'){
                Order.update({'_id':orderId }, {$set: {'operation.customer_comment': true} });
                Order.update({'_id':{ $ne : orderId },customer_id:Meteor.userId()}, {$set: {'operation.customer_comment': false}},{multi: true});
            }else if(type == 'customer_addition'){
                Order.update({'_id':orderId }, {$set: {'operation.customer_addition': true} });
                Order.update({'_id':{ $ne : orderId },customer_id:Meteor.userId()}, {$set: {'operation.customer_addition': false}},{multi: true});
            }else if(type == 'seller_comment'){
                Order.update({'_id':orderId }, {$set: {'operation.seller_comment': true} });
                Order.update({'_id':{ $ne : orderId },seller_id:Meteor.userId()}, {$set: {'operation.seller_comment': false}},{multi: true});
            }
        } else {
            throw new Meteor.Error("order id not found");
        }
    },
    'order.cancelOrder'(orderId,closed_reason) {
        check(orderId, String);
        check(closed_reason, String);
        if (Order.findOne({'_id': orderId}) != null) {
            Order.update({'_id':orderId }, {$set: {order_status:7, update_time: new Date(), closed_reason:closed_reason}});
        } else {
            throw new Meteor.Error("order id not found");
        }
    },
    'order.remindDelivery'(orderId) {
        check(orderId, String);
        let order=Order.findOne({'_id': orderId});
        if (order != null) {
            let count;
            let new_date=new Date();

            if(new_date.getTime()-order.reminder_date.getTime() > 86400000){
                Order.update({'_id':orderId }, {$set: {reminder_date:new_date, reminder_delivery:1} });
                count = 2;
            }else if(order.reminder_delivery+1 <= 3){
                Order.update({'_id':orderId }, {$inc: {reminder_delivery:1} });
                count = 2-order.reminder_delivery;
            }else{
                count = -1;
            }
            return count;
        } else {
            throw new Meteor.Error("order id not found");
        }
    },
    'order.delayReceive'(orderId) {
        check(orderId, String);
        let order=Order.findOne({'_id': orderId});
        if (order != null) {
            let delayed = false;
            if(order.delay_receive == 0){
                Order.update({'_id':orderId }, {$inc: {delay_receive:1} });
                let schedule = require("node-schedule");
                //client throw error,but server run reschedule normally
                schedule.scheduledJobs[orderId+'customerAutoReceive'].reschedule(order.update_time.getTime() + 1641600000); // 2 weeks 1209600000 + 5 days
                delayed = true;
            }
            return delayed;
        } else {
            throw new Meteor.Error("order id not found");
        }
    },
    'order.payImmediately'(orders){ // now is payment application
        check(orders,Array);
        if (orders.length > 0) {
            for (let i in orders) { //need to add function that check each _id
                Order.update({'_id':orders[i] }, {$set: {update_time: new Date(),payment_application: 1}}); // payment_application, 1:application submit 2:application approved
                // Order.update({'_id':orders[i] }, {$inc: {order_status:1}, $set: {update_time: new Date()}});
            }
        } else {
            throw new Meteor.Error("orders not found");
        }
    },
    'order.deliverGoods'(orderId,delivery){
        check(orderId, String);
        check(delivery, Object);
        if (Order.findOne({'_id': orderId}) != null) {
            Order.update({'_id':orderId }, {$inc: {order_status:1}, $set: {update_time: new Date(), delivery: delivery}});

            let date_schedule = new Date();
            date_schedule.setTime(date_schedule.getTime() + 1209600000); // 2 weeks 1209600000
            let schedule = require("node-schedule");
            let schedule_action = schedule.scheduleJob(orderId+'customerAutoReceive', date_schedule, Meteor.bindEnvironment(function(){
                let schedule_order = Order.findOne({'_id': orderId});
                if(schedule_order != null && schedule_order.order_status == 3){
                    Order.update({'_id':orderId }, {$inc: {order_status:1}, $set: {update_time: new Date()}});
                    customerAutoComment(orderId);
                }
            }));
        } else {
            throw new Meteor.Error("order id not found");
        }
    },
    'order.confirmReceive'(orderId){
        check(orderId, String);
        if (Order.findOne({'_id': orderId}) != null) {
            Order.update({'_id':orderId }, {$inc: {order_status:1}, $set: {update_time: new Date()}});

            customerAutoComment(orderId);
        } else {
            throw new Meteor.Error("order id not found");
        }
    },
    'order.evaluateProduct'(orderId,comment){
        check(orderId, String);
        check(comment, Array);
        let order=Order.findOne({'_id': orderId});
        if (order != null) {
            for(let i in comment){
                Order.update({'_id': orderId,
                    'product': {
                        $elemMatch: {
                            'product_id': order.product[i].product_id,
                        }
                    }
                }, {$set: {'product.$.comment': comment[i]}});
            }
            Order.update({'_id':orderId }, {$inc: {order_status:1}, $set: {update_time: new Date(), comment_time:new Date()} });
        } else {
            throw new Meteor.Error("order id not found");
        }
    },
    'order.sellerComment'(orderId,seller_product_comment,seller_comment_star){
        check(orderId, String);
        check(seller_product_comment, Array);
        check(seller_comment_star, Number);

        let order=Order.findOne({'_id': orderId});
        if (order != null) {
            for(let i in order.product){
                Meteor.call('comment.addSellerComment', order.product[i].comment, seller_product_comment[i], seller_comment_star, (err) => {
                    if (err) {
                        throw new Meteor.Error("comment id not found");
                    } else {

                    }
                });
            }
            Order.update({'_id':orderId }, {$inc: {order_status:3}, $set: {update_time: new Date()}});
        } else {
            throw new Meteor.Error("order id not found");
        }
    },
    'order.customerAdditionalComment'(orderId, comment){
        check(orderId, String);
        check(comment, Array);
        let order=Order.findOne({'_id': orderId});
        if (order != null) {
            Order.update({'_id': orderId}, {$set: {'additional_comment':true, update_time: new Date()}});
            for(let i in comment){
                Meteor.call('comment.addAdditionalComment', order.product[i].comment, comment[i], (err) => {
                    if (err) {
                        throw new Meteor.Error("comment id not found");
                    } else {

                    }
                });
            }
        } else {
            throw new Meteor.Error("order id not found");
        }
    },
    'order.buyerApplyReturnFund'(orderId, amount, return_reason, explain){ //also used by buyer modify application,may need to add param
        check(orderId, String);
        check(amount, Number);
        check(return_reason, String);
        check(explain, String);
        let order = Order.findOne({'_id': orderId});
        if (order != null) {
            Order.update({'_id':orderId}, {$set: {
                return_fund_status: 1,
                return_fund:{
                    amount:amount,
                    return_reason:return_reason,
                    explain:explain,
                    agreed:false,
                    refused_reason:'',
                    addition:'',
                    createAt_problem: new Date(),
                    update_time_problem: new Date()
                },
                problem_order:2,
            }});
        }
    },
    'order.sellerVerifyReturnFund'(orderId, agreed,refused_reason,addition){
        check(orderId, String);
        check(agreed, Boolean);
        check(refused_reason, String);
        check(addition, String);
        let order = Order.findOne({'_id': orderId});
        if (order != null) {
            Order.update({'_id':orderId}, {$set: {
                return_fund_status:2,
                'return_fund.agreed':agreed,
                'return_fund.refused_reason':refused_reason,
                'return_fund.addition':addition,
                'return_fund.update_time_problem': new Date(),
            }});
            if(!agreed){// attention
                Order.update({'_id':orderId}, {$set: {problem_order:3}});
            }
        }
    },
    'order.buyerCancelReturnGoods'(orderId){
        check(orderId, String);
        let order = Order.findOne({'_id': orderId});
        if (order != null) {
            Order.update({'_id':orderId}, {$set: {
                return_goods_status: 0,
                problem_order:1,
            },$unset:{return_goods:''}});
        }
    },
    'order.buyerApplyReturnGoods'(orderId, product_choice, return_reason,amount,explain){ //also used by buyer modify application,may need to add param
        check(orderId, String);
        check(product_choice, Array);
        check(return_reason, String);
        check(amount, Number);
        check(explain, String);
        let order = Order.findOne({'_id': orderId});
        if (order != null) {
            Order.update({'_id':orderId}, {$set: {
                return_goods_status: 1,
                return_goods:{
                    product_choice:product_choice,
                    amount:amount,
                    return_reason:return_reason,
                    explain:explain,
                    agreed:false,
                    refused_reason:'',
                    addition:'',
                    delivery_company:'',
                    delivery_number:0,
                    createAt_problem: new Date(),
                    update_time_problem: new Date()
                },
                problem_order:2,
            }});
        }
    },
    'order.sellerVerifyReturnGoods'(orderId, agreed,refused_reason,addition){ //used by seller on status 1 & status 3
        check(orderId, String);
        check(agreed, Boolean);
        check(refused_reason, String);
        check(addition, String);
        let order = Order.findOne({'_id': orderId});
        if (order != null) {
            Order.update({'_id':orderId}, {$set: {
                'return_goods.agreed':agreed,
                'return_goods.refused_reason':refused_reason,
                'return_goods.addition':addition,
                'return_goods.update_time_problem': new Date()
            }});
            if(agreed){
                Order.update({'_id':orderId},{$inc:{'return_goods_status':1}});
            }else{
                Order.update({'_id':orderId},{$set:{'return_goods_status':4, problem_order:3}}); // attention
            }
        }
    },
    'order.buyerDeliverReturnGoods'(orderId, delivery_company,delivery_number){
        check(orderId, String);
        check(delivery_company, String);
        check(delivery_number, Number);
        let order = Order.findOne({'_id': orderId});
        if (order != null) {
            Order.update({'_id':orderId}, {$set: {
                'return_goods_status':3,
                'return_goods.delivery_company':delivery_company,
                'return_goods.delivery_number':delivery_number,
                'return_goods.update_time_problem': new Date()
            }});
        }
    },
    'order.buyerApplyQualityAssurance'(orderId, product_choice, quality_assurance_agency){
        check(orderId, String);
        check(product_choice, Array);
        check(quality_assurance_agency, String);
        let order = Order.findOne({'_id': orderId});
        if (order != null) {
            Order.update({'_id':orderId}, {$set: {
                quality_assurance_status:2, //  should be 1, set 2 because of need verification by platform
                quality_assurance:{
                    product_choice:product_choice,
                    agency:quality_assurance_agency,
                    delivery_company:'',
                    delivery_number:0,
                    createAt_problem: new Date(),
                    update_time_problem: new Date()
                },
                problem_order:2,
            }});
        }
    },
    'order.buyerPayQualityAssurance'(orderId){
        check(orderId, String);
        let order = Order.findOne({'_id': orderId});
        if (order != null) {
            Order.update({'_id':orderId}, {$set: {
                quality_assurance_status:3,
                'quality_assurance.update_time_problem': new Date()
            }});
        }
    },
    'order.buyerDeliveryQualityAssurance'(orderId, delivery_company,delivery_number){
        check(orderId, String);
        check(delivery_company, String);
        check(delivery_number, Number);
        let order = Order.findOne({'_id': orderId});
        if (order != null) {
            Order.update({'_id':orderId}, {$set: {
                quality_assurance_status:5, //  should be 4, set 5 because of need verification by platform
                'quality_assurance.delivery_company':delivery_company,
                'quality_assurance.delivery_number':delivery_number,
                'quality_assurance.update_time_problem': new Date()
            }});
        }
    }
});
function customerAutoComment(orderId){
    let date_schedule_comment = new Date();
    date_schedule_comment.setTime(date_schedule_comment.getTime() + 1814400000);
    let schedule_comment = require("node-schedule");
    let schedule_action_comment = schedule_comment.scheduleJob(date_schedule_comment, Meteor.bindEnvironment(function(){
        let schedule_order_comment = Order.findOne({'_id': orderId});
        if(schedule_order_comment != null && schedule_order_comment.order_status == 4) {
            let stock_id = [];
            let comment = [];
            let customer_comment_images = [];
            for (let i in schedule_order_comment.product) {
                stock_id.push(schedule_order_comment.product[i].stock_id);
                comment.push({
                    arr:{
                        socle:5,
                        speed:5,
                        service:5,
                        label:5,
                        package:5
                    },
                    goods:'',
                    server:''
                });
                customer_comment_images.push([]);
            }
            Meteor.call('comment.addComment', orderId, stock_id,comment,true,customer_comment_images,schedule_order_comment.customer_name, (err) => {
                if (err) {
                } else {
                }
            });

        }
    }));
}