import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Phone = new Mongo.Collection('phone');

Meteor.methods({
    'phone.sendMessage'(phone_number) {
        check(phone_number, String);
        const phone = Phone.findOne({phone_number:phone_number});
        const phone_existing = (phone != null);

        if(!phone_existing || phone.count < 3){
            let str = '';
            for(var i =0;i<4;i++){
                str += rand(10).toString();
            }
            const client = new TopClient({
                'appkey' : '23514442' ,
                'appsecret' : '7fb6b0929219442c0613db3eab723f11' ,
                'REST_URL' : 'http://gw.api.taobao.com/router/rest'
            });
            client.execute( 'alibaba.aliqin.fc.sms.num.send' , {
                'extend' : '' ,
                'sms_type' : 'normal' ,
                'sms_free_sign_name' : '注册验证' ,
                'sms_param' : "{code:\'"+str+"\',product:'ICxyz商城'}" ,
                'rec_num' : phone_number ,
                'sms_template_code' : "SMS_25275176"
            },  Meteor.bindEnvironment(function(error, response) {
                if (!error){
                    if(phone_existing){
                        Phone.update({phone_number:phone_number },
                            {
                                $set: {
                                    code:str,
                                    // verified:false //not used now
                                },
                                $inc: {count:1}
                            });
                    }else{
                        const phone_id = Phone.insert({
                            phone_number:phone_number,
                            code:str, count:1,
                            // verified:false
                        });
                        // not acturally restrict three times in 24h,and this kind of methods also used in many task
                        let date_schedule = new Date();
                        date_schedule.setTime(date_schedule.getTime() + 86400000);
                        let schedule = require("node-schedule");
                        let schedule_action = schedule.scheduleJob(date_schedule, Meteor.bindEnvironment(function(){
                            let schedule_order=Phone.findOne({_id: phone_id});
                            if(schedule_order != null){
                                Phone.remove({_id: phone_id});
                            }
                        }));
                    }
                }
                else{
                    // code: 15, isv.BUSINESS_LIMIT_CONTROL, error and still can receive message
                    console.log(error);
                }
            }));
        }else{
            throw new Meteor.Error("send message only three times a day");
        }
    },
    'phone.checkVerifyCode'(phone_number, code) {
        check(phone_number, String);
        check(code, String);
        const phone = Phone.findOne({phone_number:phone_number});
        if(phone != null){
            if(code == phone.code){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    },
    'phone.phoneSubmit'(phone_number, code) {
        check(phone_number, String);
        check(code, String);
        const phone = Phone.findOne({phone_number:phone_number});
        if(phone != null){
            if(code == phone.code){
                // Phone.update({_id:phone._id}, {$set: {verified:true}});
            }else{
                throw new Meteor.Error("verify code wrong");
            }
        }else{
            throw new Meteor.Error("phone number not found");
        }
    },
    // not used now
    // 'phone.checkPhoneVerified'(phone_number) {
    //     check(phone_number, String);
    //     const phone = Phone.findOne({phone_number:phone_number});
    //     if(phone != null){
    //         if(phone.verified){
    //             Phone.update({_id:phone._id}, {$set: {verified:true}});
    //         }else{
    //             throw new Meteor.Error("phone number not verified");
    //         }
    //     }else{
    //         throw new Meteor.Error("phone number not found");
    //     }
    // }
});

// create random
rnd.today=new Date();
rnd.seed=rnd.today.getTime();
function rnd() {
    rnd.seed = (rnd.seed*9301+49297) % 233280;
    return rnd.seed/(233280.0);
};
function rand(number) {
    return Math.floor(rnd()*number);
};
//topClient from ali api
var urllib = require('urllib');
function TopClient(options) {
    if (!(this instanceof TopClient)) {
        return new TopClient(options);
    }
    options = options || {};
    if (!options.appkey || !options.appsecret) {
        throw new Error('appkey or appsecret need!');
    }
    this.REST_URL = options.REST_URL || 'http://gw.api.taobao.com/router/rest';
    this.appkey = options.appkey;
    this.appsecret = options.appsecret;
}
TopClient.prototype.invoke = function (method, params, reponseNames, defaultResponse, type, callback) {
    params.method = method;
    this.request(params, type, function (err, result) {
        if (err) {
            return callback(err);
        }
        var response = result;
        if (reponseNames && reponseNames.length > 0) {
            for (var i = 0; i < reponseNames.length; i++) {
                var name = reponseNames[i];
                response = response[name];
                if (response === undefined) {
                    break;
                }
            }
        }
        if (response === undefined) {
            response = defaultResponse;
        }
        callback(null, response);
    });
};
TopClient.prototype._wrapJSON = function (s) {
    var matchs = s.match(/\"id\"\:\s?\d{16,}/g);
    if (matchs) {
        for (var i = 0; i < matchs.length; i++) {
            var m = matchs[i];
            s = s.replace(m, '"id":"' + m.split(':')[1].trim() + '"');
        }
    }
    return s;
};
var IGNORE_ERROR_CODES = {
    'isv.user-not-exist:invalid-nick': 1
};
TopClient.prototype.request = function (params, type, callback) {
    if (typeof type === 'function') {
        callback = type;
        type = null;
    }
    var err = checkRequired(params, 'method');
    if (err) {
        return callback(err);
    }
    var args = {
        timestamp: this.timestamp(),
        format: 'json',
        app_key: this.appkey,
        v: '2.0',
        sign_method: 'md5'
    };
    for (var k in params) {
        if(typeof params[k] == "object"){
            args[k] = JSON.stringify(params[k]);
        }else{
            args[k] = params[k];
        }
    }
    args.sign = this.sign(args);
    type = type || 'GET';
    var options = {type: type, data: args, agent: this.agent, };
    var that = this;
    urllib.request(that.REST_URL, options, function (err, buffer) {
        var data;
        if (buffer) {
            buffer = that._wrapJSON(buffer.toString());
            try {
                data = JSON.parse(buffer);
            } catch (e) {
                err = e;
                e.data = buffer.toString();
                data = null;
            }
        }
        var errRes = data && data.error_response;
        if (errRes) {
            if (!errRes.sub_msg || !IGNORE_ERROR_CODES[errRes.sub_code]) {
                // no sub_msg error, let caller handle it.
                var msg = errRes.msg + ', code ' + errRes.code;
                if (errRes.sub_msg) {
                    msg += '; ' + errRes.sub_code + ': ' + errRes.sub_msg;
                }
                err = new Error(msg);
                err.name = 'TOPClientError';
                err.code = errRes.code;
                err.sub_code = errRes.sub_code;
                err.data = buffer.toString();
                data = null;
            }
        }

        callback(err, data);
    });
};
TopClient.prototype.timestamp = function () {
    return YYYYMMDDHHmmss();
};
TopClient.prototype.sign = function (params) {
    var sorted = Object.keys(params).sort();
    var basestring = this.appsecret;
    for (var i = 0, l = sorted.length; i < l; i++) {
        var k = sorted[i];
        basestring += k + params[k];
    }
    basestring += this.appsecret;
    return md5(basestring).toUpperCase();
};
TopClient.prototype.execute = function (apiname,params, callback) {
    this.invoke(apiname, params, [getApiResponseName(apiname)], null, 'POST', callback);
};

//topUtil from ali api
var crypto = require('crypto');
function hash(method, s, format) {
    var sum = crypto.createHash(method);
    var isBuffer = Buffer.isBuffer(s);
    if (!isBuffer && typeof s === 'object') {
        s = JSON.stringify(sortObject(s));
    }
    sum.update(s, isBuffer ? 'binary' : 'utf8');
    return sum.digest(format || 'hex');
};
function md5(s, format) {
    return hash('md5', s, format);
};
function YYYYMMDDHHmmss(d, options) {
    d = d || new Date();
    if (!(d instanceof Date)) {
        d = new Date(d);
    }

    var dateSep = '-';
    var timeSep = ':';
    if (options) {
        if (options.dateSep) {
            dateSep = options.dateSep;
        }
        if (options.timeSep) {
            timeSep = options.timeSep;
        }
    }
    var date = d.getDate();
    if (date < 10) {
        date = '0' + date;
    }
    var month = d.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    var hours = d.getHours();
    if (hours < 10) {
        hours = '0' + hours;
    }
    var mintues = d.getMinutes();
    if (mintues < 10) {
        mintues = '0' + mintues;
    }
    var seconds = d.getSeconds();
    if (seconds < 10) {
        seconds = '0' + seconds;
    }
    return d.getFullYear() + dateSep + month + dateSep + date + ' ' +
        hours + timeSep + mintues + timeSep + seconds;
};
function checkRequired(params, keys) {
    if (!Array.isArray(keys)) {
        keys = [keys];
    }
    for (var i = 0, l = keys.length; i < l; i++) {
        var k = keys[i];
        if (!params.hasOwnProperty(k)) {
            var err = new Error('`' + k + '` required');
            err.name = "ParameterMissingError";
            return err;
        }
    }
};
function getApiResponseName(apiName){
    var reg = /\./g;
    if(apiName.match("^taobao"))
        apiName = apiName.substr(7);
    return apiName.replace(reg,'_')+"_response";
};