import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Stock = new Mongo.Collection('stock');
export const Users = Mongo.Collection.get('users');
export const Seller = Mongo.Collection.get('seller');
export const Product = Mongo.Collection.get('product');

if (Meteor.isServer) {
    Meteor.publish('stock', function () {
        return Stock.find();
    });
}

Meteor.methods({
    'stock.addStock'(id, manufacturer, standard_packing, inc_number, MOQ, currency, price, stock_address, batch, origin_address, package, DPA, COC, data_packet, guarantee, ROHS, free_ship, trans_expanse, dest_pay) {
        check(id, String);
        check(manufacturer, String);
        check(standard_packing, String);
        check(inc_number, Number);
        check(MOQ, Number);
        check(currency, Number);
        check(price, Array);
        let price_arr = [];
        for (let i = 0; i < price.length; ++ i) {
            price_arr.push({min: price[i][0], max: price[i][1], price: price[i][2]});
        }
        check(stock_address, Array);
        let stock_address_arr = [];
        let stock_quantity = 0;
        for (let i = 0; i < stock_address.length; ++ i) {
            stock_address_arr.push({address: stock_address[i][0], quantity: stock_address[i][1]});
            stock_quantity = stock_quantity + stock_address[i][1];
        }
        check(batch, Array);
        let batch_arr = [];
        for (let i = 0; i < batch.length; ++ i) {
            batch_arr.push({num: batch[i][0], quantity: batch[i][1]});
        }
        check(origin_address, Array);
        let origin_address_arr = [];
        for (let i = 0; i < origin_address.length; ++ i) {
            origin_address_arr.push({address: origin_address[i][0], quantity: origin_address[i][1]});
        }
        check(package, Array);
        let package_arr = [];
        for (let i = 0; i < package.length; ++ i) {
            package_arr.push({package: package[i][2], quantity: package[i][1]});
        }
        check(DPA, Boolean);
        check(COC, Boolean);
        check(data_packet, Boolean);
        check(guarantee, Boolean);
        check(ROHS, Boolean);
        check(free_ship, Number);
        check(trans_expanse, Number);
        check(dest_pay, Boolean);

        let userInfo = Users.findOne({_id: Meteor.userId()});
        let productInfo = Product.findOne({'Manufacturer Part Number': id});
        Stock.insert({ product_id: id, user_id: Meteor.userId(), category1: productInfo.category1, category2: productInfo.category2, img: productInfo.Image, datasheet: productInfo.Datasheets, user_company: userInfo.company,
            manufacturer: manufacturer, standard_packing: standard_packing, inc_number:inc_number, MOQ: MOQ, currency: currency, price: price_arr, stock_quantity: stock_quantity, stock_address: stock_address_arr, batch: batch_arr,
            origin_address: origin_address_arr, package: package_arr, DPA: DPA, COC: COC, data_packet: data_packet, guarantee: guarantee, ROHS: ROHS, free_ship: free_ship, trans_expanse: trans_expanse,
            dest_pay: dest_pay, putaway: false, createAt: new Date(), updateAt: new Date(),
            sold_quantity: 0, order_count: 0, comment_five_stars:{'socle':0,'speed':0,'package':0,'service':0,'label':0}, comment:[]});
    },

    'stock.updateStock'(stockId, id, manufacturer, standard_packing, inc_number, MOQ, currency, price, stock_address, batch, origin_address, package, DPA, COC, data_packet, guarantee, ROHS, free_ship, trans_expanse, dest_pay) {
        check(stockId, String);
        check(id, String);
        check(manufacturer, String);
        check(standard_packing, String);
        check(inc_number, Number);
        check(MOQ, Number);
        check(currency, Number);
        check(price, Array);
        let price_arr = [];
        for (let i = 0; i < price.length; ++ i) {
            price_arr.push({min: price[i][0], max: price[i][1], price: price[i][2]});
            console.log( "price[i][2]");
            console.log( price[i][2]);
        }
        check(stock_address, Array);
        let stock_address_arr = [];
        let stock_quantity = 0;
        for (let i = 0; i < stock_address.length; ++ i) {
            stock_address_arr.push({address: stock_address[i][0], quantity: stock_address[i][1]});
            stock_quantity = stock_quantity + stock_address[i][1];
        }
        check(batch, Array);
        let batch_arr = [];
        for (let i = 0; i < batch.length; ++ i) {
            batch_arr.push({num: batch[i][0], quantity: batch[i][1]});
        }
        check(origin_address, Array);
        let origin_address_arr = [];
        for (let i = 0; i < origin_address.length; ++ i) {
            origin_address_arr.push({address: origin_address[i][0], quantity: origin_address[i][1]});
        }
        check(package, Array);
        let package_arr = [];
        for (let i = 0; i < package.length; ++ i) {
            package_arr.push({package: package[i][2], quantity: package[i][1]});
        }
        check(DPA, Boolean);
        check(COC, Boolean);
        check(data_packet, Boolean);
        check(guarantee, Boolean);
        check(ROHS, Boolean);
        check(free_ship, Number);
        check(trans_expanse, Number);
        check(dest_pay, Boolean);

        let userInfo = Users.findOne({_id: Meteor.userId()});
        let productInfo = Product.findOne({'Manufacturer Part Number': id});
        Stock.update({_id: stockId}, {$set: { product_id: id, user_id: Meteor.userId(), category1: productInfo.category1, category2: productInfo.category2, img: productInfo.Image, datasheet: productInfo.Datasheets,
            user_company: userInfo.company, manufacturer: manufacturer, standard_packing: standard_packing, inc_number:inc_number, MOQ: MOQ, currency: currency, price: price_arr, stock_quantity: stock_quantity,
            stock_address: stock_address_arr, batch: batch_arr, origin_address: origin_address_arr, package: package_arr, DPA: DPA, COC: COC, data_packet: data_packet, guarantee: guarantee, ROHS: ROHS,
            free_ship: free_ship, trans_expanse: trans_expanse, dest_pay: dest_pay, putaway: false, updateAt: new Date()}});
    },

    'stock.outOfStock'(stockId) {
        check(stockId, String);

        Stock.update({_id: stockId}, {$set: {putaway: false, updateAt: new Date()}});
    },

    'stock.allOutOfStock'() {
        let cursor = Stock.find({user_id: Meteor.userId()}, {fields: {'_id': 1}});
        cursor.forEach(function (item) {
            Stock.update({_id: item['_id']}, {$set: {putaway: false, updateAt: new Date()}});
        });
    },

    'stock.allAgentOutOfStock'() {
        let cursor = Stock.find({user_id: Meteor.userId(), type: 'agent'}, {fields: {'_id': 1}});
        cursor.forEach(function (item) {
            Stock.update({_id: item['_id']}, {$set: {putaway: false, updateAt: new Date()}});
        });
    },

    'stock.allManufacturerOutOfStock'() {
        let cursor = Stock.find({user_id: Meteor.userId(), type: 'manufacturer'}, {fields: {'_id': 1}});
        cursor.forEach(function (item) {
            Stock.update({_id: item['_id']}, {$set: {putaway: false, updateAt: new Date()}});
        });
    },

    'stock.showUpStock'(stockId) {
        check(stockId, String);

        let userInfo = Users.findOne({_id: Meteor.userId()});
        let stockInfo = Stock.findOne({_id: stockId});
        if (Roles.userIsInRole(Meteor.userId(), 'manufacturer')&&Seller.findOne({_id: Meteor.userId(), manufacturer: stockInfo.manufacturer}) != null) {
            Stock.update({_id: stockId}, {$set: {user_company: userInfo.company, putaway: true, type: 'manufacturer', updateAt: new Date()}});
        }
        else if (Roles.userIsInRole(Meteor.userId(), 'agent')&&Seller.findOne({_id: Meteor.userId(), agent: stockInfo.manufacturer}) != null) {
            Stock.update({_id: stockId}, {$set: {user_company: userInfo.company, putaway: true, type: 'agent', updateAt: new Date()}});
        }
        else {
            Stock.update({_id: stockId}, {$set: {user_company: userInfo.company, putaway: true, type: 'retailer', updateAt: new Date()}});
        }
    },

    'stock.deleteStock'(stockId) {
        check(stockId, String);

        Stock.remove({_id: stockId});
    },

    'stock.findOneStock'(stockId) {
        check(stockId, String);
        let stockInfo = Stock.findOne({_id: stockId});

        return stockInfo;
    },

    'stock.reduceStockByDelivery'(stocks) {
        check(stocks, Array);
        for(let i in stocks){
            let stock = Stock.findOne({'_id': stocks[i].stock_id});
            if (stock != null) {
                let sold_quantity = 0;
                let stock_address = [];
                for(let j in stock.stock_address){
                    sold_quantity += stocks[i].stock_address[j]; //(reduce total quantity)&(add sold quantity) by stock_address
                    stock_address.push({
                        address: stock.stock_address[j].address,
                        quantity: stock.stock_address[j].quantity - stocks[i].stock_address[j]
                    })
                }
                let origin_address = [];
                for(let j in stock.origin_address){
                    origin_address.push({
                        address: stock.origin_address[j].address,
                        quantity: stock.origin_address[j].quantity - stocks[i].origin_address[j]
                    })
                }
                let batch = [];
                for(let j in stock.batch){
                    batch.push({
                        num: stock.batch[j].num,
                        quantity: stock.batch[j].quantity - stocks[i].batch[j]
                    })
                }
                let package = [];
                for(let j in stock.package){
                    package.push({
                        package: stock.package[j].package,
                        quantity: stock.package[j].quantity - stocks[i].package[j]
                    })
                }
                let stock_quantity = stock.stock_quantity - sold_quantity;

                sold_quantity += typeof stock.sold_quantity == 'undefined'?0:stock.sold_quantity;

                Stock.update({_id: stock._id},{$set:{stock_address:stock_address, origin_address:origin_address, batch:batch, package:package, stock_quantity:stock_quantity, sold_quantity:sold_quantity}});
            }
        }
    },
});