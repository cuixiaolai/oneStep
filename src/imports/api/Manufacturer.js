import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Manufacturer = new Mongo.Collection('manufacturer');

if (Meteor.isServer) {
    Meteor.publish('manufacturer', function () {
        return Manufacturer.find({});
    });
}