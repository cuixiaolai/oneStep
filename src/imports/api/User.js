import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Accounts } from 'meteor/accounts-base';

export const Users = Mongo.Collection.get('users');

if (Meteor.isServer) {
    Meteor.publish('users', function () {
        return Users.find({});
    });
}

Meteor.methods({
    'user.checkName'(name) {
        check(name, String);

        if (Users.findOne({username: name}) != null){
            throw new Meteor.Error("name used");
        }
    },

    'user.checkPhone'(number) {
        check(number, String);

        if (Users.findOne({phone: number}) != null){
            throw new Meteor.Error("phone used");
        }
    },

    'user.checkEmail'(email) {
        check(email, String);

        if (Users.findOne({"emails.address": email.toLowerCase()}) != null){
            throw new Meteor.Error("email used");
        }
    },

    'user.checkComName'(name) {
        check(name, String);

        let user = Users.findOne({company: name});
        if (user != null && user._id != Meteor.userId()){
            throw new Meteor.Error("name used");
        }
    },

    'user.checkCompany'(name) {
        check(name, String);

        if (Users.findOne({company: name}) != null){
            throw new Meteor.Error("company used");
        }
    },

    'user.checkUser'(name) {
        check(name, String);

        var user = Accounts.findUserByUsername(name);
        if ( user != null ){
            return user;
        }
        user = Accounts.findUserByEmail(name);
        if ( user != null ){
            return user;
        }
        user = Users.findOne({phone: name});
        if ( user != null ){
            return user;
        }
        throw new Meteor.Error("no user");
    },

    'user.addPhone'(number) {
        check(number, String);

        Users.update(Meteor.userId(), {$set: {phone: number}});
    },

    'user.addEmail'(email) {
        //luck-cui
         // email.toLowerCase()；
        //

        check(email.toLowerCase(), String);

        Users.update(Meteor.userId(), {$push: {emails: {
            $each: [{address: email.toLowerCase(), verify: true}]}}});
    },

    'user.changePhone'(number) {
        check(number, String);

        if (Meteor.user().username == Meteor.user().phone) {
            Accounts.setUsername(Meteor.userId(), number);
        }
        Users.update(Meteor.userId(), {$set: {phone: number}});
    },

    'user.changeEmail'(email) {
        check(email, String);

        if (Meteor.user().username == Meteor.user().emails[0].address) {
            Accounts.setUsername(Meteor.userId(), email);
        }
        Users.update(Meteor.userId(), {$unset: {emails: 1}});
        Users.update(Meteor.userId(), {$push: {emails: {
            $each: [{address: email, verify: true}]}}});
    },

    'user.addCompany'(name) {
        check(name, String);

        Users.update(Meteor.userId(), {$set: {company: name}});
    },

    'user.phoneToUserName'(number) {
        check(number, String);

        var user = Users.findOne({phone: number});
        if (user == null) {
            return null;
        }
        else {
            return user.username;
        }
    },

    'user.verifyEmailResetPasswordCode'(userId, code) {
        check(userId, String);
        check(code, String);

        let user = Users.findOne({_id: userId});
        if (user.email_resetpassword != null) {
            if (user.email_resetpassword.code == code && user.email_resetpassword.verify == false) {
                Users.update(userId, {$set: {email_resetpassword: {code: code, verify: true}}});
                return;
            }
        }
        throw new Meteor.Error("verify failed");
    },
    //开通质保机构
    'user.addWarrantyAgency'(id){
        let user = Users.findOne({_id: Meteor.userId()});
        let s = [];
        if(user.warrantyAgency != undefined){
            if(user.warrantyAgency.length >= 0){
                user.warrantyAgency.push(id);
                Users.update({_id:user._id},{$set:{warrantyAgency:user.warrantyAgency}});
            }else{
                s.push(id);
                Users.update({_id:user._id},{$set:{warrantyAgency:s}});
            }

        }else{
            s.push(id);
            Users.update({_id:user._id},{$set:{warrantyAgency:s}});
        }
    }
});
