import React, { Component } from 'react';
import { Link } from 'react-router';
import { Tabs, Col } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
// import {createContainer} from 'meteor/react-meteor-data';
// import {Order} from '../api/Order.js';
import ReturnNoteAll from '../modules/returnnote/ReturnNoteAll.jsx';
{/*买家退换货单*/}

export default class ReturnNote extends Component {
    render() {
        let data = []
        for(let i in this.props.orderResult){
            let flag = true;
            let type = '';
            let update_time_problem = '';
            if(this.props.orderResult[i].return_goods_status > 0 && (this.props.orderResult[i].return_fund_status == 0 || this.props.orderResult[i].return_fund_status == 2) && (this.props.orderResult[i].quality_assurance_status == 0 || this.props.orderResult[i].quality_assurance_status == 5)){
                type = 'return_goods';
                update_time_problem = this.props.orderResult[i].return_goods.update_time_problem;
            }else if(this.props.orderResult[i].return_fund_status > 0 && this.props.orderResult[i].return_goods_status == 0 && this.props.orderResult[i].quality_assurance_status == 0){
                type = 'return_fund';
                update_time_problem = this.props.orderResult[i].return_fund.update_time_problem;
            }else if(this.props.orderResult[i].quality_assurance_status > 0 && (this.props.orderResult[i].return_fund_status == 0 || this.props.orderResult[i].return_fund_status == 2) && (this.props.orderResult[i].return_goods_status == 0 || this.props.orderResult[i].return_goods_status == 4)){
                type = 'quality_assurance';
                update_time_problem = this.props.orderResult[i].quality_assurance.update_time_problem;
            }else{
                flag = false;
            }
            if(flag){
                let temp = {
                    _id:this.props.orderResult[i]._id,
                    id:this.props.orderResult[i].id,
                    company_type:this.props.orderResult[i].company_type,
                    company_name:this.props.orderResult[i].company_name,
                    customer_name:this.props.orderResult[i].customer_name,
                    currency:this.props.orderResult[i].currency,
                    type:type,
                    update_time_problem:update_time_problem,
                    product:this.props.orderResult[i].product,
                    problem_detail:{
                        return_fund_status:this.props.orderResult[i].return_fund_status,
                        return_goods_status:this.props.orderResult[i].return_goods_status,
                        quality_assurance_status:this.props.orderResult[i].quality_assurance_status,

                        return_fund:(typeof this.props.orderResult[i].return_fund == 'undefined'?null:this.props.orderResult[i].return_fund),
                        return_goods:(typeof this.props.orderResult[i].return_goods == 'undefined'?null:this.props.orderResult[i].return_goods),
                        quality_assurance:(typeof this.props.orderResult[i].quality_assurance == 'undefined'?null:this.props.orderResult[i].quality_assurance)
                    }
                };
                data.push(temp);
            }
        }
        if (data != null) {
            data.sort((a,b)=>{return b.update_time_problem.getTime() - a.update_time_problem.getTime()})
            return (
                <div className="sellerCenterOrder">
                    <ReturnNoteAll data={data} user_type='person' />
                </div>
            );
        } else {
            return <div className="sellerCenterOrder"></div>;
        }
    }
}
// export default createContainer(() => {
//     Meteor.subscribe('order');
//     return {
//         orderResult: Order.find({customer_id: Meteor.userId(), problem_order: {$in:[2,3]}}).fetch(),
//     };
// }, ReturnNote);
