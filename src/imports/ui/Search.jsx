import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import Tablebox from '../modules/search/TableBox.jsx'
import SearchTop from '../modules/search/SearchTop.jsx';
import SearchBox from '../modules/search/SearchBox.jsx';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Stock } from '../api/Stock.js';
import { Button,Icon,message} from 'antd';
import config from '../config.json';

export default class Search extends Component {
	constructor(props) {
		super(props);
		this.state = ({
			flag:false,
			Stock:false,
			Origin:false,
			Batch:false,
			Packing:false,
			Tinum:'',
			result:false,
			Release:false,
			addtocart:false,
			addtocartdata:'',
			addtopayment:false,
			topaymentdata:'',
			stockcount:3,
			agentcount:3,
			retailercount:3,
			stocktype:'',
			showcard:false,
			setproduct:I18n.t('home.product'),
			setshop:I18n.t('buyer.Manufacturer'),
			setaddress:I18n.t('home.address'),
			platform_verify:false,
			shop_deposit:false,
			double_indemnity:false,
			return_exchange:false,
			gaurantee:false,
			data_packet:false,
			COC:false,
			DPA:false,
			ROHS:false,
			priceOrder:false,
			shipmentOrder:false,
			reputationOrder:false,
			num:5,
		});
		this.ShowMessage = this.ShowMessage.bind(this);
		this.ShowStock = this.ShowStock.bind(this);
		this.ShowOrigin = this.ShowOrigin.bind(this);
		this.ShowBatch = this.ShowBatch.bind(this);
		this.ShowPacking = this.ShowPacking.bind(this);
	};
	onChange(e) {
		console.log(`checked = ${e.target.checked}`,e.target.value);
		switch (e.target.value) {
			case 'platform':
				this.setState({platform_verify: e.target.checked});
				break;
			case 'shop':
				this.setState({shop_deposit: e.target.checked});
				break;
			case 'double':
				this.setState({double_indemnity: e.target.checked});
				break;
			case 'Return':
				this.setState({return_exchange: e.target.checked});
				break;
			case 'Warranty':
				this.setState({gaurantee: e.target.checked});
				break;
			case 'data':
				this.setState({data_packet: e.target.checked});
				break;
			case 'COC':
				this.setState({COC: e.target.checked});
				break;
			case 'DPA':
				this.setState({DPA: e.target.checked});
				break;
			case 'ROHS':
				this.setState({ROHS: e.target.checked});
				break;
			default:
				break;
		}
	};
	onChanges(pagination, filters, sorter) {
		switch (sorter.field) {
			case 'price':
				this.setState({priceOrder: sorter.order, shipmentOrder: false, reputationOrder: false});
				break;
			case 'shipment':
				this.setState({priceOrder: false, shipmentOrder: sorter.order, reputationOrder: false});
				break;
			case 'reputation':
				this.setState({priceOrder: false, shipmentOrder: false, reputationOrder: sorter.order});
				break;
			case undefined:
				this.setState({priceOrder: false, shipmentOrder: false, reputationOrder: false});
				break;
			default:
				break;
		}
	};
	setAddress(value){
		this.setState({setaddress: value});
	}
	setShop(value){
		this.setState({setshop: value});
	}
	setProduct(value){
		console.log(value);
		this.setState({setproduct: value});
	}
	ShowMessage(index){
		console.log(index.key);
		this.setState({flag:!this.state.flag,Tinum:index.key});
	};
	ShowMore(){
		if(this.state.flag == false){
			return(
				<p>
					{I18n.t('PersonalCenter.manageStock.more')}
					<img style={{marginLeft:2}} src={(config.theme_path +　'origindown_03.png')} alt=""/>
				</p>
			)
		}else{
			return(
				<p>
					{I18n.t('CompantInvoicsThree.hidden')}
					<img style={{marginLeft:2,transform: 'rotate(180deg)'}} src={(config.theme_path +　'origindown_03.png')} alt=""/>
				</p>
			)
		}
	};
	ShowStock(index,text,record){
		this.setState({Stock:!this.state.Stock});
	};
	ShowOrigin(){
		this.setState({Origin:!this.state.Origin});
	};
	ShowBatch(){
		this.setState({Batch:!this.state.Batch});
	};
	ShowPacking(){
		this.setState({Packing:!this.state.Packing});
	};
	ShowResult(resultcount){
		if(this.props.stockInfo < 1){
			return(
				<div className="SearchResultError">
					<p>
						<div className="left">
							<img src={(config.theme_path + 'origin03.png')} alt=""/>
						</div>
						<div className="left">
							{I18n.t('Search.nofindone')} <span style={{color:'#ed7020'}}>{decodeURIComponent(window.location.pathname.substr(8))}</span> {I18n.t('Search.nofindtwo')}
						</div>
						<div className="clear"></div>
					</p>
					<p>
						{I18n.t('Search.nofindthree')}
					</p>
					<p>{I18n.t('Search.nofindfour')}</p>
					<p>{I18n.t('Search.nofingfive')}<span onClick={this.Openclick.bind(this)}>{I18n.t('Release.requirements')}</span></p>
				</div>
			)
		}else{
			return(
				<p>({I18n.t('Search.Result')}{resultcount}{I18n.t('Search.tiao')})</p>
			)
		}
	};
	Openclick(){
		this.setState({Release:true});
	};
	Closeclick(){
		this.setState({Release:false});
	};
	ShowRelease(){
		if(this.state.Release == true){
			return(
				<SearchBox click={this.Closeclick.bind(this)} flag={true} />
			)
		}
	};
	showAddToCart(){
		if(this.state.addtocart == true){
			return(
				<SearchBox word={I18n.t('top.shop')} click={this.closeAddToCart.bind(this)} flag={false} data={this.state.addtocartdata} Card={this.AddShopCard.bind(this)} />
			)
		}
	};
	showToPayment(){
		if(this.state.topayment == true){
			return(
				<SearchBox word={I18n.t('productdetails.buyimmediately')} click={this.closeToPayment.bind(this)} flag={false} data={this.state.topaymentdata} Card={this.ToPayment.bind(this)}/>
			)
		}
	};
	openAddToCart(data, stocktype){
		if(Meteor.userId() == data.user_id){
			message.warning('不能购买自己的产品');
		} else {
			this.setState({addtocart:true, addtocartdata:data, stocktype:stocktype});

		}
	};
	tick(){
		let path=window.location.pathname;
		console.log(this.state.num);
		let arr = path.split('/');
		console.log(arr);
		if (this.state.num == 0 || arr[1] != 'Search') {
			this.setState({showcard:false,num:5});
			clearInterval(this.interval);
		}
		else {
			this.setState({num:this.state.num - 1});
		}
	}
	AddShopCard(numbervalue,value){
		if (Meteor.userId() == null) {
            message.warning('请先登录账号');
        }
        else {
            Meteor.call('cart.updateRecord', this.state.addtocartdata.key, numbervalue ,this.state.addtocartdata.name ,value, this.state.stocktype, (err) => {
                if (err) {

                } else {
                    this.closeAddToCart();
                    this.interval = setInterval(this.tick.bind(this), 1000);
                    this.setState({showcard:true});
                }
            });
        }
	};
	ToPayment(numbervalue,value){
        if (Meteor.userId() == null) {
            message.warning('请先登录账号');
        }
        else {
            Meteor.call('quote.updateRecord', this.state.topaymentdata.key, numbervalue, this.state.topaymentdata.name, value, this.state.stocktype, (err) => {
                if (err) {

                } else {
                    this.closeToPayment();
                    browserHistory.replace('/Payment');
                }
            });
        }
	}
	openToPayment(data, stocktype){
		if(Meteor.userId() == data.user_id){
			message.warning('不能购买自己的产品');
		} else {
			this.setState({topayment:true, topaymentdata:data, stocktype:stocktype});
		}
	};
	Closeshowcard(){
		this.setState({showcard:false,num:5});
	}
	ShowCard(){
		return(
			<div className="SearchShowCard" style={this.state.showcard ? {display:'block'} : {display:'none'}}>
				<div className="SearchShowCardJiao">
					<div className="SearchShowCardJiaoinner"></div>
				</div>
				<p style={{textAlign:'right'}}>
					<span onClick={this.Closeshowcard.bind(this)} style={{cursor:'pointer'}}>×</span>
				</p>
				<p>
					<div className="left">
						<img src={(config.theme_path + 'right_03.png')} alt=""/>
					</div>
					<div style={{fontSize:'20px',color:'#5b5b5b',marginLeft:10,lineHeight:'24px'}} className="left">{I18n.t('Search.success')}</div>
					<div className="clear"></div>
				</p>
				<p>
					<div style={{marginLeft:34,marginTop:8}}><span>{I18n.t('sellerorderdetails.youcango')} </span> <Link to="/shopping_cart">{I18n.t('Search.payment')}</Link> </div>
				</p>
			</div>
		)
	}
	closeAddToCart(){
		this.setState({addtocart:false});
	};
	closeToPayment(){
		this.setState({topayment:false});
	}
	resetStockCount(){
		this.setState({stockcount:3});
	}
	addStockCount(){
		this.setState({stockcount:this.state.stockcount+10});
	}
	resetAgentCount(){
		this.setState({agentcount:3});
	}
	addAgentCount(){
		this.setState({agentcount:this.state.agentcount+10});
	}
	resetRetailerCount(){
		this.setState({retailercount:3});
	}
	addRetailerCount(){
		this.setState({retailercount:this.state.retailercount+10});
	}
	sortRecord(data){ //sorter undefined
		if(data.length>1){
			if(typeof this.state.priceOrder=='string'){
				switch (this.state.priceOrder) {
					case 'ascend':
						data.sort((a,b) =>{
							return a.address.length-b.address.length
						});
						break;
					case 'descend':
						data.sort((a,b) =>{
							return b.address.length-a.address.length
						});
						break;
					default:
						break;
				}
			}else if(typeof this.state.shipmentOrder=='string'){
				switch (this.state.shipmentOrder) {
					case 'ascend':
						data.sort((a, b) => {
							return a.address.length - b.address.length
						});
						break;
					case 'descend':
						data.sort((a, b) => {
							return b.address.length - a.address.length
						});
						break;
					default:
						break;
				}
			}else if(typeof this.state.reputationOrder=='string'){
				switch (this.state.reputationOrder) {
					case 'ascend':
						data.sort((a, b) => {
							return a.address.length - b.address.length
						});
						break;
					case 'descend':
						data.sort((a, b) => {
							return b.address.length - a.address.length
						});
						break;
					default:
						break;
				}
			}
		}
	};
	category1Filter(category1){
		let flag=false;
		const arr=category1.split(', ');
		arr[0]=arr[0].trim();
		const category=this.state.setproduct[0].split(', ');
		for(let i = 0 ; !flag && i<category.length; i++){
			if(arr.includes(category[i].trim())){
				flag=true;
			}
		}
		return flag;
	}
	render() {
		const columns = [{titlr:'供应商',dataIndex: 'name'}, 
		{titlr:'型号',dataIndex: 'money'}, {titlr:'厂牌',dataIndex: 'address'}, 
		{titlr:'描述',dataIndex: 'miaoshu'}, {titlr:'库存总量',dataIndex: 'kucun'}, 
		{titlr:'库存所在地/数量',dataIndex: 'suozaidi'}, 
		{titlr:'产地/数量',dataIndex: 'chandi'}, 
		{titlr:'批号/数量',dataIndex: 'pihao'}, 
		{titlr:'价格梯度',dataIndex: 'tidu'}, 
		{titlr:'人民币（￥）',dataIndex: 'renmingbi'}, 
		{titlr:'美元（$）',dataIndex: 'meiyuan'}, 
		{titlr:'包装方式/数量',dataIndex: 'baozhuangfangshi'}, 
		{titlr:'操作',dataIndex: 'caozuo'}];
		let resultcount = 0;
		let data_stock = [];
		let data_agent = [];
		let data_retailer = [];
		if (this.props.stockInfo != null) {
			for (let i in this.props.stockInfo) {
				let stock_address = [];
				for (let j in this.props.stockInfo[i].stock_address) {
					let item = [this.props.stockInfo[i].stock_address[j].address, this.props.stockInfo[i].stock_address[j].quantity];
					stock_address.push(item);
				}
				let origin_address = [];
				for (let j in this.props.stockInfo[i].origin_address) {
					let item = [this.props.stockInfo[i].origin_address[j].address, this.props.stockInfo[i].origin_address[j].quantity];
					origin_address.push(item);
				}
				let batch = [];
				for (let j in this.props.stockInfo[i].batch) {
					let item = [this.props.stockInfo[i].batch[j].num, this.props.stockInfo[i].batch[j].quantity];
					batch.push(item);
				}
				let package_way = [];
				for (let j in this.props.stockInfo[i].package) {
					let item = [this.props.stockInfo[i].package[j].package, this.props.stockInfo[i].package[j].quantity];
					package_way.push(item);
				}
				let price = [];
				let rmb = [];
				let dollar = [];
				for (let j in this.props.stockInfo[i].price) {
					let item = this.props.stockInfo[i].price[j].min;
					price.push(item);
					if (this.props.stockInfo[i].currency == 1) {
						rmb.push(this.props.stockInfo[i].price[j].price);
					}
					else if (this.props.stockInfo[i].currency == 2) {
						dollar.push(this.props.stockInfo[i].price[j].price);
					}
				}
				let item_data = {
					key: this.props.stockInfo[i]._id,
					name: this.props.stockInfo[i].user_company,
					money: this.props.stockInfo[i].product_id,
					address: this.props.stockInfo[i].manufacturer,
					miaoshu: '查看规格',
					datasheet: this.props.stockInfo[i].datasheet,
					img: this.props.stockInfo[i].img,
					kucun: this.props.stockInfo[i].stock_quantity,
					suozaidi: stock_address,
					chandi: origin_address,
					pihao: batch,
					baozhuangfangshi: package_way,
					tidu: price,
					renmingbi: rmb,
					meiyuan: dollar,
					caozuo: '立即购买',
					standard_packing: this.props.stockInfo[i].standard_packing,
					inc_number: this.props.stockInfo[i].inc_number,
					MOQ: this.props.stockInfo[i].MOQ,
					user_id: this.props.stockInfo[i].user_id
				}
				if ((!this.state.platform_verify || Roles.userIsInRole(this.props.stockInfo[i].user_id, 'stock_verify'))
					&& (!this.state.shop_deposit || Roles.userIsInRole(this.props.stockInfo[i].user_id, 'guarantee'))
					&& (!this.state.double_indemnity || Roles.userIsInRole(this.props.stockInfo[i].user_id, 'double'))
					&& (!this.state.return_exchange || this.state.return_exchange == this.props.stockInfo[i].dest_pay)	//undefined
					&& (!this.state.gaurantee || this.state.gaurantee == this.props.stockInfo[i].guarantee)
					&& (!this.state.data_packet || this.state.data_packet == this.props.stockInfo[i].data_packet)
					&& (!this.state.COC || this.state.COC == this.props.stockInfo[i].COC)
					&& (!this.state.DPA || this.state.DPA == this.props.stockInfo[i].DPA)
					&& (!this.state.ROHS || this.state.ROHS == this.props.stockInfo[i].ROHS)
					&& (this.state.setshop == I18n.t('buyer.Manufacturer') || this.props.stockInfo[i].manufacturer == this.state.setshop)
					&& (this.state.setaddress == I18n.t('home.address') || this.props.stockInfo[i].stock_address.find((r) => r.address == this.state.setaddress) != null)
					&& (this.state.setproduct == I18n.t('home.product') || this.state.setproduct.length == 0 || (typeof this.props.stockInfo[i].category1 == 'undefined'?false:this.category1Filter(this.props.stockInfo[i].category1)))
				) {
					if (this.props.stockInfo[i].type == 'manufacturer') {
                        resultcount++;
						if(data_stock.length < this.state.stockcount){
							data_stock.push(item_data);
						}
					} else if (this.props.stockInfo[i].type == 'agent') {
                        resultcount++;
						if(data_agent.length < this.state.agentcount) {
							data_agent.push(item_data);
						}
					} else if (this.props.stockInfo[i].type == 'retailer') {
                        resultcount++;
						if(data_retailer.length < this.state.retailercount) {
							data_retailer.push(item_data);
						}
					}
				}
			}
			this.sortRecord(data_stock);
			this.sortRecord(data_agent);
			this.sortRecord(data_retailer);
		}
		//resultcount = data_stock.length + data_agent.length + data_retailer.length;
		return (
			<div className="Search">
				<p>{I18n.t('Search.name')}</p>
				{this.ShowResult(resultcount)}
				{this.ShowRelease()}
				{this.showAddToCart()}
				{this.showToPayment()}
				<SearchTop change={this.onChange.bind(this)} changes={this.onChanges.bind(this)} setAddress={this.setAddress.bind(this)} setShop={this.setShop.bind(this)} setProduct={this.setProduct.bind(this)}/>
				<Tablebox columns={columns}
						  dataSource={data_stock} name={I18n.t('Search.Manufactor')} message={I18n.t('Search.Manufacturers')} addToCart={this.openAddToCart.bind(this)} openToPayment={this.openToPayment.bind(this)} resetCount={this.resetStockCount.bind(this)} addCount={this.addStockCount.bind(this)}  stocktype="manufacturer"/>
				<Tablebox columns={columns}
						  dataSource={data_agent} name={I18n.t('Search.Dealerinventory')} message={I18n.t('Search.Dealer')} addToCart={this.openAddToCart.bind(this)} openToPayment={this.openToPayment.bind(this)}  resetCount={this.resetAgentCount.bind(this)} addCount={this.addAgentCount.bind(this)} stocktype="agent"/>
				<Tablebox columns={columns}
						  dataSource={data_retailer} name={I18n.t('Search.Marketchannels')} message={I18n.t('Search.Market')} addToCart={this.openAddToCart.bind(this)} openToPayment={this.openToPayment.bind(this)}  resetCount={this.resetRetailerCount.bind(this)} addCount={this.addRetailerCount.bind(this)} stocktype="retailer"/>
				{this.ShowCard()}
			</div>
		);
	}
};

// export default createContainer(() => {
// 	Meteor.subscribe('stock');
// 	let str = decodeURIComponent(window.location.pathname.substr(8));
// 	let q = str.toUpperCase();
// 	return {
// 		stockInfo: Stock.find({product_id: {$regex: q}, putaway: true}).fetch(),
// 	};
// }, Search);