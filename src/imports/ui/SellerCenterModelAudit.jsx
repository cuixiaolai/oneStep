import React, { Component , PropTypes} from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { NewProduct } from '../api/Product.js';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,message,Button,Modal } from 'antd';
import config from '../config.json';
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;

export default class SellerCenterModelAudit extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            key:'',
        });
    }
    transformDate(datetemp){
        return datetemp.getFullYear()+'-'+(datetemp.getMonth()+1)+'-'+datetemp.getDate();
    }
    deleteConfirm(id,refused,datasheet){
        const rfsd=refused?'删除':'取消申请';
        confirm({
            title: rfsd,
            content:'请确认是否'+rfsd,
            okText:'是',
            cancelText:'否',
            onOk() {
               Meteor.call('product.deleteNewProduct', id, (err) => {
                    if (err) {
                        message.error(`删除失败。`);
                    } else {
                        $.ajax({
                            type: 'POST',
                            url: config.file_server + 'delete_product',
                            data: { path: datasheet},
                            dataType: "json",
                            success: function (data) {
                                message.success(`操作成功。`);
                                console.log(data);
                            },
                            error: function (data) {
                                console.log(data);
                            }
                        });
                    }
                });
            },
            onCancel() {},
        });
    }
    callback(key) {
        this.setState({key:key});
    }
    render(){
        let data=new Array();
        let columns;
        let key=this.state.key;
        let pagination;
        if(key == '1'||key == ''){
            for(let i in this.props.new_product){
                data.push(this.props.new_product[i]);
            }
            columns=[{
                title: '厂商',
                dataIndex: 'manufacturer',
            },{
                title:'产品型号',
                dataIndex:'product_id',
            },{ //href has double '/'
                title:'数据表',
                dataIndex:'datasheet',
                render: (text,record) => (
                    <span>
                        <a href={(config.file_server + text)} target="_blank">
                            <img src={(config.theme_path + 'table.png')} alt=""/>
                        </a>
                    </span>
                ),
            }, {
                title: '申请时间',
                dataIndex: 'submit_date',
                render: (text,record) => (
                    <span>
                    {text == null?'':this.transformDate(text)}
                    </span>
                ),
            },{
                title:'审核时间',
                dataIndex:'operation_time',
                render: (text,record) => (
                    <span>
                    {text == null?'':this.transformDate(text)}
                    </span>
                ),
            },{
                title:'审核状态',
                render: (text,record) => (
                    <span>
                    {record.varified?'已通过':(record.refused?'未通过':'审核中')}
                    </span>
                ),
            },{
                title:'拒绝原因',
                dataIndex:'refused_reason',
                render: (text,record) => (
                    <span>
                    {text == null?'':text}
                    </span>
                ),
            },{
                title:'操作',
                dataIndex:'_id',
                render:(text, record,index)=>(
                    <span>
                        {record.varified?'':(<Button onClick={this.deleteConfirm.bind(this,text,record.refused,record.datasheet)} type="ghost" style={{width:90,height:30}}>{record.refused?'删除':'取消申请'}</Button>)}
                    </span>
                )
            }];
        }else if(key == '2'){
            for(let i in this.props.new_product){
                if(this.props.new_product[i].varified == true) {
                    data.push(this.props.new_product[i]);
                }
            }
            columns=[{
                title: '厂商',
                dataIndex: 'manufacturer',
            },{
                title:'产品型号',
                dataIndex:'product_id',
            },{ //href has double '/'
                title:'数据表',
                dataIndex:'datasheet',
                render: (text,record) => (
                    <span>
                        <a href={(config.file_server + text)} target="_blank">
                            <img src={(config.theme_path + 'table.png')} alt=""/>
                        </a>
                    </span>
                ),
            }, {
                title: '申请时间',
                dataIndex: 'submit_date',
                render: (text,record) => (
                    <span>
                    {text == null?'':this.transformDate(text)}
                    </span>
                ),
            },{
                title:'审核时间',
                dataIndex:'operation_time',
                render: (text,record) => (
                    <span>
                    {text == null?'':this.transformDate(text)}
                    </span>
                ),
            },{
                title:'审核状态',
                render: (text,record) => (
                    <span>
                    {record.varified?'已通过':(record.refused?'未通过':'审核中')}
                    </span>
                ),
            }];
        }else if(key == '3'){

            for(let i in this.props.new_product){
                if(this.props.new_product[i].varified==false&&this.props.new_product[i].refused==false){
                    data.push(this.props.new_product[i]);
                }
            }
            columns=[{
                title: '厂商',
                dataIndex: 'manufacturer',
            },{
                title:'产品型号',
                dataIndex:'product_id',
            },{ //href has double '/'
                title:'数据表',
                dataIndex:'datasheet',
                render: (text,record) => (
                    <span>
                        <a href={(config.file_server + text)} target="_blank">
                            <img src={(config.theme_path + 'table.png')} alt=""/>
                        </a>
                    </span>
                ),
            }, {
                title: '申请时间',
                dataIndex: 'submit_date',
                render: (text,record) => (
                    <span>
                    {text == null?'':this.transformDate(text)}
                    </span>
                ),
            },{
                title:'审核状态',
                render: (text,record) => (
                    <span>
                    {record.varified?'已通过':(record.refused?'未通过':'审核中')}
                    </span>
                ),
            },{
                title:'操作',
                dataIndex:'_id',
                render:(text, record)=>(
                    <span>
                        <Button onClick={this.deleteConfirm.bind(this,text,record.refused,record.datasheet)} type="ghost" style={{width:90,height:30}}>取消申请</Button>
                    </span>
                )
            }];
        }else if(key == '4'){

            for(let i in this.props.new_product){
                if(this.props.new_product[i].refused==true){
                    data.push(this.props.new_product[i]);
                }
            }
            columns=[{
                title: '厂商',
                dataIndex: 'manufacturer',
            },{
                title:'产品型号',
                dataIndex:'product_id',
            },{ //href has double '/'
                title:'数据表',
                dataIndex:'datasheet',
                render: (text,record) => (
                    <span>
                        <a href={(config.file_server + text)} target="_blank" >
                            <img src={(config.theme_path + 'table.png')} alt=""/>
                        </a>
                    </span>
                ),
            }, {
                title: '申请时间',
                dataIndex: 'submit_date',
                render: (text,record) => (
                    <span>
                    {text == null?'':this.transformDate(text)}
                    </span>
                ),
            },{
                title:'审核时间',
                dataIndex:'operation_time',
                render: (text,record) => (
                    <span>
                    {text == null?'':this.transformDate(text)}
                    </span>
                ),
            },{
                title:'审核状态',
                render: (text,record) => (
                    <span>
                    {record.varified?'已通过':(record.refused?'未通过':'审核中')}
                    </span>
                ),
            },{
                title:'原因',
                dataIndex:'refused_reason',
            },{
                title:'操作',
                dataIndex:'_id',
                render:(text, record)=>(
                    <span>
                        <Button onClick={this.deleteConfirm.bind(this,text,record.refused,record.datasheet)} type="ghost" style={{width:90,height:30}}>删除</Button>
                    </span>
                )
            }];
        }
        pagination = {
            total: data.length,
            showSizeChanger: true,
            onShowSizeChange(current, pageSize) {
            console.log('Current: ', current, '; PageSize: ', pageSize);
            },
            onChange(current) {
            console.log('Current: ', current);
            },
        };
        return(
            <div className="SellerCenterUploadStock SellerCenterModelAudit">
                <Tabs onChange={this.callback.bind(this)} type="card">
                    <TabPane tab={I18n.t('modelaudit.all')} key="1">
                        <Table
                            columns={columns}
                            dataSource={data}
                            bordered
                            pagination={pagination}
                        />
                    </TabPane>
                    <TabPane tab={I18n.t('modelaudit.done')} key="2">
                        <Table
                            columns={columns}
                            dataSource={data}
                            bordered
                            pagination={pagination}
                        />
                    </TabPane>
                    <TabPane tab={I18n.t('modelaudit.doing')} key="3">
                        <Table
                            columns={columns}
                            dataSource={data}
                            bordered
                            pagination={pagination}
                        />
                    </TabPane>
                    <TabPane tab={I18n.t('modelaudit.did')} key="4">
                        <Table
                            columns={columns}
                            dataSource={data}
                            bordered
                            pagination={pagination}
                        />
                    </TabPane>
                </Tabs>
            </div>
        )
    }
}
SellerCenterModelAudit.propTypes = {
    new_product: PropTypes.array.isRequired,
};
export default createContainer(() => {
    Meteor.subscribe('new_product.part_number');

    return {
        new_product:NewProduct.find({user_id: Meteor.userId()}, {sort: {submit_date: -1}}).fetch(),
    };
}, SellerCenterModelAudit);