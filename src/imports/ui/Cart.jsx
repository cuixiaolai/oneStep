import React, { Component, PropTypes } from 'react';
import { Upload, message, Button, Icon } from 'antd';
// import { Accounts } from 'meteor/accounts-base';
// import { Meteor } from 'meteor/meteor';

export default class Cart extends Component {
    constructor(props) {
        super(props);
    }

	render() {
        //console.log(Accounts._getLoginToken());
        //console.log(Accounts._generateStampedLoginToken());
        //console.log(Meteor.connection);
        const props = {
            action: 'http://192.168.1.203:3005',
            listType: 'picture',
            onChange(info) {
                if (info.file.status !== 'uploading') {
                    console.log(info.file);
                }
                if (info.file.status === 'done') {
                    console.log(info.file.response[0]);
                    message.success(`${info.file.name} 上传成功。`);
                } else if (info.file.status === 'error') {
                    message.error(`${info.file.name} 上传失败。`);
                }
            },
        };
		return (
            <div>
                <Upload {...props}>
                    <Button type="ghost">
                        <Icon type="upload" /> 点击上传
                    </Button>
                </Upload>
            </div>
		);
	}
}
