import React, { Component } from 'react';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Buyer } from '../api/Buyer.js';
import { Translate, I18n } from 'react-redux-i18n';
import PersonalCenterBuyerinner from '../modules/center/PersonalCenterBuyerinner.jsx';

export default class PersonalCenterBuyer extends Component{
    render(){
        if (this.props.currentUserInfo != null && (this.props.currentUserInfo.authentication == null || this.props.currentUserInfo.authentication.verify == false || this.props.currentUserInfo.authentication.success == false)) {
            return(
                <div>
                    <p className="SecurityTopName" style={{marginBottom:32}}><span></span>{I18n.t('buyer.buy')}
                    </p>
                    <PersonalCenterBuyerinner flaga={false} />
                </div>
            )
        }
        else {
            return(
                <div>
                    <p className="SecurityTopName" style={{marginBottom:32}}><span></span>{I18n.t('buyer.buy')}
                    </p>
                    <PersonalCenterBuyerinner flaga={true} />
                </div>
            )
        }
    }
}

// export default createContainer(() => {
//     Meteor.subscribe('buyer');
//     return {
//         currentUserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'authentication': 1}}),
//     };
// }, PersonalCenterBuyer);