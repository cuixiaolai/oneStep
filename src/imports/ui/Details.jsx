import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router';
import config from '../config.json';
import OrderTable from '../modules/order/OrderTable.jsx';
import {  Form,Upload,Cascader,Checkbox,Button,Input } from 'antd';
import {Order} from '../api/Order.js';

export default class Details extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            num:2,
            style:2,
            nums:2,
        });
    }
    ShowName(type){
        if(['personwarranty', 'sellerwarranty'].includes(type)){
            return(
                <p>{I18n.t('return.rightsxiang')}</p>
            )
        }else if(['personrefund', 'sellerrefund'].includes(type)){
            return(
                <p>{I18n.t('return.refundxiang')}</p>
            )
        }else if(['personreturngoods', 'sellerreturngoods'].includes(type)){
            return(
                <p>{I18n.t('return.Returnrefundxiang')}</p>
            )
        }
    }
    ShowNavigation(){
        if(this.props.type == 'personwarranty'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span> >
                    <span> <Link to="/personal_center">{I18n.t('PersonalCenter.name')}</Link></span> >
                    <span> <Link to="/personal_center/order">{I18n.t('PersonalCenter.Order.myorder')}</Link></span> >
                    <span>{I18n.t('return.rightsxiang')}</span>
                </div>
            )
        }else if(this.props.type == 'personrefund'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span> >
                    <span> <Link to="/personal_center">{I18n.t('PersonalCenter.name')}</Link></span> >
                    <span> <Link to="/personal_center/order">{I18n.t('PersonalCenter.Order.myorder')}</Link></span> >
                    <span>{I18n.t('return.refundxiang')}</span>
                </div>
            )
        }else if(this.props.type == 'personreturngoods'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span> >
                    <span> <Link to="/personal_center">{I18n.t('PersonalCenter.name')}</Link></span> >
                    <span> <Link to="/seller_center/order">{I18n.t('buyercenter.order')}</Link></span> >
                    <span>退货详情</span>
                </div>
            )
        }else if(this.props.type == 'sellerwarranty'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span>
                    <span> <Link to="/seller_center">{I18n.t('PersonalCenter.name')}</Link></span>
                    <span> <Link to="/seller_center/order">{I18n.t('buyercenter.order')}</Link></span>
                    <span>>{I18n.t('return.rightsxiang')}</span>
                </div>
            )
        }else if(this.props.type == 'sellerreturnrefund'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span>
                    <span> <Link to="/seller_center">{I18n.t('PersonalCenter.name')}</Link></span>
                    <span> <Link to="/seller_center/order">{I18n.t('buyercenter.order')}</Link></span>
                    <span>{I18n.t('return.refundxiang')}</span>
                </div>
            )
        }else if(this.props.type == 'sellerreturngoods'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span> >
                    <span> <Link to="/seller_center">{I18n.t('PersonalCenter.name')}</Link></span>
                    <span> <Link to="/seller_center/order">{I18n.t('buyercenter.order')}</Link></span>
                    <span>退货详情</span>
                </div>
            )
        }
    }
    ShowMessage(order,type){
        if(['personwarranty', 'sellerwarranty'].includes(type)){
            return(
                this.Showwarranty(order)
            )
        }else if(['personrefund', 'sellerrefund'].includes(type)){
            return(
                this.Showrefund(order)
            )
        }else if(['personreturngoods', 'sellerreturngoods'].includes(type)){
            return(
                this.Showreturnrefund(order)
            )
        }
    }
    ShowWords(type){
        if(['personwarranty', 'sellerwarranty'].includes(type)){
            return(
                <span style={{color:'#ed7020'}}>发起质保</span>
            )
        }else if(['personrefund', 'sellerrefund'].includes(type)){
            return(
                <span style={{color:'#ed7020'}}>退款</span>
            )
        }else if(['personreturngoods', 'sellerreturngoods'].includes(type)){
            return(
                <span style={{color:'#ed7020'}}>退货</span>
            )
        }
    }
    Showreturnrefund(order){
        if([1,2,3].includes(order.return_goods_status)){
            return(
                <div  className="MyRefundBoxTwo">
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>{I18n.t('return.success')}</span>
                    </p>
                    <p>{I18n.t('return.wait')}</p>
                    <p>{I18n.t('return.words')}</p>
                </div>
            )
        }else if(order.return_goods_status == 4 && order.return_goods.agreed){
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>{I18n.t('return.Refundsuccess')}</span>
                    </p>
                    <p>{this.showOrderTime(order,'personreturngoods','update_time')}</p>
                    <p>{I18n.t('return.money')} <span style={{color:'#0c5aa2'}}>{I18n.t('return.amount')}{order.currency==1?'￥':'$'}{order.return_goods.amount}</span> </p>
                </div>
            )
        }else if(order.return_goods_status == 4 && !order.return_goods.agreed){
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img src={(config.theme_path + 'rederror.png')} alt=""/>
                        <span>{I18n.t('return.Refunderror')}</span>
                    </p>
                    <p>{this.showOrderTime(order,'personreturngoods','update_time')}</p>
                    <p>{I18n.t('return.money')} <span style={{color:'#0c5aa2'}}>{order.currency==1?'￥':'$'}{order.return_goods.amount}</span> </p>
                    <p>{I18n.t('BuyerBox.errorreson')} <span>{order.return_goods.refused_reason}</span> </p>
                </div>
            )
        }
    }
    Showwarranty(order){
        if([1,2,3].includes(order.quality_assurance_status)){
            return(
                <div className="LaunchWarrantyBoxOneInner">
                    <div>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>{I18n.t('qualityassurance.success')}</span>
                    </div>
                    <p style={{marginLeft:244,marginTop:16}}>{I18n.t('qualityassurance.wait')}</p>
                </div>
            )
        }else if(order.quality_assurance_status == 4) {
            return (
                <div className="LaunchWarrantyBoxOne">
                    <div className="LaunchWarrantyBoxOneInner">
                        <div>
                            <img style={{width: 28,}} src={(config.theme_path + 'watch.png')} alt=""/>
                            <span style={{display: 'inline-block'}}>{I18n.t('qualityassurance.inspectioning')}</span>
                        </div>
                        <p style={{marginLeft: 230, marginTop: 16}}>{I18n.t('qualityassurance.doing')}</p>
                    </div>
                </div>
            )
        }else if(order.quality_assurance_status == 5) {
            return (
                <div className="LaunchWarrantyBoxThree" >
                    <div className="LaunchWarrantyBoxOneInner">
                        <span className="right" style={{color:'#0c5aa2',textAlign:'right',cursor:'pointer'}}>{I18n.t('qualityassurance.View')} <img style={{transform:'rotate(-90deg)'}} src={(config.theme_path + 'down.png')} alt=""/> </span>
                        <span className="clear"></span>
                        <div>
                            <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                            <span style={{display:'inline-block'}}>{I18n.t('qualityassurance.Appraisal')}</span>
                        </div>
                        <ul>
                            <li>
                                <span>{I18n.t('qualityassurance.unit')}</span>
                                <span>{I18n.t('qualityassurance.zhong')}</span>
                            </li>
                            <li>
                                <span>{I18n.t('qualityassurance.Quality')}</span>
                                <span>￥1500.00</span>
                            </li>
                            <li className="BuyerDown">
                                <span>{I18n.t('qualityassurance.results')}</span>
                                {this.BuyerDown()}
                            </li>
                            <li>
                                <span>{I18n.t('qualityassurance.platform')}</span>
                                <span>{I18n.t('qualityassurance.false')}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            )
        }
    }
    BuyerDown(){
        return(
            <span> <Button  type="ghost">{I18n.t('download')}</Button>  <Button  type="ghost">{I18n.t('CompantInvoicsThree.look')}</Button> </span>
        )
    }
    Showrefund(order){
        if(order.return_fund_status == 1){ //退款中
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img className="left" src={(config.theme_path + 'bigwatch.png')} alt=""/>
                        <span className="left">{I18n.t('logistics.doing')}</span>
                    </p>
                    <p></p>
                    <p>{I18n.t('return.amount')}{order.currency==1?'￥':'$'}{order.return_fund.amount}</p>
                </div>
            )
        }else if (order.return_fund_status == 2 && order.return_fund.agreed){ //退款成功
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>{I18n.t('return.Refundsuccess')}</span>
                    </p>
                    <p>{I18n.t('logistics.successtime')}{this.showOrderTime(order,'personrefund','update_time')}</p>
                    <p>{I18n.t('return.amount')}{order.currency==1?'￥':'$'}{order.return_fund.amount} </p>
                    <p>{I18n.t('return.number')}</p>
                </div>
            )
        }else if (order.return_fund_status == 2 && !order.return_fund.agreed){ //退款失败
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img src={(config.theme_path + 'rederror.png')} alt=""/>
                        <span>{I18n.t('return.Refundsuccess')}</span>
                    </p>
                    <p>{I18n.t('logistics.operator')}{this.showOrderTime(order,'personrefund','update_time')}</p>
                    <p>{I18n.t('return.amount')}{order.currency==1?'￥':'$'}{order.return_fund.amount}</p>
                    <p>{I18n.t('logistics.refush')}{order.return_fund.refused_reason}</p>
                </div>
            )
        }
    }
    calculatePrice(price, quantity){
        let grad = 0;
        let flag = true;
        for (let j = 0; j < price.length && flag; j++) {
            if (quantity >= price[j].min) {
                grad = j;
            }else {
                flag = false;
            }
        }
        return price[grad].price
    }
    showOrderTime(order,type,time_type) {
        let temp = [];
        let date = null;
        if(time_type == 'createAt'){
            if (['personreturngoods', 'sellerreturngoods'].includes(type)) {
                date = order.return_goods.createAt_problem;
            } else if (['personrefund', 'sellerrefund'].includes(type)) {
                date = order.return_fund.createAt_problem;
            } else if (['personwarranty', 'sellerwarranty'].includes(type)) {
                date = order.quality_assurance.createAt_problem;
            }
        }else if(time_type == 'update_time'){
            if (['personreturngoods', 'sellerreturngoods'].includes(type)) {
                date = order.return_goods.update_time_problem;
            } else if (['personrefund', 'sellerrefund'].includes(type)) {
                date = order.return_fund.update_time_problem;
            } else if (['personwarranty', 'sellerwarranty'].includes(type)) {
                date = order.quality_assurance.update_time_problem;
            }
        }
        let date_day = '';
        let date_time = '';
        if (date != null) {
            date_day = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
            date_time = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
        }
        return date_day+' '+date_time;
    }
    showProblemOrderDetail(order,type){
        let temp = [];
        temp.push(<li>{I18n.t('logistics.type')} {this.ShowWords(type)} </li>);
        if (['personreturngoods', 'sellerreturngoods'].includes(type)) {
            temp.push(
                <li>{I18n.t('return.amount')}{order.currency==1?'￥':'$'}{order.return_goods.amount}</li>
            );
            temp.push(
                <li>{I18n.t('return.reason')}{order.return_goods.return_reason}</li>
            );
            temp.push(
                <li>{I18n.t('logistics.time')}{this.showOrderTime(order,type,'createAt')}</li>
            );
            temp.push(
                <li>{I18n.t('return.Explain')}{order.return_goods.explain}</li>
            );
        } else if (['personrefund', 'sellerrefund'].includes(type)) {
            temp.push(
                <li>{I18n.t('return.amount')}{order.currency==1?'￥':'$'}{order.return_fund.amount}</li>
            );
            temp.push(
                <li>{I18n.t('return.reason')}{order.return_fund.return_reason}</li>
            );
            temp.push(
                <li>{I18n.t('logistics.time')}{this.showOrderTime(order,type,'createAt')}</li>
            );
            temp.push(
                <li>{I18n.t('return.Explain')}{order.return_fund.explain}</li>
            );
        } else if (['personwarranty', 'sellerwarranty'].includes(type)) {
            temp.push(
                <li>{I18n.t('logistics.time')}{this.showOrderTime(order,type,'createAt')}</li>
            );
        }
        return <ul>{temp}</ul> ;
    }
    showPackage(package_arr){
        console.log(package_arr);
        let packageStr = '';
        for (let i = 0; i < package_arr.length; ++ i) {
            if (packageStr == '') {
                packageStr = package_arr[i].package;
            }
            else {
                packageStr = packageStr + ', ' + package_arr[i].package;
            }
        }
        return packageStr;
    }
    render(){
        if(this.props.orderResult != null){
            console.log(this.props.orderResult);
            let order = this.props.orderResult;
            let type = this.props.type;
            const data = [];
            const columns = [{
                title: '商品信息',
                dataIndex: 'message',
                className:'OrderTableOne',
                render(text,record) {
                    return(
                        <div>
                            <div className="left">
                                <img src={(config.theme_path + 'huo_03.jpg')} alt=""/>
                            </div>
                            <div className="left">
                                <p>{record.product_id}</p>
                                <p>{I18n.t('OrderConfirm.Manufactor')} <span>{record.manufacturer}</span> <span>{record.package_str}</span> </p>
                                <p>{record.describe}</p>
                            </div>
                            <div className="clear"></div>
                        </div>
                    )
                },
            }, {
                title: '数量',
                dataIndex: 'num',
                render: text => <span ><span style={{display:'inline-block',transform:'rotate(45deg)'}}>+</span>{text}</span>
            }, {
                title: '单价',
                dataIndex: 'price',
                render: text => <span >￥{text}/{I18n.t('ge')}</span>
            },{
                title:'总额',
                dataIndex:'total',
                render: text => <span >￥ <span style={{color:'#0c5aa2',fontSize:'18px'}}>{text}</span></span>
            }, {
                title:['personrefund', 'personreturngoods','personwarranty'].includes(type)?'商家':'买家',
                dataIndex:'business',
                render: (value, row, index) => {
                    const obj = {
                        children: value,
                        props: {},
                    };
                    if (index == 0) {
                        obj.props.rowSpan = order.product.length;
                    }else{
                        obj.props.rowSpan = 0;
                    }
                    return obj;
                }
            }];
            for(let i in order.product){
                if(['personrefund', 'sellerrefund'].includes(this.props.type) ||
                    (['personreturngoods', 'sellerreturngoods'].includes(this.props.type) && (order.return_goods_status == 0 || order.return_goods.product_choice[i])) ||
                    (['personwarranty', 'sellerwarranty'].includes(this.props.type) && (order.quality_assurance_status == 0 || order.quality_assurance.product_choice[i]))
                ) {
                    let price = this.calculatePrice(order.product[i].price, order.product[i].quantity);
                    let package_str = this.showPackage(order.product[i].package);
                    data.push({
                        key: i.toString(),
                        message: '',
                        num: order.product[i].quantity,
                        price: price,
                        total: price * order.product[i].quantity,
                        business: ['personrefund', 'personreturngoods', 'personwarranty'].includes(type) ? order.company_name : order.customer_name,
                        product_id: order.product[i].product_id,
                        manufacturer: order.product[i].manufacturer,
                        package_str: package_str,
                        describe: order.product[i].describe
                    });
                }
            }
            return(
                <div className="Details">
                    <div className="Detailsinner OrderBox">
                        {this.ShowName(this.props.type)}
                        {this.ShowNavigation()}
                        {this.ShowMessage(order,this.props.type)}
                        <OrderTable columns={columns} data={data} name={I18n.t('return.message')} />
                        <div className="DetailsinnerBoxOne">
                            <p>{I18n.t('logistics.tui')}</p>
                            <div className="DetailsinnerBoxOneInner">
                                <p>
                                    <span className="left">{I18n.t('sellerorderdetails.orderno')}{order.id}</span>
                                    {/*<span className="right">{this.showOrderTime(order,this.props.type,'createAt')}</span>*/}
                                    <div className="clear"></div>
                                </p>
                                {this.showProblemOrderDetail(order,this.props.type)}
                            </div>
                        </div>
                    </div>
                </div>
            )
        }else{
            return null;
        }
    }
}
export default createContainer(() => {
    Meteor.subscribe('order');
    let path=window.location.pathname;
    let patharr = path.split('/');
    let arr=patharr[patharr.length-1].split('_');
    let order= null;
    if(['personrefund', 'personreturngoods','personwarranty'].includes(arr[0])){
        order=Order.findOne({_id:arr[1], customer_id: Meteor.userId(), problem_order: {$in:[2,3]}});
    }else if(['sellerrefund', 'sellerreturngoods', 'sellerwarranty'].includes(arr[0])){
        order=Order.findOne({_id:arr[1], seller_id: Meteor.userId(), problem_order: {$in:[2,3]}});
    }
    let product=0;
    if(order != null && parseInt(arr[2]) < order.product.length) {
        product = parseInt(arr[2]);
    }
    return {
        orderResult: order,
        type:arr[0]
    };
}, Details);