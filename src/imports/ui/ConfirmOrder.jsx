import React, { Component } from 'react';
import { Link,browserHistory } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import OrderTable from '../modules/order/OrderTable.jsx';
import config from '../config.json';
import { Table,Cascader,Checkbox,Button ,Modal,Input,message} from 'antd';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Order } from '../api/Order.js';


export default class ConfirmOrder extends Component {
	constructor(props) {
		super(props);
		this.state = ({
			Situation:1,//1 国内到国内    2 国外到国内   3 国外到国外
			num:1,
			id:0,
			_id:'',
			company_name:'',
			visible:false,
			password:"",
			username:""
		});
	}
	showModal(){
	  this.setState({
      visible: true,
      username:this.props.orderInfo[0].customer_name,
      password:""
    });
	  document.getElementById("password").value= "";
	}

	handleCancel(){
   
    this.setState({
      visible: false,
    });
  }

    handleOk(){
          Meteor.loginWithPassword(this.state.username, this.state.password, function(err) {
          if (err) {            
                message.error('密码错误');
                    }else {
                     this.setState({
                                     visible: false,
                                        });



                           if(typeof this.props.orderInfo[0] != 'undefined'){
			                  const id = this.props.orderInfo[0].id;
			                  const _id = this.props.orderInfo[0]._id;
			                  const company_name = this.props.orderInfo[0].company_name;
			                  Meteor.call('order.confirmReceive',_id, (err) => {
			                        if (err) {
                                    
			                        } else {
			                            Meteor.call('message.createMessage', Meteor.userId(), Meteor.userId(), 4, id.toString());
			                                    	}
			                             });
			                             this.setState({ // prevent asynchronous call after component unmount
			                                num:2,
			                                _id:_id,
			                                company_name:company_name
			                                    });
		                                    }

                                }
                            }.bind(this));

  }

  onchange(){
  	console.log("change");
  	let pass = document.getElementById("password").value;
  	this.setState({
      password: pass,
    });

  }

	confirmOrder(){
		this.showModal();


	
	}
	showPackage(package_arr){
		let packageStr = '';
		for (let i = 0; i < package_arr.length; ++ i) {
			if (packageStr == '') {
				packageStr = package_arr[i].package;
			}
			else {
				packageStr = packageStr + ', ' + package_arr[i].package;
			}
		}
		return packageStr;
	}
	forwardToOperation(order_id){
		Meteor.call('order.forwardToOperation',this.state._id,'customer_comment', (err) => {
			if (err) {

			} else {
				browserHistory.replace('/evaluate');
			}
		});
	}
	render() {
		
		if(this.state.num == 1 && typeof this.props.orderInfo[0] != 'undefined'){
			console.log(this.props.orderInfo);
			const data = [];
			let count_item = 0;
			let all_price = 0;
			for(let i in this.props.orderInfo){ //need to test multiple order confirm
				for(let j in this.props.orderInfo[i].product){
					count_item ++;
					let grad = 0;
					let flag = true;
					for (let k = 0; k < this.props.orderInfo[i].product[j].price.length && flag; k++) {
						if (this.props.orderInfo[i].product[j].quantity >= this.props.orderInfo[i].product[j].price[k].min) {
							grad = k;
						}else {
							flag = false;
						}
					}
					let item = {
						key: count_item,
						num: this.props.orderInfo[i].product[j].quantity,
						price: this.props.orderInfo[i].product[j].price[grad].price,
						discount: 0,
						total: this.props.orderInfo[i].product[j].price[grad].price * this.props.orderInfo[i].product[j].quantity,
						business: this.props.orderInfo[i].company_name,
						message: [this.props.orderInfo[i].product[j].product_id,this.props.orderInfo[i].product[j].manufacturer,this.showPackage(this.props.orderInfo[i].product[j].package),this.props.orderInfo[i].product[j].describe],
					};
					all_price = all_price + item.total;
					data.push(item);
				}
			}
			let This = this;
			const columns = [{
				title: '商品信息',
				dataIndex: 'message',
				className:'OrderTableOne',
				render(text) {
					return(
						<div>
							<div className="left">
								<img src={(config.theme_path + 'huo_03.jpg')} alt=""/>
							</div>
							<div className="left">
								<p>{text[0]}</p>
								<p>{I18n.t('OrderConfirm.Manufactor')} <span>{text[1]}</span> <span>{text[2]}</span> </p>
								<p>{text[3]}</p>
							</div>
							<div className="clear"></div>
						</div>
					)
				},
			}, {
				title: '数量',
				dataIndex: 'num',
				render: text => <span ><span style={{display:'inline-block',transform:'rotate(45deg)'}}>+</span>{text}</span>
			}, {
				title: '单价',
				dataIndex: 'price',
				render: text => <span >{This.props.orderInfo[0].currency == 1 ? '￥': '$'}{text}/{I18n.t('ge')}</span>
			}, {
				title:'总优惠',
				dataIndex:'discount',
				render: text => <span >{This.props.orderInfo[0].currency == 1 ? '￥': '$'}{text}</span>
			}, {
				title:'总额',
				dataIndex:'total',
				render: text => <span >{This.props.orderInfo[0].currency == 1 ? '￥': '$'}<span style={{color:'#0c5aa2',fontSize:'18px'}}>{text}</span></span>
			}, {
				title:'商家',
				dataIndex:'business',
			}];

			let postage,packageprice,labelprice,listprice,currency,lastprice,id,address,time;
			if(this.props.orderInfo.length != 0){
				console.log(this.props.orderInfo);
				postage = this.props.orderInfo[0].postage;
				packageprice = this.props.orderInfo[0].packagePrice;
				labelprice = this.props.orderInfo[0].labelPrice;
				listprice = this.props.orderInfo[0].listPrice;
				currency = this.props.orderInfo[0].currency;
				lastprice = all_price + postage + packageprice + labelprice + listprice;
				id = this.props.orderInfo[0].id;
				address = (typeof this.props.orderInfo[0].address == 'undefined') || (this.props.orderInfo[0].address == null) ? '':this.props.orderInfo[0].address.address + this.props.orderInfo[0].address.detailAddress;
				time = this.props.orderInfo[0].createAt.getFullYear()+'-'+(this.props.orderInfo[0].createAt.getMonth()+1)+'-'+this.props.orderInfo[0].createAt.getDate();
			}
			return (
				<div className="OrderBox">
				    <Modal title="请输入登录密码进行确认"
                      visible={this.state.visible}
                      onOk={this.handleOk.bind(this)}
                      confirmLoading={this.state.confirmLoading}
                      onCancel={this.handleCancel.bind(this)}
                      >
                      <Input placeholder="请输入登录密码" onChange = {this.onchange.bind(this)}  id="password" type="password" />
                    </Modal>
					<p>{I18n.t('OrderConfirm.name')}</p>
					<div className="ConfirmOrder">
						<OrderTable columns={columns} data={data} name={I18n.t('OrderConfirm.message')} />
						<div className="ConfirmOrderBox">
							<p>{I18n.t('OrderConfirm.information')}</p>
							<div>
								<p>
									{I18n.t('OrderConfirm.ordernumber')} {id}
								</p>
								<p>
									{I18n.t('OrderConfirm.Logistics')} <span style={{color:'#0c5aa2',cursor:'pointer'}}>{I18n.t('OrderConfirm.Logisticsdetails')}</span>
								</p>
								<p>
									{I18n.t('OrderConfirm.Receiving')} {address}
								</p>
								<p>
									{I18n.t('OrderConfirm.Closing')} {time}
								</p>
							</div>
							<div>
								<p style={{color:'#d43327',fontSize:'14px'}}>{I18n.t('OrderConfirm.Prompt')}</p>
								<div className="PaymentPageFooterbox">
									<ul>
										<li>
											<span className="right">{currency == 1 ? '￥': '$'}{all_price}</span>
											<span className="right">{I18n.t('PaymentPageTable.gong')}<span style={{color:'#0c5aa2'}}>5</span>{I18n.t('PaymentPageTable.jian')} </span>
											<div className="clear"></div>
										</li>
										<li>
											<span className="right">{currency == 1 ? '￥': '$'}{postage}</span>
											<span className="right">{I18n.t('PaymentPageTable.charges')}</span>
											<div className="clear"></div>
										</li>
										<li>
											<span className="right">{currency == 1 ? '￥': '$'}{packageprice}</span>
											<span className="right">{I18n.t('PaymentPageTable.Packingcharge')}</span>
											<div className="clear"></div>
										</li>
										<li>
											<span className="right">{currency == 1 ? '￥': '$'}{labelprice}</span>
											<span className="right">{I18n.t('PaymentPageTable.Tagfee')}</span>
											<div className="clear"></div>
										</li>
										<li>
											<span className="right">{currency == 1 ? '￥': '$'}{listprice}</span>
											<span className="right">{I18n.t('PaymentPageTable.Deliverylist')}</span>
											<div className="clear"></div>
										</li>
										<li>
											<span className="right" style={{color:'#d94730'}}>{currency == 1 ? '￥': '$'}0</span>
											<span className="right">
												<span style={{marginRight:0}}>{I18n.t('PaymentPageTable.Deductible')}</span>
											</span>
											<div className="clear"></div>
										</li>
									</ul>
									<p>{I18n.t('PaymentPageTable.Amountpayable')} <span style={{fontSize:28,color:'#d94730'}}>{currency == 1 ? '￥': '$'}{lastprice}</span> </p>
								</div>
								<Button type="primary" onClick={this.confirmOrder.bind(this)} > {I18n.t('OrderConfirm.name')} </Button>
							</div>
						</div>
					</div>
				</div>
			);
		}else if(this.state.num == 2){
			console.log(this.state._id);
			return(
				<div className="OrderBox">
					<p>{I18n.t('OrderConfirm.name')}</p>
					<div className="OrderBoxcomplete">
						<div>
							<div className="left">
								<img src={(config.theme_path + 'green.png')} alt=""/>
							</div>
							<div className="left">{I18n.t('OrderConfirm.Successfultrade')}</div>
							<div className="clear"></div>
						</div>
						<p style={{paddingLeft:'30px',fontSize:'14px',color:'#999',marginTop:'6px'}}>{this.state.company_name}</p>
						<p>
							<span> {I18n.t('OrderConfirm.Congratulations')} 2 {I18n.t('ge')} </span>
							<Link style={{color:'#ed7020',marginLeft:'20px'}} to="/personal_center/integral">{I18n.t('OrderConfirm.details')}</Link>
						</p>
						<p> {I18n.t('OrderConfirm.youcan')} <Button type="ghost" onClick={this.forwardToOperation.bind(this)}>{I18n.t('OrderConfirm.evaluate')}</Button></p>
					</div>
					<div className="OrderBoxcomplete">
						<p style={{marginTop:0,marginBottom:'15px'}}>{I18n.t('OrderConfirm.need')}</p>
						<ul>
							<li>
								<div className="left">
									<img src={(config.theme_path + 'origin.png')} alt=""/>
								</div>
								<div className="left">
									<Link  to="#">{I18n.t('OrderConfirm.search')}</Link>
								</div>
								<div className="clear"></div>
							</li>
							<li>
								<div className="left">
									<img src={(config.theme_path + 'origin.png')} alt=""/>
								</div>
								<div className="left">
									<Link to="/personal_center/order">{I18n.t('OrderConfirm.orders')}</Link>
								</div>
								<div className="clear"></div>
							</li>
							<li>
								<div className="left">
									<img src={(config.theme_path + 'origin.png')} alt=""/>
								</div>
								<div className="left">
									<Link   to="/shopping_cart">{I18n.t('OrderConfirm.Shopping')}</Link>
								</div>
								<div className="clear"></div>
							</li>
						</ul>
					</div>
				</div>
			)
		}else{
			return null;
		}

	}
}
// export default createContainer(() => {
// 	Meteor.subscribe('order');
// 	return {
// 		orderInfo: Order.find({customer_id: Meteor.userId(),'operation.customer_receive':true, order_status:3, problem_order:{$in:[1,3]} }).fetch(),
// 	};
// }, ConfirmOrder);