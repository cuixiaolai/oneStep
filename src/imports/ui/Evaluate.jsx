import React, { Component } from 'react';
import { Link,browserHistory } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../config.json';
import { Checkbox,Button,message,Input } from 'antd';
import Commodityevaluation from '../modules/Evaluation/Commodityevaluation.jsx';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Order } from '../api/Order.js';


export default class Evaluate extends Component{
    constructor(state) {
        super(state);
        this.state = ({
            runtime:0,
            num:3,
            comment:[],
            anonymous: true
        });
    }
    componentDidMount(){
        if(this.props.orderResult != null){
            for(let i in this.props.orderResult.product){
                this.state.comment.push({arr:{'socle':0,'speed':0,'package':0,'service':0,'label':0}, goods:'', server:''});
            }
            this.setState({
                runtime:1
            });
        }
    }
    componentWillReceiveProps(nextProps){
        if(this.state.runtime == 0 && nextProps.orderResult != null){
            for(let i in nextProps.orderResult.product){ //arr is object
                this.state.comment.push({arr:{'socle':0,'speed':0,'package':0,'service':0,'label':0}, goods:'', server:''});
            }
            this.setState({
                runtime:1
            });
        }
    }
    ShowCommodityevaluation(product){
        let arr = [];
        for(let i=0;i<product.length;i++){
            arr[i] = (
                <Commodityevaluation lengths={product.length} name={product[i].product_id} num={product[i].quantity} keys={i} click={this.Submit.bind(this)} />
            )
        }
        return arr;
    }
    onChange(e){
        this.setState({
            anonymous: e.target.checked
        });
    }
    Submit(comments,keys){
        this.state.comment[keys] = comments;
    }
    handleSubmit(){
        console.log(this.state.comment);
        let stock_id = [];
        let image = []; //need to add upload image,now the param is [[],[],[]...]
        for(let i in this.props.orderResult.product) {
            stock_id.push(this.props.orderResult.product[i].stock_id);
            image.push([]);
        }
        Meteor.call('comment.addComment', this.props.orderResult._id, stock_id, this.state.comment, this.state.anonymous, image, this.props.orderResult.customer_name, (err) => {
                if (err) {
                    message.warning('网络不稳，请您稍后评价！');

                } else {
                    message.warning('感谢您的评价！');
                    browserHistory.replace('/personal_center/order');
                }
        });
    }
    render(){
        console.log(this.state.comment);
        if(this.props.orderResult != null){
            return(
                <div className="OrderBox">
                    <p>{I18n.t('evaluate.name')}</p>
                    <div className="EvalualeBox">
                        <div className="EvalualeBoxInner">
                            <p>
                                <div className="right">
                                    {I18n.t('evaluate.notes')}
                                </div>
                                <div className="right">
                                    <img src={(config.theme_path + 'origin3_03.png')} alt=""/>
                                </div>
                                <div className="clear"></div>
                            </p>
                            <div>
                                <div className="left">
                                    <img src={(config.theme_path + 'shop_03.jpg')} alt=""/>
                                </div>
                                <div className="left">
                                    <p>{I18n.t('Search.Manufacturer')}</p>
                                    <p>{I18n.t('evaluate.shop')}</p>
                                    <p>{I18n.t('evaluate.reputation')}4.5</p>
                                </div>
                                <div className="clear"></div>
                            </div>
                        </div>
                        <div className="EvalualeBoxMessage">
                            <p>{I18n.t('evaluate.name')}</p>
                            {this.ShowCommodityevaluation(this.props.orderResult.product)}
                            <Checkbox checked={this.state.anonymous} onChange={this.onChange.bind(this)}>{I18n.t('evaluate.anonymous')}</Checkbox>
                            <Button onClick={this.handleSubmit.bind(this)} type="primary">{I18n.t('evaluate.subnote')}</Button>
                        </div>
                    </div>
                </div>
            )
        }else {
            return(
                <div className="OrderBox"></div>
            )
        }

    }
}
// export default createContainer(() => {
//     Meteor.subscribe('order');
//     return {
//         orderResult: Order.findOne({customer_id: Meteor.userId(),'operation.customer_comment':true, order_status:4, problem_order:{$in:[1,3]} }),
//     };
// }, Evaluate);