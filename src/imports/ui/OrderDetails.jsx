import React, { Component } from 'react';
import { Select, Dropdown, DatePicker, Modal, Button ,Input ,Menu, Icon,Form ,Tabs, Col ,Checkbox ,Pagination,Cascader} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../config.json';
import { Link, browserHistory } from 'react-router';
import { Meteor } from 'meteor/meteor';;
import {createContainer} from 'meteor/react-meteor-data';
import {Order} from '../api/Order.js';
import DialogPayment from '../modules/order/DialogPayment.jsx';
import DialogReminderdelivery from '../modules/order/DialogReminderdelivery.jsx';
import DialogDelayedreceipt from '../modules/order/DialogDelayedreceipt.jsx';
/*卖家订单详情*/



export default class OrderDetails extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            runtime:0,
            remaining_time:0
        });
    }
    componentDidMount(){
        if(this.props.orderResult != null && [1, 3].includes(this.props.orderResult.order_status)){
            let interval = 0;
            if(this.props.orderResult.order_status == 1){
                interval = 7200000;
            }else if(this.props.orderResult.order_status == 3 && this.props.orderResult.delay_receive == 0){
                interval = 1209600000;  // 2 weeks
            }else if(this.props.orderResult.order_status == 3 && this.props.orderResult.delay_receive == 1){
                interval = 1641600000   // 2 weeks + 5days
            }else {
                interval = 1814400000;  // 3 weeks, auto comment, not used now
            }
            this.setState({
                remaining_time: Math.round((new Date(
                        (typeof this.props.orderResult.update_time == undefined?0:this.props.orderResult.update_time.getTime())
                        + interval) -new Date())/1000),
                runtime:1
            });
            const schedule = require("node-schedule");
            const rule = new schedule.RecurrenceRule();
            const times = [];
            for(let i=0; i<60; i++){
                times.push(i);
            }
            rule.second = times;
            this.timer = schedule.scheduleJob(rule, function(){
                this.setState({
                    remaining_time: this.state.remaining_time - 1
                });
            }.bind(this));
        }
    }
    componentWillReceiveProps(nextProps){
        if(this.state.runtime == 0 && nextProps.orderResult != null && [1, 3].includes(nextProps.orderResult.order_status)){
            let interval = 0;
            if(nextProps.orderResult.order_status == 1){
                interval = 7200000;
            }else if(nextProps.orderResult.order_status == 3 && nextProps.orderResult.delay_receive == 0){
                interval = 1209600000;  // 2 weeks
            }else if(nextProps.orderResult.order_status == 3 && nextProps.orderResult.delay_receive == 1){
                interval = 1641600000   // 2 weeks + 5days
            }else {
                interval = 1814400000;  // 3 weeks, auto comment, not used now
            }
            this.setState({
                remaining_time: Math.round((new Date(
                        (typeof nextProps.orderResult.update_time == undefined?0:nextProps.orderResult.update_time.getTime())
                        + interval) -new Date())/1000),
                runtime:1
            });
            const schedule = require("node-schedule");
            const rule = new schedule.RecurrenceRule();
            const times = [];
            for(let i=0; i<60; i++){
                times.push(i);
            }
            rule.second = times;
            this.timer = schedule.scheduleJob(rule, function(){
                this.setState({
                    remaining_time: this.state.remaining_time - 1
                });
            }.bind(this));
        }
    }
    componentWillUnmount(){
        if(typeof this.timer != 'undefined'){
            this.timer.cancel();
        }
    }
    formateSecond(second){
        let date = [86400,3600,60];
        let unit = ['天','时','分'];
        let result = '';
        for(let i in date){
            let temp = Math.floor(second/date[i]);
            if(temp != 0 || result != ''){
                result += temp + unit[i];
            }
            second -= temp*date[i]
        }
        return result + second + '秒';
    }
    //已完结
    orderFinished(_id, alreadyevaluated){
        let temp =[];
        temp[0]=(
            <div className="alreadyevaluated">
                <p><img src={(config.theme_path+'right_03.png')} alt="图片"/>{I18n.t('sellerorderdetails.alreadyevaluated')}双方已评</p>
                {alreadyevaluated?
                    '':(
                        <p className="orderalreadyevaluated">{I18n.t('sellerorderdetails.youcango')}
                            <span onClick={this.forwardToOperation.bind(this,_id,'customer_addition')}>{I18n.t('sellerorderdetails.addevaluation')}</span>
                        </p>
                    )
                }
            </div>
        )
        return temp;
    }
    //交易失败
    transactionclosed(closed_reason, update_time){
        let temp = [];
        temp[0] = (
            <div className="transactionclosed">

                <p><img src={(config.theme_path+'rederror.png')} alt="图片"/>{I18n.t('sellerorderdetails.transactionclosed')}</p>
                <p>关闭原因:{closed_reason}</p>
                <p>{typeof update_time == undefined?'':(update_time.getFullYear()+'-'+(update_time.getMonth()+1)+'-'+update_time.getDate())}</p>
            </div>
        )
        return temp;
    }
    //已评论
    alreadyevaluated(_id, alreadyevaluated){
        let temp =[];
        temp[0]=(
            <div className="alreadyevaluated">
                <p><img src={(config.theme_path+'right_03.png')} alt="图片"/>{I18n.t('sellerorderdetails.alreadyevaluated')}已评价</p>
                {/*<p>{I18n.t('sellerorderdetails.logisticsinformation')}国际快递*/}
                    {/*{I18n.t('sellerorderdetails.logisticsinformation')}123005420001*/}
                {/*</p>*/}
                {/*<p>*/}
                    {/*<span className="time">2016-09-08 12:08:33</span>    快件已发往北京永丰中转部<Link to="/logistics_details"><span className="logisticsdetails">{I18n.t('OrderConfirm.Logisticsdetails')}>></span></Link>*/}
                {/*</p>*/}
                {alreadyevaluated?
                    '':(
                        <p className="orderalreadyevaluated">{I18n.t('sellerorderdetails.youcango')}
                            <span onClick={this.forwardToOperation.bind(this,_id,'customer_addition')}>{I18n.t('sellerorderdetails.addevaluation')}</span>
                        </p>
                    )
                }
            </div>
        )
        return temp;
    }
    //收货
    goodsreceipt(_id){
        let temp =[];
        temp[0]=(
            <div className="alreadyevaluated">
                <p><img src={(config.theme_path+'right_03.png')} alt="图片"/>{I18n.t('sellerorderdetails.alreadyevaluated')}交易成功</p>
                {/*<p>{I18n.t('sellerorderdetails.logisticsinformation')}国际快递*/}
                    {/*{I18n.t('sellerorderdetails.logisticsinformation')}123005420001*/}
                {/*</p>*/}
                {/*<p>*/}
                    {/*<span className="time">2016-09-08 12:08:33</span>    快件已发往北京永丰中转部<Link to="/logistics_details"><span className="logisticsdetails">{I18n.t('OrderConfirm.Logisticsdetails')}>></span></Link>*/}
                {/*</p>*/}
                <p className="orderalreadyevaluated">{I18n.t('sellerorderdetails.nowyoucan')}
                    <span onClick={this.forwardToOperation.bind(this,_id,'customer_comment')}>{I18n.t('sellerorderdetails.evaluate')}</span>
                    {I18n.t('sellerorderdetails.or')}
                    <Link to={"/return/personreturngoods_"+_id} >{I18n.t('sellerorderdetails.returnrefund')}</Link>
                </p>
            </div>
        )
        return temp;
    }
    //已发货
    alreadyshipped(_id){
        let temp = [];
        temp[0]=(
            <div className="alreadyshipped">
                <p><img src={(config.theme_path+'watch.png')} alt="图片"/>{I18n.t('sellerorderdetails.readyshipped')}</p>
                <p>{I18n.t('sellerorderdetails.shippedcontent1')}<span className="day">{this.formateSecond(this.state.remaining_time)}</span>{I18n.t('sellerorderdetails.shippedcontent2')}</p>
                {/*<p>{I18n.t('sellerorderdetails.logisticsinformation')}国际快递*/}
                    {/*{I18n.t('sellerorderdetails.logisticsinformation')}123005420001*/}
                {/*</p>*/}
                {/*<p>*/}
                    {/*<span className="time">2016-09-08 12:08:33</span>    快件已发往北京永丰中转部<Link to="/logistics_details"><span className="logisticsdetails">{I18n.t('OrderConfirm.Logisticsdetails')}>></span></Link>*/}
                {/*</p>*/}
                <p className="orderalreadyevaluated orderalreadyshipped">{I18n.t('sellerorderdetails.nowyoucan')}
                    <span onClick={this.forwardToOperation.bind(this, _id,'customer_receive')}>{I18n.t('sellerorderdetails.confirmreceipt')}</span>
                    {I18n.t('sellerorderdetails.or')}
                    <DialogDelayedreceipt words={I18n.t('shoppingCart.dialog.extendedcontent')}
                                          content={I18n.t('shoppingCart.dialog.extendedreceipt')}
                                          order_id={_id} />
                </p>
            </div>
        )
        return temp;
    }
    //已付款
    alreadypaid(_id){
        let temp =[];
        temp[0]=(
            <div className="alreadypaid">
                <p><img src={(config.theme_path+'watch.png')} alt="图片"/>{I18n.t('sellerorderdetails.alreadypaid')}</p>
                <p >{I18n.t('sellerorderdetails.youcango')}
                    <DialogReminderdelivery words={I18n.t('shoppingCart.dialog.remindercontent')}
                                            content={I18n.t('shoppingCart.dialog.reminderdelivery')}
                                            order_id={_id} />
                    {/*<a>{I18n.t('sellerorderdetails.applyrefund')}</a>*/}
                    <Link to={"/return/personrefund_"+_id} >申请退款</Link>
                </p>
            </div>
        )
        return temp;
    }
    //已拍下
    hasphotographed(_id){
        let temp =[];
        temp[0] = (
            <div className="hasphotographed">
                <p><img src={(config.theme_path+'watch.png')} alt="图片"/>{I18n.t('sellerorderdetails.hasphotographed')}</p>
                <p>如果买家没有在{this.formateSecond(this.state.remaining_time)}内完成付款，订单将自动取消。</p>
                <p className="orderhasphotographed">
                    {I18n.t('sellerorderdetails.youcango')}
                    <span style={{cursor: 'pointer'}} onClick={this.forwardToOperation.bind(this,_id,'customer_pay')}>{I18n.t('sellerorderdetails.immediatepayment')}</span>
                    {I18n.t('sellerorderdetails.or')}
                    <DialogPayment words={I18n.t('shoppingCart.dialog.confirmcancelorder')}
                                   content={I18n.t('shoppingCart.dialog.cancelOrder')}
                                   order_id={_id} />
                </p>
            </div>
        )
        return temp;
    }
    forwardToOperation(_id,type){
        console.log(type);
        if(type == 'customer_pay'){
            Meteor.call('quote.payOrder',_id, (err) => {
                if (err) {

                } else {
                    browserHistory.replace('/payment_money');
                }
            });
        } else {
            Meteor.call('order.forwardToOperation',_id,type, (err) => {
                if (err) {

                } else {
                    if(type == 'customer_receive'){
                        browserHistory.replace('/confirm_order');
                    }else if(type == 'customer_comment'){
                        browserHistory.replace('/evaluate');
                    }else if(type == 'customer_addition'){
                        browserHistory.replace('/additional_comment');
                    }
                }
            });
        }

    }
    showTop(_id, order_status, addition_comment_flag, closed_reason, update_time){
        let temp = [];
        if(order_status == 1){
            temp[0] = (<div>
                {this.hasphotographed(_id)}
            </div>  )
        }else if(order_status == 2){
            temp[0] = (<div>
                {this.alreadypaid(_id)}
            </div>  )
        }else if(order_status == 3){
            temp[0] = (<div>
                {this.alreadyshipped(_id)}
            </div>  )
        }else if(order_status == 4){
            temp[0] = (<div>
                {this.goodsreceipt(_id)}
            </div>  )
        }else if(order_status == 5){
            temp[0] = (<div>
                {this.alreadyevaluated(_id, addition_comment_flag)}
            </div>  )
        }else if(order_status == 7){
            temp[0] = (<div>
                {this.transactionclosed(closed_reason, update_time)}
            </div>  )
        }else if(order_status == 8){
            temp[0] = (<div>
                {this.orderFinished(_id, addition_comment_flag)}
            </div>  )
        }
        return temp;
    }
    showPackage(package_arr){
        console.log(package_arr);
        let packageStr = '';
        for (let i = 0; i < package_arr.length; ++ i) {
            if (packageStr == '') {
                packageStr = package_arr[i].package;
            }
            else {
                packageStr = packageStr + ', ' + package_arr[i].package;
            }
        }
        return packageStr;
    }
    showDetails(product_id, manufacturer, package_arr, describe){
        let temp = [];
        temp[0] = (
            <div >
                <div className="productLeft">
                    <img src={config.theme_path+'commodity_2.png'} alt="图片"/>
                </div>
                <div className="productRight">
                    <h3>{product_id}</h3>
                    <p>厂家：{manufacturer}</p>
                    <p>封装：{this.showPackage(package_arr)}</p>
                    <p className="p_">{describe}</p>
                </div>
                <div className="clearFloat"></div>
            </div>)
        return temp;
    }
    calculatePrice(price, quantity){
        let grad = 0;
        let flag = true;
        for (let j = 0; j < price.length && flag; j++) {
            if (quantity >= price[j].min) {
                grad = j;
            }else {
                flag = false;
            }
        }
        return price[grad].price
    }
    commodityinformation(order){
        let arr = [];
        let total_price = 0;
        let price = [];
        for(let i in order.product){
            let product_price = this.calculatePrice(order.product[i].price, order.product[i].quantity);
            total_price += product_price*order.product[i].quantity;
            price.push(product_price);
        }

        let temp = []; //origin width 375, 100, 110, 110, 110 ,120
        temp.push(
            <tr>
                <td style={{width:395}}>{I18n.t('order.commodity')}</td>
                <td style={{width:130}}>{I18n.t('order.amount')}</td>
                <td style={{width:130}}>{I18n.t('order.unitprice')}</td>
                {/*<td style={{width:110}}>{I18n.t('order.discount')}</td>*/}
                <td style={{width:130}}>{I18n.t('order.total')}</td>
                <td style={{width:140}}>{I18n.t('order.business')}</td>
            </tr>
        )
        if(order.product.length > 1){
            for(let i in order.product){
                let product =[];
                if(i == 0){
                    product.push(<td style={{width:395}} className="orderdetails">{this.showDetails(order.product[0].product_id, order.product[0].manufacturer, order.product[0].package, order.product[0].describe)}</td>);
                    product.push(<td style={{width:130}}>×{order.product[i].quantity}</td>);
                    product.push(<td style={{width:130}}>{order.currency == 1?'￥':'$'}{price[0]}/个</td>);
                    product.push(<td style={{width:130}} rowSpan={order.product.length}>{order.currency == 1?'￥':'$'}{total_price}</td>);
                    product.push(<td style={{width:140}} rowSpan={order.product.length}>{order.company_name}</td>);
                }else {
                    product.push(<td style={{width:395}} className="orderdetails">{this.showDetails(order.product[i].product_id, order.product[i].manufacturer, order.product[i].package, order.product[i].describe)}</td>);
                    product.push(<td style={{width:130}}> ×{order.product[i].quantity}</td>);
                    product.push(<td style={{width:130}}>{order.currency == 1?'￥':'$'}{price[i]}/个</td>);
                }
                temp.push(<tr>{product}</tr>);
            }
        }else {
            temp.push(
                <tr>
                    <td style={{width:395}} className="orderdetails">{this.showDetails(order.product[0].product_id, order.product[0].manufacturer, order.product[0].package, order.product[0].describe)}</td>
                    <td > ×{order.product[0].quantity}</td>
                    <td>{order.currency == 1?'￥':'$'}{price[0]}/个</td>
                    <td>{order.currency == 1?'￥':'$'}{total_price}</td>
                    <td>{order.company_name}</td>
                </tr>
            );
        }
        return <table border='1' cellspacing='0' bordercolor="#d6d6d6" cellpadding='0' style={{borderSpacing:0}}>{temp}</table>;
    }
    orderinformation(order){
        let temp = [];
        temp[0] = (
            <div className="orderinformation">
                <p>{I18n.t('sellerorderdetails.buyerremark')}{order.remark}{'\t备注订单号:'+order.order}</p>
                <p>{I18n.t('sellerorderdetails.orderno')}{order.id}</p>
                <p>
                    {I18n.t('sellerorderdetails.receiptinformation')}
                    <span>{order.address.address}</span>
                    <span>{order.address.detailAddress}</span>
                    <span>{order.address.name}</span>
                    <span>{'\t'+order.address.phone.slice(3)}</span>
                </p>
                <p>{I18n.t('sellerorderdetails.sellerinformation')}{order.seller_info == null?'':(order.seller_info.company+'\t'+order.seller_info.address)}{order.seller_contact == null?'':('\t'+(order.seller_contact.phone ==''?'':order.seller_contact.phone.slice(3))+(order.seller_contact.telephone ==''?'':('\t'+order.seller_contact.telephone.slice(3))))}</p>
                <p>{I18n.t('sellerorderdetails.ordertime')}{typeof order.createAt == 'object'?(order.createAt.getFullYear()+'-'+(order.createAt.getMonth()+1)+'-'+order.createAt.getDate()):''}</p>
            </div>
        )
        return temp;
    }
    invoiceinformation(order){
        let invoice_detail = [];
        let invoice_title = [];
        if(order.invoice_type == 2){
            invoice_detail.push(<p>{I18n.t('sellerorderdetails.companyname')}{order.company_invoice.name}</p>);
            invoice_detail.push(<p>{I18n.t('sellerorderdetails.registeredaddress')}{order.company_invoice.address}</p>);
            invoice_detail.push(<p>{I18n.t('sellerorderdetails.contactnumber')}{order.company_invoice.tel}</p>);
            invoice_detail.push(<p>{I18n.t('sellerorderdetails.bankaccount')}{order.company_invoice.bank_name}</p>);
            invoice_detail.push(<p>{I18n.t('sellerorderdetails.accountbankaccount')}{order.company_invoice.bank_account}</p>);
            invoice_detail.push(<p>{I18n.t('sellerorderdetails.taxpayercode')}{order.company_invoice.id}</p>);
        }else {
            invoice_title.push(
                <div className="info">
                    <span>{I18n.t('sellerorderdetails.invoiceheader')}</span><span>{order.invoice_title}</span>
                </div>
            );
        }
        invoice_detail.push(<p>发票地址:{order.address.address+'\t'}{order.address.detailAddress}</p>);
        let temp = [];
        temp[0] = (
            <div className="invoiceinformation">
                <div className="info">
                    <span >{I18n.t('sellerorderdetails.typeinvoice')}</span><span>{order.invoice_type == 2?'增值税专用发票\t':(order.invoice_way == 1?('电子发票\t'+order.invoice_email):'纸质发票')}</span>
                </div>
                {invoice_title}
                <div className="info3">
                    <div className="infoLeft">
                        <p>{I18n.t('sellerorderdetails.invoicedetails')}</p>
                    </div>

                    <div className="infoRight">
                        {invoice_detail}
                    </div>
                </div>
                <div className="clearFloat"></div>
            </div>
        )
        return temp;
    }
    price(order){
        let total_price = 0;
        for(let i in order.product){
            total_price += this.calculatePrice(order.product[i].price, order.product[i].quantity)*order.product[i].quantity;
        }
        let allPrice = total_price + order.postage + order.packagePrice + order.labelPrice + order.listPrice;

        let temp = [];
        let money_type = order.currency==1?'￥':'$';
        temp[0]=(
            <div className="price">
                <div className="priceLeft">
                    <p>{I18n.t('sellerorderdetails.gong')}<span>{order.product.length}</span>{I18n.t('sellerorderdetails.price')}</p>
                    <p>{I18n.t('sellerorderdetails.postage')}</p>
                    <p>{I18n.t('sellerorderdetails.packingcharge')}</p>
                    <p>{I18n.t('sellerorderdetails.tagfee')}</p>
                    <p>{I18n.t('sellerorderdetails.listfee')}</p>
                    <p>{I18n.t('sellerorderdetails.deductible')}</p>
                    {/*<p>{I18n.t('sellerorderdetails.freightrisk')}</p>*/}
                    <p>{I18n.t('sellerorderdetails.paid')}</p>
                </div>
                <div className="priceRight">
                    <p>{money_type+total_price}</p>
                    <p>{money_type+order.postage}</p>
                    <p>{money_type+order.packagePrice}</p>
                    <p>{money_type+order.labelPrice}</p>
                    <p>{money_type+order.listPrice}</p>
                    <p>-{money_type}0</p>
                    {/*<p>￥{this.state.data[0].freightrisk}</p>*/}
                    <p>{order.currency==1?'￥':'$'}{allPrice}</p>
                </div>
                <div className="clearFloat"></div>
            </div>
        )
        return temp;
    }
    render() {
        console.log(this.props.orderResult);
        if(this.props.orderResult != null){
            return (
                <div className="sellerorderdetails">
                    <h2>{I18n.t('shoppingCart.paymentMoney.details')}</h2>
                    <div className="trend">
                        <span >{I18n.t('logisticsdetails.home')}></span>
                        <span> <Link style={{color:'#707070'}} to="/personal_center"> {I18n.t('logisticsdetails.personalcenter')}> </Link></span>
                        <span> <Link style={{color:'#707070'}} to="/personal_center/order"> {I18n.t('logisticsdetails.order')}></Link></span>
                        <span className="active">{I18n.t('logisticsdetails.details')}</span>
                    </div>
                    <div className="detailscontent">
                        {this.showTop(this.props.orderResult._id, this.props.orderResult.order_status,
                            this.props.orderResult.additional_comment,
                            typeof this.props.orderResult.closed_reason == 'undefined'?'':this.props.orderResult.closed_reason,
                            this.props.orderResult.update_time)}
                        <h3>{I18n.t('return.message')}</h3>
                        {this.commodityinformation(this.props.orderResult)}
                        <h3>{I18n.t('OrderConfirm.information')}</h3>
                        {this.orderinformation(this.props.orderResult)}
                        <h3>{I18n.t('sellerorderdetails.invoiceinformation')}</h3>
                        {this.invoiceinformation(this.props.orderResult)}
                        {this.price(this.props.orderResult)}
                    </div>

                </div>
            );
        }else{
            return null;
        }
    }
}
export default createContainer(() => {
    Meteor.subscribe('order');
    let arr = window.location.pathname.split('/');
    let order = Order.findOne({_id:arr[arr.length-1], customer_id: Meteor.userId(),order_status:{$in:[1,2,3,4,5,7,8]},problem_order:{$in:[1,3]} });
    return {
        orderResult: order,
    };
}, OrderDetails);
