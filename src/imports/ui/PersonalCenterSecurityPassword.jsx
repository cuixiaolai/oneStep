import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import SecurityTop from '../modules/center/SecurityTop.jsx'
import { Steps, Button } from 'antd';

export default class PersonalCenterSecurityPassword extends Component {
    
    render(){
        let words={
            name:I18n.t('PersonalCenterSecurity.SecurityTopPassword.name'),
            titleone:I18n.t('PersonalCenterSecurity.SecurityTopPassword.titleone'),
            titletwo:I18n.t('PersonalCenterSecurity.SecurityTopPassword.titletwo'),
            titlethree:I18n.t('PersonalCenterSecurity.SecurityTopPassword.titlethree'),
        };
        return(
            <div>
                <SecurityTop words={words}/>
            </div>
        )
    }
}

