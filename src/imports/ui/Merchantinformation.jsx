import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link,browserHistory } from 'react-router'
import config from '../config.json';
import { createContainer } from 'meteor/react-meteor-data';
import { Rate,Tabs,Input,Button,Pagination } from 'antd';
import {Stock} from '../api/Stock.js';
import {Comment} from '../api/Comment.js';

const TabPane = Tabs.TabPane;

export default class Merchantinformation extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            num:16,
            flag:true,
            show:false,
            i:0,
            current:1
        });
    }
    Open(i){
        this.setState({show:true,i:i});
    }
    Close(){
        let MerchantinformationBoxBottomInnerShow = document.getElementsByClassName('MerchantinformationBoxBottomInnerShowinput')[0];
        console.log(MerchantinformationBoxBottomInnerShow.value);
        this.setState({show:false});
    }
    changeTabs(){
        this.setState({
            current:1
        });
    }
    changePage(page){
        this.setState({
            current:page
        });
    }
    forwardToOperation(order_id){
        //client throw error,but server run this function normally
        Meteor.call('order.forwardToOperation',order_id,'seller_comment', (err) => {
            if (err) {
            } else {
                browserHistory.replace('/sellert_evaluate');
            }
        });
    }
    formatDate(date){
        let temp = '';
        if(date != undefined){
            let date_day = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
            let date_time = (date.getHours()<10?'0'+date.getHours():date.getHours())+':'+(date.getMinutes()<10?'0'+date.getMinutes():date.getMinutes())+':'+(date.getSeconds()<10?'0'+date.getSeconds():date.getSeconds());
            temp = date_day+' '+date_time;
        }
        return temp;
    }
    ShowCaozuo(order_id,flag){
        // if(this.state.flag == false){
        //     return(
        //         <span>
        //             {I18n.t('Merchantinformation.replice')}
        //         </span>
        //     )
        // }else if (this.state.flag == true){
        if(flag){
            return(
                <span onClick={this.forwardToOperation.bind(this,order_id)} style={{color:'#0c5aa2',cursor:'pointer',position:'relative'}}>
                {I18n.t('Merchantinformation.recovery')}
            </span>
            )
        }else{
            return null;
        }
        // }
    }
    ShowReply(){
        return(
            <div style={this.state.show ? {display:'block'} : {display:'none'}} className="MerchantinformationBoxBottomInnerShow">
                <p style={{textAlign:'right'}}> <span onClick={this.Close.bind(this)}>×</span> </p>
                <Input className="MerchantinformationBoxBottomInnerShowinput" type="textarea" placeholder={I18n.t('management.five')} maxLength={500} />
                <p style={{textAlign:'right'}}> <Button onClick={this.Close.bind(this)} type="primary">{I18n.t('Merchantinformation.huifu')}</Button> </p>
            </div>
        )
    }
    ShoeBuyer(comment){
        let Buyer = [];
        for(let i in comment){
            Buyer.push(
                <p>
                    <span>
                        <p>{comment[i].customer_comment_product == ''?'买家无评价':comment[i].customer_comment_product}</p>
                        <p style={{color:'#999'}}>{typeof comment[i].comment_createAt != 'undefined'?this.formatDate(comment[i].comment_createAt):''}</p>
                    </span>
                    <span>{I18n.t('sellerorderdetails.buyers')} {comment[i].customer_name}</span>
                    <span>
                        <p>{I18n.t('Merchantinformation.type')}{comment[i].product_id}</p>
                        {/*<p>{I18n.t('Merchantinformation.price')}￥25.50</p>*/}
                    </span>
                    {this.ShowCaozuo(comment[i].order_id,typeof comment[i].seller_comment_star =='undefined')}
                </p>
            )
        }
        return Buyer;
    }
    ShowToBuyer(comment){
        let Buyer = [];
        for(let i in comment){
            let stock = '';
            for(let j in comment[i].stock){
                stock += comment[i].stock[j];
                if( j!= comment[i].stock.length-1){
                    stock += ',';
                }
            }
            Buyer.push(
                <p>
                    <span>
                        <p>{comment[i].seller_comment_star}分</p>
                    </span>
                    <span>{comment[i].name} </span>
                    <span>
                        <p>{I18n.t('Merchantinformation.type')}{stock}</p>
                        {/*<p>{I18n.t('Merchantinformation.price')}￥25.50</p>*/}
                    </span>
                </p>
            )
        }
        return Buyer;
    }
    render(){
        let top = {
            speed: 0,
            socle: 0,
            package: 0,
            label: 0,
            service: 0,
            count:0
        }
        let comment = [];
        let comment_customer = [];
        if(this.props.stockInfo.length > 0){
            let stock_count = 0;
            for(let i in this.props.stockInfo){
                if(typeof this.props.stockInfo[i].comment_five_stars != 'undefined'){
                    top.speed += this.props.stockInfo[i].comment_five_stars.speed;
                    top.socle += this.props.stockInfo[i].comment_five_stars.socle;
                    top.package += this.props.stockInfo[i].comment_five_stars.package;
                    top.label += this.props.stockInfo[i].comment_five_stars.label;
                    top.service += this.props.stockInfo[i].comment_five_stars.service;
                    top.count += this.props.stockInfo[i].order_count;
                    stock_count++;
                }
            }
            top.speed = top.speed/stock_count;
            top.socle = top.socle/stock_count
            top.package = top.package/stock_count;
            top.label = top.label/stock_count;
            top.service = top.service/stock_count;

        }
        if(this.props.commentInfo.length > 0){
            comment = this.props.commentInfo ;
            let order_id = []
            for(let i in comment){
                if(typeof comment[i].seller_comment_star != 'undefined'){
                    if(!order_id.includes(comment[i].order_id)){
                        order_id.push(comment[i].order_id);
                        comment_customer.push({
                            seller_comment_star: comment[i].seller_comment_star,
                            name:Meteor.user().username, //被评价人??????????
                            stock:[comment[i].product_id]
                        })
                    }else{
                        comment_customer[order_id.indexOf(comment[i].order_id)].stock.push(comment[i].product_id);
                    }
                }
            }
        }
        return(
            <div style={{paddingLeft:'10px',width:850}}>
                <p style={{lineHeight:'16px',marginBottom:'10px'}}>
                    <span style={{fontSize:'16px'}}>{I18n.t('Merchantinformation.name')}</span>
                    <span className="right">{I18n.t('Merchantinformation.ruler')} <Link to="">{I18n.t('Merchantinformation.rulers')}</Link> </span>
                    <p className="clear"></p>
                </p>
                <div className="MerchantinformationBox">
                    <div className="left">
                        <p>
                            <span className="left">{I18n.t('management.speed')}</span>
                            <span className="left"><Rate disabled allowHalf value={Math.round(top.speed * 2)/2} /></span>
                            <span className="left">{top.speed.toFixed(2)}</span>
                            <p className="clear"></p>
                        </p>
                        <p>
                            <span className="left">{I18n.t('management.Pin')}</span>
                            <span className="left"><Rate disabled allowHalf value={Math.round(top.socle * 2)/2} /></span>
                            <span className="left">{top.socle.toFixed(2)}</span>
                            <p className="clear"></p>
                        </p>
                        <p>
                            <span className="left">{I18n.t('management.package')}</span>
                            <span className="left"><Rate disabled allowHalf value={Math.round(top.package * 2)/2} /></span>
                            <span className="left">{top.package.toFixed(2)}</span>
                            {top.count == 0?'\t'+I18n.t('Merchantinformation.nopeople'):'  '+top.count+I18n.t('Merchantinformation.ping')}
                            <p className="clear"></p>
                        </p>
                        <p>
                            <span className="left">{I18n.t('management.identification')}</span>
                            <span className="left"><Rate disabled allowHalf value={Math.round(top.label * 2)/2} /></span>
                            <span className="left">{top.label.toFixed(2)}</span>
                            <p className="clear"></p>
                        </p>
                        <p>
                            <span className="left">{I18n.t('management.service')}</span>
                            <span className="left"><Rate disabled allowHalf value={Math.round(top.service * 2)/2} /></span>
                            <span className="left">{top.service.toFixed(2)}</span>
                            <p className="clear"></p>
                        </p>
                    </div>
                    <div className="right">
                        <p>{I18n.t('Merchantinformation.buyer')}</p>
                        <p style={{fontSize:'60px',color:'#0c5aa2',marginTop:20}}>50</p>
                    </div>
                    <div className="clear"></div>
                </div>
                <div className="MerchantinformationBoxBottom">
                    <Tabs defaultActiveKey="1" onChange={this.changeTabs.bind(this)}>
                        <TabPane tab={I18n.t('Merchantinformation.frombuyer')} key="1">
                            <div className="MerchantinformationBoxBottomInner">
                                <p>
                                    <span>{I18n.t('Merchantinformation.information')}</span>
                                    <span>{I18n.t('Merchantinformation.people')}</span>
                                    <span>{I18n.t('Merchantinformation.goods')}</span>
                                    <span>{I18n.t('Merchantinformation.operation')}</span>
                                </p>
                                {this.ShoeBuyer(comment.slice((this.state.current-1)*15, this.state.current*15))}
                            </div>
                            <nav className="page" style={comment.length < 16 ? {display:'none'} : {diplay:'block'}}>
                                <Pagination onChange={this.changePage.bind(this)} showQuickJumper current={this.state.current}  pageSize={15} total={comment.length} />
                                <div className="clear"></div>
                            </nav>
                            {this.ShowReply()}
                        </TabPane>
                        <TabPane tab={I18n.t('Merchantinformation.toother')} key="2">
                            <div className="MerchantinformationBoxBottomInners">
                                <p>
                                    <span>{I18n.t('Merchantinformation.star')}</span>
                                    <span>{I18n.t('Merchantinformation.woman')}</span>
                                    <span>{I18n.t('Merchantinformation.information')}</span>
                                </p>
                                {this.ShowToBuyer(comment_customer.slice((this.state.current-1)*15, this.state.current*15))}
                            </div>
                            <nav className="page" style={comment_customer.length < 16 ? {display:'none'} : {diplay:'block'}}>
                                <Pagination showQuickJumper onChange={this.changePage.bind(this)} current={this.state.current}  pageSize={15} total={comment_customer.length} />
                                <div className="clear"></div>
                            </nav>
                        </TabPane>
                    </Tabs>
                </div>
            </div>
        )
    }
}
export default createContainer(() => {
    Meteor.subscribe('stock');
    Meteor.subscribe('comment');
    const stock = Stock.find({user_id: Meteor.userId()}).fetch();
    const comment = [];
    let comment_count = 0;
    if(stock.length > 0){
        for(let i in stock){
            if(stock[i].comment.length > 0){
                for(let j in stock[i].comment){
                    let comment_content = Comment.findOne({_id:stock[i].comment[j]});
                    if(comment_content != null){
                        comment.push(comment_content);
                        comment[comment_count].product_id=stock[i].product_id;
                        comment_count++;
                    }
                }
            }
        }
    }
    return {
        stockInfo: stock,
        commentInfo: comment.sort((a,b)=>b.comment_createAt.getTime() - a.comment_createAt.getTime())
    };
}, Merchantinformation);