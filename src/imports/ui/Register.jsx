import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import Topbox from '../modules/TopBox.jsx'
import RegisterBox from '../modules/register/RegisterBox.jsx';
import config from '../config.json';

export default class Register extends Component {
	render() {
		let perregister=(config.theme_path + '/perRegister.png');
		let comregister=(config.theme_path + '/comRegister.png');
		let perjian=(config.theme_path + '/perjian.png');
		let comjian=(config.theme_path + '/comjian.png');
		let perP=I18n.t('Register.perP');
		let comP=I18n.t('Register.comP');
		let comPs=I18n.t('Register.comPs');
		return (
			<div>
				<div className="Register">
					<div className="registerBox">
						<RegisterBox pic={perregister} link={I18n.t('Register.perregister')} style={{background:'#f6f6f6',opacity:0.1}} jian={perjian} a={'/perRegister'} button={{border:'1px solid #fff'}} color={{color:'#fff'}} words={perP} words2='' />
					</div>
					<div className="registerBox">
						<RegisterBox pic={comregister} link={I18n.t('Register.comregister')} style={{background:'#0685da',opacity:0.1}} jian={comjian} a={'/comRegister'} button={{border:'1px solid #0685da'}} color={{color:'#0685da'}} words={comP} words2={comPs} />
					</div>
					<div className="clear"></div>
				</div>
			</div>
		);
	}
}
