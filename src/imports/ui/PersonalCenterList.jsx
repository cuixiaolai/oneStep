import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router'
import config from '../config.json';
// import { createContainer } from 'meteor/react-meteor-data';

export default class PersonalCenterList extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            myorder: false,
            chnageorder: false,
            personalmessage: false,
            address: false,
            invoice: false,
            security: false,
            integral: false,
            select:false,
            index:false,
            returnnote:false,
            name: '',
            com: '',
            topname:'',
        });
    }
    invoice() {
       //  console.log(this.props.currentUser);
       //  if (Meteor.userId() != null && this.props.currentUser != null) {
       //      if (Roles.userIsInRole(Meteor.userId(), 'company')) {
       //          return (
       //              <li style={this.state.invoice ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
       //                  <Link to="/personal_center/invoics">
       //                      <div className="left">
       //                          <img src={(config.theme_path + '/seven.png')} alt=""/>
       //                      </div>
       //                      <span className="left">
							// 	{I18n.t('PersonalCenter.Message.invoice')}
							// </span>
       //                      <div className="clear"></div>
       //                  </Link>
       //              </li>
       //          )
       //      }
       //      else if (Roles.userIsInRole(Meteor.userId(), 'person')) {

       //      }
       //  }
    }
    buyer(){
        // console.log(this.props.currentUser);
        // if (Meteor.userId() != null && this.props.currentUser != null) {
        //     if (Roles.userIsInRole(Meteor.userId(), 'company')) {
        //         return (
        //             <ul>
        //                 <li>
        //                     <img src={(config.theme_path + '/0685da.png')} alt=""/>
        //                     <span>{I18n.t('buyer.name')}</span>
        //                 </li>
        //                 <li style={this.state.select ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
        //                     <Link to="/personal_center/buyer">
        //                         <div className="left">
        //                             <img src={(config.theme_path + '/buyer.png')} alt=""/>
        //                         </div>
        //                         <span className="left">{I18n.t('buyer.buy')}</span>
        //                         <div className="clear"></div>
        //                     </Link>
        //                 </li>
        //             </ul>
        //         )
        //     }
        //     else if (Roles.userIsInRole(Meteor.userId(), 'person')) {

        //     }
        // }
    }
    render() {
        let route = window.location.pathname;
        if (route == '/personal_center/info') {
            this.state.personalmessage = true;
        } else {
            this.state.personalmessage = false;
        }
        if (route == '/personal_center/order') {
            this.state.myorder = true;
        } else {
            this.state.myorder = false;
        }
        if (route == '/personal_center/return_note') {
            this.state.returnnote = true;
        } else {
            this.state.returnnote = false;
        }
        if (route == '/personal_center/address') {
            this.state.address = true;
        } else {
            this.state.address = false;
        }
        if (route == '/personal_center/index'||route=='/personal_center') {
            this.state.index = true;
        } else {
            this.state.index = false;
        }
        if (route == '/personal_center/security' || route == '/personal_center/phone' || route == '/personal_center/new_phone' || route == '/personal_center/email'
            || route == '/personal_center/new_email' || route == '/personal_center/pay' || route == '/personal_center/password' || route == '/personal_center/authentication' || route == '/personal_center/new_authentication') {
            this.state.security = true;
        } else {
            this.state.security = false;
        }
        if (route == '/personal_center/invoics' || route == '/personal_center/new_invoics') {
            this.state.invoice = true;
        } else {
            this.state.invoice = false;
        }
        if (route == '/personal_center/integral') {
            this.state.integral = true;
        } else {
            this.state.integral = false;
        }
        if(route=='/personal_center/buyer'){
            this.state.select=true;
        }else{
            this.state.select=false;
        }
        // if (Meteor.userId() != null && this.props.currentUser != null) {
        //     if (Roles.userIsInRole(Meteor.userId(), 'company')){
        //         this.state.name = I18n.t('app.buy.com');
        //         this.state.com = I18n.t('PersonalCenter.Message.commessage');
        //         this.state.topname=I18n.t('topname')
        //     } else if (Roles.userIsInRole(Meteor.userId(), 'person')) {
                this.state.name = I18n.t('app.buy.xinxi');
                this.state.com = I18n.t('PersonalCenter.Message.personalmessage');
                this.state.topname=I18n.t('PersonalCenter.name');
            // }
        // }
        return (
            <div>
                <div className="PersonalCenterList">
                    <div className="PersonalCenterList-inner">
                        <p className="topname">
                            <span>{this.state.topname}</span>
                        </p>
                        <nav className="LeftList">
                            <ul className="LeftList-notop">
                                <li style={this.state.index ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}} >
                                    <Link to="/personal_center/index">
                                        <div className="left">
                                            <img src={(config.theme_path + '/pencenter.png')} alt=""/>
                                        </div>
                                        <span className="left">{I18n.t('PersonalCenter.Order.personalhomepage')}</span>
                                        <div className="clear"></div>
                                    </Link>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <img src={(config.theme_path + '/0685da.png')} alt=""/>
                                    <Translate value="PersonalCenter.Order.name"/>
                                </li>
                                <li style={this.state.myorder ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
                                    <Link to="/personal_center/order">
                                        <div className="left">
                                            <img src={(config.theme_path + '/one.png')} alt=""/>
                                        </div>
                                        <span className="left">{I18n.t('PersonalCenter.Order.myorder')}</span>
                                        <div className="clear"></div>
                                    </Link>
                                </li>
                                <li style={this.state.returnnote ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
                                    <Link to="/personal_center/return_note">
                                        <div className="left">
                                            <img src={(config.theme_path + '/two.png')} alt=""/>
                                        </div>
                                        <span className="left">{I18n.t('PersonalCenter.Order.chnageorder')}</span>
                                        <div className="clear"></div>
                                    </Link>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <img src={(config.theme_path + '/0685da.png')} alt=""/>
                                    <span>{this.state.name}</span>
                                </li>
                                <li style={this.state.personalmessage ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
                                    <Link to="/personal_center/info">
                                        <div className="left">
                                            <img src={(config.theme_path + '/three.png')} alt=""/>
                                        </div>
                                        <span className="left">{this.state.com}</span>
                                        <div className="clear"></div>
                                    </Link>
                                </li>
                                <li style={this.state.address ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
                                    <Link to="/personal_center/address">
                                        <div className="left">
                                            <img src={(config.theme_path + '/four.png')} alt=""/>
                                        </div>
                                        <span className="left">
                                        {I18n.t('PersonalCenter.Message.address')}
                                    </span>
                                        <div className="clear"></div>
                                    </Link>
                                </li>
                                {this.invoice()}
                                <li style={this.state.security ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
                                    <Link to="/personal_center/security">
                                        <div className="left">
                                            <img src={(config.theme_path + '/five.png')} alt=""/>
                                        </div>
                                        <span className="left">
                                        {I18n.t('PersonalCenter.Message.security')}
                                    </span>
                                        <div className="clear"></div>
                                    </Link>
                                </li>










                            </ul>
                            {this.buyer()}
                        </nav>
                        <div className="PersonalCenterList-children">
                            {this.props.children}
                        </div>
                        <div className="clear"></div>
                    </div>
                </div>
            </div>
        );
    }
}



                                // <li style={this.state.integral ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
                                //     <Link to="/personal_center/integral">
                                //         <div className="left">
                                //             <img src={(config.theme_path + '/six.png')} alt=""/>
                                //         </div>
                                //         <span className="left">
                                //         {I18n.t('PersonalCenter.Message.integral')}
                                //     </span>
                                //         <div className="clear"></div>
                                //     </Link>
                                // </li>
// export default createContainer(() => {
// 	return {
// 		currentUser: Meteor.user(),
// 	};
// }, PersonalCenterList);
