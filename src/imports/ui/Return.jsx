import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link,browserHistory } from 'react-router';
import OrderTable from '../modules/order/OrderTable.jsx';
import config from '../config.json';
import { Breadcrumb,message } from 'antd';
import Agreement from './Agreement.jsx';
// import {createContainer} from 'meteor/react-meteor-data';
// import {Order} from '../api/Order.js';

import MyRefund from '../modules/return/MyRefund.jsx';
import Returnrefund from '../modules/return/Returnrefund.jsx';
import LaunchWarranty from '../modules/return/LaunchWarranty.jsx';

export default class Return extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            num:0,
            style:2,
            flag:false,
            modified:false,
            product_choice:[],
        });
    }
    OnClick(i){
        this.setState({num:i});
        let ReturngoodsBox = document.getElementsByClassName('ReturngoodsBox');
        let Img = ReturngoodsBox[i].getElementsByTagName('img');
        for(let j=0;j<ReturngoodsBox.length;j++){
            ReturngoodsBox[j].children[0].style.display='inline-block';
            ReturngoodsBox[j].children[1].style.display='none';
            ReturngoodsBox[j].children[2].style.color='#707070';
        }
    }
    modifyApply(){
        this.setState({modified:true});
    }
    ShowMessage(order){
        if (this.props.type == 'personrefund'){
            return (
                <MyRefund card="buyer"
                          current={this.state.modified?0: order.return_fund_status}
                          handleBuyerReturnFund={this.handleBuyerReturnFund.bind(this)}
                          order={order}
                          modifyApply={this.modifyApply.bind(this)}
                />
            )
        }else if(this.props.type == 'personreturngoods'){
            return(
                <Returnrefund  card="buyer"
                               current={this.state.modified?0:order.return_goods_status}
                               handleBuyerReturnGoods={this.handleBuyerReturnGoods.bind(this)}
                               handleBuyerDeliverReturnGoods={this.handleBuyerDeliverReturnGoods.bind(this)}
                               order={order}
                               modifyApply={this.modifyApply.bind(this)}
                               handleBuyerCancelReturnGoods={this.handleBuyerCancelReturnGoods.bind(this)}
                />
            )
        }else if(this.props.type == 'personwarranty'){
            return(
                <LaunchWarranty card="buyer"
                                current={order.quality_assurance_status}
                                order={order}
                                handleBuyerQualityAssurance={this.handleBuyerQualityAssurance.bind(this)}
                                handleBuyerDeliveryQualityAssurance={this.handleBuyerDeliveryQualityAssurance.bind(this)}
                />
            )
        }else if(this.props.type == 'sellerwarranty'){
            return(
                <LaunchWarranty  card="seller"
                                 current={order.quality_assurance_status}
                                 order={order}
                />
            )
        }else if(this.props.type == 'sellerreturngoods'){
            return(
                <Returnrefund  card="seller"
                               current={order.return_goods_status}
                               handleSellerReturnGoods={this.handleSellerReturnGoods.bind(this)}
                               order={order}
                />
            )
        }else if(this.props.type == 'sellerrefund'){
            return(
                <MyRefund card="seller"
                          current={order.return_fund_status}
                          handleSellerReturnFund={this.handleSellerReturnFund.bind(this)}
                          order={order}
                />
            )
        }
    }
    ShowCard(){
        let path=window.location.pathname;
        let arr=path.split('/');
        if(arr[0] == 'personrefund'){
            return(
                <div>
                    <div className="left ReturngoodsBox" style={this.state.style == 0 ? {display:'none'} : {display:'block'}} >
                        <img src={(config.theme_path + 'border_03.png')} alt=""/>
                        <img src={(config.theme_path + 'blueborder_03.png')} alt=""/>
                        <p>{I18n.t('return.Returnrefund')}</p>
                    <div className="left ReturngoodsBox" style={this.state.style == 1 ? {display:'none'} : {display:'block'}} >
                        <img src={(config.theme_path + 'border_03.png')} alt=""/>
                        <img src={(config.theme_path + 'blueborder_03.png')} alt=""/>
                        <p>{I18n.t('return.refund')}</p>
                    </div>
                    </div>
                </div>
            )
        }else if(arr[0] == 'personwarranty'){
            return(
                <div className="left ReturngoodsBox">
                    <img src={(config.theme_path + 'border_03.png')} alt=""/>
                    <img src={(config.theme_path + 'blueborder_03.png')} alt=""/>
                    <p>{I18n.t('return.rights')}</p>
                </div>
            )
        }
    }
    ShowBuyer(){
        let path=window.location.pathname;
        let arr=path.split('/');
        let newarr = arr[2].split('_');
        if(arr[2] == 'buyerwarranty'){
            return(
                <div className="BuyerBox">
                    {I18n.t('BuyerBox.warranty')}
                </div>
            )
        }else if(arr[2] == 'buyerreturnrefund'){
            return(
                <div className="BuyerBox">
                    {I18n.t('BuyerBox.tui')}
                </div>
            )
        }else if(arr[2] == 'buyerrefund'){
            return(
                <div className="BuyerBox">
                    {I18n.t('BuyerBox.tui')}
                </div>
            )
        }
    }
    ShowNavigation(){
        if(this.props.type == 'personwarranty'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span>
                    <span> > <Link to="/personal_center">{I18n.t('PersonalCenter.name')}</Link></span>
                    <span> > <Link to="/personal_center/order">{I18n.t('PersonalCenter.Order.myorder')}</Link></span>
                    <span> > {I18n.t('return.rights')}</span>
                </div>
            )
        }else if(this.props.type == 'personrefund'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span>
                    <span> > <Link to="/personal_center">{I18n.t('PersonalCenter.name')}</Link></span>
                    <span> > <Link to="/personal_center/order">{I18n.t('PersonalCenter.Order.myorder')}</Link></span>
                    <span> > {I18n.t('return.refund')}</span>
                </div>
            )
        }else if(this.props.type == 'personreturngoods'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span>
                    <span> > <Link to="/personal_center">{I18n.t('PersonalCenter.name')}</Link></span>
                    <span> > <Link to="/personal_center/order">{I18n.t('PersonalCenter.Order.myorder')}</Link></span>
                    <span> > {I18n.t('return.name')}</span>
                </div>
            )
        }else if(this.props.type == 'sellerwarranty'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span>
                    <span> > <Link to="/seller_center">{I18n.t('PersonalCenter.name')}</Link></span>
                    <span> > <Link to="/seller_center/order">{I18n.t('buyercenter.order')}</Link></span>
                    <span> > {I18n.t('return.rights')}</span>
                </div>
            )
        }else if(this.props.type == 'sellerreturnrefund'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span>
                    <span> > <Link to="/seller_center">{I18n.t('PersonalCenter.name')}</Link></span>
                    <span> > <Link to="/seller_center/order">{I18n.t('buyercenter.order')}</Link></span>
                    <span> > {I18n.t('return.refund')}</span>
                </div>
            )
        }else if(this.props.type == 'sellerreturngoods'){
            return(
                <div className="navitation">
                    <span>{I18n.t('homes')}</span> >
                    <span> > <Link to="/seller_center">{I18n.t('PersonalCenter.name')}</Link></span>
                    <span> > <Link to="/seller_center/order">{I18n.t('buyercenter.order')}</Link></span>
                    <span> > {I18n.t('return.name')}</span>
                </div>
            )
        }
    }
    ShowName(){
        if(this.props.type == 'personwarranty' || this.props.type == 'sellerwarranty'){
            return(
                <p>{I18n.t('return.rights')}</p>
            )
        }else if(this.props.type == 'personrefund'){
            return(
                <p>{I18n.t('return.refund')}</p>
            )
        }else if(this.props.type == 'personreturngoods'){
            return(
                <p>{I18n.t('return.name')}</p>
            )
        }else if(this.props.type == 'sellerrefund'){
            return(
                <p>{I18n.t('return.refund')}</p>
            )
        }else if(this.props.type == 'sellerreturngoods'){
            return(
                <p>{I18n.t('return.name')}</p>
            )
        }
    }
    Click(){
        this.setState({flag:!this.state.flag});
    }
    calculatePrice(price, quantity){
        let grad = 0;
        let flag = true;
        for (let j = 0; j < price.length && flag; j++) {
            if (quantity >= price[j].min) {
                grad = j;
            }else {
                flag = false;
            }
        }
        return price[grad].price
    }
    handleBuyerDeliveryQualityAssurance(delivery_company,delivery_number){
        Meteor.call('order.buyerDeliveryQualityAssurance', this.props.orderResult._id, delivery_company,parseInt(delivery_number), (err) => {
            if (err) {

            } else {
                //message function should not set here
                Meteor.call('message.createMessage', '', this.props.orderResult.customer_id, 13, this.props.orderResult.id.toString(), (err) => {
                    if (err) {

                    } else {

                    }
                });
                //message function should not set here
                Meteor.call('message.createMessage', '', this.props.orderResult.seller_id, 12, this.props.orderResult.id.toString(), (err) => {
                    if (err) {

                    } else {

                    }
                });
                console.log(delivery_company,delivery_number);
            }
        });
    }
    handleBuyerQualityAssurance(quality_assurance_agency){
        if(this.state.product_choice.length != 0 && this.state.product_choice.includes(true)){
            Meteor.call('order.buyerApplyQualityAssurance', this.props.orderResult._id, this.state.product_choice,quality_assurance_agency, (err) => {
                if (err) {

                } else {
                    Meteor.call('message.createMessage', '', this.props.orderResult.seller_id, 11, this.props.orderResult.id.toString(), (err) => {
                        if (err) {

                        } else {

                        }
                    });
                    console.log(quality_assurance_agency);
                }
            });
        }else{
            message.error('请选择商品');
        }

    }
    handleBuyerCancelReturnGoods(){
        Meteor.call('order.buyerCancelReturnGoods', this.props.orderResult._id, (err) => {
            if (err) {

            } else {
                browserHistory.replace('/personal_center/order');
                console.log('success');
            }
        });
    }
    handleBuyerReturnGoods(return_reason,amount,explain){
        if(this.state.product_choice.length != 0 && this.state.product_choice.includes(true)){
            Meteor.call('order.buyerApplyReturnGoods', this.props.orderResult._id, this.state.product_choice, return_reason,parseInt(amount),explain, (err) => {
                if (err) {

                } else {
                    this.setState({modified: false});
                    console.log(return_reason,amount,explain);
                    Meteor.call('message.createMessage', '', this.props.orderResult.seller_id, 8, this.props.orderResult.id.toString(), (err) => {
                        if (err) {

                        } else {

                        }
                    });
                }
            });
        }else{
            message.error('请选择商品');
        }
    }
    handleSellerReturnGoods(agreed,refused_reason,addition){
        let order_problem_status = this.props.orderResult.return_goods_status;
        Meteor.call('order.sellerVerifyReturnGoods', this.props.orderResult._id, agreed,refused_reason,addition, (err) => {
            if (err) {

            } else {
                if(!agreed){
                    Meteor.call('message.createMessage', '', this.props.orderResult.customer_id, 9, this.props.orderResult.id.toString(), (err) => {
                        if (err) {

                        } else {

                        }
                    });
                }else if(order_problem_status == 3){
                    Meteor.call('message.createMessage', '', this.props.orderResult.customer_id, 10, this.props.orderResult.id.toString(), (err) => {
                        if (err) {

                        } else {

                        }
                    });
                }
                console.log(agreed,refused_reason,addition);
            }
        });
    }
    handleBuyerDeliverReturnGoods(delivery_company,delivery_number){
        Meteor.call('order.buyerDeliverReturnGoods', this.props.orderResult._id, delivery_company,parseInt(delivery_number), (err) => {
            if (err) {

            } else {
                console.log(delivery_company,delivery_number);
            }
        });
    }
    handleBuyerReturnFund(amount,return_reason,explain){
        Meteor.call('order.buyerApplyReturnFund', this.props.orderResult._id, parseInt(amount), return_reason, explain, (err) => {
            if (err) {

            } else {
                this.setState({modified: false});
                console.log(amount,return_reason,explain);
                Meteor.call('message.createMessage', '', this.props.orderResult.seller_id, 5, this.props.orderResult.id.toString(), (err) => {
                    if (err) {

                    } else {

                    }
                });
            }
        });
    }
    handleSellerReturnFund(agreed,refused_reason,addition){
        Meteor.call('order.sellerVerifyReturnFund', this.props.orderResult._id, agreed,refused_reason,addition, (err) => {
            if (err) {

            } else {
                if(agreed){
                    Meteor.call('message.createMessage', '', this.props.orderResult.customer_id, 7, this.props.orderResult.id.toString(), (err) => {
                        if (err) {

                        } else {

                        }
                    });
                }else{
                    Meteor.call('message.createMessage', '', this.props.orderResult.customer_id, 6, this.props.orderResult.id.toString(), (err) => {
                        if (err) {

                        } else {

                        }
                    });
                }
                console.log(agreed,refused_reason,addition);
            }
        });
    }
    showPackage(package_arr){
        let packageStr = '';
        for (let i = 0; i < package_arr.length; ++ i) {
            if (packageStr == '') {
                packageStr = package_arr[i].package;
            }
            else {
                packageStr = packageStr + ', ' + package_arr[i].package;
            }
        }
        return packageStr;
    }
    render(){
        console.log(this.props.orderResult);
        if(this.props.orderResult != null ){
            let order=this.props.orderResult;
            const data = [];
            const columns = [{
                title: '商品信息',
                dataIndex: 'message',
                className:'OrderTableOne',
                render(text,record) {
                    return(
                        <div>
                            <div className="left">
                                <img src={(config.theme_path + 'huo_03.jpg')} alt=""/>
                            </div>
                            <div className="left">
                                <p>{record.product_id}</p>
                                <p>{I18n.t('OrderConfirm.Manufactor')} <span>{record.manufacturer}</span> <span>{record.package_str}</span> </p>
                                <p>{record.describe}</p>
                            </div>
                            <div className="clear"></div>
                        </div>
                    )
                },
            }, {
                title: '数量',
                dataIndex: 'num',
                render: text => <span ><span style={{display:'inline-block',transform:'rotate(45deg)'}}>+</span>{text}</span>
            }, {
                title: '单价',
                dataIndex: 'price',
                render: text => <span >{order.currency == 1?'￥':'$'}{text}/{I18n.t('ge')}</span>
            },  {
                title:'总额',
                dataIndex:'total',
                render: text => <span >{order.currency == 1?'￥':'$'} <span style={{color:'#0c5aa2',fontSize:'18px'}}>{text}</span></span>
            }, {
                title:'商家',
                dataIndex:'business',
                render: (value, row, index) => {
                    const obj = {
                        children: value,
                        props: {},
                    };
                    if (index == 0) {
                        obj.props.rowSpan = order.product.length;
                    }else{
                        obj.props.rowSpan = 0;
                    }
                    return obj;
                }
            },{
                title:'物流信息',
                dataIndex:'logistics',
                render: (value, row, index) => {
                    const obj = {
                        children: value,
                        props: {},
                    };
                    if (index == 0) {
                        obj.props.rowSpan = order.product.length;
                    }else{
                        obj.props.rowSpan = 0;
                    }
                    return obj;
                }
            }];
            for(let i in order.product){
                if(['personrefund', 'sellerrefund'].includes(this.props.type) ||
                    (['personreturngoods', 'sellerreturngoods'].includes(this.props.type) && (order.return_goods_status == 0 || order.return_goods.product_choice[i])) ||
                    (['personwarranty', 'sellerwarranty'].includes(this.props.type) && (order.quality_assurance_status == 0 || order.quality_assurance.product_choice[i]))
                ){//may need to change
                    let product_price=this.calculatePrice(order.product[i].price, order.product[i].quantity);
                    let package_str=this.showPackage(order.product[i].package);
                    data.push({
                        key: i.toString(),
                        message: '',
                        num: order.product[i].quantity,
                        price: product_price,
                        logistics:'物流信息',
                        total:product_price*order.product[i].quantity,
                        business:order.company_name,
                        product_id:order.product[i].product_id,
                        manufacturer:order.product[i].manufacturer,
                        package_str:package_str,
                        describe:order.product[i].describe
                    });
                }
            }
            let rowSelection = null;
            if((['personreturngoods', 'sellerreturngoods'].includes(this.props.type) && order.return_goods_status == 0)
                ||(['personwarranty', 'sellerwarranty'].includes(this.props.type) && order.quality_assurance_status == 0)
            ){
                rowSelection = {
                    onSelect: (record, selected, selectedRows) => {
                        let product_choice = [];
                        for(let i in order.product){
                            if(typeof this.state.product_choice[i] == 'undefined'){
                                product_choice.push(false);
                            }else{
                                product_choice.push(this.state.product_choice[i]);
                            }
                        }
                        product_choice[record.key] = selected;
                        this.setState({
                            product_choice:product_choice
                        });
                    },
                    onSelectAll: (selected, selectedRows, changeRows) => {
                        for(let i in order.product){
                            this.state.product_choice[i] = selected;
                        }
                        this.setState({
                            product_choice:this.state.product_choice
                        });
                    }
                };
            }

            console.log(this.state.product_choice);
            return(
                <div className="OrderBox">
                    {this.ShowName()}
                    {this.ShowNavigation()}
                    <div className="ReturnInner">
                        <OrderTable rowSelection={rowSelection} columns={columns} data={data} name={I18n.t('return.message')}   />
                        {this.ShowBuyer()}
                        <div className="Returngoods">
                            {this.ShowCard()}
                            <span className="right">
                                {I18n.t('return.dispute')}
                                <span style={{cursor:'pointer',color:'#0c5aa2'}} onClick={this.Click.bind(this)}>{I18n.t('return.ruler')}</span>
                            </span>
                            <p className="clear"></p>
                            <Agreement path="trade_dispute" flag={this.state.flag} click={this.Click.bind(this)} />
                        </div>
                        {this.ShowMessage(this.props.orderResult)}
                    </div>
                </div>
            )
        }else {
                return null
        }
    }
}
// export default createContainer(() => {
//     Meteor.subscribe('order');
//     let path=window.location.pathname;
//     let patharr = path.split('/');
//     let arr=patharr[patharr.length-1].split('_');
//     let order=null;
//     if(['personrefund','personreturngoods','personwarranty'].includes(arr[0])) {
//         order = Order.findOne({_id:arr[1], customer_id: Meteor.userId()});
//     }else if(['sellerwarranty','sellerreturngoods','sellerrefund'].includes(arr[0])) {
//         order = Order.findOne({_id: arr[1], seller_id: Meteor.userId()});
//     }
//     return {
//         orderResult: order,
//         type:arr[0],
//     };
// }, Return);