import React, { Component } from 'react';
import { Link } from 'react-router';
import { Tabs, Col } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import OrderAll from '../modules/order/OrderAll.jsx';
// import {createContainer} from 'meteor/react-meteor-data';
// import {Order} from '../api/Order.js';

{/*订单*/}
const TabPane = Tabs.TabPane;

export default class PersonalCenterOrder extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            key: 0,
            page: 1,
            searchWord:'',  //搜索框关键字
            dropdownkey:'0',
            runtime:0,
            downflag: false,
            dropdownLable:I18n.t('returnnote.lastweek'), //下拉菜单显示
            current_page: 1,
        });
    }
    changePage(page){
        this.setState({
            current_page: page,
            runtime:0,
        });
    }
    setRuntime(){
        this.setState({
            runtime:1,
        });
    }
    searchcommodity(value){
        let result = [];
        console.log("输出搜索内容");
        console.log(value);
        this.setState({
            searchWord:value.trim(),
            runtime:0,
        });
    }
    //数据过滤
    dataFilter(datas){
        let filtereddata = [];
        let now = new Date();
        let timeselect;
        //最近一周
        if(this.state.dropdownkey == "0"){
            timeselect = now.getTime() - 7*24*3600*1000;
        }
        if(this.state.dropdownkey == "1"){
            timeselect = now.getTime() - 30*24*3600*1000;
        }
        if(this.state.dropdownkey == "2"){
            timeselect = now.getTime() - 90*24*3600*1000;
        }
        if(this.state.dropdownkey == "3"){
            timeselect = now.getTime() - 183*24*3600*1000;
        }
        for(let i=0;i<datas.length;i++){
            if(datas[i].id == this.state.searchWord||datas[i].product[0].product_id == this.state.searchWord ||this.state.searchWord ==''){
                if(datas[i].update_time.getTime() - timeselect>=0){
                    filtereddata.push(datas[i]);
                }
            }
        }
        return filtereddata;
    }
    clickDown(obj) {
        console.log("点击下拉菜单某项");
        if(obj.key == "0"){
            this.setState({
                dropdownLable:I18n.t('returnnote.lastweek'),
            });
        }
        if(obj.key == "1"){
            this.setState({
                dropdownLable:I18n.t('returnnote.lastmonth'),
            });
        }
        if(obj.key == "2"){
            this.setState({
                dropdownLable:I18n.t('returnnote.lastthreemonths'),
            });
        }
        if(obj.key == "3"){
            this.setState({
                dropdownLable:I18n.t('returnnote.recenthalfyear'),
            });
        }
        this.setState({
            dropdownkey:obj.key,
        });
        if (this.state.downflag == false) {
            this.setState({downflag: true});
        } else {
            this.setState({downflag: false});
        }
        this.setState({
            runtime:0,
        });
    }
    changeOrderStatus(key){
        this.setState({
            key: parseInt(key),
            page: 1
        });
    }
    render() {
        let default_key = '0';
        let path = window.location.search;
        let path_index = path.indexOf('key=');
        if(path_index != -1){
            default_key = path.substring(path_index + 4, path.length);
        }
        let filter_data = this.dataFilter(this.props.orderResult);
        let data = filter_data.slice((this.state.current_page-1)*10, this.state.current_page*10);
        if (data != null) {
            return (
                <div className="sellerCenterOrder">
                    <Tabs defaultActiveKey={default_key} onChange={this.changeOrderStatus.bind(this)} >
                        <TabPane tab={I18n.t('order.all')} key="0" >
                            {/*全部*/}
                            <OrderAll data={data} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length}/>
                        </TabPane>
                        <TabPane tab={I18n.t('order.obligation')} key="1"  >
                            {/*待付款*/}
                            <OrderAll data={data.filter(item=>item.order_status==1)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length} />
                        </TabPane>
                        <TabPane tab={I18n.t('order.waitorder')} key="2">
                            {/*待发货*/}
                            <OrderAll data={data.filter(item=>item.order_status==2)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length} />
                        </TabPane>
                        <TabPane tab={I18n.t('order.waitgoods')} key="3">
                            {/*待收货*/}
                            <OrderAll data={data.filter(item=>item.order_status==3)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length} />
                        </TabPane>
                        <TabPane tab={I18n.t('order.waitevaluate')} key="4">
                            {/*待评价*/}
                            <OrderAll data={data.filter(item=>item.order_status==4)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length} />
                        </TabPane>
                    </Tabs>
                </div>
            )
        }
    }
}
// export default createContainer(() => {
//     Meteor.subscribe('order');
//     return {
//         orderResult: Order.find({customer_id: Meteor.userId(), problem_order: {$in:[1,3]}, order_status:{$in:[1,2,3,4,5,7,8]}}, {sort: {update_time: -1}}).fetch(),
//     };
// }, PersonalCenterOrder);