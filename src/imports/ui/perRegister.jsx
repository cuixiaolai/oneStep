import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import Topbox from '../modules/TopBox.jsx'
import PerregisterBox from '../modules/register/PerregisterBox.jsx';
import EmailVerifyFail from '../modules/register/EmailVerifyFail.jsx';
import Userregistration from '../modules/Agreement/Userregistration.jsx';
import { Translate, I18n } from 'react-redux-i18n';
export default class perRegister extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            current: 0,
            email: '',
        });
    }
    componentWillMount() {
        let route = window.location.pathname;
        if (route == '/perRegister') {
            this.setState({current: 0});
        }
        else {
            let arr = route.split('/');
            let token = arr[arr.length-1];
            this.setState({current: 1});
            // Meteor.call('accounts.CheckVerificationEmail', token, function(err, result) {
            //     if(err) {
                    // this.setState({current: 2});
            //     }
            //     else {
            //         this.setState({current: 1, email: result});
            //     }
            // }.bind(this));
        }
    }

    render() {
        if (this.state.current == 0) {
            return (
                <div>
                    <Userregistration />
                    <PerregisterBox callback={false} email=''/>
                </div>
            );
        }
        else if (this.state.current == 1) {
            return (
                <div>
                    <PerregisterBox callback={true} email={this.state.email}/>
                </div>
            );
        }
        else {
            return (
                <div>
                    <EmailVerifyFail />
                </div>
            );
        }
    }
}
