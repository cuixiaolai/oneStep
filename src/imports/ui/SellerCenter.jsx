import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router'
import config from '../config.json';
import { createContainer } from 'meteor/react-meteor-data';

export default class SellerCenter extends Component {
	constructor(props) {
		super(props);
		this.state = ({
			myorder: false,
			chnageorder: false,
			uploadstock: false,
			managestock:false,
			CertificationAudit:false,
			address: false,
			invoice: false,
			security: false,
			integral: false,
			select:false,
			customerinfo:false,
			ModelAudit:false,
			ManageStock:false,
			AddType:false,
			Merchant:false,
			Quality:false,
			clientinfo: false,
			name: '',
			com: '',
			topname:'',
			word:'',
			returnnote:false,
		});
	}
	render() {
		let route = window.location.pathname;

		if (route == '/seller_center/upload_stock') {//库存上传
			this.state.uploadstock = true;
			this.state.word=I18n.t('buyercenter.uploadstock');
		} else {
			this.state.uploadstock = false;
		}

		if (route == '/seller_center/manage_stock'||route == '/seller_center/manage_stock_update') {//库存管理
			this.state.ManageStock = true;
			if(route == '/seller_center/manage_stock'){
				this.state.word = I18n.t('buyercenter.managestock');
			}else{
				this.state.word = I18n.t('buyercenter.managestockupdate');
			}
		}else {
			this.state.ManageStock = false;
		}
		if(route=='/seller_center/model_audit'){//型号审核
			this.state.ModelAudit=true;
			this.state.word=I18n.t('modelaudit.name');
		}else{
			this.state.ModelAudit=false;
		}
		if(route=='/seller_center/order'){//卖家订单
			this.state.myorder=true;
			this.state.word=I18n.t('sellercenterorder.order');
		}else{
			this.state.myorder=false;
		}
		if (route == '/seller_center/customer_info' || route == '/seller_center') {//买家信息
			this.state.customerinfo = true;
			this.state.word=I18n.t('buyerinfo.name');
		} else {
			this.state.customerinfo = false;
		}
		if (route == '/seller_center/merchant') {//评分信息
			this.state.Merchant = true;
			this.state.word=I18n.t('Merchant.name');
		} else {
			this.state.Merchant = false;
		}
		if (route == '/seller_center/quality') {//质保机构
			this.state.Quality = true;
			this.state.word=I18n.t('Quality.name');
		} else {
			this.state.Quality = false;
		}
		if(route == '/seller_center/certification_audit'){
			this.state.CertificationAudit = true;
			this.state.word=I18n.t('buyerinfo.CertificationAudit');
		}else {
			this.state.CertificationAudit = false;
		}

		if (route == '/seller_center/client_info') { //客户信息
			this.state.clientinfo = true;
			this.state.word = I18n.t('buyercenter.clientinfo');
		} else {
			this.state.clientinfo = false;
		}
		if(route == '/seller_center/add_type'){
			this.state.AddType = true;
			this.state.word=I18n.t('buyerForm.Addmodel');
		}else {
			this.state.AddType = false;
		}
		return (
			<div style={{minWidth:1214}}>
				<div className="PersonalCenterList">
					<div className="PersonalCenterList-inner">
						<p className="topname">
							<span>{I18n.t('buyercenter.name')}</span>
						</p>
						<nav className="LeftList">
							<ul>
								<li>
									<img src={(config.theme_path + '/0685da.png')} alt=""/>
									<Translate value="buyercenter.customerinfo"/>
								</li>
								<li style={this.state.customerinfo ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/customer_info" >
										<div className="left">
											<img src={(config.theme_path + '/one.png')} alt=""/>
										</div>
										<span className="left">{I18n.t('buyercenter.buyerinfo')}</span>
										<div className="clear"></div>
									</Link>
								</li>
								<li style={this.state.clientinfo ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/client_info" >
										<div className="left">
											<img src={(config.theme_path + '/one.png')} alt=""/>
										</div>
										<span className="left">{I18n.t('buyercenter.clientinfo')}</span>
										<div className="clear"></div>
									</Link>
								</li>
								<li style={this.state.CertificationAudit ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/certification_audit" >
										<div className="left">
											<img src={(config.theme_path + '/one.png')} alt=""/>
										</div>
										<span className="left">{I18n.t('buyerinfo.CertificationAudit')}</span>
										<div className="clear"></div>
									</Link>
								</li>
								<li style={this.state.Merchant ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/merchant" >
										<div className="left">
											<img src={(config.theme_path + '/one.png')} alt=""/>
										</div>
										<span className="left">{I18n.t('Merchant.name')}</span>
										<div className="clear"></div>
									</Link>
								</li>
							</ul>
							<ul>
								<li>
									<img src={(config.theme_path + '/0685da.png')} alt=""/>
									<Translate value="buyercenter.managestock"/>
								</li>
								<li style={this.state.uploadstock ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/upload_stock" >
										<div className="left">
											<img src={(config.theme_path + '/one.png')} alt=""/>
										</div>
										<span className="left">{I18n.t('buyercenter.uploadstock')}</span>
										<div className="clear"></div>
									</Link>
								</li>
								<li style={this.state.ManageStock ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/manage_stock" >
										<div className="left">
											<img src={(config.theme_path + '/four.png')} alt=""/>
										</div>
										<span className="left">{I18n.t('buyercenter.managestock')}</span>
										<div className="clear"></div>
									</Link>
								</li>
								<li style={this.state.ModelAudit ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/model_audit" >
										<div className="left">
											<img src={(config.theme_path + '/one.png')} alt=""/>
										</div>
										<span className="left">{I18n.t('modelaudit.name')}</span>
										<div className="clear"></div>
									</Link>
								</li>
								<li style={this.state.AddType ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/add_type" >
										<div className="left">
											<img src={(config.theme_path + '/one.png')} alt=""/>
										</div>
										<span className="left">{I18n.t('buyerForm.Addmodel')}</span>
										<div className="clear"></div>
									</Link>
								</li>
							</ul>
							<ul>
								<li>
									<img src={(config.theme_path + '/0685da.png')} alt=""/>
									<Translate value="buyercenter.order"/>
								</li>

								<li style={this.state.myorder ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/order" >
									<div className="left">
										<img src={(config.theme_path + '/one.png')} alt=""/>
									</div>
									<span className="left">{I18n.t('PersonalCenter.Order.myorder')}</span>
									<div className="clear"></div>
									</Link>
								</li>
								<li style={this.state.returnnote ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/return_note" >
									<div className="left">
										<img src={(config.theme_path + '/two.png')} alt=""/>
									</div>
									<span className="left">{I18n.t('PersonalCenter.Order.chnageorder')}</span>
									<div className="clear"></div>
									</Link>
								</li>
							</ul>
							<ul>
								<li>
									<img src={(config.theme_path + '/0685da.png')} alt=""/>
									<Translate value="buyercenter.bill"/>
								</li>
								<li style={this.state.Quality ? {background: 'rgba(12,90,162,0.15)'} : {background: '#f4f4f4'}}>
									<Link to="/seller_center/quality" >
										<div className="left">
											<img src={(config.theme_path + '/one.png')} alt=""/>
										</div>
										<span className="left">{I18n.t('Quality.name')}</span>
										<div className="clear"></div>
									</Link>
								</li>

							</ul>
						</nav>
					</div>
				</div>
				<div className="PersonalCenterList-children">
					{(() => {
						switch (this.state.word) {
							case I18n.t('buyercenter.clientinfo'):
								return null;
							default:
							return (<p className="SecurityTopName" style={{marginBottom:32}}><span></span>{this.state.word}
								</p>);
						}
					})()}
					{this.props.children}
				</div>
				<div className="clear"></div>
			</div>
		);
	}
}
export default createContainer(() => {
	return {
	currentUser: Meteor.user(),
};
}, SellerCenter);
