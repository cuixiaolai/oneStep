import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Table,Button} from 'antd';

// import config from '../../config.json';


let Luckcui = React.createClass({
    getInitialState() {
        return {
            dataSource: [{
  key: '1',
  name: '胡彦斌',
  age: 32,
  address: '西湖区湖底公园1号'
}, {
  key: '2',
  name: '胡彦祖',
  age: 42,
  address: '西湖区湖底公园1号'
}],
            columns:[{
  title: '姓名',
  dataIndex: 'name',
  key: 'name',
}, {
  title: '年龄',
  dataIndex: 'age',
  key: 'age',
}, {
  title: '住址',
  dataIndex: 'address',
  key: 'address',
}],
        };
    },

    render(){
//   const dataSource = [{
//   key: '1',
//   name: '胡彦斌',
//   age: 32,
//   address: '西湖区湖底公园1号'
// }, {
//   key: '2',
//   name: '胡彦祖',
//   age: 42,
//   address: '西湖区湖底公园1号'
// }];

// const columns = [{
//   title: '姓名',
//   dataIndex: 'name',
//   key: 'name',
// }, {
//   title: '年龄',
//   dataIndex: 'age',
//   key: 'age',
// }, {
//   title: '住址',
//   dataIndex: 'address',
//   key: 'address',
// }];

return <div>
<Table dataSource={this.state.dataSource} columns={this.state.columns} />
<Button type="primary">Primary</Button>
</div>

    }
       
})

export default Luckcui;
