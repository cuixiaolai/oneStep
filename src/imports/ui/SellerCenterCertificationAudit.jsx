import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal } from 'antd';

const TabPane = Tabs.TabPane;

import SellerCenterCompanyRole from '../modules/SellerCenter/SellerCenterCompanyRole.jsx';
import SellerCenterDoubleIndemnity from '../modules/SellerCenter/SellerCenterDoubleIndemnity.jsx';
import SellerCenterMarginGuarantee from '../modules/SellerCenter/SellerCenterMarginGuarantee.jsx';
import SellerCenterInventoryVerification from '../modules/SellerCenter/SellerCenterInventoryVerification.jsx';
export default class SellerCenterCertificationAudit extends Component{
    callback(key) {
        console.log(key);
    }
    render(){
        return(
            <div className="SellerCenterUploadStock SellerCenterModelAudit">
                <Tabs onChange={this.callback} type="card" defaultActiveKey="1" >
                    <TabPane tab={I18n.t('CertificationAudit.topfour')} key="1">
                        <SellerCenterCompanyRole />
                    </TabPane>
                    <TabPane tab={I18n.t('CertificationAudit.topone')} key="2">
                        <SellerCenterDoubleIndemnity />
                    </TabPane>
                    <TabPane tab={I18n.t('CertificationAudit.toptwo')} key="3">
                        <SellerCenterMarginGuarantee />
                    </TabPane>
                    <TabPane tab={I18n.t('CertificationAudit.topthree')} key="4">
                        <SellerCenterInventoryVerification />
                    </TabPane>
                </Tabs>
            </div>
        )
    }
}
