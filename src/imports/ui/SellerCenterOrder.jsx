import React, { Component } from 'react';
import { Link } from 'react-router';
import { Tabs, Col } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import SellerCenterOrderAll from '../modules/sellercenterorder/SellerCenterOrderAll.jsx';
import {createContainer} from 'meteor/react-meteor-data';
import {Order} from '../api/Order.js';
{/*订单*/}
const TabPane = Tabs.TabPane;
export default class SellerCenterOrder extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            downflag:false,
            current_page:1,
            searchWord:'',  //搜索框关键字
            dropdownLable:I18n.t('returnnote.lastweek'), //下拉菜单显示
            dropdownkey:'0',
            starttime:0,
            endtime:0,
            runtime:0
        });

    }
    changePage(page){
        this.setState({
            current_page: page,
            runtime:0,
        });
    }
    setRuntime(){
        this.setState({
            runtime:1,
        });
    }
    searchcommodity(value){
        let result = [];
        console.log("输出搜索内容");
        console.log(value);
        this.setState({
            runtime:0,
            searchWord:value.trim(),
        });
    }
    dataFilter(datas){
        let filtereddata = [];
        let now = new Date();
        let timeselect;
        //最近一周
        if(this.state.dropdownkey == "0"){
            timeselect = now.getTime() - 7*24*3600*1000;
        }
        if(this.state.dropdownkey == "1"){
            timeselect = now.getTime() - 30*24*3600*1000;
        }
        if(this.state.dropdownkey == "2"){
            timeselect = now.getTime() - 90*24*3600*1000;
        }
        if(this.state.dropdownkey == "3"){
            timeselect = now.getTime() - 183*24*3600*1000;
        }
        for(let i=0;i<datas.length;i++){
            if(datas[i].id == this.state.searchWord||datas[i].product[0].product_id == this.state.searchWord ||this.state.searchWord ==''){
                if(datas[i].update_time.getTime() - timeselect > 0){
                    if(this.state.starttime!==0&&this.state.endtime!==0){
                        if(datas[i].createAt.getTime()>=this.state.starttime&&datas[i].createAt.getTime()<=this.state.endtime){
                            filtereddata.push(datas[i]);
                        }
                    }else if(this.state.starttime==0&&this.state.endtime!==0){
                        if(datas[i].createAt.getTime()<=this.state.endtime){
                            filtereddata.push(datas[i]);
                        }
                    }else if(this.state.starttime!==0&&this.state.endtime==0){
                        if(datas[i].createAt.getTime()>=this.state.starttime){
                            filtereddata.push(datas[i]);
                        }
                    }else if(this.state.starttime==0&&this.state.endtime==0){
                        filtereddata.push(datas[i]);
                    }
                }

            }
        }
        return filtereddata;
    }
    clickDown(obj){
        if(obj.key == "0"){
            this.setState({
                dropdownLable:I18n.t('returnnote.lastweek'),
            });
        }
        if(obj.key == "1"){
            this.setState({
                dropdownLable:I18n.t('returnnote.lastmonth'),
            });
        }
        if(obj.key == "2"){
            this.setState({
                dropdownLable:I18n.t('returnnote.lastthreemonths'),
            });
        }
        if(obj.key == "3"){
            this.setState({
                dropdownLable:I18n.t('returnnote.recenthalfyear'),
            });
        }
        this.setState({
            dropdownkey:obj.key,
        });
        if(this.state.downflag == false){
            this.setState({downflag: true});
        }else{
            this.setState({downflag: false});
        }
        this.setState({
            runtime:0,
        });
    }
    searchdate(startvalue,endvalue){
        let result = [];
        let start = new Date(startvalue).getTime();
        let end = new Date(endvalue).getTime();
        this.setState({
            starttime:start,
            endtime:end,
            runtime:0,
        });
    }
	render() {
        let filter_data = this.dataFilter(this.props.orderResult);
        let data = filter_data.slice((this.state.current_page-1)*10, this.state.current_page*10);
        if (data != null) {
            return (
                <div className="sellerCenterOrder">
                    <Tabs >
                        <TabPane tab={I18n.t('order.all')} key="1"  >
                            {/*全部*/}
                            <SellerCenterOrderAll data = {data} searchdate={this.searchdate.bind(this)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length}/>
                        </TabPane>
                        <TabPane tab={I18n.t('sellercenterorder.nopayment')} key="2"  >
                            {/*未付款*/}
                            <SellerCenterOrderAll data = {data.filter(item=>item.order_status==1)} searchdate={this.searchdate.bind(this)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length}/>
                        </TabPane>
                        <TabPane tab={I18n.t('order.waitorder')} key="3">
                            {/*待发货*/}
                            <SellerCenterOrderAll data = {data.filter(item=>item.order_status==2)} searchdate={this.searchdate.bind(this)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length}/>
                        </TabPane>
                        <TabPane tab={I18n.t('sellercenterorder.goodsnotreceived')} key="4">
                            {/*未收货*/}
                            <SellerCenterOrderAll data = {data.filter(item=>item.order_status==3)} searchdate={this.searchdate.bind(this)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length}/>
                        </TabPane>
                        <TabPane tab={I18n.t('sellercenterorder.successfultrade')} key="5">
                            {/*交易成功*/}
                            <SellerCenterOrderAll data = {data.filter(item=>item.order_status==4)} searchdate={this.searchdate.bind(this)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length}/>
                        </TabPane>
                        <TabPane tab={I18n.t('sellercenterorder.tobeevaluated')} key="6"  >
                            {/*待评价*/}
                            <SellerCenterOrderAll  data = {data.filter(item=>item.order_status==5)} searchdate={this.searchdate.bind(this)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length}/>
                        </TabPane>
                        <TabPane tab={I18n.t('sellercenterorder.problemorder')} key="7">
                            {/*问题订单*/}
                            <SellerCenterOrderAll data = {data.filter(item=>item.problem_order==2)} searchdate={this.searchdate.bind(this)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length}/>
                        </TabPane>

                        <TabPane tab={I18n.t('sellercenterorder.transactionclosed')} key="8">
                            {/*交易关闭*/}
                            <SellerCenterOrderAll data = {data.filter(item=>item.order_status==7)} searchdate={this.searchdate.bind(this)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length}/>
                        </TabPane>
                        <TabPane tab={I18n.t('sellercenterorder.thoroughcompletion')} key="9">
                            {/*彻底完结*/}
                            <SellerCenterOrderAll data = {data.filter(item=>item.order_status==8)} searchdate={this.searchdate.bind(this)} searchcommodity={this.searchcommodity.bind(this)} clickDown={this.clickDown.bind(this)} downflag={this.state.downflag} setRuntime={this.setRuntime.bind(this)} runtime={this.state.runtime} dropdownLable={this.state.dropdownLable} current_page={this.state.current_page} changePage={this.changePage.bind(this)} datalength={filter_data.length}/>
                        </TabPane>
                    </Tabs>
                </div>
            );
		}
		else {
            return <div className="sellerCenterOrder"></div>;
		}
	}
}
export default createContainer(() => {
	Meteor.subscribe('order');
	return {
		orderResult: Order.find({seller_id: Meteor.userId()}, {sort: {update_time: -1}}).fetch(),
	};
}, SellerCenterOrder);
