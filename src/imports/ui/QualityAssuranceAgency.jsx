import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { createContainer } from 'meteor/react-meteor-data';
import { Table, Button } from 'antd';
import OpenServiceBox from '../modules/SellerCenter/OpenServiceBox';

import {Warranty} from '../api/Warranty.js';
import {Users} from '../api/User.js';
import config from '../config.json';
import { Meteor } from 'meteor/meteor';


class QualityAssuranceAgency extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            hasOpen: [],
            serviceBoxDis: 'none',
            openIndex: '',
            data:[],
            selectone:'',
        });
        this.handleService = this.handleService.bind(this);
        this.handleOpen = this.handleOpen.bind(this);
    }
    //格式化数据
    initData(data){
        let role = Users.findOne({_id:Meteor.userId()});
        let formattedData = [];
        let hasOpen = [];
        console.log(role);
        for(let i = 0 ;i < data.length ;i++){
            let flag = false;
            if(role.warrantyAgency != undefined){
                if(role.warrantyAgency.length > 0){
                    for(let j = 0;j<role.warrantyAgency.length;j++){
                        if(role.warrantyAgency[j] == data[i]._id){
                            flag = true;
                            console.log("222222");
                            break;
                        }
                    }
                }
            }
            hasOpen.push(flag);
            console.log(flag);
            let item = {
                key: data[i]._id,
                agencyName: data[i].agencyName,
                agreement:data[i].agreement,
                flag:flag,
            };
            formattedData.push(item);
        }
        console.log("-------------------");
        this.setState({
            data:formattedData,
            hasOpen:hasOpen,
        });
    }
    componentDidMount() {
        if (this.props.warrantyList != null) {
            this.initData(this.props.warrantyList);
        }
    }
    componentWillReceiveProps(nextProps) {
        console.log("执行componentWillReceiveProps 函数");
        if (this.props.warrantyList.length==0 || (nextProps.warrantyList != null && nextProps.warrantyList != this.props.warrantyList) || (nextProps.userlist != null && nextProps.userlist != this.props.userlist)) {
            console.log("props改变");
            this.initData(nextProps.warrantyList);
        }
    }

    handleService(e,record,index,text) {
        this.setState({
            serviceBoxDis: 'block',
            openIndex: index,
            selectone:record.key,
        });

    }

    handleOpen(checked) {
        console.log(checked);
        const hasOpen = this.state.hasOpen;
        if(checked){
            Meteor.call('user.addWarrantyAgency',this.state.selectone,function(err,result){
                if(err){
                    console.log(err);
                }else{
                    console.log("成功添加质保机构");
                    this.setState({
                        serviceBoxDis: 'none',
                        openIndex: '',
                        selectone:'',
                    });

                }
            }.bind(this));
        }else{
            this.setState({
                serviceBoxDis: 'none',
                openIndex: '',
                selectone:'',
            });
        }
    }

    render() {
        let This = this;
        let hasOpen = this.state.hasOpen;
        const columns = [{
            title: '质保机构名称',
            dataIndex: 'agencyName',
        }, {
            title: '条例/协议',
            dataIndex: 'agreement',
            render: (text, record) => (
                <span>
                    <a href={(config.file_server + text)} target="_blank" >{I18n.t('CompantInvoicsOne.warranty')}</a>
                </span>
            ),
        }, {
            title: '操作',
            dataIndex: 'flag',
            render(text, record, index) {
                    if (!text) {
                        //if (!hasOpen [index]){
                        console.log(text);
                        return <Button
                            className="open-service-btn"
                            type="ghost"
                            onClick={This.handleService.bind(this, event, record, index, text)}
                        >开通服务
                        </Button>;
                    } else {
                        return <span className="has-open-font">已开通</span>;
                    }
            }
        }];
        return(
            <div>
                <Table
                    className="quality-agency-table"
                    columns={columns}
                    dataSource={this.state.data}
                    bordered
                />
                <OpenServiceBox
                    handle={this.handleOpen}
                    style={{'display': this.state.serviceBoxDis}}/>
            </div>
        );
    }
}


export default createContainer(() => {
    Meteor.subscribe('warranty_agency');
    Meteor.subscribe('users');
    return {
        warrantyList: Warranty.find({}).fetch(),
        userlist : Users.find({}).fetch(),
    };
}, QualityAssuranceAgency);