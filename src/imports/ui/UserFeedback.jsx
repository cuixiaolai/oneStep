import React, { Component } from 'react';
import { I18n } from 'react-redux-i18n';
import config from '../config.json';
import { Radio,Input,message } from 'antd';

const RadioGroup = Radio.Group;

export default class UserFeedback extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            type: null,
            suggestion: '',
            satisfaction: I18n.t('userFeedback.verySatisfied'),
            contact: '',
            phone: '',
            inputPhoneWarn: '',
        });
        this.changeType = this.changeType.bind(this);
        this.changeSatisfaction = this.changeSatisfaction.bind(this);
        this.changeText = this.changeText.bind(this);
        this.submit = this.submit.bind(this);
    }
    changeType(e) {
        this.setState({
            type: e.target.value,
        });
    }
    changeSatisfaction(e) {
        this.setState({
            satisfaction: e.target.value
        })
    }
    getContactWay(type,e) {
        console.log(type,e.target.value);
        if (type == 'contact') {
            this.setState({
                contact: e.target.value
            });
        } else if (type == 'phone') {
            const phone = e.target.value;
            const regPhone = /^\d*$/;
            if (regPhone.test(phone)) {
                e.target.style.outline = 0;
                e.target.style.borderColor = '#5586b4';
                this.setState({
                    phone: phone,
                    inputPhoneWarn: ''
                });
            }else{
                e.target.style.outline = '1px solid #ed7020';
                e.target.style.borderColor = 'transparent';
                this.setState({
                    inputPhoneWarn: I18n.t('userFeedback.iptPhone'),
                    phone: '',
                });
            }
        }
    }
    changeText(e) {
        console.log(e.target.value);
        this.setState({
            suggestion: e.target.value
        })
    }
    submit() {
        if (this.state.type && this.state.suggestion && this.state.satisfaction && this.state.contact && this.state.phone) {
            Meteor.call('feedback.addItem', this.state.type, this.state.suggestion, this.state.satisfaction, this.state.contact, this.state.phone);
            history.back();
            message.success('感谢您的反馈！');
        }else{
            message.warning('请将反馈信息填写完整！');
        }
    }
    render() {
        const vertical = {
            display: 'block',
            marginBottom: '15px',
        }
        return (
            <div className="user-feedback-box">
                <div className="feedback-content">
                    <p className="title">{I18n.t('userFeedback.title')}</p>
                    <p>{I18n.t('userFeedback.greet')}</p>
                    <p className="desc">{I18n.t('userFeedback.description1')}<span>{I18n.t('userFeedback.corp')}</span>{I18n.t('userFeedback.description2')}</p>
                    <p className="put">{I18n.t('userFeedback.put')}</p>
                    <div className="feedback-card feedback-type">
                        <p className="card-title"><img src={(config.theme_path + '1icon.png')} alt="1"/>{I18n.t('userFeedback.type')}</p>
                        <RadioGroup onChange={this.changeType} value={this.state.type}>
                            <Radio value={I18n.t('userFeedback.pageQues')}>{I18n.t('userFeedback.pageQues')}</Radio>
                            <Radio value={I18n.t('userFeedback.funcQues')}>{I18n.t('userFeedback.funcQues')}</Radio>
                            <Radio value={I18n.t('userFeedback.optimizeSug')}>{I18n.t('userFeedback.optimizeSug')}</Radio>
                            <Radio value={I18n.t('userFeedback.expFeedback')}>{I18n.t('userFeedback.expFeedback')}</Radio>
                            <Radio value={I18n.t('userFeedback.other')}>{I18n.t('userFeedback.other')}</Radio>
                        </RadioGroup>
                    </div>
                    <div className="feedback-card">
                        <p className="card-title"><img src={(config.theme_path + '2icon.png')} alt="2"/>{I18n.t('userFeedback.suggestion')}</p>
                        <Input onChange={this.changeText} className="area" type="textarea" maxLength={500} placeholder={I18n.t('userFeedback.wordLimit')}/>
                    </div>
                    <div className="feedback-card feedback-satisfaction">
                        <p className="card-title"><img src={(config.theme_path + '3icon.png')} alt="3"/>{I18n.t('userFeedback.satisfaction')}</p>
                        <RadioGroup onChange={this.changeSatisfaction} value={this.state.satisfaction}>
                            <Radio style={vertical} value={I18n.t('userFeedback.verySatisfied')}>{I18n.t('userFeedback.verySatisfied')}</Radio>
                            <Radio style={vertical} value={I18n.t('userFeedback.satisfied')}>{I18n.t('userFeedback.satisfied')}</Radio>
                            <Radio style={vertical} value={I18n.t('userFeedback.soso')}>{I18n.t('userFeedback.soso')}</Radio>
                            <Radio value={I18n.t('userFeedback.dissatisfied')}>{I18n.t('userFeedback.dissatisfied')}</Radio>
                        </RadioGroup>
                    </div>
                    <div className="feedback-card">
                        <p className="card-title"><img src={(config.theme_path + '4icon.png')} alt="4"/>{I18n.t('userFeedback.contactWay')}</p>
                        <label htmlFor="feedback-contact" style={{marginLeft: 25}}>{I18n.t('userFeedback.contactPerson')}</label>
                        <Input maxLength={10} id="feedback-contact" onChange={this.getContactWay.bind(this,'contact')} style={{width: 160,height: 32}}/>
                        <label htmlFor="feedback-phone" style={{marginLeft: 38}}>{I18n.t('userFeedback.phone')}</label>
                        <Input id="feedback-phone" onChange={this.getContactWay.bind(this,'phone')} style={{width: 160,height:32}}/>
                        <span style={{color: '#ed7020',marginLeft: 10}}>{this.state.inputPhoneWarn}</span>
                    </div>
                </div>
                <button onClick={this.submit} className="submit-btn">{I18n.t('userFeedback.submit')}</button>
            </div>
        );
    }
}
