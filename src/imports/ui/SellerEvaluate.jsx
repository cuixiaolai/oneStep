import React, {Component} from 'react';
// import {Meteor} from 'meteor/meteor';
import {Link, browserHistory} from 'react-router';
import {Translate, I18n} from 'react-redux-i18n';
import config from '../config.json';
import {Checkbox, Button, message, Input, Rate} from 'antd';
// import {createContainer} from 'meteor/react-meteor-data';
// import {Order} from '../api/Order.js';
// import {Comment} from '../api/Comment.js';


export default class SellerEvaluate extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            runtime:0,
            show_comment: [],
            comment_value: [],
            comment_star:0
        });
    }
    componentDidMount(){
        if(this.props.orderInfo != null){
            for(let i in this.props.orderInfo.product){
                this.state.show_comment.push(false);
                this.state.comment_value.push('');
            }
            this.setState({
                runtime:1,
                show_comment:this.state.show_comment,
                comment_value:this.state.comment_value
            });
        }
    }
    componentWillReceiveProps(nextProps){
        if(this.state.runtime == 0 && nextProps.orderInfo != null){
            for(let i in nextProps.orderInfo.product){
                this.state.show_comment.push(false);
                this.state.comment_value.push('');
            }
            this.setState({
                runtime:1,
                show_comment:this.state.show_comment,
                comment_value:this.state.comment_value
            });
        }
    }
    Open(num) {
        this.state.show_comment[num] = true;
        this.setState({
            show_comment:this.state.show_comment
        });
    }
    handleSubmitSellerProductComment(num) {
        this.state.show_comment[num] = false;
        this.state.comment_value[num] = document.getElementById('comment_value'+num).value;
        this.setState({
            show_comment:this.state.show_comment,
            comment_value:this.state.comment_value
        });
    }
    showCommodityEvaluation() {
        let temp = [];
        for (let i in this.props.orderInfo.product) {
            temp.push(
                <div className="Commodityevaluation" style={{borderBottom: 0}}>
                    <div className="left">
                        <img src={(config.theme_path + 'huo_03.jpg')} alt=""/>
                        <p style={{color: '#ed7020'}}>{this.props.orderInfo.product[i].product_id}</p>
                        <p>{I18n.t('evaluate.num')}{this.props.orderInfo.product[i].quantity}</p>
                    </div>
                    <div className="left CommodityevaluationText"
                         style={{borderBottom: '1px solid #d6d6d6', paddingBottom: '40px'}}>
                        <p className="SellerEvaluate-inner">
                            <div className="left">{I18n.t('sellerorderdetails.buyers')}</div>
                            <div className="left">
                                <p><span>{I18n.t('productdetails.commodityevaluation')}：</span></p>
                                <div>{this.props.commentResult[i].customer_comment_product}</div>
                                <p><span>{I18n.t('evaluate.Serviceevaluation')}：</span></p>
                                <div>{this.props.commentResult[i].customer_comment_service}</div>
                                <p style={{marginTop: 20, marginBottom: 20}}>
                                    <img src={(config.theme_path + 'huo_03.jpg')} alt=""/>
                                    <img src={(config.theme_path + 'huo_03.jpg')} alt=""/>
                                    <img src={(config.theme_path + 'huo_03.jpg')} alt=""/>
                                </p>
                                <p>
                                    <span className="left">{I18n.t('management.Pin')}</span>
                                    <Rate disabled allowHalf
                                          defaultValue={this.props.commentResult[i].comment_five_stars.socle}/>
                                </p>
                                <p>
                                    <span>{I18n.t('management.speed')}</span>
                                    <Rate disabled allowHalf
                                          defaultValue={this.props.commentResult[i].comment_five_stars.speed}/>
                                </p>
                                <p>
                                    <span className="left">{I18n.t('management.package')}</span>
                                    <Rate disabled allowHalf
                                          defaultValue={this.props.commentResult[i].comment_five_stars.package}/>
                                </p>
                                <p>
                                    <span className="left">{I18n.t('management.service')}</span>
                                    <Rate disabled allowHalf
                                          defaultValue={this.props.commentResult[i].comment_five_stars.service}/>
                                </p>
                                <p style={{marginBottom: 15}}>
                                    <span className="left">{I18n.t('management.identification')}</span>
                                    <Rate disabled allowHalf
                                          defaultValue={this.props.commentResult[i].comment_five_stars.label}/>
                                </p>
                            </div>
                            <div className="clear"></div>
                        </p>
                        {this.props.commentResult[i].customer_has_addition ?
                            <p className="SellerEvaluate-inner">
                                <div className="left">买家追评:</div>
                                <div className="left">
                                    <p style={{lineHeight: 1.5}}>{this.props.commentResult[i].customer_additional_comment}</p>
                                </div>
                                <div className="clear"></div>
                            </p>
                            :''
                        }
                        {
                            this.state.comment_value[i] !=''?
                            <p className="SellerEvaluate-inner" >
                                <div className="left">{I18n.t('BuyerBox.seller')}</div>
                                <div className="left">
                                    <p style={{lineHeight:1.5}}>{this.state.comment_value[i]}</p>
                                </div>
                                <div className="clear"></div>
                            </p>:''
                        }
                        <p className="SellerEvaluateReplyBox" style={this.state.show_comment[i] ? {display:'block'} : {display:'none'}}>
                            <p>
                                <Input id={'comment_value'+i} type="textarea"  maxLength={500} placeholder={I18n.t('management.five')} />
                                <p className="clear"></p>
                            </p>
                            <p>
                                <Button onClick={this.handleSubmitSellerProductComment.bind(this,i)} style={{width:60,height:28,marginTop:'10px',padding:0,fontSize:'14px'}} className="right" type="primary">{I18n.t('Addresspage.sure')}</Button>
                                <p className="clear"></p>
                            </p>
                        </p>
                        <Button  style={this.state.show_comment[i] ? {display:'none'} : {display:'block'}} onClick={this.Open.bind(this,i)} className="SellerEvaluateReply" type="primary">{I18n.t('management.buyerreply')}</Button>
                    </div>
                    <div className="clear"></div>
                </div>
            );
        }
        return temp;
    }
    handleChangeCommentStar(value){
        this.setState({
            comment_star:value
        });
    }
    handleSubmit() {
        Meteor.call('order.sellerComment', this.props.orderInfo._id, this.state.comment_value,this.state.comment_star, (err) => {
            if (err) {

            } else {
                browserHistory.replace('/seller_center/order');
            }
        });
    }
    render() {
        console.log( this.props.orderInfo, this.props.commentResult,this.state.comment_value);
        if (this.props.orderInfo != null && this.props.commentResult[0] != null) {
            return (
                <div className="OrderBox">
                    <p>{I18n.t('evaluate.name')}</p>
                    <div className="EvalualeBoxInner">
                        <p>
                            <div className="right">
                                {I18n.t('evaluate.notes')}
                            </div>
                            <div className="right">
                                <img src={(config.theme_path + 'origin3_03.png')} alt=""/>
                            </div>
                            <div className="clear"></div>
                        </p>
                        <div>
                            <div className="left">
                                <img src={(config.theme_path + 'shop_03.jpg')} alt=""/>
                            </div>
                            <div className="left">
                                <p>{I18n.t('BuyerBox.buyers')}</p>
                                <p>{this.props.orderInfo.customer_name}</p>
                                <p>{I18n.t('buyer.level')} {I18n.t('Grade.two')} </p>
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>

                    <div className="EvalualeBoxMessage" style={{paddingBottom: 40}}>
                        <p>{I18n.t('evaluate.message')}</p>
                        {this.showCommodityEvaluation()}
                        <div className="SellerEvaluateBottomBtn" style={{padding: '0 175px'}}>
                            <span className="left" style={{marginRight: 30}}>{I18n.t('logistics.buyer')}</span>
                            <Rate onChange={this.handleChangeCommentStar.bind(this)} value={this.state.comment_star} style={{fontSize: '14px'}} allowHalf defaultValue={0}/>
                            <p className="clear"></p>
                            <Button type="primary"
                                    onClick={this.handleSubmit.bind(this)}>{I18n.t('Addresspage.sure')}</Button>
                        </div>
                    </div>
                </div>
            )
        } else {
            return <div></div>
        }
    }
}
// export default createContainer(() => {
//     Meteor.subscribe('order');
//     Meteor.subscribe('comment');
//     let order = Order.findOne({seller_id: Meteor.userId(), 'operation.seller_comment': true});
//     let comment = [];
//     if (order != null) {
//         for (let i in order.product) {
//             comment.push(Comment.findOne({_id: order.product[i].comment}));
//         }
//     }
//     return {
//         orderInfo: order,
//         commentResult: comment
//     };
// }, SellerEvaluate);