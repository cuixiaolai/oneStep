import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Buyer } from '../api/Buyer.js';
import PersonAuthentication from '../modules/center/PersonAuthentication.jsx';
import CompanyAuthentication from '../modules/center/CompanyAuthentication.jsx';

export default class PersonalCenterSecurityAuthentication extends Component {
    show() {
        if(Meteor.userId() != null && this.props.currentUserInfo!=null){
            if (Roles.userIsInRole(Meteor.userId(), 'company')) {
                return <CompanyAuthentication currentUserInfo={this.props.currentUserInfo} />;
            }
            else if(Roles.userIsInRole(Meteor.userId(), 'person')){
                return <PersonAuthentication currentUserInfo={this.props.currentUserInfo} />;
            }
        }
    }
    render() {
        return(
            <div>
                {this.show()}
            </div>
        )
    }
}

// export default createContainer(() => {
//     Meteor.subscribe('buyer');
//     return {
//         currentUserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'authentication': 1}}),
//     };
// }, PersonalCenterSecurityAuthentication);