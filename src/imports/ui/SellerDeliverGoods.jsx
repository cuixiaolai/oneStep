import React, { Component } from 'react';
import { Link,browserHistory } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../config.json';
import { Button,Input,Cascader } from 'antd';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Order } from '../api/Order.js';
import { Stock } from '../api/Stock.js';


export default class SellerDeliverGoods extends Component{
    constructor(state) {
        super(state);
        this.state = ({
            delivery_type:1,
            logistics_company:[{
                value: 'yunda',
                label: '韵达快递',
            }]
        });
    }
    handleSubmit(stock,order){
        const delivery_stock = [];
        for(let i in stock){
            let stock_address_temp = [];
            for(let j in stock[i].stock_address){
                let temp = parseInt(document.getElementById('stock_address'+i+'_'+j).value);
                stock_address_temp.push(isNaN(temp)?0:temp);
            }
            let origin_address_temp = [];
            for(let j in stock[i].origin_address){
                let temp = parseInt(document.getElementById('origin_address'+i+'_'+j).value);
                origin_address_temp.push(isNaN(temp)?0:temp);
            }
            let batch_temp = [];
            for(let j in stock[i].batch){
                let temp = parseInt(document.getElementById('batch'+i+'_'+j).value);
                batch_temp.push(isNaN(temp)?0:temp);
            }
            let package_arr_temp = [];
            for(let j in stock[i].package){
                let temp = parseInt(document.getElementById('package_arr'+i+'_'+j).value);
                package_arr_temp.push(isNaN(temp)?0:temp);
            }
            delivery_stock.push({
                stock_id:stock[i]._id,
                stock_address:stock_address_temp,
                origin_address:origin_address_temp,
                batch:batch_temp,
                package:package_arr_temp
            })
        }
        const delivery_type = this.state.delivery_type;
        let delivery_order;
        if(delivery_type == 1){
            const logistics_company = this.state.logistics_company[0].label;
            const logistics_number = parseInt(document.getElementById('logistics_number').value);
            delivery_order = {
                delivery_type:delivery_type,
                delivery_stock:delivery_stock,
                logistics_company:logistics_company,
                logistics_number:logistics_number
            }
        }else if(delivery_type == 2){
            const delivery_people = document.getElementById('delivery_people').value;
            const delivery_number = parseInt(document.getElementById('delivery_number').value);
            delivery_order = {
                delivery_type:delivery_type,
                delivery_stock:delivery_stock,
                delivery_people:delivery_people,
                delivery_number:delivery_number
            }
        }
        Meteor.call('stock.reduceStockByDelivery', delivery_stock, (err) => {
            if (err) {

            } else {
                Meteor.call('order.deliverGoods', order._id, delivery_order, (err) => {
                    if (err) {

                    } else {
                        Meteor.call('message.createMessage', Meteor.userId(),order.customer_id, 3, order.id.toString());
                        browserHistory.replace('/seller_center/order');
                    }
                });
            }
        });
    }
    handleCancel(){
        browserHistory.replace('/seller_center/order');
    }
    changeCompany(value,selectedOptions){
        this.setState({
            logistics_company: selectedOptions,
        });
    }
    changeDeliveryTypeToLogistics(){
        this.setState ({
            delivery_type:1,
        });
    }
    changeDeliveryTypeToOffline(){
        this.setState ({
            delivery_type:2,
        });
    }
    showStockAddress(stock_address,stock_num){
        let temp = [];
        for(let i in stock_address){
          temp.push(<div>{stock_address[i].address} <Input  style={{width:100,marginLeft:10}} id={'stock_address'+stock_num+'_'+i}/> </div>);
            // temp.push(<div><Input id={'stock_address'+stock_num+'_'+i} style={{width:100,marginLeft:10}}/></div>);
        }
        return temp;
    }
    showOriginAddress(origin_address,stock_num){
        let temp = [];
        for(let i in origin_address){
          // temp.push(<div><Input id={'origin_address'+stock_num+'_'+i} style={{width:100,marginLeft:10}}/></div>);
            temp.push(<div>{origin_address[i].address}<Input id={'origin_address'+stock_num+'_'+i} style={{width:100,marginLeft:10}}/></div>);
        }
        return temp;
    }
    showBatch(batch,stock_num){
        let temp = [];
        for(let i in batch){
          // temp.push(<div><Input id={'batch'+stock_num+'_'+i} style={{width:100,marginLeft:10}}/></div>);
            temp.push(<div>{batch[i].num}<Input id={'batch'+stock_num+'_'+i} style={{width:100,marginLeft:10}}/></div>);
        }
        return temp;
    }
    showPackage(package_arr,stock_num){
        let temp = [];
        for(let i in package_arr){
          // temp.push(<div><Input id={'package_arr'+stock_num+'_'+i} style={{width:100,marginLeft:10}}/></div>);
            temp.push(<div>{package_arr[i].package}<Input id={'package_arr'+stock_num+'_'+i} style={{width:100,marginLeft:10}}/></div>);
        }
        return temp;
    }
    showProduct(product,stock,stock_num){
        return(
            <div>
                <div className="left">
                    <img src={(config.theme_path + 'huo_03.jpg')} alt=""/>
                    <p>型号:{product.product_id}</p>
                    <p>厂牌:{product.manufacturer} </p>
                </div>
                <div className="SellerCenterUploadStockAddBox" style={{padding:10,marginBottom:10}}>
                    <div style={{with:'100%'}}>
                        <div style={{lineHeight:'28px',marginRight:5,width:100}}>购买总量:{product.quantity}</div>
                    </div>

                    <div>
                        <div style={{lineHeight:'28px',marginRight:5,width:100,float:'left'}} >库存地:</div>
                            {this.showStockAddress(stock.stock_address,stock_num)}
                    </div>
                    <div>
                        <span style={{lineHeight:'28px',marginRight:5,width:100,float:'left'}} >产地:</span>
                            {this.showOriginAddress(stock.origin_address,stock_num)}
                    </div>
                    <div>
                        <span style={{lineHeight:'28px',marginRight:5,width:100,float:'left'}} >批号:</span>
                            {this.showBatch(stock.batch,stock_num)}
                    </div>
                    <div>
                        <span style={{lineHeight:'28px',marginRight:5,width:100,float:'left'}} >包装方式:</span>
                            {this.showPackage(stock.package,stock_num)}
                    </div>
                </div>
                <div className="clear"></div>
            </div>
        )
    }
    showOrder(order,stocks){
        let temp = [];
        for(let i in order.product){
            temp.push(this.showProduct(order.product[i],stocks[i],i));
        }
        return temp;
    }
    showDeliver(){
        let temp = [];
        const optionslogistics = [{
            value: 'yunda',
            label: '韵达快递',
        }, {
            value: 'yuantong',
            label: '圆通速递',
        }, {
            value: 'zhongtong',
            label: '中通快递',
        }, {
            value: 'baishi',
            label: '百世快递 ',
        }, {
            value: 'youzheng',
            label: '邮政包裹 ',
        }, {
            value: 'tiantian',
            label: '天天快递',
        }, {
            value: 'shunfeng',
            label: '顺丰速递',
        }, {
            value: 'ems',
            label: 'EMS',
        },{
          value: 'esle',
          label: '其他'
        }];
        if(this.state.delivery_type == 1){
            temp.push(
                <div className="logistics" >
                    <p className="selectlogistics">
                        <span>{I18n.t('sellercenterorder.selectlogistics')}</span>
                        <Cascader options={optionslogistics} onChange={this.changeCompany.bind(this)} defaultValue={['yunda']} />
                    </p>
                    <p >
                        <span>{I18n.t('sellercenterorder.logisticsnumber')}</span>
                        <Input id='logistics_number'/>
                    </p>
                </div>
            );
        }else if(this.state.delivery_type == 2){
            temp.push(
                <div className="delivery">
                    <p className="deliveryman">
                        <span>{I18n.t('sellercenterorder.deliveryman')}</span>
                        <Input id='delivery_people' style={{width:200}}/>
                    </p>
                    <p>
                        <span>{I18n.t('sellercenterorder.contactnumber')}</span>
                        <Input id='delivery_number' style={{width:200}}/>
                    </p>
                </div>
            );
        }else{
            temp = null;
        }
        return temp;
    }
    render(){
        if(this.props.orderResult != null && typeof this.props.stockResult[0] != 'undefined'){ //should check each stock?
            console.log(this.props.orderResult,this.props.stockResult);
            return(
                <div className="OrderBox">
                    <Button onClick={this.changeDeliveryTypeToLogistics.bind(this)} type={this.state.delivery_type == 1?'primary':'ghost'} style={{marginRight:'20px'}}>物流发货</Button>
                    <Button onClick={this.changeDeliveryTypeToOffline.bind(this)} type={this.state.delivery_type == 2?'primary':'ghost'}>线下送货</Button>
                    <div className="clear"></div>
                    {this.showOrder(this.props.orderResult,this.props.stockResult)}
                    {this.showDeliver()}
                    <Button onClick={this.handleSubmit.bind(this, this.props.stockResult, this.props.orderResult)} style={{marginRight:'20px'}}>确定</Button>
                    <Button onClick={this.handleCancel.bind(this)} >取消</Button>
                </div>
            )
        }else {
            return(
                <div className="OrderBox"></div>
            )
        }

    }
}
export default createContainer(() => {
    Meteor.subscribe('order');
    Meteor.subscribe('stock');
    let path=window.location.pathname;
    let arr=path.split('/');
    let order=Order.findOne({seller_id: Meteor.userId(), _id:arr[arr.length-1], order_status:2, problem_order:{$in:[1,3]}});
    let stock=[];
    if(order != null){
        for(let i in order.product){
            let temp=Stock.findOne({_id:order.product[i].stock_id, user_id: Meteor.userId()});//if stock is off-shelve or... ?
            stock.push(temp);
        }
    }
    return {
        orderResult: order,
        stockResult: stock
    };
}, SellerDeliverGoods);
