import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
// import { Meteor } from 'meteor/meteor'
import {Menu, Icon} from 'antd';
// import BottomBox from '../modules/BottomBox.jsx';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Buyer } from '../api/Buyer.js';
// import { Cart } from '../api/Cart.js';
// import { Message } from '../api/Message.js';
import config from '../config.json';
const SubMenu = Menu.SubMenu;
import TopBox from '../modules/TopBox.jsx';
import {get} from '../../utils/request.js';

export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = ({
			user: null,
			flag:true,
			scrollTop: '',
			i:0,
		});
		this.handleScroll = this.handleScroll.bind(this);
	}
	componentWillMount() {
		this.setState({user: "Meteor.userId()"});
	}
	componentDidMount() {
		const This = this;
		window.addEventListener('scroll',This.handleScroll);
	}
	handleScroll() {
		this.setState({
			scrollTop: window.pageYOffset
		});
	}
	TopBox(){
		let path=window.location.pathname;
		if(path!='/' && path != '/user_feedback'){
			return(
				<TopBox path={path}/>
			)
		}
	}
	feedbackTopBox() {
		let path = window.location.pathname;
		if(path == '/user_feedback') {
			return (
				<div className="feedback-top-box">
					<Link to="/"><img src={(config.theme_path + '/logo.png')} alt="" className="left" /></Link>
					<div className="longxian left"></div>
					<div className="left cheng">
						<p>{I18n.t('top.name')}</p>
					</div>
				</div>
			);
		}
	}
	Showpeotop(){
		// if (this.props.currentUser != null) {
		// 	if (Roles.userIsInRole(Meteor.userId(), 'company')){
		// 		return (
		// 			<Translate value="PersonalCenter.comname" />
		// 		)
		// 	}
		// 	else if (Roles.userIsInRole(Meteor.userId(), 'person')) {
			console.log("Showpeotop")
				return (
					<Translate value="app.person.top" />
				)
		// 	}
		// }
	}
	Showpeo(){
		console.log("Showpeo")
		if (this.props.currentUser != null) {
			// if (Roles.userIsInRole(Meteor.userId(), 'company')){
			// 	return (
			// 	<ul>
			// 		<li><Link to="/personal_center/order"><Translate value="logisticsdetails.order" /></Link></li>
			// 		<li><Link to="/personal_center/info"><Translate value="PersonalCenter.Message.personalmessage" /></Link></li>
			// 		<li><Link to="/personal_center/address"><Translate value="PersonalCenter.Message.address" /></Link></li>
			// 	</ul>
			// 	)
			// }
			// else if (Roles.userIsInRole(Meteor.userId(), 'person')) {
				return(
					<ul>
						<li><Link to="/personal_center/order"><Translate value="logisticsdetails.order" /></Link></li>
						<li><Link to="/personal_center/info"><Translate value="PersonalCenter.Message.personalmessage" /></Link></li>
						<li><Link to="/personal_center/address"><Translate value="PersonalCenter.Message.address" /></Link></li>
					</ul>
				)
			// }
		}
	}
	LogoutClick(e){
		// Meteor.logout(function(err) {
		// 	if(err) {
		// 		console.log(err);
		// 	}
		// 	else {
		// 		this.setState({user: null});
		// 		this.props.history.pushState(null,'/login');
		// 	}
		// }.bind(this));
	}
	loginpersonal(){
		if ( this.props.currentUser == null){
			return(
				<div className="addRight-box">
					<li className="addRight">
						<Link to="/login" style={{fontSize:'14px',color:'#707070',float:'left',textDecoration:'none'}}><Translate value="app.person.top" /></Link>
						<div className="jiantou right">
							<img src={(config.theme_path+'tobottom.png')} alt="" className="personal06-jiantou"/>
						</div>
						<span className="shu"></span>
						<div className="clear"></div>
					</li>
					<div className="addRight-xiala">
						<ul>
							<li><Link to="/login"><Translate value="app.person.xinxi" /></Link></li>
							<li><Link to="/login"><Translate value="app.person.address" /></Link></li>
							<li><Link to="/login"><Translate value="app.person.security" /></Link></li>
							<li><Link to="/login"><Translate value="PersonalCenter.company.integral" /></Link></li>
						</ul>
					</div>
				</div>
			)
		}else if( this.props.currentUser != null){
			return(
				<div className="addRight-box">
					<li className="addRight">
						<Link to="/personal_center" style={{fontSize:'14px',color:'#707070',float:'left',textDecoration:'none'}}>{this.Showpeotop()}</Link>
						<div className="jiantou right">
							<img src={(config.theme_path+'tobottom.png')} alt="" className="personal06-jiantou"/>
						</div>
						<span className="shu"></span>
						<div className="clear"></div>
					</li>
					<div className="addRight-xiala">
						{this.Showpeo()}
					</div>
				</div>
			)
		}
	}
	ShowBuyer(){
		return(
			<ul>
				<li><Link to="/seller_center/order"><Translate value="buyercenter.order" /></Link></li>
				<li><Link to="/seller_center/upload_stock"><Translate value="buyercenter.uploadstock" /></Link></li>
				<li><Link to="/seller_center/manage_stock"><Translate value="buyercenter.managestock" /></Link></li>
				<li><Link to="/seller_center/return_note"><Translate value="PersonalCenter.Order.chnageorder" /></Link></li>
			</ul>
		)
	}
	loginseller(){
		if (this.props.currentUser == null){
			return(
				<div className="addRight-box">
					<li className="addRight">
						<Link to="/login" style={{fontSize:'14px',color:'#707070',float:'left',textDecoration:'none'}}><Translate value="buyer.buy" /></Link>
						<span className="shu"></span>
						<div className="clear"></div>
					</li>
				</div>
			)
		}else if(this.props.currentUser != null){
			// if (Roles.userIsInRole(Meteor.userId(), 'person')) {

			// }
			// else if (Roles.userIsInRole(Meteor.userId(), 'company')){
			// 	if (Roles.userIsInRole(Meteor.userId(), 'retailer')){
			// 		return(
			// 			<div className="addRight-box">
			// 				<li className="addRight">
			// 					<Link to="/seller_center" style={{fontSize:'14px',color:'#707070',float:'left',textDecoration:'none'}}><Translate value="buyercenter.name" /></Link>
			// 					<div className="jiantou right">
			// 						<img src={(config.theme_path+'tobottom.png')} alt="" className="personal06-jiantou"/>
			// 					</div>
			// 					<span className="shu"></span>
			// 					<div className="clear"></div>
			// 				</li>
			// 				<div className="addRight-xiala">
			// 					{this.ShowBuyer()}
			// 				</div>
			// 			</div>
			// 		)
			// 	} else{
			// 		return(
			// 			<div className="addRight-box">
			// 				<li className="addRight">
			// 					<Link to="/personal_center/buyer" style={{fontSize:'14px',color:'#707070',float:'left',textDecoration:'none'}}><Translate value="buyer.buy" /></Link>
			// 					<span className="shu"></span>
			// 					<div className="clear"></div>
			// 				</li>
			// 			</div>
			// 		)
			// 	}
			// }
		}
	}
	loginRender(){
		// if (Meteor.userId() != null && this.props.currentUser != null && this.props.currentUserInfo != null){
		if (this.props.currentUser != null ){
            // let name = (this.props.currentUserInfo.nickname == null || this.props.currentUserInfo.nickname == '') ? this.props.currentUser.username : this.props.currentUserInfo.nickname;
            let name = this.props.currentUser.name ;
			let len = 0;
			let i = 0;
			let reg = /[\u4e00-\u9FA5]+/;
			for (; i < name.length; ++ i) {
				let c = name.charAt(i);
				if (reg.test(c)) {
					len = len + 2;
				}
				else {
					len = len + 1;
				}
				if (len >= 10) {
					break;
				}
			}
			if (i + 1 < name.length) {
				name = name.substring(0, i + 1);
				name = name + '...';
			}
								// <li className="addRight left">
					// 	<span style={{fontSize:'14px',color:'#707070',textDecoration:'none'}}><Translate value="app.jifen" /></span>
					// 	<span style={{color:'#0a66bc'}}> 3</span>
					// </li>
			return (
				<div className="addbox left">
					<p id="top" className="left" style={{fontSize:'14px',color:'#707070',lineHeight:'34px'}}><Translate value="app.hello" /></p>
					<p className="left" style={{marginLeft:'10px',color:'#0a66bc'}}> <Link to="/personal_center/info"> {name}</Link></p>



					<li className="addRight left">
						<span style={{fontSize:'14px',color:'#0a66bc',textDecoration:'none',cursor: 'pointer'}} onClick={this.LogoutClick.bind(this)}><Translate value="app.tuichu" /></span>
					</li>
				</div>
			);
		}
		else {
			return (
				<div className="addbox left">
					<p className="left welcom"><Translate value="app.names" /></p>
					<div className="left">
						<li style={{marginLeft:'20px',fontSize:'14px',color:'#707070',lineHeight:'34px'}}  className="app-please">
							<Link to="/login" style={{fontSize:'14px',color:'#0a66bc',textDecoration:'none',float:'left',marginLeft:'3px'}}>
								<Translate value="app.please"/>
								<Translate value="app.login" />
							</Link>
						</li>
					</div>
					<div className="left">
						<li style={{marginLeft:'30px'}}><Link to="/register" style={{fontSize:'14px',color:'#0a66bc',textDecoration:'none'}}><Translate value="app.soon" /></Link></li>
					</div>
				</div>
			)
		}
	}
	shopRender(){
		// if(Meteor.userId() != null){
		if(this.props.currentUser != null){
			return(
				<li className="addRight shop">
					<Link to="/shopping_cart" style={{fontSize:'14px',color:'#707070',textDecoration:'none'}}>
						<Translate value="app.shop" />
						<span style={{color:'#0a66bc'}}> {this.props.cartInfo != null ? this.props.cartInfo.size : 0}</span>
					</Link>
					<span className="shu"></span>
				</li>
			)
		}
	}
	showmessage(){
		// if(Meteor.userId() != null){
		if(this.props.currentUser != null){
			return(
				<li className="addRight shop">
					<Link to="/message_cart" style={{fontSize:'14px',color:'#707070',textDecoration:'none'}}>
						<Translate value="messageCard.name" />
						<span style={{color:'#0a66bc'}}> {this.props.messageNum != null ? this.props.messageNum : 0}</span>
					</Link>

					<span className="shu"></span>
				</li>
			)
		}
	}
	showGoTop() {
		const path = window.location.pathname;
		if (path != '/login' && path != '/register' && path != '/' && path != '/perRegister' && path != '/perRegister/:id' && path != '/comRegister' && path != '/comRegister/:id' && path != '/forget' ) {
			return (
				<div id="goTop" className="goTop">
					{(()=>{
						if (this.state.scrollTop > 350) {
							return (
								<div className="go-top-back" onClick={this.goTop.bind(this)}>
									<img
										src={(config.theme_path + 'goTop.png')}
										alt={I18n.t('userFeedback.goTop')}
										title={I18n.t('userFeedback.goTop')}
									/>
								</div>
							);
						}
					})()}
					<div className="go-top-back">
						<img src={(config.theme_path + 'customer-service-icon.png')} alt={I18n.t('userFeedback.customerService')} title={I18n.t('userFeedback.customerService')}/>
					</div>
					<Link className="feedback-icon" style={{display: 'block'}} to="/user_feedback">
						<div className="go-top-back feedback1">
							<img src={(config.theme_path + 'feedback1-icon.png')} alt={I18n.t('userFeedback.name')} title={I18n.t('userFeedback.name')}/>
						</div>
						<div className="go-top-back feedback2">{I18n.t('userFeedback.feedback')}</div>
					</Link>
				</div>
			);
		}
	}
	goTop() {
		const timer = setInterval(function() {
			let scrollTop = window.pageYOffset;
			let speed = Math.floor(-scrollTop / 5);
			window.scrollTo(0,scrollTop + speed);
			if (scrollTop == 0) {
				clearInterval(timer);
			}
		}, 30);
	}
	render() {
		console.log(this.state.i)
		// let path=this.props.routes[1].path;
		return (
			<div>
				<div id="addBox" className="container addBox">
					<div className="row addBox-inner">
						{ this.loginRender() }
						
                        <div className="right">
					        <li className="addRight">
								<p style={{fontSize:'14px',color:'#707070'}}><Translate value="app.phone" />：4008055955</p>
							</li>
						</div>
						
						<div className="right">
							{ this.loginseller() }
						</div>
						<div className="right">
							{ this.shopRender() }
						</div>
						<div className="right">
							{ this.loginpersonal() }
						</div>
						<div className="right">
							{ this.showmessage() }
						</div>
						<div className="clear"></div>
					</div>
				</div>
				{this.TopBox()}
				{this.feedbackTopBox()}
				{this.props.children}
				{this.showGoTop()}
				{/*<BottomBox path={path} />*/}
			</div>
		);
	}
}

 class Apps extends Component {
	constructor(props) {
		super(props);
		this.state = ({
			currentUserInfo: null,
			cartInfo:null,
			currentUser: null,
			messageNum: null,
		});
	}
	componentWillMount() {
		console.log("Request")
		console.log(get)
		// get("user").then(body=>{
		get("user").then(body=>{
			console.log("body")
			console.log(body)
			if(body.code&&body.code == 200)
			this.setState({currentUser: body.body});
		})
		// this.setState({user: "Meteor.userId()"});
	}	
	componentWillReceiveProps(nextProps){
		console.log("willrece")
		if(!this.state.currentUser){
		get("user").then(body=>{
			console.log("body")
			console.log(body)
			if(body.code&&body.code == 200)
			this.setState({currentUser: body.body});
		})			
		}
	}

    render() {
        return <App currentUser={this.state.currentUser} children={this.props.children}/>;
    }

}

// export default createContainer(() => {
// 	Meteor.subscribe('buyer');
// 	Meteor.subscribe('cart');
//     Meteor.subscribe('message');
// 	return {
// 		currentUserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'nickname': 1}}),
// 		cartInfo: Cart.findOne({_id: Meteor.userId()}, {fields: {'size': 1}}),
// 		currentUser: Meteor.user(),
//         messageNum: Message.find({to: Meteor.userId(), sign: 1}).count(),
// 	};
// }, App);