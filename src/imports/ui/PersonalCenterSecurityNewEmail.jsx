import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import SecurityTop from '../modules/center/SecurityTop.jsx'
import { Steps, Button } from 'antd';

export default class PersonalCenterSecurityNewEmail extends Component {
    render(){
        let words={
            name: I18n.t('PersonalCenterSecurity.SecurityTopEmail.names'),
            titleone: I18n.t('PersonalCenterSecurity.SecurityTopEmail.titleone'),
            titletwo: I18n.t('PersonalCenterSecurity.SecurityTopEmail.titletwo'),
            titlethree: I18n.t('PersonalCenterSecurity.SecurityTopEmail.titlethree'),
        };
        return(
            <div>
                <SecurityTop words={words} flag={true} />
            </div>
        )
    }
}
