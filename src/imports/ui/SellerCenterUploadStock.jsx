import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs } from 'antd';
import SellerCenterUploadStockBox from '../modules/SellerCenter/SellerCenterUploadStockBox.jsx';
import SellerCenterUploadStockAdd from '../modules/SellerCenter/SellerCenterUploadStockAdd.jsx';
const TabPane = Tabs.TabPane;


export default class SellerCenterUploadStock extends Component {
	click(){
		this.setState({key:'2'});
	}
	render(){
		console.log(this.state);
		return (
			<div  className="SellerCenterUploadStock">
				<SellerCenterUploadStockBox click={this.click.bind(this)} />
			</div>
		);
	}
}
