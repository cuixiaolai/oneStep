import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router'
import { createContainer } from 'meteor/react-meteor-data';
import { Cart } from '../api/Cart.js';
import { Tabs } from 'antd';
import ShoppingCartContent from '../modules/shoppingCart/ShoppingCartContent.jsx';
{/*购物车*/}
const TabPane = Tabs.TabPane;

export default class ShoppingCart extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            key:'1',
        });
    }
    changekey(key){
        console.log(key);
        this.setState({key:key});
    }
    showCartContent() {
        if (this.props.cartInfo != null) {
            if (this.props.cartInfo.cart != null) {
                return (
                    <Tabs defaultActiveKey="1" onChange={this.changekey.bind(this)}>
                        <TabPane tab={I18n.t('shoppingCart.tab1')} key="1">
                            <ShoppingCartContent  keys={0} cartInfo={this.props.cartInfo.cart.filter(item=>item.delivery_address==0)}/>
                        </TabPane>
                        <TabPane tab={I18n.t('shoppingCart.tab2')} key="2">
                            <ShoppingCartContent  keys={1} cartInfo={this.props.cartInfo.cart.filter(item=>item.delivery_address==1)}/>
                        </TabPane>
                        <TabPane tab={I18n.t('shoppingCart.tab3')} key="3">
                            <ShoppingCartContent keys={2} cartInfo={this.props.cartInfo.cart.filter(item=>item.delivery_address==2)}/>
                        </TabPane>
                    </Tabs>
                );
            }
            else {
                let cart = [];
                return (
                    <Tabs defaultActiveKey="1" onChange={this.changekey.bind(this)}>
                        <TabPane tab={I18n.t('shoppingCart.tab1')} key="1">
                            <ShoppingCartContent  keys={0} cartInfo={cart}/>
                        </TabPane>
                        <TabPane tab={I18n.t('shoppingCart.tab2')} key="2">
                            <ShoppingCartContent  keys={1} cartInfo={cart}/>
                        </TabPane>
                        <TabPane tab={I18n.t('shoppingCart.tab3')} key="3">
                            <ShoppingCartContent keys={2} cartInfo={cart}/>
                        </TabPane>
                    </Tabs>
                );
            }
        }
    }
    render(){
        return(
            <div className="shoppingCart">
                <h2>{I18n.t('shoppingCart.shopping')}</h2>
                <div className="cartTab">
                    {this.showCartContent()}
                </div>
        </div>);
    }
}
export default createContainer(() => {
    Meteor.subscribe('cart');
    return {
        cartInfo: Cart.findOne({_id: Meteor.userId()}),
    };
}, ShoppingCart);
