import React, { Component } from 'react';
import { Select,Rate, Dropdown, DatePicker, Modal, Button ,Input ,Menu, Icon,Form ,Tabs, InputNumber ,Checkbox ,Pagination,Radio} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../config.json';
import { Link } from 'react-router';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Stock } from '../api/Stock.js';
// import { Comment } from '../api/Comment.js';
import ProductDetailsCard from '../modules/product/ProductDetailsCard.jsx';
import ProductInfo from '../modules/product/productInfo.jsx';
import GoodsDetail from '../modules/product/GoodsDetail.jsx';
import GoodsEvaluation from '../modules/product/GoodsEvaluation.jsx';
import MerchantInfo from '../modules/product/MerchantInfo.jsx';
/*商品详情*/

export default class ProductDetails extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            value:1,
            number: 1,
            onSpan:0,
            showcard:false,
            num:5,
        });

    }
    clickLi(value){
        this.setState({onSpan:value});
    }
    onClick(word,j){
        console.log(word);
        let ProductDetailsBoxBottomname = document.getElementsByClassName('ProductDetailsBoxBottomname')[0];
        let num = ProductDetailsBoxBottomname.getElementsByTagName('span');
        for(let i=0;i<num.length;i++){
            if(i == j){
                num[j].style.color = '#0c5aa2';
                num[j].style.borderBottom = '2px solid #0c5aa2';
            }else{
                num[i].style.color = '#707070';
                num[i].style.borderBottom = '0';
            }
        }
    }
    showTab(){
        if (this.props.stockInfo != null) {
            if (this.state.onSpan == 0) {
                return <GoodsDetail stockInfo={this.props.stockInfo}/>;
            } else if (this.state.onSpan == 1) {
                return <GoodsEvaluation stockInfo={this.props.stockInfo} commentInfo={this.props.commentInfo}/>;
            } else if (this.state.onSpan == 2) {
                return <MerchantInfo sellerId={this.props.stockInfo.user_id}/>;
            }
        }
    }
    productInfo() {
        if (this.props.stockInfo != null) {
            return (
                <div className="productdetailscontent">
                    <ProductInfo stockInfo={this.props.stockInfo} />
                </div>
            );
        }
    }
    
    render() {
        if (this.props.stockInfo != null) {
            return (
                <div className="productdetails" style={{position:'relative'}}>
                    <h2>{I18n.t('productdetails.productdetails')}</h2>
                    {this.productInfo()}
                    <div className="clearFloat"></div>
                    <div>
                        <ul className="title">
                            <li ><span className={this.state.onSpan==0?'active':''}  onClick={this.clickLi.bind(this,0)}>{I18n.t('productdetails.productdetail')}</span></li>
                            <li><span  className={this.state.onSpan==1?'active':''}   onClick={this.clickLi.bind(this,1)}>{I18n.t('productdetails.commodityevaluation')}</span></li>
                            <li ><span className={this.state.onSpan==2?'active':''}   onClick={this.clickLi.bind(this,2)}>{I18n.t('productdetails.businessqualification')}</span></li>
                        </ul>
                    </div>
                    <div className="clearFloat"></div>
                    {this.showTab()}
                </div>
            );
        }else{
            return null;
        }
    }
}

// export default createContainer(() => {
//     Meteor.subscribe('stock');
//     Meteor.subscribe('comment');
//     let str = window.location.pathname.substr(17);
//     let comment = [];
//     let stock = Stock.findOne({_id: str});
//     if(stock != null){
//         for(let i in stock.comment){
//             comment.push(Comment.findOne({_id: stock.comment[i]}));
//         }
//     }
//     return {
//         stockInfo:stock,
//         commentInfo:comment
//     };
// }, ProductDetails);
