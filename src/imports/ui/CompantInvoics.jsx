import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import { Link, browserHistory } from 'react-router';
// import { createContainer } from 'meteor/react-meteor-data';
import { Steps, Button } from 'antd';
// import { Buyer } from '../api/Buyer.js';
import config from '../config.json';
import SecurityTop from '../modules/center/SecurityTop.jsx'

export default class CompantInvoics extends Component {
    constructor(props) {
        super(props);
        this.state={
            flag: false,
        };
    }
    componentWillMount() {
        if (this.props.currentUserInfo != null) {
            if (this.props.currentUserInfo.invoice == null || this.props.currentUserInfo.invoice.verify == null) {
                this.setState({flag: true});
            }
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.currentUserInfo != null) {
            if (nextProps.currentUserInfo != this.props.currentUserInfo && (nextProps.currentUserInfo.invoice == null || nextProps.currentUserInfo.invoice.verify == null)) {
                this.setState({flag: true});
            }
        }
    }
    click() {
        $.ajax({
            type: 'POST',
            url: config.file_server + 'delete_invoice',
            data: { path: this.props.currentUserInfo.invoice.tax, path1: this.props.currentUserInfo.invoice.tax_payer, path2: this.props.currentUserInfo.invoice.attorney },
            dataType: "json",
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
        // Meteor.call('buyer.deleteInvoice');
    }
    card(){
        if(this.props.currentUserInfo.invoice.verify == true && this.props.currentUserInfo.invoice.success == true){//审核成功
            return(
                <div className="CompantInvoics-card">
                    <div className="left">
                        <img src={(config.theme_path + 'green.png')} alt=""/>
                    </div>
                    <p className="left">{I18n.t('CompantInvoics.success')}</p>
                    <div className="clear"></div>
                    <div>{I18n.t('CompantInvoics.inspect')}</div>
                </div>
            )
        }
        else if(this.props.currentUserInfo.invoice.verify == false){//审核中
            return(
                <div className="CompantInvoics-card">
                    <div className="left">
                        <img src={(config.theme_path + 'watch.png')} alt=""/>
                    </div>
                    <p className="left">{I18n.t('CompantInvoics.wait')}</p>
                    <div className="clear"></div>
                    <div>{I18n.t('CompantInvoics.please')}</div>

                </div>
            )
        }
        else if(this.props.currentUserInfo.invoice.verify == true && this.props.currentUserInfo.invoice.success == false){//审核失败
            return(
                <div className="CompantInvoics-card">
                    <div className="left">
                        <img src={(config.theme_path + 'rederror.png')} alt=""/>
                    </div>
                    <p className="left">{I18n.t('CompantInvoics.error')}</p>
                    <div className="clear"></div>
                    <div>
                        {I18n.t('CompantInvoics.careful')}
                        <span onClick={this.click.bind(this)} style={{color:'#0c5aa2',cursor:'pointer'}}>{I18n.t('CompantInvoics.again')}</span>
                    </div>
                </div>
            )
        }
    }
    show(){
        if (this.props.currentUserInfo != null) {
            if (this.state.flag == true) {
                let words={
                    name:I18n.t('CompantInvoics.invoice.name'),
                    titleone:I18n.t('CompantInvoics.invoice.one'),
                    titletwo:I18n.t('CompantInvoics.invoice.two'),
                    titlethree:I18n.t('CompantInvoics.invoice.three'),
                    word:I18n.t('CompantInvoics.invoice.word'),
                    will:I18n.t('CompantInvoics.invoice.will'),
                    top:I18n.t('PersonalCenterSecurity.SecurityAuthentication.info'),
                };
                return <SecurityTop words={words} invoice={this.props.currentUserInfo.invoice} />;
            }
            else {
                return(
                    <div>
                        <p className="SecurityTopName"><span></span>{I18n.t('CompantInvoics.invoice.name')}</p>
                        {this.card()}
                        <div className="CompantInvoics-invoics">
                            <div className="CompantInvoics-invoics-name">
                                {I18n.t('CompantInvoics.yourinvoics')}
                            </div>
                            <ul className="CompantInvoics-box">
                                <li>
                                    <span>{I18n.t('CompantInvoicsThree.comname')}</span>
                                    {this.props.currentUserInfo.invoice.name}
                                </li>
                                <li>
                                    <span>{I18n.t('CompantInvoicsThree.address')}</span>
                                    {this.props.currentUserInfo.invoice.address}
                                </li>
                                <li>
                                    <span>{I18n.t('CompantInvoicsThree.phone')}</span>
                                    {this.props.currentUserInfo.invoice.tel}
                                </li>
                                <li>
                                    <span>{I18n.t('CompantInvoicsThree.code')}</span>
                                    {this.props.currentUserInfo.invoice.id}
                                </li>
                                <li>
                                    <span>{I18n.t('CompantInvoicsThree.band')}</span>
                                    {this.props.currentUserInfo.invoice.bank_name}
                                </li>
                                <li>
                                    <span>{I18n.t('CompantInvoicsThree.bandnum')}</span>
                                    {this.props.currentUserInfo.invoice.bank_account}
                                </li>
                            </ul>
                        </div>
                        <div className="CompantInvoics-invoics">
                            <div className="CompantInvoics-invoics-name">
                                {I18n.t('CompantInvoics.yourinvoics')}
                            </div>
                            <p>
                                <span className="left">{I18n.t('CompantInvoicsThree.Tax')}</span>
                                <img src={(config.file_server + this.props.currentUserInfo.invoice.tax)} alt="" className="left" />
                                <div className="clear"></div>
                            </p>
                            <p>
                                <span className="left">{I18n.t('CompantInvoicsThree.Taxpayer')}</span>
                                <img src={(config.file_server + this.props.currentUserInfo.invoice.tax_payer)} alt="" className="left" />
                                <div className="clear"></div>
                            </p>
                            <p>
                                <span className="left">{I18n.t('CompantInvoicsThree.attorney')}</span>
                                <img src={(config.file_server + this.props.currentUserInfo.invoice.attorney)} alt="" className="left" />
                                <div className="clear"></div>
                            </p>
                        </div>
                    </div>
                )
            }
        }
        else {
            return <p className="SecurityTopName"><span></span>{I18n.t('CompantInvoics.invoice.name')}</p>;
        }
    }
    render(){
        return(
            this.show()
        )
    }
}

// export default createContainer(() => {
//     Meteor.subscribe('buyer');
//     return {
//         currentUserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'invoice': 1}}),
//     };
// }, CompantInvoics);
