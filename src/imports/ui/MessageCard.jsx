import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Link, withRouter } from 'react-router';
import MessageCardBox from '../modules/messagecard/MessageCard.jsx';

class MessageCard extends Component{
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.router.setRouteLeaveHook(
            this.props.route,
            this.routerWillLeave
        )
    }
    routerWillLeave(nextLocation) {
        Meteor.call('message.readMessage');
    }
    render(){
        return <MessageCardBox />;
    }
}
export default withRouter(MessageCard);
