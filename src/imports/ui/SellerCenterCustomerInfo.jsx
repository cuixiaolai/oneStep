import React, { Component } from 'react';
import { Buyer } from '../api/Buyer.js';
import { Translate, I18n } from 'react-redux-i18n';
import { createContainer } from 'meteor/react-meteor-data';
import EditImg from '../modules/center/EditImg.jsx';
import config from '../config.json';
import SellerCenterCustomerInfoBox from '../modules/SellerCenter/SellerCenterCustomerInfoBox.jsx';


export default class SellerCenterCustomerInfo extends Component {
	constructor(props) {
		super(props);
		this.state = ({
			flags: true,
			img: null,
		});
	}
	clickOpen() {
		this.setState({flags: false});
	}
	clickClose() {
		this.setState({flags: true});
	}
	setImg(path) {
		this.setState({img: path});
		Meteor.call('buyer.updatePersonalIcon', path);
	}
	renderIcon() {
		if (this.state.img != null) {
			return (
				<div className="PerImg">
					<img src={(config.file_server + this.state.img)} alt="" style={{width:140,height:140}}/>
					<p onClick={this.clickOpen.bind(this)}>{I18n.t('PerCenterLeft.upload')}</p>
				</div>
			);
		}
		else if (this.props.UserInfo && this.props.UserInfo.icon) {
			return (
				<div className="PerImg">
					<img src={(config.file_server + this.props.UserInfo.icon)} alt="" style={{width:140,height:140}}/>
					<p onClick={this.clickOpen.bind(this)}>{I18n.t('PerCenterLeft.upload')}</p>
				</div>
			);
		}
		else {
			return (
				<div className="PerImg">
					<img src={(config.theme_path + 'centering.png')} alt="" style={{width:140,height:140}}/>
					<p onClick={this.clickOpen.bind(this)}>{I18n.t('PerCenterLeft.upload')}</p>
				</div>
			);
		}
	}
	renderEdit() {
		if (this.state.flags == false) {
			if (this.state.img != null) {
				return (
					<div>
						<div style={{position: 'fixed',top: 0,bottom: 0,left: 0,right: 0,background: 'rgba(0,0,0,.1)',zIndex: 101}}/>
						<EditImg click={this.clickClose.bind(this)} img={this.setImg.bind(this)} icon={this.state.img}/>
					</div>
				);
			}
			else if (this.props.UserInfo && this.props.UserInfo.icon) {
				return (
					<div>
						<div style={{position: 'fixed',top: 0,bottom: 0,left: 0,right: 0,background: 'rgba(0,0,0,.1)',zIndex: 101}}/>
						<EditImg click={this.clickClose.bind(this)} img={this.setImg.bind(this)} icon={this.props.UserInfo.icon}/>
					</div>
				);
			}
			else {
				return (
					<div>
						<div style={{position: 'fixed',top: 0,bottom: 0,left: 0,right: 0,background: 'rgba(0,0,0,.1)',zIndex: 101}}/>
						<EditImg click={this.clickClose.bind(this)} img={this.setImg.bind(this)}/>
					</div>
				);
			}
		}
	}
	componentWillMount() {
		if (this.props.UserInfo && this.props.UserInfo.icon) {
			this.setState({ img: this.props.UserInfo.icon });
		}
	}
	render() {
		return (
			<div>
				<div className="SellerCenterCustomerInfoBigBox  left">
					<SellerCenterCustomerInfoBox user_info={this.props.UserInfo}/>
				</div>
				<div className="right PerCenterLeft" style={{marginRight:100}}>
					{this.renderIcon()}
				</div>
				<div className="clear"></div>
				{this.renderEdit()}
			</div>
		);
	}
}
export default createContainer(() => {
	Meteor.subscribe('buyer');
	return {
		UserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'icon': 1, 'authentication': 1, 'invoice': 1, 'company_url': 1, 'company_detail': 1}}),
	};
}, SellerCenterCustomerInfo);
