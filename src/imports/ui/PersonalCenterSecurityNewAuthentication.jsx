import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Buyer } from '../api/Buyer.js';
// import { Users } from '../api/User.js';
import { Translate, I18n } from 'react-redux-i18n';
import SecurityTop from '../modules/center/SecurityTop.jsx';

export default class PersonalCenterSecurityNewAuthentication extends Component {
    show() {
        if(this.props.currentUser != null && this.props.currentUserInfo != null){
            let words={
                name: I18n.t('PersonalCenterSecurity.SecurityTopCom.name'),
                titleone: I18n.t('PersonalCenterSecurity.SecurityTopCom.titleone'),
                titletwo: I18n.t('PersonalCenterSecurity.SecurityTopCom.titletwo'),
                titlethree: I18n.t('PersonalCenterSecurity.SecurityTopCom.titlethree'),
            };
            return(
                <div>
                    <SecurityTop words={words} flag={false} company_name={this.props.currentUser.company}/>
                </div>
            )
        }
    }
    render() {
        return(
            <div>
                {this.show()}
            </div>
        )
    }
}

// export default createContainer(() => {
//     Meteor.subscribe('buyer');
//     Meteor.subscribe('users');
//     return {
//         currentUserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'authentication': 1}}),
//         currentUser: Users.findOne({_id: Meteor.userId()}, { fields: {'company': 1}})
//     };
// }, PersonalCenterSecurityNewAuthentication);