import React, { Component } from 'react';
import { Select, DatePicker, Modal, Button ,Input ,Menu, Dropdown, Icon,Form ,Tabs, Col } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import SellerCenterManageStockAll from '../modules/SellerCenter/SellerCenterManageStockAll.jsx';
import SellerCenterManageStockUpdate from '../modules/SellerCenter/SellerCenterManageStockUpdate.jsx';
import { createContainer } from 'meteor/react-meteor-data';
import { Stock } from '../api/Stock.js';
/*卖家中心-库存管理*/
const TabPane = Tabs.TabPane;
export default class SellerCenterManageStock extends Component {
	constructor(props) {
		super(props);
		this.state = ({
			flag:true,
			record:'',
			keys:[],
		});
	}
	callback(key) {
		console.log(key);
	}
	changeFlag(value){
		this.setState({flag: false, record: value});
	}
	Close(){
		this.setState({flag:true});
	}
	
	render() {
		let thisState = this.state;
		if(thisState.flag == false){
			let stock = Stock.findOne({_id: this.state.record});
			return (<SellerCenterManageStockUpdate stockInfo={stock} Close={this.Close.bind(this)} changeflag={this.changeFlag.bind(this)}/>)
		}else{
			return (
				<div className="stockTop">
					{/*库存管理*/}
					<div className="stockTab1">
						<Tabs onChange={this.callback.bind(this)}>
							<TabPane tab={I18n.t('PersonalCenter.manageStock.all')} key="1">
								{/*全部*/}
								<SellerCenterManageStockAll stockInfo={this.props.stockInfo} changeflag={this.changeFlag.bind(this)}/>
							</TabPane>
							<TabPane tab={I18n.t('PersonalCenter.manageStock.on')} key="2">
								{/*上架*/}
								<SellerCenterManageStockAll stockInfo={this.props.stockInfo.filter(item=>item.putaway)}  changeflag={this.changeFlag.bind(this)}/>
							</TabPane>
							<TabPane tab={I18n.t('PersonalCenter.manageStock.off')} key="3">
								{/*下架*/}
								<SellerCenterManageStockAll stockInfo={this.props.stockInfo.filter(item=>!item.putaway)}  changeflag={this.changeFlag.bind(this)}/>
							</TabPane>
						</Tabs>
					</div>
				</div>

			)
		}
	}
}

export default createContainer(() => {
	Meteor.subscribe('stock');
	return {
		stockInfo: Stock.find({user_id: Meteor.userId()}, {sort: {createAt: -1}}).fetch(),
	};
}, SellerCenterManageStock);