import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import SecurityTop from '../modules/center/SecurityTop.jsx'
import { Steps, Button } from 'antd';

export default class PersonalCenterSecurityPay extends Component {
    render(){
        let words={
            name:I18n.t('PersonalCenterSecurity.SecurityTopPay.name'),
            titleone:I18n.t('PersonalCenterSecurity.SecurityTopPay.titleone'),
            titletwo:I18n.t('PersonalCenterSecurity.SecurityTopPay.titletwo'),
            titlethree:I18n.t('PersonalCenterSecurity.SecurityTopPay.titleone'),
        };
        return(
            <div>
                <SecurityTop words={words}/>
            </div>
        )
    }
}
