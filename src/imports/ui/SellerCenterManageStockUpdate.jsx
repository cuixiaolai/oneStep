import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import {  Upload,Form,Input,Button} from 'antd';
const FormItem = Form.Item;
const createForm = Form.create;
import config from '../config.json';


let SellerCenterManageStockUpdate = React.createClass({
    handleChange(info){
        console.log(info.fileList);
        let word=info.file.name;
        this.setState({word:word});
    },
    getInitialState() {
        return {
            word:''
        };
    },
    render(){
        
        const props ={
            action:config.file_server + 'icon',
            onChange:this.handleChange,
        }
        return(
            <div className="SellerCenterUploadStockAdd">
                <div className="SellerCenterUploadStockAddBox">
                    <Form form={this.props.form} inline className="SellerCenterUploadStockForm">
                        <FormItem
                            required
                            label={I18n.t('buyerForm.type')}
                        >
                            <Input type="text" />

                        </FormItem>
                        <FormItem
                            required
                            label={I18n.t('buyerForm.com')}
                        >
                            <Input type="text" />
                        </FormItem>
                        <FormItem
                            required
                            label={I18n.t('buyerForm.PDF')}
                        >
                            <div className="UploadStockPDF">
                                {this.state.word}
                                <Upload {...props}>
                                    <div className="UploadStockPDF-btn">
                                        {I18n.t('buyerForm.searchfile')}
                                    </div>
                                </Upload>
                            </div>
                        </FormItem>
                        <FormItem>
                            <Button type="primary">{I18n.t('submit')}</Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
        )
    }
})
SellerCenterManageStockUpdate = createForm()(SellerCenterManageStockUpdate);
export default SellerCenterManageStockUpdate;
