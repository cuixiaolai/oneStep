import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Tabs,Icon } from 'antd';
const TabPane = Tabs.TabPane;
import config from '../config.json';
import Tablebox from '../modules/Table.jsx'
import { Translate, I18n } from 'react-redux-i18n';
export default class PersonalCenterIntegral extends Component {
    constructor(props) {

        super(props);
        this.state = ({
            current:1,
            flag:true,
        });
    }
    callback(key) {
        console.log(key);
    }
    render(){
        const columns = [{
            title: '日期',
            dataIndex: 'name',
        }, {
            title: '收入/支出',
            dataIndex: 'age',
            render(text) {
                if(text<=2){
                    return <a href="#" style={{color:'#5e9ad3'}}>{text+'(冻结中)'}</a>;
                }else if(text>=6){
                    return <a href="#" style={{color:'#ed7020'}}>{text}</a>;
                }else{
                    return <a href="#">{text}</a>;
                }
            },
        }, {
            title: '详细说明',
            dataIndex: 'address',
        },{
            title:'结余',
            dataIndex:'Balance',
        }
        ];
        const data = [];
        for (let i = 0; i < 46; i++) {
            data.push({
                key: i,
                name: `2016.9.10  17:45`,
                age: `+${i}`,
                address: `购买商品奖励阿尔法豆10个.s商品号${i}`,
                Balance:`${i+'0个'}`,
            });
        }
        const pagination = {
            total: data.length,
            showQuickJumper:true,
            pageSize:10,
            defaultPageSize:4,
            onChange(current) {
                console.log('Current: ', current);
            },
        };
        return(
            <div>
                <p className="SecurityTopName"><span></span>{I18n.t('Integral.name')}</p>
                <p className="Integral-top">
                    <div className="left">
                        {I18n.t('Integral.your')}
                        <span>111</span>
                    </div>
                    <div className="left">
                        {I18n.t('Integral.Frozen')}
                        <span>111</span>
                    </div>
                    <div className="right">
                        {I18n.t('Integral.overdue')}
                        <span>111</span>
                    </div>
                    <div className="clear"></div>
                </p>
                <div className="Integral">
                    <span style={{left:162}}></span>
                    <span style={{left:324}}></span>
                    <Tabs defaultActiveKey="1" onChange={this.callback}>
                        <TabPane tab="阿尔法豆明细" key="1">
                            <Tablebox  columns={columns} dataSource={data} pagination={pagination}/>
                        </TabPane>
                        <TabPane tab="收入阿尔法豆" key="2">
                            <Tablebox columns={columns} dataSource={data} pagination={pagination}/>
                        </TabPane>
                        <TabPane tab="支出阿尔法豆" key="3">
                            <Tablebox columns={columns} dataSource={data} pagination={pagination}/>
                        </TabPane>
                    </Tabs>
                    <div className="TableMessage">
                        <p>
                            <div className="left">
                                <img src={(config.theme_path+'origin03.png')} alt=""/>
                            </div>
                            <p className="left">
                                {I18n.t('Integral.messagename')}
                            </p>
                            <div className="clear"></div>
                        </p>
                        <ul>
                            <li>
                                <div className="left">
                                    <img src={(config.theme_path+'origin.png')} alt=""/>
                                </div>
                                <p className="left">
                                    {I18n.t('Integral.messone')}
                                </p>
                                <div className="clear"></div>
                            </li>
                            <li>
                                <div className="left">
                                    <img src={(config.theme_path+'origin.png')} alt=""/>
                                </div>
                                <p className="left">
                                    {I18n.t('Integral.messtwo')}
                                </p>
                                <div className="clear"></div>
                            </li>
                            <li>
                                <div className="left">
                                    <img src={(config.theme_path+'origin.png')} alt=""/>
                                </div>
                                <p className="left">
                                    {I18n.t('Integral.messthree')}
                                </p>
                                <div className="clear"></div>
                            </li>
                            <li>
                                <div className="left">
                                    <img src={(config.theme_path+'origin.png')} alt=""/>
                                </div>
                                <p className="left">
                                    {I18n.t('Integral.messfour')}
                                </p>
                                <div className="clear"></div>
                            </li>
                        </ul>
                    </div>
                    <div className="Integral-box">
                        <p>
                            {I18n.t('Integral.threemonth')}
                            <Icon type="down" />
                        </p>
                        <div className="Integral-boxInner">
                            <ul>
                                <li>{I18n.t('Integral.onemonth')}</li>
                                <li>{I18n.t('Integral.threemonth')}</li>
                                <li>{I18n.t('Integral.onryear')}</li>
                                <li>{I18n.t('Integral.year')}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
