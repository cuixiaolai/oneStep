import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import SellerClientInput from '../modules/SellerCenter/SellerClientInput';
import ClientRemarksBox from '../modules/SellerCenter/ClientRemarksBox';
import { Select, Table } from 'antd';
import {createContainer} from 'meteor/react-meteor-data';
import {Seller} from '../api/Seller.js';
import config from '../config.json';

const Option = Select.Option;


export default class SellerCenterClientInfo extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            hasClientinfo: true,
            remarksBoxDisplay: 'none',
            clientinfoIndex: '125px',
            remarksBoxText: '',
            remarksIndex: '',
            isCorpUser: true,
            searchValue:''
        });
        this.handleRemarksChange = this.handleRemarksChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
    }
    fixRemarks(text, record, index) {
        const offset = (index * 47) - 255 + 'px';
        this.setState({
            remarksBoxDisplay: 'block',
            clientinfoIndex: offset,
            remarksBoxText: text,
            remarksIndex: index
        });
    }
    handleSelectChange(value) {
        if (value == '企业用户') {
            this.setState({
                isCorpUser: true
            });
        } else {
            this.setState({
                isCorpUser: false
            });
        }
    }
    handleRemarksChange(text, index) {
        // const data = this.state.isCorpUser ? this.state.data1 : this.state.data2;
        // data[index] = text;
        if(this.state.isCorpUser){
            Meteor.call('seller.addCustomerRemarks',this.props.customerInfo.company_customer[index].customer_id, text, 'company', (err) => {
                if (err) {
                    console.log('c_e');
                } else {
                    console.log('c_g');
                }
            });
        }else{
            Meteor.call('seller.addCustomerRemarks',this.props.customerInfo.person_customer[index].customer_id, text, 'person', (err) => {
                if (err) {
                    console.log('p_e');
                } else {
                    console.log('p_g');
                }
            });
        }
        this.setState({
            // data: data,
            remarksBoxDisplay: 'none'
        });
    }
    onSearch(value){
        this.setState({
            searchValue: value
        });
    }
    render() {
        const data1 = [];
        const columns1 = [{
            title: '用户名',
            dataIndex: 'name',
        }, {
            title: '交易金额(￥)',
            dataIndex: 'amountRMB',
        }, {
            title: '交易金额($)',
            dataIndex: 'amountDollar',
        }, {
            title: '公司名称',
            dataIndex: 'corpName',
        }, {
            title: '联系电话',
            dataIndex: 'phone',
        }, {
            title: '邮箱',
            dataIndex: 'mail',
        },{
            title: '客户级别',
            dataIndex: 'grade',
        }, {
            title: '备注',
            dataIndex: 'remarks',
            render: (text, record, index) => (
                <span>
                    {record.remarks}
                    <button className="remarks-fixbtn" onClick={this.fixRemarks.bind(this,text,record,index)}>(修改)</button>
                </span>
            )
        }];
        const data2 = [];
        const columns2 = [{
            title: '用户名',
            dataIndex: 'name',
        }, {
            title: '交易金额(￥)',
            dataIndex: 'amountRMB',
        }, {
            title: '交易金额($)',
            dataIndex: 'amountDollar',
        }, {
            title: '联系人',
            dataIndex: 'contact',
        }, {
            title: '联系电话',
            dataIndex: 'phone',
        }, {
            title: '邮箱',
            dataIndex: 'mail',
        }, {
            title: '客户级别',
            dataIndex: 'grade',
        }, {
            title: '备注',
            dataIndex: 'remarks',
            render: (text, record, index) => (
                <span>
                    {record.remarks}
                    <button className="remarks-fixbtn" onClick={this.fixRemarks.bind(this,text,record,index)}>(修改)</button>
                </span>
            )
        }];
        if(this.props.customerInfo != null){
            if(this.state.isCorpUser && typeof this.props.customerInfo.company_customer != 'undefined'){
                for(let i in this.props.customerInfo.company_customer){
                    if(this.state.searchValue == '' || (typeof this.props.customerInfo.company_customer[i].user_name == 'undefined'?'':this.props.customerInfo.company_customer[i].user_name).indexOf(this.state.searchValue) != -1){
                        data1.push({
                            key: i.toString(),
                            name: this.props.customerInfo.company_customer[i].user_name,
                            amountRMB: this.props.customerInfo.company_customer[i].totalRMB,
                            amountDollar: this.props.customerInfo.company_customer[i].totalDollar,
                            corpName: this.props.customerInfo.company_customer[i].company_name,
                            phone: this.props.customerInfo.company_customer[i].phone,
                            mail: this.props.customerInfo.company_customer[i].emails,
                            grade: '铜牌',
                            remarks: (typeof this.props.customerInfo.company_customer[i].remarks == 'undefined' || this.props.customerInfo.company_customer[i].remarks == '')?'无备注':this.props.customerInfo.company_customer[i].remarks
                        });
                    }
                }
            }else if(!this.state.isCorpUser && typeof this.props.customerInfo.person_customer != 'undefined'){
                for(let i in this.props.customerInfo.person_customer){
                    if(this.state.searchValue == '' || (typeof this.props.customerInfo.person_customer[i].user_name == 'undefined'?'':this.props.customerInfo.person_customer[i].user_name).indexOf(this.state.searchValue) != -1) {
                        data2.push({
                            key: i.toString(),
                            name: this.props.customerInfo.person_customer[i].user_name,
                            amountRMB: this.props.customerInfo.person_customer[i].totalRMB,
                            amountDollar: this.props.customerInfo.person_customer[i].totalDollar,
                            contact: this.props.customerInfo.person_customer[i].contact_name,
                            phone: this.props.customerInfo.person_customer[i].phone,
                            mail: this.props.customerInfo.person_customer[i].emails,
                            grade: '铜牌',
                            remarks: (typeof this.props.customerInfo.person_customer[i].remarks == 'undefined' || this.props.customerInfo.person_customer[i].remarks == '') ? '无备注' : this.props.customerInfo.person_customer[i].remarks
                        });
                    }
                }
            }
        }
        return (
            <div>
                <div className="seller-clientinfo-ipt">
                    <SellerClientInput placeholder= {I18n.t('PerCenterRight.names')}
                                       onSearch={this.onSearch.bind(this)} style={{ width: 220 }}
                    />
                    <Select
                        className="seller-clientinfo-ipt-select"
                        defaultValue={I18n.t('buyerinfo.corpUser')}
                        style={{width: 150}}
                        onChange={this.handleSelectChange}>
                        <Option value={I18n.t('buyerinfo.personalUser')}>{I18n.t('buyerinfo.personalUser')}</Option>
                        <Option value={I18n.t('buyerinfo.corpUser')}>{I18n.t('buyerinfo.corpUser')}</Option>
                    </Select>
                </div>
                {(() => {if (this.state.hasClientinfo) {
                    if (this.state.isCorpUser) {
                        const pagination = {
                            total: data1.length,
                        };
                        return (
                            <Table
                                className="seller-clientinfo-table"
                                columns={columns1}
                                dataSource={data1}
                                bordered
                                pagination={pagination}
                            />
                        );
                    } else {
                        const pagination = {
                            total: data2.length,

                        };
                        return (
                            <Table
                                className="seller-clientinfo-table"
                                columns={columns2}
                                dataSource={data2}
                                bordered
                                pagination={pagination}
                            />
                        );
                    }
                } else {
                    return (
                    <div className="seller-clientinfo-nonebox">
                        <div className="seller-clientinfo-nonemsg">
                            <img src={(config.theme_path + 'noaddress.png')} alt=""/>
                            <div className="clientinfo-nonemsg-box">
                                <p className="font-24">{I18n.t('buyercenter.noClientinfo')}</p>
                                <p className="font-16">{I18n.t('buyercenter.tryToGetClient')}</p>
                            </div>
                        </div>
                    </div>
                    );
                }})()}
                <ClientRemarksBox
                    dis={{'display': this.state.remarksBoxDisplay, 'marginTop': this.state.clientinfoIndex}}
                    text={this.state.remarksBoxText}
                    index={this.state.remarksIndex}
                    handle={this.handleRemarksChange}
                />
            </div>
        );
    }
}
export default createContainer(() => {
    Meteor.subscribe('seller');
    return {
        customerInfo: Seller.findOne({_id: Meteor.userId()}),
    };
}, SellerCenterClientInfo);