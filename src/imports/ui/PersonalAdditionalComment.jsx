import React, {Component} from 'react';
import {Link, browserHistory} from 'react-router';
import {Translate, I18n} from 'react-redux-i18n';
import config from '../config.json';
import {Checkbox, Button, message, Input} from 'antd';
// import {Meteor} from 'meteor/meteor';
// import {createContainer} from 'meteor/react-meteor-data';
// import {Order} from '../api/Order.js';
// import {Comment} from '../api/Comment.js';

export default class PersonalAdditionalComment extends Component {
    constructor(props) {
        super(props);
    }

    showCommodityevaluation(product, comment, comment_time) {
        let temp = [];
        for (let i in product) {
            temp.push(
                <div className="noBorder Commodityevaluation">
                    <div className="left">
                        <img src={(config.theme_path + 'huo_03.jpg')} alt=""/>
                        <p style={{color: '#ed7020'}}>{product[i].product_id}</p>
                        <p>{I18n.t('evaluate.num')}{product[i].quantity}</p>
                    </div>
                    <div className="left evaluationInput" style={{position: 'relative'}}>
                        <div className="evaluationprogress">
                            <img src={(config.theme_path + 'jindu_03.png')} alt=""/>
                        </div>
                        <p>{I18n.t('evaluate.First')}
                            <span>{typeof comment_time == 'undifined' ? '' : comment_time.getFullYear() + '-' + (comment_time.getMonth() + 1) + '-' + comment_time.getDate()}</span>
                        </p>
                        <ul>
                            <li>{I18n.t('evaluate.name')}：{comment[i].customer_comment_product}</li>
                            <li>{I18n.t('evaluate.Serviceevaluation')}：{comment[i].customer_comment_service}</li>
                        </ul>
                        <p>{I18n.t('management.Additional')}</p>
                        <Input type="textarea" id="textarea" name="textarea" className="AdditionalComment"
                               maxLength={500} placeholder={I18n.t('management.say')}/>
                    </div>
                    <div className="clear"></div>
                </div>
            )
        }
        return temp
    }
    handleSubmit() {
        let comment = [];
        for (let i in this.props.orderInfo.product) {
            comment.push(document.getElementsByClassName('AdditionalComment')[i].value);
        }
        Meteor.call('order.customerAdditionalComment', this.props.orderInfo._id, comment, (err) => {
            if (err) {

            } else {
                browserHistory.replace('/personal_center/order');
            }
        });
    }
    render() {
        console.log(this.props);
        if (this.props.orderInfo != null && this.props.commentResult[0] != null) {
            console.log(this.props.orderInfo, this.props.commentResult);
            return (
                <div className="OrderBox">
                    <p>{I18n.t('management.Additional')}</p>
                    <div className="EvalualeBoxMessage">
                        <p>{I18n.t('evaluate.name')}</p>
                        {this.showCommodityevaluation(this.props.orderInfo.product, this.props.commentResult, this.props.orderInfo.comment_time)}
                        <Button onClick={this.handleSubmit.bind(this)}
                                type="primary">{I18n.t('evaluate.subnote')}</Button>
                    </div>
                </div>
            )
        } else {
            return (
                <div></div>
            )
        }

    }
}
// export default createContainer(() => {
//     Meteor.subscribe('order');
//     Meteor.subscribe('comment');
//     let order = Order.findOne({customer_id: Meteor.userId(), 'operation.customer_addition': true});
//     let comment = [];
//     if (order != null) {
//         for (let i in order.product) {
//             comment.push(Comment.findOne({_id: order.product[i].comment}));
//         }
//     }
//     return {
//         orderInfo: order,
//         commentResult: comment
//     };
// }, PersonalAdditionalComment);