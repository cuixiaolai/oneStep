import React, { Component } from 'react';
import { Link } from 'react-router';
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
import { Button, Form, Input,Select,Option ,Checkbox} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import HomeBox from '../modules/HomeBox.jsx';

export default class Home extends Component {
	render() {
		return (
				<HomeBox placeholder={I18n.t("home.input")}
						 onSearch={value => console.log(value)} style={{ width:700,height:40,borderRadius:4,position:'relative'}}/>
		);
	}
}
