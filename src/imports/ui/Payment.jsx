import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Buyer } from '../api/Buyer.js';
// import { Quote } from '../api/Quote.js';
import PaymentView from '../modules/payment/Payment.jsx';

export default class Payment extends Component {
	render() {
		return <PaymentView quoteInfo={this.props.quoteInfo} currentUserInfo={this.props.currentUserInfo} orderInfo={this.props.orderInfo} />
	}
}
// export default createContainer(() => {
// 	Meteor.subscribe('buyer');
//     Meteor.subscribe('quote');
// 	return {
// 		currentUserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'invoice': 1, 'address': 1, 'defaultAddress': 1}}),
//         quoteInfo: Quote.findOne({_id: Meteor.userId()}),
// 	};
// }, Payment);
