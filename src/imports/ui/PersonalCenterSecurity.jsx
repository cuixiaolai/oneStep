import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
import { Translate, I18n } from 'react-redux-i18n';
import SecurityCard from '../modules/center/SecurityCard.jsx'
import Securitylevel from '../modules/center/Securitylevel.jsx';
// import { Buyer } from '../api/Buyer.js';
// import { Users } from '../api/User.js';
import { Input} from 'antd';
import config from '../config.json'

export default class PersonalCenterSecurity extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            flag: true,
        });
    }
    safe(level){
        switch (level) {
            case 0:
                return I18n.t('PersonalCenterSecurity.top.very');
                break;
            case 1:
                return I18n.t('PersonalCenterSecurity.top.top');
                break;
            case 2:
                return I18n.t('PersonalCenterSecurity.top.ordinary');
                break;
            case 3:
                return I18n.t('PersonalCenterSecurity.top.commonly');
                break;
            case 4:
                return I18n.t('PersonalCenterSecurity.top.Unsafe');
                break;
            default:
                break;
        };
    };
	render() {
        let person_icon = '', person_name = '', person_phone = '', person_email = '';
        let level = 4;
        let password, phone, email, pay, authentication;
        if(this.props.currentUser && this.props.currentUserInfo) {
            //个人信息
            person_icon = this.props.currentUserInfo.icon == null ? config.theme_path + 'centering.png' : config.file_server + this.props.currentUserInfo.icon;
            // if (Roles.userIsInRole(Meteor.userId(), 'company')) {
                person_name = this.props.currentUser.company;
            // }
            // else if (Roles.userIsInRole(Meteor.userId(), 'person')){
            //     person_name = this.props.currentUserInfo.nickname == null ? '' : this.props.currentUserInfo.nickname;
            // }
            password={
                flag: true,
                pic: config.theme_path + 'true2.png',
                picname: I18n.t('PersonalCenterSecurity.card.verification'),
                name: I18n.t('PersonalCenterSecurity.card.password'),
                words: I18n.t('PersonalCenterSecurity.card.words'),
                modify: I18n.t('PersonalCenterSecurity.card.modify'),
                link: '/personal_center/password',
            };
            //个人手机参数
            person_phone = this.props.currentUser.phone == null ? I18n.t('PerCenterRight.noBind') : this.props.currentUser.phone;
            if (this.props.currentUser.phone != null) {
                let substr = person_phone.substring(4, 8);
                substr = substr.replace(/[^\n\r]/g, '*');
                person_phone = person_phone.substring(0, 4) + substr + person_phone.substring(8);
                level = level - 1;
                phone = {
                    flag: true,
                    pic: config.theme_path + 'true2.png',
                    picname: I18n.t('PersonalCenterSecurity.cardphone.verification'),
                    name: I18n.t('PersonalCenterSecurity.cardphone.name'),
                    words: I18n.t('PersonalCenterSecurity.cardphone.words') +  person_phone + I18n.t('PersonalCenterSecurity.email.wordstwo'),
                    modify: I18n.t('PersonalCenterSecurity.card.modify'),
                    link: '/personal_center/phone',
                };
            }
            else {
                phone = {
                    flag: false,
                    pic: config.theme_path + 'false.png',
                    picname: I18n.t('PersonalCenterSecurity.cardphone.verificationerror'),
                    name: I18n.t('PersonalCenterSecurity.cardphone.name'),
                    words: I18n.t('PersonalCenterSecurity.cardphone.wordserror'),
                    modify: I18n.t('PersonalCenterSecurity.cardphone.gomodify'),
                    link: '/personal_center/new_phone',
                };
            }
            //个人邮箱参数
            person_email = this.props.currentUser.emails == null ? I18n.t('PerCenterRight.noBind') : this.props.currentUser.emails[0].address;
            if (this.props.currentUser.emails != null) {
                let arr = person_email.split('@');
                if (arr[0] <= 4) {
                    let substr = arr[0].substring(1);
                    substr = substr.replace(/[^\n\r]/g, '*');
                    person_email = arr[0][0] + substr + '@' + arr[1];
                }
                else {
                    let begin = (arr[0].length - 4) % 2 === 0 ? (arr[0].length - 4) / 2 : (arr[0].length - 3) / 2
                    console.log(begin);
                    let substr = arr[0].substring(begin, begin + 4);
                    substr = substr.replace(/[^\n\r]/g, '*');
                    person_email = arr[0].substring(0, begin) + substr + arr[0].substring(begin + 4) + '@' + arr[1];
                }
                level = level - 1;
                email={
                    flag: true,
                    pic: config.theme_path + 'true2.png',
                    picname: I18n.t('PersonalCenterSecurity.cardphone.verification'),
                    name: I18n.t('PersonalCenterSecurity.email.name'),
                    words: I18n.t('PersonalCenterSecurity.email.words') + person_email + I18n.t('PersonalCenterSecurity.email.wordstwo'),
                    modify: I18n.t('PersonalCenterSecurity.card.modify'),
                    link: '/personal_center/email',
                };
            }
            else {
                email={
                    flag: false,
                    pic: config.theme_path + 'false.png',
                    picname: I18n.t('PersonalCenterSecurity.cardphone.verificationerror'),
                    name: I18n.t('PersonalCenterSecurity.email.name'),
                    words: I18n.t('PersonalCenterSecurity.email.wordserror'),
                    modify: I18n.t('PersonalCenterSecurity.cardphone.gomodify'),
                    link: '/personal_center/new_email',
                };
            }
            //支付密码参数
            if (this.props.currentUserInfo.pay != null) {
                pay={
                    flag: true,
                    pic: config.theme_path + 'true2.png',
                    picname: I18n.t('PersonalCenterSecurity.card.verification'),
                    name: I18n.t('PersonalCenterSecurity.pay.name'),
                    words: I18n.t('PersonalCenterSecurity.pay.words'),
                    modify: I18n.t('PersonalCenterSecurity.card.modify'),
                    link: '/personal_center/pay',
                };
                level = level - 1;
            }
            else {
                pay={
                    flag: false,
                    pic: config.theme_path + 'false.png',
                    picname: I18n.t('PersonalCenterSecurity.card.verificationerror'),
                    name: I18n.t('PersonalCenterSecurity.pay.name'),
                    words: I18n.t('PersonalCenterSecurity.pay.wordserror'),
                    modify: I18n.t('PersonalCenterSecurity.card.gomodify'),
                    link:'/personal_center/pay',
                };
            }
            身份审核参数
            // if (Roles.userIsInRole(Meteor.userId(), 'company')) {//企业实名认证
            //     if (this.props.currentUserInfo.authentication == null) {//未提交审核
            //         authentication={
            //             flag: false,
            //             pic: config.theme_path + 'authentication.png',
            //             picname: I18n.t('PersonalCenterSecurity.authentication.verificationerror'),
            //             name: I18n.t('PersonalCenterSecurity.authentication.comname'),
            //             words: I18n.t('PersonalCenterSecurity.authentication.wordserror'),
            //             modify: I18n.t('PersonalCenterSecurity.authentication.gomodify'),
            //             link: '/personal_center/authentication',
            //         };
            //     }
            //     else if (this.props.currentUserInfo.authentication.verify == false){//等待审核
            //         authentication={
            //             flag: false,
            //             pic: config.theme_path + 'authentication.png',
            //             picname: I18n.t('PersonalCenterSecurity.authentication.verificationwait'),
            //             name: I18n.t('PersonalCenterSecurity.authentication.comname'),
            //             words: I18n.t('PersonalCenterSecurity.authentication.wordserror'),
            //             modify: I18n.t('PersonalCenterSecurity.authentication.wait'),
            //             link: '/personal_center/authentication',
            //         };
            //     }
            //     else if (this.props.currentUserInfo.authentication.success == true){//审核成功
            //         authentication={
            //             flag: true,
            //             pic: config.theme_path + 'true2.png',
            //             picname: I18n.t('PersonalCenterSecurity.authentication.verification'),
            //             name: I18n.t('PersonalCenterSecurity.authentication.comname'),
            //             words: I18n.t('PersonalCenterSecurity.authentication.words'),
            //             modify: I18n.t('PersonalCenterSecurity.authentication.modify'),
            //             link: '/personal_center/authentication',
            //         };
            //         level = level - 1;
            //     }
            //     else {//审核失败
            //         authentication={
            //             flag: true,
            //             pic: config.theme_path + 'rederror.png',
            //             picname: I18n.t('PersonalCenterSecurity.authentication.verification'),
            //             name: I18n.t('PersonalCenterSecurity.authentication.comname'),
            //             words: I18n.t('PersonalCenterSecurity.authentication.words'),
            //             modify: I18n.t('PersonalCenterSecurity.authentication.click'),
            //             link: '/personal_center/authentication',
            //         };
            //     }
            // }
            // else if(Roles.userIsInRole(Meteor.userId(), 'person')){//个人实名认证
            //     if (this.props.currentUserInfo.authentication == null) {//未提交审核
            //         authentication={
            //             flag: false,
            //             pic: config.theme_path + 'authentication.png',
            //             picname: I18n.t('PersonalCenterSecurity.authentication.verificationerror'),
            //             name: I18n.t('PersonalCenterSecurity.authentication.name'),
            //             words: I18n.t('PersonalCenterSecurity.authentication.wordserror'),
            //             modify: I18n.t('PersonalCenterSecurity.authentication.gomodify'),
            //             link: '/personal_center/authentication',
            //         };
            //     }
            //     else if (this.props.currentUserInfo.authentication.verify == false){//等待审核
            //         authentication={
            //             flag: false,
            //             pic: config.theme_path + 'authentication.png',
            //             picname: I18n.t('PersonalCenterSecurity.authentication.verificationwait'),
            //             name: I18n.t('PersonalCenterSecurity.authentication.name'),
            //             words: I18n.t('PersonalCenterSecurity.authentication.wordserror'),
            //             modify: I18n.t('PersonalCenterSecurity.authentication.wait'),
            //             link: '/personal_center/authentication',
            //         };
            //     }
            //     else if (this.props.currentUserInfo.authentication.success == true){//审核成功
            //         authentication={
            //             flag: true,
            //             pic: config.theme_path + 'true2.png',
            //             picname: I18n.t('PersonalCenterSecurity.authentication.verification'),
            //             name: I18n.t('PersonalCenterSecurity.authentication.name'),
            //             words: I18n.t('PersonalCenterSecurity.authentication.words'),
            //             modify: I18n.t('PersonalCenterSecurity.authentication.modify'),
            //             link: '/personal_center/authentication',
            //         };
            //         level = level - 1;
            //     }
            //     else {//审核失败
            //         authentication={
            //             flag: true,
            //             pic: config.theme_path + 'rederror.png',
            //             picname: I18n.t('PersonalCenterSecurity.authentication.verification'),
            //             name: I18n.t('PersonalCenterSecurity.authentication.name'),
            //             words: I18n.t('PersonalCenterSecurity.authentication.words'),
            //             modify: I18n.t('PersonalCenterSecurity.authentication.click'),
            //             link: '/personal_center/authentication',
            //         };
            //     }
            // }
            return (
                <div>
                    <div className="PersonalCenterSecurity">
                        <div className="PersonalCenterSecurity-top">
                            <div className="PersonalCenterSecurity-toppic left">
                                <img src={person_icon} alt="" style={{width:170,height:170,borderRadius:'50%',marginLeft:30,marginTop:30,marginRight:30}} />
                            </div>
                            <div className="PersonalCenterSecurity-topwords left">
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.top.name')}</span>
                                    <span>{person_name}</span>
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.top.phone')}</span>
                                    <span>{person_phone}</span>
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.top.email')}</span>
                                    <span>{person_email}</span>
                                </p>
                            </div>
                            <div className="PersonalCenterSecurity-topsec right">
                                <p>{I18n.t('PersonalCenterSecurity.top.security')}</p>
                                <Securitylevel num={level} width='250'/>
                                <p>{this.safe(level)}</p>
                            </div>
                            <div className="clear"></div>
                        </div>
                        <SecurityCard word={password} />
                        <SecurityCard word={phone} />
                        <SecurityCard word={email} />
                        <SecurityCard word={pay} type="security" size="2" />
                        <SecurityCard word={authentication} />
                    </div>
                </div>
            );
        }
        else {
            return (
                <div>
                    <div className="PersonalCenterSecurity">
                        <div className="PersonalCenterSecurity-top">
                            <div className="PersonalCenterSecurity-toppic left">
                                <img alt="" style={{width:170,height:170,borderRadius:'50%',marginLeft:30,marginTop:30,marginRight:30}} />
                            </div>
                            <div className="PersonalCenterSecurity-topwords left">
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.top.name')}</span>
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.top.phone')}</span>
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.top.email')}</span>
                                </p>
                            </div>
                            <div className="PersonalCenterSecurity-topsec right">
                                <p>{I18n.t('PersonalCenterSecurity.top.security')}</p>
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>
                </div>
            );
        }
	}
}

// export default createContainer(() => {
//     Meteor.subscribe('buyer');
//     Meteor.subscribe('users');
//     return {
//         currentUserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'nickname': 1, 'icon': 1, 'pay': 1, 'authentication': 1}}),
//         currentUser: Users.findOne({_id: Meteor.userId()}, { fields: {'emails': 1, 'phone': 1, 'company': 1}}),
//     };
// }, PersonalCenterSecurity);
