import React, { Component } from 'react';
import { Select, Dropdown, DatePicker, Modal, Button ,Input ,Menu, Icon,Form ,Tabs, Col ,Checkbox ,Steps} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import LogisticsDetailsContent from '../modules/order/LogisticsDetailsContent.jsx';
/*我的订单-物流详情*/
export default class LogisticsDetails extends Component {
    constructor(props) {
        super(props);
        this.state = ({

        });
    }
    render() {
        return (
            <div className="logisticsdetails">
                <LogisticsDetailsContent />
            </div>
        );
    }
}
