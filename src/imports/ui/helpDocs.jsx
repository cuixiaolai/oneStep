import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Meteor } from 'meteor/meteor'
import { Button } from 'antd';
import config from '../config.json';

export default class helpDocs extends Component {
    render(){
        if( this.props.path == 11){ //
            return(
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>购物流程</p>
                        <div className="AgreementBoxInner">
                            <p className="name">一、 如何查看我的订单？</p>
                        </div>
                        <Button onClick={this.props.click} type="primary">关闭</Button>
                    </div>
                </div>
            )
        }else if(this.props.path == 2){
            return(
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>用户信用</p>
                        <div className="AgreementBoxInner" >

                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('Agreement.Useragree')}</Button>
                    </div>
                </div>
            )
        }
        else if(this.props.path == 21){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>ICxyz购物流程</p>
                        <div className="AgreementBoxInner">
                            <p>注册-查找商品-查看商品-立即购买/加入购物车-填写并核对订单信息-提交订单</p>
                            <p>注册打开ICxyz首页，在左上方，点击“快速注册”按钮；</p>
                            <img src={(config.theme_path + 'shopping_process_1.png')} alt=""/>
                            <p>1.选择“个人注册”，进入个人注册页面</p>
                            <img src={(config.theme_path + 'shopping_process_2.png')} alt=""/>
                            <img src={(config.theme_path + 'shopping_process_3.png')} alt=""/>
                            <p>2.进入个人信息注册页面，请根据提示信息完成注册  </p>
                            <p>邮箱注册与手机号注册两种方式</p>
                            <img src={(config.theme_path + 'shopping_process_4.png')} alt=""/>
                            <p>设置密码</p>
                            <img src={(config.theme_path + 'shopping_process_5.png')} alt=""/>
                            <p>注册成功</p>
                            <p>3.查找商品</p>
                            <img src={(config.theme_path + 'shopping_process_6.png')} alt=""/>
                            <p>4.查看商品具体信息</p>
                            <img src={(config.theme_path + 'shopping_process_7.png')} alt=""/>
                            <img src={(config.theme_path + 'shopping_process_8.png')} alt=""/>
                            <img src={(config.theme_path + 'shopping_process_9.png')} alt=""/>
                            <p>5立即购买/加入购物车</p>
                            <p>5.1点击立即购买弹出的页面</p>
                            <img src={(config.theme_path + 'shopping_process_10.png')} alt=""/>
                            <p>5.2点击购物车弹出的页面</p>
                            <img src={(config.theme_path + 'shopping_process_11.png')} alt=""/>
                            <p>6.填写并核对订单信息</p>
                            <img src={(config.theme_path + 'shopping_process_12.png')} alt=""/>
                            <img src={(config.theme_path + 'shopping_process_13.png')} alt=""/>
                            <p>7.付款与发货</p>
                            <p>用户提交订单，线下付款，卖家在买家付款完毕后即可安排发货。</p>
                            <p>8.确认收货</p>
                            <p>买家在检查收到的商品无误后，登录ICxyz商城账号，进行确认收货。</p>
                            <p></p>
                            <p></p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }



        else if(this.props.path == 22){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>用户信用</p>
                        <div className="AgreementBoxInner">
                            <p>文档正在火速赶来，请稍后</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }
        else if(this.props.path == 23) {
            return(
                <div className="AgreementShow" style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox" >
                        <p>订单相关</p>
                        <div className="AgreementBoxInner">
                        <p className="name">一、 如何查看我的订单？</p>
                        <p>点击平台上方用户中心—我的订单，就可以显示我的订单</p>
                        <img src={(config.theme_path + 'orderone.png')} alt=""/>
                        <p className="name">购物与订单相关问题</p>
                        <p>Q1:为什么购物车中显示有货，去结算时无货？</p>
                        <p>A:1.可能因为抢购（特别是紧俏的商品），没有抢购到，若别的地区还有货，修改收货地</p>
                        <p>区后，请尝试重新购买；</p>
                        <p>  2.赠品在购物车中没显示库存信息，结算时赠品如果显示无货，可以回到购物车页中删除赠品后继续下单。</p>
                        <p>Q2:为什么加入购物车后，去结算时价格发生变化?</p>
                        <p>A:1.因为商品在促销时可能优惠力度比较大，造成抢购数超过限购数量，或促销结束时间</p>
                        <p>  到了，所以可能会出现结算时价格变高或变低；</p>
                        <p>Q3:如何向店铺咨询问题?</p>
                        <p>A:1.购物车页中店铺名称右边有个客服入口，您可以点击“联系客服”与店铺客服交流；或者拨打店铺客服电话</p>
                        <p>Q4:商品丢失，或无法加入购物车?</p>
                        <p>A:1.已下柜商品会自动消失；</p>
                        <p>2.购物车的主商品上限是70个，达到70个时将无法再加入，可以把暂时不需要购买的商品移到我的关注，</p>
                        <p>待需要时再加入到购物车中；</p>
                        <p>3.请在登陆状态下添加商品，以便保存到您的账号中，方便您跨终端（手机、平板电脑</p>
                        <p>等）浏览和购物。</p>
                        <p>Q5:为什么结算时无法货到付款</p>
                        <p>A:1.您所勾选的商品可能因为配送地区或个别商品不支持货到付款，若出现该情况，请咨</p>
                        <p>询店铺客服或选择支持货到付款的商品一起支付。</p>
                        <p>Q6:在结算页无法使用优惠券?</p>
                        <p>A:1.提交去结算中的商品所属不同店铺，有些优惠券无法通用，建议您把需要购买的商品</p>
                        <p>都选择是同一个店铺，一次提交一个店铺的商品去结算，该店铺下的店铺优惠券方可</p>
                        <p>使用；有的品类券不支持别的品类商品，所以不能使用；</p>
                        <p>Q7:库存状态显示为 “预订”的意思?</p>
                        <p>A:1.预订表示库存暂时无货，但厂商会尽快生产和供应，客户可以预订，一旦有货就会马</p>
                        <p>上发货；预订商品的到货时间请以页面显示为准或联系客服确认；</p>
                        <p>Q8:如何进行促销活动的凑单（如：满减、满赠、换购）?</p>
                        <p>A:1.总价类促销活动多有 【去凑单】 的操作，凑单可以获得更多的实惠；如果某些商品</p>
                        <p>在同一个店铺中都有，建议在同一店铺里购买，可能会节省运费和少接收快递的麻烦；</p>
                        <p>可以点击店铺名称去店铺中找需要的商品。</p>
                        <p>Q9:页面响应慢或显示异常?</p>
                        <p>A:1.浏览器的兼容性问题，可能会影响顺畅购物，本网推荐使用火狐、谷歌浏览器；</p>
                        <p>Q10:为什么运费算了多份?</p>
                        <p>A:1.您所勾选的商品可能不是同一个店铺的，或有些店铺没有包邮门槛，店铺配送的商品</p>
                        <p>价格未达到门槛等都有可能，建议在同一个店铺里购买更多的东西，节省运费，减少收</p>
                        <p>取快递的麻烦。</p>
                        <p>Q11:如何反馈意见或建议?</p>
                        <p>A:1.有意见或建议，欢迎拨打4008055955，谢谢 ！</p>
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }else if(this.props.path == 24){
            return(
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>谨防假借ICxyz名义的诈骗</p>
                        <div className="AgreementBoxInner">
                            <p>一．《诈骗预防必备手册》</p>
                            <p>1、 ICxyz平台及平台商家不会存在订单卡单、无效等情况；客服人员不会通过私人手机号码、400电话或</p>
                            <p>短信形式告知您订单异常，更不会引导您ATM转账、QQ提供退款链接等操作。</p>
                            <p>2、 订单实际状态请您以“我的订单”页面的交易状态为准。 如需查看订单、取消订单请点击“我的订单”</p>
                            <p>进行操作。</p>
                            <p>3、 ICxyz平台网址是www.196.168.1.204.com ，平台所有页面网址均以连续的“.待定.com ”构成，如：</p>
                            <p>***.待定.com**（如下图），尤其是域名中待定前后的分隔号“.”，无此分隔号的链接，均为钓鱼网站，</p>
                            <p>请用户仔细辨别。</p>
                            <img src={(config.theme_path + 'order_safe_1.png')} alt=""/>
                            <p>4、 拒绝向陌生人转账、代付款项，不要在陌生链接中付款，不要接受陌生文件或随意扫描陌生二维码付款。</p>
                            <p>5、 不要轻信客服、QQ、短信、邮件等传送的非法钓鱼链接，通过前述方式并以阿尔法贝特名义要求您点</p>
                            <p>击链接操作退款、转账或者招聘代刷信誉等均属于诈骗活动。</p>
                            <p>6、 任何一个验证码有可能和您的款项相关，请勿随意告知他人。</p>
                            <p>7、 任何涉及款项、个人信息等相关操作请第一时间联系官方客服核实确认。</p>
                            <p>三．防受骗小知识：</p>
                            <p>•用户在网上购物时，不要轻易添加任何陌生人的聊天好友。</p>
                            <p>•用户网购时一定要选择正规的网站，这样交易才有保障。</p>
                            <p>•切勿相信网页或聊天工具中弹出的代刷信誉赚钱的广告！京东明确禁止商家存在恶意刷量的行为，不允许</p>
                            <p>商家炒作店铺信誉，一经发现，必将严惩不贷。</p>
                            <p>二．具体案例说明</p>
                            <p>案例一  伪阿尔法贝特链接骗术</p>
                            <p>近日张先生接到一个自称“ICxyz商城异常订单处理中心”的电话，电话号码显示：1373920479，自称“ICxyz客服人员”的骗子告诉张先生，他在京东购买的订单商品无法发货了，需要给张先生退款，然后骗子加了张先生的QQ发送了名为“ICxyz退款平台“的钓鱼网站：//icxyz.flanbz.com链接， 并告知张先生填写银行卡信息后才可以退款，张先生立即联系了ICxyz客服电话进行确认订单是否出现异常，得知订单并无异常后，张先生长叹一口气，幸好及时联系客服人员确认，否则后果不堪设想！</p>
                            <p>ICxyz在此提醒您：ICxyz退款不会向您提供链接，要求客户在链接页面中输入任何信息的，请您妥善保管自己的财产信息。</p>
                            <p>案例二 伪ICxyz客服外拨号码骗术</p>
                            <p>前不久王女士接到一个自称“ICxyz礼品中心”的电话，电话号码显示为400-666-5560；对方自称 “京东客服人员“，并称由于王女士一直支持ICxyz，为了答谢王女士，京东会邮寄赠送一张八折卡，需要用户签收时支付运费。王女士回复骗子：“ICxyz从来没有以你这个号码给我打过电话，官网快报都说了，不会用400电话外拨用户的，你这个骗子也太低级了吧！”</p>
                            <p>ICxyz在此提醒您：遇到此情况可以立即联系ICxyz客服来确认。</p>
                            <p>案例三 微信骗术新花样</p>
                            <p>张女士通过微信朋友圈，查看到有“Icxyz分享拿0元元器件”活动，点击进入后查看公众号显示为个人，非Icxyz官方信息，此时张女士提高了警惕，并未继续操作，避免了损失。</p>
                            <p>ICxyz在此提醒您：您在打开以ICxyz名义宣传的公众号，点击查看微信账户主体信息。如账户主体并非微信端认证的ICxyz公众号，则该活动非ICxyz商城活动。</p>
                            <p>如让您填写身份证，姓名，联系方式等信息，还请您提高警惕。</p>
                            <p>案例四、刷单骗术新花样 </p>
                            <p>用户李女士通过QQ聊天认识了QQ名称为“***刷单群”</p>
                            <p>李女士加了此QQ号后，对方让其微信扫码前后支付了3笔电路板订单；对方告诉李女士不用担心，刷单完成后会将所有已支付款连同返利一起退给客户，李女士如实操作，把刷单注册的ICxyz账户和密码一起给了对方，提供后发现QQ消息发送不出去，骗子已经把李女士QQ拉入黑名单；用户李女士总计损失***元。</p>
                            <p>ICxyz在此提醒您： “刷单”是严重影响公平公正交易的行为，ICxyz不存在也不允许任何形式的刷单,如遇相关机构或个人以刷单的名义让您付款，全部为诈骗行为。还请您提高警惕。</p>
                            <p>案例五、“代购”新骗术</p>
                            <p>孙先生在QQ上结识一个“ICxyz代购”的聊友，对方表示自己有“ICxyz特殊优惠券”可以在ICxyz为其下单，比现价格更加优惠，孙先生同意骗子为其代购，骗子将下单后物流信息截图发送给孙先生，孙先生看到截图里订单收件信息是自己的，并且货物就快送货了就将款项支付宝转账给骗子；随后骗子转而拨打客服电话声称自己银行卡被盗刷了要求ICxyz拦截取消订单。</p>
                            <p>ICxyz在此提醒您： 代购、代付有风险，建议您注册ICxyz账号或通过自有ICxyz账号购买商品!</p>
                            <p></p>
                            <p></p>
                            <p></p>
                            <p></p>
                            <p></p>
                            <p></p>
                            <p></p>
                            <p></p>
                            <p></p>

                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }
        else if(this.props.path == 31){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>公司转账</p>
                        <div className="AgreementBoxInner">
                            <p>点击“企业账户”进入企业账户页面，选择所需银行点击后，在快钱企业账户支付页面完成付款</p>
                            <img src={(config.theme_path + 'company_transfer_1.png')} alt=""/>
                            <img src={(config.theme_path + 'company_transfer_2.png')} alt=""/>
                            <p>付款完成点击“已完成支付”确认</p>
                            <img src={(config.theme_path + 'company_transfer_3.png')} alt=""/>
                            <p></p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }

        else if(this.props.path == 32){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>在线支付</p>
                        <div className="AgreementBoxInner">
                            <p>1.在线支付支持那些银行卡？</p>
                            <p>目前Icxyz商城在线支付支持国内大部分银行，部分地方性银行暂时不支持，具体还请提交在线支付订单选择为准。</p>
                            <p>2.如何到银行修改我开通的快捷支付银行卡的银行预留手机号？</p>
                            <p>为保证您的支付信息安全，京东无法帮您修改，建议您到发卡行柜台办理：</p>
                            <p>（1）携带身份证及银行卡前去银行柜台网点，告知银行柜台工作人员需要修改客户信息中的手机号码；</p>
                            <p>（2）办理银行“客户信息维护”业务，填写“特殊业务凭证”；</p>
                            <p>（3）您可以新增客户手机号码或者修改原来的手机号码，在您新增或者修改结束后，需要告知银行柜台工作人员将其设置为默认手机号码。如柜台工作人员告知手机号正确，则需要让柜员删除后，重新录入才能生效。</p>
                            <p>3.什么是快捷支付？</p>
                            <p>快捷支付是京东联合支付公司推出的支付服务。持卡人只要拥有银行卡，首次使用时输入相应卡信息、以及身份信息等信息并验证成功，即可完成支付；第二次使用该银行卡，输入支付密码或短信验证码即可一步付款。</p>
                            <p>4.如何使用网银支付？</p>
                            <img src={(config.theme_path + 'online_payment_1.png')} alt=""/>
                            <p></p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }


        else if(this.props.path == 33){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>第三方支付</p>
                        <div className="AgreementBoxInner">
                            <p>点击“企业账户”进入企业账户页面，选择所需银行点击后，在快钱企业账户支付页面完成付款</p>
                            <img src={(config.theme_path + 'third_party_payment_1.png')} alt=""/>
                            <img src={(config.theme_path + 'third_party_payment_2.png')} alt=""/>
                            <p>付款完成点击“已完成支付”确认</p>
                            <img src={(config.theme_path + 'third_party_payment_3.png')} alt=""/>
                            <p></p>

                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }


        else if(this.props.path == 34){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>支付安全</p>
                        <div className="AgreementBoxInner">
                            <p>文档正在火速赶来，请稍后</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }

        else if(this.props.path == 35){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>注意事项</p>
                        <div className="AgreementBoxInner">
                            <p>文档正在火速赶来，请稍后</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }

        else if(this.props.path == 41){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>国内交货</p>
                        <div className="AgreementBoxInner">
                            <p>文档正在火速赶来，请稍后</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }



        else if(this.props.path == 42){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>港澳台/保税区交货</p>
                        <div className="AgreementBoxInner">
                            <p>文档正在火速赶来，请稍后</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }



        else if(this.props.path == 43){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>运输说明</p>
                        <div className="AgreementBoxInner">
                            <p>文档正在火速赶来，请稍后</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }
        else if(this.props.path == 44){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>报关说明</p>
                        <div className="AgreementBoxInner">
                            <p>文档正在火速赶来，请稍后</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }
        else if(this.props.path == 45){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>货币关联规则</p>
                        <div className="AgreementBoxInner">
                            <p>文档正在火速赶来，请稍后</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }
        else if(this.props.path == 51){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>发票信息</p>
                        <div className="AgreementBoxInner">
                            <p>1.上传发票资质</p>
                            <p>1.1 公司主页中左上角直接点击用户名，进入企业中心</p>
                            <img src={(config.theme_path + 'invoice_info_1.png')} alt=""/>
                            <p>1.2点击左侧导视栏中“发票管理”</p>
                            <img src={(config.theme_path + 'invoice_info_2.png')} alt=""/>
                            <p>1.3根据提示填写发票信息</p>
                            <img src={(config.theme_path + 'invoice_info_3.png')} alt=""/>
                            <p>1.4上传资质文件</p>
                            <img src={(config.theme_path + 'invoice_info_4.png')} alt=""/>
                            <img src={(config.theme_path + 'invoice_info_5.png')} alt=""/>
                            <p>1.5 等待审核</p>
                            <p>发票分为电子发票和纸质发票两种。电子发票通过邮箱发送。</p>
                            <p>2.如何设置发票信息？</p>
                            <p>2.1确认购买商品无误后点击立即购买</p>
                            <img src={(config.theme_path + 'invoice_info_6.png')} alt=""/>
                            <p>2.2 什么是电子发票？</p>
                            <p>电子发票是指在购销商品、提供或者接受服务以及从事其他经营活动中，开具、收取的以电子方式存储的收付款凭证，京东开具的电子发票均为真实有效的合法发票，与传统纸质发票具有同等法律效力，可作为用户维权、保修的有效凭据。</p>
                            <p>2.3 遇到质量问题，电子发票是否能作为维权的依据？</p>
                            <p>根据北京市国家税务局、北京市地方税务局、北京市商务委员会、北京市工商行政管理局《关于电子发票应用试点若干事项的公告》（2013年第8号）、《关于扩大电子发票应用试点范围若干事项的公告》（2013年第18号文）；上海市国家税务局、上海市商务委员会、上海市发展和改革委员会、上海市财政局上海市工商行政管理局《关于上海市电子发票应用试点的公告》（2013年第6号文）；成都事国家税务局、成都市商务局、成都市发展和改革委员会、成都市财政局、成都市工商行政管理局《关于成都市电子发票应用试点若干事项的公告》（2014年第1号文）；广州市国家税务局、广州市发展和改革委员会、广州市经济贸易委员会、广州市财政局、广州市对外贸易经济合作局、广州市人民政府国有资产监督管理委员会、广州市工商行政管理局、广州市地方税务局《关于广州市电子发票应用试点的公告》（2014年第5号文），电子发票与纸质发票具有相同法律效力，可作为售后、维权凭据。</p>
                            <p>2.4单位购买可以开电子发票吗？</p>
                            <p>电子发票是税务局认可的有效凭证，单位可以选择开具电子发票，可以报销入账，其法律效力、基本用途及使用规定同纸质发票，如需纸质票可自行下载打印。</p>
                            <p>2.5 选择开具电子发票的顾客申请部分退货，发票如何处理？</p>
                            <p>开具电子发票的订单申请部分退货，原电子发票会通过系统自动冲红（原电子发票显示无效），并对未发生退货的商品重新自动开具电子发票。如整单退货，则我司将原电子发票做冲红处理（原电子发票显示无效）。</p>
                            <p>2.6 如果选择了电子发票，后来又想要纸质的发票怎么办？</p>
                            <p>电子发票与纸质普票具有相同法律效力，是售后维修、用户维权的有效凭据，个人用户选择开具电子发票，若后期需要换取纸质普票，需要承担发票邮寄的运费，因此请谨慎选择发票类型。若涉及相关电子发票更换问题，请直接联系客服处理即可。</p>
                            <p>注意：1、发票换开时效：电子发票（个人抬头）换开纸质发票（个人抬头）可在30天内换开；电子发票（个人抬头）换开纸质发票（公司抬头）可在60天内换开。2、发票开具时效：财务接到通知后3-5个工作日开具并寄出。</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }
        else if(this.props.path == 52){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>交易纠纷处理总则</p>
                        <div className="AgreementBoxInner">
                            <p>第一章：概述</p>
                            <p>1.1适用范围：本规则适用于ICxyz开放平台商家。</p>
                            <p>1.2目的和依据：ICxyz秉承“客户为先，诚信至上”的宗旨、“让购物变得简单，快乐！”为我们的最终目标，为广大用户提供最优质的商品及服务。为保障ICxyz用户合法权益，维护ICxyz正常经营秩序，根据《ICxyz用户注册协议》、《“ICxyz.COM”开放平台店铺服务协议》等服务/合作协议，制定本规则。</p>
                            <p>1.3定义</p>
                            <p>【纠纷申请单】 用户购买商家的商品时,产生了售前或售后问题, 申请由ICxyz介入处理的单据凭证。</p>
                            <p>【交易纠纷率】当月由ICxyz介入处理的，最终判处为商家责任的纠纷申请单量+双方责任的纠纷申请单量-申诉通过的纠纷申请单量，与商家当月实际订单量的比值。</p>
                            <p>1.4ICxyz并非司法机关，亦非专业的纠纷解决机构，ICxyz对于商家及买家之间纠纷的处理完全是基于相关法规的规定、协议的约定及买卖双方的意愿，ICxyz仅能以普通非专业人士的知识水平和能力对买家和商家提交的相关证据材料进行鉴别和认定，ICxyz对据此作出的交易纠纷处理结果等无法保证完全正确，也不对此承担任何责任。</p>
                            <p>1.5处理纠纷期间，ICxyz有权通过交易纠纷端口、电子邮件、短信或电话等方式向买卖双方发送的与纠纷处理相关的提示或通知。</p>
                            <p>第二章：交易</p>
                            <p>2.1.商家交付给买家的商品应当符合法律法规的相关规定，且所售商品不得违反ICxyz开放平台总则中关于发布违禁信息、出售假冒商品、滥发信息、假冒材质成分、出售未经报关进口商品、发布非约定商品等条款的相关规定。</p>
                            <p>2.2.商家应当对其所售商品进行如实描述，即应当在商品描述页面、店铺页面等宣传页面及ICxyz沟通工具等所有ICxyz提供的沟通渠道中，对商品的基本属性、成色、瑕疵等必须说明的信息进行真实、完整的描述。</p>
                            <p>2.3.商家应当对其所售商品质量承担保证责任，即保证其交付给买家的商品在质保期内可以正常使用，包括商品不存在危及人身财产安全的不合理危险、具备商品应当具备的使用性能、符合商品或其包装上注明采用的标准等。</p>
                            <p>2.4.商品属于“三包”范围内的，买家要求商家履行退货、换货或维修义务的，商家应当按照买家的要求提供相应服务。</p>
                            <p>2.5.商品符合七天无理由退货的，商家应积极处理买家的退货申请并提供相应服务。</p>
                            <p>2.6商家应积极响应并处理买家的售后问题，并ICxyz有权对交易纠纷率严重超过行业平均水平（“交易纠纷率行业平均水平”可登陆店铺首页“店铺综合评分”—“交易纠纷率”中查看）的店铺采取包括但不限于限制参加ICxyz平台营销活动、暂停向商家提供服务及终止合作等措施。</p>
                            <p>第三章：举证责任</p>
                            <p>3.1.买卖双方申请ICxyz介入纠纷处理后的，在纠纷处理过程中，ICxyz有权要求买家或商家提供证明依据，且有权以普通非专业人士的知识水平和能力对买家和商家提交的相关证据材料进行鉴别和认定。</p>
                            <p>3.2针对申请ICxyz介入、ICxyz受理的各类型争议所需提供的证明文件，以ICxyz要求及各类纠纷处理规则的的内容为准。</p>
                            <p>3.3ICxyz作为独立第三方，仅对买卖双方提交的证据进行形式审查，并作出独立判断，买卖双方应自行对证据的真实性、完整性、准确性和及时性负责，并承担相关后果。</p>
                            <p>第四章：交易纠纷处理程序</p>
                            <p>4.1.申请和受理</p>
                            <p>4.1.1.买、卖双方就订单产生交易纠纷的，买卖双方可以选择自行协商，如自买家提起交易纠纷申请后1个工作日内，买卖双方协商未果的，则可以申请ICxyz客服介入处理。由ICxyz客服介入的，ICxyz客服即有权根据本规则对纠纷进行处理。</p>
                            <p>4.1.2.买卖双方向ICxyz申请争议处理，必须符合以下条件：</p>
                            <p>买家购买的商品须为在ICxyz平台上通过第三方支付机构、货到付款方式（该货到付款方式须符合ICxyz平台相应规则及流程要求）或ICxyz平台认可的其他支付方式购买的第三方商家商品。</p>
                            <p>4.2.纠纷的处理</p>
                            <p>4.2.1.ICxyz处理争议期间，买卖双方应当按照ICxyz系统的提示和(或）ICxyz发送的短信、电话或邮件通知及时提供凭证。</p>
                            <p>4.2.2.ICxyz收集到双方提供的凭证后，将按照本规则对相应纠纷做出处理；本规则没有明确规定的，由ICxyz依其独立判断做出处理。</p>
                            <p>任何一方无正当理由，未按照前款规定提供凭证的，ICxyz有权按照实际收集到的凭证做出处理。</p>
                            <p>二、交易纠纷处理细则</p>
                            <p>第五章：签收问题纠纷处理</p>
                            <p>5.1签收问题的处理规则</p>
                            <p>5.1.1收货人拒绝签收商品后，商家应当及时联系承运人取回商品。若因商家怠于取回商品所产生的额外运费、保管费等费用由商家自行承担。</p>
                            <p>5.1.2收货人可以本人签收商品或委托他人代为签收商品，收货人委托他人代收的行为视为收货人本人签收。
                                注意：承运人未经买家同意将货物放置在智能快件箱（主要指设立在公共场所,供经营快递业务的企业投递和买家提取快件的自助服务设备），无论物流信息显示签收与否，均不属于买家本人签收，由此产生的相应费用及货物风险需要由商家承担。</p>
                            <p>5.1.3涉及表面一致的事项，若买家提供有效凭证证明签收时既已存在表面不一致情形的，商家应支持买家退款（买家拒绝签收的）或退货退款要求。</p>
                            <p>“表面一致”：是指凭肉眼即可判断所收到的商品表面状况良好且与商品页面描述相符，表面一致的判断范围包括但不限于货物的形状、大小、数量、重量等。</p>
                            <p>5.2签收问题的举证</p>
                            <p>5.2.1若买家表示未收到货，而商家表示买家已签收货物的，商家需要承担举证责任，需要提供收货人本人签收货物的签收底单、承运人出具的收货人本人授权第三方签收凭证等证明，证实买家已收到货物。</p>
                            <p>5.2.2如交易产生退换货，商品退回至商家处时，若商家签收商品后对于商品表面一致的问题有异议或由于商品表面一致问题拒绝签收的，商家需要对商品已存在表面不一致的情形承担举证责任，需要提供承运人加盖公章的证明文件，证实退换货商品存在表面不一致情形（如破损/少件/空包裹等）。</p>
                            <p>第六章：质量问题纠纷处理</p>
                            <p>6.1质量问题的处理规则</p>
                            <p>6.1.1如果出售的商品确实存在质量问题的，买家申请ICxyz介入的，ICxyz将支持买家退货退款要求，并且退换货产生的所有运费由商家承担。</p>
                            <p>6.1.2如果出售的商品存在危及人身财产安全的不合理危险，导致买家损失的，建议商家与买家自行协商或通过其他途径解决，ICxyz不介入该类纠纷处理。</p>
                            <p>6.1.3如果买家在超过三包期限向ICxyz或商家提出关于质量问题的投诉，原则上ICxyz不介入该类纠纷的处理，但这并不能代表免除商家应尽的售后服务责任，商家仍需要提供相应的售后服务。（例如：商品在三包期限内出现质量问题，商家仍应为买家提供相应的售后服务；如商品已经超出三包期限，此时买家发起售后投诉，ICxyz不介入该类纠纷处理，建议买卖双方自行协商处理）。</p>
                            <p>6.2质量问题的举证</p>
                            <p>6.2.1如果买家表示收到的商品存在质量问题，并且该质量问题通过买家提供的举证证明文件无法判断的（如商品性能故障、管脚氧化等），买家申请ICxyz介入纠纷处理后，商家应当按照ICxyz的要求提供商品的正规进货凭证，如厂家的经销凭证、产品合格证、商业发票等证明文件。</p>
                            <p>6.2.2如果买家表示商品存在质量问题，且可以通过买家提供的举证证明文件可判断的（如商品开裂，管脚断裂问题），买家申请ICxyz介入纠纷处理后，ICxyz将根据商品图片或其他证明材料进行判断，直接认定是否存在质量问题。</p>
                            <p>6.3质量问题的检测</p>
                            <p>如果买卖双方就商品质量问题产生纠纷，并买家申请ICxyz介入纠纷处理后，商家需要向ICxyz提供相关的证明文件（如权威机构出具的质量检测报告、产品合格证等证明文件），可证明商品无质量问题，且ICxyz判断该证明文件有效后，买家此时也应当按ICxyz要求提供相应的检测凭证。而送检的检测费用将根据检测报告结果进行判定。</p>
                            <p>注：检测机构建议使用正规权威认证机构，非正规国家认证机构出具的检测报告将视为无效举证。</p>
                            <img src={(config.theme_path + 'rights_regulation_1.jpg')} alt=""/>
                            <p>第七章：运费问题纠纷处理</p>
                            <p>7.1运费问题处理规则：交易中的运费纠纷，根据“谁过错，谁承担”的原则处理，但买卖双方协商一致的除外。</p>
                            <p>7.1.1发货、签收运费纠纷：</p>
                            <p>7.1.1.1如果商家违反ICxyz平台规则或未按约定时间内发货，导致买家未收到货、拒绝签收商品或者签收后退回商品的，买家可以申请退货、退款，运费需要由商家承担。</p>
                            <p>7.1.1.2商家按照约定发货后，收货人有收货的义务。买家拒绝签收商品后，运费的承担方式如下：</p>
                            <img src={(config.theme_path + 'rights_regulation_2.jpg')} alt=""/>
                            <p>7.1.1.3收货人拒绝签收商品后，商家应当及时联系承运人取回商品，若商家怠于取回商品而产生额外的运费、保管费等费用，则由商家自行承担。</p>
                            <p>7.1.2退换货运费纠纷：</p>
                            <p>7.1.2.1买卖双方达成退款协议，但未就退货运费进行约定的，需要由商家承担与其发货相同货运方式的运费。</p>
                            <p>7.1.2.2商品在换货或维修过程中需要寄送且买卖双方未约定运费承担方式的，由此产生的运费需由商家承担。</p>
                            <p>7.1.2.3商家超时未处理售后服务单，未在规定时间内提供退货地址，或者提供的退货地址错误导致买家无法退货或操作退回商品后无法送达的，或者买家根据服务单审核意见操作退货后，商家无正当理由拒绝签收商品的，买家有权直接申请商品退款处理，退货运费由商家承担。若商家需要取回商品的，应当与买家另行协商或通过其他途径解决。</p>
                            <p>7.1.2.4买家或商家申请ICxyz介入处理纠纷的，若商家同意退货的，但买家在未和商家确认的情况下，使用了到付方式给商家退货，商家应先支付运费签收货物，由于买家到付而产生的多余的运费，商家可以与买家协商承担。</p>
                            <p>7.1.3物流运费纠纷</p>
                            <p>未经买家明确同意，若商家未按照与买家约定的方式发货，买家有权拒绝签收商品，发货及退货运费由商家承担。</p>
                            <p>7.1.4商品运费纠纷</p>
                            <p>7.1.4.1如果买家提供有效凭证证实收到商品有问题（如质量问题）或是因为商家承诺未履行而导致的退货退款，商品退回运费需要由商家承担；</p>
                            <p>7.1.5约定运费纠纷</p>
                            <p>7.1.5.1运费由买家承担的，商家向买家收取的运费不得高于承运人的收费标准。</p>
                            <p>7.1.5.2商品描述中对运费做出两个以上的不同描述，或者实际发生的运费与商品描述的运费不一致的，以有利于买家的运费描述为准。</p>
                            <p>7.2运费问题的举证</p>
                            <p>买卖双方申请ICxyz介入处理运费纠纷后，ICxyz有权要求买卖双方就相关纠纷问题提供举证信息，用以核实纠纷问题。</p>
                            <p>7.3为减少运费纠纷，商家应在商品详情页中对运费如实说明，避免出现有歧义的描述，误导买家。</p>
                            <p>第八章：退换货问题纠纷处理</p>
                            <p>为保证交易过程中商家和买家之间的退换货问题可以及时得到解决，减少因此产生的纠纷，商家应在处理退换货问题时遵守以下规则：</p>
                            <p>【退换货地址】</p>
                            <p>商家同意买家提出的退换货申请后，商家应在48小时内对买家提交的退换货服务单进行审核。如果商家逾期未处理，服务单超时48小时将自动审核通过，系统会以商家最后维护的地址作为退货地址推送给买家，如商家有多个退货地址，请及时修改准确信息，避免无法正常收到退货。</p>
                            <p>如果商家提供的退货地址错误导致买家操作退回商品后无法送达的，需要由商家承担因此产生的退货运费。</p>
                            <p>【退换货方式】</p>
                            <p>买家提交售后退换货申请后，商家应根据实际情况如实填写审核意见及要求，如：买家不得使用货到付款方式支付运费等。买家寄回退货商品后，商家有收货的义务。</p>
                            <p>8.1退换货问题的处理原则</p>
                            <p>8.1.1买家提交售后退换货申请后，如果商家未在48小时内处理并提供退货地址，或者提供退货地址错误导致买家无法退货或退回商品后无法送达的，或者买家根据商家售后审核结果操作退货后，商家无正当理由拒绝签收商品的，买家有权直接申请订单退款处理，相关的退货运费也由商家承担。如商家需要取回商品的，应当与买家另行协商或通过其他途径解决。</p>
                            <p>8.1.2买家根据商家售后审核结果操作退换货时，商品在退换货过程中如因非商家原因导致损毁、丢失的（如物流损、丢件、丢失赠品等），商家有权拒绝买家的退换货要求。</p>
                            <p>8.1.3买家提交换货申请并得到商家审核同意后，商家应在收到买家退回的商品后72小时为买家再次发货，并在规定时间内对买家的退换货服务单进行处理。</p>
                            <p>8.2退换货问题的举证问题</p>
                            <p>买卖双方申请ICxyz介入处理退换货纠纷后，ICxyz有权要求买卖双方就相关纠纷问题提供举证信息，用以核实纠纷问题，商家应遵守以下规则：</p>
                            <p>8.2.1如果商家表示未收到退货，而买家确认已经退货的，买家应提供相关证明（如快递发货单、签收底单等），若商家仍未收到退货的，请自行联系承运人处理。</p>
                            <p>8.2.2如果买家表示商家提供的退货地址是错误的，ICxyz有权根据商家系统填写的退货地址进行核实，并进行判定。</p>
                            <p>8.2.3如果商家对买家退回的商品有异议，拒绝签收商品或签收后对退货商品本身有异议的，商家应提供相关证明文件（如：物流公司加盖公章后的证明）证实商品退回时状态。</p>
                            <p>第九章：商品描述不符纠纷处理</p>
                            <p>9.1描述不符问题的处理原则</p>
                            <p>9.1.1如买家收到的商品跟商家商品详情页描述不一致的，买家有权申请退货退款，相关的发货及退货运费由商家承担；</p>
                            <p>9.1.2如果交易中商家没有对商品的描述约定清楚的（如：未就具体尺寸进行说明）:</p>
                            <p>9.1.2.1买家与商家已经就退款事宜达成一致的，但未对运费进行约定的，则需要商家承担发货运费及退货运费。</p>
                            <p>9.1.2.2买家与商家未就退款事宜达成一致的，申请ICxyz介入后也无法判定责任方的，商家应同意买家的退货退款处理，发货运费由商家承担，退货运费由买家承担。</p>
                            <p>9.2描述不符问题的举证</p>
                            <p>9.2.1如果买家收到的商品跟商家商品详情页描述不一致，申请ICxyz介入纠纷处理后，买家应提供商品的相关图片，如通过图片可以直接判定商品是否一致的，ICxyz有权根据商品图片进行判断；</p>
                            <p>9.2.2如果买家收到的商品跟商家商品详情页描述不一致，申请ICxyz介入纠纷处理后，买家提供商品的相关图片后，如商品问题通过肉眼无法做出判断的，ICxyz将会要求商家提供相关的证明文件，如厂家的经销凭证、产品合格证、商业发票、检测凭证等，以便核实处理。</p>
                            <p>第十章：发货问题纠纷处理</p>
                            <p>为了保障交易顺利完成，让买家尽快收到商品，避免因为发货问题导致交易纠纷，商家在发货时应当遵守以下几项要求：</p>
                            <p>【发货时间】</p>
                            <p>买家与商家就发货时间自行约定的，商家须在买家付款后按照在约定时间发货；</p>
                            <p>买家与商家就发货时间未约定的，商家须在买家付款后按《ICxyz开放平台发货管理规则》规定的时间发货；</p>
                            <p>预售、定制类商品，相应的发货时间商家应当在商品详情页中如实描述具体的发货时间；</p>
                            <p>若商家参与了ICxyz开放平台官方发起的促销活动，相应发货时间按ICxyz平台规则公示的发货时间为准；</p>
                            <p>国家法定节假日以ICxyz开放平台通知或公告的发货时间为准。</p>
                            <p>商家的发货时间，是以承运商系统内揽件记录的时间为准。若买家在商家还未发货时提交了退款申请，应写明退款申请理由，等待卖家同意。若买家在商家发货后提交退款申请，若商家未经买家的同意在买家申请退款后强行发货的，买家有权选择拒收，此时商家应当自行追回已经发出的商品，但买家已经签收并且确认收货的除外。</p>
                            <p>【收货地址】</p>
                            <p>商家应当按照买家在订单中填写的收货地址进行发货，若买家与商家另行约定修改收货地址的，应按约定修改的地址进行发货。如果商品需要买家到指定的地点自提的（如物流公司自提的情况），商家应在商品详情页描述告知买家发货方式，同时也应当在发货之前明确告知买家并征得买家的同意。</p>
                            <p>【发货物流】</p>
                            <p>如商家和买家约定了特定的承运人（如物流公司等）发货，商家应当按约定进行发货；否则买家有权选择拒收商品，相应损失由商家承担。</p>
                            <p>10.1发货问题的处理原则</p>
                            <p>若买卖双方就发货问题产生纠纷，申请ICxyz介入的，按如下原则处理：</p>
                            <p>10.1.1如商家违反了发货要求，导致买家没有收到货、拒签商品或者签收商品后退回的，买家可申请退款。</p>
                            <p>10.1.2如商家提供有效凭证可以证明买家已收到商品，且买家未对凭证提出其他异议的，商家有权拒绝买家该订单的退款要求。</p>
                            <p>10.2发货问题的举证</p>
                            <p>10.2.1买卖双方就发货问题申请ICxyz介入后，商家是否按ICxyz开放平台发货要求、发货时间发货的，将依据承运人系统内的揽件记录的时间信息进行判定。如商家有异议的，请提供相关的物流公司加盖公章的证明（可证实商家实际的揽件时间未违约），或与买家约定发货时间的咚咚聊天记录等凭证，以便处理；</p>
                            <p>10.2.2如买家表示没有收到货物但商家确实已经发货的，商家应提供相应的发货凭证（如物流公司发货单），及买家已签收的凭证（买家本人签收底单或买家授权第三方签收的加盖物流公司公章的证明等），以便核实；</p>
                            <p>10.2.3如商家未按照与买家约定的发货方式发货的，商家应提供与买家约定一致的咚咚聊天记录等证明予以证实，以便核实。</p>
                            <p>第十一章：发票&赠品纠纷处理</p>
                            <p>买家在ICxyz开放平台购买商家的商品，有权要求提供购物发票商家开具发票时应按照买家实际支付的货款金额，并依据买家申请开具发票时填写的发票内容（抬头、金额、数量、发票公章等）进行开具，如有疑问可以联系买家进行确认，以避免后期产生不必要的纠纷。</p>
                            <p>赠品是指商家在所售商品外赠送给买家的物品。为避免后期产生不必要的纠纷，商家应对商品所含有的赠品进行清晰、如实的描述，不得有误导买家的信息出现。</p>
                            <p>11.1发票&赠品问题的处理原则</p>
                            <p>11.1.1发票问题的处理原则</p>
                            <p>11.1.1.1交易过程中若商家提供了发票给买家，后期交易发生了退货退款的，买家需要将发票一并退回。</p>
                            <p>若买家未将发票退回或发票丢失的，商家处理退款时可按照实际情况要求买家承担相应的发票税点金额。</p>
                            <p>11.1.1.2若买家下单时已要求商家开具发票，或咚咚提出要求并且商家已承诺为买家开具发票的，因商家的原因导致买家未收到发票的（如未开具或推延开具），自买家反馈之日起商家在30天之内仍无法开具的（如页面标注开票时间的，以页面标注时间为准顺延30天），买家有权申请退货退款，相关的发货及退回运费由商家承担。</p>
                            <p>11.1.2赠品问题的处理原则</p>
                            <p>11.1.2.1若商家的商品描述中注明附送赠品，或未注明但买卖双方协商确认一致有赠品附送的，后期交易最终发生了退货退款的，买家需要将赠品一并退回；若未退回，商家有权按照赠品的市场价格（如买家有异议，申请ICxyz介入的，由ICxyz按照实际情况进行估价）收取相应的费用。</p>
                            <p>11.1.2.2若商家的商品描述中注明附送赠品，或未注明但买卖双方协商确认一致有赠品附送的，交易商品无问题而赠品存在问题，商家有权只受理赠品问题。</p>
                            <p>11.1.2.3如赠品作为商品单独产生交易生成订单的，相应的纠纷问题按正常商品的纠纷处理规则进行处理。</p>
                            <p>11.2发票&赠品的举证</p>
                            <p>交易商品最终退货退款的，若商家表示未收到买家退回的发票或赠品的，申请ICxyz介入纠纷处理后，商家需要提供快递公司加盖公章的收货证明，可证实商家收到的货物确实无发票或赠品的情况；或买家承认未退回发票或赠品的咚咚聊天记录等，以便ICxyz处理相应的纠纷。</p>
                            <p>三、附则</p>
                            <p>13.1.ICxyz开放平台商家的行为，发生在本管理规则生效之日以前的，适用当时的规则。发生在本管理规则生效之日以后的，适用本规则。</p>
                            <p>13.2.ICxyz可根据平台运营情况随时调整本管理规则并以“ICxyz开放平台”公告的形式向商家公示。</p>
                            <p>13.3.商家应遵守国家法律、行政法规、部门规章等规范性文件。对任何涉嫌违反国家法律、行政法规、部门规章等规范性文件的行为，本规则已有规定的，适用于本规则。本规则尚无规定的，ICxyz有权酌情处理。但ICxyz对商家的处理不免除其应承担的法律责任。商家在ICxyz的任何行为，应同时遵守与ICxyz及其关联公司签订的各项协议。</p>
                            <p>13.4.本规则于2016年6月24日修订，于2016年7月1日生效。</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }
        else if(this.props.path == 53){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>交易约定</p>
                        <div className="AgreementBoxInner">
                            <p>文档正在火速赶来，请稍后</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }
        else if(this.props.path == 54){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>退款退货流程</p>
                        <div className="AgreementBoxInner">
                            <p>文档正在火速赶来，请稍后</p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }
        else if(this.props.path == 55){
            return (
                <div className="AgreementShow"  style={this.props.flag ? {display:'block'} : {display:'none'}}>
                    <div className="AgreementBox">
                        <p>质保流程</p>
                        <div className="AgreementBoxInner">
                            <p>质保服务说明</p>
                            <p>为确保质保服务正常有序开展，请用户仔细阅读以下质保服务说明。</p>
                            <p>1定义</p>
                            <p>质保是用户向专业鉴定机构提供交易的部分或全部商品，用一系列试验来鉴别电子元件类商品可靠性或真伪性的一种方法。</p>
                            <p>2质保介绍</p>
                            <p>ICXYZ商城在平台上提供用户与质保单位的对接服务，质保服务由专业质保机构提供。买卖双方在平台交易过程中有质量疑问时，其中一方可以提出质保申请，选择质保服务即平台默认买卖双方认可质保机构出具相关鉴定证明真实有效，否则不能发起质保。其中质保结果由质保单位出具，ICxyz平台依据质保单位出具的报告，将对争议订单做出不同的处理意见。具体处理意见3.1.2质保申请的发起和相应责任或质保页面的处理结果。注意，同意此质保说明即意味着同意平台的处理结果，同意在平台上交易亦意味着同意平台的处理结果。</p>
                            <p>3质保流程</p>
                            <p>3.1发起质保</p>
                            <p>3.1.1发起质保条件</p>
                            <p>用户要求的质保服务所要鉴定的商品，必须是争议订单的部分或全部商品，且必须为一个或多个争议型号的同一批次商品。如出现非本订单或非同一批次的商品，平台将不予受理此次质保申请。注意，提供用于质保试验商品的用户对该质保样品负有真实性义务，若出现私自替换，或其他任何形式的加工修改等造假行为，造成质保结果不准确，该用户的负全部责任。</p>
                            <p>若用户不能提供完好的，可供鉴定的商品，则质保无法开展，平台将无法受理质保申请。</p>
                            <p>用户发起质保的申请需要在确认收货后180日内提交，逾期发起质保，平台将不予受理。</p>
                            <p>3.1.2 质保申请的发起和相应责任</p>
                            <p>依据平台用户注册协议，若买卖双方中的一方在对方认可的质保机构范围内发起质保，即平台默认买卖双方认可质保机构出具的报告，平台可依据报告，做出对争议订单的处理。</p>
                            <p>买方发起：买方收货后对产品的质量存有疑义，在与卖方无法达成一致意见时，在该订单的管理页面提出在线申请，选择质保机构，提供相关资料，并预先支付质保服务产生的所有费用。若鉴定结果表明产品质量确有问题，则质保费用应由卖方承担，此时若货款在平台，平台将执行退款；若货款已结算给卖方，平台将帮助买方与卖方协商退货退款事宜，若卖方拒不配合，平台将提供买方相关证据，以便买方起诉或仲裁，但平台不承担为买方追讨货款和质保费的义务和法律责任。若鉴定结果表明产品质量没有问题，则质保费用应由买方承担，平台视此次交易为正常完成。若鉴定结果无法确定产品质量是否存在问题。则质保费用由发起方承担。若此时货款不在平台，买家仍认为商品质量存在问题，可以向平台申请提取争议订单详情及相关材料。</p>
                            <p>卖方发起：卖方在买方对产品质量有疑义，且与买方无法达成一致意见时，可以提出质保申请以证明清白。在该订单的管理页面提出在线申请，选择质保机构，提供相关资料，并预先支付质保服务产生的所有费用。注意：卖方发起质保无论质保结果如何，质保费用都应由卖方承担。若质保报告显示卖方商品质量确有问题，买方应退货，卖方应退款 。若质量不存在问题，平台视此次交易为正常订单。</p>
                            <p>3.1.3支付质保费</p>
                            <p>不同质保机构有不同的收费标准，具体收费标准将由质保机构在线更新。</p>
                            <p>支付质保费可以选择在线支付或线下转账，线下转账后需上传汇款底单。底单查询到帐后，该质保申请即刻生效。</p>
                            <p>ICxyz商城对所收的质保费不支付任何手续费和利息。</p>
                            <p>3.1.4 送检</p>
                            <p>送检方式：直接寄送，平台代送等方式</p>
                            <p>具体送检方式和注意事项将由质保机构在线更新。</p>
                            <p>3.1.5 质保结果查询</p>
                            <p>质保报告出具时间由质保机构在线更新。买卖双方均可在线查询进度，并下载质保报告。</p>
                            <p>3.1.6质保结论的处理</p>
                            <p>平台将依据质保报告的结论进行处理，具体处理方法将在质保工单的结果页面显示和更新。</p>
                            <p>4责任追究及处罚</p>
                            <p>如平台判定在质保过程中买方或卖方存在诬陷或造假等过错，平台将依据平台规则，扣除买家或卖家相应的信用等级分，并对账号进行包括但不限于：限制平台活动，冻结账号，关店，全网通告，黑名单等处罚；</p>
                            <p>若卖方存在以上情形，平台将依据平台规则，扣除卖家相应平台把此次交易视为卖方销售假冒伪劣商品，卖方应返还货款和质保费，若货款在平台上，平台将会把相关货款返还给买方。若货款已结算给卖方，平台将帮助买方与卖方协商，但不承担相应的义务和法律责任。若卖方拒绝执行平台处理结果，平台有权从卖方店铺的待结货款、店铺保证金中支付卖方损失。</p>
                            <p>如果有双倍赔偿协议的供应商，参见双倍赔付协议条款。</p>
                            <p>ICxyz作为第三方交易平台不承担任何费用和法律责任。</p>
                            <p>ICxyz商城对此协议具有最终解释权。</p>
                            <p>协议自声明之日起生效。</p>
                            <p></p>
                            {/*<p style={{textAlign:'right'}}>2016年11月10日</p>*/}
                        </div>
                        <Button onClick={this.props.click} type="primary">{I18n.t('bottomdoc.close')}</Button>
                    </div>
                </div>
            )
        }
        else{
            return(
                <div className="AgreementBox">
                    23344
                </div>
            )
        }

    }
}
