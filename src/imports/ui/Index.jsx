import React, { Component } from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Meteor } from 'meteor/meteor';
import App from './App.jsx';
import Home from './Home.jsx';
import Login from './Login.jsx';
import Register from './Register.jsx';
import Authenticate from './Authenticate.jsx';
import Search from './Search.jsx';
import PersonalCenter from './PersonalCenterList.jsx';
import PersonalCenterInfo from './PersonalCenterInfo.jsx';
import PersonalCenterAddress from './PersonalCenterAddress.jsx';
import PersonalCenterSecurity from './PersonalCenterSecurity.jsx';
import OrderDetails from './OrderDetails.jsx';
import Cart from './Cart.jsx';
import Order from './Order.jsx';
import ConfirmOrder from './ConfirmOrder.jsx';
import Payment from './Payment.jsx';
import SellerCenter from './SellerCenter.jsx';
import SellerCenterOrder from './SellerCenterOrder.jsx';
import SellerCenterUploadStock from './SellerCenterUploadStock.jsx';
import SellerCenterManageStock from './SellerCenterManageStock.jsx';
import SellerCenterManageStockUpdate from './../modules/SellerCenter/SellerCenterManageStockUpdate.jsx';
import SellerOrderDetails from './SellerOrderDetails.jsx';
import SellerDeliverGoods from './SellerDeliverGoods.jsx';
import LogisticsDetails from './LogisticsDetails.jsx';
import SellerCenterBill from './SellerCenterBill.jsx';
import SellerCenterCustomerInfo from './SellerCenterCustomerInfo.jsx';
import SellerReturnNote from './SellerReturnNote.jsx';
import Forget from './Forget.jsx';
import perRegister from './perRegister.jsx';
import comRegister from './comRegister.jsx';
import PersonalCenterSecurityPassword from './PersonalCenterSecurityPassword.jsx';
import PersonalCenterSecurityPhone from './PersonalCenterSecurityPhone.jsx';
import PersonalCenterSecurityEmail from './PersonalCenterSecurityEmail.jsx';
import PersonalCenterSecurityNewPhone from './PersonalCenterSecurityNewPhone.jsx';
import PersonalCenterSecurityNewEmail from './PersonalCenterSecurityNewEmail.jsx';
import PersonalCenterSecurityPay from './PersonalCenterSecurityPay.jsx';
import PersonalCenterSecurityAuthentication from './PersonalCenterSecurityAuthentication.jsx';
import PersonalCenterSecurityNewAuthentication from './PersonalCenterSecurityNewAuthentication.jsx';
import PersonalCenterOrder from './PersonalCenterOrder.jsx';
import CompantInvoics from './CompantInvoics.jsx';
import PersonalCenterIntegral from './PersonalCenterIntegral.jsx';
import PersonalCenterBuyer from './PersonalCenterBuyer.jsx';
import SellerCenterModelAudit from './SellerCenterModelAudit.jsx';
import SellerCenterCertificationAudit from './SellerCenterCertificationAudit.jsx';
import ShoppingCart from './ShoppingCart.jsx';
import PaymentMoney from './../modules/payment/PaymentMoney.jsx';
import SellerCenterAddType from './SellerCenterAddType.jsx';
import ProductDetails from './ProductDetails.jsx';
import Evaluate from './Evaluate.jsx';
import AdditionalComment from './PersonalAdditionalComment.jsx';
import Return from './Return.jsx';
import zh_cn from '../zh_cn';
import Agreement from './Agreement.jsx';
import PersonalCenterIndex from './PersonalCenterIndex.jsx';
import Details from './Details.jsx';
import Merchantinformation from './Merchantinformation.jsx';
import QualityAssuranceAgency from './QualityAssuranceAgency.jsx';
import ReturnNote from './ReturnNote.jsx';
import SellerCenterClientInfo from './SellerCenterClientInfo';
import SellerEvaluate from './SellerEvaluate.jsx';
import MessageCard from './MessageCard.jsx';
import UserFeedback from './UserFeedback';
import Luckcui from './Luckcui.jsx';
class NotFound extends Component{
    render() {
        return(
            <h1>404 NotFound</h1>
        );
    }
}

//离开页面时提示
window.addEventListener('beforeunload', function (e) {
    if (window.location.pathname == '/seller_center/upload_stock' || window.location.pathname == '/seller_center/add_type' || window.location.pathname == '/Payment') {
        var confirmationMessage = 'It looks like you have been editing something.';
        confirmationMessage += 'If you leave before saving, your changes will be lost.';

        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    }
    else if (window.location.pathname == '/message_cart') {
        Meteor.call('message.readMessage');
    }
});

function needLogin(nextState, replaceState) { //登录后才能访问，否则跳转到login
    if (Meteor.userId() == null) {
        replaceState({nextPathname: nextState.location.pathname}, '/login');
    }
}

function needunLogin(nextState, replaceState) { //登录前才能访问，否则跳转到home
    if (Meteor.userId() != null) {
        replaceState({nextPathname: nextState.location.pathname}, '/');
    }
}

export default class Index extends Component {
    render() {
        return (
            <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory}>
                <Route path="/" component={App}>
                    <IndexRoute component={Home} />
                    <Route path="user_feedback" component={UserFeedback} onEnter={needLogin}></Route>
                    <Route path="Register" component={Register} words={zh_cn} onEnter={needunLogin}/>
                    <Route path="perRegister" component={perRegister} onEnter={needunLogin}/>
                    <Route path="perRegister/:id" component={perRegister} onEnter={needunLogin}/>
                    <Route path="comRegister" component={comRegister} onEnter={needunLogin}/>
                    <Route path="comRegister/:id" component={comRegister} onEnter={needunLogin}/>
                    <Route path="Login" component={Login}  onEnter={needunLogin}/>
                    <Route path="forget" component={Forget}  onEnter={needunLogin}/>
                    <Route path="authenticate" component={Authenticate} />
                    <Route path="Search/:id" component={Search} />
                    <Route path="Search" component={Search} />
                    
                    <Route path="personal_center" component={PersonalCenter} onEnter={needLogin}>
                        <IndexRoute component={PersonalCenterIndex} />
                        <Route path="index" component={PersonalCenterIndex} />
                        <Route path="info" component={PersonalCenterInfo} />
                        <Route path="address" component={PersonalCenterAddress} />
                        <Route path="order" component={PersonalCenterOrder} />
                        <Route path="return_note" component={ReturnNote}/>
                        <Route path="security" component={PersonalCenterSecurity} />
                        <Route path="invoics" component={CompantInvoics} />
                        <Route path="authentication" component={PersonalCenterSecurityAuthentication} />
                        <Route path="new_authentication" component={PersonalCenterSecurityNewAuthentication} />
                        <Route path="integral" component={PersonalCenterIntegral} />
                        <Route path="password" component={PersonalCenterSecurityPassword} />
                        <Route path="phone" component={PersonalCenterSecurityPhone} />
                        <Route path="email" component={PersonalCenterSecurityEmail} />
                        <Route path="new_phone" component={PersonalCenterSecurityNewPhone} />
                        <Route path="new_email" component={PersonalCenterSecurityNewEmail} />
                        <Route path="pay" component={PersonalCenterSecurityPay} />
                        <Route path="buyer" component={PersonalCenterBuyer} />
                    </Route>
                    <Route path="cart" component={Cart} onEnter={needLogin}/>
                    <Route path="order" component={Order} onEnter={needLogin}/>
                    <Route path="confirm_order" component={ConfirmOrder} onEnter={needLogin}/>
                    <Route path="Payment" component={Payment} onEnter={needLogin}/>
                    <Route path="evaluate" component={Evaluate} onEnter={needLogin}/>
                    <Route path="additional_comment" component={AdditionalComment} onEnter={needLogin}/>
                    <Route path="sellert_evaluate" component={SellerEvaluate} onEnter={needLogin}/>
                    <Route path="return/:id" component={Return} onEnter={needLogin}/>
                    <Route path="seller_center" component={SellerCenter} onEnter={needLogin}>
                        <IndexRoute component={SellerCenterCustomerInfo} />
                        <Route path="order" component={SellerCenterOrder} />
                        <Route path="upload_stock" component={SellerCenterUploadStock} />
                        <Route path="manage_stock" component={SellerCenterManageStock} />
                        <Route path="return_note" component={SellerReturnNote}/>
                        <Route path="manage_stock_update" component={SellerCenterManageStockUpdate} />
                        <Route path="bill" component={SellerCenterBill} />
                        <Route path="customer_info" component={SellerCenterCustomerInfo} />
                        <Route path="client_info" component={SellerCenterClientInfo} />
                        <Route path="model_audit" component={SellerCenterModelAudit} />
                        <Route path="certification_audit" component={SellerCenterCertificationAudit} />
                        <Route path="add_type" component={SellerCenterAddType} />
                        <Route path="merchant" component={Merchantinformation} />
                        <Route path="quality" component={QualityAssuranceAgency} />
                    </Route>
                    <Route path="shopping_cart" component={ShoppingCart} onEnter={needLogin}/>
                    <Route path="message_cart" component={MessageCard}  onEnter={needLogin}  />
                    <Route path="product_details/:id" component={ProductDetails}/>
                    <Route path="sellerorder_details/:id" component={SellerOrderDetails} onEnter={needLogin}/>
                    <Route path="sellerorder_deliver/:id" component={SellerDeliverGoods} onEnter={needLogin}/>
                    <Route path="order_details/:id" component={OrderDetails} onEnter={needLogin}/>
                    <Route path="goods_details/:id" component={Details} onEnter={needLogin}/>
                    <Route path="payment_money" component={PaymentMoney} onEnter={needLogin}/>
                    <Route path="logistics_details" component={LogisticsDetails} onEnter={needLogin}/>
                    <Route path="agreement/:id"  component={Agreement} />
                </Route>
                <Route path="*" component={NotFound} />
            </Router>
        );
    }
}
