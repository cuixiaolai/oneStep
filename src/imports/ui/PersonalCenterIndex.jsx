import React, { Component } from 'react';
// import { createContainer } from 'meteor/react-meteor-data'; 
// import { Meteor } from 'meteor/meteor';
// import { Buyer } from '../api/Buyer.js';
// import { Users } from '../api/User.js';
// import { Order } from '../api/Order.js';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../config.json';
import EditImg from '../modules/center/EditImg.jsx';
import Securitylevel from '../modules/center/Securitylevel.jsx';
import { Button } from 'antd';
import { Link } from 'react-router'
import { browserHistory } from 'react-router';
import currentUserInfo from './currentUserInfo.json';
import currentUser from './currentUser.json';
import orderInfo from './orderInfo.json';

 class PersonalCenterIndex extends Component{
    constructor(props) {
        super(props);
        // this.props.currentUserInfo=currentUserInfo;
        // this.props.currentUser=currentUser;
        // this.props.orderInfo=orderInfo;
        this.state = ({
            flags: true,
            img: null,
            num:3,
            nums:2,
        });
    }
    setImg(path) {
        this.setState({img: path});
        // Meteor.call('buyer.updatePersonalIcon', path);
    }
    clickClose() {
        this.setState({flags: true});
    }
    clickOpen() {
        this.setState({flags: false});
    }
    renderEdit() {
        if (this.state.flags == false) {
            if (this.state.img != null) {
                return <EditImg click={this.clickClose.bind(this)} img={this.setImg.bind(this)} icon={this.state.img}/>;
            }
            else if (this.props.UserInfo && this.props.UserInfo.icon) {
                return <EditImg click={this.clickClose.bind(this)} img={this.setImg.bind(this)} icon={this.props.UserInfo.icon}/>;
            }
            else {
                return <EditImg click={this.clickClose.bind(this)} img={this.setImg.bind(this)}/>;
            }
        }
    }
    renderIcon() {
        if (this.props.currentUserInfo != null && this.props.currentUserInfo.icon != null) {
            return (
                <div className="PerImg">
                    <img src={(config.file_server + this.props.currentUserInfo.icon)} alt="" style={{width:116,height:116,display: 'inline-block'}}/>
                    <p
                        style={{position: 'relative',textAlign: 'center',top: '-24px',height: 20,background: 'rgba(0,0,0,.34)',cursor: 'pointer',color: '#fff'}}
                        onClick={this.clickOpen.bind(this)}
                    >{I18n.t('PerCenterLeft.upload')}</p>
                </div>
            );
        }
        else {
            return (
                <div className="PerImg">
                    <img src={(config.theme_path + 'centering.png')} alt="" style={{width:116,height:116,display: 'inline-block'}}/>
                    <p
                        style={{position: 'relative',textAlign: 'center',top: '-24px',height: 20,background: 'rgba(0,0,0,.34)',cursor: 'pointer',color: '#fff'}}
                        onClick={this.clickOpen.bind(this)}
                    >{I18n.t('PerCenterLeft.upload')}</p>
                </div>
            );
        }
    }
    Name(){
        if(this.props.currentUser != null){
            return(
                <p>{this.props.currentUser.username}</p>
            )
        }
    }
    Level() {
        if (this.props.currentUserInfo != null && this.props.currentUser != null) {
            let level = 4;
            if (this.props.currentUser.phone != null) {
                level --;
            }
            if (this.props.currentUser.emails != null) {
                level --;
            }
            if (this.props.currentUserInfo.pay != null) {
                level --;
            }
            if (this.props.currentUserInfo.authentication != null && this.props.currentUserInfo.authentication.verify == true && this.props.currentUserInfo.authentication.success == true) {
                level --;
            }
            return (
                <p>
                    {I18n.t('buyer.Accountsecurity')}：
                    <Securitylevel width='100' num={level} />
                </p>
            );
        }
    }
    receiveClick(_id){
        // Meteor.call('order.forwardToOperation',_id,'customer_receive', (err) => {
        //     if (err) {

        //     } else {
                browserHistory.replace('/confirm_order');
        //     }
        // });
    }
    ShowGrade(){
        let Pic = [];
        if(this.state.num == 1){
            for(let i = 0 ; i< this.state.nums; i++){
                Pic[i] = (
                    <img style={{width:14,height:14,marginRight:4}} src={(config.theme_path + 'tongpai_07.png')} alt=""/>
                )
            }
        }else if (this.state.num == 2){
            for(let i = 0 ; i< this.state.nums; i++){
                Pic[i] = (
                    <img style={{width:14,height:14,marginRight:4}} src={(config.theme_path + 'yinpai_05.png')} alt=""/>
                )
            }
        }else if(this.state.num == 3){
            for(let i = 0 ; i< this.state.nums; i++){
                Pic[i] = (
                    <img style={{width:14,height:14,marginRight:4}} src={(config.theme_path + 'jinpai_03.png')} alt=""/>
                )
            }
        }
        return Pic;
    }
    Showauthentication(){
        if (this.props.currentUserInfo != null && this.props.currentUserInfo.authentication != null && this.props.currentUserInfo.authentication.verify == true && this.props.currentUserInfo.authentication.success == true ) {
            return(
                <img src={(config.theme_path + 'alreadyCertified.png')} alt=""/>
            );
        }
        else {
            return(
                <img src={(config.theme_path + 'unauthorized.png')} alt=""/>
            );
        }
    }
    showCompanyType(company_type){
        let sign = '';
        if (company_type == 'agent') {
            sign = '代理商';
        } else if (company_type == 'stock') {
            sign = '厂商'
        } else if (company_type == 'retailer') {
            sign = '零售商'
        }
        return <span className="sign">{sign}</span>;
    }
    showOrder(order){
        let temp = [];
        temp.push(<p>{I18n.t('logistics.name')}</p>);
        if(order.length == 0){
            temp.push(this.shownull());
        }else{
            for(let i in order){
                temp.push(
                    <div className="PersonalCenterIndexlogisticsInner">
                        <div className="PersonalCenterIndexlogisticsInnerTop">
                            <span>{order[i].company_name}{this.showCompanyType(order[i].company_type)}</span>
                            <span>{I18n.t('sellerorderdetails.orderno')}{order[i].id}</span>
                            <span className="right">{typeof update_time != 'object'?'':(order[i].update_time.getFullYear()+'-'+(order[i].update_time.getMonth()+1)+'-'+order[i].update_time.getDate())}</span>
                            <div className="clear"></div>
                        </div>
                        <div className="PersonalCenterIndexlogisticsInnerBottom">
                            <div>
                                <div className="left">
                                    <img src={(config.theme_path + 'shop_03.jpg')} alt=""/>
                                </div>
                                <div className="left">
                                    <p>{order[i].product[0].product_id}</p>
                                    <p>{order[i].product[0].manufacturer}</p>
                                    <p>{order[i].product[0].describe}</p>
                                </div>
                                <div className="clear"></div>
                            </div>
                            <div>
                                <p> <Link to="/logistics_details"> {I18n.t('logistics.message')} </Link></p>
                            </div>
                            <div>
                                <Button onClick={this.receiveClick.bind(this,order[i]._id)} type="ghost">{I18n.t('shoppingCart.dialog.confirmreceipt')}</Button>
                            </div>
                            <p className="clear"></p>
                        </div>
                        {order[i].product.length > 1? <div>......</div>:null}
                    </div>
                );
            }
        }
        return temp;
    }
    shownull(){
        return(
            <div style={{margin:'50px auto',textAlign:'center'}}>
                <img src={(config.theme_path + 'noaddress.png')} alt=""/><span style={{color:'#999',fontSize:'20px'}}>{I18n.t('Merchantinformation.nomessage')}</span>
            </div>
        )
    }
    render(){
        let obligation = 0;
        let waitorder = 0;
        let waitgoods = 0;
        let waitevaluate = 0;
        if(this.props.orderInfo.length > 0){
            for(let i in this.props.orderInfo){
                if(this.props.orderInfo[i].order_status == 1){
                    obligation++;
                }else if(this.props.orderInfo[i].order_status == 2){
                    waitorder++;
                }else if(this.props.orderInfo[i].order_status == 3) {
                    waitgoods++;
                }else if(this.props.orderInfo[i].order_status == 4) {
                    waitevaluate++;
                }
            }
        }
        console.log(this.props.orderInfo.length);
         // <p>{I18n.t('PersonalCenter.Message.integral')}：  <span style={{color:'#0c5aa2'}}>30</span>{I18n.t('ge')}  </p>
        return(
            <div className="PersonalCenterIndex">
                <div className="PersonalCenterIndexBox">
                    {this.renderIcon()}
                    <div className="left PersonalCenterIndexMess">
                        {this.Name()}
                        <p>{I18n.t('PersonalCenterSecurity.authentication.name')}： {this.Showauthentication()} </p>



                        
                       
                        <p>{I18n.t('buyer.Accountlevel')}： {this.ShowGrade()}</p>
                        {this.Level()}
                    </div>
                    <div className="clear"></div>
                    <div className="PersonalCenterIndexBoxInner">
                        <p> <Link to="/personal_center/order" query={{key:'1'}} style={{color:'#707070'}}> {I18n.t('order.obligation')} <span>{obligation}</span> </Link></p>
                        <p> <Link to="/personal_center/order" query={{key:'2'}} style={{color:'#707070'}}>{I18n.t('order.waitorder')} <span>{waitorder}</span> </Link></p>
                        <p><Link to="/personal_center/order" query={{key:'3'}} style={{color:'#707070'}}>{I18n.t('order.waitgoods')} <span style={{color:'#0c5aa2'}}>{waitgoods}</span> </Link></p>
                        <p><Link to="/personal_center/order" query={{key:'4'}} style={{color:'#707070'}} >{I18n.t('order.waitevaluate')} <span>{waitevaluate}</span></Link> </p>
                    </div>
                </div>
                <div className="PersonalCenterIndexlogistics">
                    {this.props.orderInfo.length > 0 ?
                        this.showOrder(this.props.orderInfo.filter(item=>item.order_status==3)) : this.shownull()
                    }
                </div>
                {this.renderEdit()}
            </div>
        )
    }
}
export default class PersonalCenterIndex1 extends Component {
        render() {
            return <PersonalCenterIndex currentUserInfo={currentUserInfo} currentUser={currentUser} orderInfo={orderInfo} ></PersonalCenterIndex>;
        }

}
// export default createContainer(() => {
//     Meteor.subscribe('buyer');
//     Meteor.subscribe('users');
//     Meteor.subscribe('order');
//     return {
//         currentUserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'nickname': 1, 'icon': 1, 'pay': 1, 'authentication': 1}}),
//         currentUser: Users.findOne({_id: Meteor.userId()}, { fields: {'username': 1, 'emails': 1, 'phone': 1, 'company': 1}}),
//         orderInfo: Order.find({customer_id: Meteor.userId(), problem_order: 1},{sort: {update_time: -1}}).fetch()
//     };
// }, PersonalCenterIndex);
