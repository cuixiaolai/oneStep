import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import SecurityTop from '../modules/center/SecurityTop.jsx'
import { Steps, Button } from 'antd';

export default class PersonalCenterSecurityNewPhone extends Component {
    render(){
        let words={
            name: I18n.t('PersonalCenterSecurity.SecurityTopPhone.names'),
            titleone: I18n.t('PersonalCenterSecurity.SecurityTopPhone.titleone'),
            titletwo: I18n.t('PersonalCenterSecurity.SecurityTopPhone.titletwo'),
            titlethree: I18n.t('PersonalCenterSecurity.SecurityTopPhone.titlethree'),
        };
        console.log(I18n.t('PersonalCenterSecurity.SecurityTopPhone.names'));
        return(
            <div>
                <SecurityTop words={words} flag={true} />
            </div>
        )
    }
}
