import React, { Component } from 'react';
import Topbox from '../modules/TopBox.jsx'
import ComRegisterBox from '../modules/register/ComRegisterBox.jsx'
// import { Meteor } from 'meteor/meteor';
import EmailVerifyFail from '../modules/register/EmailVerifyFail.jsx';
import Userregistration from '../modules/Agreement/Userregistration.jsx';

export default class comRegister extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            current: 0,
            email: '',
        });
    }
    componentWillMount() {
        let route = window.location.pathname;
        if (route == '/comRegister') {
            this.setState({current: 0});
        }
        else {
            let arr = route.split('/');
            let token = arr[arr.length-1];
            this.setState({current: 1});
            // Meteor.call('accounts.CheckVerificationEmail', token, function(err, result) {
            //     if(err) {
            //         this.setState({current: 2});
            //     }
            //     else {
            //         this.setState({current: 1, email: result});
            //     }
            // }.bind(this));
        }
    }

    render() {
        if (this.state.current == 0) {
            return (
                <div>
                    <Userregistration />
                    <ComRegisterBox callback={false} email=''/>
                </div>
            );
        }
        else if (this.state.current == 1) {
            return (
                <div>
                    <ComRegisterBox callback={true} email={this.state.email}/>
                </div>
            );
        }
        else {
            return (
                <div>
                    <EmailVerifyFail />
                </div>
            );
        }
    }
}
