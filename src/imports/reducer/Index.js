import { combineReducers } from 'redux';
import stock from './stock.js';
import payment from './payment.js';
import category from './category.js';

const Reducer = combineReducers({
    stock,
    category
});

export default Reducer;
