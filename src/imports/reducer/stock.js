export default function stock(state = [], action) {
    switch (action.type) {
        case 'ADD_DATA':
            return [...state, action.text];
        case 'CLEAN_DATA':
            return [];
        default:
            return state;
    }
}