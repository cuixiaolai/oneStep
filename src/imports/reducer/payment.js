export default function payment(state = [], action) {
    switch (action.type) {
        case 'ADD_ORDER':
            return [...state, action.text];
        case 'CLEAN_ORDER':
            return [];
        default:
            return state;
    }
}