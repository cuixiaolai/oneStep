import React, { Component } from 'react';
import PerEmailone from './PerEmailone.jsx';
import PerEmailtwo from './PerEmailtwo.jsx';
import PerPhonethree from './PerPhonethree.jsx';

export default class PeremailBox extends Component {
    PerEmail(){
        if(this.props.current == 1){
            return(
                <PerEmailtwo next={this.props.next}  email={this.props.email}/>
            )
        }else if(this.props.current == 2){
            return(
                <PerPhonethree />
            )
        }else if(this.props.current == null || this.props.current == 0){
            return(
                <PerEmailone next={this.props.next} click={this.props.click} />
            )
        }
    }
    render(){
        console.log(this.props)
        return(
           <div className="PerphoneBox">
               {this.PerEmail()}
            </div>
        )
    }
}
