import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Input, Button, Checkbox } from 'antd';
import VerifyCode from '../VerifyCode.jsx';
// import { Accounts } from 'meteor/accounts-base';
// import { Meteor } from 'meteor/meteor';
const FormItem = Form.Item
const createForm = Form.create;
import config from '../../config.json';

let PerPhonetwo = React.createClass({
    HandleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }else{
                // Accounts.createUser({username: this.props.phone, password: values.emailpasswd},function(err) {
                //     if(err) {
                //         console.log(err);
                //     }
                //     else {
                //         console.log(Meteor.userId());
                //         Meteor.call('user.addPhone', this.props.phone);
                //         Meteor.call('Roles.addUsersToRoles', Meteor.userId(), 'person');
                //         Meteor.call('buyer.create');
                //         Meteor.call('cart.create');
                //         Meteor.call('order.create');
                //         this.props.next();
                //     }
                // }.bind(this));
                console.log(this.props.phone);
                console.log(values.emailpasswd);
                const url = "http://localhost:3001/api/v1/user";
                fetch(url, {
                    credentials: 'include',
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                      name: this.props.phone,
                      pwd: values.emailpasswd,
                      type:"phone"
                    }),
                }).then(response=>{
                    return  response.json()
                
                }).then(body=>{
                    console.log(body)
                    if(body.code==200){
                     this.props.next();
                    }
                    
                
                }).catch(err=>console.log(err))
                               console.log(this.props.phone)
                               
                           }
                       });
                   },
    checkEmailPass(rule, value, callback) {
        if(!value){
            return callback(I18n.t('Register.passwordNull'));
        }
        let aa = value.indexOf('@');
        let bb = value.indexOf('!');
        let cc = value.indexOf('#');
        let dd = value.indexOf('$');
        let ee = value.indexOf('%');
        let ff = value.indexOf('&');
        let gg = value.indexOf('*');
        let hh = value.indexOf('(');
        let ii = value.indexOf(')');
        let jj = value.indexOf('[');
        let kk = value.indexOf(']');
        let ll = value.indexOf('{');
        let mm = value.indexOf('}');
        let nn = value.indexOf(';');
        let oo = value.indexOf('<');
        let pp = value.indexOf('>');
        let qq = value.indexOf(',');
        let rr = value.indexOf('/');
        let ss = value.indexOf('。');
        let tt = value.indexOf('《');
        let uu = value.indexOf('》');
        let vv = value.indexOf('|');
        let ww = value.indexOf('?');
        let xx = value.indexOf('~');
        let yy = value.indexOf('"');
        let zz = value.indexOf('^');
        let ab = value.indexOf("\ ");
        let ac = value.indexOf("'");
        const { validateFields } = this.props.form;
        if(aa<0&&bb<0&&cc<0&&dd<0&&ee<0&&ff<0&&gg<0&&hh<0&&ii<0&&jj<0&&kk<0&&ll<0&&mm<0&&nn<0&&oo<0&&pp<0&&qq<0&&rr<0&&ss<0&&tt<0&&uu<0&&vv<0&&ww<0&&xx<0&&yy<0&&zz<0&&ab<0&&ac<0){
            if (value) {
                validateFields(['emailrePasswd'], { force: true });
                callback();
            }
            
        }else{
            callback('不包含符号');
        }
    },
    checkEmailPass2(rule, value, callback) {
        const { getFieldValue } = this.props.form;
        if (value && value !== getFieldValue('emailpasswd')) {
            callback(I18n.t('Register.pwTishi'));
        } else { 
            callback();
        }
    },
    render(){
        const { getFieldProps } = this.props.form;
        const passwdEmailProps = getFieldProps('emailpasswd', {
            rules: [
                { whitespace: true,min:6,max:20 ,message: I18n.t('Register.passwordTishi'),},
                { validator: this.checkEmailPass },
            ],
        });
        const rePasswdEmailProps = getFieldProps('emailrePasswd', {
            rules: [{
                required: true,
                whitespace: true,
                message: I18n.t('Register.repasswordTishi'),
            }, {
                validator: this.checkEmailPass2,
            }],
        });
        const formItemLayout = {
            labelCol: { span: 7 },
        };
        return(
            <Form horizontal className="Perphtwobox">
                <FormItem
                    {...formItemLayout}
                    hasFeedback
                    label={I18n.t('Register.compassword')}
                >
                    <Input {...passwdEmailProps} type="password" autoComplete="off" placeholder={I18n.t('Register.passwordTishi')}
                    />
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    hasFeedback
                    label={I18n.t('Register.recompassword')}
                >
                    <Input {...rePasswdEmailProps} type="password" autoComplete="off" placeholder={I18n.t('Register.pwTishi')}
                    />
                </FormItem>
                <FormItem>
                    <Button type="primary" onClick={this.HandleSubmit}>{I18n.t('forgetpassword.next')}</Button>
                </FormItem>
            </Form>
        )
    }
})
PerPhonetwo = createForm()(PerPhonetwo);
export default PerPhonetwo;
