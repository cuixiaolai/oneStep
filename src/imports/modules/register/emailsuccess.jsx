import React, { Component, PropTypes } from 'react';
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';
import { Button, Form, Input,Select,Option ,Checkbox,Tabs,Icon} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import Rightnow from '../Rightnow.jsx';
import config from '../../config.json';


export default class Emailsuccess extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            opacity: true,
            current: 0,
            num:5,
        });
    }
    handleClick(e) {

    }

    getEmail() {
        if (this.props.user != null){
            return this.props.user.emails[0].address;
        }
    }
    // componentWillMount() {
    //     let route = window.location.pathname;
    //     if (route == '/verify-email') {
    //         this.setState({current: 1});
    //     }
    //     else {
    //         let arr = route.split('/');
    //         let token = arr[arr.length-1];
    //         Accounts.verifyEmail(token, function(err) {
    //             if(err) {
    //                 this.setState({current: 0});
    //                 alert(err);
    //             }
    //             else {
    //                 this.setState({current: 0});
    //             }
    //         }.bind(this));
    //     }
    // }
    click(){
        this.setState({opacity: false});
        setTimeout(()=> {
            this.setState({opacity: true})
        },1000*5);
        console.log(this.state.current);
    }

    render(){
        if (this.state.current == 0){
            setTimeout(()=> {
                console.log(this.props.path);
                this.props.history.pushState(null,'list/'+ Id +'/page/'+index);  这个不可以吗
            },1000*5);
            return(
                <div>
                    <ul className="Emailsuccess">
                        <li>
                            <img src={(config.theme_path + '/bigright_03.png')} alt=""/>
                            <span>{I18n.t('emailSuccess.Congratulations')}</span>
                        </li>
                        <li>
                            <span ref="three">{this.state.num}</span>
                            <span>{I18n.t('emailSuccess.Jump')}</span>
                        </li>
                        <li>
                            <Rightnow />
                        </li>
                    </ul>
                </div>
            )
        }
        else if (this.state.current == 1){
            let success = this.refs.success;
            if(this.state.opacity == false){
                success.style.opacity = 1;
            }
            return (
                <div>
                    <div className="success" ref="success" style={this.state.opacity ? {opacity: 0} : {opacity: 1}}>
                        <p>
                            <img src={(config.theme_path + "/right_03.png")} alt=""/>
                            <span>{I18n.t('emailWait.agains')}</span>
                        </p>
                    </div>
                    <ul className="Emailwait">
                        <li>
                            <img src={(config.theme_path + '/bigright_03.png')} alt=""/>
                            <span>{I18n.t('emailSuccess.Congratulations')}</span>
                        </li>
                        <li>
                            <span>{I18n.t('emailWait.first')}</span><span>{this.getEmail()}</span><span>{I18n.t('emailWait.please')}</span>
                        </li>
                        <li>
                            <Button type="primary" onClick={this.handleClick}><Translate
                                value="emailWait.denglu"/></Button>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <span>{I18n.t('emailWait.ifnot')}</span>
                                    <span onClick={this.click.bind(this)} style={{
                                        color: "#0685da",
                                        cursor: 'pointer'
                                    }}>{I18n.t('emailWait.again')}</span>
                                    <span>{I18n.t('emailWait.email')}</span>
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            )
        }
    }
}
export default createContainer(() => {
    return {
        user: Meteor.user(),
    };
}, Emailsuccess);