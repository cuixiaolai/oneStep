import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Input, Button, Checkbox,message } from 'antd';
import VerifyCode from '../VerifyCode.jsx';
// import { Meteor } from 'meteor/meteor';
const FormItem = Form.Item
const createForm = Form.create;
import config from '../../config.json';

let PerPhoneone = React.createClass({
    getInitialState() { 
        return {
            verify_code: this.generateCode(),
            error:I18n.t('forgetpassword.one.error'),
            flag:false,
            phone:this.props.flag,
            send:false,
            sendverify:false,
            num:60,
            sendmessage:I18n.t('sendmessage'),
            sends:false,
            false:false,
            message:I18n.t('Register.phoneTishi'),
            validateStatus:'',
        };
    },
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
            //     console.log('Errors in form!!!');
            //     return;
            }else{
                // Meteor.call('phone.phoneSubmit', values.phone, values.message, (err) => {
                //     // need to change
                //     if (err) {
                //         console.log(err);
                //     } else {
                        this.props.phone(values.phone);
                        this.props.next();
                        clearInterval(this.interval);
                //     }
                // });


            }
        });
    },
    userPhoneExists(rule, value, callback) {
        console.log(value);
        let This = this;
        let react = /^1[34578]{1}\d{9}$/ ;
        if (!value || value == undefined) {
            callback(I18n.t('Register.phoneTishi'));
        } else {
            // setTimeout(() => {
            //     Meteor.call('user.checkPhone', value, (err) => {
            //         if(!react.test(value)){
                        // callback(I18n.t('Register.phonetishis'));
            //         }else{
            //             if (err) {
                            // callback([new Error(I18n.t('Register.sorryPhone'))]);
            //             } else {
                            callback();
            //             }
            //         }
            //     });
            // }, 100);
        }
    },
    checkPhoneVerifyCode(rule, value, callback){
        if (!value) {
            callback();
        } else {
            // setTimeout(() => {
                const phone_number = this.props.form.getFieldValue('phone');
            //     Meteor.call('phone.checkVerifyCode', phone_number, value, (err,result) => {
            //         if(result){
            //             callback();
            //         }else{
            //             callback([new Error('验证码错误')]);
            //         }
            //     });
            // },100)
            const phonenum = {};
                                const url = "http://localhost:3001/api/v1/phone";
                 //    var xhr_rate = new XMLHttpRequest();
                 //    xhr_rate.open('post', url, true);
                 //    xhr_rate.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                 //    phonenum.phoneCode=phone_number;
                 //    var fd = new FormData();
                 //    fd.append('phoneCode', value);
                 //    xhr_rate.send(fd);
                 //    xhr_rate.onload = function (event) {
                 //        const rate_ret = JSON.parse(xhr_rate.responseText)
                 //        console.log(rate_ret)
                 //        if (rate_ret.code == 200) {
                 //            console.log(rate_ret.body)
                 // // this.setState({ img: rate_ret.body });
                 //        }
                 //    }.bind(this)
              
                 fetch(url, {
                    credentials: 'include',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    phoneCode: value,
    login: 'hubot',
  }),

  // credentials: 'include'
}).then(response=>{
    return  response.json()

}).then(body=>{
    console.log("body.code")
    console.log(body.code)
    if(body.code==200){
     return   callback()
    }
    callback([new Error('验证码错误')])

}).catch(err=>console.log(err))
        }

    },
    checkVerifyCode(rule, value, callback) {
        if (value && value.toLowerCase() != this.state.verify_code.toLowerCase()) {
            callback(I18n.t('Register.verifyCodeError'));
        } else {
            callback();
        }
    },
    generateCode() {
        let items = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPRSTUVWXYZ23456789'.split('');
        let randInt = function (start, end) {
            return Math.floor(Math.random() * (end - start)) + start;
        }
        let code = '';
        for (let i = 0; i < 4; ++i) {
            code += items[randInt(0, items.length)];
        }
        return code;
    },
    refresh() {
        if(this.state.sends==false) {
            this.props.form.resetFields(['yan']);
            this.setState({verify_code: this.generateCode()});
        }
    },
    click(){
        if(this.state.sends==false){
            this.props.form.validateFields(['yan','phone'],(errors, values) => {
                if (errors) {
                    if (this.props.form.getFieldError('yan') == I18n.t('Register.verifyCodeError')) {
                        this.setState({ verify_code: this.generateCode() });
                    }
                    console.log('Errors in form!!!');
                    return;
                }else{
                    const phone_number = this.props.form.getFieldValue('phone');
                    console.log(phone_number);
                    // Meteor.call('phone.sendMessage', phone_number, (err) => {
                    //     if (err) {
                    //         message.error('每天只能发送三次验证码'); //in fact three times for each phone_number
                    //         console.log(err);
                    //     } else {
                    //         // this.refresh();
                    //         this.interval = setInterval(this.tick, 1000);
                    //         this.state.sends=true;
                    //     }
                    // });
                                        const url = "http://localhost:3001/api/v1/phone/"+phone_number;
                 //    var xhr_rate = new XMLHttpRequest();
                 //    xhr_rate.open('get', url, true);
                 //    xhr_rate.send();
                 //    xhr_rate.onload = function (event) {
                 //        const rate_ret = JSON.parse(xhr_rate.responseText)
                 //        console.log("rate_ret.body")
                 //        console.log(rate_ret)
                 //        if (rate_ret.code == 200) {
                 //            console.log(rate_ret.body)
                 //            console.log("rate_ret.body")
                 // // this.setState({ img: rate_ret.body });
                 //        }
                 //    }.bind(this)
                                     fetch(url, {
                    credentials: 'include',
  method:"GET"

  // credentials: 'include'
}).then(
(body)=>{
    console.log("body")
    console.log(body)
}
).catch(err=>{
    console.log("err")
    console.log(err)
})
                }
            });
        }
    },
    tick(){
        console.log(this.state.sends);
        if(this.state.sends==true) {
            this.state.num-=1;
            this.setState({sendmessage:this.state.num+I18n.t('sendagain')});
            console.log(this.state.num);
            console.log(this.state.num+I18n.t('sendagain'));
        }
        if(this.state.num==0){
            clearInterval(this.interval);
            this.setState({sendmessage:I18n.t('sendmessage'),num:60,sends:false});
            console.log(this.state.sends);
        }
    },
    render(){
        const { getFieldProps ,getFieldError, isFieldValidating } = this.props.form;
        const phoneProps = getFieldProps('phone', {
            validate: [{
                rules: [
                    { validator: this.userPhoneExists },
                ],
                trigger: 'onBlur',
            }],
        });
        const yanProps = getFieldProps('yan', {
            validate: [{
                rules: [
                    { required: true, min: 4,max:4,message:I18n.t('Register.yanTishi')},
                    { validator: this.checkVerifyCode }
                ],
                trigger: 'onBlur'
            }],
        });
        const messageProps = getFieldProps('message', {
            validate: [{
                rules: [
                    { required: true, min: 1, message:'请输入验证码'},
                    { validator: this.checkPhoneVerifyCode }
                ],
                trigger: 'onBlur'
            }],
        });
        const formItemLayout = {
            labelCol: { span: 7 },
        };
        return(
            <Form horizontal form={this.props.form} className="Perphonebox">
                <FormItem
                    {...formItemLayout}
                    hasFeedback
                    label={I18n.t('Register.perPhone.phone')}
                >
                    <Input {...phoneProps}  placeholder={I18n.t('Register.perPhone.phonePlear')} />
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    hasFeedback
                    label={I18n.t('Register.perPhone.phonever')}
                >
                    <Input {...yanProps} type="text" placeholder={I18n.t('Register.perPhone.verification')} />
                    <VerifyCode code={this.state.verify_code} />
                    <div style={{position:'absolute',top:38,right:0}}>{I18n.t('kan')}
                        <span onClick={this.refresh} style={this.state.sends ? {  cursor: 'auto',color:'#0a66bc'} : {  cursor:'pointer',color:'#0a66bc'}}>{I18n.t('shu')}</span>
                    </div>
                    <div className="clear"></div>
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    hasFeedback
                    label={I18n.t('Register.perPhone.messagever')}
                >
                    <Input {...messageProps} type="text" placeholder={I18n.t('Register.perPhone.verificationmessage')} maxLength='4' ref='verificationmessage'    />
                    <div className="send" style={this.state.sends ? {  cursor: 'auto',color:'#999999'} : {  cursor:'pointer',color:'#5b5b5b'}} onClick={this.click}>
                        {this.state.sendmessage}
                    </div>
                    <div className="clear"></div>
                </FormItem>
                <FormItem style={{height:'auto'}}>
                    <Button type="primary" onClick={this.handleSubmit}>{I18n.t('forgetpassword.next')}</Button>
                </FormItem>
                <div className="switch">
                    <span>{I18n.t('Register.switch')}</span>
                    <span style={{color:'#0a66bc'}} onClick={this.props.click}>{I18n.t('Register.emailswitch')}</span>
                    <img src={config.theme_path + '/switch.png'} alt=""/>
                </div>
            </Form>
        )
    }
})
PerPhoneone = createForm()(PerPhoneone);
export default PerPhoneone;
