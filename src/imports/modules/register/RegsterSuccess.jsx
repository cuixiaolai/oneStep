import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Input, Checkbox,Button } from 'antd';
// import { Meteor } from 'meteor/meteor'
import { browserHistory } from 'react-router';

export default class RegsterSuccess extends Component{
    render(){
        return(
            <div className="Rightnow">
                <Link to="/personal_center"><Button type="primary"><Translate value="Register.perPhone.successper" /></Button></Link>
                <Link to="/"><Translate value="Register.perPhone.login" /></Link>
            </div>
        )
    }
}
