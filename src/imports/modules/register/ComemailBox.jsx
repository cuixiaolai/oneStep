import React, { Component } from 'react';
import ComEmailone from './ComEmailone.jsx';
import Comemailtwo from './Comemailtwo.jsx';
import PerPhonethree from './PerPhonethree.jsx';

export default class ComemailBox extends Component{
    PerEmail(){
        if(this.props.current == 1){
            return(
                <Comemailtwo next={this.props.next}  email={this.props.email}/>
            )
        }else if(this.props.current == 2){
            return(
                <PerPhonethree />
            )
        }else if(this.props.current == null || this.props.current == 0){
            return(
                <ComEmailone next={this.props.next} />
            )
        }
    }
    render(){
        console.log(this.props)
        return(
            <div className="PerphoneBox">
                {this.PerEmail()}
            </div>

        )
    }
}
