import React, { Component } from 'react';
import PerPhoneone from './PerPhoneone.jsx';
import PerPhonetwo from './PerPhonetwo.jsx';
import PerPhonethree from './PerPhonethree.jsx';

export default class PerphoneBox extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            phone: '',
        });
    }
    setPhone(code) {
        this.setState({phone: code});
    }
    PerPhone(){
        if(this.props.current==1){
            return(
                <PerPhonetwo next={this.props.next} phone={this.state.phone}/>
            )
        }else if(this.props.current==2){
            return(
                <PerPhonethree />
            )
        }else if(this.props.current==null||this.props.current==0){
            return(
                <PerPhoneone next={this.props.next} click={this.props.click} phone={this.setPhone.bind(this)}/>
            )
        }
    }
    render(){
        console.log(this.props.current);
        return(
            <div className="PerphoneBox">
                {this.PerPhone()}
            </div>
        )
    }
}
