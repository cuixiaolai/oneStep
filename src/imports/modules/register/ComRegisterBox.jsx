import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Steps, Button } from 'antd';
const Step = Steps.Step;
import config from '../../config.json';
import ComemailBox from './ComemailBox.jsx';
const array = Array.apply(null, [5,6,7]);
const steps = array.map((item,i) => ({
    title: I18n.t('Register.top.one')
}));

export default class ComRegisterBox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            current: 0,
            style: 152,
            email: '',
        });
    }
    next() {
        let current = this.state.current + 1;
        let style=this.state.style + 210;
        if (current === steps.length) {
            current = 0;
            style = 152;
        }
        this.setState({ current,style });
    }
    componentWillMount() {
        if (this.props.callback == true) {
            let current = this.state.current + 1;
            let style=this.state.style + 210;
            if (current === steps.length) {
                current = 0;
                style = 152;
            }
            this.setState({ current, style });
        }
    }
    render(){
        const { current } = this.state;
        return (
            <div className="perPhoneregister">
                <Steps current={current}>
                    <Step title={I18n.t('Register.top.one')}  key="1" />
                    <Step title={I18n.t('Register.top.two')}  key="2" />
                    <Step title={I18n.t('Register.top.three')}  key="3" />
                    <div className="bigXian"></div>
                    <img  src={(config.theme_path + '/center.png')} alt="" ref="picShaw" style={{left:this.state.style+'px',width:54}}  />
                </Steps>
                <ComemailBox current={this.state.current} next={this.next.bind(this)} email={this.props.email}/>
            </div>
        )
    }
}
