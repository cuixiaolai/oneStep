import React, { Component, propTypes } from 'react';
import { I18n } from 'react-redux-i18n';
import { browserHistory } from 'react-router';
import config from '../../config.json';
import RegsterSuccess from './RegsterSuccess.jsx'

export default class PerPhonethree extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            count: 5
        });
    }
    tick(){
        if (this.state.count == 0) {
            browserHistory.replace('/personal_center');
        }
        else {
            this.setState({count:this.state.count - 1});
        }
    }
    componentDidMount(){
        this.interval = setInterval(this.tick.bind(this), 1000);
    }
    componentWillUnmount(){
        clearInterval(this.interval);
    }
    render(){
        return(
            <ul className="Emailwait">
                <li>
                    <img src={(config.theme_path + '/bigright_03.png')} alt=""/>
                    <span>{I18n.t('Register.perPhone.success')}</span>
                    <span>{I18n.t('Register.perPhone.auth')}</span>
                </li>
                <li>
                    <span>{I18n.t('Register.perPhone.number')}</span>
                </li>
                <li>
                    <span>{this.state.count}</span>
                    <span>{I18n.t('Register.perPhone.miao')}</span>
                    <div className="clear"></div>
                </li>
                <RegsterSuccess />
            </ul>
        )
    }
}
