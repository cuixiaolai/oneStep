import React, { Component } from 'react';
import { Link } from 'react-router';

export default class RegisterBox extends Component{
    render(){
        console.log(this.props.pic);
        let pic=this.props.pic;
        return(
            <div className="registerSmallBox">
                <div className="shaw"  style={this.props.style} ></div>
                <img className="regis-img-sm" src={pic} alt=""/>
                <Link to={this.props.a}>
                    <div className="registerButton" style={this.props.button}>
                        <span style={this.props.color}>{this.props.link}</span>
                        <img src={this.props.jian} alt=""/>
                    </div>
                </Link>
                <p style={this.props.color}>{this.props.words}</p>
                <p style={this.props.color}>{this.props.words2}</p>
            </div>
        )
    }
}