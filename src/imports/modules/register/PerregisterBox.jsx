import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Steps, Button } from 'antd';
import config from '../../config.json';
import PerphoneBox from './PerphoneBox.jsx';
import PeremailBox from './PeremailBox.jsx';
const Step = Steps.Step;
const array = Array.apply(null, [5,6,7]);
const steps = array.map((item,i) => ({
    title: I18n.t('Register.top.one')
}));

export default class PerregisterBox extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            current: 0,
            style: 152,
            flag: true,
            email: '',
        });
    }
    next() {
        let current = this.state.current + 1;
        let style=this.state.style + 210;
        if (current === steps.length) {
            current = 0;
            style = 152;
        }
        this.setState({ current,style });
    }

    componentWillMount() {
        if (this.props.callback == true) {
            let current = this.state.current + 1;
            let style=this.state.style + 210;
            if (current === steps.length) {
                current = 0;
                style = 152;
            }
            let flag = false;
            this.setState({ current, style, flag });
        }
    }

    flag(){
        if(this.state.flag == true){
            return(
                <PerphoneBox current={this.state.current} next={this.next.bind(this)} click={this.click.bind(this)} />
            )
        }
        else if(this.state.flag == false){
            return(
                <PeremailBox current={this.state.current} next={this.next.bind(this)} click={this.click.bind(this)} email={this.props.email}/>
            )
        }
    }

    click(){
        let flag=!this.state.flag;
        this.setState({flag});
    }

    render(){
        const { current } = this.state;
         return(
            <div className="perPhoneregister">
                <Steps current={current}>
                    <Step title={I18n.t('Register.top.one')}  key="1" />
                    <Step title={I18n.t('Register.top.two')}  key="2" />
                    <Step title={I18n.t('Register.top.three')}  key="3" />
                    <div className="bigXian"></div>
                    <img src={(config.theme_path + '/center.png')} alt="" ref="picShaw" style={{left:this.state.style+'px',width:54}}  />
                </Steps>
                {this.flag()}
            </div>
        )
    }
}
