import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Input, Button, Checkbox, message} from 'antd';
import VerifyCode from '../VerifyCode.jsx';
// import { Meteor } from 'meteor/meteor';
const FormItem = Form.Item;
const createForm = Form.create;
import config from '../../config.json';

let ComEmailone = React.createClass({
    getInitialState() {
        return {
            verify_code: this.generateCode(),
            error: I18n.t('forgetpassword.one.error'),
            flag: false,
            phone: this.props.flag,
            num: 1,
            email: '',
        };
    },

    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }else{
                this.setState({ num: 0, email: values.email});
                this.sendEmail(values.email);
            }
        });
    },

    userEmailExists(rule, value, callback) {
        if (!value) {
            callback();
        } else {
            // setTimeout(() => {
            //     Meteor.call('user.checkEmail', value, (err) => {
            //         if (err) {
            //             callback([new Error(I18n.t('Register.sorryEmail'))]);
            //         } else {
            //             callback();
            //         }
            //     });
            // }, 100);
        }
    },

    generateCode() {
        let items = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPRSTUVWXYZ23456789'.split('');
        let randInt = function (start, end) {
            return Math.floor(Math.random() * (end - start)) + start;
        }
        let code = '';
        for (let i = 0; i < 4; ++i) {
            code += items[randInt(0, items.length)];
        }
        return code;
    },

    refresh() {
        this.props.form.resetFields(['yan']);
        this.setState({ verify_code: this.generateCode() });
    },

    checkVerifyCode(rule, value, callback) {
        if (value && value.toLowerCase() != this.state.verify_code.toLowerCase()) {
            callback(I18n.t('Register.verifyCodeError'));
        } else {
            callback();
        }
    },

    sendEmail(email) {
        // Meteor.call('accounts.comVerificationEmail', email);
    },

    getEmail() {
        if (this.state.email != ''){
            let email = this.state.email;
            let arr = email.split('@');
            if (arr[0] <= 4) {
                let substr = arr[0].substring(1);
                substr = substr.replace(/[^\n\r]/g, '*');
                email = arr[0][0] + substr + '@' + arr[1];
            }
            else {
                let begin = (arr[0].length - 4)%2 === 0 ? (arr[0].length - 4)/2 : (arr[0].length - 3)/2
                console.log(begin);
                let substr = arr[0].substring(begin, begin+4);
                substr = substr.replace(/[^\n\r]/g, '*');
                email = arr[0].substring(0, begin) + substr + arr[0].substring(begin+4) + '@' + arr[1];
            }
            return email;
        }
    },

    resend() {
        message.success(I18n.t('Register.perEmail.success'));
        // Meteor.call('accounts.perVerificationEmail', this.state.email);
    },

    show(){
        if (this.state.num == 1){
            const { getFieldProps ,getFieldError, isFieldValidating } = this.props.form;
            const EmailProps = getFieldProps('email', {
                validate: [{
                    rules: [
                        { required: true, message: I18n.t('Register.emailTishi') },
                    ],
                    trigger: 'onBlur',
                }, {
                    rules: [
                        { type: 'email', message:  I18n.t('Register.pleaseemailTishi') },
                        { validator: this.userEmailExists},
                    ],
                    trigger: ['onBlur', 'onChange'],
                }],
            });
            const yanProps = getFieldProps('yan', {
                rules: [
                    { required: true, min: 4,max:4,message:I18n.t('Register.yanTishi')},
                    { validator: this.checkVerifyCode }
                ],
            });
            const formItemLayout = {
                labelCol: { span: 7 },
            };
            return(
                <Form horizontal form={this.props.form} className="Perphonebox">
                    <FormItem
                        {...formItemLayout}
                        hasFeedback
                        label={I18n.t('Register.com.email')}
                    >
                        <Input {...EmailProps} type="text" placeholder={I18n.t('Register.perEmail.emailPlear')} />
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        hasFeedback
                        label={I18n.t('Register.perPhone.phonever')}
                    >
                        <Input {...yanProps} type="text" placeholder={I18n.t('Register.perPhone.verification')} />
                        <VerifyCode code={this.state.verify_code} />
                        <div style={{position:'absolute',top:38,right:0}}>{I18n.t('kan')} <span style={{color:'#0a66bc',cursor:'pointer'}} onClick={this.refresh}>{I18n.t('shu')}</span> </div>
                        <div className="clear"></div>
                    </FormItem>
                    <FormItem  style={{height:'auto'}}>
                        <Button type="primary" onClick={this.handleSubmit}>{I18n.t('Register.perEmail.yanemail')}</Button>
                    </FormItem>
                </Form>
            )
        }
        else if(this.state.num==0){
            return(
                <div className="sendsuccess">
                    <p>
                        <span>{I18n.t('Register.perEmail.send')}</span>
                        <span style={{color:'#0a66bc'}}>{this.getEmail()}</span>
                    </p>
                    <p>
                        {I18n.t('Register.perEmail.please')}
                    </p>
                    <p>
                        <p>
                            {I18n.t('Register.perEmail.no')}
                            <div className="senderrorBox">
                                <p>
                                    <img src={config.theme_path + '/origin.png'} alt=""/>
                                    <span>{I18n.t('Register.perEmail.reasonone')}</span>
                                    <div className="clear"></div>
                                </p>
                                <p>
                                    <img src={config.theme_path + '/origin.png'} alt=""/>
                                    <span>{I18n.t('Register.perEmail.reasontwo')} <span onClick={this.resend} style={{color:"#0a66bc",cursor:'pointer'}}>{I18n.t('Register.perEmail.again')}</span> </span>
                                    <div className="clear"></div>
                                </p>
                                <div className="jiao"></div>
                            </div>
                        </p>
                    </p>
                </div>
            )
        }
    },

    render(){
        return(
            this.show()
        )
    }
})
ComEmailone = createForm()(ComEmailone);
export default ComEmailone;
