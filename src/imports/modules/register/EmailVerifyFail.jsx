import React, { Component, PropTypes } from 'react';
// import { createContainer } from 'meteor/react-meteor-data';
import { Button, Form, Input,Select,Option ,Checkbox,Tabs,Icon} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';


export default class EmailVerifyFail extends Component {
    render(){
        return(
            <div className="Emailerror">
                <div className="Emailerror-inner">
                    <ul>
                        <li>
                            <img src={(config.theme_path + ('/emailerror.png'))} alt=""/>
                            <div className="Emailerror-name left">
                                {I18n.t('Emailerror.name')}
                            </div>
                            <div className="clear"></div>
                        </li>
                        <li>
                            {I18n.t('Emailerror.reason')}
                        </li>
                        <li>
                            {I18n.t('Emailerror.one')}
                        </li>
                        <li>
                            {I18n.t('Emailerror.two')}
                        </li>
                        <li>
                            {I18n.t('Emailerror.three')}
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}