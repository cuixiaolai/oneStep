import React, { Component } from 'react';
import { Link, browserHistory  } from 'react-router';
import { Button,Icon} from 'antd';
import config from '../config.json';
import { Translate, I18n } from 'react-redux-i18n';

export default class MyTr extends Component{
    constructor(state, props) {
        super(state);
		super(props);
        this.state = ({
            suozaidi:false,
            chandi:false,
            pihao:false,
            tidu:false,
            renmingbi:false,
            meiyuan:false,
            baozhuangfangshi:false,
        });
    }
    Showsuozaidi(){
        // date/data
        let suozaidi = [];
        let length = this.state.suozaidi == false&&this.props.date.suozaidi.length >= 3 ? 3 : this.props.date.suozaidi.length;
        for(let i =0 ;i<length;i++){
            suozaidi[i] = (
                <p>{this.props.date.suozaidi[i][0]}/{this.props.date.suozaidi[i][1]}</p>
            )
        }
        return suozaidi;
    }
    Showchandi(){
        let chandi = [];
        let length = this.state.chandi == false&&this.props.date.chandi.length >= 3 ? 3 : this.props.date.chandi.length;
        for(let i =0 ;i<length;i++){
            chandi[i] = (
                <p>{this.props.date.chandi[i][0]}/{this.props.date.chandi[i][1]}</p>
            )
        }
        return chandi;
    }
    Showpihao(){
        let pihao = [];
        let length = this.state.pihao == false&&this.props.date.pihao.length >= 3 ? 3 : this.props.date.pihao.length;
        for(let i =0 ;i<length;i++){
            pihao[i] = (
                <p>{this.props.date.pihao[i][0]}/{this.props.date.pihao[i][1]}</p>
            )
        }
        return pihao;
    }
    Showtidu(){
        let tidu = [];
        let length = this.state.tidu == false&&this.props.date.tidu.length >= 3 ? 3 : this.props.date.tidu.length;
        for(let i =0 ;i<length;i++){
            tidu[i] = (
                <p>{this.props.date.tidu[i]}+</p>
            )
        }
        return tidu;
    }
    Showrenmingbi(){
        let renmingbi = [];
        let length = this.state.renmingbi == false&&this.props.date.renmingbi.length >= 3 ? 3 : this.props.date.renmingbi.length;
        for(let i =0 ;i<length;i++){
            renmingbi[i] = (
                <p>￥{this.props.date.renmingbi[i]}</p>
            )
        }
        return renmingbi;
    }
    Showmeiyuan(){
        let meiyuan = [];
        let length = this.state.meiyuan == false&&this.props.date.meiyuan.length >= 3 ? 3 : this.props.date.meiyuan.length;
        for(let i =0 ;i<length;i++){
            meiyuan[i] = (
                <p>${this.props.date.meiyuan[i]}</p>
            )
        }
        return meiyuan;
    }
    Showbaozhuangfangshi(){
        let baozhuangfangshi = [];
        let length = this.state.baozhuangfangshi == false&&this.props.date.baozhuangfangshi.length >= 3 ? 3 : this.props.date.baozhuangfangshi.length;
        for(let i =0 ;i<length;i++){
            baozhuangfangshi[i] = (
                <p>{this.props.date.baozhuangfangshi[i][0]}/{this.props.date.baozhuangfangshi[i][1]}</p>
            )
        }
        return baozhuangfangshi;
    }
    ShowWords(word){
        if(this.props.date[word].length>3){
            console.log(this.state[word]);
            if(this.state[word]==false){
                return(
                    <p onClick={this.ShowMore.bind(this,word)} style={{cursor:'pointer',color:'#ed7020'}}>
                        {I18n.t('PersonalCenter.manageStock.more')}
                        <img style={{marginLeft:2}} src={(config.theme_path +　'origindown_03.png')} alt=""/>
                    </p>
                )
            }
            else{
                return(
                    <p onClick={this.ShowMore.bind(this,word)}  style={{cursor:'pointer',color:'#ed7020'}}>
                        {I18n.t('CompantInvoicsThree.hidden')}
                        <img style={{marginLeft:2,transform: 'rotate(180deg)'}} src={(config.theme_path +　'origindown_03.png')} alt=""/>
                    </p>
                )
            }
        }
    }
    ShowMore(word){
        if(word == 'tidu'){
            this.setState({[word]: !this.state[word], meiyuan: !this.state.meiyuan, renmingbi: this.state.renmingbi});
        }
        else{
            this.setState({[word]: !this.state[word]});
        }
    }
    addToCart(){
     console.log("addToCart ........ ");
        this.props.addToCart(this.props.date,this.props.stocktype);
    }
    openToPayment(){
        this.props.openToPayment(this.props.date,this.props.stocktype);
    }
    goTop() {
        console.log('kengaaaa');
        window.scrollTo(0,0);
    }
    render(){
        console.log(this.props);
        console.log("ddddddddddd");
        return(
            <tr>
                <td>
                    {this.props.date.name}
                </td>
                <td style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    <span style={{cursor:'pointer'}}><Link to={'/product_details/'+this.props.date.key}>{this.props.date.money}</Link></span>
                </td>
                <td style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    {this.props.date.address}
                </td>
                <td className="MyTabledescribe">
                    <a href={(config.file_server + this.props.date.datasheet)} target="_blank" className="watch-datasheet" style={{display: 'block'}}><Icon type="download" />{I18n.t('ShoppingCart.Specifications')}</a>
                    <p className="look-pic"><Icon type="eye-o" />{I18n.t('Search.look')}</p>
                    <img className="model-pic" src={(config.file_server + this.props.date.img)} alt={I18n.t('Search.noImg')}/>
                </td>
                <td >
                    {this.props.date.kucun}
                </td>
                <td name='suozaidi' style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    {this.Showsuozaidi()}
                    {this.ShowWords('suozaidi')}
                </td>
                <td name='chandi' style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    {this.Showchandi()}
                    {this.ShowWords('chandi')}
                </td>
                <td name='pihao' style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    {this.Showpihao()}
                    {this.ShowWords('pihao')}
                </td>
                <td name='tidu' style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    {this.Showtidu()}
                    {this.ShowWords('tidu')}
                </td>
                <td name='renmingbi'>
                    {this.Showrenmingbi()}
                </td>
                <td name='meiyuan'>
                    {this.Showmeiyuan()}
                </td>
                <td name='baozhuangfangshi' style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    {this.Showbaozhuangfangshi()}
                    {this.ShowWords('baozhuangfangshi')}
                </td>
                <td style={{minWidth:'80px'}}>
                    <div className="MyTabliBtn" onClick={this.openToPayment.bind(this)} >{I18n.t('ShoppingCart.rightnow')}</div>
                    <div className="MyTabliBtnshop" onClick={this.addToCart.bind(this)}>{I18n.t('Search.shopping')}</div>
                </td>
            </tr>
        )
    }
}
