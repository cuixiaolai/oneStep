import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
// import { Meteor } from 'meteor/meteor'
import config from '../config.json';
import { Badge,Icon,Input, Button, Select } from 'antd';
// import { Product } from '../api/Product.js';
// import { Cart } from '../api/Cart.js';
// import { createContainer } from 'meteor/react-meteor-data';
import classNames from 'classnames';
const InputGroup = Input.Group;
const search = ['电机/电磁阀/驱动器板/模块','美国 +45','德国 +56','台湾 +93','港澳 +10'];
const Option = Select.Option;
import Products from './Product.jsx';

import Reducer from '../reducer/Index.js'
import { connect } from 'react-redux';

 class TopBoxView extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            data: [],
            value: '',
            focus: false,
            search: search[0],
            selectedOptions:[],
        });
    }
    handleChange(e) {
        this.setState({value: e});
        if (e.trim() == '') {
            this.setState({data: []});
        }
        else {
            let q = '^' + e.trim().toUpperCase();
            let result = [];
            // let cursor = Product.find({"Manufacturer Part Number": {$regex: q}}, {
            //     fields: {
            //         'Manufacturer Part Number': 1,
            //         '_id': 0
            //     }, sort: ['Manufacturer Part Number', 'asc'], limit: 15
            // });
            // console.log(cursor);
            // cursor.forEach(function (item) {
            //     result.push({value: item["Manufacturer Part Number"], text: item["Manufacturer Part Number"]});
            // });
            this.setState({data: result});
        }
    }
    handleProvinceChange(value,selectedOptions) {
        this.props.onChange(value,selectedOptions);
        this.setState({
            search: value,
            selectedOptions:value,
        });
    }
    handleSubmit() {
        browserHistory.replace('/Search/'+encodeURIComponent(this.state.value.trim()));
    }
    handleFocusBlur(e) {
        this.setState({
            focus: e.target === document.activeElement,
        });
    }
    show(){
        let route=window.location.pathname;
        let welcome;
        let ifhave;
        let login;
        let please;
        if(route=='/login'||route=='/register'||route.match('/perRegister')||route.match('/comRegister')||route=="/forget"){
            if(route=='/login'){
                welcome=I18n.t('top.Register.welD');
                ifhave=I18n.t('top.Register.nohave');
                login=<Link to='/register' >{I18n.t('top.please')}{I18n.t('top.Register.zhu')}</Link>;
            }else if(route=='/register'||route.match('/perRegister')||route.match('/comRegister')){
                welcome=I18n.t('top.Register.welcome');
                ifhave=I18n.t('top.Register.have');
                login=<Link to='/login' >{I18n.t('top.please')}{I18n.t('top.Register.login')}</Link>;
            }else if(route=="/forget"){
                welcome=I18n.t('top.Register.find');
                ifhave='';
                login=<Link to='/register' >{I18n.t('top.Register.register')}</Link>;
                please= <Link to='/login' >{I18n.t('top.Register.login')}/</Link>;
            }
            return(
                <div id="topBox" className="Topbox">
                    <div className="Topbox-inner">
                        <Link to="/"><img src={(config.theme_path + '/logo.png')} alt="" className="left" /></Link>
                        <div className="longxian left"></div>
                        <div className="left cheng">
                            <p>{welcome}</p>
                        </div>
                        <div className="right deng">
                            <p>{ifhave}</p>
                            <span>{please}</span>
                            <span>{login}</span>
                        </div>
                        <div className="clear"></div>
                    </div>
                </div>
            )
        }else{
            welcome=I18n.t('top.name');
            const provinceOptions = search.map(province => <Option key={province}>{province}</Option>);
            const btnCls = classNames({
                'ant-search-btn': true,
                'ant-search-btn-noempty': !!this.state.value.trim(),
            });
            const searchCls = classNames({
                'ant-search-input': true,
                'ant-search-input-focus': this.state.focus,
            });
            const options = this.state.data.map(d => <Option key={d.value}>{d.text}</Option>);
            let size;
            if(this.props.cartInfo !=null){
                if(this.props.cartInfo.size>99){
                    size = '99+';
                }else {
                    size = this.props.cartInfo.size;
                }
            }else {
                size = 0;
            }
            console.log(this.props);
            return(
                <div className="Topbox">
                    <div className="Topbox-inner">
                        <Link to="/"><img src={(config.theme_path + '/logo.png')} alt="" className="left" /></Link>
                        <div className="longxian left"></div>
                        <div className="left cheng">
                            <p>{welcome}</p>
                        </div>
                        <div className="Topbox-search left" style={{position:'relative'}}>
                            <Input.Group className={searchCls}>
                                <Select
                                    combobox
                                    value={this.state.value}
                                    placeholder={I18n.t('home.input')}
                                    notFoundContent=""
                                    defaultActiveFirstOption={false}
                                    showArrow={false}
                                    filterOption={false}
                                    onChange={this.handleChange.bind(this)}
                                    onFocus={this.handleFocusBlur.bind(this)}
                                    onBlur={this.handleFocusBlur.bind(this)}
                                >
                                    {options}
                                </Select>
                                <div className="ant-input-group-wrap">
                                    <Button style={{width:70,height:36}} type="primary" onClick={this.handleSubmit.bind(this)}> <Translate value="home.search" /> </Button>
                                </div>
                            </Input.Group>
                            <Products value={this.state.selectedOptions} style={{ width:90,height:34,position:'absolute',top:0,left:1,borderRadius:'4px 0 0 4px',borderRight:'1px solid #0a66bc',zIndex:98989}} onChange={this.handleProvinceChange.bind(this)} />
                        </div>
                        <Link to="/shopping_cart" style={{color:'#707070',textDecoration:'none'}}>
                        <div className="right shopping-card">
                            <div className="shopping-hui">
                                {size}
                            </div>

                            <div className="shopping-icon">
                                <Icon type="shopping-cart" />
                            </div>
                            <span>&nbsp;{I18n.t('top.shop')}</span>
                            <div className="clear"></div>
                        </div>
                        </Link>
                        <div className="clear"></div>
                    </div>
                </div>
            )
        }
    }
    render() {
        return(
            this.show()
        )
    }
}

export default connect(null, (dispatch) => {
    return {
        onChange: (value,selectedOptions) => {
            dispatch({
                type: 'CHANGE',
                new_category: value,
                new_options:selectedOptions,
                from:'topbox'
            });
        }
    }
})(TopBoxView);