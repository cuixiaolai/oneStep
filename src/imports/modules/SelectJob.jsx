import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
// import { Accounts } from 'meteor/accounts-base';
import { Select,DatePicker } from 'antd';
const Option = Select.Option;

const provinceData = ['请选择', '消费电子','汽车电子','医疗电子','照明显示','能源电力','网络与通信系统','智能手机与平板电脑','测试仪器及设备','国防航空','IC及元器件','行业分销服务','高校教育','安防','智能交通','其它'];
let SelectJob = React.createClass({
    getInitialState() {
        return {
            job: provinceData[0],
        };
    },
    handleProvinceChange(value) {
        this.props.industry(value);
        this.setState({
            job: value,
        });
    },
    componentWillMount() {
        if (this.props.default != null) {
            this.setState({job: this.props.default});
            this.props.industry(this.props.default);
        }
        else {
            this.props.industry(provinceData[0]);
        }
    },
    render(){
        const provinceOptions = provinceData.map(province => <Option key={province}>{province}</Option>);
        return(
            <Select size="large" defaultValue={this.state.job} style={this.props.style} onChange={this.handleProvinceChange}>
                {provinceOptions}
            </Select>
        )
    }
})
export default SelectJob;
