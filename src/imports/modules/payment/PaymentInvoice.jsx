import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Upload,Modal,Radio,Input,Checkbox,Cascader,Form,message } from 'antd';
import config from '../../config.json';
const RadioGroup = Radio.Group;
const RadioButton = Radio.Button;
import PaymentAddress from './PaymentAddress.jsx';

export default class PaymentInvoice extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            value:1, //发票管理
            InvoiceType:1,
            InvoiceAddress:1,
            title:'',
            email:'',
        });
    }
    InvoiceType(e){
        this.setState({InvoiceType: e.target.value});
        this.props.setInvoiceWay(e.target.value);
    }
    InvoiceAddress(e){
        this.setState({InvoiceAddress: e.target.value});
        this.props.setInvoiceAddressType(e.target.value)
    }
    ShowInvoice(){
        if(this.props.currentUserInfo.invoice == null || this.props.currentUserInfo.invoice.verify == null){

        }else{
            if(this.props.currentUserInfo.invoice.verify == true && this.props.currentUserInfo.invoice.success == true){
                return(
                    <Radio value={2}>{I18n.t('PaymentPage.zengzhi')}</Radio>
                )
            }
        }
    }
    title(e) {
        this.props.setInvoiceTitle(e.target.value);
    }
    email(e) {
        this.props.setInvoiceEmail(e.target.value);
    }
    InvoiceMessage(){
        if(this.state.value == 1){
            return(
                <div className="InvoiceMessageBox">
                    <p>
                        <div className="left" style={{lineHeight:'36px',marginRight:3}}>{I18n.t('PaymentPage.invoicehead')}</div>
                        <div className="left" style={{position:'relative'}}>
                            <Input placeholder={I18n.t('PaymentPage.words')} onChange={this.title.bind(this)}></Input>
                            <p style={this.props.titleflag ? {display:'block'} : {display:'none'}} className="InvoiceMessageBoxerror">{I18n.t('PaymentPage.error')}</p>
                        </div>
                        <div className="clear"></div>
                    </p>
                    <p>
                        <div className="left" style={{lineHeight:'36px',marginRight:3}}>{I18n.t('PaymentPage.invoicetype')}</div>
                        <div className="left">
                            <RadioGroup defaultValue={1} size="large" onChange={this.InvoiceType.bind(this)}>
                                <RadioButton value={1}>
                                    {I18n.t('PaymentPage.Electronicinvoice')}
                                    <img src={(config.theme_path + 'smallsan_03.png')} alt=""/>
                                </RadioButton>
                                <RadioButton value={2}>
                                    {I18n.t('PaymentPage.Untiltheinvoice')}
                                    <img src={(config.theme_path + 'smallsan_03.png')} alt=""/>
                                </RadioButton>
                            </RadioGroup>
                        </div>
                        <div className="left"  style={{position:'relative'}} >
                            <Input style={{width:240,height:30}} placeholder={I18n.t('PaymentPage.emaliaddress')} onChange={this.email.bind(this)}/>
                            <p  style={this.props.emailflag ? {display:'block'} : {display:'none'}} className="InvoiceMessageBoxerror">{I18n.t('Register.emailTishi')}</p>
                        </div>
                        <div className="clear"></div>
                    </p>
                </div>
            )
        }else if(this.state.value == 2){
            if(this.props.currentUserInfo != null){
                if(this.props.currentUserInfo.invoice == null || this.props.currentUserInfo.invoice.verify == null){

                }else{
                    if(this.props.currentUserInfo.invoice.verify == true && this.props.currentUserInfo.invoice.success == true){
                        return(
                            <div className="Valueaddedinvoice">
                                <p> <span>{I18n.t('CompantInvoicsThree.comname')}</span> {this.props.currentUserInfo.invoice.name}</p>
                                <p> <span>{I18n.t('CompantInvoicsThree.address')}</span>{this.props.currentUserInfo.invoice.address}</p>
                                <p> <span>{I18n.t('CompantInvoicsThree.phone')}</span>{this.props.currentUserInfo.invoice.tel}</p>
                                <p> <span>{I18n.t('CompantInvoicsThree.code')}</span>{this.props.currentUserInfo.invoice.id}</p>
                                <p> <span>{I18n.t('CompantInvoicsThree.band')}</span>{this.props.currentUserInfo.invoice.bank_name}</p>
                                <p> <span>{I18n.t('CompantInvoicsThree.bandnum')}</span>{this.props.currentUserInfo.invoice.bank_account}</p>
                            </div>
                        )
                    }
                }
            }
        }
    }
    onChange(e){
        this.setState({value: e.target.value});
        this.props.setInvoiceType(e.target.value);
    }
    render(){
        return(
            <div className="PaymentPageBoxInvoice">
                <RadioGroup onChange={this.onChange.bind(this)} value={this.state.value}>
                    <Radio key="a" value={1}>{I18n.t('PaymentPage.putong')}</Radio>
                    {this.ShowInvoice()}
                </RadioGroup>
                {this.InvoiceMessage()}
                <div className="InvoiceMessageBox">
                    <p>
                        <div className="left" style={{lineHeight:'36px',marginRight:3}}>{I18n.t('PaymentPage.invoiceaddress')}</div>
                        <div className="left">
                            <RadioGroup defaultValue={1} size="large" onChange={this.InvoiceAddress.bind(this)}>
                                <RadioButton value={1}>
                                    {I18n.t('PaymentPage.inaddress')}
                                    <img src={(config.theme_path + 'smallsan_03.png')} alt=""/>
                                </RadioButton>
                                <RadioButton value={2}>
                                    {I18n.t('PaymentPage.otheraddress')}
                                    <img src={(config.theme_path + 'smallsan_03.png')} alt=""/>
                                </RadioButton>
                            </RadioGroup>
                        </div>
                        <div className="right InvoiceAddressLink" style={this.state.InvoiceAddress == '2' ? {display:'block'} : {display:'none'}}  > <span style={{cursor:'pointer'}} onClick={this.props.click}>{I18n.t('PaymentPage.address')}</span> </div>
                        <div className="clear"></div>
                        <div className="InvoiceAddress" style={this.state.InvoiceAddress == '2' ? {display:'block'} : {display:'none'}}>
                            <PaymentAddress flag={true} address={this.props.currentUserInfo} onclick={this.props.onclick}/>
                        </div>
                    </p>
                </div>
            </div>
        )
    }
}
