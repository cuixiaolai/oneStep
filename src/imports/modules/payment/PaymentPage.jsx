import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
// import { createContainer } from 'meteor/react-meteor-data';
import { Upload,Modal,Radio,Input,Checkbox,Cascader,Form,message,Table,Button } from 'antd';
// import { Buyer } from '../../api/Buyer.js';
import config from '../../config.json';
import PaymentTable from './PaymentTable.jsx';
const confirm = Modal.confirm;
const RadioGroup = Radio.Group;
const RadioButton = Radio.Button;
const FormItem = Form.Item;

export default class PaymentPageBoxSmaill extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            options: false,
            optionstwo: false,
            optionsthree: false,
            totalPrice: 0,
            totalQuantity: 0,
            data: [],
            company: [],
            destPay: [],
            postage: [],
            candestPay: [],
            word:'',
        });
    }
    componentDidMount() {
        let company = [];
        for (let i = 0; i < this.props.quoteInfo.product.length; ++ i) {
            let companyItem = {
                company: this.props.quoteInfo.product[i].key.company,
                type: this.props.quoteInfo.product[i].key.type,
            };
            let dataItem = [];
            for (let j = 0; j < this.props.quoteInfo.product[i].value.length; ++ j) {
                let item = {
                    key: this.props.quoteInfo.product[i].value[j].stockId,
                    Shopmessage: {
                        id: '',
                        img: '',
                        manufacturer: '',
                        package: '',
                        detail: '',
                    },
                    Ti:[],
                    Unit_Price:[],
                    number:this.props.quoteInfo.product[i].value[j].quantity,
                    price:0,
                    Time:this.props.quoteInfo.delivery_address == 0 ? 1 : 2,
                };
                dataItem.push(item);
                this.state.totalQuantity += this.props.quoteInfo.product[i].value[j].quantity;
            }
            company.push(companyItem);
            this.state.data.push(dataItem);
            this.state.destPay.push(false);
            this.state.postage.push(0);
            this.state.candestPay.push(true);
        }
        this.setState({data: this.state.data, company: company, totalQuantity: this.state.totalQuantity, destPay: this.state.destPay, postage: this.state.postage, candestPay: this.state.candestPay});
        this.props.setTotalQuantity(this.state.totalQuantity);
        this.props.setDest(this.state.destPay);
        this.props.setOrderNum(company.length);
        for (let i = 0; i < this.state.data.length; ++ i) {
            for (let j = 0; j < this.state.data[i].length; ++ j) {
                Meteor.call('stock.findOneStock', this.state.data[i][j].key, function(err, stockdata) {
                    if (err != null) {
                        console.log(err);
                    }
                    else {
                        Meteor.call('product.findDetailAndImg', stockdata.product_id, function(err, productdata) {
                            if (err != null) {
                                console.log(err);
                            }
                            else {
                                this.state.data[i][j].Shopmessage.id = stockdata.product_id;
                                this.state.data[i][j].Shopmessage.manufacturer = stockdata.manufacturer;
                                this.state.data[i][j].Shopmessage.detail = productdata.detail;
                                let packageStr = '';
                                for (let i = 0; i < stockdata.package.length; ++ i) {
                                    if (packageStr == '') {
                                        packageStr = stockdata.package[i].package;
                                    }
                                    else {
                                        packageStr = packageStr + ', ' + stockdata.package[i].package;
                                    }
                                }
                                this.state.data[i][j].Shopmessage.package = packageStr;
                                let min = [];
                                let price = [];
                                for (let k = 0; k < stockdata.price.length; ++ k) {
                                    min.push(stockdata.price[k].min);
                                    price.push(stockdata.price[k].price);
                                }
                                this.state.data[i][j].Ti = min;
                                this.state.data[i][j].Unit_Price = price;
                                let k = 0;
                                for (; k < min.length; ++ k) {
                                    if (this.state.data[i][j].number < min[k]) {
                                        break;
                                    }
                                }
                                let total = price[k-1] * this.state.data[i][j].number;
                                this.state.data[i][j].price = total;
                                this.state.totalPrice += total;
                                let postage = stockdata.free_ship == 1 ? 0 : stockdata.trans_expanse;
                                this.state.postage[i] += postage;
                                if (stockdata.dest_pay == false) {
                                    this.state.candestPay[i] = false;
                                }
                                this.setState({data: this.state.data});
                                this.setState({totalPrice: this.state.totalPrice, postage: this.state.postage, candestPay: this.state.candestPay});
                                this.props.setTotalPrice(this.state.totalPrice);
                                this.props.setPostage(this.state.postage);
                            }
                        }.bind(this));
                    }
                }.bind(this));
            }
        }
    }
    setDestPay(i, value) {
        this.state.destPay[i] = value;
        this.setState({destPay: this.state.destPay});
        let postage = [];
        for (let j = 0; j < this.state.destPay.length; ++ j) {
            if (this.state.destPay[j] == true) {
                postage.push(0);
            }
            else {
                postage.push(this.state.postage[j]);
            }
        }
        this.props.setPostage(postage);
        this.props.setDest(this.state.destPay);
    }
    ShowFive(){
        if(this.props.quoteInfo != null && this.props.quoteInfo.delivery_address != 2){
            return(
                <p className="left">4</p>
            )
        }else if(this.props.quoteInfo != null && this.props.quoteInfo.delivery_address == 2){
            return(
                <p className="left">5</p>
            )
        }
    }
    options(value){
        this.props.setOption(0, value[0]);
        if(value[0] == "custom"){
            this.setState({options:true});
            this.props.setOptionflag(0,true);
        }else{
            this.setState({options:false});
            this.props.setOptionflag(0,false);
        }
    }
    optionstwo(value){
        this.props.setOption(1, value[0]);
        if(value[0] == "custom"){
            this.setState({optionstwo:true});
            this.props.setOptionflag(1,true);
        }else{
            this.setState({optionstwo:false});
            this.props.setOptionflag(1,false);
        }
    }
    optionsthree(value){
        this.props.setOption(2, value[0]);
        if(value[0] == "custom"){
            this.setState({optionsthree:true});
            this.props.setOptionflag(2,true);
        }else{
            this.setState({optionsthree:false});
            this.props.setOptionflag(2,false);
        }
    }
    handleChange1(info) {
        let word=info.file.name;
        this.setState({word:word});
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace(/public/, '');
            this.props.setOptionFile(0, path);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    }
    handleChange2(info) {
        let word=info.file.name;
        this.setState({words:word});
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace(/public/, '');
            this.props.setOptionFile(1, path);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    }
    handleChange3(info) {
        let word=info.file.name;
        this.setState({wordes:word});
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace(/public/, '');
            this.props.setOptionFile(2, path);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    }
    Cascader(){
        let options = [];
        let optionstwo = [];
        let optionsthree = [];
        if(this.props.quoteInfo != null && this.props.quoteInfo.delivery_address == 0){
            options = [{
                value: 'packaging',
                label: I18n.t('PaymentPageTable.packaging'),
            }, {
                value: 'custom',
                label: I18n.t('PaymentPageTable.custom'),
            }];
            optionstwo = [{
                value: 'packaging',
                label: I18n.t('PaymentPageTable.packaging'),
            }, {
                value: 'custom',
                label: I18n.t('PaymentPageTable.custom'),
            }, {
                value: 'label',
                label: I18n.t('PaymentPageTable.label'),
            }];

            optionsthree = [{
                value: 'packaging',
                label: I18n.t('PaymentPageTable.free'),
            }, {
                value: 'label',
                label: I18n.t('PaymentPageTable.delivery'),
            }, {
                value: 'custom',
                label: I18n.t('PaymentPageTable.customlist'),
            }];
        }else if(this.props.quoteInfo != null && this.props.quoteInfo.delivery_address == 1){
            options = [{
                value: 'packaging',
                label: I18n.t('PaymentPageTable.packaging'),
            }, {
                value: 'custom',
                label: I18n.t('PaymentPageTable.customs'),
            }];
            optionstwo = [{
                value: 'packaging',
                label: I18n.t('PaymentPageTable.packaging'),
            }, {
                value: 'custom',
                label: I18n.t('PaymentPageTable.customs'),
            }, {
                value: 'label',
                label: I18n.t('PaymentPageTable.labels'),
            }];

            optionsthree = [{
                value: 'packaging',
                label: I18n.t('PaymentPageTable.free'),
            }, {
                value: 'label',
                label: I18n.t('PaymentPageTable.deliverys'),
            }, {
                value: 'custom',
                label: I18n.t('PaymentPageTable.customlist'),
            }];
        }else if(this.props.quoteInfo != null && this.props.quoteInfo.delivery_address == 2){
            options = [{
                value: 'packaging',
                label: I18n.t('PaymentPageTable.packaging'),
            },{
                value: 'Simple',
                label: I18n.t('PaymentPageTable.Simple')
            },{
                value: 'vacuum',
                label: I18n.t('PaymentPageTable.vacuum')
            },{
                value: 'double',
                label: I18n.t('PaymentPageTable.double')
            },{
                value: 'custom',
                label: I18n.t('PaymentPageTable.Packingcustom'),
            }];
            optionstwo = [{
                value: 'packaging',
                label: I18n.t('PaymentPageTable.Label'),
            }, {
                value: 'Simpletwo',
                label: I18n.t('PaymentPageTable.Simpletwo'),
            },{
                value: 'Fulllabel',
                label: I18n.t('PaymentPageTable.Fulllabel'),
            },{
                value: 'Packaginglabels',
                label: I18n.t('PaymentPageTable.Packaginglabels'),
            },{
                value: 'Boxlabel',
                label: I18n.t('PaymentPageTable.Boxlabel'),
            },{
                value: 'custom',
                label: I18n.t('PaymentPageTable.customTwo'),
            }, {
                value: 'label',
                label: I18n.t('PaymentPageTable.labeltwo'),
            }];
            optionsthree = [{
                value: 'packaging',
                label: I18n.t('PaymentPageTable.free'),
            }, {
                value: 'label',
                label: I18n.t('PaymentPageTable.deliverytwo'),
            },{
                value: 'Simplelist',
                label: I18n.t('PaymentPageTable.Simplelist'),
            },{
                value: 'fulllist',
                label: I18n.t('PaymentPageTable.fulllist'),
            }, {
                value: 'custom',
                label: I18n.t('PaymentPageTable.customlisttwo'),
            },{
                value: 'fullsend',
                label: I18n.t('PaymentPageTable.fullsend'),
            }];
        }

        const propu1 ={
            name: 'file',
            showUploadList: false,
            action:config.file_server + 'order',
            onChange:this.handleChange1.bind(this),
            beforeUpload(file){
                let isJPG;
                if(file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || file.type == 'application/msword'||file.type == 'application/vnd.ms-excel'||file.type == 'application/pdf'){
                    isJPG = true;
                }else{
                    isJPG = false;
                    message.error('只能上传word,Excel和pdf的文件！');
                }
                return isJPG;
            }
        };
        const propu2 ={
            name: 'file',
            showUploadList: false,
            action:config.file_server + 'order',
            onChange:this.handleChange2.bind(this),
            beforeUpload(file){
                let isJPG;
                if(file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"|| file.type == 'application/msword'||file.type == 'application/vnd.ms-excel'||file.type == 'application/pdf'){
                    isJPG = true;
                }else{
                    isJPG = false;
                    message.error('只能上传word和Excel的文件！');
                }
                return isJPG;
            }
        };
        const propu3 ={
            name: 'file',
            showUploadList: false,
            action:config.file_server + 'order',
            onChange:this.handleChange3.bind(this),
            beforeUpload(file){

                let isJPG;
                if(file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"|| file.type == 'application/msword'||file.type == 'application/vnd.ms-excel'||file.type == 'application/pdf'){
                    isJPG = true;
                }else{
                    isJPG = false;
                    message.error('只能上传word和Excel的文件！');
                }
                return isJPG;
            }
        };
        return(
            <div style={{marginBottom:10}}>
                <div className="PaymentPageCascader left">
                    <Cascader options={options} defaultValue={["packaging"]} onChange={this.options.bind(this)} />
                    <Cascader options={optionstwo} defaultValue={["packaging"]} onChange={this.optionstwo.bind(this)}  placeholder={I18n.t('PaymentPageTable.packaging')} />
                    <Cascader options={optionsthree} defaultValue={["packaging"]} onChange={this.optionsthree.bind(this)}  placeholder={I18n.t('PaymentPageTable.free')} />
                </div>
                <div className="left">
                    <Form inline className="SellerCenterUploadStockForm">
                        <div style={{height:38}}>
                            <FormItem
                                style={this.state.options ? {display:'block'} : {display:'none'}}
                                label={I18n.t('PaymentPageTable.thecustom')}
                            >
                                <div className="UploadStockPDF">
                                    {this.state.word}
                                    <Upload {...propu1}>
                                        <div className="UploadStockPDF-btn">
                                            {I18n.t('search')}
                                        </div>
                                    </Upload>
                                </div>
                                <span>{I18n.t('type')}</span>
                            </FormItem>
                        </div>
                        <div style={{height:38}}>
                            <FormItem
                                style={this.state.optionstwo ? {display:'block'} : {display:'none'}}
                                label={I18n.t('PaymentPageTable.thecustom')}
                            >
                                <div className="UploadStockPDF">
                                    {this.state.words}
                                    <Upload {...propu2}>
                                        <div className="UploadStockPDF-btn">
                                            {I18n.t('search')}
                                        </div>
                                    </Upload>
                                </div>
                                <span>{I18n.t('type')}</span>
                            </FormItem>
                        </div>
                        <div style={{height:38}}>
                            <FormItem
                                style={this.state.optionsthree ? {display:'block'} : {display:'none'}}
                                label={I18n.t('PaymentPageTable.thecustom')}
                            >
                                <div className="UploadStockPDF">
                                    {this.state.wordes}
                                    <Upload {...propu3}>
                                        <div className="UploadStockPDF-btn">
                                            {I18n.t('search')}
                                        </div>
                                    </Upload>
                                </div>
                                <span>{I18n.t('type')}</span>
                            </FormItem>
                        </div>
                    </Form>
                </div>
                <div className="clear"></div>
            </div>
        )
    }
    tableRender() {
        const columns = this.props.quoteInfo.delivery_address == 0 ? [{
            dataIndex: 'Shopmessage',
            className:'PaymentTableTitleOne',
            render:(text, record) =>(
                <div>
                    <div className="left"><img src={(config.theme_path + 'shop_03.jpg')} alt=""/></div>
                    <div className="left">
                        <p> <Link to={'/product_details/'+record.key}>{text.id}</Link></p>
                        <p>
                            <span>{I18n.t('PaymentPageTable.Manufactor')}</span>
                            <span>{text.manufacturer}</span>
                        </p>
                        <p>
                            <span>{I18n.t('PaymentPageTable.encapsulation')}</span>
                            <span>{text.package}</span>
                        </p>
                        <p>{text.detail}</p>
                    </div>
                    <div className="clear"></div>
                </div>
            ),
        }, {
            dataIndex: 'Ti',
            className:'PaymentTableTitleTwo',
            render:(text) => (text.map(p => <p>{p}+</p>))
        }, {
            dataIndex: 'Unit_Price',
            className:'PaymentTableTitleTwo',
            render:(text) => (text.map(p => <p>￥{p}/{I18n.t('ge')}</p>))
        }, {
            className:'PaymentTableTitleThree',
            dataIndex: 'number',
        }, {
            dataIndex: 'price',
            render:(text) => (<p>￥<span style={{color:'#0c5aa2',fontSize:18}}>{text}</span></p>)
        }, {
            dataIndex: 'Time',
            render:(text) => (<p>{text}{I18n.t('week')}</p>)
        }] : [{
            dataIndex: 'Shopmessage',
            className:'PaymentTableTitleOne',
            render:(text, record) =>(
                <div>
                    <div className="left"><img src={(config.theme_path + 'shop_03.jpg')} alt=""/></div>
                    <div className="left">
                        <p>{text.id}</p>
                        <p>
                            <span>{I18n.t('PaymentPageTable.Manufactor')}</span>
                            <span>{text.manufacturer}</span>
                        </p>
                        <p>
                            <span>{I18n.t('PaymentPageTable.encapsulation')}</span>
                            <span>{text.package}</span>
                        </p>
                        <p>{text.detail}</p>
                    </div>
                    <div className="clear"></div>
                </div>
            ),
        }, {
            dataIndex: 'Ti',
            className:'PaymentTableTitleTwo',
            render:(text) => (text.map(p => <p>{p}+</p>))
        }, {
            dataIndex: 'Unit_Price',
            className:'PaymentTableTitleTwo',
            render:(text) => (text.map(p => <p>${p}/{I18n.t('ge')}</p>))
        }, {
            className:'PaymentTableTitleThree',
            dataIndex: 'number',
        }, {
            dataIndex: 'price',
            render:(text) => (<p>$<span style={{color:'#0c5aa2',fontSize:18}}>{text}</span></p>)
        }, {
            dataIndex: 'Time',
            render:(text) => (<p>{text}{I18n.t('week')}</p>)
        }];
        let table = [];
        for (let i = 0; i < this.state.company.length; ++ i) {
            table[i] = <PaymentTable columns = {columns} data = {this.state.data[i]} name={this.state.company[i].company}  type={this.state.company[i].type} deliveryAddress={this.props.quoteInfo.delivery_address} setDestPay={this.setDestPay.bind(this)} candestPay={this.state.candestPay[i]} flag={i}/>;
        }
        return table;
    }
    order(e) {
        this.props.setOrder(e.target.value);
    }
    remark(e) {
        this.props.setRemark(e.target.value);
    }
    render(){
        return(
            <div className="PaymentPageBoxSmaill">
                <div className="PaymentPageBox">
                    <p className="PaymentPageBoxTop">
                        {this.ShowFive()}
                        <p className="left">{I18n.t('PaymentPage.message')}</p>
                        <p className="right"><Link to="/shopping_cart">{I18n.t('PaymentPage.back')}</Link></p>
                        <p className="clear"></p>
                    </p>
                    <p className="PaymentPageBoxTableTop">
                        <span >{I18n.t('PaymentPageTable.message')}</span>
                        <span >{I18n.t('PaymentPageTable.number')}</span>
                        <span >{I18n.t('PaymentPageTable.price')}</span>
                        <span >{I18n.t('PaymentPageTable.num')}</span>
                        <span >{I18n.t('PaymentPageTable.allprice')}</span>
                        <span>{I18n.t('PaymentPageTable.Time')}</span>
                    </p>
                    {this.tableRender()}
                    {this.Cascader()}
                    <p style={{fontSize:14}}>{I18n.t('Remarks')}</p>
                    <div className="PaymentPageBoxRemarks">
                        <p>
                            <span>{I18n.t('PaymentPageTable.Ordernumber')}</span>
                            <Input onChange={this.order.bind(this)}/>
                        </p>
                        <p>
                            <span>{I18n.t('PaymentPageTable.Remarks')}</span>
                            <Input onChange={this.remark.bind(this)}/>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}