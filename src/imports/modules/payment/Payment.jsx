import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Link, browserHistory } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
// import { createContainer } from 'meteor/react-meteor-data';
import { Upload,Modal,Radio,Input,Checkbox,Cascader,Form,message,Table,Button } from 'antd';
import PaymentAdress from './PaymentAddress.jsx';
import PaymentInvoice from './PaymentInvoice.jsx';
import config from '../../config.json';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
import PaymentPageBoxSmaill from './PaymentPage.jsx';
import AddAddress from '../center/AddAddress.jsx';

export default class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            flag: false,
            abroadvalue:1,
            Expressinformation:false,
            addressId: 0, //收货地址
            totalPrice: 0, //商品总价
            dest: [], //到付
            postage: [], //邮费
            totalQuantity: 0, // 商品个数
            InvoiceType:1, //发票类型： 1 普通发票 2 增值发票
            InvoiceTitle: '', //发票抬头
            InvoiceWay: 1, //发票寄送 1 电子发票 2 纸质发票
            InvoiceEmail: '', // 普通发票邮箱
            InvoiceAddressType: 1, //发票地址 1 与发货地址相同 2 其他地址
            InvoiceAddress: 0, //发票其他地址
            InvoiceName: '',
            options:['packaging', 'packaging', 'packaging'], //选项
            optionsfile:['', '', ''], //上传文件
            order: '', //私有订单号
            remark: '', //备注
            packagePrice: 0,
            labelPrice: 0,
            listPrice: 0,
            orderNum: 1,
            titleflag:false,
            emailflag:false,
            titleflags:false,
            emailflags:false,
            nameflags:false,
            agree:false,
            optionflag:[false,false,false], //是否使用自己上传选项
            addaddress:false,

            // fixme: need to correct the delivery_address *(need to call tax api or rate api)
            // fixme: need to insert data to db

            // tax( total tax "right now" )
            // rate( null:not receive rate data; else: valueof 100 dollar to RMB )
            // receive_all_tax( true: each product get the tax from api; false:not )
            tax: null,
            rate : null,
            receive_all_tax:false,

            // product_tax_arr( data received from api )
            // product_id_arr( product_id_arr[i][j] = {product_id,quantity} )
            product_tax_arr:null,
            product_id_arr:[],
            runtime:0,
            refresh:true
        });
    }
    componentDidMount(){
        if(this.props.quoteInfo != null) {
            this.state.runtime = 1
            if(this.props.quoteInfo.delivery_address  ==2) { // fixme: need to correct the delivery_address * tax
                this.state.product_id_arr = new Array(this.props.quoteInfo.product.length)
                for (let i = 0; i < this.props.quoteInfo.product.length; ++i) {
                    this.state.product_id_arr[i] = new Array(this.props.quoteInfo.product[i].value.length)
                }
                for (let i = 0; i < this.props.quoteInfo.product.length; ++i) {
                    for (let j = 0; j < this.props.quoteInfo.product[i].value.length; ++j) {
                        const quantity_tmp = this.props.quoteInfo.product[i].value[j].quantity
                        Meteor.call('stock.findOneStock', this.props.quoteInfo.product[i].value[j].stockId, function (err, stockdata) {
                            if (err != null) {
                                console.log(err);
                            }
                            else {
                                this.state.product_id_arr[i][j] = {
                                    product_id: stockdata.product_id,
                                    quantity: quantity_tmp
                                }
                                this.setState({
                                    refresh: !this.state.refresh
                                })
                            }
                        }.bind(this))
                    }
                }
            }
            if(this.props.quoteInfo.delivery_address ==2) { // fixme: need to correct the delivery_address * rate
                if (this.state.rate == null) {
                    var xhr_rate = new XMLHttpRequest();
                    xhr_rate.open('get', config.file_server+'crossDomain', true);
                    xhr_rate.send();
                    xhr_rate.onload = function (event) {
                        const rate_ret = JSON.parse(xhr.responseText)
                        if (rate_ret.error_code == 0 &&
                            typeof rate_ret.result != 'undefined' &&
                            typeof rate_ret.result.list != 'undefined' &&
                            rate_ret.result.list.length > 0) {
                            this.setState({
                                rate: parseInt(rate_ret.result.list[0][2]) // todo: make it safe
                            })
                        }
                    }.bind(this)
                }
            }
        }
    }
    componentWillReceiveProps(nextProps){
        if(this.state.runtime == 0 && nextProps.quoteInfo != null) {
            this.state.runtime = 1
            if(nextProps.quoteInfo.delivery_address ==2) { // fixme: need to correct the delivery_address * tax
                this.state.product_id_arr = new Array(nextProps.quoteInfo.product.length)
                for (let i = 0; i < nextProps.quoteInfo.product.length; ++i) {
                    this.state.product_id_arr[i] = new Array(nextProps.quoteInfo.product[i].value.length)
                }
                for (let i = 0; i < nextProps.quoteInfo.product.length; ++i) {
                    for (let j = 0; j < nextProps.quoteInfo.product[i].value.length; ++j) {
                        const quantity_tmp = nextProps.quoteInfo.product[i].value[j].quantity
                        Meteor.call('stock.findOneStock', nextProps.quoteInfo.product[i].value[j].stockId, function (err, stockdata) {
                            if (err != null) {
                                console.log(err);
                            }
                            else {
                                this.state.product_id_arr[i][j] = {
                                    product_id: stockdata.product_id,
                                    quantity: quantity_tmp
                                }
                                this.setState({
                                    refresh: !this.state.refresh
                                })
                            }
                        }.bind(this))
                    }
                }
            }
            if(nextProps.quoteInfo.delivery_address == 2) { // fixme: need to correct the delivery_address * rate
                if(this.state.rate == null) {
                    var xhr_rate = new XMLHttpRequest();
                    xhr_rate.open('get', config.file_server+'crossDomain', true);
                    xhr_rate.send();
                    xhr_rate.onload = function (event) {
                        console.log(xhr_rate.responseText)
                        const rate_ret = JSON.parse(xhr_rate.responseText)
                        if (rate_ret.error_code == 0 &&
                            typeof rate_ret.result != 'undefined' &&
                            typeof rate_ret.result.list != 'undefined' &&
                            rate_ret.result.list.length > 0) {
                            this.setState({
                                rate : parseInt(rate_ret.result.list[0][2]) // fixme: todo: make it safe
                            })
                        }
                    }.bind(this)
                }
            }
        }
    }
    componentWillUpdate(nextProps,nextState){
        if(nextState.runtime == 1 && nextProps.quoteInfo != null) {
            if(nextProps.quoteInfo.delivery_address ==2) { // fixme: need to correct the delivery_address * tax
                product_id_flag = true

                for (let i = 0; i < nextProps.quoteInfo.product.length; ++i) {
                    for (let j = 0; j < nextProps.quoteInfo.product[i].value.length; ++j) {
                        if (typeof nextState.product_id_arr[i][j] == 'undefined') {
                            product_id_flag = false
                        }
                    }
                }
                if (product_id_flag) {
                    var product_arr = []
                    var quantity_arr = []
                    for (let i = 0; i < nextProps.quoteInfo.product.length; ++i) {
                        for (let j = 0; j < nextProps.quoteInfo.product[i].value.length; ++j) {
                            product_arr.push(nextState.product_id_arr[i][j].product_id)
                            quantity_arr.push(nextState.product_id_arr[i][j].quantity)

                        }
                    }
                    // product_arr = ['LQG15HS6N8J02D','GBJ3510-BP']
                    var item = ''
                    for (var i = 0; i < product_arr.length; i++) {
                        item += product_arr[i]
                        if (i != product_arr.length - 1) {
                            item += '&_&'
                        }
                    }
                    // var randstr = "63364747" // todo: change to order id or sth random
                    // var timestamp = (Date.parse(new Date()) / 1000).toString()
                    // var login = "AEFBTT01"
                    // var hash_code = hex_md5("B6684aF59aDc" + randstr + timestamp)
                    // var fd = new FormData();
                    // fd.append("dataStr", "{\"randStr\":\"" + randstr + "\",\"timestamp\":\"" + timestamp + "\",\"signature\":\"" + hash_code + "\",\"itemModel\":\"" + item + "\",\"loginName\":\"" + login + "\"}");
                    var xhr = new XMLHttpRequest();
                    xhr.open('get', config.file_server+'rate?product_id='+item, true);
                    xhr.send();
                    xhr.onload = function (event) {
                        var tex_respond = JSON.parse(xhr.responseText)
                        if (tex_respond.code == 200) {
                            const tax_ret = tex_respond.productList
                            var tax_tmp = null
                            var receive_all_tax_tmp = false
                            var tax_count = 0;
                            for (var i = 0; i < product_arr.length; i++) {
                                for (let j = 0; j < tax_ret.length; j++) {
                                    if (tax_ret[j].itemModel == product_arr[i]) {
                                        tax_count++;
                                        if (tax_tmp == null) {
                                            tax_tmp = 0
                                        } else {
                                            tax_tmp += tax_ret[j].hgRate * quantity_arr[i] // hgRate now is 0 because of xinlikang test api
                                        }
                                    }
                                }
                            }
                            if (product_arr.length == tax_count) {
                                receive_all_tax_tmp = true
                            }
                            this.setState({
                                product_tax_arr: tax_ret,
                                tax: tax_tmp,
                                receive_all_tax: receive_all_tax_tmp
                            })
                        }
                    }.bind(this)
                    this.setState({
                        runtime: 2
                    })
                }
            }
        }
    }
    setOrderNum(value) {
        this.setState({setOrderNum: value});
    }
    setTotalPrice(value) {
        this.setState({totalPrice: value});
    }
    setTotalQuantity(value) {
        this.setState({totalQuantity: value});
    }
    setInvoiceType(value) {
        this.setState({InvoiceType: value});
    }
    setInvoiceTitle(value) {
        this.setState({InvoiceTitle: value,titleflag:false});
    }
    setInvoiceWay(value) {
        if(value == 2){
            this.setState({InvoiceWay: value,emailflag:false});
        }else{
            this.setState({InvoiceWay: value});
        }
    }
    setInvoiceEmail(value) {
        this.setState({InvoiceEmail: value,emailflag:false});
    }
    setInvoiceAddressType(value) {
        this.setState({InvoiceAddressType: value});
    }
    setOrder(value) {
        this.setState({order: value});
    }
    setRemark(value) {
        this.setState({remark: value});
    }
    setOptionflag(i,value){
        this.state.optionflag[i] = value;
        this.setState({
            optionflag:this.state.optionflag,
        });
    }
    setOption(i, value) {
        this.state.options[i] = value;
        this.setState({options: this.state.options});
        if (this.props.quoteInfo != null && this.props.quoteInfo.delivery_address != 2){ //todo 包装费问题    
            if (i == 0 && (value == 'custom' || value == 'vacuum')) {
                this.setState({packagePrice: 10});
            }else if (i == 0 && value == 'packaging'){
                this.setState({packagePrice: 0});
            } else if (i == 1 && (value == 'custom' || value == 'label')) {
                this.setState({labelPrice: 10});
            }else if (i == 1 && value == 'packaging'){
                this.setState({labelPrice: 0});
            } else if (i == 2 && value == 'custom') {
                this.setState({listPrice: 20});
            } else if (i == 2 && value == 'label') {
                this.setState({listPrice: 10});
            }else if (i == 2 && value == 'packaging'){
                this.setState({listPrice: 0});
            }
        }else if (this.props.quoteInfo != null && this.props.quoteInfo.delivery_address == 2){ //todo 包装费问题
            if (i == 0 && value == 'custom') {
                this.setState({packagePrice: 15});
            }else if(i == 0 && value == 'Simple'){
                this.setState({packagePrice: 5});
            }else if(i == 0 && value == 'vacuum'){
                this.setState({packagePrice: 10});
            }else if(i == 0 && value == 'double'){
                this.setState({packagePrice: 6});
            }else if(i == 0 && value == 'double'){
                this.setState({packagePrice: 6});
            }else if (i == 0 && (value == 'packaging')) {
                this.setState({packagePrice: 0});
            }else if(i == 1 && (value == 'packaging')){
                this.setState({labelPrice: 0});
            }else if(i == 1 && (value == 'custom')){
                this.setState({labelPrice: 5});
            }else if (i == 1 && ( value == 'label' || value == 'Simpletwo'||value == 'Fulllabel'||value == 'Packaginglabels'||value == 'Boxlabel')) {
                this.setState({labelPrice: 2});
            }else if (i == 2 && value == 'custom') {
                this.setState({listPrice: 5});
            }else if (i == 2 && (value == 'packaging'||value == 'label'||value == 'Simplelist'||value == 'fulllist'|| value == 'fullsend')){
                this.setState({listPrice: 0});
            }
        }
    }
    setOptionFile(i, value) {
        this.state.optionsfile[i] = value;
        this.setState({optionsfile: this.state.optionsfile});
    }
    setDest(value) {
        this.setState({dest: value});
    }
    setPostage(value) {
        this.setState({postage: value});
    }
    Block(){
        this.setState({flag:true});
    }
    ShowSpan(){
        if(this.props.currentUserInfo != null && this.props.currentUserInfo.address != null){
            if(this.props.currentUserInfo.address.length>5){
                return(
                    <span onClick={this.Block.bind(this)} style={this.state.flag ? {display:'none'} : {display:'block'}} className="PaymentPageBoxBtn cursor">{I18n.t('PaymentPage.otheradd')}<img src={(config.theme_path + 'down.png')} alt=""/></span>
                )
            }
        }else{

        }
    }

    Agree(e){
        console.log(e);
        this.setState({agree:e.target.checked});
    }
    onChangeabroad(e){
        this.setState({
            abroadvalue: e.target.value,
        });
        console.log(e.target.value);
    }
    onClickAddressBox(id){
        this.setState({addressId: id});
        let Box = document.getElementsByClassName("PaymentPageBox")[0];
        let smallbox= Box.getElementsByClassName('PaymentPageBoxAddressDefault')[0];
        for(let i=0;i<Box.children[1].children.length;i++){
            if(Box.children[1].children[i].id == id){
                Box.children[1].children[i].style.background = '#fef6f1';
                Box.children[1].children[i].style.border = '1px solid #ed7020';
                Box.children[1].children[i].children[7].style.display = 'block'
            }else {
                Box.children[1].children[i].style.background = '#fff';
                Box.children[1].children[i].style.border = '1px solid #fff';
                smallbox.style.border = '1px solid #ed7020';
                Box.children[1].children[i].children[7].style.display = 'none'
            }
        }
    }
    onClickAddressBoxs(id){
        this.setState({InvoiceAddress: id});
        let Box = document.getElementsByClassName("InvoiceAddress")[0];
        let smallbox= Box.getElementsByClassName('PaymentPageBoxAddressDefault')[0];
        for(let i=0;i<Box.children[0].children.length;i++){
            if(Box.children[0].children[i].id == id){
                Box.children[0].children[i].style.background = '#fef6f1';
                Box.children[0].children[i].style.border = '1px solid #ed7020';
                Box.children[0].children[i].children[7].style.display = 'block'
            }else {
                Box.children[0].children[i].style.background = '#fff';
                Box.children[0].children[i].style.border = '1px solid #fff';
                smallbox.style.border = '1px solid #ed7020';
                Box.children[0].children[i].children[7].style.display = 'none'
            }
        }
    }
    onClickAddressBoxes(id){
        this.setState({addressId: id});
        let Box = document.getElementsByClassName("AbroadPaymentAdressInner")[0];
        let smallbox= Box.getElementsByClassName('PaymentPageBoxAddressDefault')[0];
        for(let i=0;i<Box.children[0].children.length;i++){
            if(Box.children[0].children[i].id == id){
                Box.children[0].children[i].style.background = '#fef6f1';
                Box.children[0].children[i].style.border = '1px solid #ed7020';
                Box.children[0].children[i].children[7].style.display = 'block'
            }else {
                Box.children[0].children[i].style.background = '#fff';
                Box.children[0].children[i].style.border = '1px solid #fff';
                smallbox.style.border = '1px solid #ed7020';
                Box.children[0].children[i].children[7].style.display = 'none'
            }
        }
    }
    ShowPaymentAdress(){
        if(this.props.quoteInfo != null && this.props.quoteInfo.delivery_address != 2){
            return(
                <div className="PaymentPageBox">
                    <p className="PaymentPageBoxTop" >
                        <p className="left">1</p>
                        <p className="left">{I18n.t('PaymentPage.add')}</p>
                        <p className="right"><span style={{cursor:'pointer'}} onClick={this.showaddaddress.bind(this)}>{I18n.t('PaymentPage.address')}</span></p>
                        <p className="clear"></p>
                    </p>
                    <PaymentAdress   onclick={this.onClickAddressBox.bind(this)} flag={this.state.flag} address={this.props.currentUserInfo} />
                    {this.ShowSpan()}
                </div>
            )
        }else if(this.props.quoteInfo != null && this.props.quoteInfo.delivery_address == 2){
            const options = [{
                value: 'zhejiang',
                label: 'zhejiang',
            }, {
                value: 'jiangsu',
                label: 'Jiangsu',
            }];
            return(
                <div className="PaymentPageBox">
                    <p className="PaymentPageBoxTop">
                        <p className="left">1</p>
                        <p className="left">{I18n.t('PaymentPage.add')}</p>
                        <p className="right"><span style={{cursor:'pointer'}} onClick={this.showaddaddress.bind(this)}>{I18n.t('PaymentPage.address')}</span></p>
                        <p className="clear"></p>
                    </p>
                    <div className="AbroadPaymentAdress">
                        <RadioGroup onChange={this.onChangeabroad.bind(this)} value={this.state.abroadvalue}>
                            <Radio key="a" value={1}>{I18n.t('PaymentPageTable.Commonreceiveraddress')}
                                <img style={this.state.abroadvalue == 1 ? {transform: "rotate(-90deg)"} : {transform: "rotate(90deg)"}} src={(config.theme_path + 'comjian.png')} alt=""/>
                            </Radio>
                            <div className="AbroadPaymentAdressInner" style={this.state.abroadvalue == 1 ? {display:'block'} : {display:'none'}}>
                                <PaymentAdress flag={this.state.flag} address={this.props.currentUserInfo} onclick={this.onClickAddressBoxes.bind(this)} />
                                {this.ShowSpan()}
                            </div>
                            <Radio key="b" value={2}>{I18n.t('PaymentPageTable.platform')}
                                <img style={this.state.abroadvalue == 2 ? {transform: "rotate(-90deg)"} : {transform: "rotate(90deg)"}}  src={(config.theme_path + 'comjian.png')} alt=""/>
                            </Radio>
                            <div className="AbroadPaymentAdressInner" style={this.state.abroadvalue == 2 ? {display:'block'} : {display:'none'}}>
                                <div className="left"><Cascader options={options} placeholder={I18n.t('PaymentPageTable.Storage')}/></div>
                                <div className="left"><Input  placeholder={I18n.t('PaymentPageTable.please')}/></div>
                                <div className="clear"></div>
                            </div>
                        </RadioGroup>
                    </div>
                </div>
            )
        }
    }
    Expressinformation(e){
        this.setState({Expressinformation:e.target.checked})
        console.log(e.target.checked);
    }
    ShowExpressinformation(){ //提交订单页面选择国际快递
        if(this.props.quoteInfo != null && this.props.quoteInfo.delivery_address == 2){
            const options = [{
                value: 'FedEx',
                label: 'FedEx（联邦快递）',
            }, {
                value: 'UPS',
                label: 'UPS（联合包裹）',
            }, {
                value: 'DHL',
                label: 'DHL快递(敦豪物流) ',
            }, {
                value: 'TNT',
                label: 'TNT快递 ',
            }, {
                value: 'OCS',
                label: 'OCS（欧西爱斯）',
            }, {
                value: 'DPEX',
                label: 'DPEX',
            }, {
                value: 'ZMS',
                label: 'ZMS-威鹏达 ',
            },{
                value: 'AAE',
                label: 'AAE（美亚快递）',
            }];
            return(
                <div className="PaymentPageBox">
                    <p className="PaymentPageBoxTop">
                        <p className="left">2</p>
                        <p className="left">{I18n.t('PaymentPage.express')}</p>
                        <p className="clear"></p>
                    </p>
                    <div className="left">
                        <Checkbox onChange={this.Expressinformation.bind(this)} >{I18n.t('PaymentPageTable.InternationalExpress')}</Checkbox>
                    </div>
                    <div className="left">
                        {this.state.Expressinformation ? (<Cascader options={options}  placeholder={I18n.t('PaymentPage.Express')} />) : (<Cascader options={options} disabled={true}  placeholder={I18n.t('PaymentPage.Express')} />)}
                    </div>
                    <div className="left">
                        {this.state.Expressinformation ? (<Input placeholder={I18n.t('PaymentPage.Accountnumber')} />) : (<Input disabled={true} placeholder={I18n.t('PaymentPage.Accountnumber')} />)}
                    </div>
                    <div className="clear"></div>
                    <p style={{fontSize:14,color:'#aeaeae',marginTop:5,marginLeft:148}}>{I18n.t('PaymentPage.kouchu')}</p>
                </div>
            )
        }
    }
    invoiceTitle(e) {
        this.setState({InvoiceTitle: e.target.value,titleflags:false});
    }
    invoiceEmail(e) {
        this.setState({InvoiceEmail: e.target.value,emailflags:false});
    }
    invoiceName(e) {
        this.setState({InvoiceName: e.target.value,nameflags:false});
    }
    ShowInvoiceinformation(){
        if(this.props.quoteInfo != null && this.props.currentUserInfo && this.props.quoteInfo.delivery_address != 1){ //购物车入口的情况 
            if(this.props.quoteInfo.delivery_address == 0){
                return(
                    <div className="PaymentPageBox">
                        <p className="PaymentPageBoxTop">
                            <p className="left">2</p>
                            <p className="left">{I18n.t('PaymentPage.invoicemess')}</p>
                            <p className="clear"></p>
                        </p>
                        <PaymentInvoice click={this.showaddaddress.bind(this)} currentUserInfo={this.props.currentUserInfo} onclick={this.onClickAddressBoxs.bind(this)} setInvoiceType={this.setInvoiceType.bind(this)}
                                        setInvoiceTitle={this.setInvoiceTitle.bind(this)} setInvoiceWay={this.setInvoiceWay.bind(this)} setInvoiceEmail={this.setInvoiceEmail.bind(this)}
                                        titleflag={this.state.titleflag}  emailflag={this.state.emailflag}
                                        setInvoiceAddressType={this.setInvoiceAddressType.bind(this)}/>

                    </div>
                )

            }else {

            }
            return(
                <div className="PaymentPageBox">
                    <p className="PaymentPageBoxTop">
                        <p className="left">3</p>
                        <p className="left">{I18n.t('PaymentPage.invoicemess')}</p>
                        <p className="clear"></p>
                    </p>
                    <PaymentInvoice click={this.showaddaddress.bind(this)} currentUserInfo={this.props.currentUserInfo} onclick={this.onClickAddressBoxs.bind(this)} setInvoiceType={this.setInvoiceType.bind(this)}
                                    setInvoiceTitle={this.setInvoiceTitle.bind(this)} setInvoiceWay={this.setInvoiceWay.bind(this)} setInvoiceEmail={this.setInvoiceEmail.bind(this)}
                                    titleflag={this.state.titleflag}  emailflag={this.state.emailflag}
                                    setInvoiceAddressType={this.setInvoiceAddressType.bind(this)}/>

                </div>
            )
        }else if(this.props.quoteInfo != null && this.props.currentUserInfo && this.props.quoteInfo.delivery_address  == 1){

            return(
                <div className="PaymentPageBox">
                    <p className="PaymentPageBoxTop">
                        <p className="left">3</p>
                        <p className="left">{I18n.t('PaymentPage.invoicemess')}</p>
                        <p className="clear"></p>
                    </p>
                    <div className="PaymentInvoiceabroad">
                        <p>{I18n.t('PaymentPage.Electronicinvoice')}</p>
                        <Form inline>
                            <FormItem
                                label={I18n.t('PaymentPage.invoiceEmail')}
                            >
                                <Input  placeholder={I18n.t('PaymentPage.pleinvoiceEmail')} onChange={this.invoiceTitle.bind(this)}/>
                                <div style={this.state.titleflags ? {display:'block'} : {display:'none'}} className="PaymentInvoiceabroadInput">{I18n.t('Register.emailTishi')}</div>
                            </FormItem>
                            <FormItem
                                label={I18n.t('PaymentPage.invoicehead')}
                            >
                                <Input maxLength={20} placeholder={I18n.t('PaymentPage.Twenty')} onChange={this.invoiceEmail.bind(this)}/>
                                <div style={this.state.emailflags ? {display:'block'} : {display:'none'}}  className="PaymentInvoiceabroadInput">{I18n.t('PaymentPage.error')}</div>
                            </FormItem>
                            <FormItem
                                label={I18n.t('PaymentPage.Salename')}
                            >
                                <Input maxLength={20} placeholder={I18n.t('PaymentPage.Twenty')} onChange={this.invoiceName.bind(this)}/>
                                <div style={this.state.nameflags ? {display:'block'} : {display:'none'}}  className="PaymentInvoiceabroadInput">{I18n.t('PaymentPage.nameerror')}</div>
                            </FormItem>
                        </Form>
                    </div>
                </div>
            )
        }
    }
    ShowFour(){
        if(this.props.quoteInfo != null && this.props.quoteInfo.delivery_address != 2){
            return(
                <p className="left">3</p>
            )
        }else if(this.props.quoteInfo != null && this.props.quoteInfo.delivery_address == 2){
            return(
                <p className="left">4</p>
            )
        }
    }
    showTable() {
        if (this.props.quoteInfo != null) {
            return <PaymentPageBoxSmaill quoteInfo={this.props.quoteInfo} setTotalPrice={this.setTotalPrice.bind(this)} setTotalQuantity={this.setTotalQuantity.bind(this)}
                                         setOption={this.setOption.bind(this)} setOptionFile={this.setOptionFile.bind(this)} setDest={this.setDest.bind(this)} setOrderNum={this.setOrderNum.bind(this)}
                                         setOrder={this.setOrder.bind(this)} setRemark={this.setRemark.bind(this)} setPostage={this.setPostage.bind(this)}
                                         setOptionflag={this.setOptionflag.bind(this)}
            />;
        }
    }
    showFooter() {
        const columnss = [{
            title: '商品名',
            dataIndex: 'name',
        }, {
            title: '报率',
            dataIndex: 'rate ',
        }, {
            title: '金额',
            dataIndex: 'money',
        }];

        const datas = []
        for(let i=0;i<this.state.product_id_arr.length;i++) {
            for(let j=0;j<this.state.product_id_arr[i].length;j++) {
                if(typeof this.state.product_id_arr[i][j] != 'undefined'){
                    var rate_tmp = ''
                    var money_tmp = '无关税信息'
                    if (this.state.product_tax_arr == null) {
                        money_tmp = "请稍后"
                    } else {
                        for (let k = 0; k < this.state.product_tax_arr.length; k++) {
                            if (this.state.product_tax_arr[k].itemModel == this.state.product_id_arr[i][j].product_id) {
                                rate_tmp = this.state.product_tax_arr[k].hgRate // hgRate now is 0 because of xinlikang test api
                                money_tmp = this.state.product_tax_arr[k].hgRate * this.state.product_id_arr[i][j].quantity // same as above
                            }
                        }
                    }
                    datas.push({
                        key: i.toString(),
                        name: this.state.product_id_arr[i][j].product_id,
                        rate: rate_tmp,
                        money: money_tmp
                    })
                }
            }
        }

        // const datas = [{
        //     key: '1',
        //     name: 'John Brown',
        //     rate: 32,
        //     money: 'New York No',
        // }, {
        //     key: '2',
        //     name: 'Jim Green',
        //     rate: 42,
        //     money: 'London No',
        // }, {
        //     key: '3',
        //     name: 'Joe Black',
        //     rate: 32,
        //     money: 'Sidney No',
        // }];
        const abroad = [{
            value: 'zhejiang',
            label: 'Zhejiang',
        }, {
            value: 'jiangsu',
            label: 'Jiangsu',
        }];
        if (this.props.quoteInfo != null) {
            let postage = 0;
            for (let i = 0; i < this.state.postage.length; ++ i) {
                postage += this.state.postage[i];
            }
            let totalPrices = this.props.quoteInfo.delivery_address != 2 ?
            this.state.totalPrice + postage + (this.state.packagePrice + this.state.labelPrice + this.state.listPrice) * this.state.orderNum :
            this.state.totalPrice + postage + (this.state.packagePrice + this.state.labelPrice + this.state.listPrice) * this.state.orderNum;
            if(this.props.quoteInfo.delivery_address ==2){ // fixme: need to correct the delivery_address * rate or tax
                totalPrices = totalPrices * (this.state.rate == null?1: (this.state.rate/100.0))
                totalPrices += (this.state.tax == null?0: this.state.tax)
            }
            let totalPrice = Math.round(totalPrices*100)/100;
            return (
                <div className="PaymentPageFooterbox">
                    <ul>
                        <li>
                            <span className="right">{this.props.quoteInfo.delivery_address == 0 ? '￥' : '$'}{this.state.totalPrice}</span> {/*商品总金额*/}
                            <span className="right">{I18n.t('PaymentPageTable.gong')}<span style={{color:'#0c5aa2'}}>{this.state.totalQuantity}</span>{I18n.t('PaymentPageTable.jian')} </span>
                            <div className="clear"></div>
                        </li>
                        <li>
                            <span className="right">{this.props.quoteInfo.delivery_address == 0 ? '￥' : '$'}{this.state.postage}</span> {/*货运邮费*/}
                            <span className="right">{I18n.t('PaymentPageTable.charges')}</span>
                            <div className="clear"></div>
                        </li>
                        <li>
                            <span className="right">{this.props.quoteInfo.delivery_address == 0 ? '￥' : '$'}{this.state.packagePrice}*{this.state.orderNum}</span> {/*//包装费*/}
                            <span className="right">{I18n.t('PaymentPageTable.Packingcharge')}</span>
                            <div className="clear"></div>
                        </li>
                        <li>
                            <span className="right">{this.props.quoteInfo.delivery_address == 0 ? '￥' : '$'}{this.state.labelPrice}*{this.state.orderNum}</span> {/*//标签费*/}
                            <span className="right">{I18n.t('PaymentPageTable.Tagfee')}</span>
                            <div className="clear"></div>
                        </li>
                        <li style={this.props.quoteInfo != null && this.props.quoteInfo.delivery_address == 2 ? {display:'block'} : {display:'none'}}> {/*//关税*/}
								<span className="right">
                                    {this.state.tax == null?"无信息":this.state.tax}
									<div className="PaymentPagetariff">
										<span>{I18n.t('PaymentPageTable.details')}</span>
										<img src={config.theme_path + 'comjian.png'} alt="" />
										<Table bordered pagination={false} columns={columnss} dataSource={datas}  />
									</div>
								</span>
                            <span className="right">{I18n.t('PaymentPageTable.tariff')}</span>
                            <div className="clear"></div>
                        </li>
                        <li>
                            <span className="right">{this.props.quoteInfo.delivery_address == 0 ? '￥' : '$'}{this.state.listPrice}*{this.state.orderNum}</span> {/*//送货清单*/}
                            <span className="right">{I18n.t('PaymentPageTable.Deliverylist')}</span>
                            <div className="clear"></div>
                        </li>
                        <li  style={this.props.quoteInfo != null && this.props.quoteInfo.delivery_address == 1 ? {display:'block'} : {display:'none'}}> 
                            {/*<span className="right">￥40.00</span>
                            <span className="right"><Cascader options={abroad}  defaultValue={['zhejiang']} /></span>
                            <div className="clear"></div>*/}
                        </li>
                        <li>
                            {/*<span className="right">￥0</span>
                            <span className="right">
                                <Checkbox >{I18n.t('PaymentPageTable.aerfa')} 30 {I18n.t('ge')}</Checkbox>
                                <span style={{marginRight:0}}>{I18n.t('PaymentPageTable.Deductible')}</span>
                            </span>*/}
                            <div className="clear"></div>
                        </li>
                    </ul>
                    <p>{I18n.t('PaymentPageTable.Amountpayable')} <span style={{fontSize:28,color:'#d94730'}}>{(this.props.quoteInfo.delivery_address == 1 )? '$' : '¥'}{totalPrice}</span> </p> {/*//总价*/}
                </div>
            );
        }
    }
    Success(){
        let addressId = this.state.addressId == 0 ? this.props.currentUserInfo.defaultAddress : this.state.addressId;
        if(addressId == null){
            message.error('请选择收货地址！');
        }else{
            let files = [];
            for (let i = 0; i < this.state.optionflag.length; ++ i) {
                if(this.state.optionflag[i]){
                    $.ajax({
                        type: 'POST',
                        url: config.file_server + 'save_order',
                        data: { path: this.state.optionsfile[i]},
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                    files.push(this.state.optionsfile[i].replace('tmp/', ''));
                }
            }
            Meteor.call('quote.createOrder', addressId, this.state.totalPrice, this.state.totalQuantity, this.state.postage, this.state.packagePrice, this.state.labelPrice, this.state.listPrice,
                this.state.InvoiceType, this.state.InvoiceTitle, this.state.InvoiceWay, this.state.InvoiceEmail, this.state.InvoiceAddressType, this.state.InvoiceAddress, this.state.InvoiceName,
                this.state.options, files, this.state.order, this.state.remark,this.state.receive_all_tax,  function(err, data) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        for (let i = 0; i < data.seller.length; ++ i) {
                            Meteor.call('message.createMessage', Meteor.userId(), data.seller[i], 1, data.order[i].toString());
                        }
                        browserHistory.replace('/payment_money');
                    }
                }.bind(this));
        }
    }
    handleSubmit(e) {
        console.log(this.props.currentUserInfo.address,this.props.currentUserInfo);
        console.log(this.props.quoteInfo.delivery_address,this.state.rate,this.state.receive_all_tax);
        if(this.props.currentUserInfo.address== undefined ||this.props.currentUserInfo.address.length == 0) {
            message.warning('请填写付款人信息！');
        }else if( // fixme: need to correct the delivery_address * not in condition of rate or tax
            (this.props.quoteInfo.delivery_address == 2 && this.state.rate == null )){ // fixme: need to correct the delivery_address * rate
            alert(this.props.quoteInfo.delivery_address)
            message.warning('汇率信息不正确！');
        }
        // else if(!(this.props.quoteInfo.delivery_address == 0 || // fixme: need to correct the delivery_address * not in condition of rate or tax
        //     (this.props.quoteInfo.delivery_address == 1 && this.state.receive_all_tax ))){ // fixme: need to correct the delivery_address * tax
        //     message.warning('关税信息不正确！');
        // }
        else{
            if(this.state.agree == false){
                message.warning('请确认并同意质保信息！');
            }else if (this.state.agree == true){
                if (Roles.userIsInRole(Meteor.userId(), 'person')) {
                    this.Success();
                }else{
                    if(this.props.quoteInfo.delivery_address != 2){
                        if(this.state.InvoiceType == 1 && this.state.InvoiceTitle == '' ){
                            this.setState({titleflag:true});
                            window.scrollTo(0,0);
                            if(this.state.InvoiceWay == 1 && this.state.InvoiceEmail == '' ){
                                this.setState({emailflag:true});
                                window.scrollTo(0,0);
                            }
                        }else if(this.state.InvoiceType == 1 && this.state.InvoiceWay == 1 && this.state.InvoiceEmail == '' ){
                            this.setState({emailflag:true});
                            window.scrollTo(0,0);
                        }
                        else{
                            this.Success();
                        }
                    }else if (this.props.quoteInfo.delivery_address == 2){
                        if(this.state.InvoiceTitle == '' ){
                            this.setState({titleflags:true});
                        }
                        if(this.state.InvoiceEmail == ''){
                            this.setState({emailflags:true});
                        }
                        if(this.state.InvoiceName == ''){
                            this.setState({nameflags:true});
                        }
                        if(this.state.InvoiceTitle != '' && this.state.InvoiceEmail != '' && this.state.InvoiceName != ''){
                            this.Success();
                        }
                    }
                }

            }
        }
    }
    showaddaddress(){
        this.setState({addaddress:true});
    }
    closeaddaddress(){
        this.setState({addaddress:false});
    }
    showAddaddress(){
        if(this.state.addaddress == true){
            return(
                <AddAddress click={this.closeaddaddress.bind(this)} />
            )
        }
    }
    render() {
        console.log(this.state,this.props.quoteInfo,'render')
        return (
            <div className="PaymentPage">
                <div className="PaymentPage-inner">
                    <p>{I18n.t('PaymentPage.topname')}</p>
                    {this.ShowPaymentAdress()}
                    {this.ShowExpressinformation()}
                    {this.ShowInvoiceinformation()}
                    <div className="PaymentPageBox">
                        <p className="PaymentPageBoxTop">
                            {this.ShowFour()}
                            <p className="left">{I18n.t('PaymentPage.Warrantyinformation')}</p>
                            <p className="clear"></p>
                        </p>
                        <div className="WarrantyinformationBox">
                            <div className="WarrantyinformationBoxInner">
                                <p style={{textAlign:'center'}}>质保服务说明</p>
                                <p>为确保质保服务正常有序开展，请用户仔细阅读以下质保服务说明。</p>
                                <p>1定义</p>
                                <p>质保是用户向专业鉴定机构提供交易的部分或全部商品，用一系列试验来鉴别电子元件类商品可靠性或真伪性的一种方法。</p>
                                <p>2质保介绍</p>
                                <p>ICXYZ商城在平台上提供用户与质保单位的对接服务，质保服务由专业质保机构提供。买卖双方在平台交易过程中有质量疑问时，其中一方可以提出质保申请，选择质保服务即平台默认买卖双方认可质保机构出具相关鉴定证明真实有效，否则不能发起质保。其中质保结果由质保单位出具，ICxyz平台依据质保单位出具的报告，将对争议订单做出不同的处理意见。具体处理意见3.1.2质保申请的发起和相应责任或质保页面的处理结果。注意，同意此质保说明即意味着同意平台的处理结果，同意在平台上交易亦意味着同意平台的处理结果。</p>
                                <p>3质保流程</p>
                                <p> 3.1发起质保</p>
                                <p> 3.1.1发起质保条件</p>
                                <p> 用户要求的质保服务所要鉴定的商品，必须是争议订单的部分或全部商品，且必须为一个或多个争议型号的同一批次商品。如出现非本订单或非同一批次的商品，平台将不予受理此次质保申请。注意，提供用于质保试验商品的用户对该质保样品负有真实性义务，若出现私自替换，或其他任何形式的加工修改等造假行为，造成质保结果不准确，该用户的负全部责任。</p>
                                <p>若用户不能提供完好的，可供鉴定的商品，则质保无法开展，平台将无法受理质保申请。</p>
                                <p>用户发起质保的申请需要在确认收货后180日内提交，逾期发起质保，平台将不予受理。</p>
                                <p>3.1.2 质保申请的发起和相应责任</p>
                                <p>依据平台用户注册协议，若买卖双方中的一方在对方认可的质保机构范围内发起质保，即平台默认买卖双方认可质保机构出具的报告，平台可依据报告，做出对争议订单的处理。</p>
                                <p>
                                    买方发起：买方收货后对产品的质量存有疑义，在与卖方无法达成一致意见时，在该订单的管理页面提出在线申请，选择质保机构，提供相关资料，并预先支付质保服务产生的所有费用。若鉴定结果表明产品质量确有问题，则质保费用应由卖方承担，此时若货款在平台，平台将执行退款；若货款已结算给卖方，平台将帮助买方与卖方协商退货退款事宜，若卖方拒不配合，平台将提供买方相关证据，以便买方起诉或仲裁，但平台不承担为买方追讨货款和质保费的义务和法律责任。若鉴定结果表明产品质量没有问题，则质保费用应由买方承担，平台视此次交易为正常完成。若鉴定结果无法确定产品质量是否存在问题。则质保费用由发起方承担。若此时货款不在平台，买家仍认为商品质量存在问题，可以向平台申请提取争议订单详情及相关材料。
                                </p>
                                <p>卖方发起：卖方在买方对产品质量有疑义，且与买方无法达成一致意见时，可以提出质保申请以证明清白。在该订单的管理页面提出在线申请，选择质保机构，提供相关资料，并预先支付质保服务产生的所有费用。注意：卖方发起质保无论质保结果如何，质保费用都应由卖方承担。若质保报告显示卖方商品质量确有问题，买方应退货，卖方应退款 。若质量不存在问题，平台视此次交易为正常订单。 </p>
                                <p>3.1.3支付质保费</p>
                                <p>不同质保机构有不同的收费标准，具体收费标准将由质保机构在线更新。</p>
                                <p>支付质保费可以选择在线支付或线下转账，线下转账后需上传汇款底单。底单查询到帐后，该质保申请即刻生效。</p>
                                <p>ICxyz商城对所收的质保费不支付任何手续费和利息。  </p>
                                <p>3.1.4 送检</p>
                                <p>送检方式：直接寄送，平台代送等方式</p>
                                <p>具体送检方式和注意事项将由质保机构在线更新。</p>
                                <p>3.1.5 质保结果查询</p>
                                <p>质保报告出具时间由质保机构在线更新。买卖双方均可在线查询进度，并下载质保报告。</p>
                                <p>3.1.6质保结论的处理</p>
                                <p>平台将依据质保报告的结论进行处理，具体处理方法将在质保工单的结果页面显示和更新。</p>
                                <p>4责任追究及处罚</p>
                                <p>如平台判定在质保过程中买方或卖方存在诬陷或造假等过错，平台将依据平台规则，扣除买家或卖家相应的信用等级分，并对账号进行包括但不限于：限制平台活动，冻结账号，关店，全网通告，黑名单等处罚；</p>
                                <p>若卖方存在以上情形，平台将依据平台规则，扣除卖家相应平台把此次交易视为卖方销售假冒伪劣商品，卖方应返还货款和质保费，若货款在平台上，平台将会把相关货款返还给买方。若货款已结算给卖方，平台将帮助买方与卖方协商，但不承担相应的义务和法律责任。若卖方拒绝执行平台处理结果，平台有权从卖方店铺的待结货款、店铺保证金中支付卖方损失。</p>
                                <p> 如果有双倍赔偿协议的供应商，参见双倍赔付协议条款。</p>
                                <p>ICxyz作为第三方交易平台不承担任何费用和法律责任。</p>
                                <p>ICxyz商城对此协议具有最终解释权。</p>
                                <p>协议自声明之日起生效。</p>
                                <p style={{textAlign:'right',marginTop:10}}>2016年11月10日</p>
                            </div>
                            <Checkbox onChange={this.Agree.bind(this)} value="Agree">{I18n.t('PaymentPage.Agree')}</Checkbox>
                        </div>
                    </div>
                    {this.showTable()}
                    {this.showFooter()}
                    <Button className="right" type="primary" onClick={this.handleSubmit.bind(this)}>{I18n.t('placeorder')}</Button>
                    <div className="clear"></div>
                </div>
                {this.showAddaddress()}
            </div>
        );
    }
}


// todo: import function hex_md5
// var hexcase = 0; /* hex output format. 0 - lowercase; 1 - uppercase  */
// var b64pad = ""; /* base-64 pad character. "=" for strict RFC compliance */
// var chrsz = 8; /* bits per input character. 8 - ASCII; 16 - Unicode  */
// /*
//  * These are the functions you'll usually want to call
//  * They take string arguments and return either hex or base-64 encoded strings
//  */
// function hex_md5(s){ return binl2hex(core_md5(str2binl(s), s.length * chrsz));}
// function b64_md5(s){ return binl2b64(core_md5(str2binl(s), s.length * chrsz));}
// function str_md5(s){ return binl2str(core_md5(str2binl(s), s.length * chrsz));}
// function hex_hmac_md5(key, data) { return binl2hex(core_hmac_md5(key, data)); }
// function b64_hmac_md5(key, data) { return binl2b64(core_hmac_md5(key, data)); }
// function str_hmac_md5(key, data) { return binl2str(core_hmac_md5(key, data)); }
/*
 * Perform a simple self-test to see if the VM is working
 */

// function core_md5(x, len)
// {
//     /* append padding */
//     x[len >> 5] |= 0x80 << ((len) % 32);
//     x[(((len + 64) >>> 9) << 4) + 14] = len;
//     var a = 1732584193;
//     var b = -271733879;
//     var c = -1732584194;
//     var d = 271733878;
//     for(var i = 0; i < x.length; i += 16)
//     {
//         var olda = a;
//         var oldb = b;
//         var oldc = c;
//         var oldd = d;
//         a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
//         d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
//         c = md5_ff(c, d, a, b, x[i+ 2], 17, 606105819);
//         b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
//         a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
//         d = md5_ff(d, a, b, c, x[i+ 5], 12, 1200080426);
//         c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
//         b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
//         a = md5_ff(a, b, c, d, x[i+ 8], 7 , 1770035416);
//         d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
//         c = md5_ff(c, d, a, b, x[i+10], 17, -42063);
//         b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
//         a = md5_ff(a, b, c, d, x[i+12], 7 , 1804603682);
//         d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);
//         c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
//         b = md5_ff(b, c, d, a, x[i+15], 22, 1236535329);
//         a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
//         d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
//         c = md5_gg(c, d, a, b, x[i+11], 14, 643717713);
//         b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
//         a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
//         d = md5_gg(d, a, b, c, x[i+10], 9 , 38016083);
//         c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);
//         b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
//         a = md5_gg(a, b, c, d, x[i+ 9], 5 , 568446438);
//         d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
//         c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
//         b = md5_gg(b, c, d, a, x[i+ 8], 20, 1163531501);
//         a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
//         d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
//         c = md5_gg(c, d, a, b, x[i+ 7], 14, 1735328473);
//         b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);
//         a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
//         d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
//         c = md5_hh(c, d, a, b, x[i+11], 16, 1839030562);
//         b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);
//         a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
//         d = md5_hh(d, a, b, c, x[i+ 4], 11, 1272893353);
//         c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
//         b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
//         a = md5_hh(a, b, c, d, x[i+13], 4 , 681279174);
//         d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
//         c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
//         b = md5_hh(b, c, d, a, x[i+ 6], 23, 76029189);
//         a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
//         d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);
//         c = md5_hh(c, d, a, b, x[i+15], 16, 530742520);
//         b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);
//         a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
//         d = md5_ii(d, a, b, c, x[i+ 7], 10, 1126891415);
//         c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
//         b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
//         a = md5_ii(a, b, c, d, x[i+12], 6 , 1700485571);
//         d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
//         c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);
//         b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
//         a = md5_ii(a, b, c, d, x[i+ 8], 6 , 1873313359);
//         d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);
//         c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
//         b = md5_ii(b, c, d, a, x[i+13], 21, 1309151649);
//         a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
//         d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
//         c = md5_ii(c, d, a, b, x[i+ 2], 15, 718787259);
//         b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);
//         a = safe_add(a, olda);
//         b = safe_add(b, oldb);
//         c = safe_add(c, oldc);
//         d = safe_add(d, oldd);
//     }
//     return Array(a, b, c, d);
// }
// /*
//  * These functions implement the four basic operations the algorithm uses.
//  */
// function md5_cmn(q, a, b, x, s, t)
// {
//     return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);
// }
// function md5_ff(a, b, c, d, x, s, t)
// {
//     return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
// }
// function md5_gg(a, b, c, d, x, s, t)
// {
//     return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
// }
// function md5_hh(a, b, c, d, x, s, t)
// {
//     return md5_cmn(b ^ c ^ d, a, b, x, s, t);
// }
// function md5_ii(a, b, c, d, x, s, t)
// {
//     return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
// }
// /*
//  * Calculate the HMAC-MD5, of a key and some data
//  */
// function core_hmac_md5(key, data)
// {
//     var bkey = str2binl(key);
//     if(bkey.length > 16) bkey = core_md5(bkey, key.length * chrsz);
//     var ipad = Array(16), opad = Array(16);
//     for(var i = 0; i < 16; i++)
//     {
//         ipad[i] = bkey[i] ^ 0x36363636;
//         opad[i] = bkey[i] ^ 0x5C5C5C5C;
//     }
//     var hash = core_md5(ipad.concat(str2binl(data)), 512 + data.length * chrsz);
//     return core_md5(opad.concat(hash), 512 + 128);
// }
// /*
//  * Add integers, wrapping at 2^32. This uses 16-bit operations internally
//  * to work around bugs in some JS interpreters.
//  */
// function safe_add(x, y)
// {
//     var lsw = (x & 0xFFFF) + (y & 0xFFFF);
//     var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
//     return (msw << 16) | (lsw & 0xFFFF);
// }
// /*
//  * Bitwise rotate a 32-bit number to the left.
//  */
// function bit_rol(num, cnt)
// {
//     return (num << cnt) | (num >>> (32 - cnt));
// }
// /*
//  * Convert a string to an array of little-endian words
//  * If chrsz is ASCII, characters >255 have their hi-byte silently ignored.
//  */
// function str2binl(str)
// {
//     var bin = Array();
//     var mask = (1 << chrsz) - 1;
//     for(var i = 0; i < str.length * chrsz; i += chrsz)
//         bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (i%32);
//     return bin;
// }
// /*
//  * Convert an array of little-endian words to a string
//  */
// function binl2str(bin)
// {
//     var str = "";
//     var mask = (1 << chrsz) - 1;
//     for(var i = 0; i < bin.length * 32; i += chrsz)
//         str += String.fromCharCode((bin[i>>5] >>> (i % 32)) & mask);
//     return str;
// }
// /*
//  * Convert an array of little-endian words to a hex string.
//  */
// function binl2hex(binarray)
// {
//     var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
//     var str = "";
//     for(var i = 0; i < binarray.length * 4; i++)
//     {
//         str += hex_tab.charAt((binarray[i>>2] >> ((i%4)*8+4)) & 0xF) +
//             hex_tab.charAt((binarray[i>>2] >> ((i%4)*8 )) & 0xF);
//     }
//     return str;
// }
// /*
//  * Convert an array of little-endian words to a base-64 string
//  */
// function binl2b64(binarray)
// {
//     var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
//     var str = "";
//     for(var i = 0; i < binarray.length * 4; i += 3)
//     {
//         var triplet = (((binarray[i >> 2] >> 8 * ( i %4)) & 0xFF) << 16)
//             | (((binarray[i+1 >> 2] >> 8 * ((i+1)%4)) & 0xFF) << 8 )
//             | ((binarray[i+2 >> 2] >> 8 * ((i+2)%4)) & 0xFF);
//         for(var j = 0; j < 4; j++)
//         {
//             if(i * 8 + j * 6 > binarray.length * 32) str += b64pad;
//             else str += tab.charAt((triplet >> 6*(3-j)) & 0x3F);
//         }
//     }
//     return str;
// }