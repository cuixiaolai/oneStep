import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link,browserHistory } from 'react-router'
// import { createContainer } from 'meteor/react-meteor-data';
import { Tabs,message} from 'antd';
import config from '../../config.json';
{/*支付款*/}

const styles=({
    bg:{
        backgroundImage:'url('+config.theme_path+'paymentMark.png)',

    },
})

export default class PaymentMoneyMethod extends Component {
    constructor(props) {
        super(props);
        this.state = ({

            onSpan:0,
            active:'active',
            onImg:0,
        });
    }
    clickLi(value){
        this.setState({onSpan:value});
    }
    clickImg(value){
        this.setState({onImg:value});
    }
    showTab(){
        let temp = [];
        if(this.state.onSpan == 0){
            temp[0] = (
                <div className="paymentmethod">
                    <div className="methodImg">
                        <img src={(config.theme_path+'jianhang.jpg')} onClick={this.clickImg.bind(this,0)} className={this.state.onImg==0?'active':''}  alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==0?styles.bg:{}}> </span>
                    </div>
                    <div className="methodImg">
                        <img src={(config.theme_path+'weixin.jpg')} onClick={this.clickImg.bind(this,1)} className={this.state.onImg==1?'active':''} alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==1?styles.bg:{}}> </span>
                    </div>
                    <div className="methodImg">
                        <img src={(config.theme_path+'zhifu.jpg')} onClick={this.clickImg.bind(this,2)} className={this.state.onImg==2?'active':''} alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==2?styles.bg:{}}> </span>
                    </div>

                </div>
            )
        }else if(this.state.onSpan == 1){
            temp[0] = (
                <div className="paymentmethod">
                    <div className="methodImg">
                        <img src={(config.theme_path+'jianhang.jpg')} onClick={this.clickImg.bind(this,0)} className={this.state.onImg==0?'active':''}  alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==0?styles.bg:{}}> </span>
                    </div>
                    <div className="methodImg">
                        <img src={(config.theme_path+'weixin.jpg')} onClick={this.clickImg.bind(this,1)} className={this.state.onImg==1?'active':''} alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==1?styles.bg:{}}> </span>
                    </div>
                    <div className="methodImg">
                        <img src={(config.theme_path+'zhifu.jpg')} onClick={this.clickImg.bind(this,2)} className={this.state.onImg==2?'active':''} alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==2?styles.bg:{}}> </span>
                    </div>
                </div>
            )
        }else if(this.state.onSpan == 2){
            temp[0] = (
                <div className="paymentmethod">
                    <div className="methodImg">
                        <img src={(config.theme_path+'jianhang.jpg')} onClick={this.clickImg.bind(this,0)} className={this.state.onImg==0?'active':''}  alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==0?styles.bg:{}}> </span>
                    </div>
                    <div className="methodImg">
                        <img src={(config.theme_path+'weixin.jpg')} onClick={this.clickImg.bind(this,1)} className={this.state.onImg==1?'active':''} alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==1?styles.bg:{}}> </span>
                    </div>
                    <div className="methodImg">
                        <img src={(config.theme_path+'zhifu.jpg')} onClick={this.clickImg.bind(this,2)} className={this.state.onImg==2?'active':''} alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==2?styles.bg:{}}> </span>
                    </div>
                </div>
            )
        }else if(this.state.onSpan == 3){
            temp[0] = (
                <div className="paymentmethod">
                    <div className="methodImg">
                        <img src={(config.theme_path+'jianhang.jpg')} onClick={this.clickImg.bind(this,0)} className={this.state.onImg==0?'active':''}  alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==0?styles.bg:{}}> </span>
                    </div>
                    <div className="methodImg">
                        <img src={(config.theme_path+'weixin.jpg')} onClick={this.clickImg.bind(this,1)} className={this.state.onImg==1?'active':''} alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==1?styles.bg:{}}> </span>
                    </div>
                    <div className="methodImg">
                        <img src={(config.theme_path+'zhifu.jpg')} onClick={this.clickImg.bind(this,2)} className={this.state.onImg==2?'active':''} alt="图片"/>
                        <span className="imgmark" style={this.state.onImg==2?styles.bg:{}}> </span>
                    </div>

                </div>

            )
        }
        return temp;
    }
    handleSubmit(){
        if(this.props.type == 'buyer_order_payment'){
            Meteor.call('order.payImmediately',this.props.orders, (err) => {
                if (err) {
                    message.error(`支付失败。`);
                    browserHistory.replace('/personal_center/order');
                } else {
                    message.success(`支付成功。`);
                    console.log(this.props.orderInfo);
                    for (let i = 0; i < this.props.orderInfo.length; ++ i) {
                        Meteor.call('message.createMessage', Meteor.userId(), this.props.orderInfo[i].sellerId, 2, this.props.orderInfo[i].orderId.toString());
                    }
                    browserHistory.replace('/personal_center/order');
                }
            });
        }else if(this.props.type == 'buyer_quality_assurance_payment'){
            Meteor.call('order.buyerPayQualityAssurance', this.props.order_id, (err) => {
                if (err) {

                } else {

                }
            });
        }
    }
    render(){
        if(this.props.orderInfo != null && this.props.orderInfo.length > 0){
            console.log(this.props.orderInfo[0].payment_application)
            return(
                <div >
                    {/*<div className="clearFloat">*/}
                        {/*<ul>*/}
                            {/*<li ><span className={this.state.onSpan==0?'active':''} style={this.state.onSpan==0?styles.bg:{}} onClick={this.clickLi.bind(this,0)}>{I18n.t('shoppingCart.paymentMoney.recentUse')}</span></li>*/}
                            {/*<li><span  className={this.state.onSpan==1?'active':''} style={this.state.onSpan==1?styles.bg:{}}  onClick={this.clickLi.bind(this,1)}>{I18n.t('shoppingCart.paymentMoney.companyTransfer')}</span></li>*/}
                            {/*<li ><span className={this.state.onSpan==2?'active':''} style={this.state.onSpan==2?styles.bg:{}}  onClick={this.clickLi.bind(this,2)}>{I18n.t('shoppingCart.paymentMoney.thirdParty')}</span></li>*/}
                            {/*<li ><span className={this.state.onSpan==3?'active':''} style={this.state.onSpan==3?styles.bg:{}}  onClick={this.clickLi.bind(this,3)}>{I18n.t('shoppingCart.paymentMoney.payImmediately')}</span></li>*/}
                        {/*</ul>*/}
                    {/*</div>*/}

                    {/*{this.showTab()}*/}
                    {this.props.orderInfo[0].payment_application == 0?<span className="payImmediately" onClick={this.handleSubmit.bind(this)} >提交付款申请</span>:<div style={{color:'red',fontSize:20,paddingTop:50}}>审核中</div>}
                </div>);

        }else{
            return null
        }
    }
}