import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
// import { createContainer } from 'meteor/react-meteor-data';
import { Table,Checkbox  } from 'antd';
// import { Buyer } from '../../api/Buyer.js';
import config from '../../config.json';

export default class PaymentTable extends Component{
    constructor(props) {
        super(props);
        this.state = ({
           value:false,
        });
    }
    onChange(e){
        this.setState({value:e.target.checked});
        this.props.setDestPay(this.props.flag, e.target.checked);
    }
    render(){
        let price = 0;
        for(let i=0;i<this.props.data.length;i++){
            price = price + this.props.data[i].price;
        }
        return(
            <Table
                columns={this.props.columns}
                dataSource={this.props.data}
                title={() => <div className="PaymentTableTitle"><p>{this.props.name}</p><div className="PaymentTableIcon">{I18n.t('shoppingCart.content.' + this.props.type)}</div><p><img src={(config.theme_path +'tail_03.png'　)} alt=""/></p><div className="clear"></div></div>}
                footer={() =><div className="PaymentTableFooter">
                    <span className="right">{I18n.t('PaymentPageTable.shopPrice')}{this.props.deliveryAddress == 0 ? '￥' : '$'}{price}</span>
                    <span className="right">{this.props.candestPay == true ? (
                        <Checkbox onChange={this.onChange.bind(this)}>{I18n.t('PaymentPageTable.email')}</Checkbox>
                    ) : (
                        <Checkbox defaultChecked disabled>{I18n.t('PaymentPageTable.email')}</Checkbox>
                    )}</span>
                    <span className="right" style={this.state.value ?　{textDecoration:'line-through'} : {}}>{I18n.t('PaymentPageTable.eamilprice')}</span>
                    <div className="clear"></div>
                </div>}
                showHeader = {false}
                className="PaymentTable"
                bordered
                pagination = {false}
            />
        )
    }

}

