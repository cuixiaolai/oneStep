import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router'
import { Meteor } from 'meteor/meteor';

import { createContainer } from 'meteor/react-meteor-data';
import { Quote } from '../../api/Quote.js';
import { Tabs } from 'antd';
import { connect } from 'react-redux';
import config from '../../config.json';
import PaymentMoneyMethod from './PaymentMoneyMethod.jsx';
{/*支付款*/}

export default class PaymentMoney extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            flag: false,
            totalPrice: 0,
            currency: 1,
            orderInfo: [],
            delivery_address: (0)
        });
        this.clickDetails = this.clickDetails.bind(this);
    }
    componentDidMount() {
      console.log('componentDidMount')
      if(this.state.currency == 2) { this.state.delivery_address = 1 }
        if (this.props.quoteInfo != null) {
          console.log(this.props.quoteInfo)
            let currency = this.props.quoteInfo.currency;
            this.state.orderInfo = [];
            this.state.totalPrice = 0;
            this.setState({currency: currency, delivery_address:this.props.quoteInfo.delivery_address, orderInfo: this.state.orderInfo, totalPrice: this.state.totalPrice});
            try {
              if(window.ccc == 1){
                console.log('------11111-----')
                // this.setState({currency:0})
              }
            } catch (e) {
              console.log('ccccccc')
            } finally {
            }

            for (let i = 0; i < this.props.quoteInfo.orders.length; ++ i) {
                Meteor.call('order.findPaymentInfo', this.props.quoteInfo.orders[i], function(err, data) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log(data);

                        var xhr_rate = new XMLHttpRequest();
                        xhr_rate.open('get', config.file_server+'crossDomain', true);
                        xhr_rate.send();
                        xhr_rate.onload = function (event) {
                            const rate_ret = JSON.parse(xhr.responseText)
                            if (rate_ret.error_code == 0 &&
                                typeof rate_ret.result != 'undefined' &&
                                typeof rate_ret.result.list != 'undefined' &&
                                rate_ret.result.list.length > 0) {
                                var rate =  rate_ret.result.list[0][2] || 688

                                    let item = {
                                        orderId: data.id,
                                        sellerId: data.seller_id,
                                        name: data.name,
                                        company: data.company,
                                        price: Math.round(data.price*100)/100,
                                        payment_application:data.payment_application
                                    }
                                    this.state.orderInfo.push(item);
                                    this.state.totalPrice += item.price;
                                    this.setState({orderInfo: this.state.orderInfo, totalPrice: (this.state.totalPrice*rate)/100});
                            }
                        }.bind(this)
                    }
                }.bind(this))
            }
        }
    }
    componentWillReceiveProps(nextProps) {
      console.log('componentWillReceiveProps')
        if (nextProps.quoteInfo.orders != null && this.props.quoteInfo == null) {
            let currency = nextProps.quoteInfo.currency;
            this.state.orderInfo = [];
            this.state.totalPrice = 0;
            if(currency == 2) { let delivery_address = 1;
              this.setState({delivery_address:delivery_address})}

            console.log(this.state)

            this.setState({currency: currency, orderInfo: this.state.orderInfo, totalPrice: this.state.totalPrice});
            for (let i = 0; i < nextProps.quoteInfo.orders.length; ++ i) {
                Meteor.call('order.findPaymentInfo', nextProps.quoteInfo.orders[i], function(err, data) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log(data);

                        console.log(this.state)
                        let item = {
                            orderId: data.id,
                            sellerId: data.seller_id,
                            name: data.name,
                            company: data.company,
                            price: Math.round(data.price*100)/100,
                            payment_application:data.payment_application
                        }
                        this.state.orderInfo.push(item);
                        this.state.totalPrice += item.price;
                        this.setState({orderInfo: this.state.orderInfo, totalPrice: this.state.totalPrice,delivery_address:delivery_address});
                    }
                }.bind(this))
            }
        }
    }
    showTable(){
        let temp = [];
        for(let i = 0; i < this.state.orderInfo.length; ++ i){
            temp[i]=(
                <tr>
                    <td>{this.state.orderInfo[i].orderId}</td>
                    <td>{this.state.orderInfo[i].name}</td>
                    <td ><br/>阿尔法贝特（天津）供应链管理股份有限公司<br/><br/>
                      开户行：招商银行股份有限公司天津滨海分行营业部<br/><br/>
                      帐号：122906439310301</td>

                    <td>{(this.state.delivery_address == 1 )? '$' : '¥'}{this.state.orderInfo[i].price}</td>
                </tr>
            );
        }
        return temp;
    }
    showPayment(){
        let temp = [];
        temp[0]=(
            <div className="paymentPlatform">
                <h2>{I18n.t('shoppingCart.paymentMoney.paymentPlatform')}</h2>
            </div>
        );
        return temp;
    }
    clickDetails(){
        this.setState({flag:!this.state.flag});
    }
    render(){
        return(
            <div className="paymentMoney">
                <div className="paymentTop">
                    <div>
                        <p>{I18n.t('shoppingCart.paymentMoney.prompt')}</p>
                        <p className="price">{I18n.t('shoppingCart.paymentMoney.price')} <span>{(this.state.delivery_address == 1 )? '$' : '¥'}{this.state.totalPrice}</span> </p>
                    </div>
                    <p className="p2">{I18n.t('shoppingCart.paymentMoney.message')}</p>
                </div>
                <div className="details" >
                    <a href="#" onClick={this.clickDetails}>{I18n.t('shoppingCart.paymentMoney.details')}</a>
                    <img style={this.state.flag ? {transform:'scale(0.667) rotate(180deg)'} : {transform:'scale(0.667) rotate(0deg)'}} src={(config.theme_path+'details.png')} alt="图片"/>
                </div>
                    <table style={this.state.flag ? {diaplay:'block',width:'96%'} : {display:'none'}} >
                        <tr>
                            <td>{I18n.t('shoppingCart.paymentMoney.transactionNumber')}</td>
                            <td>{I18n.t('shoppingCart.paymentMoney.name')}</td>
                            <td>{I18n.t('shoppingCart.paymentMoney.payee')}</td>
                            <td>{I18n.t('shoppingCart.paymentMoney.loan')}</td>
                        </tr>
                        {this.showTable()}
                    </table>
                <p style={{marginTop:'50px',fontSize:'23px',fontWeight:'800',color:'red'}}>请将汇款底单发邮件至payment@alphabeta-group.com</p>
                {/*{this.showPayment()}*/}
                <PaymentMoneyMethod type='buyer_order_payment' orders={typeof this.props.quoteInfo =='undefined'? []:this.props.quoteInfo.orders } orderInfo={this.state.orderInfo}/>
            </div>
        );
    }
}
export default createContainer(() => {
    Meteor.subscribe('quote');
    return {
        quoteInfo: Quote.findOne({_id: Meteor.userId()}),
    };
}, PaymentMoney);
