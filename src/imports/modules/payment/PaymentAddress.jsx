import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Upload,Modal,Radio,Input,Checkbox,Cascader,Form,message } from 'antd';
import config from '../../config.json';
const confirm = Modal.confirm;

import EditAddress from '../center/EditAddress.jsx';

export default class PaymentAddress extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            key:1,
            flags: true,
            address: null,
            num:0,
        });
    }
    clickOpen(){
        this.setState({flags: false});
    }
    clickClose(){
        this.setState({flags: true});
    }
    delClick(i){
        let itemId = i;
        confirm({
            title: I18n.t('Addresspage.tips'),
            content: '',
            okText:'是',
            cancelText:'否',
            onOk(){
                Meteor.call('buyer.deleteAddress', itemId);
            },
            onCancel() {},
        });
    }
    Click(i){
        Meteor.call('buyer.setDefaultAddress', i);
        window.scrollTo(0,0);
    }
    Onclick(id){
        for(let i=0;i<this.props.address.address.length;i++){
            if(this.props.address.address[i].id == id){
                this.setState({num: i});
            }
        }
        this.props.onclick(id);
    }
    EditAddress() {
        if (this.state.flags == false) {
            if (this.props.address != null) {
                return (
                    <EditAddress click={this.clickClose.bind(this)} item={this.props.address.address[this.state.num]}/>
                )
            }
        }
    }
    ShowAddress(){
        let Address=[];
        if(this.props.address != null){
            if ( this.props.address.address != null && this.props.address.address.length !=0){
                let num = this.props.address.defaultAddress;
                if(this.props.flag == false){
                    if(this.props.address.address.length<=5){
                        for(let i=0;i<this.props.address.address.length;i++){
                            if(this.props.address.address[i].id != num){
                                Address[i] = (
                                    <p key={i} id={this.props.address.address[i].id} onClick={this.Onclick.bind(this,this.props.address.address[i].id)}>
                                        <span>{this.props.address.address[i].address}</span>
                                        <span>{this.props.address.address[i].detailAddress}</span>
                                        <span>{this.props.address.address[i].name}{I18n.t('shou')}</span>
                                        <span>{this.props.address.address[i].phone.slice(3)}</span>
                                        <span onClick={this.delClick.bind(this,this.props.address.address[i].id)} className="right cursor">{I18n.t('Addresspage.del')}</span>
                                        <span  onClick={this.clickOpen.bind(this)} className="right cursor" style={{color:'#0c5aa2'}}>{I18n.t('PerCenterRight.modify')}</span>
                                        <span onClick={this.Click.bind(this,this.props.address.address[i].id)} className="right cursor">{I18n.t('Addresspage.todefault')}</span>
                                        <div className="PaymentPageBoxAddressDefaultadd">
                                            <img src={(config.theme_path + 'address_03.png')} alt=""/>
                                        </div>
                                        <span className="clear"></span>
                                    </p>
                                )
                            }
                            else{
                                Address[i] = (
                                    <p key={i} id={this.props.address.address[i].id} className="PaymentPageBoxAddressDefault"  onClick={this.Onclick.bind(this,this.props.address.address[i].id)}>
                                        <span>{this.props.address.address[i].address}</span>
                                        <span>{this.props.address.address[i].detailAddress}</span>
                                        <span>{this.props.address.address[i].name}{I18n.t('shou')}</span>
                                        <span>{this.props.address.address[i].phone.slice(3)}</span>
                                        <span style={{color:'#999'}}>{I18n.t('Addresspage.defaultaddress')}</span>
                                        <span  onClick={this.delClick.bind(this,this.props.address.address[i].id)} className="right cursor">{I18n.t('Addresspage.del')}</span>
                                        <span  onClick={this.clickOpen.bind(this)} className="right cursor" style={{color:'#0c5aa2'}}>{I18n.t('PerCenterRight.modify')}</span>
                                        <div className="PaymentPageBoxAddressDefaultadd">
                                            <img src={(config.theme_path + 'address_03.png')} alt=""/>
                                        </div>
                                        <span className="clear cursor"></span>
                                        <div className="PaymentPageBoxAddressDefaultright">
                                            <img src={(config.theme_path + 'addright_03.png')} alt=""/>
                                        </div>
                                    </p>
                                )
                            }
                        }
                    }
                    else {
                        let newAddress = [];
                        for(let i=0;i<this.props.address.address.length;i++){
                            if(this.props.address.address[i].id != num){
                                Address[i] = (
                                    <p key={i}  id={this.props.address.address[i].id} onClick={this.Onclick.bind(this,this.props.address.address[i].id)}>
                                        <span>{this.props.address.address[i].address}</span>
                                        <span>{this.props.address.address[i].detailAddress}</span>
                                        <span>{this.props.address.address[i].name}{I18n.t('shou')}</span>
                                        <span>{this.props.address.address[i].phone.slice(3)}</span>
                                        <span onClick={this.delClick.bind(this,this.props.address.address[i].id)} className="right cursor">{I18n.t('Addresspage.del')}</span>
                                        <span  onClick={this.clickOpen.bind(this)} className="right cursor" style={{color:'#0c5aa2'}}>{I18n.t('PerCenterRight.modify')}</span>
                                        <span onClick={this.Click.bind(this,this.props.address.address[i].id)} className="right cursor">{I18n.t('Addresspage.todefault')}</span>
                                        <div className="PaymentPageBoxAddressDefaultadd">
                                            <img src={(config.theme_path + 'address_03.png')} alt=""/>
                                        </div>
                                        <span className="clear"></span>
                                    </p>
                                )
                            }
                            else{
                                Address[i] = (
                                    <p key={i} id={this.props.address.address[i].id} className="PaymentPageBoxAddressDefault"  onClick={this.Onclick.bind(this,this.props.address.address[i].id)}>
                                        <span>{this.props.address.address[i].address}</span>
                                        <span>{this.props.address.address[i].detailAddress}</span>
                                        <span>{this.props.address.address[i].name}{I18n.t('shou')}</span>
                                        <span>{this.props.address.address[i].phone.slice(3)}</span>
                                        <span>{I18n.t('Addresspage.defaultaddress')}</span>
                                        <span  onClick={this.delClick.bind(this,this.props.address.address[i].id)} className="right cursor">{I18n.t('Addresspage.del')}</span>
                                        <span  onClick={this.clickOpen.bind(this)} className="right cursor" style={{color:'#0c5aa2'}}>{I18n.t('PerCenterRight.modify')}</span>
                                        <div className="PaymentPageBoxAddressDefaultadd">
                                            <img src={(config.theme_path + 'address_03.png')} alt=""/>
                                        </div>
                                        <span className="clear cursor"></span>
                                        <div className="PaymentPageBoxAddressDefaultright">
                                            <img src={(config.theme_path + 'addright_03.png')} alt=""/>
                                        </div>
                                    </p>
                                )
                                newAddress.push(Address[i]);
                            }
                        }
                        for(let i =0 ;i<this.props.address.address.length;i++){
                            if( Address[i].key!=newAddress[0].key && newAddress.length<5){
                                newAddress.push(Address[i]);
                            }
                        }
                        Address = newAddress;
                    }
                }
                else if(this.props.flag == true){
                    for(let i=0;i<this.props.address.address.length;i++){
                        if(this.props.address.address[i].id != num){
                            Address[i] = (
                                <p key={i} id={this.props.address.address[i].id} onClick={this.Onclick.bind(this,this.props.address.address[i].id,i)}>
                                    <span>{this.props.address.address[i].address}</span>
                                    <span>{this.props.address.address[i].detailAddress}</span>
                                    <span>{this.props.address.address[i].name}{I18n.t('shou')}</span>
                                    <span>{this.props.address.address[i].phone.slice(3)}</span>
                                    <span onClick={this.delClick.bind(this,this.props.address.address[i].id)} className="right cursor">{I18n.t('Addresspage.del')}</span>
                                    <span  onClick={this.clickOpen.bind(this)} className="right cursor" style={{color:'#0c5aa2'}}>{I18n.t('PerCenterRight.modify')}</span>
                                    <span onClick={this.Click.bind(this,this.props.address.address[i].id)} className="right cursor">{I18n.t('Addresspage.todefault')}</span>
                                    <div className="PaymentPageBoxAddressDefaultadd">
                                        <img src={(config.theme_path + 'address_03.png')} alt=""/>
                                    </div>
                                    <span className="clear"></span>
                                </p>
                            )
                        }
                        else{
                            Address[i] = (
                                <p key={i} id={this.props.address.address[i].id} className="PaymentPageBoxAddressDefault"  onClick={this.Onclick.bind(this,this.props.address.address[i].id,i)}>
                                    <span>{this.props.address.address[i].address}</span>
                                    <span>{this.props.address.address[i].detailAddress}</span>
                                    <span>{this.props.address.address[i].name}{I18n.t('shou')}</span>
                                    <span>{this.props.address.address[i].phone.slice(3)}</span>
                                    <span>{I18n.t('Addresspage.defaultaddress')}</span>
                                    <span  onClick={this.delClick.bind(this,this.props.address.address[i].id)} className="right cursor" >{I18n.t('Addresspage.del')}</span>
                                    <span  onClick={this.clickOpen.bind(this)} className="right cursor" style={{color:'#0c5aa2'}}>{I18n.t('PerCenterRight.modify')}</span>
                                    <div className="PaymentPageBoxAddressDefaultadd">
                                        <img src={(config.theme_path + 'address_03.png')} alt=""/>
                                    </div>
                                    <span className="clear cursor"></span>
                                    <div className="PaymentPageBoxAddressDefaultright">
                                        <img src={(config.theme_path + 'addright_03.png')} alt=""/>
                                    </div>
                                </p>
                            )
                        }
                    }
                }
            }else{
                Address = (
                    <div className="PaymentPageBoxAddressNo">
                        <span style={{color:'#ed7020'}}>{I18n.t('buyer.worm')}</span>{I18n.t('PaymentPage.noaddress')}
                    </div>
                )
            }
        }else{
            Address = (
                <div className="PaymentPageBoxAddressNo">
                    <span style={{color:'#ed7020'}}>{I18n.t('buyer.worm')}</span>{I18n.t('PaymentPage.noaddress')}
                </div>
            )
        }
        let TrueAddress = [];
        if(Address.length != undefined){
            for(let i=0;i<Address.length;i++){
                if(Address[i].props.className == 'PaymentPageBoxAddressDefault'){
                    TrueAddress.unshift(Address[i]);
                }else{
                    TrueAddress.push(Address[i]);
                }
            }
        }else{
            TrueAddress.push(Address);
        }
        return TrueAddress;
    }
    render(){
        return(
            <div className="PaymentPageBoxAddress">
                {this.ShowAddress()}
                {this.EditAddress()}
            </div>
        )
    }
}
