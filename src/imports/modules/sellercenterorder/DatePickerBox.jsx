import React, { Component } from 'react';
import { DatePicker } from 'antd';

const DatePickerBox = React.createClass({
        getInitialState() {
            return {
                startValue: null,
                endValue: null,
                endOpen: false,
            };
        },
    disabledStartDate(startValue) {
        const endValue = this.state.endValue;
        return  endValue?(startValue.getTime() >= Date.now() || startValue.getTime() > endValue.getTime()):(startValue.getTime() >= Date.now());
    },
    disabledEndDate(endValue) {
        const startValue = this.state.startValue;
        return startValue?(Date.now() <= endValue.getTime() || endValue.getTime() <= startValue.getTime()):(Date.now() <= endValue.getTime());
    }
    ,
    onStartChange(date) {
        this.setState({
            startValue: date,
        });
    },
    onEndChange(date) {
        this.setState({
            endValue: date,
        });
        if (this.props.onSearch) {
            this.props.onSearch(this.state.startValue,date);
        }
    },
    handleStartToggle({ open }){
        if (!open) {
            this.setState({ endOpen: true });
        }
    },
    handleEndToggle({ open }) {
        this.setState({ endOpen: open });
    },
        render() {
            return (
                <div className="datepicker">
                    <DatePicker
                        disabledDate={this.disabledStartDate}
                        value={this.state.startValue}
                        placeholder="开始日期"
                        onChange={this.onStartChange}
                        toggleOpen={this.handleStartToggle}
                        style={{
                            width:110,
                            height:30,
                        }}
                    />
                    <span style={{display:'inline-block',width:8,height:1,background:'#999',marginRight:6,marginLeft:6,marginBottom:3}}></span>
                    <DatePicker
                        disabledDate={this.disabledEndDate}
                        value={this.state.endValue}
                        placeholder="结束日期"
                        onChange={this.onEndChange}
                        open={this.state.endOpen}
                        toggleOpen={this.handleEndToggle}
                        style={{
                            width:110,
                            height:30,
                        }}
                    />
                </div>
            );
        },
    });


export default DatePickerBox;