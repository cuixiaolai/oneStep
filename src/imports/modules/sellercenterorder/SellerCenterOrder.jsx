import React, { Component } from 'react';
import { Select, Dropdown, DatePicker, Modal, Button ,Input ,Menu, Icon,Form ,Tabs, Col ,Checkbox ,Pagination,Cascader} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
// import SearchCommodity from '../order/SearchCommodity.jsx';
import DialogPayment from '../order/DialogPayment.jsx';
/*卖家中心-库存管理*/



export default class SellerCenterOrder extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            data: [{
                key: 1,
                orderNumber:'1255561197455',
                orderDate:'2016-08-13',
                productDetails: {
                    url: 'commodity_2.png',
                    title: 'LPY430L',
                    manufacturer: '北京',
                    mode: '托盘无拆分',
                    content: '价格实惠性能好，七天包换全新无拆封匹配任何型号(为详细描述)',
                }
                ,
                amount: 2,
                unitPrice: 28.52,
                commodityOperator: '退货/退款',
                totalPrice: 57,
                deliveryStatus:['waitingbuyerspay','waitorder','waitgoods','orderdetails',],
                operator:['payment','cancelorder','reminderdelivery','confirmreceipt','extendedreceipt','evaluate'],
                ordernumber_:'6688004444455',
                orderamount:'228',
            }],
            downflag:false,
            checked:[false],
            all:false,
        });

    }
    showSearch() {
        let temp = [];
        temp[0] = (

            <div className="searchcommodity">
                <SearchCommodity placeholder= {I18n.t('order.placeholder')}
                                 onSearch={value => console.log(value)} style={{ width: 240,height: 40 }}
                />
            </div>
        )
        return temp;
    }
    showDetails(){
        let temp = [];
        temp[0]=(
            <div className="clearFloat">
                <div className="productLeft">
                    <img src={(config.theme_path+this.state.data[0].productDetails.url)} alt="图片"/>
                </div>
                <div className="productRight">

                    <p>厂家：{this.state.data[0].productDetails.manufacturer}</p>
                    <p>封装：{this.state.data[0].productDetails.mode}</p>
                    <p className="p_">{this.state.data[0].productDetails.content}</p>
                </div>
            </div>)
        return temp;
    }
    showAmount(){
        let temp = [];
        temp[0]=(
            <div >
                ×{this.state.data[0].amount}
            </div>)
        return temp;
    }
    showUnitPrice(){
        let temp = [];
        temp[0]=(
            <div >
                ￥{this.state.data[0].unitPrice}/个
            </div>)
        return temp;
    }
    showCommodityOperator(){
        let temp = [];
        temp[0]=(
            <div >
                {this.state.data[0].commodityOperator}
            </div>)
        return temp;
    }
    //交货状态
    showDeliveryStatus(){
        let temp = [];
        // for(let i = 0; i < this.state.data[0].deliveryStatus.length; i++){
        for(let i = 0; i < 2; i++){
            if(this.state.data[0].deliveryStatus[i] =='waitingbuyerspay' ){
                temp[i]=(
                    <span >{I18n.t('shoppingCart.dialog.waitingbuyerspay')} </span>
                )
            }else if(this.state.data[0].deliveryStatus[i] == 'waitorder'){
                temp[i]=(
                    <span >{I18n.t('order.waitorder')} </span>
                )
            }

        }

        return temp;
    }
    //交易操作

    showTransactionOperator(){
        let temp = [];
        for(let i = 0; i < 2; i++){
            if(this.state.data[0].operator[i]== 'payment'){
                temp[i]=(
                    <span className="operator_" >
                   <DialogPayment  className="deletePadding left"  words={I18n.t('shoppingCart.dialog.confirmcancelorder')}  content={I18n.t('shoppingCart.dialog.payment')} />
                </span>
                )
            }else if(this.state.data[0].operator[i]=='cancelorder'){
                temp[i]=(
                    <span className="operator_" >
                        {I18n.t('shoppingCart.dialog.cancelOrder')}
                    </span>

                )}
        }
        return temp;
    }
    //
    showTotal(){
        let temp = [];
        temp[0]=(
            <div >
                ￥<span className="total">{this.state.data[0].amount*this.state.data[0].unitPrice}</span>
            </div>)
        return temp;
    }
    clickDown(){
        console.log('clickDown');
        if(this.state.downflag == false){
            this.setState({downflag: true});
        }else{
            this.setState({downflag: false});
        }
    }
    showTop(){
        let temp = [];
        const menu = (
            <Menu>
                <Menu.Item key="0">
                    <span>最近一周</span>
                </Menu.Item>
                <Menu.Item key="1">
                    <span>最近一个月</span>
                </Menu.Item>
                <Menu.Divider />
                <Menu.Item key="3">
                    <span>最近三个月</span>
                </Menu.Item>
                <Menu.Item key="4">
                    <span>最近半年</span>
                </Menu.Item>
            </Menu>
        );

        temp[0] = (
            <div className="showTop">
                <ul className="top">
                    <li style={{width:80}} className="timeframe">
                        <Dropdown overlay={menu} trigger={['click']}>
                            <a className="ant-dropdown-link" href="#" onClick={this.clickDown.bind(this)}>
                                最近一周 <Icon type="down" style={this.state.downflag ? {transform:'scale(0.667) rotate(180deg)'} : {transform:'scale(0.667) rotate(0deg)'}} />
                            </a>
                        </Dropdown>
                    </li>
                    <li style={{width:240}}>
                        {I18n.t('order.commodity')}
                    </li>
                    <li style={{width:100}}>
                        {I18n.t('order.amount')}
                    </li>
                    <li style={{width:110}}>
                        {I18n.t('order.unitprice')}
                    </li>
                    <li style={{width:110}}>
                        {I18n.t('order.commodityoperator')}
                    </li>
                    <li style={{width:110}}>
                        {I18n.t('order.total')}
                    </li>
                    <li style={{width:120}}>
                        {I18n.t('order.deliverystatus')}
                    </li>
                    <li style={{width:115}}>
                        {I18n.t('order.transactionoperator')}
                    </li>
                </ul>
            </div>

        )
        return temp;
    }
    clickChecked(i){
        if(this.state.checked[i] == true){
            this.state.checked[i] = false;

            this.setState({all:false});
        }else{
            this.state.checked[i] = true;
        }
        this.setState({checked:this.state.checked});
    }
    showTableTop(){
        let temp = [];
        for(let i = 0; i< 1; i++){
            temp[i] = (
                <div className="tableTop">
                    <Checkbox  className="checkbox" checked={this.state.checked[i]} onClick={this.clickChecked.bind(this,i)}>{I18n.t('order.manufacturer')}</Checkbox>
                    <span className="sign">{I18n.t('buyer.Manufacturer')}</span>
                    <img src={(config.theme_path+'talk.png')} className="talkImg" alt="图片"/>
                    <p>{I18n.t('order.ordernumber')}<span>{this.state.data[0].orderNumber}</span></p>
                    <p className="orderdate">{this.state.data[0].orderDate}</p>
                </div>
            )
        }

        return temp;
    }
    showTr(i,number){
        let temp = [];

        if(number>1){
            if(i == 0){
                temp[0] = (
                    <tr>
                        <td style={{width:323}}>
                            {this.showDetails()}
                        </td>
                        <td style={{width:100}}>
                            {this.showAmount()}
                        </td>
                        <td style={{width:110}}>
                            {this.showUnitPrice()}
                        </td>
                        <td style={{width:110}}>
                            {this.showCommodityOperator()}
                        </td>
                        <td style={{width:110,borderBottom:0}} rowSpan={number}>
                            {this.showTotal()}
                        </td>
                        <td style={{width:120,borderBottom:0}} className="status" rowSpan={number}>
                            {this.showDeliveryStatus()}
                        </td>
                        <td style={{width:115,borderBottom:0}} rowSpan={number}>
                            {this.showTransactionOperator()}
                        </td>

                    </tr>)
            }else{
                temp[0] = (
                    <tr>
                        <td style={{width:323}}>
                            {this.showDetails()}
                        </td>
                        <td style={{width:100}}>
                            {this.showAmount()}
                        </td>
                        <td style={{width:110}}>
                            {this.showUnitPrice()}
                        </td>
                        <td style={{width:110}}>
                            {this.showCommodityOperator()}
                        </td>
                        <td style={{width:110}}> </td>
                        <td style={{width:120}}> </td>
                        <td style={{width:115}}> </td>

                    </tr>
                )
            }
        }else{
            temp[0] = (
                <tr>
                    <td style={{width:323}}>
                        {this.showDetails()}
                    </td>
                    <td style={{width:100}}>
                        {this.showAmount()}
                    </td>
                    <td style={{width:110}}>
                        {this.showUnitPrice()}
                    </td>
                    <td style={{width:110}}>
                        {this.showCommodityOperator()}
                    </td>
                    <td style={{width:110}} >
                        {this.showTotal()}
                    </td>
                    <td style={{width:120}} className="status" >
                        {this.showDeliveryStatus()}
                    </td>
                    <td style={{width:115}} >
                        {this.showTransactionOperator()}
                    </td>

                </tr>)
        }



        return temp;
    }
    showTable(number){
        let temp = [];
        let table = [];

        for(let i = 0; i < number ; i++){
            temp[i]=(
                <div className="tabletr">
                    { this.showTr(i,number)}
                </div>

            )
        }

        table[0] = (
            <div className="table_">
                {this.showTableTop()}
                <table  >
                    {temp}
                </table>
            </div>

        )

        return table;
    }
    showTableAll(){
        let temp = [];

        for(let i = 0; i < 5; i++){
            if(i == 2){
                temp[i]=(
                    this.showTable(2)
                )
            }else{
                temp[i]=(
                    this.showTable(1)
                )

            }


        }
        return temp;
    }
    clickAll(){
        if(this.state.all == true){
            this.state.all = false;
            for(let i = 0; i < this.state.checked.length; i++){
                this.state.checked[i] = false;
            }

        }else{
            this.state.all = true;
            for(let i = 0; i < this.state.checked.length; i++){
                this.state.checked[i] = true;
            }

        }

        this.setState({all:this.state.all});
        this.setState({checked:this.state.checked});
    }
    ShowPagination(){
        return(
            <Pagination showQuickJumper defaultCurrent={1} total={5} />
        )
    }

    render() {


        return (
            <div>
                {this.showSearch()}
                {this.showTop()}

                <div className="select">
                    <Checkbox  className="all" checked={this.state.all} onClick={this.clickAll.bind(this)}>{I18n.t('PersonalCenter.manageStock.select')}</Checkbox>
                    <span className="span1">{I18n.t('order.consolidatedPayment')}</span>
                    <span className="span1">{I18n.t('order.confirmReceipt')}</span>
                </div>
                {this.showTableAll()}
                <div className="ShowPaginationBox">
                    {this.ShowPagination()}
                    <div className="clear"></div>
                </div>


            </div>
        );




    }
}

