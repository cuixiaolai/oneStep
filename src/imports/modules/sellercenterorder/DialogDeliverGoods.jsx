import { Modal, Button,Radio ,Cascader,Input} from 'antd';
import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
{/*发货弹窗*/}

const RadioGroup = Radio.Group;

export default class DialogDeliverGoods extends Component{
    constructor(props) {
        super(props);
        this.state = ({
          value:1,
            loading: false,
            visible: false,
            flaglogistics:true,
            flagdelivery:false,
            logistics_company:'',
            logistics_nomber:'',
            delivery_name:'',
            delivery_phone:''
        });
    }
    componentDidMount(){
        console.log(this.props.key);
    }
    onChange(e) {
        console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });
        this.setState({flaglogistics:!this.state.flaglogistics});
        this.setState({flagdelivery:!this.state.flagdelivery});
    }
    changeCompany(value,selectedOptions){
        this.setState({
            logistics_company: value[0],
        });
    }
    showModal() {
        this.setState({
            visible: true,
        });
    }
    handleOk(){
        let This = this;
        {/*弹窗确认事件*/}
        Meteor.call('order.deliverGoods', this.props.order_id, function(err) {
            if (err) {

            } else {
                Meteor.call('message.createMessage', Meteor.userId(), this.props.customer_id, 3, this.props.id.toString());
                This.props.close();
                console.log('good');
            }
        }.bind(this));
        this.setState({
            visible: false,
        });
    }
    handleCancel() {
        this.setState({ visible: false });
    }

    render() {
        console.log(this.state.value);
        const styles = {
            display: 'block',
            height: '30px',
            lineHeight: '30px',
            textAlign:'left',
        };
        const optionslogistics = [{
            value: 'yunda',
            label: '韵达快递',
        }, {
            value: 'yuantong',
            label: '圆通速递',
        }, {
            value: 'zhongtong',
            label: '中通快递',
        }, {
            value: 'baishi',
            label: '百世快递 ',
        }, {
            value: 'youzheng',
            label: '邮政包裹 ',
        }, {
            value: 'tiantian',
            label: '天天快递',
        }, {
            value: 'shunfeng',
            label: '顺丰速递',
        }, {
            value: 'ems',
            label: 'EMS',
        }];
        return (
            <div className='PopupBox' style={this.props.flag ? {display:'block'} : {display:'none'}} >
                <div className="PopupBoxInner">
                    <p style={{textAlign:'right'}}><span onClick={this.props.close}>×</span></p>
                    <div className="delivergoodsDialog"  style={{paddingTop:0,fontSize:14}}>
                        <RadioGroup onChange={this.onChange.bind(this)} value={this.state.value}>
                            <Radio style={styles} key="a" value={1}>
                                {I18n.t('sellercenterorder.logistics')}
                                <img src={(config.theme_path+'down.png')} className="downImg" alt="图片" style={this.state.flaglogistics ? {transform:'rotate(180deg)'} : {transform:'rotate(0deg)'}}/>
                            </Radio>
                            <div className="logistics" style={this.state.flaglogistics ? {} : {display:'none'}}>
                                <p className="selectlogistics">
                                    <span>{I18n.t('sellercenterorder.selectlogistics')}</span>
                                    <Cascader options={optionslogistics} onChange={this.changeCompany.bind(this)}  />
                                </p>
                                <p >
                                    <span>{I18n.t('sellercenterorder.logisticsnumber')}</span>
                                    <Input/>
                                </p>
                            </div>
                            <Radio style={styles} key="b" value={2}>
                                {I18n.t('sellercenterorder.delivery')}
                                <img src={(config.theme_path+'down.png')} className="downImg" alt="图片" style={this.state.flagdelivery ? {transform:'rotate(180deg)'} : {transform:'rotate(0deg)'}}/>
                            </Radio>
                            <div className="delivery" style={this.state.flagdelivery ? {} : {display:'none'}}>
                                <p className="deliveryman">
                                    <span>{I18n.t('sellercenterorder.deliveryman')}</span>
                                    <Input/>
                                </p>
                                <p>
                                    <span>{I18n.t('sellercenterorder.contactnumber')}</span>
                                    <Input/>
                                </p>
                            </div>
                        </RadioGroup>
                    </div>
                    <p style={{marginTop:16}}>
                        <Button  onClick={this.handleOk.bind(this)} type="primary" style={{marginRight:'20px'}}>{I18n.t('buyercenter.sure')}</Button><Button type="ghost"  onClick={this.props.close}>{I18n.t('PersonalCenter.manageStock.cancel')}</Button>
                    </p>
                </div>
            </div>
        );
    }
}