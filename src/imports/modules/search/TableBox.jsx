import React, { Component } from 'react';
import { Link } from 'react-router';
import { Table,Button,Icon} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import MyTable from '../MyTable.jsx';


export default class TableBox extends Component{
    ShowBtn(){
        if(this.props.dataSource.length != 0){
            return(
                <div style={{marginTop:10,paddingRight:5}}>
                    <Button type="ghost" className="right" onClick={this.props.addCount}>
                        {I18n.t('Search.more')}<Icon type="right" />
                    </Button>
                    <Button type="ghost" className="right" style={{marginRight:20}} onClick={this.props.resetCount}>
                        {I18n.t('Search.up')}<Icon type="up" />
                    </Button>
                    <div className="clear"></div>
                </div>
            )
        }
    }
    render(){
        return(
            <div className="Search-TableBox">
                <p>{this.props.name}</p>
                <MyTable columns={this.props.columns}
                         date={this.props.dataSource} openToPayment={this.props.openToPayment} message={this.props.message} addToCart={this.props.addToCart} stocktype={this.props.stocktype}/>
                {this.ShowBtn()}
            </div>
        )
    }
}