import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Badge,Icon,Input, Button, Select ,Checkbox,Table,Thead } from 'antd';

const Option = Select.Option;
import Stocklocation from '../Stocklocation.jsx';
import SelectShop from '../SelectShop.jsx';
import Products from '../Product.jsx';

import { createStore } from 'redux'
import Reducer from '../../../js/reducers/index.js'
import { connect } from 'react-redux';

 class SearchTop extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            search:[],
            setaddress:I18n.t('home.address'),
            setshop:I18n.t('PersonalCenter.manageStock.manufacturer'),
            selectedOptions:I18n.t('home.product'),
            fromOtherPage:true,
        });
    }
    componentWillReceiveProps(newProps) {
        if(newProps.category1 != this.props.category1 && newProps.from == 'topbox' ) { // determined by pointer changing
            if(newProps.category1.length==0) {
                this.CloseCategory();
            }else {
                this.handleProvinceChange(newProps.category1,newProps.category1_options);
            }
        }
        if((newProps.from == 'homebox' || newProps.from == 'topbox') && this.state.fromOtherPage){
            this.setState({fromOtherPage:false});
            this.handleProvinceChange(newProps.category1,newProps.category1_options);
        }
    }
    setAddress(text) {
        this.setState({ setaddress: text });
        this.props.setAddress(text);
    }
    setShop(text){
        this.setState({ setshop:text });
        this.props.setShop(text);
    }
    handleProvinceChange(value,selectedOptions) {
        if(value == ''){
            this.setState({search:[],selectedOptions:I18n.t('home.product')});
        }else {
            console.log(value,selectedOptions);
            this.state.search = value;
            this.setState({search:this.state.search,selectedOptions:selectedOptions[0].label});
            console.log(this.state.search);
            this.props.setProduct(value);
        }
    };
    ShowCategory(){
        if(this.state.selectedOptions != I18n.t('home.product')){
            return(
                <div className="left">
                    <Icon  className="left" type="right" />
                    <div className="left">
                        {I18n.t('Search.category')}
                        <span style={{color:'#0c5aa2'}}>{this.state.selectedOptions}</span>
                        <Icon onClick={this.CloseCategory.bind(this)} type="plus" className="right" style={{transform:'rotate(45deg)',cursor:'pointer'}}/>
                        <div className="clear"></div>
                    </div>
                    <p className="clear"></p>
                </div>
            )
        }
    }
    CloseCategory(){
        this.setState({search:[],selectedOptions:I18n.t('home.product')});
        this.props.setProduct([]);
    }
    ShowManufacturer(){
        if(this.state.setshop != I18n.t('PersonalCenter.manageStock.manufacturer')){
            return(
                <div className="left">
                    <Icon  className="left" type="right" />
                    <div className="left">
                        {I18n.t('PersonalCenter.manageStock.manufacturer')}
                        <span style={{color:'#0c5aa2'}}>{this.state.setshop}</span>
                        <Icon onClick={this.CloseManufacturer.bind(this)} type="plus" className="right" style={{transform:'rotate(45deg)',cursor:'pointer'}}/>
                        <div className="clear"></div>
                    </div>
                    <p className="clear"></p>
                </div>
            )
        }
    }
    CloseManufacturer(){
        this.setState({setshop:I18n.t('PersonalCenter.manageStock.manufacturer')});
        this.props.setShop(I18n.t('PersonalCenter.manageStock.manufacturer'));
    }
    ShowGround(){
        if(this.state.setaddress != I18n.t('home.address')){
            return(
                <div className="left">
                    <Icon  className="left" type="right" />
                    <div className="left">
                        {I18n.t('Search.ground')}
                        <span style={{color:'#0c5aa2'}}>{this.state.setaddress}</span>
                        <Icon  onClick={this.CloseGround.bind(this)}type="plus" className="right" style={{transform:'rotate(45deg)',cursor:'pointer'}}/>
                        <div className="clear"></div>
                    </div>
                    <p className="clear"></p>
                </div>
            )
        }
    }
    CloseGround(){
        this.setState({setaddress:I18n.t('home.address')});
        this.props.setAddress(I18n.t('home.address'));
    }
    render(){
        const columns = [{
            title: '价格',
            dataIndex: 'price',
            // 指定确定筛选的条件函数
            // 这里是名字中第一个字是 value
            onFilter: (value, record) => record.name.indexOf(value) === 0,
            sorter: (a, b) => a.name.length - b.name.length,
        }, {
            title: '出货量',
            dataIndex: 'shipment',
            sorter: (a, b) => a.age - b.age,
        }, {
            title: '信誉',
            dataIndex: 'reputation',
            filterMultiple: false,
            onFilter: (value, record) => record.address.indexOf(value) === 0,
            sorter: (a, b) => a.address.length - b.address.length,
        }];
        return(
            <div>
                <div className="SearchTop-name">
                    <span className="left">{I18n.t('Search.all')}</span>
                    {this.ShowCategory()}
                    {this.ShowManufacturer()}
                    {this.ShowGround()}
                    <div className="left">
                        <Icon  className="left" type="right" />
                        {decodeURIComponent(window.location.pathname.substr(8))}
                        <p className="clear"></p>
                    </div>
                    <p className="clear"></p>
                </div>
                <div className="SearchTop-box">
                    <p>
                        <span>{I18n.t('Search.screen')}</span>
                        <span>
                            <Products value={this.state.search} onChange={this.handleProvinceChange.bind(this)} style={{width:110,height:32,marginLeft:50,float:'left'}} />
                            <SelectShop Closecircle={this.CloseManufacturer.bind(this)}  func={this.setShop.bind(this)} style={{width:110,height:32,marginLeft:40,textAlign:'left',display:'inline-block',position:'relative'}} value={this.state.setshop} />
                            <Stocklocation CloseGround={this.CloseGround.bind(this)} func={this.setAddress.bind(this)} style={{width:110,height:32,marginLeft:40,textAlign:'left',display:'inline-block',position:'relative'}} value={this.state.setaddress} />
                            <div className="clear"></div>
                        </span>
                    </p>
                    <p>
                        <span>{I18n.t('Search.searchmore')}</span>
                        <span>
                            <Checkbox onChange={this.props.change} value="platform">{I18n.t('Search.platform')}</Checkbox>
                            <Checkbox onChange={this.props.change} value="shop">{I18n.t('Search.shop')}</Checkbox>
                            <Checkbox onChange={this.props.change} value="double">{I18n.t('Search.double')}</Checkbox>
                            <Checkbox onChange={this.props.change} value="Return">{I18n.t('Search.Return')}</Checkbox>
                            <Checkbox onChange={this.props.change} value="Warranty">{I18n.t('Search.Warranty')}</Checkbox>
                            <Checkbox onChange={this.props.change} value="data">{I18n.t('Search.data')}</Checkbox>
                            <Checkbox onChange={this.props.change} value="COC">COC</Checkbox>
                            <Checkbox onChange={this.props.change} value="DPA">DPA</Checkbox>
                            <Checkbox onChange={this.props.change} value="ROHS">ROHS</Checkbox>
                        </span>
                    </p>
                    <p>
                        <span>{I18n.t('Search.sort')}</span>
                        <span>
                           <Table columns={columns} onChange={this.props.changes}></Table>
                        </span>
                    </p>
                </div>
            </div>
        )
    }
}

export default connect((state) => {
    console.log("state")
    console.log(state)
    return {
        category1: state.Reducer.category[0],
        category1_options: state.Reducer.category[1],
        from:state.Reducer.category[2]
    }
})(SearchTop);














