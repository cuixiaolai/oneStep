import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Button,Icon,Form,Input,message,Radio,InputNumber,Checkbox,Select  } from 'antd';
const Option = Select.Option;
import config from '../../config.json';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const createForm = Form.create;
// import { createContainer } from 'meteor/react-meteor-data';
// import { NewProduct } from '../../api/Product.js';
import SelectShop from '../SelectShop.jsx';

let SearchBox = React.createClass ({
    getInitialState() {
        console.log(this.props.data);
        return{
            currency:1,
            value:1,
            numbervalue:false,
            flag:false,
            total_price:'',
            cur: 1,
            setshop:I18n.t('PersonalCenter.manageStock.manufacturer'),
        }
    },
    componentDidMount(){
        let arr = [];
        if(typeof this.props.data != null){
            arr.push(this.props.data.renmingbi.length);
            arr.push(this.props.data.renmingbi.length);
            arr.push(this.props.data.MOQ);
            let currency = arr[0] > 0? 1:2;
            let value = arr[1]> 0? 0:1;
            if (this.props.data.quantity != null) {
                this.setState({currency:currency,value:value,numbervalue:this.props.data.quantity});
            }
            else {
                this.setState({currency:currency,value:value,numbervalue:this.props.data.MOQ});
            }
        }
    },
    onChange(e) {
        console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });
    },
    handleSubmit(e){
        e.preventDefault();
        this.props.form.validateFields((errors, values) =>{
            if(errors){
                console.log('error')
            }else{
                message.success('发布需求成功！');
                console.log(values);
                console.log(this.state.cur);
                Meteor.call('wishlist.addItem', values.company, values.type, parseInt(values.num), this.state.cur, parseFloat(values.price), values.name, values.Release, values.ReleaseTel);
                this.props.click();
            }
        })
    },
    AddShopCard(){
        this.props.Card(this.state.numbervalue,this.state.value)
    },
    onChangeNumber(value){
        let tmp = value-this.props.data.MOQ;
        let result = Math.ceil(tmp / this.props.data.inc_number)*this.props.data.inc_number+this.props.data.MOQ;
        this.setState({numbervalue:result});
    },
    Addnumber(){
        if(this.state.numbervalue+this.props.data.inc_number <= this.props.data.kucun){
            let value = this.state.numbervalue + this.props.data.inc_number;
            this.setState({numbervalue:value});
        }
    },
    handleChange(value) {
        if (value == 'rmb') {
            this.setState({cur: 1});
        }
        else {
            this.setState({cur: 2});
        }
        console.log(`selected ${value}`);
    },
    Reducenumber(){
        if(this.state.numbervalue-this.props.data.inc_number >= this.props.data.MOQ){
            let value = this.state.numbervalue - this.props.data.inc_number;
            this.setState({numbervalue:value});
        }
    },
    userReleaseTelProps(rule, value, callback){
        let react = /^\d{6,}$/;
        if(!react.test(value)){
            callback(I18n.t('buyer.error'));
        }else{
            callback();
        }
    },
    usernumProps(rule, value, callback){
        let react = /^\d{1,}$/;
        if(!react.test(value)){
            callback(I18n.t('buyer.error'));
        }else{
            callback();
        }
    },
    userPriceProps(rule, value, callback){
        let react = /^\d{1,}$/;
        if(!react.test(value)){
            callback(I18n.t('buyer.error'));
        }else{
            callback();
        }
    },
    CloseManufacturer(){
        this.setState({setshop:I18n.t('PersonalCenter.manageStock.manufacturer')});
    },
    setShop(text){
        this.setState({ setshop:text });
    },
    ShowMessage(price,total_price){
        const { getFieldProps ,getFieldError, isFieldValidating } = this.props.form;
        const typeProps = getFieldProps('type', {
            rules: [
                { required: true,min:1,message:I18n.t('buyer.error')},
            ],
        });
        const numProps = getFieldProps('num', {
            rules: [
                { validator: this.usernumProps }
            ],
        });
        const PriceProps = getFieldProps('price', {
            rules: [
                { validator: this.userPriceProps }
            ],
        });
        const NameProps = getFieldProps('name', {
            rules: [
                { required: true,min:1,message:I18n.t('buyer.error')},
            ],
        });
        const ReleaseProps = getFieldProps('Release', {
            rules: [
                { required: true,min:1,message:I18n.t('buyer.error')},
            ],
        });
        const ReleaseTelProps = getFieldProps('ReleaseTel', {
            rules: [
                { validator: this.userReleaseTelProps }
            ],
        });
        if(this.props.flag == false){
            return(
                <div className="SearchBoxInner" style={{width:600}}>
                    <p>
                        <span className="left">{this.props.word}</span>
                        <span className="right">
                            <img onClick={this.props.click} src={( config.theme_path + 'close.png')} alt=""/>
                        </span>
                        <div className="clear"></div>
                    </p>
                    <div className="ShoppingCart">
                        <p>
                            <span className="left">{I18n.t('ShoppingCart.merchant')}</span>
                            <span className="left">：</span>
                            <span className="left">
                                {this.props.data.name}
                            </span>
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left distribution">{I18n.t('ShoppingCart.type')}</span>
                            <span className="left">：</span>
                            <span className="left" style={{color:'#ed7020'}}>{this.props.data.money}</span>
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left distribution">{I18n.t('ShoppingCart.Manufacturer')}</span>
                            <span className="left">：</span>
                            <span className="left">{this.props.data.address}</span>
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left distribution">{I18n.t('ShoppingCart.Stock')}</span>
                            <span className="left">：</span>
                            <span className="left"  style={{color:'#ed7020'}}>{this.props.data.kucun}</span>
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left">{I18n.t('ShoppingCart.minPacking')}</span>
                            <span className="left">：</span>
                            <span className="left">{this.props.data.standard_packing}</span>
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left">{I18n.t('ShoppingCart.addNum')}</span>
                            <span className="left">：</span>
                            <span className="left">{this.props.data.inc_number}</span>
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left distributionthree">{I18n.t('ShoppingCart.quantitative')}</span>
                            <span className="left">：</span>
                            <span className="left">{this.props.data.MOQ}</span>
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left distributionthree">{I18n.t('ShoppingCart.delivery')}</span>
                            <span className="left">：</span>
                            <span className="left">
                                {this.state.currency == 1 ?
                                    <RadioGroup onChange={this.onChange} value={this.state.value}>
                                        <Radio key="c" value={0}>大陆-大陆</Radio>
                                    </RadioGroup>:
                                    <RadioGroup onChange={this.onChange} value={this.state.value}>
                                        <Radio key="a" value={1}>海外-香港/大陆保税区</Radio>
                                        <Radio key="b" value={2}>海外/海关外-大陆</Radio>
                                    </RadioGroup>
                                }
                            </span>
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left distribution">{I18n.t('ShoppingCart.Delivery')}</span>
                            <span className="left">：</span>
                            {this.state.currency == 1 > 0 ?
                                <span className="left"><p style = {{marginBottom:10}}>国内(一周左右)</p></span>
                                :<span className="left"><p style = {{marginBottom:10}}>香港(两周左右)</p><p>国外(两周左右)</p></span>
                            }
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left distribution">{I18n.t('ShoppingCart.num')}</span>
                            <span className="left">：</span>
                            <span className="left">
                                <div className="left" style={{marginLeft:10,marginRight:60}}>
                                    <span onClick={this.Reducenumber}  style={{cursor:'pointer'}}>-</span>
                                    <InputNumber  min={this.props.data.MOQ} max={this.props.data.kucun} step={this.props.data.inc_number} value={this.state.numbervalue} onChange={this.onChangeNumber} />
                                    <span onClick={this.Addnumber} style={{cursor:'pointer'}}>+</span>
                                </div>
                                <div className="left">
                                    <span>{this.state.currency == 1 ? '￥': '$'}{price}</span>{I18n.t('ShoppingCart.Price')} = <span>{this.state.currency == 1 ? '￥': '$'}{total_price}</span>{I18n.t('ShoppingCart.Total')}
                                </div>
                                <div className="clear"></div>
                            </span>
                            <div className="clear"></div>
                        </p>
                        <p>
                            <Button onClick={this.AddShopCard.bind(this)} type="primary">{this.props.word}</Button>
                        </p>
                    </div>
                </div>
                )
        }else if(this.props.flag == true){
            return(
                <div className="SearchBoxInner">
                    <p>
                        <span className="left">{I18n.t('Release.requirements')}</span>
                        <span className="right">
                            <img onClick={this.props.click} src={( config.theme_path + 'close.png')} alt=""/>
                        </span>
                        <div className="clear"></div>
                    </p>
                    <p>
                        <div className="left">
                            <img src={(config.theme_path + 'origin03.png')} alt=""/>
                        </div>
                        <div  className="left">
                            {I18n.t('Release.Backstage')}
                        </div>
                        <span className="clear"></span>
                    </p>
                    <Form inline>
                        <FormItem
                            label={I18n.t('PersonalCenter.manageStock.manufacturer')}
                        >
                            <SelectShop Closecircle={this.CloseManufacturer}  func={this.setShop} style={{width:300,height:40,position:'relative'}}  value={this.state.setshop} />
                        </FormItem>
                        <FormItem
                            label={I18n.t('PersonalCenter.manageStock.model')}
                        >
                            <Input  maxLength={50} {...typeProps} />
                        </FormItem>
                        <FormItem
                            label={I18n.t('buyerForm.NUM')}
                        >
                            <Input maxLength={8} {...numProps} />
                        </FormItem>
                        <FormItem
                            label={I18n.t('buyerForm.targetprice')}
                        >
                            <Select size="large" defaultValue="rmb" style={{ width: 80, height:40, marginRight:20 }} onChange={this.handleChange}>
                                <Option value="rmb">{I18n.t('buyerForm.ren')}</Option>
                                <Option value="dollar">{I18n.t('buyerForm.dollar')}</Option>
                            </Select>
                            <Input maxLength={8}  style={{width:200}} {...PriceProps}/>
                        </FormItem>
                        <FormItem
                            label={I18n.t('Register.com.name')}
                        >
                            <Input maxLength={50}  {...NameProps}/>
                        </FormItem>
                        <FormItem
                            label={I18n.t('Search.Release')}
                        >
                            <Input maxLength={12} {...ReleaseProps} />
                        </FormItem>
                        <FormItem
                            label={I18n.t('Search.Releasetel')}
                        >
                            <Input maxLength={20} {...ReleaseTelProps} />
                        </FormItem>
                        <FormItem>
                            <Button onClick={this.handleSubmit} type="primary">{I18n.t('Addresspage.sure')}</Button>
                        </FormItem>
                    </Form>
                </div>
            )
        }
    },
    render(){
        let price = '';
        let total_price = '';
        if(typeof this.props.data != 'undefined') {
            let grad = 0;
            let flag = true;
            for (let i = 0; i < this.props.data.tidu.length - 1 && flag; i++) {
                if (this.state.numbervalue < this.props.data.tidu[i + 1]) {
                    grad = i;
                    flag = false;
                }
            }
            if (flag == true) {
                grad = this.props.data.tidu.length - 1;
            }
            if (this.state.currency == 1) {
                price = this.props.data.renmingbi[grad];
                total_price = Math.round((this.props.data.renmingbi[grad] * this.state.numbervalue)*100)/100;
            } else {
                price = this.props.data.meiyuan[grad]
                total_price = Math.round((this.props.data.meiyuan[grad] * this.state.numbervalue)*100)/100;
            }
        }
        return(
            <div className="SearchBox">
                {this.ShowMessage(price,total_price)}
            </div>
        )
    }
})
SearchBox = createForm()(SearchBox);
export default SearchBox;