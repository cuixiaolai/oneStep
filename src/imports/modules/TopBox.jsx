import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor'
// import { Cart } from '../api/Cart.js';
// import { createContainer } from 'meteor/react-meteor-data';
import TopBoxView from './TopBoxView.jsx';

import config from '../config.json';

export default class TopBox extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <TopBoxView cartInfo={this.props.cartInfo} />
        )
    }
};
TopBox.propTypes = {
};
// export default createContainer(() => {
//     Meteor.subscribe('product.part_number');
//     Meteor.subscribe('cart');
//     return {
//         cartInfo: Cart.findOne({_id: Meteor.userId()}, {fields: {'size': 1}}),
//     };
// }, TopBox);
