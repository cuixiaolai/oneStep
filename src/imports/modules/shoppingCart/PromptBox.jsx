import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { createContainer } from 'meteor/react-meteor-data';
import { Tabs,Table,Checkbox,InputNumber } from 'antd';
import config from '../../config.json';

{/*购物车*/}



export default class PromptBox extends Component {

    constructor(props) {
        super(props);
        this.state = ({
            name:'promptBox',
            flag:false,

        });

    }
    clickClose(){
        console.log('close');
        this.setState({flag:true});

    }
    render(){


        return(

            <div className={this.state.name} style={this.state.flag ? {display:'none'} : {}}>
                <div className="promptImg" onClick={this.clickClose.bind(this)} ><img src={(config.theme_path+'close.png')} alt="图片"/></div>
               <p>{I18n.t('shoppingCart.content.prompt')}</p>
            </div>
        );
    }
}