import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router'
import { createContainer } from 'meteor/react-meteor-data';
import { Tabs,Table,Checkbox,InputNumber,Button,Modal } from 'antd';
import config from '../../config.json';
const confirm = Modal.confirm;
{/*原厂商*/}

export default class ShoppingCartTr extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            product_id: '',
            img: '',
            detail: '',
            manufacturer: '',
            package: '',
            num: [],
            price: [],
            quantity: 0,
            operate: '',
            currency: 0,
            totalPrice: 0,
            valid: true,
            address:1,
            value:1,
            inc_number:1,
            MOQ:1,
            stock_quantity:1,
            id:'',
        });
    }
    componentDidMount(){
        Meteor.call('stock.findOneStock', this.props.stockId, function(err, stockdata) {
            if (err != null) {
                console.log(err);
            }
            else {
                Meteor.call('product.findDetailAndImg', stockdata.product_id, function(err, productdata) {
                    if (err != null) {
                        console.log(err);
                    }
                    else {
                        if (stockdata.putaway == false || stockdata.type != this.props.type) {
                            this.setState({valid: false});
                            this.props.valid(this.props.flag);
                        }
                        let packageStr = '';
                        for (let i = 0; i < stockdata.package.length; ++ i) {
                            if (packageStr == '') {
                                packageStr = stockdata.package[i].package;
                            }
                            else {
                                packageStr = packageStr + ', ' + stockdata.package[i].package;
                            }
                        }
                        let min = [];
                        let price = [];
                        for (let i = 0; i < stockdata.price.length; ++ i) {
                            min.push(stockdata.price[i].min);
                            price.push(stockdata.price[i].price);
                        }
                        let i = 0;
                        for (; i < min.length; ++ i) {
                            if (this.props.quantity < min[i]) {
                                break;
                            }
                        }
                        console.log('currency', stockdata.currency);
                        let total = price[i-1] * this.props.quantity;
                        this.props.price(this.props.flag, total);
                        console.log(stockdata,productdata);
                        this.setState({
                            product_id: stockdata.product_id,
                            img: productdata.img,
                            detail: productdata.detail,
                            manufacturer: stockdata.manufacturer,
                            currency: stockdata.currency,
                            inc_number: stockdata.inc_number,
                            MOQ: stockdata.MOQ,
                            stock_quantity: stockdata.stock_quantity,
                            package: packageStr,
                            num: min,
                            price: price,
                            quantity: this.props.quantity,
                            totalPrice: total,
                            id: stockdata._id,
                        });
                    }
                }.bind(this));
            }
        }.bind(this));
    }
    componentWillReceiveProps(nextProps){
        if (this.props.stockId != nextProps.stockId) {
            Meteor.call('stock.findOneStock', nextProps.stockId, function(err, stockdata) {
                if (err != null) {
                    console.log(err);
                }
                else {
                    Meteor.call('product.findDetailAndImg', stockdata.product_id, function(err, productdata) {
                        if (err != null) {
                            console.log(err);
                        }
                        else {
                            if (stockdata.putaway == false || stockdata.type != nextProps.type) {
                                this.setState({valid: false});
                                nextProps.valid(nextProps.flag);
                            }
                            let packageStr = '';
                            for (let i = 0; i < stockdata.package.length; ++ i) {
                                if (packageStr == '') {
                                    packageStr = stockdata.package[i].package;
                                }
                                else {
                                    packageStr = packageStr + ', ' + stockdata.package[i].package;
                                }
                            }
                            let min = [];
                            let price = [];
                            for (let i = 0; i < stockdata.price.length; ++ i) {
                                min.push(stockdata.price[i].min);
                                price.push(stockdata.price[i].price);
                            }
                            let i = 0;
                            for (; i < min.length; ++ i) {
                                if (nextProps.quantity < min[i]) {
                                    break;
                                }
                            }
                            console.log('currency', stockdata.currency);
                            let total = price[i-1] * nextProps.quantity;
                            nextProps.price(nextProps.flag, total);

                            this.setState({
                                product_id: stockdata.product_id,
                                img: productdata.img,
                                detail: productdata.detail,
                                manufacturer: stockdata.manufacturer,
                                currency: stockdata.currency,
                                package: packageStr,
                                num: min,
                                price: price,
                                quantity: nextProps.quantity,
                                totalPrice: total,
                                id: stockdata._id,
                            });
                        }
                    }.bind(this));
                }
            }.bind(this));
        }
        else if (this.props.select != nextProps.select) {
            nextProps.price(nextProps.flag, this.state.totalPrice);
        }
        else if (this.props.quantity != nextProps.quantity) {
            let i = 0;
            for (; i < this.state.num.length; ++ i) {
                if (nextProps.quantity < this.state.num[i]) {
                    break;
                }
            }
            let total = this.state.price[i-1] * nextProps.quantity;
            nextProps.price(nextProps.flag, total);
            this.setState({
                quantity: nextProps.quantity,
                totalPrice: total,
            });
        }
    }
    //数量
    clickPlus(){
        if(this.state.quantity < this.state.stock_quantity){
            Meteor.call('cart.updateQuantity', this.props.stockId, this.state.inc_number, this.props.delivery_address);
        }
    }
    clickMinus(){
        if(this.state.quantity > this.state.MOQ){
            Meteor.call('cart.updateQuantity', this.props.stockId, -this.state.inc_number, this.props.delivery_address);
        }
    }
    changeNum(value){
        let tmp = value - this.state.MOQ;
        let result = Math.ceil(tmp / this.state.inc_number)*this.state.inc_number+this.state.MOQ;
        Meteor.call('cart.updateQuantity', this.props.stockId, result-this.props.quantity, this.props.delivery_address);
    }
    showNum(){
        let temp = [];
        temp[0] = (
            <div className="numInput">
                <span className="minus" onClick={this.clickMinus.bind(this)}><img src={(config.theme_path+'minus.png')} /></span>
                <InputNumber className="input_" min={this.state.MOQ} max={this.state.stock_quantity}  onChange={this.changeNum.bind(this)} value={this.state.quantity} />
                <span className="plus" onClick={this.clickPlus.bind(this)} ><img src={(config.theme_path+'plus.png') } /></span>
            </div>
        )
        return temp;
    }
    //商品详情
    showDetails(){
        let temp = [];
        temp[0]=(
            <div className="clearFloat">
                <div className="productLeft">
                    <img src={(config.theme_path+'shop_03.jpg')} alt=""/>
                </div>
                <div className="productRight">
                    <h3><Link to={'/product_details/'+ this.state.id}> {this.state.product_id}</Link></h3>
                    <p>厂家：{this.state.manufacturer}</p>
                    <p>封装：{this.state.package}</p>
                    <p className="p_">{this.state.detail}</p>
                </div>
            </div>)
        return temp;
    }
    //阶梯数量
    showNumber(){
        let temp = [];
        if (this.state.num != null) {
            for(let i = 0; i < this.state.num.length; i++){
                temp[i]=( <p className="price">{this.state.num[i]}+</p>)
            }
            return temp;
        }
    }
    //单价
    showPrice(){
        let temp = [];
        if (this.state.price != null) {
            if (this.state.currency == 2) {
                for(let i = 0; i < this.state.price.length; i++){
                    temp[i]=( <p className="price">${this.state.price[i]}/个</p>)
                }
            }
            else {
                for(let i = 0; i < this.state.price.length; i++){
                    temp[i]=( <p className="price">￥{this.state.price[i]}/个</p>)
                }
            }
            return temp;
        }
    }
    //总价
    showTotal(){
        let temp = [];
        if (this.state.totalPrice != null) {
            let price = Math.ceil(this.state.totalPrice*100)/100;
            if (this.state.currency == 2) {
                temp[0] = (
                    <div >
                        $<span className="total_price">{ price }</span>
                    </div>
                );
            }
            else {
                temp[0] = (
                    <div >
                        ￥<span className="total_price">{ price }</span>
                    </div>
                );
            }
            return temp;
        }
    }
    //操作
    showConfirm() {
        let This = this;
        let lebal = '';
        if(this.props.delivery_address == 1){
            lebal = I18n.t('shoppingCart.tab3');
        }else if(this.props.delivery_address == 2){
            lebal = I18n.t('shoppingCart.tab2');
        }
        confirm({
            title:I18n.t('shoppingCart.content.changeLocation')+lebal,
            content: I18n.t('shoppingCart.content.changeLocations'),
            onOk() {
                This.handleOks();
            },
            onCancel() {},
        });
    }
    ShowAlert(){
        console.log('this.props.delivery_address', this.props.delivery_address);
        if (this.props.delivery_address != 0) {
            // return (
            //     <DialogBoxDelete className="changeLocation " handleOk={this.handleOks.bind(this)} words={I18n.t('shoppingCart.content.changeLocation')}  content={I18n.t('shoppingCart.content.changeLocation')} sure={I18n.t('shoppingCart.content.changeLocations')} />
            // )
            return <p className="changeLocation " onClick={this.showConfirm.bind(this)} >{I18n.t('shoppingCart.content.changeLocation')}</p>;
        }
    }
    showConfirms() {
        let This = this;
        confirm({
            title:I18n.t('shoppingCart.dialog.deleteTitle'),
            content:I18n.t('shoppingCart.dialog.deleteContent'),
            onOk() {
                Meteor.call('cart.deleteRecord', This.props.stockId, This.props.delivery_address, This.props.quantity);
            },
            onCancel() {},
        });
    }
    showOperator(){
        let temp = [];
        if(this.state.valid == true){
            temp[0] = (
                <div>
                    {/*<DialogBoxDelete className="dialogBox " sure={I18n.t('shoppingCart.dialog.deleteContent')} handleOk={this.handleOk.bind(this)} words={I18n.t('shoppingCart.dialog.deleteTitle')}  content={I18n.t('PersonalCenter.manageStock.delete')} />*/}
                    <p className="dialogBox " onClick={this.showConfirms.bind(this)} >{I18n.t('shoppingCart.dialog.deleteTitle')}</p>
                    {this.ShowAlert()}
                </div>
            )
        }else if(this.state.valid == false){
            temp[0] = (
                <div>
                    {/*<DialogBoxDelete className="dialogBox " handleOk={this.handleOk.bind(this)} words={I18n.t('shoppingCart.dialog.deleteTitle')}  content={I18n.t('PersonalCenter.manageStock.delete')} />*/}
                    <p className="dialogBox " onClick={this.showConfirms.bind(this)} >{I18n.t('shoppingCart.dialog.deleteTitle')}</p>
                    <p style={{color:'#0c5aa2',cursor:'pointer',textAlign:'center'}}>{I18n.t('Besimilar')}</p>
                </div>
            )
        }
        return temp;
    }
    handleOk(){
        {/*删除弹窗确认事件*/}
        Meteor.call('cart.deleteRecord', this.props.stockId, this.props.delivery_address, this.props.quantity);
    }

    handleOks(){
        if (this.props.delivery_address == 1) {
            Meteor.call('cart.changeDeliveryAddress', this.props.stockId, this.props.quantity, this.props.delivery_address, 2);
        }
        else {
            Meteor.call('cart.changeDeliveryAddress', this.props.stockId, this.props.quantity, this.props.delivery_address, 1);
        }
    }
    select() {
        this.props.click(this.props.flag);
    }
    ShowCheck(){
        if(this.state.valid == true){
            return(
                <Checkbox checked={this.props.select} className="" onClick={this.select.bind(this)} />
            )
        }
    }
    render(){
        return (
            <tr style={{borderColor:'red'}} >
                <div style={this.state.valid ? {display:'none'} : {display:'block'}} className="table_Shaw" ></div>
                <Button style={this.state.valid ? {display:'none'} : {display:'block'}} >{I18n.t('Invalid')}</Button>
                <td style={{width: 50}} className="checkboxs">
                    {this.ShowCheck()}
                </td>
                <td style={{width: 300}} className="productDetails">
                    {this.showDetails()}
                </td>
                <td style={{width: 150}} className="number">
                    {this.showNumber()}
                </td>
                <td style={{width: 150}}  className="price">
                    {this.showPrice()}
                </td>
                <td style={{width: 140}}  className="num">
                    {this.showNum()}
                </td>
                <td style={{width: 135}} className="total">
                    {this.showTotal()}
                </td >
                <td style={{width: 130}} className="deliveryCycle">
                    {(this.props.delivery_address == 0) ? '一周' : '两周'}
                </td >
                <td style={{width: 146}} className="operator">
                    {this.showOperator()}
                </td >
            </tr>
        );
    }
}