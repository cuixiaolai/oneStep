import { Modal, Button,Radio } from 'antd';
import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
{/*更改交货地弹窗*/}
const RadioGroup = Radio.Group;

export default class DialogBoxChange extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            value:1,
            loading: false,
            visible: false,
        });

    }
    componentDidMount(){
        
    }
    onChange(e) {
        this.setState({
            value: e.target.value,
        });
        this.props.onChange(e.target.value);
    }
    showModal() {
        this.setState({
            visible: true,
        });
    }
    handleOk(){
        {/*弹窗确认事件*/}
        console.log('Clicked OK');

        this.setState({
            visible: false,
        });
        this.props.handleaddressOk()
    }
    handleCancel() {
        this.setState({ visible: false });
    }
    render() {
        const radioStyle = {
            display: 'block',
            height: '30px',
            lineHeight: '30px',
        };
        return (
            <div className={this.props.className}>
                <span onClick={this.showModal.bind(this)} >
                    {this.props.content}
                </span>
                <Modal ref="modal"
                       visible={this.state.visible}
                       title="" onOk={this.handleOk.bind(this)} onCancel={this.handleCancel.bind(this)}
                       footer={[
            <Button  key="back" type="ghost" style={{height:32,width:72,marginRight:50,fontSize:14}} onClick={this.handleCancel.bind(this)}>{I18n.t('PersonalCenter.company.no')}</Button>,
            /*在此处加*/
            <Button className="yes" key="submit" type="primary" style={{height:32,width:72,marginRight:140,fontSize:14}} loading={this.state.loading} onClick={this.handleOk.bind(this)}>
                {I18n.t('PersonalCenter.company.yes1')}
            </Button>
          ]}
                >
                    <div className="changeDialog"  style={{paddingRight:30,paddingLeft:30,paddingTop:30,fontSize:14}}>
                        <p className="title">{this.props.words}</p>
                        <RadioGroup onChange={this.onChange.bind(this)} value={this.state.value}>
                            <Radio style={radioStyle} key="a" value={1}>{I18n.t('shoppingCart.dialog.changeContent1')} </Radio>
                            <Radio style={radioStyle} key="b" value={2}>{I18n.t('shoppingCart.dialog.changeContent2')} </Radio>
                        </RadioGroup>
                     </div>

                </Modal>
            </div>
        );
    }
}