import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router'
import { createContainer } from 'meteor/react-meteor-data';
import { Tabs,Table,Checkbox,InputNumber } from 'antd';
import config from '../../config.json';
import DialogBoxDelete from './DialogBoxDelete.jsx';
import DialogBoxChange from './DialogBoxChange.jsx';
import PromptBox from './PromptBox.jsx';
import ShoppingCartTable from './ShoppingCartTable.jsx';
{/*原厂商*/}

export default class ShoppingCartOriginalManufacturer extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            selectAll: false,
            select: [],
            selectItems: [],
            valid: [],
            totalPrice: [],
            data: [],
            flag: true,
        });
        this.clickSelectAll = this.clickSelectAll.bind(this);
        this.clickunSelectAll = this.clickunSelectAll.bind(this);
        this.clickAll = this.clickAll.bind(this);
    }
    componentDidMount() {
        let map = new Map();
        for (let i = 0; i < this.props.cartInfo.length; ++ i) {
            let item = {
                stockId: this.props.cartInfo[i].stockId,
                company: this.props.cartInfo[i].company,
                quantity: this.props.cartInfo[i].quantity,
                delivery_address: this.props.cartInfo[i].delivery_address,
                stock_type: this.props.cartInfo[i].stock_type,
            };
            if (map.has(this.props.cartInfo[i].company)) {
                let items = map.get(this.props.cartInfo[i].company);
                items.push(item);
                map.set(this.props.cartInfo[i].company, items);
            }
            else {
                map.set(this.props.cartInfo[i].company, [item]);
            }
        }
        let data = [];
        let select = [];
        let selectItems = [];
        let totalPrice = [];
        let valid = [];
        for (let [key, value] of map) {
            data.push({company: key, value: value});
            select.push(false);
            let tmp = [];
            let tmp1 = [];
            let tmp2 = [];
            for (let i = 0; i < value.length; ++ i) {
                tmp.push(false);
                tmp1.push(0);
                tmp2.push(true);
            }
            selectItems.push(tmp);
            totalPrice.push(tmp1);
            valid.push(tmp2);
        }
        this.setState({data: data, select: select, selectItems: selectItems, totalPrice: totalPrice, valid: valid});
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.cartInfo.length != nextProps.cartInfo.length) {
            let map = new Map();
            for (let i = 0; i < nextProps.cartInfo.length; ++ i) {
                let item = {
                    stockId: nextProps.cartInfo[i].stockId,
                    company: nextProps.cartInfo[i].company,
                    quantity: nextProps.cartInfo[i].quantity,
                    delivery_address: nextProps.cartInfo[i].delivery_address,
                    stock_type: nextProps.cartInfo[i].stock_type,
                };
                if (map.has(nextProps.cartInfo[i].company)) {
                    let items = map.get(nextProps.cartInfo[i].company);
                    items.push(item);
                    map.set(nextProps.cartInfo[i].company, items);
                }
                else {
                    map.set(nextProps.cartInfo[i].company, [item]);
                }
            }
            let data = [];
            let select = [];
            let selectItems = [];
            let totalPrice = [];
            let valid = [];
            for (let [key, value] of map) {
                data.push({company: key, value: value});
                select.push(false);
                let tmp = [];
                let tmp1 = [];
                let tmp2 = [];
                for (let i = 0; i < value.length; ++ i) {
                    tmp.push(false);
                    tmp1.push(0);
                    tmp2.push(true);
                }
                selectItems.push(tmp);
                totalPrice.push(tmp1);
                valid.push(tmp2);
            }
            this.setState({data: data, select: select, selectItems: selectItems, totalPrice: totalPrice, valid: valid, selectAll: false});
            this.props.select(this.props.flag, [], 0);
            this.props.click(false);
        }
        else {
            let i = 0;
            for (; i < this.props.cartInfo.length; ++ i) {
                if (this.props.cartInfo[i] != nextProps.cartInfo[i]) {
                    break;
                }
            }
            if (i != this.props.cartInfo.length) {
                let map = new Map();
                for (let i = 0; i < nextProps.cartInfo.length; ++ i) {
                    let item = {
                        stockId: nextProps.cartInfo[i].stockId,
                        company: nextProps.cartInfo[i].company,
                        quantity: nextProps.cartInfo[i].quantity,
                        delivery_address: nextProps.cartInfo[i].delivery_address,
                        stock_type: nextProps.cartInfo[i].stock_type,
                    };
                    if (map.has(nextProps.cartInfo[i].company)) {
                        let items = map.get(nextProps.cartInfo[i].company);
                        items.push(item);
                        map.set(nextProps.cartInfo[i].company, items);
                    }
                    else {
                        map.set(nextProps.cartInfo[i].company, [item]);
                    }
                }
                let data = [];
                for (let [key, value] of map) {
                    data.push({company: key, value: value});
                }
                this.setState({data: data});
                let id = [];
                let price = 0;
                for (let i = 0; i < this.state.selectItems.length; ++ i) {
                    for (let j = 0; j < this.state.selectItems[i].length; ++ j) {
                        if (this.state.selectItems[i][j] == true && this.state.valid[i][j] == true) {
                            id.push(data[i].value[j]);
                            price += this.state.totalPrice[i][j];
                        }
                    }
                }
                this.props.select(this.props.flag, id, price);
            }
        }
    }
    //单一公司全选
    clickAll(i){
        this.state.select[i] = !this.state.select[i];
        let tmp = [];
        for (let k = 0; k < this.state.selectItems[i].length; ++ k) {
            tmp.push(this.state.select[i]);
        }
        this.state.selectItems[i] = tmp;
        this.setState({select: this.state.select, selectItems: this.state.selectItems});
        let j = 0;
        for(; j < this.state.select.length; j++) {
            if (this.state.select[j] == false) {
                break;
            }
        }
        if (j == this.state.select.length) {
            this.setState({selectAll:true});
            this.props.click(true);
        }
        else if (this.state.selectAll == true){
            this.setState({selectAll:false});
            this.props.click(false);
        }
        let id = [];
        let price = 0;
        for (let i = 0; i < this.state.selectItems.length; ++ i) {
            for (let j = 0; j < this.state.selectItems[i].length; ++ j) {
                if (this.state.selectItems[i][j] == true && this.state.valid[i][j] == true) {
                    id.push(this.state.data[i].value[j]);
                    price += this.state.totalPrice[i][j];
                }
            }
        }
        this.props.select(this.props.flag, id, price);
    }
    //原厂商全选
    clickAllSelect(){
        if(this.state.selectAll == false){
            this.setState({selectAll:true});
            let select = [];
            let selectItems = [];
            for (let i = 0; i < this.state.select.length; ++ i) {
                select.push(true);
                let tmp = [];
                for (let j = 0; j < this.state.selectItems[i].length; ++ j) {
                    tmp.push(true);
                }
                selectItems.push(tmp);
            }
            this.setState({select: select, selectItems: selectItems});
            this.props.click(true);
            let id = [];
            let price = 0;
            for (let i = 0; i < selectItems.length; ++ i) {
                for (let j = 0; j < selectItems[i].length; ++ j) {
                    if (this.state.valid[i][j] == true) {
                        id.push(this.state.data[i].value[j]);
                        price += this.state.totalPrice[i][j];
                    }
                }
            }
            this.props.select(this.props.flag, id, price);
        }else{
            this.setState({selectAll:false});
            let select = [];
            let selectItems = [];
            for (let i = 0; i < this.state.select.length; ++ i) {
                select.push(false);
                let tmp = [];
                for (let j = 0; j < this.state.selectItems[i].length; ++ j) {
                    tmp.push(false);
                }
                selectItems.push(tmp);
            }
            this.setState({select: select, selectItems: selectItems});
            this.props.click(false);
            this.props.select(this.props.flag, [], 0);
        }
    }

    //全选
    clickSelectAll(){
        this.setState({selectAll:true});
        let select = [];
        let selectItems = [];
        for (let i = 0; i < this.state.select.length; ++ i) {
            select.push(true);
            let tmp = [];
            for (let j = 0; j < this.state.selectItems[i].length; ++ j) {
                tmp.push(true);
            }
            selectItems.push(tmp);
        }
        this.setState({select: select, selectItems: selectItems});
        let id = [];
        let price = 0;
        for (let i = 0; i < selectItems.length; ++ i) {
            for (let j = 0; j < selectItems[i].length; ++ j) {
                if (this.state.valid[i][j] == true) {
                    id.push(this.state.data[i].value[j]);
                    price += this.state.totalPrice[i][j];
                }
            }
        }
        this.props.select(this.props.flag, id, price);
    }
    //不选
    clickunSelectAll(){
        this.setState({selectAll:false});
        let select = [];
        let selectItems = [];
        for (let i = 0; i < this.state.select.length; ++ i) {
            select.push(false);
            let tmp = [];
            for (let j = 0; j < this.state.selectItems[i].length; ++ j) {
                tmp.push(false);
            }
            selectItems.push(tmp);
        }
        this.setState({select: select, selectItems: selectItems});
        this.props.select(this.props.flag, [], 0);
    }
    //单项选择
    itemSelect(i, j) {
        this.state.selectItems[i][j] = !this.state.selectItems[i][j];
        this.setState({selectItems: this.state.selectItems});
        let k = 0;
        for(; k < this.state.selectItems[i].length; k++) {
            if (this.state.selectItems[i][k] == false && this.state.valid[i][k] == true) {
                break;
            }
        }
        if (k == this.state.selectItems[i].length) {
            this.state.select[i] = true;
        }
        else {
            this.state.select[i] = false;
        }
        this.setState({select: this.state.select});
        k = 0;
        for(; k < this.state.select.length; k++) {
            if (this.state.select[k] == false) {
                break;
            }
        }
        if (k == this.state.select.length) {
            this.setState({selectAll:true});
            this.props.click(true);
        }
        else if (this.state.selectAll == true) {
            this.setState({selectAll:false});
            this.props.click(false);
        }
        let id = [];
        let price = 0;
        for (let i = 0; i < this.state.selectItems.length; ++ i) {
            for (let j = 0; j < this.state.selectItems[i].length; ++ j) {
                if (this.state.selectItems[i][j] == true && this.state.valid[i][j] == true) {
                    id.push(this.state.data[i].value[j]);
                    price += this.state.totalPrice[i][j];
                }
            }
        }
        this.props.select(this.props.flag, id, price);
    }
    //价格
    totalPrice(i, j, price) {
        this.state.totalPrice[i][j] = price;
        //this.setState({totalPrice: this.state.totalPrice});
        let total = 0;
        for (let i = 0; i < this.state.selectItems.length; ++ i) {
            for (let j = 0; j < this.state.selectItems[i].length; ++ j) {
                if (this.state.selectItems[i][j] == true && this.state.valid[i][j] == true) {
                    total += this.state.totalPrice[i][j];
                }
            }
        }
        this.props.price(this.props.flag, total);
    }
    //失效
    valid(i, j) {
        this.state.valid[i][j] = false;
        this.setState({valid: this.state.valid});
    }
    ShowAllName(name){
        if(name.length>4){
            return(
                <div className="ShowAllName">
                    {name}
                </div>
            )
        }
    }
    showOManufacturer(){
        let temp = [];
        if(this.state.data.length == 0){
            temp[0] = (
                <div className="MyTableNoMessage">
                    <div className="MyTableNoMessageInner">
                        <p>{I18n.t('top.noshop')}</p>
                    </div>
                </div>
            )
        }else {
            for(let i = 0; i < this.state.data.length; i++){
                temp[i]=(
                    <div>
                        <div className="allSelect">
                            <Checkbox  className="tCheckbox" checked={this.state.select[i]} onClick={this.clickAll.bind(this,i)}>
                                {this.state.data[i].company.length > 5 ? this.state.data[i].company.substr(0,4)+ '...': this.state.data[i].company}
                            </Checkbox>
                            {this.ShowAllName(this.state.data[i].company)}
                            <img src={(config.theme_path+'talk.png')} className="talkImg" alt="图片"/>
                        </div>
                        <ShoppingCartTable items={this.state.data[i].value} select={this.state.selectItems[i]} click={this.itemSelect.bind(this)} price={this.totalPrice.bind(this)} valid={this.valid.bind(this)} flag={i}/>
                    </div>
                )
            }
        }

        return temp;
    }
    render(){
        return(
            <div>
                <div className="table1">
                    <Checkbox  className="oCheckbox" checked={this.state.selectAll}  onClick={this.clickAllSelect.bind(this)}> {this.props.title}</Checkbox>
                    <div className = 'table_'>
                        {this.showOManufacturer()}
                        {
                            this.state.numflag == true?(<PromptBox />):(<div></div>)
                        }
                    </div>
                </div>
            </div>
        );
    }
}