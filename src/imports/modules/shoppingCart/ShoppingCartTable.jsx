import React, { Component } from 'react';
import ShoppingCartTr from './ShoppingCartTr.jsx';

export default class ShoppingCartTable extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            totalPrice: [],
        });
    }
    select(i) {
        this.props.click(this.props.flag, i);
    }
    price(i, total) {
        this.props.price(this.props.flag, i, total);
    }
    valid(i) {
        this.props.valid(this.props.flag, i);
    }
    tableRender() {
        console.log(this.props.items);
        let temp = [];
        for (let i = 0; i < this.props.items.length; ++ i) {
            temp[i] = (
                <ShoppingCartTr stockId={this.props.items[i].stockId} quantity={this.props.items[i].quantity} delivery_address={this.props.items[i].delivery_address} type={this.props.items[i].stock_type} select={this.props.select[i]} flag={i} click={this.select.bind(this)} price={this.price.bind(this)} valid={this.valid.bind(this)} />
            );
        }
        return temp;
    }
    render(){
        
        return (
            <table>
                {this.tableRender()}
            </table>
        );
    }
}