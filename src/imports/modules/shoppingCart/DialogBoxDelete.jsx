import { Modal, Button,Radio } from 'antd';
import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
{/*更改交货地弹窗*/}

export default class DialogBoxDelete extends Component{
    constructor(props) {
        super(props);
        this.state = ({
          value:1,
            loading: false,
            visible: false,
        });

    }
    onChange(e) {
        console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });
    }
    showModal() {
        this.setState({
            visible: true,
        });
    }
    handleOk(){
        {/*弹窗确认事件*/}
        // this.props.handleOk();
        if (this.props.type == 0) {
            for (let k = 0; k < 3; ++ k) {
                for (let j = 0; j < this.props.data[k].length; ++ j) {
                    if (this.props.data[k][j].delivery_address == 1) {
                        Meteor.call('cart.changeDeliveryAddress', this.props.data[k][j].stockId, this.props.data[k][j].quantity, this.props.data[k][j].delivery_address, 2);
                    }
                    else {
                        Meteor.call('cart.changeDeliveryAddress', this.props.data[k][j].stockId, this.props.data[k][j].quantity, this.props.data[k][j].delivery_address, 1);
                    }
                }
            }
        }
        console.log('Clicked OK');
        this.setState({
            visible: false,
        });
    }
    handleCancel() {
        this.setState({ visible: false });
    }
    render() {
        return (
            <div>
                <span onClick={this.showModal.bind(this)} >
                    {this.props.content}
                </span>
                <Modal ref="modal"
                       visible={this.state.visible}
                       title="" onOk={this.handleOk.bind(this)} onCancel={this.handleCancel.bind(this)}
                       okText={I18n.t('PersonalCenter.company.yes1')} cancelText={I18n.t('PersonalCenter.company.no')}
                >
                    <div className="deleteDialog"  style={{paddingRight:30,paddingLeft:120,paddingTop:30,fontSize:14}}>
                        <div className="title">
                            <img src={(config.theme_path+'mark.png')} alt="图片"/>
                            <p>{this.props.words}</p>
                        </div>
                        <p className="content"> {this.props.sure} </p>
                    </div>
                </Modal>
            </div>
        );
    }
}