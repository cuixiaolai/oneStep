import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router'
import { createContainer } from 'meteor/react-meteor-data';
import { Tabs,Table,Checkbox,InputNumber,Modal } from 'antd';
import { browserHistory } from 'react-router';
import ShoppingCartOriginalManufacturer from './ShoppingCartOriginalManufacturer.jsx';
import EmptyCart from './EmptyCart.jsx';
const confirm = Modal.confirm;
{/*购物车*/}
export default class ShoppingCartContent extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            selectAll: 0,
            selected: false,
            selectId: [[], [], []],
            selectPrice: [0, 0, 0],
            count: 0,
            price: 0,
        });
    }
    componentDidMount() {
        if (this.props.cartInfo.length == 0) {
            this.setState({count: 0, price: 0});
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.cartInfo.length == 0) {
            this.setState({count: 0, price: 0});
        }
    }
    setSelectId(i, select, total) {
        this.state.selectId[i] = select;
        this.state.selectPrice[i] = total;
        let count = 0;
        for (let k = 0; k < 3; ++ k) {
            for (let j = 0; j < this.state.selectId[k].length; ++ j) {
                if (this.state.selectId[k][j] != null) {
                    count += this.state.selectId[k][j].quantity;
                }
            }
        }
        let price = this.state.selectPrice[0] + this.state.selectPrice[1] + this.state.selectPrice[2];
        this.setState({selectId: this.state.selectId, count: count, selectPrice: this.state.selectPrice, price: price});
    }
    setSelectPrice(i, total) {
        this.state.selectPrice[i] = total;
        let price = this.state.selectPrice[0] + this.state.selectPrice[1] + this.state.selectPrice[2];
        this.setState({selectPrice: this.state.selectPrice, price: price});
    }
    clickAll(value) {
        let count = this.state.selectAll;
        if (value == true) {
            count = count + 1;
            if (count == 3) {
                this.setState({selected: true});
            }
        }
        else {
            count = count - 1;
            this.setState({selected: false});
        }
        this.setState({selectAll: count});
    }
    onclick(){
        if(this.state.selected == true){
            this.setState({selected: false, selectAll: 0});
            this.refs.func1.clickunSelectAll();
            this.refs.func2.clickunSelectAll();
            this.refs.func3.clickunSelectAll();
        }else{
            this.setState({selected: true, selectAll: 3});
            this.refs.func1.clickSelectAll();
            this.refs.func2.clickSelectAll();
            this.refs.func3.clickSelectAll();
        }
    }
    //删除弹窗
    handleOk(){
        this.setState({
            visible: false,
        });
        for (let k = 0; k < 3; ++ k) {
            for (let j = 0; j < this.state.selectId[k].length; ++ j) {
                if (this.state.selectId[k][j] != null) {
                    Meteor.call('cart.deleteRecord', this.state.selectId[k][j].stockId, this.state.selectId[k][j].delivery_address, this.state.selectId[k][j].quantity);
                }
            }
        }
    }
    showTop(){
        let temp = [];
        temp [0] = (
            <ul>
                <li style={{width:76,paddingLeft:14}}>
                    <Checkbox  className="checkbox1" checked={this.state.selected}  onClick={this.onclick.bind(this)} > {I18n.t('PersonalCenter.manageStock.select')}</Checkbox>
                </li>
                <li style={{width:270}}><p>{I18n.t('shoppingCart.content.productDetails')}</p></li>
                <li style={{width:150}}><p>{I18n.t('shoppingCart.content.number')}</p></li>
                <li style={{width:150}}><p>{I18n.t('shoppingCart.content.price')}</p></li>
                <li style={{width:140}}><p>{I18n.t('shoppingCart.content.num')}</p></li>
                <li style={{width:135}}><p>{I18n.t('shoppingCart.content.totalPrice')}</p></li>
                <li style={{width:130}}><p>{I18n.t('shoppingCart.content.deliveryCycle')}</p></li>
                <li style={{width:146}}><p>{I18n.t('shoppingCart.content.operator')}</p></li>
                <p className="clear"></p>
            </ul>
        )
        return temp;
    }
    handleOks(){
        for (let k = 0; k < 3; ++ k) {
            for (let j = 0; j < this.state.selectId[k].length; ++ j) {
                if (this.state.selectId[k][j].delivery_address == 1) {
                    Meteor.call('cart.changeDeliveryAddress', this.state.selectId[k][j].stockId, this.state.selectId[k][j].quantity, this.state.selectId[k][j].delivery_address, 2);
                }
                else {
                    Meteor.call('cart.changeDeliveryAddress', this.state.selectId[k][j].stockId, this.state.selectId[k][j].quantity, this.state.selectId[k][j].delivery_address, 1);
                }
            }
            this.state.selectId[k] = [];
        }
    }
    showConfirms() {
        let This = this;
        confirm({
            title:I18n.t('shoppingCart.dialog.deleteTitle'),
            content:I18n.t('shoppingCart.dialog.deleteContent'),
            onOk() {
                This.handleOk();
            },
            onCancel() {},
        });
    }
    showConfirm() {
        let This = this;
        confirm({
            title:I18n.t('shoppingCart.content.changeLocation'),
            content: I18n.t('shoppingCart.content.changeLocations'),
            onOk() {
                This.handleOks();
            },
            onCancel() {},
        });
    }
    ShowAlert(){
        if(this.props.keys != 0){
            if(this.state.selectId[0].length == 0&&this.state.selectId[1].length == 0&&this.state.selectId[2].length == 0){
                return(
                    <div className="deletePadding left"  >
                        <span>{I18n.t('shoppingCart.content.changeLocation')}</span>
                    </div>
                );
            }else{
                return(
                    <div className="deletePadding left"  >
                        <span onClick={this.showConfirm.bind(this)}>{I18n.t('shoppingCart.content.changeLocation')}</span>
                    </div>
                );
            }
        }
    }
    ShowDelete(){
        if(this.state.selectId[0].length == 0&&this.state.selectId[1].length == 0&&this.state.selectId[2].length == 0){
            return(
                <div className="deletePadding left"  >
                    <span>{I18n.t('shoppingCart.content.delete')}</span>
                </div>
            );
        }else{
            return(
                <div className="deletePadding left"  >
                    <span  onClick={this.showConfirms.bind(this)}>{I18n.t('shoppingCart.content.delete')}</span>
                </div>
            );
        }
    }
    account() {
        console.log(this.state.selectId);
        let data = [];
        for (let i = 0; i < 3; ++ i) {
            for (let j = 0; j < this.state.selectId[i].length; ++j) {
                if (i == 0) {
                    let item = {
                        stockId: this.state.selectId[i][j].stockId,
                        stockType: 'manufacturer',
                        company: this.state.selectId[i][j].company,
                        quantity: this.state.selectId[i][j].quantity,
                    }
                    data.push(item);
                }
                else if (i == 1) {
                    let item = {
                        stockId: this.state.selectId[i][j].stockId,
                        stockType: 'agent',
                        company: this.state.selectId[i][j].company,
                        quantity: this.state.selectId[i][j].quantity,
                    }
                    data.push(item);
                }
                else {
                    let item = {
                        stockId: this.state.selectId[i][j].stockId,
                        stockType: 'retailer',
                        company: this.state.selectId[i][j].company,
                        quantity: this.state.selectId[i][j].quantity,
                    }
                    data.push(item);
                }
            }
        }
        console.log(data);
        Meteor.call('quote.insertProduct', data, this.props.keys);
        browserHistory.replace('/Payment');
    }
    ShowPment(){
        if(this.state.count > 0){
            return(
                <span className="span_" onClick={this.account.bind(this)}>{I18n.t('shoppingCart.content.settlement')}</span>
            )
        }else{
            return(
                <span className="span_">{I18n.t('shoppingCart.content.settlement')}</span>
            )
        }
    }
    showBottom(){
        let temp = [];
        temp[0] = (
            <div className="tableBottom">
                <Checkbox  className="checkbox1 left" checked={this.state.selected}  onClick={this.onclick.bind(this)}> <p>{I18n.t('PersonalCenter.manageStock.select')}</p></Checkbox>
                {this.ShowDelete()}
                {this.ShowAlert()}
                {(this.props.keys == 0) ? (
                    <span className="right" style={{color:'#0c5aa2',fontSize:'20px',marginRight:160}}>￥{this.state.price}</span>
                ) : (
                    <span className="right" style={{color:'#0c5aa2',fontSize:'20px',marginRight:160}}>${this.state.price}</span>
                )}

                <p className="right">总计（不含运费）：</p>
                <p className="p_ right">已选择 <span className="span1">{this.state.count}</span> 件商品</p>
                {this.ShowPment()}
                <div className="clear"></div>
            </div>
        )
        return temp;
    }
    render(){
    /* modify by pylon
     shoppingCart.content.manufacturer 
    item=>item.stock_type=='manufacturer'
    */
        return(
            <div className="shoppingCartContent">
                <div className="contentTop" >
                    {this.showTop()}
                </div>
                {this.props.cartInfo.length != 0 ?
                    (
                    <div>
                        <ShoppingCartOriginalManufacturer ref="func1" title={I18n.t('shoppingCart.content.manufacturer')} click={this.clickAll.bind(this)} cartInfo={this.props.cartInfo.filter(item=>item.stock_type=='manufacturer')} select={this.setSelectId.bind(this)} price={this.setSelectPrice.bind(this)} flag={0}/>
                    <div className="table2">
                        <ShoppingCartOriginalManufacturer ref="func2" title={I18n.t('shoppingCart.content.agent')} click={this.clickAll.bind(this)} cartInfo={this.props.cartInfo.filter(item=>item.stock_type=='agent')} select={this.setSelectId.bind(this)} price={this.setSelectPrice.bind(this)} flag={1}/>
                    </div>
                    <div className="table3">
                        <ShoppingCartOriginalManufacturer ref="func3" title={I18n.t('shoppingCart.content.retailer')} click={this.clickAll.bind(this)} cartInfo={this.props.cartInfo.filter(item=>item.stock_type=='retailer')} select={this.setSelectId.bind(this)} price={this.setSelectPrice.bind(this)} flag={2}/>
                    </div>
                    {this.showBottom()}
                    </div>
                    ):(
                    <EmptyCart />
                )}

            </div>
        );
    }
}