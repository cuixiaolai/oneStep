import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Select, DatePicker, Modal, Button ,Input ,Menu, Dropdown, Icon,Form ,Tabs, Col, Table, Checkbox, Pagination } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
import { Link } from 'react-router'
/*卖家中心-库存管理-表单*/
export default class EmptyCart extends Component{
    constructor(props) {
        super(props);
        this.state = ({
        });
    }
    componentDidMount(){
    }
    render(){
        return(
            <div className="emptyCart clearFloat">
                {/*空购物车*/}
                <div className = "emptyCart_">
                    <img src={(config.theme_path+'noaddress.png')} alt="图片"/>
                    <p className="p1">{I18n.t('shoppingCart.dialog.emptyP1')}</p>
                    <p className="p2">{I18n.t('shoppingCart.dialog.emptyP2')}
                        <Link to="/"><span className="span_">{I18n.t('shoppingCart.dialog.emptyP3')}</span></Link>
                        {I18n.t('shoppingCart.dialog.emptyP4')}</p>
                </div>
            </div>
        );
    }
}
