import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Input, Checkbox,Button } from 'antd';
import { Meteor } from 'meteor/meteor'
import { browserHistory } from 'react-router';

export default class RightNow extends Component{
    handleSubmit(e) {
        e.preventDefault();
        Meteor.logout(function(err) {
            if (err) {
                console.log(err);
            }
            else {
                browserHistory.replace('/login');
            }
        });
    }

    render(){
        return(
            <div className="Rightnow">
                <Link to="/"><Button type="primary"><Translate value="alertsuccess.rightnow" /></Button></Link>
                <span onClick={this.handleSubmit}><Translate value="alertsuccess.index" /></span>
            </div>
        )
    }
}
