import React, { Component } from 'react';
import { Link } from 'react-router';
import { Button,Icon} from 'antd';
import config from '../config.json';
import { Translate, I18n } from 'react-redux-i18n';
import MyTr from './MyTr.jsx';


export default class MyTable extends Component{
    constructor(state) {
        super(state);
    }
    ShowTh(){
        let TH = [];
        for(let i=0;i<this.props.columns.length;i++){
            TH[i] = (
                // <th key={this.props.columns[i].dataIndex}>
                <th key={i}>
                    {this.props.columns[i].titlr}
                </th>
            )
        }
        return TH;
    }
    ShowTr(){
        let TR = [];
        if (this.props.date.length != 0) {
            for(let i = 0;i<this.props.date.length;i++){
                TR[i] = (
                    <MyTr date={this.props.date[i]} openToPayment={this.props.openToPayment} addToCart={this.props.addToCart} stocktype={this.props.stocktype}/>
                )
            }
        }
        return TR;
    }
    ShowMessage(){
        if (this.props.date.length == 0){
            return(
                <div className="MyTableNoMessage">
                    <div className="MyTableNoMessageInner">
                        <p>{this.props.message}</p>
                    </div>
                </div>
            )
        }
    }
    render(){
        return(
            <div style={{width:'100%',overflowX:'auto'}}>
                <table style={this.props.date.length == 0 ? {width:'100%'} : {width:1600}}  border='1' cellspacing='0' bordercolor="#d6d6d6" cellpadding='0' className="MyTable">
                    <tr>
                        {this.ShowTh()}
                    </tr>
                    {this.ShowTr()}
                </table>
                {this.ShowMessage()}
            </div>
        )
    }
}
