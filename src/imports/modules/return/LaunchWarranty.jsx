import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router';
import { Steps, Button } from 'antd';
const Step = Steps.Step;
import config from '../../config.json';
import LaunchWarrantyBox from './LaunchWarrantyBox.jsx';
const array = Array.apply(null, [5,6,7,8,9]);
const steps = array.map((item,i) => ({
    title: I18n.t('Register.top.one')
}));

export default class LaunchWarranty extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            style: 116,
        });
    }
    componentDidMount(){
        this.setState({
            style:this.state.style + 146*this.props.current
        })
    }
    render(){
        return (
            <div className="perPhoneregister LaunchWarranty">
                <Steps current={this.props.current}>
                    <Step title={I18n.t('PersonalCenterSecurity.SecurityAuthentication.Submit')}  key="1" />
                    <Step title='平台审核中' key="2" />
                    <Step title={I18n.t('return.payment')}  key="3" />
                    <Step title={I18n.t('return.Inspection')}  key="4" />
                    <Step title={I18n.t('return.Detection')}  key="5" />
                    <Step title={I18n.t('return.Identification')}  key="6" />
                    <div className="bigXian"></div>
                    <img  src={(config.theme_path + '/center.png')} alt="" ref="picShaw" style={{left:this.state.style+'px',width:54}}  />
                </Steps>
                <LaunchWarrantyBox current={this.props.current}
                                   card={this.props.card}
                                   order={this.props.order}
                                   handleBuyerQualityAssurance={this.props.handleBuyerQualityAssurance}
                                   handleBuyerDeliveryQualityAssurance={this.props.handleBuyerDeliveryQualityAssurance}
                />
            </div>
        )
    }
}