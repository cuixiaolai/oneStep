import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router';
import { Input,Button,Cascader  } from 'antd';
import config from '../../config.json';
export default class ReturnRefundSuccess extends Component{
    constructor(props) {
        super(props);
    }
    show(){
        if(this.props.order.return_goods.agreed){
            return (
                <div className="MyRefundBoxThree">
                        <p>
                            <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                            <span>{I18n.t('return.Refundsuccess')}</span>
                        </p>
                        <p>{I18n.t('return.number')}</p>
                        <p>{typeof this.props.order.update_time_problem != 'object'?'':(this.props.order.update_time_problem.getFullYear()+'-'+(this.props.order.update_time_problem.getMonth()+1)+'-'+this.props.order.update_time_problem.getDate())}</p>
                        <p>{I18n.t('return.money')} <span style={{color: '#0c5aa2'}}>{this.props.order.currency == 1?'￥':'$'}{this.props.order.return_goods.amount}</span>
                        </p>
                    </div>
            )
        }else {
            return(
                    <div className="MyRefundBoxThree">
                        <p>
                            <img src={(config.theme_path + 'rederror.png')} alt=""/>
                            <span>{I18n.t('return.Refunderror')}</span>
                        </p>
                        <p>{I18n.t('BuyerBox.errorreson')} <span>{this.props.order.return_goods.refused_reason}</span></p>
                        <p>{typeof this.props.order.update_time_problem != 'object'?'':(this.props.order.update_time_problem.getFullYear()+'-'+(this.props.order.update_time_problem.getMonth()+1)+'-'+this.props.order.update_time_problem.getDate())}</p>
                        <p>备注说明:<span style={{color:'#0c5aa2'}}>{this.props.order.return_goods.addition}</span> </p>
                    </div>
            )
        }
    }
    render(){
        return(
            <div className="MyRefundBox">
                {this.show()}
            </div>
        )
    }
}
