import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router';
import { Input,Button,Cascader  } from 'antd';
import config from '../../config.json';
import Photo from '../Evaluation/Photo.jsx';
export default class MyRefundBox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            num:1,
            return_reason:''
        });
    }
    onChange(value, label){
        this.setState({
            return_reason:label
        });
    }
    setList(arr){
        console.log(arr);
    }
    handleSubmit(){
        if(this.state.return_reason!= ''){
            const amount = document.getElementById('amount').value;
            const explain = document.getElementById('explain').value;
            this.props.handleBuyerReturnFund(amount,this.state.return_reason[0].label,explain);
        }else{
            console.log('need to choose reason');
        }
    }
    show(){
        const options = [{
            value: 'size',
            label: '大小尺寸与商品插述不符',
        }, {
            value: 'color',
            label: '颜色案式与商品插述不符',
        }, {
            value: 'Fabric',
            label: '质面料与商品插述不符',
        }, {
            value: 'small',
            label: '少发货 ',
        }, {
            value: 'package',
            label: '包装商品损圬 ',
        }, {
            value: 'counterfeit',
            label: '假冒品牌',
        }, {
            value: 'time',
            label: '未按约定时间发货',
        }, {
            value: 'invoice',
            label: '发票问题',
        }];
        if(this.props.current == 0){
            return(
                <div className="MyRefundBoxOne">
                    <p>{I18n.t('return.Applyrefund')}</p>
                    <div className="MyRefundBoxOneInner">
                        <p>
                            <span>{I18n.t('return.amount')}</span>
                            <Input id="amount"/>
                        </p>
                        <p>
                            <span>{I18n.t('return.reason')}</span>
                            <Cascader options={options} onChange={this.onChange.bind(this)} placeholder={I18n.t('return.search')} />
                        </p>
                        <p>
                            <span className="left">{I18n.t('return.Explain')}</span>
                            <Input id='explain' max={300} className="left" type="textarea" placeholder={I18n.t('return.max')} />
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left">{I18n.t('return.document')}</span>
                            <div className="left">
                                <Photo setList={this.setList.bind(this)} flag={false} />
                                <div className="MyRefundBoxOneworm">
                                    <p>{I18n.t('buyer.worm')}</p>
                                    <p>
                                        <div className="left">
                                            <img src={(config.theme_path + 'origin.png')} alt=""/>
                                        </div>
                                        <div className="left">
                                            {I18n.t('return.pic')}
                                        </div>
                                        <div className="clear"></div>
                                    </p>
                                    <p>
                                        <div className="left">
                                            <img src={(config.theme_path + 'origin.png')} alt=""/>
                                        </div>
                                        <div className="left">
                                            {I18n.t('return.type')}
                                        </div>
                                        <div className="clear"></div>
                                    </p>
                                </div>
                            </div>
                            <div className="clear"></div>
                        </p>
                        <Button  type="primary" onClick={this.handleSubmit.bind(this)} > {I18n.t('Addresspage.sure')} </Button>
                    </div>
                </div>
            )
        }else if(this.props.current == 1){
            return(
                <div  className="MyRefundBoxTwo">
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>{I18n.t('return.success')}</span>
                    </p>
                    <p>{I18n.t('return.wait')}</p>
                    <p>{I18n.t('return.words')}</p>
                    <Button type="ghost" onClick={this.props.modifyApply}>{I18n.t('return.chack')}</Button>
                </div>
            )
        }else if(this.props.current == 2){
            return(
                this.ShowResult()
            )
        }
    }
    ShowResult(){
        if(this.props.order.return_fund.agreed){ //成功
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>{I18n.t('return.Refundsuccess')}</span>
                    </p>
                    <p>{I18n.t('return.number')}</p>
                    <p>{typeof this.props.order.update_time_problem != 'object'?'':(this.props.order.update_time_problem.getFullYear()+'-'+(this.props.order.update_time_problem.getMonth()+1)+'-'+this.props.order.update_time_problem.getDate())}</p>
                    <p>{I18n.t('return.money')} <span style={{color:'#0c5aa2'}}>{this.props.order.currency == 1?'￥':'$'}{this.props.order.return_fund.amount}</span> </p>
                </div>
            )
        }else if (!this.props.order.return_fund.agreed){
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img src={(config.theme_path + 'rederror.png')} alt=""/>
                        <span>{I18n.t('return.Refunderror')}</span>
                    </p>
                    <p>{I18n.t('BuyerBox.errorreson')} <span>{this.props.order.return_fund.refused_reason}</span> </p>

                    <p>{typeof this.props.order.update_time_problem != 'object'?'':(this.props.order.update_time_problem.getFullYear()+'-'+(this.props.order.update_time_problem.getMonth()+1)+'-'+this.props.order.update_time_problem.getDate())}</p>
                    <p>备注说明:<span style={{color:'#0c5aa2'}}>{this.props.order.return_fund.addition}</span> </p>
                </div>
            )
        }
    }
    render(){
        return(
            <div className="MyRefundBox">
                {this.show()}
            </div>
        )
    }
}
