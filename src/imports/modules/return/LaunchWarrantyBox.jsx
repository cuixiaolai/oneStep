import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router';
import {  Form,Upload,Cascader,Checkbox,Button,Input } from 'antd';
import config from '../../config.json';
import PaymentMoneyMethod from '../payment/PaymentMoneyMethod.jsx';
const FormItem = Form.Item;

 class LaunchWarrantyBox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            word:'',
            address:'',
            water:'',
            wateraddress:'',
            quality_assurance_agency:[{
                value: 'zhejiang',
                label: '北航'
            }],
            rules_agreed:false,
            delivery_company:[{
                value: 'yunda',
                label: '韵达快递',
            }]
        });
    }
    buyerDeliveryQualityAssurance(){
        const delivery_number = this.props.form.getFieldValue('delivery_number');
        this.props.handleBuyerDeliveryQualityAssurance(this.state.delivery_company[0].label,delivery_number);
    }
    buyerQualityAssurance(){
        if(this.state.rules_agreed){
            this.props.handleBuyerQualityAssurance(this.state.quality_assurance_agency[0].label);
        }else{
            console.log("must agree the rules");
        }
    }
    changeDelivery(value, label){
        this.setState({
            delivery_company:label
        });
    }
    onChange(value, label){
        this.setState({
            quality_assurance_agency:label
        });
    }
    onChangeCheckbox(e) {
        this.setState({
            rules_agreed:e.target.checked
        });
    }
    handleChange(info) {
        console.log(info.file, info.fileList);
        let word=info.file.name;
        this.setState({word:word});
        if (info.file.status === 'done') {
            let path = info.file.response[0].path;
            path = path.replace(/public/, '');
            this.setState({address:path});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    }
    handleChangeWater(info) {
        console.log(info.file, info.fileList);
        let word=info.file.name;
        this.setState({water:word});
        if (info.file.status === 'done') {
            let path = info.file.response[0].path;
            path = path.replace(/public/, '');
            this.setState({wateraddress:path});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    }
    ShowLaunchWarrantyBoxOne(){
        const options = [{
            value: 'zhejiang',
            label: '北航',
        }, {
            value: 'jiangsu',
            label: '南航',
        }];
        const propu ={
            name: 'file',
            showUploadList: false,
            action:config.file_server + 'product',
            onChange:this.handleChange.bind(this),
            beforeUpload(file){
                let isJPG;
                if(file.size>4000000){
                    isJPG = false;
                    message.error('上传文件不可超过4M！');
                }
                return isJPG;
            }
        };
        return(
            <div className="LaunchWarrantyBoxOneInner">
                <p>
                    <span>{I18n.t('qualityassurance.Agency')}</span>
                    <Cascader options={options} onChange={this.onChange.bind(this)} defaultValue={['zhejiang']} />
                </p>
                <div className="LaunchWarrantyBoxAgreement">

                </div>
                <Checkbox onChange={this.onChangeCheckbox.bind(this)}>{I18n.t('Register.read')} <Link to="#">{I18n.t('qualityassurance.Ordinance')}</Link> </Checkbox>
                <p className="LaunchWarrantyBoxUpload">
                    <span className="left">{I18n.t('qualityassurance.upload')}</span>
                    <div className="left">
                        {this.state.word}
                        <Upload {...propu}>
                            <div className="LaunchWarrantyBoxUploadBtn">
                                {I18n.t('search')}
                            </div>
                        </Upload>
                    </div>
                    <div className="clear"></div>
                    <span>{I18n.t('qualityassurance.Template')}</span>
                </p>
                <Button onClick={this.buyerQualityAssurance.bind(this)} type="primary">{I18n.t('qualityassurance.Confirmation')}</Button>
            </div>
        )
    }

    ShowLaunchWarrantyBoxTwo(){
        // 上传银行汇款回单
        // const propu ={
        //     name: 'file',
        //     showUploadList: false,
        //     action:config.file_server + 'product',
        //     onChange:this.handleChangeWater.bind(this),
        //     beforeUpload(file){
        //         let isJPG;
        //         if(file.size>4000000){
        //             isJPG = false;
        //             message.error('上传文件不可超过4M！');
        //         }
        //         return isJPG;
        //     }
        // };
        return(
            <div className="LaunchWarrantyBoxTwo" >
                <p>{I18n.t('return.payment')}：</p>
                <div className="LaunchWarrantyBoxTwoInner">
                    <span className="left">{I18n.t('qualityassurance.inspectionfee')}</span>
                    <span className="right" style={{color:'#0c5aa2',textAlign:'right',cursor:'pointer'}}>{I18n.t('qualityassurance.View')} <img style={{transform:'rotate(-90deg)'}} src={(config.theme_path + 'down.png')} alt=""/> </span>
                    <span className="clear"></span>
                    <div className="paymentMoney">
                        <PaymentMoneyMethod type='buyer_quality_assurance_payment'
                                            order_id={this.props.order._id}
                        />
                    </div>
                </div>
            </div>
        )
        // 上传银行汇款回单
        // return(
        //     <div className="LaunchWarrantyBoxTwo" >
        //         <div className="LaunchWarrantyBoxTwoInner">
        //             <span className="right"  style={{color:'#0c5aa2',textAlign:'right',cursor:'pointer'}}>{I18n.t('qualityassurance.View')} <img style={{transform:'rotate(-90deg)'}} src={(config.theme_path + 'down.png')} alt=""/> </span>
        //             <span className="clear"></span>
        //             <p className="LaunchWarrantyBoxUpload">
        //                 <span className="left">{I18n.t('qualityassurance.water')}</span>
        //                 <span></span>
        //                 <div className="left">
        //                     {this.state.water}
        //                     <Upload {...propu}>
        //                         <div className="LaunchWarrantyBoxUploadBtn">
        //                             {I18n.t('search')}
        //                         </div>
        //                     </Upload>
        //                 </div>
        //                 <div className="clear"></div>
        //             </p>
        //         </div>
        //     </div>
        // )
    }
    BigShow(){
        if(this.props.card == 'buyer'){
            return(
                this.show()
            )
        }else if(this.props.card == 'seller'){
            return(
                this.shows()
            )
        }
    }
    BuyerDown(){
        return(
            <span> <Button  type="ghost">{I18n.t('download')}</Button>  <Button  type="ghost">{I18n.t('CompantInvoicsThree.look')}</Button> </span>
        )
    }
    shows(){
        console.log(this.props.current);
        if(this.props.current == 1){
            return(
                <div className="LaunchWarrantyBoxOne">
                    <p>{I18n.t('TopName.buyerzhi')}</p>
                    <div className="LaunchWarrantyBoxOneInner">
                        <div>
                            <img style={{width:28,}} src={(config.theme_path + 'bigright_03.png')} alt=""/>
                            <span style={{display:'inline-block'}}>{I18n.t('qualityassurance.success')}</span>
                        </div>
                        <p style={{marginLeft:230,marginTop:16}}>{I18n.t('qualityassurance.buyerwait')}</p>
                        <p style={{marginLeft:230,marginTop:40}} className="BuyerDown">
                            <span>{I18n.t('BuyerBox.inspection')}</span>
                            {this.BuyerDown()}
                        </p>

                    </div>
                </div>
            )
        }else if(this.props.current == 2){
            return(
                <div className="LaunchWarrantyBoxOneInner">
                    <p style={{marginLeft:292,marginTop:16}}>正在等待买家付款</p>
                </div>
            )
        }else if(this.props.current == 3){
            return(
                <div className="LaunchWarrantyBoxOneInner">
                    <p style={{marginLeft:292,marginTop:16}}>正在等待买家发货</p>
                </div>
            )
        }else if(this.props.current == 4){
            return(
                <div className="LaunchWarrantyBoxOne">
                    <p>{I18n.t('return.Inspection')}：</p>
                    <div className="LaunchWarrantyBoxOneInner">
                        <div>
                            <img style={{width:28,}} src={(config.theme_path + 'watch.png')} alt=""/>
                            <span style={{display:'inline-block'}}>{I18n.t('qualityassurance.inspectioning')}</span>
                        </div>
                        <p style={{marginLeft:230,marginTop:16}}>{I18n.t('qualityassurance.buuyerdoing')}</p>
                        <p style={{marginLeft:230,marginTop:40}} className="BuyerDown">
                            <span>{I18n.t('BuyerBox.inspection')}</span>
                            {this.BuyerDown()}
                        </p>
                    </div>
                </div>
            )
        }else if (this.props.current == 5){
            return(
                <div className="LaunchWarrantyBoxThree" >
                    <p>{I18n.t('qualityassurance.results')}</p>
                    <div className="LaunchWarrantyBoxOneInner">
                        <span className="right" style={{color:'#0c5aa2',textAlign:'right',cursor:'pointer'}}>{I18n.t('qualityassurance.View')} <img style={{transform:'rotate(-90deg)'}} src={(config.theme_path + 'down.png')} alt=""/> </span>
                        <span className="clear"></span>
                        <div>
                            <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                            <span style={{display:'inline-block'}}>{I18n.t('qualityassurance.Appraisal')}</span>
                        </div>
                        <ul>
                            <li>
                                <span>{I18n.t('qualityassurance.unit')}</span>
                                <span>{I18n.t('qualityassurance.zhong')}</span>
                            </li>
                            <li>
                                <span>{I18n.t('qualityassurance.Quality')}</span>
                                <span>￥1500.00</span>
                            </li>
                            <li className="BuyerDown">
                                <span>{I18n.t('qualityassurance.results')}</span>
                                {this.BuyerDown()}
                            </li>
                            <li>
                                <span>{I18n.t('qualityassurance.platform')}</span>
                                <span>{I18n.t('qualityassurance.false')}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            )
        }
    }
    show(){
        if(this.props.current == 0){
            return(
                <div className="LaunchWarrantyBoxOne">
                    <p>{I18n.t('TopName.Submit')}</p>
                    {this.ShowLaunchWarrantyBoxOne()}
                </div>
            )
        }else if(this.props.current == 1){
            return(
                <div className="LaunchWarrantyBoxOneInner">
                    <div>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>{I18n.t('qualityassurance.success')}</span>
                    </div>
                    <p style={{marginLeft:292,marginTop:16}}>{I18n.t('qualityassurance.wait')}</p>
                </div>
            )
        }else if(this.props.current == 2){
            return(
                this.ShowLaunchWarrantyBoxTwo()
            )
        }else if(this.props.current == 3){
            const { getFieldProps} = this.props.form;
            const deliveryNumberProps = getFieldProps('delivery_number');
            const options = [{
                value: 'yunda',
                label: '韵达快递',
            }, {
                value: 'yuantong',
                label: '圆通速递',
            }, {
                value: 'zhongtong',
                label: '中通快递',
            }, {
                value: 'baishi',
                label: '百世快递',
            }, {
                value: 'youzheng',
                label: '邮政包裹',
            }, {
                value: 'tiantian',
                label: '天天快递',
            }, {
                value: 'shunfeng',
                label: '顺风速递',
            }, {
                value: 'EMS',
                label: 'EMS',
            }];
            return(
                <div className="LaunchWarrantyBoxThree" >
                    <p>{I18n.t('qualityassurance.inspection')}：</p>
                    <div className="LaunchWarrantyBoxThreeInner">
                        <span className="right" style={{color:'#0c5aa2',textAlign:'right',cursor:'pointer'}}>{I18n.t('qualityassurance.View')} <img style={{transform:'rotate(-90deg)'}} src={(config.theme_path + 'down.png')} alt=""/> </span>
                        <span className="clear"></span>
                        <Form inline>
                            <FormItem
                                label={I18n.t('qualityassurance.email')}
                            >
                                <span>{I18n.t('OrderConfirm.company')}</span>
                            </FormItem>
                            <FormItem
                                label={I18n.t('qualityassurance.logistics')}
                            >
                                <Cascader options={options} onChange={this.changeDelivery.bind(this)} defaultValue={['yunda']} />
                            </FormItem>
                            <FormItem
                                label={I18n.t('qualityassurance.express')}
                            >
                                <Input {...deliveryNumberProps}/>
                            </FormItem>
                            <FormItem>
                                <Button onClick={this.buyerDeliveryQualityAssurance.bind(this)} type="primary" > {I18n.t('qualityassurance.sure')} </Button>
                            </FormItem>
                        </Form>
                    </div>
                </div>
            )
        }else if(this.props.current == 4){
            return(
                <div className="LaunchWarrantyBoxOne">
                    <p>{I18n.t('qualityassurance.inspection')}：</p>
                    <div className="LaunchWarrantyBoxOneInner">
                        <div>
                            <img style={{width:28,}} src={(config.theme_path + 'watch.png')} alt=""/>
                            <span style={{display:'inline-block'}}>{I18n.t('qualityassurance.inspectioning')}</span>
                        </div>
                        <p style={{marginLeft:230,marginTop:16}}>{I18n.t('qualityassurance.doing')}</p>
                    </div>
                </div>
            )
        }else if(this.props.current == 5){
            return(
                <div className="LaunchWarrantyBoxThree" >
                    <p>{I18n.t('qualityassurance.results')}</p>
                    <div className="LaunchWarrantyBoxOneInner">
                        <span className="right" style={{color:'#0c5aa2',textAlign:'right',cursor:'pointer'}}>{I18n.t('qualityassurance.View')} <img style={{transform:'rotate(-90deg)'}} src={(config.theme_path + 'down.png')} alt=""/> </span>
                        <span className="clear"></span>
                        <div>
                            <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                            <span style={{display:'inline-block'}}>{I18n.t('qualityassurance.Appraisal')}</span>
                        </div>
                        <ul>
                            <li>
                                <span>{I18n.t('qualityassurance.unit')}</span>
                                <span>{I18n.t('qualityassurance.zhong')}</span>
                            </li>
                            <li>
                                <span>{I18n.t('qualityassurance.Quality')}</span>
                                <span>￥1500.00</span>
                            </li>
                            <li className="BuyerDown">
                                <span>{I18n.t('qualityassurance.results')}</span>
                                {this.BuyerDown()}
                            </li>
                            <li>
                                <span>{I18n.t('qualityassurance.platform')}</span>
                                <span>{I18n.t('qualityassurance.false')}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            )
        }
    }
    render(){
        return(
            this.BigShow()
        )
    }
}
LaunchWarrantyBox = Form.create()(LaunchWarrantyBox);
export default LaunchWarrantyBox;