import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
import { } from 'antd';
import { Modal, Button,Radio ,Cascader,Icon ,Input,Select } from 'antd';
const Option = Select.Option;

export default class AlertMessageBox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            flag:false,
            value:'',
            text:'',
        });
    }
    componentDidMount(){
        let words = [];
        words.push(this.props.options[0]);
        this.setState({value:words[0]});
    }
    onChange(value){
        console.log(value);
        this.setState({value:value});
    }
    getMessage(){
        let AlertMessageBoxInnerinput = document.getElementsByClassName('AlertMessageBoxInnerinput')[0];
        console.log(AlertMessageBoxInnerinput.value);
        this.props.getMessage(this.state.value,AlertMessageBoxInnerinput.value);
    }
    render(){
        const provinceOptions = this.props.options.map(province => <Option key={province}>{province}</Option>);
        return(
            <div className="AlertMessageBox"  style={this.props.show ? {display:'block'} : {display:'none'}}>
                <div className="AlertMessageBoxInner">
                    <p style={{textAlign:'right'}}> <span style={{cursor:'pointer'}} onClick={this.props.Close}>×</span> </p>
                    <p style={{marginLeft:50,fontSize:20}}>
                        <Icon style={{fontSize:28,color:'#fab53e',marginRight:10}} type="exclamation-circle" />
                        {I18n.t('alertmessage.name')}
                    </p>
                    <p style={{marginLeft:88,color:'#999',marginTop:5}}>{I18n.t('alertmessage.after')}</p>
                    <div style={{marginLeft:88,marginTop:30}} >
                       <span>{I18n.t('refusereson')}</span>
                        <Select defaultValue={this.props.options[0]} style={{ width: 180 }} onChange={this.onChange.bind(this)}>
                            {provinceOptions}
                        </Select>
                    </div>
                    <div style={{marginLeft:88,marginTop:10}}>
                        <span className="left">{I18n.t('Remarkreson')}</span>
                        <Input className="left AlertMessageBoxInnerinput" type="textarea" maxLength={200} placeholder={I18n.t('return.twomax')} />
                        <div className="clear"></div>
                    </div>
                    <div className="AlertMessageBoxInnerbtn">
                        <Button  onClick={this.props.Close} type="primary">{I18n.t('PersonalCenter.company.no')}</Button> <Button type="ghost" onClick={this.getMessage.bind(this)} >{I18n.t('PersonalCenter.company.yes1')}</Button>
                    </div>
                </div>
            </div>
        )
    }
}