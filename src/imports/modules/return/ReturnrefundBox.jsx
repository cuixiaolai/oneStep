import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router';
import config from '../../config.json';
import ReturnRefundOne from './ReturnRefundOne.jsx';
import ReturnRefundSuccess from './ReturnRefundSuccess.jsx';
import { Button,Input,Cascader, Modal } from 'antd';
const confirm = Modal.confirm;
import AlertMessageBox from './AlertMessageBox.jsx';
export default class ReturnrefundBox extends Component{
    constructor(props) {
        super(props);
        this.state =({
            flag:false,
            value:'',
            num:2,
            show:false,
        })
    }
    ShowCard(){
        if (this.props.card == 'buyer'){
            return(
                this.show()
            )
        }else if (this.props.card == 'seller'){
            return(
                this.Sellers()
            )
        }
    }
    ShowSure(){
        confirm({
            title:I18n.t('BuyerBox.confirmreceipt'),
            content: I18n.t('BuyerBox.sure'),
            okText:I18n.t('PersonalCenter.company.yes'),
            cancelText:I18n.t('PersonalCenter.manageStock.cancel'),
            onOk() {

            },
            onCancel() {},
        });
    }
    onChange(value, label){
        this.setState({value:label});
    }
    Open(){
        this.setState({flag:true});
    }
    Close(){
        this.setState({flag:false});
    }
    CloseWin(){
        this.setState({show:false});
    }
    Openwin(){
        this.setState({show:true});
        console.log(111);
    }
    GetMessage(value,text){
        this.setState({show:false});
        console.log(value,text);
        this.props.handleSellerReturnGoods(false,value,text);
    }
    SellerReturnGoods(agreed){
        if(agreed){
            this.props.handleSellerReturnGoods(agreed,'','');
        }else{
            const addition = document.getElementById('addition').value;
            this.props.handleSellerReturnGoods(agreed,this.state.value[0].label,addition);
        }
    }
    Sellers(){
        if(this.props.current == 1){
            const options = [{
                value: 'all',
                label: '不能退还全款',
            }, {
                value: 'noreson',
                label: '货物所说问题不存在',
            }, {
                value: 'other',
                label: '其它',
            }];
            return(
                <div className="MyRefundBoxOne">
                    <p>{I18n.t('BuyerBox.Processing')}</p>
                    <div className="MyRefundBoxOneInner">
                        <div className="left BuyerBorderRadius">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                        <div className="left">
                            <div className="MyRefundBoxOneInnerBuyer">
                                <div className="left">
                                    <span>{I18n.t('BuyerBox.buyer')}</span>
                                </div>
                                <div className="left">
                                    <p>{I18n.t('return.amount')} <span>{this.props.order.return_goods.amount}</span> </p>
                                    <p>{I18n.t('return.reason')} <span>{this.props.order.return_goods.return_reason}</span></p>
                                    <p>{I18n.t('return.Explain')} <span>{this.props.order.return_goods.explain}</span></p>
                                    <p>{I18n.t('return.document')} <span></span></p>
                                </div>
                                <div className="clear"></div>
                            </div>
                            <div className="MyRefundBoxOneInnerBuyer">
                                <div className="left">
                                    <span>{I18n.t('BuyerBox.seller')}</span>
                                </div>
                                <div className="left">
                                    <Button type="primary" onClick={this.SellerReturnGoods.bind(this,true)} >{I18n.t('Agree')}</Button>
                                    <Button onClick={this.Open.bind(this)} type="ghost">{I18n.t('refuse')}</Button>
                                </div>
                                <div className="MyRefundBoxrefuse" style={this.state.flag ? {display:'block'} : {display:'none'}}>
                                    <span className="right" onClick={this.Close.bind(this)} style={{marginRight:0,cursor:'pointer'}}>×</span>
                                    <div className="clear"></div>
                                    <p>
                                        <span >{I18n.t('refusereson')}</span>
                                        <Cascader options={options} onChange={this.onChange.bind(this)} placeholder={I18n.t('buyerForm.searchp')} />
                                    </p>
                                    <p>
                                        <span className="left">{I18n.t('Remarkreson')}</span>
                                        <Input id='addition' type="textarea" max={200}  placeholder={I18n.t('return.twomax')} className="left"/>
                                        <div className="clear"></div>
                                    </p>
                                    <p>
                                        <Button onClick={this.SellerReturnGoods.bind(this,false)} type="primary">{I18n.t('Addresspage.sure')}</Button>
                                        <Button onClick={this.Close.bind(this)} type="ghost">{I18n.t('PersonalCenter.manageStock.cancel')}</Button>
                                    </p>
                                </div>
                                <div className="clear"></div>
                            </div>
                        </div>
                        <div className="clear"></div>
                    </div>
                </div>
            )
        }else if(this.props.current == 2){
            return(
                <div  className="MyRefundBoxTwo">
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>{I18n.t('return.success')}</span>
                    </p>
                    <p>{I18n.t('return.wait')}</p>
                    <p>{I18n.t('return.words')}</p>
                </div>
            )
        }else if(this.props.current == 3){
            const options = [ '其它', '不能退换全款','商品不存在问题','此商品不能退换'];
            return(
                <div className="MyRefundBoxOne">
                    <p>{I18n.t('BuyerBox.Processing')}</p>
                    <div className="MyRefundBoxOneInner">
                        <div className="MyRefundBoxOneInnerBuyer">
                            <div className="left">
                                <span>{I18n.t('BuyerBox.buyer')}</span>
                            </div>
                            <div className="left">
                                <p>{I18n.t('logisticsdetails.company')} <span>{this.props.order.return_goods.delivery_company}</span> </p>
                                <p>{I18n.t('logisticsdetails.number')} <span>{this.props.order.return_goods.delivery_number}</span></p>
                                <p>{I18n.t('return.Explain')} <span>{this.props.order.return_goods.explain}</span></p>
                                <p>{I18n.t('return.document')} <span></span></p>
                            </div>
                            <div className="clear"></div>
                        </div>
                        <div className="MyRefundBoxOneInnerBuyer">
                            <div className="left">
                                <span>{I18n.t('BuyerBox.seller')}</span>
                            </div>
                            <div className="left">
                                <Button style={{width:110}} onClick={this.SellerReturnGoods.bind(this,true)} type="primary">{I18n.t('BuyerBox.Receiving')}</Button>
                                <Button style={{width:90}} onClick={this.Openwin.bind(this)} type="ghost">{I18n.t('BuyerBox.refuse')}</Button>
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>
                    <AlertMessageBox options={options} show={this.state.show} Close={this.CloseWin.bind(this)}  getMessage={this.GetMessage.bind(this)} />
                </div>
            )
        }else if(this.props.current == 4){
            return(
                this.ShowResult()
            )
        }
    }
    ShowResult(){
        if(this.props.order.return_goods.agreed){ //成功
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>已同意退货</span>
                    </p>
                    <p>退款金额已退回至买家的账户</p>
                    <p>{typeof this.props.order.update_time_problem != 'object'?'':(this.props.order.update_time_problem.getFullYear()+'-'+(this.props.order.update_time_problem.getMonth()+1)+'-'+this.props.order.update_time_problem.getDate())}</p>
                    <p>{I18n.t('return.money')} <span style={{color:'#0c5aa2'}}>{this.props.order.currency == 1?'￥':'$'}{this.props.order.return_goods.amount}</span> </p>
                </div>
            )
        }else if (!this.props.order.return_goods.agreed){
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>已拒绝退货</span>
                    </p>
                    <p>拒绝原因<span>{this.props.order.return_goods.refused_reason}</span> </p>
                    <p>{typeof this.props.order.update_time_problem != 'object'?'':(this.props.order.update_time_problem.getFullYear()+'-'+(this.props.order.update_time_problem.getMonth()+1)+'-'+this.props.order.update_time_problem.getDate())}</p>
                    <p>备注说明:<span style={{color:'#0c5aa2'}}>{this.props.order.return_goods.addition}</span> </p>
                </div>
            )
        }
    }
    show(){
        if(this.props.current == 0){
            return(
                <div>
                    <ReturnRefundOne current={this.props.current}
                                     title={I18n.t('return.returnrefund.application')}
                                     handleBuyerReturnGoods={this.props.handleBuyerReturnGoods}
                                     order={this.props.order}
                    />
                </div>
            )
        }else if(this.props.current == 1){
            return(
                <div className="returnrefund2">
                    <img src={(config.theme_path+'success.png')} alt="图片"/>
                    <h2>{I18n.t('return.returnrefund.success')} </h2>
                    <h3>{I18n.t('return.returnrefund.wait')}</h3>
                    <p>{I18n.t('return.returnrefund.content')}</p>
                    <span onClick={this.props.modifyApply}>修改退货申请</span>
                    <span onClick={this.props.handleBuyerCancelReturnGoods}>取消退货申请</span>
                </div>
            )
        }else if(this.props.current == 2){
            return(
                <div>
                    <ReturnRefundOne current={this.props.current} title={I18n.t('return.returnrefund.application')} handleBuyerDeliverReturnGoods={this.props.handleBuyerDeliverReturnGoods}/>
                </div>
            )
        }else if(this.props.current == 3){
            return(
                <div className="returnrefund2">
                    等待卖家处理
                </div>
            )
        }else if(this.props.current == 4){
            return(
                <div>
                    <ReturnRefundSuccess order={this.props.order}/>
                </div>
            )
        }
    }
    render(){
        return(
            <div className="MyRefundBox">
                {this.ShowCard()}
            </div>

        )
    }
}