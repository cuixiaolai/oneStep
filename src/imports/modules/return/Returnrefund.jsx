import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router';
import { Steps, Button } from 'antd';
const Step = Steps.Step;
import config from '../../config.json';
import ReturnrefundBox from './ReturnrefundBox.jsx';

const array = Array.apply(null, [5,6,7,8]);
const steps = array.map((item,i) => ({
    title: I18n.t('Register.top.one')
}));

export default class Returnrefund extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            style: 112,
        });
    }
    componentDidMount(){
        this.setState({
            style:this.state.style + 190*this.props.current
        })
    }
    render(){
        return (
            <div className="perPhoneregister Returnrefund">
                <Steps current={this.props.current}>
                    <Step title="申请退货"  key="1" />
                    <Step title={I18n.t('return.busines')}  key="2" />
                    <Step title="等待买家发货"  key="3" />
                    <Step title="等待卖家处理"  key="4" />
                    <Step title="退货处理完成"  key="5" />
                    <div className="bigXian"></div>
                    <img  src={(config.theme_path + '/center.png')} alt="" ref="picShaw" style={{left:this.state.style+'px',width:54}}  />
                </Steps>
                <ReturnrefundBox card={this.props.card}
                                 current={this.props.current}
                                 handleBuyerReturnGoods={this.props.handleBuyerReturnGoods}
                                 handleBuyerDeliverReturnGoods={this.props.handleBuyerDeliverReturnGoods}
                                 handleSellerReturnGoods={this.props.handleSellerReturnGoods}
                                 order={this.props.order}
                                 modifyApply={this.props.modifyApply}
                                 handleBuyerCancelReturnGoods={this.props.handleBuyerCancelReturnGoods}
                />
            </div>
        )
    }
}