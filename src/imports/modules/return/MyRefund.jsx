import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router';
import { Steps, Button,Input,Cascader, Modal } from 'antd';
const Step = Steps.Step;
import config from '../../config.json';
const confirm = Modal.confirm;
import MyRefundBox from './MyRefundBox.jsx';
const array = Array.apply(null, [5,6,7]);
const steps = array.map((item,i) => ({
    title: I18n.t('Register.top.one')
}));

export default class MyRefund extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            style: 150,
            num:1,
            value:'',
            flag:false,
        });
    }
    componentDidMount(){
        this.setState({
            style:this.state.style + 210*this.props.current
        })
    }
    ShowBox(){
        if (this.props.card == 'buyer'){
            return(
                <MyRefundBox current={this.props.current}
                             handleBuyerReturnFund={this.props.handleBuyerReturnFund}
                             order={this.props.order}
                             modifyApply={this.props.modifyApply}
                />
            )
        }else if (this.props.card == 'seller'){
            if( this.props.current == 1){
                const options = [{
                    value: 'all',
                    label: '不能退还全款',
                }, {
                    value: 'noreson',
                    label: '货物所说问题不存在',
                }, {
                    value: 'other',
                    label: '其它',
                }];
                return(
                    <div className="MyRefundBoxOne">
                        <p>{I18n.t('BuyerBox.Processing')}</p>
                        <div className="MyRefundBoxOneInner">
                            <div className="left BuyerBorderRadius">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                            <div className="left">
                                <div className="MyRefundBoxOneInnerBuyer">
                                    <div className="left">
                                        <span>{I18n.t('BuyerBox.buyer')}</span>
                                    </div>
                                    <div className="left">
                                        <p>{I18n.t('return.amount')} <span>{this.props.order.return_fund.amount}</span> </p>
                                        <p>{I18n.t('return.reason')} <span>{this.props.order.return_fund.return_reason}</span></p>
                                        <p>{I18n.t('return.Explain')} <span>{this.props.order.return_fund.explain}</span></p>
                                        <p>{I18n.t('return.document')} <span></span></p>
                                    </div>
                                    <div className="clear"></div>
                                </div>
                                <div className="MyRefundBoxOneInnerBuyer">
                                    <div className="left">
                                        <span>{I18n.t('BuyerBox.seller')}</span>
                                    </div>
                                    <div className="left">
                                        <Button onClick={this.SellerReturnFund.bind(this,true)}type="primary">{I18n.t('Agree')}</Button>
                                        <Button onClick={this.Open.bind(this)} type="ghost">{I18n.t('refuse')}</Button>
                                    </div>
                                    <div className="MyRefundBoxrefuse" style={this.state.flag ? {display:'block'} : {display:'none'}}>
                                        <span className="right" onClick={this.Close.bind(this)} style={{marginRight:0,cursor:'pointer'}}>×</span>
                                        <div className="clear"></div>
                                        <p>
                                            <span >{I18n.t('refusereson')}</span>
                                            <Cascader options={options} onChange={this.onChange.bind(this)} placeholder={I18n.t('buyerForm.searchp')} />
                                        </p>
                                        <p>
                                            <span className="left">{I18n.t('Remarkreson')}</span>
                                            <Input id='addition' type="textarea" max={200} placeholder={I18n.t('return.twomax')} className="left"/>
                                            <div className="clear"></div>
                                        </p>
                                        <p>
                                            <Button onClick={this.SellerReturnFund.bind(this,false)} type="primary">{I18n.t('Addresspage.sure')}</Button>
                                            <Button onClick={this.Close.bind(this)} type="ghost">{I18n.t('PersonalCenter.manageStock.cancel')}</Button>
                                        </p>
                                    </div>
                                    <div className="clear"></div>
                                </div>
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>
                )
            }else if (this.props.current == 2){
                return(
                    this. ShowResult()
                )
            }
        }
    }
    onChange(value, label){
        this.setState({value:label});
    }
    SellerReturnFund(agreed){
        if(agreed){
            this.props.handleSellerReturnFund(agreed,'','');
        }else{
            const addition = document.getElementById('addition').value;
            this.props.handleSellerReturnFund(agreed,this.state.value[0].label,addition);
        }
    }
    Open(){
        this.setState({flag:true});
    }
    Close(){
        this.setState({flag:false});
    }
    ShowResult(){
        if(this.props.order.return_fund.agreed){ //成功
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>已同意退款</span>
                    </p>
                    <p>退款金额已退回至买家的账户</p>
                    <p>{typeof this.props.order.update_time_problem != 'object'?'':(this.props.order.update_time_problem.getFullYear()+'-'+(this.props.order.update_time_problem.getMonth()+1)+'-'+this.props.order.update_time_problem.getDate())}</p>
                    <p>{I18n.t('return.money')} <span style={{color:'#0c5aa2'}}>{this.props.order.currency == 1?'￥':'$'}{this.props.order.return_fund.amount}</span> </p>
                </div>
            )
        }else if (!this.props.order.return_fund.agreed){
            return(
                <div className="MyRefundBoxThree" >
                    <p>
                        <img src={(config.theme_path + 'rederror.png')} alt=""/>
                        <span>已拒绝退款</span>
                    </p>
                    <p>拒绝原因：<span>{this.props.order.return_fund.refused_reason}</span> </p>

                    <p>{typeof this.props.order.update_time_problem != 'object'?'':(this.props.order.update_time_problem.getFullYear()+'-'+(this.props.order.update_time_problem.getMonth()+1)+'-'+this.props.order.update_time_problem.getDate())}</p>
                    <p>备注说明：<span style={{color:'#0c5aa2'}}>{this.props.order.return_fund.addition}</span> </p>
                </div>
            )
        }
    }
    render(){
        return (
            <div className="perPhoneregister">
                <Steps current={this.props.current}>
                    <Step title={I18n.t('return.Applyrefund')}  key="1" />
                    <Step title={I18n.t('return.business')}  key="2" />
                    <Step title={I18n.t('return.Refundcompleted')}  key="3" />
                    <div className="bigXian"></div>
                    <img  src={(config.theme_path + '/center.png')} alt="" ref="picShaw" style={{left:this.state.style+'px',width:54}}  />
                </Steps>
                
                {this.ShowBox()}
                
            </div>
        )

    }
}