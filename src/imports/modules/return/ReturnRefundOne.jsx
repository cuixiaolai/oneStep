import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router';
import { Input,Button,Cascader  } from 'antd';
import config from '../../config.json';
import Photo from '../Evaluation/Photo.jsx';

const options = [{
    value: 'size',
    label: '大小尺寸与商品插述不符',
}, {
    value: 'color',
    label: '颜色案式与商品插述不符',
}, {
    value: 'Fabric',
    label: '质面料与商品插述不符',
}, {
    value: 'small',
    label: '少发货 ',
}, {
    value: 'package',
    label: '包装商品损圬 ',
}, {
    value: 'counterfeit',
    label: '假冒品牌',
}, {
    value: 'time',
    label: '未按约定时间发货',
}, {
    value: 'invoice',
    label: '发票问题',
}];
const optionscompany = [{
    value: 'yunda',
    label: '韵达快递',
}, {
    value: 'yuantong',
    label: '圆通速递',
}, {
    value: 'zhongtong',
    label: '中通快递',
}, {
    value: 'baishi',
    label: '百世快递 ',
}, {
    value: 'youzheng',
    label: '邮政包裹 ',
}, {
    value: 'tiantian',
    label: '天天快递',
}, {
    value: 'shunfeng',
    label: '顺丰速递',
}, {
    value: 'ems',
    label: 'EMS',
}];
export default class ReturnRefundOne extends Component{
    constructor(props) {
        super(props);
        console.log(props);
        this.state = ({
            return_goods_reason:'',
            delivery_company:''
        });
    }
    onChange(value, label){
        this.setState(
            {
                return_goods_reason:label
            }
        )
    }
    onChangeCompany(value,label){
        this.setState(
            {
                delivery_company:label
            }
        )
    }
    setList(arr){
        console.log(arr);
    }
    showOne(){
        let temp = [];
        if(this.props.current == 0){
            temp[0]=(
                <p>
                    <span>退货原因：</span>
                    <Cascader options={options} onChange={this.onChange.bind(this)} placeholder={I18n.t('return.search')} />
                </p>
            )
        }else if(this.props.current == 2){
            temp[0]=(
                <p>
                    <span>{I18n.t('return.returnrefund.logisticscompany')}</span>
                    <Cascader options={optionscompany} onChange={this.onChangeCompany.bind(this)} placeholder={I18n.t('return.search')} />
                </p>
            )
        }
        return temp;
    }
    showTwo(){
        let temp = [];
        if(this.props.current == 0){
            temp.push(
                <p>
                    <span>{I18n.t('return.amount')}</span>
                    <Input id='amount'/>
                </p>
            );
            temp.push(
                <p>
                    <span className="left">退货说明：</span>
                    <Input id='explain' max={300} className="left" type="textarea" placeholder={I18n.t('return.max')} />
                    <div className="clear"></div>
                </p>
            );
            temp.push(
                <p>
                    <span className="left">{I18n.t('return.document')}</span>
                    <div className="left">
                        <Photo setList={this.setList.bind(this)} flag={false} />
                        <div className="MyRefundBoxOneworm">
                            <p>{I18n.t('buyer.worm')}</p>
                            <p>
                                <div className="left">
                                    <img src={(config.theme_path + 'origin.png')} alt=""/>
                                </div>
                                <div className="left">
                                    {I18n.t('return.pic')}
                                </div>
                                <div className="clear"></div>
                            </p>
                            <p>
                                <div className="left">
                                    <img src={(config.theme_path + 'origin.png')} alt=""/>
                                </div>
                                <div className="left">
                                    {I18n.t('return.type')}
                                </div>
                                <div className="clear"></div>
                            </p>
                        </div>
                    </div>
                    <div className="clear"></div>
                </p>
            );
        }else if(this.props.current == 2){
            temp[0]=(
                <p>
                    <span>{I18n.t('return.returnrefund.number')}</span>
                    <Input id='delivery_number' />
                </p>
            )
        }
        return temp;
    }
    show(){
        if(this.props.current == 0||this.props.current == 2){
            return(
                <div className="MyRefundBoxOne">
                    <p>{this.props.title}</p>
                    <div className="MyRefundBoxOneInner">
                        {this.showOne()}
                        {this.showTwo()}
                        <Button type="primary" onClick={this.buyerReturnGoods.bind(this)}> {I18n.t('Addresspage.sure')} </Button>
                    </div>
                </div>
            )
        }
    }
    buyerReturnGoods(){
        if(this.props.current == 0) {
            const amount = document.getElementById('amount').value;
            const explain = document.getElementById('explain').value;
            this.props.handleBuyerReturnGoods(this.state.return_goods_reason[0].label, amount, explain);
        }else if(this.props.current == 2){
            const delivery_number = document.getElementById('delivery_number').value;
            this.props.handleBuyerDeliverReturnGoods(this.state.delivery_company[0].label, delivery_number);
        }
    }
    render(){
        console.log(this.props.order);
        return(
            <div className="MyRefundBox">
                {this.show()}
            </div>
        )
    }
}
