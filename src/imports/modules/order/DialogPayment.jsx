import { Modal, Button,Radio ,Cascader} from 'antd';
import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
{/*取消订单弹窗*/}


export default class DialogPayment extends Component{
    constructor(props) {
        super(props);
        this.state = ({
          value:1,
            loading: false,
            visible: false,
            closed_reason:'信息有误，重新拍'
        });

    }
    componentDidMount(){

console.log(this.props.key);

    }
    onChange(e) {
        console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });
    }
    showModal() {
        this.setState({
            visible: true,
        });
    }
    handleOk(){
        {/*弹窗确认事件*/}
        Meteor.call('order.cancelOrder', this.props.order_id, this.state.closed_reason, (err) => {
            if (err) {

            } else {

            }
        });
        this.setState({
            visible: false,
        });
    }
    handleCancel() {
        this.setState({ visible: false });
    }
    onChangeMessage(value,label){
        console.log(value,label);
        this.setState({ closed_reason: label[0].label });
    }
    render() {
        const styles = {
            display: 'block',
            height: '30px',
            lineHeight: '30px',
        };
        const options = [{
            value: 'messageerror',
            label: '信息有误，重新拍',
        }, {
            value: 'color',
            label: '颜色案式与商品插述不符',
        }, {
            value: 'Fabric',
            label: '质面料与商品插述不符',
        }, {
            value: 'small',
            label: '少发货 ',
        }, {
            value: 'package',
            label: '包装商品损圬 ',
        }, {
            value: 'counterfeit',
            label: '假冒品牌',
        }, {
            value: 'time',
            label: '未按约定时间发货',
        }, {
            value: 'invoice',
            label: '发票问题',
        }];

        return (
            <div className={this.props.className} style={{display: 'inline-block'}}>
                <span onClick={this.showModal.bind(this)}  >
                    {this.props.content}
                </span>
                <Modal ref="modal"
                       visible={this.state.visible}
                       title="" onOk={this.handleOk.bind(this)} onCancel={this.handleCancel.bind(this)}
                       footer={[
            <Button  key="back" type="ghost" style={{height:32,width:72,marginRight:50,fontSize:14}} onClick={this.handleCancel.bind(this)}>{I18n.t('PersonalCenter.company.no')}</Button>,
            <Button className="yes" key="submit" type="primary" style={{height:32,width:72,marginRight:140,fontSize:14}} loading={this.state.loading} onClick={this.handleOk.bind(this)}>
                {I18n.t('PersonalCenter.company.yes1')}
            </Button>
          ]}
                >
                    <div className="paymentDialog"  style={{paddingRight:30,paddingLeft:138,paddingTop:0,fontSize:14}}>
                        <img src={(config.theme_path+'bigorigin.png')} alt="图片"/>
                        <p className="title">{this.props.words}</p>
                        <p className="cancelnotrestored">{I18n.t('shoppingCart.dialog.cancelnotrestored')}</p>
                        <div>
                            <span>{I18n.t('shoppingCart.dialog.cancelreason')}</span>
                            <Cascader options={options} onChange={this.onChangeMessage.bind(this)} placeholder={I18n.t('shoppingCart.dialog.messageerror')} />
                        </div>
                     </div>
                </Modal>
            </div>
        );
    }
}