import React, { Component } from 'react';
import { Select, Dropdown, DatePicker, Modal, Button ,Input ,Menu, Icon,Form ,Tabs, Col ,Checkbox ,Steps} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
/*我的订单-物流详情*/

const styles =({
    one: {
        backgroundImage: 'url(' + config.theme_path + 'logistics_bg3.png)',
        // width: 145,
        // height: 45,
    },
    two: {
        backgroundImage: 'url(' + config.theme_path + 'logistics_bg2.png)',
    },
    three: {
        backgroundImage: 'url(' + config.theme_path + 'logistics_bg4.png)',
    },
   
});



export default class LogisticsDetailsContent extends Component {
    constructor(props) {
        super(props);
        this.state = ({

            data:[{
                company:'邦德速递',
                number:'10004256780900',
                faddress:'阿尔法贝特科技有限公司',
                saddress:'浙江省点开市提升区都浪费空间街道8号。     屋顶花 13278903362',
                message1:[{day:'2016-10-02（周二）',
                    time:'09：50：33',
                    stat:'[淮安市]淮安洪泽水产大市场营业点 已发出'},
                    {day:'',
                        time:'09：50：33',
                        stat:'[淮安市]淮安洪泽水产大市场营业点 已发出'},
                    {day:'',
                        time:'09：50：33',
                        stat:'[淮安市]淮安洪泽水产大市场营业点 已发出'},
                    {day:'2016-10-01（周一）',
                        time:'09：50：33',
                        stat:'[淮安市]淮安洪泽水产大市场营业点 已发出'}
                ],
            }],
        });

    }
    showTrend(){
        let temp = [];
        temp [0] = (
            <div className="trend">
                <span >{I18n.t('logisticsdetails.home')}></span>
                <span>{I18n.t('logisticsdetails.personalcenter')}></span>
                <span>{I18n.t('logisticsdetails.order')}></span>
                <span className="active">{I18n.t('logisticsdetails.details')}</span>
            </div>
        )

        return temp;
    }
    showMessage(){
        let temp = [];
        let i
        for(i =this.state.data[0].message1.length-1;  i > 0 ;i--){
            if(this.state.data[0].message1[i].day != ''){
                temp[i] = (
                    <div className="message message3">
                        <p className="day">{this.state.data[0].message1[i].day}</p>
                        <img src={config.theme_path+'circlemark3.png'} alt="图片"/>
                        <p  className="time">{this.state.data[0].message1[i].time}</p>
                        <p className="stat">{this.state.data[0].message1[i].stat}</p>
                    </div>
                )
            }else
            temp[i] = (
                <div className="message">
                    <p className="day">{this.state.data[0].message1[i].day}</p>
                    <img src={config.theme_path+'circlemark2.png'} alt="图片"/>
                    <p  className="time">{this.state.data[0].message1[i].time}</p>
                    <p className="stat">{this.state.data[0].message1[i].stat}</p>
                </div>
            )

        }
        temp[i] = (
            <div className="message new">
                <p className="day">{this.state.data[0].message1[i].day}</p>
                <img src={config.theme_path+'circlemark1.png'} alt="图片"/>
                <p className="time">{this.state.data[0].message1[i].time}</p>
                <p className="stat">{this.state.data[0].message1[i].stat}</p>
            </div>
        )
        return temp;
    }
    showTrack(){
        let temp = [];
        temp [0] = (
            <div className="track">
                <h3>{I18n.t('logisticsdetails.trank')}</h3>
                <span style={1==1?styles.one:styles.two} className="home span_">{I18n.t('logisticsdetails.pendingpackage')}</span>
                <span style={styles.one} className="home">{I18n.t('logisticsdetails.indelivery')}</span>
                <span style={styles.three} className="home">{I18n.t('logisticsdetails.delivery')}</span>
                <span style={styles.two} className="sign">{I18n.t('logisticsdetails.sign')}</span>
                <div className="messages">
                    {this.showMessage()}
                </div>
            </div>
        )

        return temp;
    }
    showDetailsMessage(){
        let temp = [];
        temp [0] = (
            <div className="detailsmessage">
                <h3>{I18n.t('logisticsdetails.detailsmessage')}</h3>
                <p>{I18n.t('logisticsdetails.company')}{this.state.data[0].company}</p>
                <p>{I18n.t('logisticsdetails.number')}{this.state.data[0].number}</p>
                <p>{I18n.t('logisticsdetails.faddress')}{this.state.data[0].faddress}</p>
                <p>{I18n.t('logisticsdetails.saddress')}{this.state.data[0].saddress}</p>
            </div>
        )

        return temp;
    }

    render() {
            return (
                <div>
                    <h2>{I18n.t('logisticsdetails.details')}</h2>
                    {this.showTrend()}
                    {this.showTrack()}
                    {this.showDetailsMessage()}
                </div>
            );
    }
}
