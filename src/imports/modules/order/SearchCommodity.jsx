import { Input, Button } from 'antd';
import classNames from 'classnames';
const InputGroup = Input.Group;
import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
const SearchCommodity = React.createClass({
    getInitialState() {
        return {
            value: '',
            focus: false,
        };
    },
    handleInputChange(e) {
        this.setState({
            value: e.target.value,
        });
    },
    handleFocusBlur(e) {
        this.setState({
            focus: e.target === document.activeElement,
        });
    },
    handleSearch() {
        if (this.props.onSearch) {
            this.props.onSearch(this.state.value);
        }
    },
    render() {
        const { style, size, placeholder } = this.props;
        const btnCls = classNames({
            'ant-search-btn': true,
            'ant-search-btn-noempty': !!this.state.value.trim(),
        });
        const searchCls = classNames({
            'ant-search-input': true,
            'ant-search-input-focus': this.state.focus,
        });
        return (
            <div className="ant-search-input-wrapper" style={style}>
                <InputGroup className={searchCls}>
                    <Input placeholder={placeholder} value={this.state.value} onChange={this.handleInputChange}
                           onFocus={this.handleFocusBlur} onBlur={this.handleFocusBlur} onPressEnter={this.handleSearch}
                    />
                    <span style={{position: 'absolute',right: 8,top: 13,zIndex: 2,cursor: 'pointer'}} size={size} onClick={this.handleSearch} >{I18n.t('order.search')}</span>
                </InputGroup>
            </div>
        );
    },
});

export default SearchCommodity;