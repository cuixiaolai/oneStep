import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Table } from 'antd';
export default class OrderTable extends Component{
    render(){
        return(
            <div className="OrderTable">
                <p>{this.props.name}</p>
                <Table rowSelection={this.props.rowSelection} columns={this.props.columns} dataSource={this.props.data} bordered pagination={false} />
            </div>
        )
    }
}