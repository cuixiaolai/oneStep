import React, {Component} from 'react';
import {Select, Dropdown, DatePicker, Modal, Button, Input, Menu, Icon, Form, Tabs,
    Col, Checkbox, Pagination, Cascader} from 'antd';
import {Translate, I18n} from 'react-redux-i18n';
import config from '../../config.json';
import SearchCommodity from './SearchCommodity.jsx';
import DialogPayment from './DialogPayment.jsx';
import DialogReminderdelivery from './DialogReminderdelivery.jsx';
import DialogDelayedreceipt from './DialogDelayedreceipt.jsx';
import {Link,browserHistory} from 'react-router'
// import {createContainer} from 'meteor/react-meteor-data';
// import {Order} from '../../api/Order.js';

/*买家订单*/



export default class OrderAll extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            all: false,
            ci: 0,
            // view:[],
            totalnum:0,
            logistics_data:[],
            refresh:true,
            isfirst:true
        });
    }
    componentDidMount(){
        if(this.props.data != null && this.props.data.length > 0){
                this.state.logistics_data = new Array(this.props.data.length)
                console.log("this.props.data.length");
                console.log(this.props.data.length);
            for(let i = 0; i < this.props.data.length; i++){
                this.state.logistics_data[i]=[]
                //change to react fetch?

                if(typeof this.props.data[i].delivery != 'undefined'){
                    console.log(this.props.data[i])
                    var logistics_company = ''
                    switch (this.props.data[i].delivery.logistics_company){
                        case '中通快递':
                            logistics_company = 'ZTO';
                            break;
                        case '韵达快递':
                            logistics_company = 'YD';
                            break;
                        case '圆通速递':
                            logistics_company = 'YTO';
                            break;
                        case '百世快递':
                            logistics_company = 'HTKY';
                            break;
                        case '邮政包裹': //邮政平邮/小包
                            logistics_company = 'YZPY';
                            break;
                        case '天天快递':
                            logistics_company = 'HHTT';
                            break;
                        case '顺丰速递':
                            logistics_company = 'SF';
                            break;
                        case 'EMS':
                            logistics_company = 'EMS';
                            break;
                    }

                    // const logistics = '{\"LogisticCode\":\"'+this.props.data[i].delivery.logistics_number+'\",\"ShipperCode\":\"'+logistics_company+'\"}';
                    // var data_sign = logistics+'6ad4a997-2ca3-4409-a1a4-5b9bf5ddd8cc';
                    // data_sign = hex_md5(data_sign);
                    // data_sign = data_sign.toLowerCase();
                    // const b = new Base64();
                    // data_sign = b.encode(data_sign);
                    // const xhr = new XMLHttpRequest();
                    // xhr.open('get', config.file_server+'kdniao?LogisticCode='+this.props.data[i].delivery.logistics_number+'&ShipperCode='+logistics_company, true);
                    // var fd = new FormData();
                    // fd.append('RequestData', encodeURIComponent(logistics));
                    // fd.append('EBusinessID', '1268442');
                    // fd.append('RequestType', '1002');
                    // fd.append('DataSign', encodeURIComponent(data_sign));
                    // fd.append('DataType', '2');
                    // xhr.send(fd);

                    const LogisticCode = this.props.data[i].delivery.logistics_number;
                    const xhr = new XMLHttpRequest();
                    xhr.open('get', config.file_server+'kdniao?LogisticCode='+LogisticCode+'&ShipperCode='+logistics_company, true);

                    xhr.send();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4) {
                            var temp = [];
                            console.log(JSON.parse(xhr.response))
                            const logistics_data = (JSON.parse(xhr.response)).Traces;
                            for(let j = logistics_data.length - 1; j > logistics_data.length - 4; j--){
                                temp.push(
                                    <li>
                                        <p>{logistics_data[j].AcceptStation}</p>
                                        <p>{logistics_data[j].AcceptTime}</p>
                                    </li>
                                )
                                console.log("pushed")
                            }
                            this.state.logistics_data[i] = temp;
                            this.setState({
                                refresh:!this.state.refresh
                            })
                            console.log("refresh")
                            console.log(this.state.logistics_data,'++++++++++------------')
                        }
                    }.bind(this)

                }
            }
            console.log(this.props.runtime,this.props.data)
            this.props.setRuntime()
        }
    }
    componentWillReceiveProps(nextProps){
        if((nextProps.runtime == 0 && nextProps.data != null && nextProps.data.length > 0)||(nextProps.runtime == 0 && nextProps.data != null && nextProps.data.length > 0)){
            this.state.logistics_data = new Array(nextProps.data.length)
            for(let i = 0; i < nextProps.data.length; i++){
                                            console.log("i");
                            console.log(i);
                            console.log("nextProps.data.length");
                            console.log(nextProps.data.length);
                this.state.logistics_data[i]=[]

                //change to react fetch?

                if(typeof nextProps.data[i].delivery != 'undefined'){
                    console.log(nextProps.data[i])
                    var logistics_company = ''
                    switch (nextProps.data[i].delivery.logistics_company){
                        case '中通快递':
                            logistics_company = 'ZTO';
                            break;
                        case '韵达快递':
                            logistics_company = 'YD';
                            break;
                        case '圆通速递':
                            logistics_company = 'YTO';
                            break;
                        case '百世快递':
                            logistics_company = 'HTKY';
                            break;
                        case '邮政包裹': //邮政平邮/小包
                            logistics_company = 'YZPY';
                            break;
                        case '天天快递':
                            logistics_company = 'HHTT';
                            break;
                        case '顺丰速递':
                            logistics_company = 'SF';
                            break;
                        case 'EMS':
                            logistics_company = 'EMS';
                            break;
                    }
                    // const logistics = '{\"LogisticCode\":\"'+nextProps.data[i].delivery.logistics_number+'\",\"ShipperCode\":\"'+logistics_company+'\"}';
                    // var data_sign = logistics+'6ad4a997-2ca3-4409-a1a4-5b9bf5ddd8cc';
                    // data_sign = hex_md5(data_sign);
                    // data_sign = data_sign.toLowerCase();
                    // const b = new Base64();
                    // data_sign = b.encode(data_sign);
                    // const xhr = new XMLHttpRequest();
                    // xhr.open('get', config.file_server+'kdniao?LogisticCode='+nextProps.data[i].delivery.logistics_number+'&ShipperCode='+logistics_company, true);
                    // xhr.open('post', 'http://api.kdniao.cc/Ebusiness/EbusinessOrderHandle.aspx', true);
                    // var fd = new FormData();
                    // fd.append('RequestData', encodeURIComponent(logistics));
                    // fd.append('EBusinessID', '1268442');
                    // fd.append('RequestType', '1002');
                    // fd.append('DataSign', encodeURIComponent(data_sign));
                    // fd.append('DataType', '2');
                    // xhr.send(fd);

                                       
                    const LogisticCode = nextProps.data[i].delivery.logistics_number;
                    const xhr = new XMLHttpRequest();
                    xhr.open('get', config.file_server+'kdniao?LogisticCode='+LogisticCode+'&ShipperCode='+logistics_company, true);

                    xhr.send();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4) {
                            var temp = [];
  
                            const logistics_data = (JSON.parse(xhr.response)).Traces;
                            console.log(logistics_data)
                            for(let j = logistics_data.length - 1; j > logistics_data.length - 4; j--){
                                console.log(logistics_data[j].AcceptStation)
                                temp.push(
                                    <li>
                                        <p>{logistics_data[j].AcceptStation}</p>
                                        <p>{logistics_data[j].AcceptTime}</p>
                                    </li>
                                )
                            }
                            console.log("temp,'-------------'")
                            console.log(temp,'-------------')
                            console.log("i");
                            console.log(i);

                            this.state.logistics_data[i] = temp;
                            this.setState({
                                refresh:!this.state.refresh
                            })
                        }
                    }.bind(this)

                }
            }
            this.props.setRuntime()
        }
    }
    showSearch() {
        let temp = [];
        temp[0] = (
            <div className="searchcommodity">
                <SearchCommodity placeholder={I18n.t('order.placeholder')}
                                 onSearch={this.props.searchcommodity} style={{ width: 240,height: 40 }}/>
            </div>
        )
        return temp;
    }
    showPackage(package_arr){
        let packageStr = '';
        for (let i = 0; i < package_arr.length; ++ i) {
            if (packageStr == '') {
                packageStr = package_arr[i].package;
            }
            else {
                packageStr = packageStr + ', ' + package_arr[i].package;
            }
        }
        return packageStr;
    }
    //商品
    showDetails(product_id, manufacturer, package_arr, describe) {
        let temp = [];
        temp[0] = (
            <div className="clearFloat">
                <div className="productLeft">
                    <img src={(config.theme_path+'shop_03.jpg')} alt="图片"/>
                </div>
                <div className="productRight">
                    <h3>{product_id}</h3>
                    <p> <span style={manufacturer.length+package_arr[0].package.length>20?{display:'block'}:{}}>{I18n.t('Search.Manufacturer')}{manufacturer}</span>
                        <span style={manufacturer.length+package_arr[0].package.length>20?{}:{marginLeft:30}}>{this.showPackage(package_arr)}</span> </p>
                    <p className="p_">{describe}</p>
                </div>
            </div>
        )
        return temp;
    }
    //数量
    showAmount(quantity) {
        let temp = [];
        temp[0] = (
            <div >
                ×{quantity}
            </div>)
        return temp;
    }
    //单价
    showUnitPrice(price,currency) {
        let temp = [];
        temp[0] = (
            <div >{currency==1?'￥':'$'}{price}/个</div>
        )
        return temp;
    }
    //商品操作
    showCommodityOperator(order_status, _id) {
        let temp = [];
        if (order_status == 1) {
            temp.push(<div></div>);
        } else if (order_status == 2) {
            temp.push(
                <Link to={"/return/personrefund_"+_id} >
                    <div className="commodityOperator">
                        退款
                    </div>
                </Link>
            );
        } else if (order_status == 3) {
            temp.push(
                <Link to={"/return/personrefund_"+_id} >
                    <div className="commodityOperator">
                        退款
                    </div>
                </Link>
            );
        } else if (order_status == 4) {
            temp.push(
                <Link to={"/return/personreturngoods_"+_id} >
                    <div className="commodityOperator">
                        退货
                    </div>
                </Link>
            );
            temp.push(
                <Link to={"/return/personwarranty_" + _id}>
                    <div className="commodityOperator">
                        {I18n.t('order.launchwarranty')}
                    </div>
                </Link>
            );
        }
        return temp;
    }

    //交货状态
    showDeliveryStatus(order_status, _id, num, delivery) {
        let temp = [];
        if (order_status == 1) {
            temp[0] = (
                <div>
                    <span >{I18n.t('shoppingCart.dialog.waitingbuyerspay')} </span>
                    <Link to={'/order_details/'+_id} ><span
                        className="orderdetails">{I18n.t('shoppingCart.paymentMoney.details')} </span></Link>
                </div>
            )
        } else if (order_status == 2) {
            temp[0] = (
                <div>
                    <span >{I18n.t('order.waitorder')} </span>
                    <Link to={'/order_details/'+_id} ><span
                        className="orderdetails">{I18n.t('shoppingCart.paymentMoney.details')} </span></Link>
                </div>

            )
        } else if (order_status == 3) {
            temp[0] = (<div >
                    <span >{I18n.t('order.waitgoods')} </span>
                    <p className="viewlogisticsboxShow">
                        {/*<Link to="/logistics_details" >*/}
                            <span className="orderdetails">{I18n.t('sellercenterorder.viewlogistics')} </span>
                        {/*</Link>*/}
                        <div className="viewlogisticsbox" >
                            {this.viewlogisticsbox(num, delivery)}
                        </div>
                    </p>
                    <Link to={'/order_details/'+_id} ><span
                        className="orderdetails"     >{I18n.t('shoppingCart.paymentMoney.details')} </span></Link>
                </div>
            )
        } else if (order_status == 4) {
            temp[0] = (
                <div>
                    <span >{I18n.t('sellercenterorder.successfultrade')} </span>
                    <p  className="viewlogisticsboxShow">
                        {/*<Link to="/logistics_details" >*/}
                            <span className="orderdetails">{I18n.t('sellercenterorder.viewlogistics')} </span>
                        {/*</Link>*/}
                        <div className="viewlogisticsbox" >
                            {this.viewlogisticsbox(num, delivery)}
                        </div>
                    </p>
                    <Link to={'/order_details/'+_id} >
                        <span className="orderdetails">{I18n.t('shoppingCart.paymentMoney.details')} </span>
                    </Link>
                </div>
            )
        } else if (order_status == 5) {
            temp[0] = (
                <div>
                    <span >{I18n.t('sellercenterorder.successfultrade')} </span>
                    <span >{I18n.t('sellercenterorder.buyershavebeen')} </span>
                    <p  className="viewlogisticsboxShow">
                        {/*<Link to="/logistics_details" >*/}
                            <span className="orderdetails">{I18n.t('sellercenterorder.viewlogistics')} </span>
                        {/*</Link>*/}
                        <div className="viewlogisticsbox" >
                            {this.viewlogisticsbox(num, delivery)}
                        </div>
                    </p>
                    <Link to={'/order_details/'+_id} ><span
                        className="orderdetails">{I18n.t('shoppingCart.paymentMoney.details')} </span></Link>
                </div>
            )
        } else if (order_status == 7) {
            temp[0] = (
                <div>
                    <span >{I18n.t('sellercenterorder.transactionclosed')} </span>
                    <Link to={'/order_details/'+_id}><span className="orderdetails">{I18n.t('shoppingCart.paymentMoney.details')} </span></Link>
                </div>
            )
        } else if (order_status == 8){
            temp[0] = (
                <div>
                    <span >{I18n.t('sellercenterorder.successfultrade')} </span>
                    <span >{I18n.t('sellercenterorder.bothvaluation')} </span>
                    <p  className="viewlogisticsboxShow">
                        {/*<Link to="/logistics_details" >*/}
                            <span className="orderdetails">{I18n.t('sellercenterorder.viewlogistics')} </span>
                        {/*</Link>*/}
                        <div className="viewlogisticsbox" >
                            {this.viewlogisticsbox(num, delivery)}
                        </div>
                    </p>
                    <Link to={'/order_details/'+_id} ><span
                        className="orderdetails">{I18n.t('shoppingCart.paymentMoney.details')} </span></Link>
                </div>
            )
        }
        return temp;
    }

    //交易操作
    clickCi() {
        let ci = this.state.ci++;
        this.setState({ci: ci});
    }
    forwardToOperation(order_id,type){
        console.log(type);
        if(type == 'customer_pay'){
            Meteor.call('quote.payOrder',order_id, (err) => {
                if (err) {

                } else {
                    browserHistory.replace('/payment_money');
                }
            });
        } else {
            Meteor.call('order.forwardToOperation',order_id,type, (err) => {
                if (err) {

                } else {
                    if(type == 'customer_receive'){
                        browserHistory.replace('/confirm_order');
                    }else if(type == 'customer_comment'){
                        browserHistory.replace('/evaluate');
                    }else if(type == 'customer_addition'){
                        browserHistory.replace('/additional_comment');
                    }
                }
            });
        }

    }
    showTransactionOperator(order_status, _id, addition_comment_flag){
        let temp = [];
        if (order_status == 1) {
            temp[0] = (
                <div>
                    <div className="operatorbox">
                        <Button className="operator_  paymentspan" onClick={this.forwardToOperation.bind(this,_id,'customer_pay')} >{I18n.t('shoppingCart.dialog.payment')}</Button>
                    </div>
                    <span className="operator_ ">
                         <DialogPayment words={I18n.t('shoppingCart.dialog.confirmcancelorder')}
                                        content={I18n.t('shoppingCart.dialog.cancelOrder')}
                                        order_id={_id} />
                    </span>
                </div>
            )
        } else if (order_status == 2) {
            temp[0] = (
                <span className="operator_">
                         <DialogReminderdelivery words={I18n.t('shoppingCart.dialog.remindercontent')}
                                                 content={I18n.t('shoppingCart.dialog.reminderdelivery')}
                                                 order_id={_id} />
                </span>
            )
        } else if (order_status == 3) {
            temp[0] = (
                <div >
                    <div className="operatorbox">
                        <Button className="operator_  paymentspan" onClick={this.forwardToOperation.bind(this,_id,'customer_receive')} >{I18n.t('shoppingCart.dialog.confirmreceipt')}</Button>
                    </div>
                    <span className="operator_ " onClick={this.clickCi.bind(this)}>
                        <DialogDelayedreceipt words={I18n.t('shoppingCart.dialog.extendedcontent')}
                                              content={I18n.t('shoppingCart.dialog.extendedreceipt')}
                                              order_id={_id} />
                    </span>
                </div>
            )
        } else if (order_status == 4) {
            temp[0] = (
                <div className="operatorbox">
                    <Button className="operator_  paymentspan" onClick={this.forwardToOperation.bind(this,_id,'customer_comment')} >{I18n.t('shoppingCart.dialog.evaluate')}</Button>
                </div>

            )
        } else if ((order_status == 5 ||order_status == 8) && !addition_comment_flag) {
            temp[0] = (
                <div className="operatorbox">
                    <Button className="operator_  paymentspan" onClick={this.forwardToOperation.bind(this,_id,'customer_addition')} >追评</Button>
                </div>
            )
        } else {
            temp[0] = (
                <div></div>
            )
        }
        return temp;
    }
    //总额
    showTotal(total_price, postage, packagePrice, labelPrice, listPrice, currency) {
        let temp = [];
        temp[0] = (
            <div >
                {currency==1?'￥':'$'}
                <span className="total">{total_price + postage + packagePrice + labelPrice + listPrice}</span>
            </div>
        )
        return temp;
    }

    showTop() {
        let temp = [];
        const menu = (
            <Menu onClick={this.props.clickDown}>
                <Menu.Item key="0">
                    <span>{I18n.t('returnnote.lastweek')}</span>
                </Menu.Item>
                <Menu.Item key="1">
                    <span>{I18n.t('returnnote.lastmonth')}</span>
                </Menu.Item>
                <Menu.Divider />
                <Menu.Item key="2">
                    <span>{I18n.t('returnnote.lastthreemonths')}</span>
                </Menu.Item>
                <Menu.Item key="3">
                    <span>{I18n.t('returnnote.recenthalfyear')}</span>
                </Menu.Item>
            </Menu>
        );

        temp[0] = (
            <div className="showTop">
                <ul className="top">
                    <li style={{width:80}} className="timeframe">
                        <Dropdown overlay={menu} trigger={['click']}>
                            <a className="ant-dropdown-link" href="#" >
                                {this.props.dropdownLable}
                                <Icon type="down" style={this.props.downflag ? {transform:'scale(0.667) rotate(180deg)'} : {transform:'scale(0.667) rotate(0deg)'}}/>
                            </a>
                        </Dropdown>
                    </li>
                    <li style={{width:240}}>
                        {I18n.t('order.commodity')}
                    </li>
                    <li style={{width:100}}>
                        {I18n.t('order.amount')}
                    </li>
                    <li style={{width:110}}>
                        {I18n.t('order.unitprice')}
                    </li>
                    <li style={{width:110}}>
                        {I18n.t('order.commodityoperator')}
                    </li>
                    <li style={{width:110}}>
                        {I18n.t('order.total')}
                    </li>
                    <li style={{width:120}}>
                        {I18n.t('order.deliverystatus')}
                    </li>
                    <li style={{width:115}}>
                        {I18n.t('order.transactionoperator')}
                    </li>
                </ul>
            </div>

        )
        return temp;
    }

    showTableTop(company_type, company_name, id, update_time, num) {
        let temp = [];
        let sign = '';
        if (company_type == 'agent') {
            sign = '代理商';
        } else if (company_type == 'stock') {
            sign = '厂商'
        } else if (company_type == 'retailer') {
            sign = '零售商'
        }
        temp[0] = (
            <div className="tableTop">
                {/*<Checkbox className="checkbox" checked={this.state.checked[num]}*/}
                {/*onClick={this.clickChecked.bind(this,num)}>*/}
                <span className="tableTopName">{company_name.length>5?company_name.substr(0,4)+'...':company_name}</span>
                {/*</Checkbox>*/}
                {this.ShowAllName(company_name)}
                <span className="sign">{sign}</span>
                <img src={(config.theme_path+'talk.png')} className="talkImg" alt="图片"/>
                <p className="ordernumber">{I18n.t('order.ordernumber')}<span>{id}</span></p>
                <p className="orderdate">{typeof update_time != 'object'?'':(update_time.getFullYear()+'-'+(update_time.getMonth()+1)+'-'+update_time.getDate())}</p>
            </div>
        )
        return temp;
    }
    ShowAllName(name){
        if(name.length>4){
            return(
                <div className="ShowAllName">
                    {name}
                </div>
            )
        }
    }
    calculatePrice(price, quantity){
        let grad = 0;
        let flag = true;
        for (let j = 0; j < price.length && flag; j++) {
            if (quantity >= price[j].min) {
                grad = j;
            }else {
                flag = false;
            }
        }
        return price[grad].price
    }
    showOrder(order, num) {
        let arr = [];
        let total_price = 0;
        let price = [];
        for(let i in order.product){
            let product_price = this.calculatePrice(order.product[i].price, order.product[i].quantity);
            total_price += product_price*order.product[i].quantity;
            price.push(product_price);
        }
        for(let i in order.product){
            let product = [];
            if (order.product.length > 1) {
                if (i == 0) {
                    product.push(<td style={{width:350}} className="orderdetails">{this.showDetails(order.product[i].product_id, order.product[i].manufacturer, order.product[i].package, order.product[i].describe)}</td>)
                    product.push(<td style={{width:100}}>{this.showAmount(order.product[i].quantity)}</td>)
                    product.push(<td style={{width:110}}>{this.showUnitPrice(price[i], order.currency)}</td>)
                    product.push(<td style={{width:115,borderTop:0}} rowSpan={order.product.length}>{this.showCommodityOperator(order.order_status, order._id)}</td>)
                    product.push(<td style={{width:110,borderTop:0}} rowSpan={order.product.length}>{this.showTotal(total_price, order.postage, order.packagePrice, order.labelPrice, order.listPrice, order.currency)}</td>)
                    product.push(<td style={{width:120,borderTop:0}} className="status" rowSpan={order.product.length}>
                        {this.showDeliveryStatus(order.order_status, order._id, num, order.delivery)}</td>)
                    product.push(<td style={{width:115,borderTop:0}} rowSpan={order.product.length} className="transactionoperator">
                        {this.showTransactionOperator(order.order_status, order._id, order.additional_comment)}</td>)
                }else {
                    product.push(<td style={{width: 350}}>{this.showDetails(order.product[i].product_id, order.product[i].manufacturer, order.product[i].package, order.product[i].describe)}</td>)
                    product.push(<td style={{width: 100}}>{this.showAmount(order.product[i].quantity)}</td>)
                    product.push(<td style={{width: 110}}>{this.showUnitPrice(price[i], order.currency)}</td>)
                    {/*product.push(<td style={{width: 115}}>{this.showCommodityOperator(order.order_status, order._id, i, order.product[i])}</td>)*/}
                }
            } else {
                product.push(<td style={{width:350}}>{this.showDetails(order.product[i].product_id, order.product[i].manufacturer, order.product[i].package, order.product[i].describe)}</td>)
                product.push(<td style={{width:100}}>{this.showAmount(order.product[i].quantity)}</td>)
                product.push(<td style={{width:110}}>{this.showUnitPrice(price[i], order.currency)}</td>)
                product.push(<td style={{width:115}}>{this.showCommodityOperator(order.order_status, order._id)}</td>)
                product.push(<td style={{width:110}}>{this.showTotal(total_price, order.postage, order.packagePrice, order.labelPrice, order.listPrice, order.currency)}</td>)
                product.push(<td style={{width:120}} className="status">{this.showDeliveryStatus(order.order_status, order._id, num, order.delivery)}</td>)
                product.push(<td style={{width:115}}>{this.showTransactionOperator(order.order_status, order._id, order.additional_comment)}</td>)
            }
            arr.push(<tr className="tabletr">{product}</tr>)
        }
        return  arr;
    }
    showTable(showdata) {
        let temp = [];
        for (let i = 0; i < showdata.length; i++) {
            temp[i] = (
                <div className="table_">
                    {this.showTableTop(showdata[i].company_type, showdata[i].company_name, showdata[i].id, showdata[i].update_time, i)}
                    <table  style={{border:1}}>
                        {this.showOrder(showdata[i], i)}
                    </table>
                </div>
            )
        }

        return temp;
    }
    //查看物流弹窗
    viewlogisticsbox(num, delivery){
        console.log("num")
        console.log(num)
        let temp = typeof this.state.logistics_data[num] == 'undefined'?[]:this.state.logistics_data[num]
        console.log("this.state.logistics_data[num],'++++++++++'")
        console.log(this.state.logistics_data[num],'++++++++++')
        console.log(this.state.logistics_data,'++++++++++')
        let view = [];
        view[0] = (

            <div className="">
                <img src={(config.theme_path+'mark_2.png')}  alt="图片"/>
                <p className="logisticsTop">{delivery.logistics_company}:{delivery.logistics_number}</p>
                <ul>
                    {temp}
                </ul>
                {/*<p className="logisticsBottom">{I18n.t('sellerorderdetails.viewall')} <span><Link to="/logistics_details">{I18n.t('logistics.lookall')}</Link></span> </p>*/}
            </div>
        )
        return view;
    }
    ShowPagination(){
        console.log(this.props.datalength)
        return(
            <Pagination showQuickJumper  current={this.props.current_page} pageSize="10" onChange={this.props.changePage} total={this.props.datalength}/>
        )
    }
    render() {
        let showdata1 = this.props.data
        console.log(this.state.logistics_data,'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq')
        return (
            <div>
                {this.showSearch()}
                {this.showTop()}
                <div className="select">
                </div>
                {this.showTable(showdata1)}
                <div className="ShowPaginationBox" style={this.props.datalength > 10 ? {display:'block'} : {display:'none'}} >
                    {this.ShowPagination()}
                    <div className="clear"></div>
                </div>
            </div>
        );
    }
}


// md5
var hexcase = 0; /* hex output format. 0 - lowercase; 1 - uppercase  */
var b64pad = ""; /* base-64 pad character. "=" for strict RFC compliance */
var chrsz = 8; /* bits per input character. 8 - ASCII; 16 - Unicode  */
/*
 * These are the functions you'll usually want to call
 * They take string arguments and return either hex or base-64 encoded strings
 */
function hex_md5(s){ return binl2hex(core_md5(str2binl(s), s.length * chrsz));}
function b64_md5(s){ return binl2b64(core_md5(str2binl(s), s.length * chrsz));}
function str_md5(s){ return binl2str(core_md5(str2binl(s), s.length * chrsz));}
function hex_hmac_md5(key, data) { return binl2hex(core_hmac_md5(key, data)); }
function b64_hmac_md5(key, data) { return binl2b64(core_hmac_md5(key, data)); }
function str_hmac_md5(key, data) { return binl2str(core_hmac_md5(key, data)); }
/*
 * Perform a simple self-test to see if the VM is working
 */
function md5_vm_test()
{
    return hex_md5("abc") == "900150983cd24fb0d6963f7d28e17f72";
}
/*
 * Calculate the MD5 of an array of little-endian words, and a bit length
 */
function core_md5(x, len)
{
    /* append padding */
    x[len >> 5] |= 0x80 << ((len) % 32);
    x[(((len + 64) >>> 9) << 4) + 14] = len;
    var a = 1732584193;
    var b = -271733879;
    var c = -1732584194;
    var d = 271733878;
    for(var i = 0; i < x.length; i += 16)
    {
        var olda = a;
        var oldb = b;
        var oldc = c;
        var oldd = d;
        a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
        d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
        c = md5_ff(c, d, a, b, x[i+ 2], 17, 606105819);
        b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
        a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
        d = md5_ff(d, a, b, c, x[i+ 5], 12, 1200080426);
        c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
        b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
        a = md5_ff(a, b, c, d, x[i+ 8], 7 , 1770035416);
        d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
        c = md5_ff(c, d, a, b, x[i+10], 17, -42063);
        b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
        a = md5_ff(a, b, c, d, x[i+12], 7 , 1804603682);
        d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);
        c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
        b = md5_ff(b, c, d, a, x[i+15], 22, 1236535329);
        a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
        d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
        c = md5_gg(c, d, a, b, x[i+11], 14, 643717713);
        b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
        a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
        d = md5_gg(d, a, b, c, x[i+10], 9 , 38016083);
        c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);
        b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
        a = md5_gg(a, b, c, d, x[i+ 9], 5 , 568446438);
        d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
        c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
        b = md5_gg(b, c, d, a, x[i+ 8], 20, 1163531501);
        a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
        d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
        c = md5_gg(c, d, a, b, x[i+ 7], 14, 1735328473);
        b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);
        a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
        d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
        c = md5_hh(c, d, a, b, x[i+11], 16, 1839030562);
        b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);
        a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
        d = md5_hh(d, a, b, c, x[i+ 4], 11, 1272893353);
        c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
        b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
        a = md5_hh(a, b, c, d, x[i+13], 4 , 681279174);
        d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
        c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
        b = md5_hh(b, c, d, a, x[i+ 6], 23, 76029189);
        a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
        d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);
        c = md5_hh(c, d, a, b, x[i+15], 16, 530742520);
        b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);
        a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
        d = md5_ii(d, a, b, c, x[i+ 7], 10, 1126891415);
        c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
        b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
        a = md5_ii(a, b, c, d, x[i+12], 6 , 1700485571);
        d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
        c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);
        b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
        a = md5_ii(a, b, c, d, x[i+ 8], 6 , 1873313359);
        d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);
        c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
        b = md5_ii(b, c, d, a, x[i+13], 21, 1309151649);
        a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
        d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
        c = md5_ii(c, d, a, b, x[i+ 2], 15, 718787259);
        b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);
        a = safe_add(a, olda);
        b = safe_add(b, oldb);
        c = safe_add(c, oldc);
        d = safe_add(d, oldd);
    }
    return Array(a, b, c, d);
}
/*
 * These functions implement the four basic operations the algorithm uses.
 */
function md5_cmn(q, a, b, x, s, t)
{
    return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);
}
function md5_ff(a, b, c, d, x, s, t)
{
    return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
}
function md5_gg(a, b, c, d, x, s, t)
{
    return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
}
function md5_hh(a, b, c, d, x, s, t)
{
    return md5_cmn(b ^ c ^ d, a, b, x, s, t);
}
function md5_ii(a, b, c, d, x, s, t)
{
    return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
}
/*
 * Calculate the HMAC-MD5, of a key and some data
 */
function core_hmac_md5(key, data)
{
    var bkey = str2binl(key);
    if(bkey.length > 16) bkey = core_md5(bkey, key.length * chrsz);
    var ipad = Array(16), opad = Array(16);
    for(var i = 0; i < 16; i++)
    {
        ipad[i] = bkey[i] ^ 0x36363636;
        opad[i] = bkey[i] ^ 0x5C5C5C5C;
    }
    var hash = core_md5(ipad.concat(str2binl(data)), 512 + data.length * chrsz);
    return core_md5(opad.concat(hash), 512 + 128);
}
/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function safe_add(x, y)
{
    var lsw = (x & 0xFFFF) + (y & 0xFFFF);
    var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
    return (msw << 16) | (lsw & 0xFFFF);
}
/*
 * Bitwise rotate a 32-bit number to the left.
 */
function bit_rol(num, cnt)
{
    return (num << cnt) | (num >>> (32 - cnt));
}
/*
 * Convert a string to an array of little-endian words
 * If chrsz is ASCII, characters >255 have their hi-byte silently ignored.
 */
function str2binl(str)
{
    var bin = Array();
    var mask = (1 << chrsz) - 1;
    for(var i = 0; i < str.length * chrsz; i += chrsz)
        bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (i%32);
    return bin;
}
/*
 * Convert an array of little-endian words to a string
 */
function binl2str(bin)
{
    var str = "";
    var mask = (1 << chrsz) - 1;
    for(var i = 0; i < bin.length * 32; i += chrsz)
        str += String.fromCharCode((bin[i>>5] >>> (i % 32)) & mask);
    return str;
}
/*
 * Convert an array of little-endian words to a hex string.
 */
function binl2hex(binarray)
{
    var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
    var str = "";
    for(var i = 0; i < binarray.length * 4; i++)
    {
        str += hex_tab.charAt((binarray[i>>2] >> ((i%4)*8+4)) & 0xF) +
            hex_tab.charAt((binarray[i>>2] >> ((i%4)*8 )) & 0xF);
    }
    return str;
}
/*
 * Convert an array of little-endian words to a base-64 string
 */
function binl2b64(binarray)
{
    var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var str = "";
    for(var i = 0; i < binarray.length * 4; i += 3)
    {
        var triplet = (((binarray[i >> 2] >> 8 * ( i %4)) & 0xFF) << 16)
            | (((binarray[i+1 >> 2] >> 8 * ((i+1)%4)) & 0xFF) << 8 )
            | ((binarray[i+2 >> 2] >> 8 * ((i+2)%4)) & 0xFF);
        for(var j = 0; j < 4; j++)
        {
            if(i * 8 + j * 6 > binarray.length * 32) str += b64pad;
            else str += tab.charAt((triplet >> 6*(3-j)) & 0x3F);
        }
    }
    return str;
}

//base64
function Base64() {
    // private property
    _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    // public method for encoding
    this.encode = function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = _utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
                _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
                _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
        }
        return output;
    }
    // public method for decoding
    this.decode = function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = _keyStr.indexOf(input.charAt(i++));
            enc2 = _keyStr.indexOf(input.charAt(i++));
            enc3 = _keyStr.indexOf(input.charAt(i++));
            enc4 = _keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = _utf8_decode(output);
        return output;
    }
    // private method for UTF-8 encoding
    _utf8_encode = function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    }
    // private method for UTF-8 decoding
    _utf8_decode = function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while ( i < utftext.length ) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}
