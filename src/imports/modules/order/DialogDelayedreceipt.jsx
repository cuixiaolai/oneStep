import { Modal, Button,Radio ,Cascader} from 'antd';
import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
{/*延长收货弹窗*/}


export default class DialogDelayedreceipt extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            value:1,
            loading: false,
            visible: false,
            delay:true,
        });

    }
    componentDidMount(){

        console.log(this.props.key);

    }
    onChange(e) {
        console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });
    }
    showModal() {
        Meteor.call('order.delayReceive', this.props.order_id, (err,result) => {
            if (err) {

            } else {
                this.setState({
                    delay: result
                });
            }
        });
        this.setState({
            visible: true,
        });
    }
    handleOk(){
        {/*弹窗确认事件*/}
        this.setState({
            visible: false,
        });
    }
    handleCancel() {
        this.setState({ visible: false});
    }

    render() {


        return (
            <div className={this.props.className} style={{display:'inline-block'}}>
                <span onClick={this.showModal.bind(this)}  >
                    {this.props.content}
                </span>
                <Modal ref="modal"
                       visible={this.state.visible}
                       title="" onOk={this.handleOk.bind(this)} onCancel={this.handleCancel.bind(this)}
                       footer={[

            <Button className="yes" key="submit" type="primary" style={{height:32,width:72,marginRight:140,fontSize:14}} loading={this.state.loading} onClick={this.handleOk.bind(this)}>
                {I18n.t('PersonalCenter.company.yes1')}
            </Button>
          ]}
                >
                {this.state.delay ?
                    (
                        <div className="reminderDialog"  style={{paddingRight:30,paddingLeft:138,paddingTop:0,fontSize:14}}>
                            <img src={(config.theme_path+'true.png')} alt="图片"/>
                            <p className="title">{this.props.words}</p>
                            <p className="content">{I18n.t('shoppingCart.dialog.delayedreceiptcontent')}</p>
                            <p className="content">{I18n.t('shoppingCart.dialog.delayedreceiptcontent2')}</p>

                        </div>
                    ):(
                        <div className="reminderDialog"  style={{paddingRight:30,paddingLeft:138,paddingTop:0,fontSize:14}}>
                            <p className="title">您已经进行过延迟收货操作</p>
                            <p className="content">{I18n.t('shoppingCart.dialog.delayedreceiptcontent')}</p>
                            <p className="content">{I18n.t('shoppingCart.dialog.delayedreceiptcontent2')}</p>
                        </div>
                    )
                }

                </Modal>
            </div>
        );
    }
}