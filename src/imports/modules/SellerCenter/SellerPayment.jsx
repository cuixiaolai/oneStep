import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal,Select,Input } from 'antd';
import config from '../../config.json';
import { createContainer } from 'meteor/react-meteor-data';
import { Seller } from '../../api/Seller.js';


export default class SellerPayment extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            num:0,
            nums:0,
        });
    }
    showCard(){
       if(this.state.num == 0){
            return(
                <div>
                    <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,0)}>
                        <img src={(config.theme_path + 'zhifu.jpg')} alt=""/>
                        <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                    </div>
                    <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,1)}>
                        <img src={(config.theme_path + 'jianhang.jpg')} alt=""/>
                        <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                    </div>
                    <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,2)}>
                        <img src={(config.theme_path + 'zhongguo_03.jpg')} alt=""/>
                        <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                    </div>
                    <p className="clear"></p>
                </div>
            )
       }else if(this.state.num == 1){
           return(
               <div>
                   <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,0)}>
                       <img src={(config.theme_path + 'zhifu.jpg')} alt=""/>
                       <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                   </div>
                   <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,1)}>
                       <img src={(config.theme_path + 'jianhang.jpg')} alt=""/>
                       <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                   </div>
                   <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,2)}>
                       <img src={(config.theme_path + 'zhongguo_03.jpg')} alt=""/>
                       <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                   </div>
                   <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,3)}>
                       <img src={(config.theme_path + 'zhifu.jpg')} alt=""/>
                       <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                   </div>
                   <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,4)}>
                       <img src={(config.theme_path + 'jianhang.jpg')} alt=""/>
                       <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                   </div>
                   <p className="clear"></p>
               </div>
           )
       }else if(this.state.num == 2){
           return(
               <div>
                   <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,1)}>
                       <img src={(config.theme_path + 'jianhang.jpg')} alt=""/>
                       <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                   </div>
                   <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,2)}>
                       <img src={(config.theme_path + 'zhongguo_03.jpg')} alt=""/>
                       <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                   </div>
                   <p className="clear"></p>
               </div>
           )
       }else if(this.state.num == 3){
           return(
               <div>
                   <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,0)}>
                       <img src={(config.theme_path + 'zhifu.jpg')} alt=""/>
                       <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                   </div>
                   <div className="SellerPaymentSmallCard  left" onClick={this.onClick.bind(this,1)}>
                       <img src={(config.theme_path + 'weixin.jpg')} alt=""/>
                       <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                   </div>
                   <p className="clear"></p>
               </div>
           )
       }
    }
    Click(i){
        let SellerPaymentCard = document.getElementsByClassName('SellerPaymentCard');
        let img = SellerPaymentCard[i].getElementsByTagName('img')[0];
        for(let j = 0 ;j < SellerPaymentCard.length ; j++){
            SellerPaymentCard[j].getElementsByTagName('img')[0].style.display = 'none';
            SellerPaymentCard[j].style.border = '1px solid #d6d6d6';
        }
        img.style.display = 'block';
        SellerPaymentCard[i].style.border = '1px solid #ed7020';
        this.setState({num:i});
    }
    onClick(i){
        let SellerPaymentSmallCard = document.getElementsByClassName('SellerPaymentSmallCard');
        let img = SellerPaymentSmallCard[i].getElementsByTagName('img')[1];
        for(let j = 0 ;j < SellerPaymentSmallCard.length ; j++){
            SellerPaymentSmallCard[j].getElementsByTagName('img')[1].style.display = 'none';
            SellerPaymentSmallCard[j].style.border = '1px solid #d6d6d6';
        }
        img.style.display = 'block';
        SellerPaymentSmallCard[i].style.border = '1px solid #ed7020';
        this.setState({nums:i});
    }
    click(){
        this.props.Click(this.state.num,this.state.nums);
    }
    render(){
        return(
            <div className="SellerPayment" style={this.props.style}>
                <span style={{marginBottom:15,display:'block'}}>{I18n.t('shoppingCart.paymentMoney.paymentPlatform')}</span>
                <p style={{marginBottom:30}}>
                    <div className="left SellerPaymentCard" onClick={this.Click.bind(this,0)}>
                        {I18n.t('shoppingCart.paymentMoney.recentUse')}
                        <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                    </div>
                    <div className="left SellerPaymentCard" onClick={this.Click.bind(this,1)}>
                        {I18n.t('shoppingCart.paymentMoney.companyTransfer')}
                        <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                    </div>
                    <div className="left SellerPaymentCard" onClick={this.Click.bind(this,2)}>
                        {I18n.t('shoppingCart.paymentMoney.epayment')}
                        <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                    </div>
                    <div className="left SellerPaymentCard" onClick={this.Click.bind(this,3)}>
                        {I18n.t('shoppingCart.paymentMoney.thirdParty')}
                        <img src={(config.theme_path + 'paymentMark.png')} alt=""/>
                    </div>
                    <p className="clear"></p>
                </p>
                {this.showCard()}
                <p>
                    <Button onClick={this.click.bind(this)} type="primary">{I18n.t('DoubleIndemnity.payment')}</Button>
                </p>
            </div>
        )
    }
}