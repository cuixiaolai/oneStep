import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal } from 'antd';
import config from '../../config.json';
import Phone from '../center/Phone.jsx';
const confirm = Modal.confirm;

export default class SellerCenterAgent extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            AgentList: [],
            state: false,
        });
    }
    setAgentList(arr) {
        let agent = [];
        if (arr.length > 10) {
            agent = arr.slice(0, 10);
        }
        else {
            agent = arr;
        }
        console.log(agent);
        this.setState({AgentList: agent});
    }
    Again() {
        for (let i = 0; i < this.props.authInfo.authentication.agent.files.length; ++ i) {
            $.ajax({
                type: 'POST',
                url: config.file_server + 'delete_seller',
                data: { path: this.props.authInfo.authentication.agent.files[i] },
                dataType: "json",
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }

            });
        }
        Meteor.call('seller.deleteAgentAuthentication');
        Meteor.call('authentication.cancelAgentAuthentication', Meteor.userId(), this.props.authInfo.authentication.agent.files);
    }
    Commit() {
        let newList = this.state.AgentList;
        let agent = [];
        for (let i = 0; i < newList.length; ++ i) {
            $.ajax({
                type: 'POST',
                url: config.file_server + 'save_seller',
                data: { path: newList[i]},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
            agent.push(newList[i].replace('tmp/', ''));
        }
        Meteor.call('seller.updateAgentAuthentication', agent);
        Meteor.call('authentication.addAgentAuthentication', Meteor.userId(), agent);
    }
    handelOK() {
        console.log(this.state.AgentList.length);
        if (this.state.AgentList.length != 0) {
            let newList = this.state.AgentList;
            let oldList = this.props.authInfo.authentication.agent.files;
            let addList = newList.filter(v => !oldList.includes(v));
            let delList = oldList.filter(v => !newList.includes(v));
            let agent = oldList.filter(v => newList.includes(v));
            for (let i = 0; i < addList.length; ++ i) {
                $.ajax({
                    type: 'POST',
                    url: config.file_server + 'save_seller',
                    data: { path: addList[i]},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
                agent.push(addList[i].replace('tmp/', ''));
            }
            for (let i = 0; i < delList.length; ++ i) {
                $.ajax({
                    type: 'POST',
                    url: config.file_server + 'delete_seller',
                    data: { path: delList[i] },
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (data) {
                        console.log(data);

                    }

                });
            }
            Meteor.call('authentication.cancelAgentAuthentication', Meteor.userId(), delList);
            if (Roles.userIsInRole(Meteor.userId(), 'agent')) {
                Meteor.call('Roles.removeUsersToRoles', Meteor.userId(), 'agent');
                Meteor.call('stock.allAgentOutOfStock');
            }
            Meteor.call('seller.updateAgentAuthentication', agent);
            Meteor.call('authentication.addAgentAuthentication', Meteor.userId(), agent);
        }
        else {
            let delList = this.props.authInfo.authentication.agent.files;
            for (let i = 0; i < delList.length; ++ i) {
                $.ajax({
                    type: 'POST',
                    url: config.file_server + 'delete_seller',
                    data: { path: delList[i] },
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }

                });
            }
            Meteor.call('authentication.cancelAgentAuthentication', Meteor.userId(), delList);
            if (Roles.userIsInRole(Meteor.userId(), 'agent')) {
                Meteor.call('Roles.removeUsersToRoles', Meteor.userId(), 'agent');
                Meteor.call('stock.allAgentOutOfStock');
            }
            Meteor.call('seller.updateAgentAuthentication', []);
        }
        this.setState({ state: false, AgentList: [] });
    }
    showConfirm() {
        confirm({
            title: '修改资质文件',
            content: '资质文件修改后，代理商身份将重新进行审核. 确定修改？',
            okText:'是',
            cancelText:'否',
            closable:true,
            onOk:this.handelOK.bind(this),
            onCancel() {},
        });
    }
    ShowTop(){
        if(this.props.authInfo.authentication != null && this.props.authInfo.authentication.agent.files.length == 0){//不是代理商
            return(
                <div className="SellerCenterManufacturer-top">
                    <p>
                        <span style={{color:'#ed7020'}}>{I18n.t('buyer.worm')}</span>{I18n.t('CertificationAudit.topnames')}
                    </p>
                    <p>{I18n.t('CertificationAudit.dai')}</p>
                </div>
            )
        }else if(this.props.authInfo.authentication != null){
            return(
                <p>{I18n.t('CertificationAudit.Agent')}</p>
            )

        }
    }
    agentPicList() {
        let PIC=[];
        if(this.props.authInfo.authentication.agent.files.length != 0){
            for(let i = 0; i < this.props.authInfo.authentication.agent.files.length; ++ i){
                PIC[i] = (
                    <div>
                        <img src={(config.file_server + this.props.authInfo.authentication.agent.files[i])}  alt=""/>
                        <p>{I18n.t('CertificationAudit.AgentWord')}</p>
                    </div>
                )
            }
        }
        return PIC;
    }
    Change() {
        this.setState({ state: true });
    }
    Cancel() {
        this.setState({ state: false });
    }
    ShowResult(){
        if(this.props.authInfo.authentication != null && this.props.authInfo.authentication.agent.files.length == 0){
            return(
                <div>
                    <div className="SellerCenterManufacturer-photo">
                        <Phone style={{width:'100%',height:'auto'}} setList={this.setAgentList.bind(this)} />
                    </div>
                    <p style={{textAlign:'center'}}><Button type="primary" onClick={this.Commit.bind(this)}>{I18n.t('Addresspage.sure')}</Button> </p>
                </div>
            )
        }else if(this.props.authInfo.authentication != null){
            if(this.props.authInfo.authentication.agent.files.length != 0 && this.props.authInfo.authentication.agent.verify == null && this.state.state == false){//卖家认证以认证成为代理商  或者审核成功？
                return(
                    <div className="SellerCenterAgentSuccess">
                        {this.agentPicList()}
                        <p className="clear"></p>
                        <p><span onClick={this.Change.bind(this)}>{I18n.t('CertificationAudit.change')}</span></p>
                    </div>
                )
            }
            else if(this.props.authInfo.authentication.agent.files.length != 0 && this.props.authInfo.authentication.agent.verify == null && this.state.state == true){//确认修改资质文件
                return(
                    <div className="SellerCenterAgentBoxChange">
                        <Phone style={{width:'100%',height:'auto'}} close={false} setList={this.setAgentList.bind(this)} picList={this.props.authInfo.authentication.agent.files}/>
                        <p style={{textAlign:'left',marginTop:0,marginLeft:45}}>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.infofour')}</p>
                        <p>
                            <Button onClick={this.showConfirm.bind(this)} type="primary" style={{marginRight:50}}>{I18n.t('PerCenterRight.save')}</Button>
                            <Button type="ghost" onClick={this.Cancel.bind(this)}>{I18n.t('quxia')}</Button>
                        </p>
                    </div>
                )
            }else if(this.props.authInfo.authentication.agent.files.length != 0 && this.props.authInfo.authentication.agent.verify == false){//等待审核
                return(
                    <div className="SellerCenterAgentBoxWait">
                        <div className="SellerCenterAgentBoxWait-top">
                            <p>
                                <div className="left">
                                    <img src={(config.theme_path + 'bigwatch.png')} alt=""/>
                                </div>
                                <p className="left">{I18n.t('buyer.waiting')}</p>
                                <div className="clear"></div>
                            </p>
                            <p>{I18n.t('buyer.please')}</p>
                        </div>
                        <div className="SellerCenterAgentBoxWait-pic">
                            {this.agentPicList()}
                            <p className="clear"></p>
                        </div>
                    </div>
                )
            }else if(this.props.authInfo.authentication.agent.files.length != 0 && this.props.authInfo.authentication.agent.verify == true){//审核失败
                return(
                    <div className="SellerCenterAgentBoxWait">
                        <div className="SellerCenterAgentBoxWait-top">
                            <p>
                                <div className="left">
                                    <img src={(config.theme_path + 'emailerror.png')} alt=""/>
                                </div>
                                <p className="left">{I18n.t('error')}</p>
                                <div className="clear"></div>
                            </p>
                            <p>{I18n.t('BuyerBox.errorreson')} 认证失败</p>
                            <p>{I18n.t('CompantInvoics.careful')} <span onClick={this.Again.bind(this)} style={{color:'#0c5aa2',cursor:'pointer'}}>{I18n.t('CompantInvoics.again')}</span> </p>
                        </div>
                        <div className="SellerCenterAgentBoxWait-pic">
                            {this.agentPicList()}
                            <p className="clear"></p>
                        </div>
                    </div>
                )
            }
        }
    }
    render(){
        if (this.props.authInfo == null) {
            return <div className="SellerCenterAgentBox"></div>;
        }
        else {
            return(
                <div className="SellerCenterAgentBox">
                    {this.ShowTop()}
                    {this.ShowResult()}
                </div>
            );
        }
    }
}
