import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Table,Radio,Checkbox, DatePicker, Form, InputNumber, Steps, Button,Input,Cascader , Col, Modal ,message,Badge,Icon, Select} from 'antd';
import { createContainer } from 'meteor/react-meteor-data';
import { Product } from '../../api/Product.js';
const FormItem = Form.Item;
const createForm = Form.create;
import Selectcity from '../Selectcity.jsx';
let InvetoryVerificationForm = React.createClass({
    getInitialState() {
        return {
            address: '',
        };
    },
    handleSubmit(e) {
        e.preventDefault();
        if(this.state.address == '请选择 请选择 请选择'){
            message.error('请选择仓库地址！');
        }else{
            this.props.form.validateFields((errors, values) => {
                console.log(values);
                if (errors) {
                    console.log('Errors in form!!!');
                    return;
                }
                else {
                    Meteor.call('seller.addStockVerify',values.name, values.phone, this.state.address, values.address);
                    Meteor.call('authentication.addStockVerify',values.name, values.phone, this.state.address, values.address);
                    this.props.return();
                }
            });
        }
    },
    setAddress(text) {
        this.setState({ address: text });
    },
    userPhoneExists(rule, value, callback){
        let react = /^\d{6,}$/;
        if(value!=''){
            if(!react.test(value)){
                callback(I18n.t('Register.phonetishis'));
            }else{
                callback();
            }
        }else{
            callback();
        }

    },
    render(){
        const formItemLayout = {
            labelCol: { span: 7 },
        };
        const { getFieldProps ,getFieldError, isFieldValidating } = this.props.form;
        const nameProps = getFieldProps('name', {
            rules: [
                { required: true, message: I18n.t('statement.nameerror') },
            ],
        });
        const phoneProps = getFieldProps('phone', {
            rules: [
                { required: true, message: I18n.t('statement.phoneerror') },
                { validator: this.userPhoneExists},
            ],
        });
        const addressProps = getFieldProps('address', {
            rules: [
                { required: true, message: I18n.t('statement.words') },
            ],
        });
        return(
            <div>
                <Form inline className="InvetoryVerificationForm">
                    <FormItem
                        label={I18n.t('statement.people')}
                    >
                        <Input type="text" maxLength="10" {...nameProps}   />
                    </FormItem>
                    <FormItem
                        label={I18n.t('buyerinfo.telphone')}
                    >
                        <Input type="text" maxLength="18" {...phoneProps} />
                    </FormItem>
                    <FormItem
                        label={I18n.t('statement.address')}
                    >
                        <Selectcity  func={this.setAddress}/>
                    </FormItem>
                    <FormItem
                        label={I18n.t('statement.discript')}
                        {...formItemLayout}
                    >
                        <Input type="textarea"  maxLength="120" placeholder={I18n.t('return.twomax')} {...addressProps} />
                    </FormItem>
                    <FormItem>
                        <Button type='primary' onClick={this.handleSubmit}>{I18n.t('buyercenter.sure')}</Button>
                    </FormItem>
                </Form>
            </div>
        )
    }
})
InvetoryVerificationForm = createForm()(InvetoryVerificationForm);
export default InvetoryVerificationForm;
