import React, { Component } from 'react';
import { Link } from 'react-router';
import { Button,Icon, Checkbox, Modal,message } from 'antd';
const confirm = Modal.confirm;
import { Translate, I18n } from 'react-redux-i18n';
import { Meteor } from 'meteor/meteor';
import NewTr from './NewTr.jsx';
export default class NewTable extends Component{
    constructor(props){
        super(props);
        this.state = ({
            flag:false,
            checked:[],
            onShelves: '',
            offShelves: '',
            all: [],
            selectedRowKeys: [],
            arr:[],
        })
    }
    componentDidMount(){
        // let arr = [];
        // for(let i=0;i<this.props.date.length;i++){
        //     if(i == 0){
        //         arr.push(this.props.date[0]);
        //     }else{
        //         for(let j =0 ; j<arr.length; j++){
        //             if(this.props.date[i].model<arr[j].model){
        //                 arr.unshift(this.props.date[i]);
        //             }else{
        //                 arr.push(this.props.date[i]);
        //             }
        //         }
        //     }
        // }
        // this.setState({arr:arr});
    }
    clickOn() {
        let checked = [];
        for(let i = 0; i < this.props.date.length; i++){
            checked[i] = false;
        }
        this.setState({checked: checked, selectedRowKeys: []});
        for (let i = 0; i < this.state.selectedRowKeys.length; ++ i) {
            Meteor.call('stock.showUpStock', this.state.selectedRowKeys[i]);
        }
        if (this.state.selectedRowKeys.length) {
            message.success('成功上架！');
        } else {
            message.warn('请选择上架型号！');
        }
    }
    clickOff(){
        let checked = [];
        for(let i = 0; i < this.props.date.length; i++){
            checked[i] = false;
        }
        this.setState({checked: checked, selectedRowKeys: []});
        for (let i = 0; i < this.state.selectedRowKeys.length; ++ i) {
            Meteor.call('stock.outOfStock', this.state.selectedRowKeys[i]);
        }
        if (this.state.selectedRowKeys.length) {
            message.success('成功下架！');
        } else {
            message.warn('请选择下架型号！');
        }
    }
    clickDelete(){
        {/*删除事件*/}
        let selectedRowKeys = this.state.selectedRowKeys;
        let date = this.props.date;
        let This = this;
        for(let i =0 ;i<this.state.checked.length;i++){
            if(this.state.checked[i] == true){
                confirm({
                    title: I18n.t('management.top'),
                    content: I18n.t('management.del'),
                    onOk() {
                        let checked = [];
                        for(let i = 0; i < date.length; i++){
                            checked[i] = false;
                        }
                        This.setState({checked: checked, selectedRowKeys: []});
                        for (let i = 0; i < selectedRowKeys.length; ++ i) {
                            Meteor.call('stock.deleteStock', selectedRowKeys[i]);
                        }
                    },
                    onCancel() {},
                });
                return;
            }else{

            }
        }

    }
    handleOk(){
        {/*删除弹窗确认事件*/}
        console.log('delete'+this.state.selectedRowKeys);
    }
    clickAll(){
        let current = this.props.current;
        let index = (this.props.current-1)*10;
        let checked = this.state.checked;
        let len = this.props.date.length;


        if(this.state.all[current] == false){
            this.state.all[current] = true
            this.setState({all: this.state.all});
            for(let i = 0; i < len; i++){
                checked[i+index] = true;
                this.pushKey(this.props.date[i].key);
            }
            this.setState({checked: checked});
        }else{
            this.state.all[current] = false;
            this.setState({all: this.state.all});
            for(let i = 0; i < len; i++){
                checked[i+index] = false;

                this.removeKey(this.props.date[i].key);
            }
            this.setState({checked: checked});
        }
    }
    click(){
        this.setState({flag:!this.state.flag});
    }
    pushKey(key){
        if(this.state.selectedRowKeys.indexOf(key) == -1){
            this.state.selectedRowKeys.push(key);
        }
        console.log('pushkey:');
        console.log(this.state.selectedRowKeys);
    }
    removeKey(key){
        for(let i = 0,len = this.state.selectedRowKeys.length; i < len ; i++){
            if(this.state.selectedRowKeys[i] == key){
                this.state.selectedRowKeys.splice(i,1);
            }
        }
        console.log('removekey:');
        console.log(this.state.selectedRowKeys);
    }
    clickCheckbox(i){
        let current = this.props.current;
        let index = (this.props.current-1)*10;
        let len = this.props.date.length;
        this.state.checked[i+index] = !this.state.checked[i+index];
        this.setState({checked:this.state.checked});
        let flag = 0;
        if(this.state.checked[i+index] == true){
            this.pushKey(this.props.date[i].key);
        }else{
            this.removeKey(this.props.date[i].key);
        }
        for(let j = 0; j < this.state.checked.length; j++){
            if (this.state.checked[j+index] == true){
                flag++;
            }

        }
        if (flag == len) {
            this.state.all[current] = true;
            this.setState({all: this.state.all});
        }
        else {
            this.state.all[current] = false;
            this.setState({all: this.state.all});
        }
    }
    showTop(){
        let temp =[];
        temp[0] = (
            <div className="showTop">
                <Checkbox  className="checkbox1" checked={this.state.all[this.props.current]}  onClick={this.clickAll.bind(this)}>{I18n.t('PersonalCenter.manageStock.select')}</Checkbox>
                <Button className={this.state.onShelves} onClick={this.clickOn.bind(this)}>{I18n.t('PersonalCenter.manageStock.on')}</Button>
                <Button className={this.state.offShelves} onClick={this.clickOff.bind(this)}>{I18n.t('PersonalCenter.manageStock.off')}</Button>
                <span className="a1" onClick={this.clickDelete.bind(this)} >{I18n.t('PersonalCenter.manageStock.deleteSelect')}</span>
            </div>
        )
        return temp;
    }
    showBottom(){
        let temp = [];
        temp [0] = (
            <div className="tableBottom">
                <Checkbox checked={this.state.all[this.props.current]} className="checkbox1"  onClick={this.clickAll.bind(this)}>{I18n.t('PersonalCenter.manageStock.select')}</Checkbox>
                <span className="a1" onClick={this.clickDelete.bind(this)} >{I18n.t('PersonalCenter.manageStock.deleteSelect')}</span>
            </div>
        )
        return temp;
    }
    ShowTh(){
        let TH = [];
        for(let i=0;i<this.props.columns.length;i++){
            TH[i] = (
                <th key={this.props.columns[i].dataIndex}>
                    {this.props.columns[i].title}
                </th>
            )
        }
        TH.unshift(<th></th>);
        return TH;
    }
    ShowTr(){
        let TR = [];
        let index = (this.props.current-1)*10;
        for(let i = 0;i<this.props.date.length;i++){
            TR[i] = (
                <NewTr date={this.props.date[i]} checked={this.state.checked[i+index] }  changeflag={this.props.changeflag} clickCheckbox={this.clickCheckbox.bind(this,i)}/>
            )
        }
        return TR;
    }
    render(){
        return(
            <div>
                {this.showTop()}
                <div style={{overflow:'auto'}}>
                    <table className="MyTable MyNewTble" style={{overflow:'auto',width:1800}}>
                        <tr>
                            {this.ShowTh()}
                        </tr>
                        {this.ShowTr()}
                    </table>
                </div>
                {this.showBottom()}
            </div>
        )
    }
}
