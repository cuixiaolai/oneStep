import React, { Component } from 'react';
import { Button,Checkbox } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router';
class OpenServiceBox extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            checked: false
        });
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(e) {
        this.setState({
            checked: e.target.checked
        });
    }
    handleClick(checked) {
        this.props.handle(checked);
        this.setState({
            checked: false
        });
    }
    render() {
        return (
            <div className="open-service-box-shade" style={this.props.style}>
                <p style={{textAlign:'right'}}><span>×</span></p>
                <div className="open-service-box">
                    <p className="bold-font">{I18n.t('buyercenter.knowService')}</p>
                    <div className="agree-content">
                        <p className="name">一、使用规则</p>
                        <p>您在使用本网站时必须遵守如下规则：</p>
                        <p>（1）遵守国家有关法律和政策等，维护国家利益，保护国家安全；</p>
                        <p>（2）遵守所有与本网站相关的网络协议、规定和程序；</p>
                        <p>（3）仅通过ICxyz商城提供或许可的合法通道登陆使用本网站；</p>
                        <p>（4）不得为任何非法目的而使用本网站；</p>
                        <p>（5）不得利用本网站系统进行任何可能对互联网的正常运行造成不利影响的行为；</p>
                        <p>（6）不得利用本网站系统进行任何不利于ICxyz商城的行为；</p>
                        <p>（7）如发现任何账户出现安全漏洞或被非法使用的情况，应立即通知ICxyz商城；</p>
                        <p>（8）不得使用任何“机器人”、“蜘蛛”等自动或手动方式访问、获取、复制或检测本网站全部或部分内容，或者扫描、探测本网站之弱点。</p>
                        <p className="name">二、服务中断或终止</p>
                        <p>1. 为完善本网站内容和提升您的用户体验，ICxyz商城需不定期对本网站进行维护、升级或调整，由此可能造成中断、停止本网站之正常运行。</p>
                        <p>2. 如发生下列任何一种情形，ICxyz商城无需通知可随时中断或终止质保服务，对因此而产生的不便或损失，ICxyz商城不承担任何责任：</p>
                        <p>（1）服务器遭受损坏，无法正常运行；</p>
                        <p>（2）突发性的软硬件设备与电子通信设备故障；</p>
                        <p>（3）网络提供商线路或其它第三方原因；</p>
                        <p>（4）在紧急情况下为维护国家安全或其他用户及第三者之权益；</p>
                        <p>（5）不可抗力。</p>
                        <p>3. 尽管如此，除法律另有规定外，您同意ICxyz商城保留以事先通知方式中断或终止全部或部分本网站所提供的服务，ICxyz商城不承担责任。</p>
                        <p>4. 若发布新版本，ICxyz商城不保证新版本功能在旧版本可继续使用，因此您应根据提示及时对需更新的软件进行升级。若因您未及时更新而造成的影响或损失，您将自行承担责任。</p>
                        <p className="name">三、账户、密码及安全</p>
                        <p>本网站提供的某些功能可能要求您创设用户名及密码。您有义务保管好个人用户名及密码，定期进行密码重置，并对他人使用您的私人计算机做出限制。您不得以任何形式擅自转让您的用户名及密码。如果因为您的管理不善造成用户名、密码等被复制、被盗用或未经授权而使用，您同意自行承担可能遭受的损失及责任。</p>
                        <p className="name">四、隐私权政策</p>
                        <p>ICxyz商城的《隐私权政策》适用于本网站，其条款是本使用条款不可分割的一部分。您同意ICxyz商城按照上述《隐私权政策》收集、存储、使用、披露和保护您的个人信息。</p>
                        <p className="name">五、责任声明</p>
                        <p>1. 在法律允许的最大范围内，本网站或本网站的任何内容、服务或功能均“按照现状”提供，可能存在瑕疵、错误或故障。ICxyz商城不对本网站或本网站的所有内容作任何形式的明示或默示保证，包括但不限于适销性、质量满意度、适合特定目的、不侵犯第三方权利等保证。</p>
                        <p>2. 除法律禁止外，在任何情况下ICxyz商城不对使用本网站或与本网站的任何内容、服务或功能导致的任何特殊、附带、偶然或间接的损害进行赔偿，即使ICxyz商城已被告知可能发生该等损害，包括但不限于商业利润损失、数据或文档丢失产生的损失、人身伤害、隐私泄漏、因未能履行包括诚信或合理谨慎在内的任何责任、因过失或其他损失。</p>
                        <p>3. 使用本网站需接入互联网，请注意防范网络安全风险。因网络攻击、黑客攻击、病毒感染等原因将导致网站运行异常、信息泄露、数据丢失或其他问题，您在此同意将自行承担风险。</p>
                        <p>4. 您使用本网站时可能需第三方软件或服务支持，可能因第三方原因导致通讯线路故障、技术问题、网络中断、终端故障、系统不稳定或其他突发状况，ICxyz商城不保证第三方服务之安全性、准确性、有效性及其他不确定的风险。由此若引发的任何争议及损害，ICxyz商城不承担任何责任。</p>
                        <p className="name">六、违约责任</p>
                        <p>若您存在违反适用法律或本条款项下的行为，ICxyz商城有权依据该行为性质，无需事先通知即可采取包括但不限于中断服务、限制使用、终止服务、追究法律责任等措施，并且您同意承担由此给ICxyz商城或第三方造成的全部损失。</p>
                    </div>
                    <Checkbox className="service-box-check"
                              onChange={this.handleChange}
                              checked={this.state.checked}>
                        {I18n.t('buyercenter.readAndAgree')}
                        <span className="blue-font"><Link to="/agreement/warranty_ordinance "> {I18n.t('buyercenter.agreementExplain')}</Link></span>
                    </Checkbox>
                    <Button className="service-box-btn"
                            type="primary"
                            onClick={this.handleClick.bind(this,this.state.checked)}
                    >{I18n.t('buyercenter.open')}</Button>
                </div>
            </div>
        );
    }
}

export default OpenServiceBox;