import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal,message,InputNumber,Select } from 'antd';
import config from '../../config.json';
import { createContainer } from 'meteor/react-meteor-data';
import { Seller } from '../../api/Seller.js';
import SellerPayment from './SellerPayment.jsx';
import InvetoryVerificationForm from './InvetoryVerificationForm.jsx';
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;
const Option = Select.Option;

export default class SellerCenterInventoryVerification extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            check:false,
            value:'rmb',
            money:null,
            num:0,
            showflag:false,
            flag:true,
            flags:true,
            alert:false,
        });
        this.Onclick = this.Onclick.bind(this);
    }
    return(){
        this.setState({num: 1});
    }
    one(){
        this.setState({num:0});
    }
    Click(num,nums){
        Meteor.call('seller.payStockVerify');
    }
    Again(){
        this.setState({num:0});
        Meteor.call('seller.again');
    }
    Onclick(){
        confirm({
            title: I18n.t('statement.sure'),
            content: I18n.t('statement.words'),
            onOk() {
                Meteor.call('seller.cancelStockVerify');
            },
            onCancel() {},
        });
    }
    show(){
        if (this.props.sellerInfo != null) {
            if (this.state.num == 1) {
                return(
                    <div className="SellerCenterDoubleIndemnityBox">
                        <p>
                            <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                            <span>{I18n.t('statement.resuccess')}</span>
                        </p>
                        <p style={{marginBottom:60}}>{I18n.t('DoubleIndemnity.wating')}</p>
                    </div>
                )
            }
            else if (this.state.num == 3){
                return(
                    <div className="SellerCenterDoubleIndemnityBox">
                        <p>
                            <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                            <span>{I18n.t('statement.returnsuccess')}</span>
                        </p>
                        <p style={{marginBottom:60}}>{I18n.t('statement.returnsuccesswords')}</p>
                    </div>
                )
            }
            else {
                if(this.props.sellerInfo.stock_verify == null || (this.props.sellerInfo.stock_verify != null && this.props.sellerInfo.stock_verify.length == 0)){
                    return(
                        <InvetoryVerificationForm return={this.return.bind(this)} />
                    )
                }
                else {
                    let state = 0;
                    let price = 0;
                    let reason = '';
                    for (let i = 0; i < this.props.sellerInfo.stock_verify.length; ++ i) {
                        if (this.props.sellerInfo.stock_verify[i].verify == false) {
                            state = 2;
                            break;
                        }
                        else if (this.props.sellerInfo.stock_verify[i].verify == true && this.props.sellerInfo.stock_verify[i].success == false) {
                            state = 3;
                            reason = this.props.sellerInfo.stock_verify[i].reason;
                            break;
                        }
                        else if (this.props.sellerInfo.stock_verify[i].pay == false) {
                            state = 1;
                            price = this.props.sellerInfo.stock_verify[i].price;
                            break;
                        }
                    }
                    if (state == 0) {
                        return(
                            <InvetoryVerificationForm return={this.return.bind(this)} />
                        )
                    }
                    else if (state == 1) {
                        return(
                            <div className="SellerCenterDoubleIndemnityBoxPayment">
                                <p> {I18n.t('statement.payment')} <span style={{color:'#ed7020',fontSize:'22px'}}>￥{price}</span> </p>
                                <SellerPayment Click={this.Click.bind(this)} style={{width:700,margin:'0 auto'}} />
                            </div>
                        )
                    }
                    else if (state == 2) {
                        return(
                            <div className="SellerCenterDoubleIndemnityBox">
                                <p>
                                    <img src={(config.theme_path + 'bigwatch.png')} alt=""/>
                                    <span>{I18n.t('PersonalCenterSecurity.authentication.wait')}</span>
                                </p>
                                <p style={{marginBottom:60}}>{I18n.t('DoubleIndemnity.wating')}</p>
                            </div>
                        )
                    }
                    else if (state == 3) {
                        return(
                            <div className="SellerCenterDoubleIndemnityBox" style={{marginBottom:30}}>
                                <p>
                                    <img src={(config.theme_path + 'error_03.png')} alt=""/>
                                    <span>{I18n.t('statement.error')}</span>
                                </p>
                                <p style={{marginBottom:6}}>{I18n.t('DoubleIndemnity.reason')} {reason} </p>
                                <p style={{textAlign:'right'}}>
                                    <Button type="ghost" onClick={this.Again.bind(this)}>{I18n.t('DoubleIndemnity.again')}</Button>
                                </p>
                            </div>
                        )
                    }
                }
            }
        }
    }
    render(){
        if (this.props.sellerInfo != null) {
            const columns = [{
                title: '联系人',
                dataIndex: 'name',
            }, {
                title: '联系电话',
                dataIndex: 'phone',
            }, {
                title: '仓库地址',
                dataIndex: 'address',
                width:250,
            }, {
                title: '仓库描述',
                dataIndex: 'describe',
            }, {
                title: '申请时间',
                dataIndex: 'time',
            }, {
                title: '申请金额',
                dataIndex: 'money',
                render: (text, record) => (
                    <span>{this.state.flag ? <span>￥</span> : <span>$</span>}{text}</span>
                ),
            }, {
                title: '状态',
                dataIndex: 'state',
            }, {
                title: '操作',
                dataIndex: 'operation',
                render: (text, record) => (
                    <span style={ text == '取消' ? {} : {cursor:'pointer'}} onClick={this.Onclick.bind(this)} >{text}</span>
                ),
            }];
            let dataSource = [];
            if (this.props.sellerInfo.stock_verify != null) {
                for (let i = 0; i < this.props.sellerInfo.stock_verify.length; ++ i) {
                    let date = this.props.sellerInfo.stock_verify[i].date;
                    console.log(this.props.sellerInfo.stock_verify[i]);
                    let month = date.getMonth() + 1;
                    let strDate = date.getDate();
                    if (month >= 1 && month <= 9) {
                        month = "0" + month;
                    }
                    if (strDate >= 0 && strDate <= 9) {
                        strDate = "0" + strDate;
                    }
                    let state = '';
                    let operation = '';
                    if (this.props.sellerInfo.stock_verify[i].verify == false) {
                        state = '未验证';
                    }
                    else if (this.props.sellerInfo.stock_verify[i].verify == true && this.props.sellerInfo.stock_verify[i].success == false) {
                        state = '未通过';
                    }
                    else if (this.props.sellerInfo.stock_verify[i].pay == false) {
                        state = '未支付';
                        operation = '取消';
                    }
                    else {
                        state = '完成';
                    }
                    let item = {
                        name: this.props.sellerInfo.stock_verify[i].username,
                        phone: this.props.sellerInfo.stock_verify[i].phone,
                        address: this.props.sellerInfo.stock_verify[i].address,
                        describe: this.props.sellerInfo.stock_verify[i].describe,
                        time: date.getFullYear()+'-'+month+'-'+strDate,
                        money: this.props.sellerInfo.stock_verify[i].price,
                        state: state,
                        operation: operation,
                    };
                    dataSource.push(item);
                }
            }
            return(
                <div>
                    <Tabs defaultActiveKey="1" className="CompanyRole">
                        <TabPane tab={I18n.t('statement.name')} key="1">
                            {this.show()}
                        </TabPane>
                        <TabPane tab={I18n.t('statement.names')} key="2">
                            <Table  dataSource={dataSource} columns={columns}  pagination={false}  />
                        </TabPane>
                    </Tabs>
                </div>
            )
        }
        else {
            return <div></div>;
        }
    }
}
export default createContainer(() => {
    Meteor.subscribe('seller');
    return {
        sellerInfo: Seller.findOne({_id: Meteor.userId()}, { fields: {'stock_verify': 1}}),
    };
}, SellerCenterInventoryVerification);
