import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal } from 'antd';
import config from '../../config.json';
import Phone from '../center/Phone.jsx';
const confirm = Modal.confirm;

export default class SellerCenterManufacturer extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            AgentList: [],
            state: false,
        });
    }
    setAgentList(arr) {
        let agent = [];
        if (arr.length > 10) {
            agent = arr.slice(0, 10);
        }
        else {
            agent = arr;
        }
        console.log(agent);
        this.setState({AgentList: agent});
    }
    Again() {
        for (let i = 0; i < this.props.authInfo.authentication.manufacturer.files.length; ++ i) {
            $.ajax({
                type: 'POST',
                url: config.file_server + 'delete_seller',
                data: { path: this.props.authInfo.authentication.manufacturer.files[i] },
                dataType: "json",
                success: function (data) {

                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }

            });
        }
        Meteor.call('seller.deleteManufacturerAuthentication');
        Meteor.call('authentication.cancelManufacturerAuthentication', Meteor.userId(), this.props.authInfo.authentication.manufacturer.files);
    }
    Commit() {
        let newList = this.state.AgentList;
        let manufacturer = [];
        for (let i = 0; i < newList.length; ++ i) {
            $.ajax({
                type: 'POST',
                url: config.file_server + 'save_seller',
                data: { path: newList[i]},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
            manufacturer.push(newList[i].replace('tmp/', ''));
        }
        Meteor.call('seller.updateManufacturerAuthentication', manufacturer);
        Meteor.call('authentication.addManufacturerAuthentication', Meteor.userId(), manufacturer);
    }
    handelOK(){
        if (this.state.AgentList.length != 0) {
            let newList = this.state.AgentList;
            let oldList = this.props.authInfo.authentication.manufacturer.files;
            let addList = newList.filter(v => !oldList.includes(v));
            let delList = oldList.filter(v => !newList.includes(v));
            let manufacturer = oldList.filter(v => newList.includes(v));
            for (let i = 0; i < addList.length; ++ i) {
                $.ajax({
                    type: 'POST',
                    url: config.file_server + 'save_seller',
                    data: { path: addList[i]},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
                manufacturer.push(addList[i].replace('tmp/', ''));
            }
            for (let i = 0; i < delList.length; ++ i) {
                $.ajax({
                    type: 'POST',
                    url: config.file_server + 'delete_seller',
                    data: { path: delList[i] },
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }


                });
            }
            Meteor.call('authentication.cancelManufacturerAuthentication', Meteor.userId(), delList);
            if (Roles.userIsInRole(Meteor.userId(), 'manufacturer')) {
                Meteor.call('Roles.removeUsersToRoles', Meteor.userId(), 'manufacturer');
                Meteor.call('stock.allManufacturerOutOfStock');
            }
            Meteor.call('seller.updateManufacturerAuthentication', manufacturer);
            Meteor.call('authentication.addManufacturerAuthentication', Meteor.userId(), manufacturer);
        }
        else {
            let delList = this.props.authInfo.authentication.manufacturer.files;
            for (let i = 0; i < delList.length; ++ i) {
                $.ajax({
                    type: 'POST',
                    url: config.file_server + 'delete_seller',
                    data: { path: delList[i] },
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
            Meteor.call('authentication.cancelManufacturerAuthentication', Meteor.userId(), delList);
            if (Roles.userIsInRole(Meteor.userId(), 'manufacturer')) {
                Meteor.call('Roles.removeUsersToRoles', Meteor.userId(), 'manufacturer');
                Meteor.call('stock.allManufacturerOutOfStock');
            }
            Meteor.call('seller.updateManufacturerAuthentication', []);
        }
        this.setState({ state: false, AgentList: [] });
    }
    showConfirm() {
        confirm({
            title: '修改资质文件',
            content: '资质文件修改后，代理商身份将重新进行审核. 确定修改？',
            okText:'是',
            cancelText:'否',
            closable:true,
            onOk:this.handelOK.bind(this),
            onCancel() {},
        });
    }
    Change() {
        this.setState({ state: true });
    }
    Cancel() {
        this.setState({ state: false });
    }
    manufacturerPicList() {
        let PIC=[];
        if(this.props.authInfo.authentication.manufacturer.files.length != 0){
            for(let i = 0; i < this.props.authInfo.authentication.manufacturer.files.length; ++ i){
                PIC[i] = (
                    <div>
                        <img src={(config.file_server + this.props.authInfo.authentication.manufacturer.files[i])}  alt=""/>
                        <p>{I18n.t('CertificationAudit.ManufacturerWord')}</p>
                    </div>
                )
            }
        }
        return PIC;
    }
    ShowResult(){
        if(this.props.authInfo.authentication != null && this.props.authInfo.authentication.manufacturer.files.length == 0){
            return(
                <div>
                    <div className="SellerCenterManufacturer-photo">
                        <Phone style={{width:'100%',height:'auto'}} setList={this.setAgentList.bind(this)} />
                    </div>
                    <p style={{textAlign:'center'}}><Button type="primary" onClick={this.Commit.bind(this)}>{I18n.t('Addresspage.sure')}</Button> </p>
                </div>
            )
        }else if(this.props.authInfo.authentication != null){
            if(this.props.authInfo.authentication.manufacturer.files.length != 0 && this.props.authInfo.authentication.manufacturer.verify == null && this.state.state == false){//卖家认证以认证成为厂商  或者审核成功？
                return(
                    <div className="SellerCenterAgentSuccess">
                        {this.manufacturerPicList()}
                        <p className="clear"></p>
                        <p><span onClick={this.Change.bind(this)}>{I18n.t('CertificationAudit.change')}</span></p>
                    </div>
                )
            }
            else if(this.props.authInfo.authentication.manufacturer.files.length != 0 && this.props.authInfo.authentication.manufacturer.verify == null && this.state.state == true){//确认修改资质文件
                return(
                    <div className="SellerCenterAgentBoxChange">
                        <Phone style={{width:'100%',height:'auto'}} close={false} setList={this.setAgentList.bind(this)} picList={this.props.authInfo.authentication.manufacturer.files}/>
                        <p style={{textAlign:'left',marginTop:0,marginLeft:45}}>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.infofour')}</p>
                        <p>
                            <Button onClick={this.showConfirm.bind(this)} type="primary" style={{marginRight:50}}>{I18n.t('PerCenterRight.save')}</Button>
                            <Button type="ghost" onClick={this.Cancel.bind(this)}>{I18n.t('quxia')}</Button>
                        </p>
                    </div>
                )
            }else if(this.props.authInfo.authentication.manufacturer.files.length != 0 && this.props.authInfo.authentication.manufacturer.verify == false){//等待审核
                return(
                    <div className="SellerCenterAgentBoxWait">
                        <div className="SellerCenterAgentBoxWait-top">
                            <p>
                                <div className="left">
                                    <img src={(config.theme_path + 'bigwatch.png')} alt=""/>
                                </div>
                                <p className="left">{I18n.t('buyer.waiting')}</p>
                                <div className="clear"></div>
                            </p>
                            <p>{I18n.t('buyer.please')}</p>
                        </div>
                        <div className="SellerCenterAgentBoxWait-pic">
                            {this.manufacturerPicList()}
                            <p className="clear"></p>
                        </div>
                    </div>
                )
            }else if(this.props.authInfo.authentication.manufacturer.files.length != 0 && this.props.authInfo.authentication.manufacturer.verify == true){//审核失败
                return(
                    <div className="SellerCenterAgentBoxWait">
                        <div className="SellerCenterAgentBoxWait-top">
                            <p>
                                <div className="left">
                                    <img src={(config.theme_path + 'error_03.png')} alt=""/>
                                </div>
                                <p className="left">{I18n.t('error')}</p>
                                <div className="clear"></div>
                            </p>
                            <p>{I18n.t('BuyerBox.errorreson')} 认证失败</p>
                            <p>{I18n.t('CompantInvoics.careful')} <span onClick={this.Again.bind(this)} style={{color:'#0c5aa2',cursor:'pointer'}}>{I18n.t('CompantInvoics.again')}</span> </p>
                        </div>
                        <div className="SellerCenterAgentBoxWait-pic">
                            {this.manufacturerPicList()}
                            <p className="clear"></p>
                        </div>
                    </div>
                )
            }
        }
    }
    ShowTop(){
        if(this.props.authInfo.authentication != null && this.props.authInfo.authentication.manufacturer.files.length == 0){//不是厂商
            return(
                <div className="SellerCenterManufacturer-top">
                    <p>
                        <span style={{color:'#ed7020'}}>{I18n.t('buyer.worm')}</span>{I18n.t('CertificationAudit.topname')}
                    </p>
                    <p>{I18n.t('CertificationAudit.chang')}</p>
                </div>
            )
        }else if(this.props.authInfo.authentication != null){
           return(
               <p style={{width:'100%',height:40,background:'#f4f4f4',fontSize:'14px',color:'#707070',paddingLeft:'20px',marginBottom:'50px',lineHeight:'40px',textAlign:'left'}}>{I18n.t('CertificationAudit.Manufacturer')}</p>
           )

        }
    }

    render(){
        if (this.props.authInfo == null) {
            return <div className="SellerCenterAgentBox"></div>;
        }
        else {
            return(
                <div className="SellerCenterAgentBox">
                    {this.ShowTop()}
                    {this.ShowResult()}
                </div>
            );
        }
    }
}