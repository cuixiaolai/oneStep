import React, { Component } from 'react';
import { Select, DatePicker, Modal, Button ,Input ,Menu, Dropdown, Icon,Form ,Tabs, Col, Table, Checkbox, Pagination } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router'
import NewTable from './NewTable.jsx';
/*卖家中心-库存管理-表单*/
export default class SellerCenterManageStockTable extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            key: '',
            keys:[],
            current: 1,
        });
    }
    onChange(page) {
        console.log(page);
        this.setState({
            current: page,
        });
    }
    
    ShowPagination(){
        return(
            <Pagination onChange={this.onChange.bind(this)} showQuickJumper   total={this.props.data.length}   />
        )
    }
    render(){
        let columns = [{
            title: '型号',
            dataIndex: 'model',
        }, {
            title: '厂牌',
            dataIndex: 'manufacturer',
        }, {
            title: '包装方式',
            dataIndex: 'packing',
        }, {
            title: '递增数量',
            dataIndex: 'number',
        }, {
            title: '最小订货量',
            dataIndex: 'minNum',
        }, {
            title: '交易货币',
            dataIndex: 'change',
        }, {
            title: '价格梯度',
            dataIndex: 'price',
        }, {
            title: '库存总量',
            dataIndex: 'totalInventory',
        }, {
            title: '库存所在地',
            dataIndex: 'stockLocation',
        }, {
            title: '批号',
            dataIndex: 'num',
        }, {
            title: '产地',
            dataIndex: 'place',
        }, {
            title: '是否包邮',
            dataIndex: 'yn',
        }, {
            title: '操作',
            dataIndex: 'operation',
        }];
        return(
            <div className="stockTable">
                {/*表*/}
                {this.props.data.length < 10? ( <NewTable columns={columns} date={this.props.data} changeflag={this.props.changeflag} current={this.state.current}/>)
                    : (
                    <div>
                        <NewTable columns={columns}
                                  date={this.props.data.slice(10*(this.state.current-1), 10*this.state.current)}
                                  changeflag={this.props.changeflag}
                                  current={this.state.current}
                        />
                        <div className="ShowPaginationBox">
                            {this.ShowPagination()}
                            <div className="clear"></div>
                        </div>
                    </div>
                    )
                }
                </div>
        );
    }
}
