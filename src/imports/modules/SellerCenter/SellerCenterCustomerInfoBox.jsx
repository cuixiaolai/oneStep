import React, { Component , PropTypes} from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import {Modal,Form,Input,Button,Select} from 'antd';
import DialogBox from '../center/DialogBox.jsx';
import config from '../../config.json';
import { Link, browserHistory } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';
import { Seller } from '../../api/Seller.js';
const confirm = Modal.confirm;
const FormItem = Form.Item;
const createForm = Form.create;
const cityPhone = ['中国 +86','台湾 +886','香港 +852','美国 +1'];


let SellerCenterCustomerInfoBox = React.createClass({
    getInitialState(){
        return{
            contactModifyFlag:false,
            cityOfPhone: '',
            cityOfTelephone: '',
        }
    },
    onChangeIndustry(value) {
        this.setState({
            industry: value,
        });
    },
    HandleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }else {
                let cityOfPhone = this.state.cityOfPhone;
                let cityOfTelephone = this.state.cityOfTelephone;
                if (this.props.seller.contact == null) {
                    if (cityOfPhone == '') {
                        cityOfPhone = cityPhone[0];
                    }
                    if (cityOfTelephone == '') {
                        cityOfTelephone = cityPhone[0];
                    }
                }
                else {
                    if (cityOfPhone == '' && this.props.seller.contact.phone!=null) {
                        cityOfPhone = this.props.seller.contact.phone.split(' ')[0]+' '+this.props.seller.contact.phone.split(' ')[1];
                    }
                    else if(cityOfPhone == '') {
                        cityOfPhone = cityPhone[0];
                    }
                    if (cityOfTelephone == '' && this.props.seller.contact.telephone!=null) {
                        cityOfTelephone = this.props.seller.contact.telephone.split(' ')[0]+' '+this.props.seller.contact.telephone.split(' ')[1];
                    }
                    else if(cityOfTelephone == '') {
                        cityOfTelephone = cityPhone[0];
                    }
                }
                let phone = values.phone == '' ? '' : cityOfPhone+' '+values.phone;
                let telephone = values.telephone == '' ? '' : cityOfTelephone+' '+values.telephone;
                Meteor.call('seller.updateContactInfo', values.name, phone, telephone, values.email);
                this.setState({contactModifyFlag:false});
                //insert contact_info successfully

            }
        });
    },
    Change(){
        confirm({
            title: 'Want to delete these items?',
            content: 'When clicked the OK button, this dialog will be closed after 1 second',
            onOk() {
                
            },
            onCancel() {},
        });
    },
    handleOk() {
        $.ajax({
            type: 'POST',
            url: config.file_server + 'delete_invoice',
            data: { path: this.props.user_info.invoice.tax, path1: this.props.user_info.invoice.tax_payer, path2: this.props.user_info.invoice.attorney },
            dataType: "json",
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
        Meteor.call('buyer.deleteInvoice', function(err){
            if (err) {
                console.log(err);
            }
            else {
                Meteor.call('authentication.cancelInvoiceAuthentication', Meteor.userId());
                browserHistory.replace('/personal_center/invoics');
            }
        });
    },
    contactModify(flag) {
        this.setState({contactModifyFlag:flag});
    },
    handlePhoneProvinceChange(value) {
        this.setState({cityOfPhone:value}, function () {
            this.props.form.validateFields(['phone'], { force: true });
        });
    },
    handleTelephoneProvinceChange(value) {
        this.setState({cityOfTelephone:value}, function () {
            this.props.form.validateFields(['telephone'], { force: true });
        });
    },
    checkTelephoneOfChina(rule, value, callback) {
        if (!value) {
            if(this.props.form.getFieldValue('phone')==''){
                callback([new Error('请填写至少一种联系方式')]);
            }else{
                callback();
            }
        } else {
            setTimeout(() => {
                const reg= /^0\d{2,3}-\d{7,8}(-\d{1,4})?$/;
                if(reg.test(value)){
                    callback();
                }else {
                    callback([new Error('号码错误')]);
                }
            }, 100);
        }
    },
    checkLowerEmpty(rule, value, callback){
        if(!value&&this.props.form.getFieldValue('telephone')==''){
            callback([new Error('请填写至少一种联系方式')]);
        } else {
            callback();
        }
    },
    checkUpperEmpty(rule, value, callback){
        this.props.form.validateFields(['phone'], { force: true });
        callback();
    },
    render(){
        if (this.props.user_info != null && this.props.seller != null) {
            const { getFieldProps ,getFieldError, isFieldValidating} = this.props.form;
            const nameProps = getFieldProps('name', {
                initialValue:this.props.seller.contact!=null?this.props.seller.contact.name:'',
                rules: [
                    { required: true, min: 2,max:8,message:I18n.t('buyerinfo.nameerror')},
                ],
            });
            
            let phone=this.props.seller.contact==null||this.props.seller.contact.phone=='' ? '' : this.props.seller.contact.phone.split(' ')[2]
            let default_cityOfPhone =this.props.seller.contact==null||this.props.seller.contact.phone=='' ? '中国 +86': this.props.seller.contact.phone.split(' ')[0] + ' ' + this.props.seller.contact.phone.split(' ')[1];

            let phoneProps;

            if (this.state.cityOfPhone == '') {
                if(this.props.seller.contact==null || this.props.seller.contact.phone==''){
                    this.setState({cityOfPhone: '中国 +86'});
                    default_cityOfPhone='中国 +86';
                }
                
                switch (default_cityOfPhone) {
                    case cityPhone[0]:
                        phoneProps  = getFieldProps('phone', {
                            initialValue: phone,
                            rules: [
                                { min: 11, max: 11, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkLowerEmpty},
                            ],
                        });

                        break;
                    case cityPhone[1]:
                        phoneProps  = getFieldProps('phone', {
                            initialValue: phone,
                            rules: [
                                { min: 9, max: 9, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkLowerEmpty},
                            ],
                        });
                        break;
                    case cityPhone[2]:
                        phoneProps  = getFieldProps('phone', {
                            initialValue: phone,
                            rules: [
                                { min: 8, max: 8, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkLowerEmpty},
                            ],
                        });
                        break;
                    case cityPhone[3]:
                        phoneProps  = getFieldProps('phone', {
                            initialValue: phone,
                            rules: [
                                { min: 10, max: 10, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkLowerEmpty},
                            ],
                        });
                        break;
                    default:
                        break;
                }
            }
            else {
                switch (this.state.cityOfPhone) {
                    case cityPhone[0]:
                        phoneProps  = getFieldProps('phone', {
                            initialValue: phone,
                            rules: [
                                { min: 11, max: 11, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkLowerEmpty},
                            ],
                        });
                        break;
                    case cityPhone[1]:
                        phoneProps  = getFieldProps('phone', {
                            initialValue: phone,
                            rules: [
                                { min: 9, max: 9, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkLowerEmpty},
                            ],
                        });
                        break;
                    case cityPhone[2]:
                        phoneProps  = getFieldProps('phone', {
                            initialValue: phone,
                            rules: [
                                { min: 8, max: 8, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkLowerEmpty},
                            ],
                        });
                        break;
                    case cityPhone[3]:
                        phoneProps  = getFieldProps('phone', {
                            initialValue: phone,
                            rules: [
                                { min: 10, max:10, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkLowerEmpty},
                            ],
                        });
                        break;
                    default:
                        break;
                }
            }

            let telephone=this.props.seller.contact==null||this.props.seller.contact.telephone=='' ? '' : this.props.seller.contact.telephone.split(' ')[2]
            let default_cityOfTelephone =this.props.seller.contact==null||this.props.seller.contact.telephone=='' ? '中国 +86': this.props.seller.contact.telephone.split(' ')[0] + ' ' + this.props.seller.contact.telephone.split(' ')[1];

            let telephoneProps;

            if (this.state.cityOfTelephone == '') {

                if(this.props.seller.contact==null || this.props.seller.contact.telephone==''){
                    this.setState({cityOfTelephone: '中国 +86'});
                    default_cityOfTelephone='中国 +86';
                }
   

                switch (default_cityOfTelephone) {
                    case cityPhone[0]:
                        telephoneProps  = getFieldProps('telephone', {
                            initialValue: telephone,
                            rules: [
                                { min: 1, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkTelephoneOfChina },
                            ],
                        });
                        break;
                    case cityPhone[1]:
                        telephoneProps  = getFieldProps('telephone', {
                            initialValue: telephone,
                            rules: [
                                { min: 9, max: 9, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkUpperEmpty},
                            ],
                        });
                        break;
                    case cityPhone[2]:
                        telephoneProps  = getFieldProps('telephone', {
                            initialValue: telephone,
                            rules: [
                                { min: 8, max: 8, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkUpperEmpty},
                            ],
                        });
                        break;
                    case cityPhone[3]:
                        telephoneProps  = getFieldProps('telephone', {
                            initialValue: telephone,
                            rules: [
                                { min: 10, max: 10, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkUpperEmpty},
                            ],
                        });
                        break;
                    default:
                        break;
                }
            }
            else {
                switch (this.state.cityOfTelephone) {
                    case cityPhone[0]:
                        telephoneProps  = getFieldProps('telephone', {
                            initialValue: telephone,
                            rules: [
                                { min: 1, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkTelephoneOfChina },
                            ],
                        });
                        break;
                    case cityPhone[1]:
                        telephoneProps  = getFieldProps('telephone', {
                            initialValue: telephone,
                            rules: [
                                { min: 9, max: 9, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkUpperEmpty},
                            ],
                        });
                        break;
                    case cityPhone[2]:
                        telephoneProps  = getFieldProps('telephone', {
                            initialValue: telephone,
                            rules: [
                                { min: 8, max: 8, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkUpperEmpty},
                            ],
                        });
                        break;
                    case cityPhone[3]:
                        telephoneProps  = getFieldProps('telephone', {
                            initialValue: telephone,
                            rules: [
                                { min: 10, max:10, message: I18n.t('EditAddress.phone') },
                                { validator: this.checkUpperEmpty},
                            ],
                        });
                        break;
                    default:
                        break;
                }
            }
            const provinceOptions = cityPhone.map(province => <Option key={province}>{province}</Option>);
            const emailProps = getFieldProps('email', {
                initialValue:this.props.seller.contact!=null?this.props.seller.contact.email:'',
                rules: [
                    { type: 'email', message:  I18n.t('Register.pleaseemailTishi')},
                ],
            });
        
            return(
                <div>
                    <div className="SellerCenterCustomerInfoBox">
                        <div className="SellerCenterCustomerInfoBox-name">
                            <span>1</span>
                            <span>{I18n.t('buyerinfo.coninfo')}</span>
                        </div>
                        <div className="SellerCenterCustomerInfoBox-inner">
                            <div>
                                <span>{I18n.t('CompantInvoicsThree.comname')}</span>
                                {this.props.user_info.authentication.company}
                            </div>
                            <div>
                                <span>{I18n.t('CompantInvoicsThree.address')}</span>
                                {this.props.user_info.authentication.address}
                            </div>
                            <div>
                                <span>{I18n.t('buyerinfo.comwww')}</span>
                                {this.props.user_info.company_url}
                            </div>
                            <div>
                                <span>{I18n.t('buyerinfo.comin')}</span>
                                <span style={{lineHeight:1.5}}>{this.props.user_info.company_detail}</span>
                            </div>
                            <span><Link to="/personal_center/info">{I18n.t('PerCenterRight.modify')}</Link></span>

                            <div className="clear"></div>
                        </div>
                    </div>
                    <div className="SellerCenterCustomerInfoBox">
                        <div className="SellerCenterCustomerInfoBox-name">
                            <span>2</span>
                            <span>{I18n.t('buyerinfo.comzhang')}</span>
                        </div>
                        <div className="SellerCenterCustomerInfoBox-inner">
                            <div>
                                <span>{I18n.t('CompantInvoicsThree.comname')}</span>
                                { (this.props.user_info.invoice != null && this.props.user_info.invoice.verify == true && this.props.user_info.invoice.success == true)
                                    ? this.props.user_info.invoice.name
                                    : I18n.t('PersonalCenter.company.noWrite')
                                }
                            </div>
                            <div>
                                <span>{I18n.t('buyerinfo.comband')}</span>
                                { (this.props.user_info.invoice != null && this.props.user_info.invoice.verify == true && this.props.user_info.invoice.success == true)
                                    ? this.props.user_info.invoice.bank_name
                                    : I18n.t('PersonalCenter.company.noWrite')
                                }
                            </div>
                            <div>
                                <span>{I18n.t('buyerinfo.bandnum')}</span>
                                { (this.props.user_info.invoice != null && this.props.user_info.invoice.verify == true && this.props.user_info.invoice.success == true)
                                    ? this.props.user_info.invoice.bank_account
                                    : I18n.t('PersonalCenter.company.noWrite')
                                }
                            </div>

                            { (this.props.user_info.invoice != null && this.props.user_info.invoice.verify == true && this.props.user_info.invoice.success == true)
                                ? (<DialogBox handleOk={this.handleOk} style={{marginBottom:0}} className="dialogBox " words={I18n.t('PersonalCenter.company.moremessage')} path='/personal_center/invoics' content={I18n.t('PersonalCenterSecurity.card.modify')} authentication={this.props.user_info.authentication} />)
                                : (<span><Link to="/personal_center/invoics">{I18n.t('PersonalCenter.company.perfect')}</Link></span>)
                            }
                            <div className="clear"></div>
                        </div>
                    </div>

                    <div className="SellerCenterCustomerInfoBox">
                        <div className="SellerCenterCustomerInfoBox-name">
                            <span>3</span>
                            <span>{I18n.t('buyerinfo.addre')}</span>
                        </div>
                        
                            {(this.props.seller.contact==null || this.state.contactModifyFlag)?
                            (
                            <div className="SellerCenterCustomerInfoBox-inner">
                                <Form form={this.props.form} inline className="SellerCenterCustomerInfoBox-form">
                                    <FormItem
                                        label={I18n.t('buyerinfo.Sale')}
                                    >
                                        <Input {...nameProps}/>
                                    </FormItem>
                                    <FormItem
                                        label={I18n.t('buyerinfo.phone')}
                                    >
                                        <Select size="large"  defaultValue={default_cityOfPhone} style={{width:100,height:36,marginRight:10}} onChange={this.handlePhoneProvinceChange}>
                                            {provinceOptions}
                                        </Select>
                                        <Input style={{width:190,marginRight:0}} {...phoneProps} />
                                    </FormItem>
                                    <FormItem
                                        label={I18n.t('buyerinfo.telphone')}
                                    >
                                        <Select size="large"  defaultValue={default_cityOfTelephone} style={{width:100,height:36,marginRight:10}} onChange={this.handleTelephoneProvinceChange}>
                                            {provinceOptions}
                                        </Select>
                                        <Input style={{width:190,marginRight:0}} placeholder={I18n.t('buyerinfo.qu')} {...telephoneProps} />
                                    </FormItem>
                                    <FormItem
                                        label={I18n.t('buyerinfo.email')}
                                    >
                                        <Input type="email" name="email" {...emailProps} />
                                    </FormItem>
                                </Form>
                                <span onClick={this.HandleSubmit} >{I18n.t('PerCenterRight.save')} </span>
                                <div className="clear"></div>
                            </div>
                            )
                            :(
                            <div className="SellerCenterCustomerInfoBox-inner">
                                <div>
                                    <span>{I18n.t('buyerinfo.Sale')}：</span>
                                    {(this.props.seller.contact.name=='') ? I18n.t('PersonalCenter.company.noWrite') : this.props.seller.contact.name}
                                </div>
                                <div>
                                    <span>{I18n.t('buyerinfo.phone')}：</span>
                                    {(this.props.seller.contact.phone=='') ? I18n.t('PersonalCenter.company.noWrite') : this.props.seller.contact.phone.split(' ')[1]+' '+this.props.seller.contact.phone.split(' ')[2]}
                                </div>
                                <div>
                                    <span>{I18n.t('buyerinfo.telphone')}：</span>
                                    {(this.props.seller.contact.telephone=='') ? I18n.t('PersonalCenter.company.noWrite') : this.props.seller.contact.telephone.split(' ')[1]+' '+this.props.seller.contact.telephone.split(' ')[2]}
                                </div>
                                <div>
                                    <span>{I18n.t('buyerinfo.email')}：</span>
                                    {(this.props.seller.contact.email=='') ? I18n.t('PersonalCenter.company.noWrite') : this.props.seller.contact.email}
                                </div>
                                <span onClick={this.contactModify.bind(this,true)}>修改</span>
                                <div className="clear"></div>
                            </div>
                            )}

                    </div>

                </div>
            );
        }
        else {
            return <div className="SellerCenterCustomerInfoBox"></div>;
        }
    }
})
SellerCenterCustomerInfoBox = createForm()(SellerCenterCustomerInfoBox);
export default SellerCenterCustomerInfoBox;

SellerCenterCustomerInfoBox.propTypes = {
    seller: PropTypes.object.isRequired,
};
export default createContainer(() => {
    Meteor.subscribe('seller');
    return {
        seller:Seller.findOne({_id: Meteor.userId()}, { fields: {'contact': 1} }),
    };
}, SellerCenterCustomerInfoBox);