import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import {  Upload,Form,Input,Button,message} from 'antd';

import { NewProduct } from '../../api/Product.js';

const FormItem = Form.Item;
const createForm = Form.create;
import config from '../../config.json';
import SelectShop from '../SelectShop.jsx'

let SellerCenterUploadStockAdd = React.createClass({
    getInitialState() {
        return {
            word:'',
            address:'',
            industry:'',
            flag:false,
        };
    },
    HandleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) =>{
            if (errors) {
                message.error('库存上传失败！');
                return;
            }else{

            }
        })
        const id = this.props.form.getFieldValue('id');
        const manufacturer = this.state.industry;
        console.log(this.props.form);
        if(this.state.flag&&id !== ''&&id&&manufacturer !== ''&&manufacturer) {
            if (this.state.address.match('tmp')) {
                $.ajax({
                    type: 'POST',
                    url: config.file_server + 'save_product',
                    data: { path: this.state.address, old_path: '' },
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
            const filepath = this.state.address.replace('tmp/', '');
            Meteor.call('product.addNewProduct', id, manufacturer, filepath, (err) => {
                if (err) {
                    alert("该型号已存在");
                } else {
                    message.success(`添加成功。`);
                    this.props.form.resetFields();
                    this.setState({industry:'',word:''});
                }
            });

        } else if(id === ''||!id||manufacturer === ''||!manufacturer){
            message.error(`请将信息填写完整。`);
        } else if(!this.state.flag){
            message.error(`请上传文件。`);
        }


    },
    checkId(rule, value, callback) {
        if (!value) {
            callback();
        } else {
            setTimeout(() => {
                Meteor.call('product.checkNewProduct', value, (err) => {
                    if (err && err.error == 'product id used') {
                        callback([new Error('该型号已存在')]);
                    } else if(err && err.error == 'product id auditing'){
                        callback([new Error('该型号审核中......')]);
                    } else {
                        callback();
                    }
                });
            }, 800);
        }
    },
    handleChange(info) {
        console.log(info.file, info.fileList);
        let word=info.file.name;
        this.setState({word:word});

        if (info.file.status === 'done') {
            let path = info.file.response[0].path;
            path = path.replace(/public/, '');
            this.setState({address:path});
            this.setState({flag:true});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    },

    onChangeIndustry(value){
        this.state.industry = value;
        this.setState({ industry:this.state.industry });
    },
    Closeindustry(){
        this.state.industry = '';
        this.setState({ industry:this.state.industry });
    },
    render() {
        const propu ={
            name: 'file',
            showUploadList: false,
            action:config.file_server + 'product',
            onChange:this.handleChange,
            beforeUpload(file){
                let isJPG;
                if(file.size > 4194304){
                    isJPG = false;
                    message.error('上传文件不可超过4M！');
                }else{
                    if(file.type !== 'application/pdf'){
                        isJPG = false;
                        message.error('只能上传PDF的文件！');
                    }else{
                        isJPG = true;
                    }
                }
                return isJPG;
            }
        };
        const { getFieldProps, getFieldError, isFieldValidating  } = this.props.form;
        const idProps = getFieldProps ('id',{
            rules: [
                { required: true, min:1, message:'型号不能为空' },
                { validator: this.checkId },
            ],
        });

        return(
            <div className="SellerCenterUploadStockAdd">
                <div className="SellerCenterUploadStockAddBox">
                    <Form form={this.props.form} inline className="SellerCenterUploadStockForm">
                        <FormItem
                        required
                        hasFeedback
                        label={I18n.t('buyerForm.type')}
                        >
                            <Input type="text" {...idProps}/>
                        </FormItem>
                        <FormItem
                        required
                        hasFeedback
                        label={I18n.t('buyerForm.com')}
                        >
                            <SelectShop func={this.onChangeIndustry} Closecircle={this.Closeindustry} style={{width:280,height:32}} value={this.state.industry} />
                        </FormItem>
                        <FormItem
                        required
                        label={I18n.t('buyerForm.PDF')}
                        >
                            <div className="UploadStockPDF">
                            {this.state.word}
                                <Upload {...propu}>
                                    <div className="UploadStockPDF-btn">
                                    {I18n.t('buyerForm.searchfile')}
                                    </div>
                                </Upload>
                            </div>
                        </FormItem>
                        <FormItem>
                            <Button type="primary" onClick={this.HandleSubmit}>{I18n.t('submit')}</Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
            )
    }
})
SellerCenterUploadStockAdd = createForm()(SellerCenterUploadStockAdd);
export default SellerCenterUploadStockAdd;