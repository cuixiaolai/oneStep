import React, { Component } from 'react';
import { Button } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';

class ClientRemarksBox extends Component {
    constructor(props) {
        super(props);
        this.state = ({
           textareaValue: ''
        });
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            textareaValue: nextProps.text
        });
    }
    handleChange(event) {
        console.log(event.target.value);
        this.setState({
           textareaValue: event.target.value
        });
    }
    handleClick(text, index) {
        this.props.handle(text, index);
        this.refs.area.value = '';
    }
    render() {
        return (
            <div style={this.props.dis} className="client-remarks-box">
                <textarea
                    ref="area"
                    className="remarks-box-area"
                    placeholder={this.state.textareaValue}
                    rows="10"
                    onChange={this.handleChange}
                />
                <Button
                    className="remarks-box-btn"
                    type="primary"
                    onClick={this.handleClick.bind(this, this.state.textareaValue, this.props.index)}
                >{I18n.t('buyercenter.sure')}
                </Button>
            </div>
        );
    }
}

export default ClientRemarksBox;