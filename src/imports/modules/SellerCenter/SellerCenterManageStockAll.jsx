import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import config from '../../config.json';
import { Select, DatePicker, Modal, Button ,Input,InputNumber ,Menu, Dropdown, Icon,Form ,Tabs, Col, Table, Checkbox, Pagination } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import SellerCenterManageStockTable from './SellerCenterManageStockTable.jsx';
import SellerCenterManageStockData from './SellerCenterManageStockData.jsx';
import SelectShop from '../SelectShop.jsx';
import Packing from '../Packings.jsx';
import Stocklocation from '../Stocklocation.jsx';
import classNames from 'classnames';
import { createContainer } from 'meteor/react-meteor-data';
import { Cart } from '../../api/Cart.js';
import { Product } from '../../api/Product.js';
/*卖家中心-库存管理-全部*/
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const InputGroup = Input.Group;
const Option = Select.Option;

export default class SellerCenterManageStockAll extends Component{
    constructor(props) {
        super(props);
        console.log(props);
        this.state = ({
            key:'',
            stocktab: true,
            selectRowKey: '',
            text1: '',
            model:'',//型号
            industry: I18n.t('buyer.Manufacturer'),//厂商
            shipping: '',//包邮
            PackingsArr: ['', ''],//包装方式
            packing: '',
            total1: 0,//库存总量
            total2: 0,
            total:[['', '']],
            ShowAddress:I18n.t('buyerForm.searchp'),//库存所在地
            value:'',
            data: [],
            focus: false,
        });
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeTotal1 = this.handleChangeTotal1.bind(this);
        this.handleChangeTotal2 = this.handleChangeTotal2.bind(this);
        this.onChangeIndustry = this.onChangeIndustry.bind(this);
        this.Closeindustry = this.Closeindustry.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }
    //型号
    handleChange(e) {
        this.setState({value: e});
        if (e == '') {
            this.setState({data: []});
        }
        else {
            let q = '^' + e.toUpperCase();
            let result = [];
            let cursor = Product.find({"Manufacturer Part Number": {$regex: q}}, {fields: {'Manufacturer Part Number': 1, '_id': 0}, sort: ['Manufacturer Part Number','asc'], limit: 15});
            cursor.forEach(function(item) {
                result.push({value: item["Manufacturer Part Number"], text: item["Manufacturer Part Number"]});
            });
            this.setState({data: result});
        }
    }
    handleFocus(e) {
        this.setState({
            focus: e.target === document.activeElement,
            typevalue:e,
        });
        this.setState({modelClass: 'model inputYes'});
    }
    handleBlur(e) {
        this.setState({
            focus: e.target === document.activeElement,
            typevalue:e,
        });
        this.setState({modelClass: 'model', model: e});
    }
    //库存总量
    handleChangeTotal1(value){
        this.setState({total1:value});
    }
    handleChangeTotal2(value){
        this.setState({total2:value});
    }
    //库存筛选-厂商
    onChangeIndustry(value){
        this.setState({ industry: value });
    }
    //包邮
    showShipping(){
        let Shipping = [];
        Shipping[0]=(
            <Select  onChange={this.handleChangeShipping.bind(this)} id="select" size="large" defaultValue={I18n.t('PersonalCenter.manageStock.free')}  style={{ width:100,marginRight:30,height:30 }}>
                <Option value={'是'}>{I18n.t('PersonalCenter.manageStock.freeShipping')}</Option>
                <Option value={'否'}>{I18n.t('PersonalCenter.manageStock.shipping')}</Option>
            </Select>
        )
        return Shipping;
    }
    handleChangeShipping(value) {
        this.setState({shipping: value,});
        console.log(`包邮： ${value}`);
    }
    // 库存筛选-包装方式
    onChangePackings(value) {
        console.log(value);
        if (value.length == 0) {
            this.setState({PackingsArr: value, packing: ''});
        }
        else {
            let str1 = I18n.t('packing.'+value[0]);
            let str2 = I18n.t('packing.'+value[1]);
            this.setState({PackingsArr: value, packing: str1+'/'+str2});
        }
    }
    //库存筛选-库存所在地
    setAddress(text) {
        this.setState({ShowAddress: text});
    }
    Closecircle(){
        this.setState({ShowAddress: I18n.t('buyerForm.searchp')});
    }
    Closeindustry(){
        this.setState({ industry: I18n.t('buyer.Manufacturer') });
    }
    render(){
        const children = [];
        const searchCls = classNames({
            'ant-search-input': true,
            'ant-search-input-focus': this.state.focus,
        });
        const options = this.state.data.map(d => <Option key={d.value}>{d.text}</Option>);
        for (let i = 10; i < 36; i++) {
            children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
        }
      
        return (
            <div>
                {/*全部*/}
                <div className="SearchTop-box">
                    <p>
                        <span>{I18n.t('PersonalCenter.manageStock.filterCondition')}</span>
                        <span>
                            <span style={{display:'inline-block',marginLeft:30,marginRight:10}}>
                                {I18n.t('PersonalCenter.manageStock.model')}
                            </span>
                            <span style={{display:'inline-block',marginRight:30}}>
                                <Input.Group>
                                    <Select
                                        combobox
                                        value={this.state.value}
                                        notFoundContent=""
                                        defaultActiveFirstOption={false}
                                        showArrow={false}
                                        filterOption={false}
                                        onChange={this.handleChange}
                                        onFocus={this.handleFocus}
                                        onBlur={this.handleBlur}
                                        style={{ width: 100 ,height: 30}}
                                    >
                                        {options}
                                    </Select>
                                </Input.Group>
                            </span>
                            <span  style={{display:'inline-block',marginRight:10}}>
                                {I18n.t('PersonalCenter.manageStock.manufacturer')}
                            </span>
                            <SelectShop Closecircle={this.Closeindustry} style={{ width: 100 ,height: 30,display:'inline-block',position:'relative',marginRight:30}} func={this.onChangeIndustry}  value={this.state.industry}/>
                            <span  style={{display:'inline-block',marginRight:10}}>{I18n.t('PersonalCenter.manageStock.freeShipping')}</span>
                            {this.showShipping()}
                            <span  style={{display:'inline-block',marginRight:10}}>
                                {I18n.t('PersonalCenter.manageStock.packingMethod')}
                            </span>
                             <div style={{display:'inline-block'}}>
                                <Packing  onChangePackings={this.onChangePackings.bind(this)} placeholder={I18n.t('buyerForm.searchp')}  value={this.state.PackingsArr} style={{width:100,height:30,display:'inline-block'}}/>
                            </div>
                        </span>
                    </p>
                    <p>
                        <span>
                            {I18n.t('PersonalCenter.manageStock.stockFilter')}
                        </span>
                        <span>
                            <span style={{display:'inline-block',marginLeft:30,marginRight:10}}>{I18n.t('PersonalCenter.manageStock.totalInventory')}</span>
                            <InputNumber  min={0} value={this.state.total1}  onChange={this.handleChangeTotal1}  />
                            <img style={{margin:'0 4px'}} src={(config.theme_path + 'split.png')} alt="图片"/>
                            <InputNumber style={{marginRight:30}} value={this.state.total2}  onChange={this.handleChangeTotal2}   id="" min={this.state.total1} />

                            <span style={{display:'inline-block',marginRight:10}}>{I18n.t('PersonalCenter.manageStock.stockLocation')}</span>
                            <span style={{height:30,lineHeight:'1'}}>
                               <Stocklocation CloseGround={this.Closecircle.bind(this)} func={this.setAddress.bind(this)} placeholder={I18n.t('buyerForm.searchp')} value={this.state.ShowAddress} style={{minWidth:140,width:'auto',height:30,display:'inline-block',position:'relative'}} />
                            </span>
                        </span>
                    </p>
                </div>
                <SellerCenterManageStockData stockInfo={this.props.stockInfo} changeflag={this.props.changeflag} model={this.state.model} industry={this.state.industry} shipping={this.state.shipping} packing={this.state.packing} total1={this.state.total1} total2={this.state.total2} ShowAddress={this.state.ShowAddress} stockAddress={this.state.ShowAddress} />
            </div>
        );
    }
}

SellerCenterManageStockAll.propTypes = {
};
export default createContainer(() => {
    Meteor.subscribe('product.part_number');
    return {
    };
}, SellerCenterManageStockAll);