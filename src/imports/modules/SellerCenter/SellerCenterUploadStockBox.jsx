import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import SellerCenterUploadStockForm from './SellerCenterUploadStockForm.jsx'
import { Table } from 'antd';

export default class SellerCenterUploadStockBox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            flag:false,
            data:'',
        });
    }
    onclick(){
        this.setState({
            flag:true,
        });
    }

    render(){
        return(
            <div>
                <SellerCenterUploadStockForm Link="/seller_center/upload_stock" click={this.onclick.bind(this)} onclick={this.props.click}/>
            </div>
        )
    }
}