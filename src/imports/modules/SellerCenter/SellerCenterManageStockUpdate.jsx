import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Table,Radio,Checkbox, DatePicker, Form, InputNumber, Steps, Button,Input,Cascader , Col, Modal ,message,Badge,Icon, Select} from 'antd';
import config from '../../config.json';
import { createContainer } from 'meteor/react-meteor-data';
import { Product } from '../../api/Product.js';
import { Link, browserHistory } from 'react-router';
import SelectShop from '../SelectShop.jsx';
import classNames from 'classnames';
import Packing from '../Packings.jsx';
import Stocklocation from '../Stocklocation.jsx';
const FormItem = Form.Item;
const createForm = Form.create;
const Option = Select.Option;
const RadioGroup = Radio.Group;

let SellerCenterManageStockUpdate = React.createClass({
    getInitialState() {
        return {
            data: [],
            value: '',
            focus: false,
            industry: I18n.t('buyer.Manufacturer'),
            values: 1,
            answer:1,
            PackingsArr:[[[],0,'']],
            ShowAddress:[[I18n.t('buyerForm.inputaddress'),0]],
            ShowBatch:[['',0]],
            ShowOrigin:[['',0]],
            ShowTi:[[0,1,0.00]],
            getPackings:'',
            getAddnums:'',
            allnum:0,
            getReserveds:'',
            DPA:false,
            COC:false,
            ROHS:false,
            datapacket:false,
            Warranty:false,
            flage:false,
            daufu:false,
            trans_expanse: 0,
            dest_pay: false,
            flag:false,
        };
    },
    componentDidMount(){
        console.log(this.props.stockInfo.package);
        let price = [];
        let allnum = 0;
        for(let i=0;i<this.props.stockInfo.price.length;i++){
            price.push([this.props.stockInfo.price[i].min,this.props.stockInfo.price[i].max,this.props.stockInfo.price[i].price])
        };
        let ShowAddress = [];
        for(let i=0;i<this.props.stockInfo.stock_address.length;i++){
            ShowAddress.push([this.props.stockInfo.stock_address[i].address,this.props.stockInfo.stock_address[i].quantity]);
        };
        for(let i = 0 ; i < ShowAddress.length; i ++){
            allnum = ShowAddress[i][1] + allnum;
        }
        let ShowBatch = [];
        for(let i=0;i<this.props.stockInfo.batch.length;i++){
            ShowBatch.push([this.props.stockInfo.batch[i].num,this.props.stockInfo.batch[i].quantity])
        };
        let ShowOrigin = [];
        for(let i=0;i<this.props.stockInfo.origin_address.length;i++){
            ShowOrigin.push([this.props.stockInfo.origin_address[i].address,this.props.stockInfo.origin_address[i].quantity])
        };

        let PackingsArr = [];
        const options = [{value: 'juandai',label: '卷带'}, {value: 'guanzhuang',label: '管装'}, {value: 'tuopan',label: '托盘'}, {value: 'yuanbaozhuang',label: '原包装'}, {value: 'lingsanbaozhuang',label: '零散包装'},{value: 'weichai',label: '未拆原厂包装'},{value: 'bufen',
            label: '部分原厂包装'},{value: 'gongfang',label: '供方包装'}];
        for(let i=0 ;i<this.props.stockInfo.package.length;i++){
            let arr=this.props.stockInfo.package[i].package.split('/');
            let packingarr = [];
            for(let j=0;j<options.length;j++){
                if(arr[0] == options[j].label){
                    packingarr[0] = options[j].value;
                }
                if(arr[1] == options[j].label){
                    packingarr[1] = options[j].value;
                }
            }
            console.log(packingarr,arr);
            PackingsArr.push([packingarr,this.props.stockInfo.package[i].quantity,this.props.stockInfo.package[i].package]);
        }

        this.setState({
            value:this.props.stockInfo.product_id,
            industry: this.props.stockInfo.manufacturer,
            getPackings:this.props.stockInfo.standard_packing,  //供方标准包装
            getAddnums:this.props.stockInfo.inc_number, //递增数量
            getReserveds:this.props.stockInfo.MOQ, //最小订货量
            values:this.props.stockInfo.currency, //货币
            ShowTi:price,
            ShowAddress:ShowAddress,//库存所在地
            ShowBatch:ShowBatch, //批号
            ShowOrigin:ShowOrigin, //产地
            PackingsArr:PackingsArr, //包装方式
            DPA:this.props.stockInfo.DPA,
            COC:this.props.stockInfo.COC,
            ROHS:this.props.stockInfo.ROHS,
            datapacket:this.props.stockInfo.data_packet, //数据包
            Warranty:this.props.stockInfo.guarantee, //质保
            answer:this.props.stockInfo.free_ship, //是否包邮
            trans_expanse:this.props.stockInfo.trans_expanse,
            dest_pay:this.props.stockInfo.dest_pay,
            allnum:allnum,
        });
    },
    handleChange(e) {
        this.setState({value: e});
        if (e == '') {
            this.setState({data: []});
        }
        else {
            let q = '^' + e.toUpperCase();
            let result = [];
            let cursor = Product.find({"Manufacturer Part Number": {$regex: q}}, {fields: {'Manufacturer Part Number': 1, '_id': 0}, sort: ['Manufacturer Part Number','asc'], limit: 15});
            cursor.forEach(function(item) {
                result.push({value: item["Manufacturer Part Number"], text: item["Manufacturer Part Number"]});
            });
            this.setState({data: result});
        }
    },
    handleFocusBlur(e) {
        this.setState({
            focus: e.target === document.activeElement,
            typevalue:e,
        });
    },
    checkNumberExist() {
        let q = this.state.value.toUpperCase();
        if (q != '' && Product.findOne({"Manufacturer Part Number": q}) == null){
            return <span style={{position:'absolute',top:10,right:-200}}>{I18n.t('buyerForm.onhave')} <Link to="/seller_center/add_type" style={{color:'#0a66bc',cursor:'pointer'}}>{I18n.t('buyerForm.addtype')}</Link> </span>;
        }
    },
    onChangeIndustry(value){
        this.state.industry = value;
        this.setState({ industry:this.state.industry });
    },
    onChange(e) {
        this.state.values = e.target.value;
        this.setState({
            values: this.state.values,
        });
    },
    onChanges(e) {
        this.state.answer = e.target.value;
        this.setState({
            answer:this.state.answer,
        });
    },
    Change(e) {
        if(e.target.value == "DPA" ){
            this.state.DPA = e.target.checked;
            this.setState({DPA:this.state.DPA});
        }else if(e.target.value == "COC"){
            this.state.COC = e.target.checked;
            this.setState({COC:this.state.COC});
        }else if(e.target.value == "ROHS"){
            this.state.ROHS = e.target.checked;
            this.setState({ROHS:this.state.ROHS});
        }else if(e.target.value == "datapacket"){
            this.state.datapacket = e.target.checked;
            this.setState({datapacket:this.state.datapacket});
        }else if(e.target.value == "Warranty"){
            this.state.Warranty = e.target.checked;
            this.setState({Warranty:this.state.Warranty});
        }
    },
    //价格梯度

    onFocus(i,value){
        console.log("value");
        console.log(value);
        this.state.ShowTi[i][1] = value;
        this.setState({ShowTi:this.state.ShowTi});
    },
    Addnum(i,value){
        this.state.ShowTi[i][2] = value;
        this.setState({ShowTi:this.state.ShowTi});
    },
    ChangeOneTi(i,value){
        console.log(value);
        this.state.ShowTi[i][0] = value;
        this.setState({ShowTi:this.state.ShowTi});
    },
    ShowTi(){
        let Ti=[];
        for(let i=0;i<this.state.ShowTi.length;i++){
            Ti[i] =(
                <div style={{display:'inline-block'}}  key={i} className="ShowTi TI">
                    <InputNumber type="number" value={this.state.ShowTi[i][0]} min={1} style={{width:80,height:32,marginRight:2}} placeholder={I18n.t('buyerForm.NUM')} onChange={this.ChangeOneTi.bind(this,i)} />
                    <span style={{width:6,height:1,background:'#aeaeae',display:'inline-block',marginRight:2,position:'relative',top:-3}}></span>
                    <InputNumber type="number" min={1} value={this.state.ShowTi[i][1]} style={{width:80,marginRight:20,height:32}} placeholder={I18n.t('buyerForm.NUM')} onChange={this.onFocus.bind(this,i)}/>
                    <InputNumber min={0} value={this.state.ShowTi[i][2]}  step={0.01} style={{width:90,marginRight:0,height:32}} placeholder={I18n.t('buyerForm.PRICE')} onChange={this.Addnum.bind(this,i)} />
                </div>
            )
        }
        return Ti;
    },
    Add(){
        if(this.state.ShowTi.length<10){
            let num = this.state.ShowTi[this.state.ShowTi.length-1][1];
            console.log(this.state.ShowTi[this.state.ShowTi.length-1][1]);
            this.state.ShowTi.push([++num,0,1.00]);
            this.setState({ShowTi:this.state.ShowTi});

        }else if(this.state.ShowTi.length>=10){
            message.warning('价格梯度最多可添加10条信息');
            return;
        }
    },
    Del(){
        this.state.ShowTi.splice(-1,1);
        this.setState({ShowTi:this.state.ShowTi});

    },
    //价格梯度

    //库存所在地
    setAddress(i,text) {
        this.state.ShowAddress[i][0] = text ;
        this.setState({ShowAddress:this.state.ShowAddress});
    },
    addressnum(i,value){
        this.state.ShowAddress[i][1] = value;
        let allnum = 0;
        for(let j = 0;j<this.state.ShowAddress.length ; j++ ){
            if(this.state.ShowAddress[j][1] != undefined){
                allnum = allnum + this.state.ShowAddress[j][1];
                console.log(allnum);
            }
        }
        this.setState({ShowAddress:this.state.ShowAddress,allnum:allnum});
    },
    Addaddress(){
        if(this.state.ShowAddress.length>=10){
            message.warning('库存所在地最多可添加10条信息');
            return;
        }else if(this.state.ShowAddress.length<10){
            this.state.ShowAddress.push([I18n.t('buyerForm.inputaddress'),0]);
            this.setState({ShowAddress:this.state.ShowAddress});
        }
    },
    Deladdress(i){
        console.log(this.state.ShowAddress);
        this.state.allnum = this.state.allnum - this.state.ShowAddress[i][1];
        this.state.ShowAddress.splice(i,1);
        this.setState({ShowAddress:this.state.ShowAddress,allnum:this.state.allnum});
    },
    onChangeAddress(i,label){
        this.state.ShowAddress[i][0] = label;
        this.setState({ShowAddress:this.state.ShowAddress});
    },
    ShowAddress(){
        let Address=[];
        for(let i=0;i<this.state.ShowAddress.length;i++){
            Address[i]=(
                <div className="ShowTi" key={i}>
                    <Stocklocation func={this.setAddress.bind(this,i)}  onChangePackings={this.onChangeAddress.bind(this,i)} value={this.state.ShowAddress[i][0]} style={{width:170,height:32,marginRight:5,display:'inline-block',position:'relative'}} />
                    <span style={{width:10,height:1,background:'#aeaeae',display:'inline-block',marginRight:5,position:'relative',top:-3}}></span>
                    <InputNumber onChange={this.addressnum.bind(this,i)} value={this.state.ShowAddress[i][1]} style={{width:90,marginRight:0,height:32}} placeholder={I18n.t('buyerForm.NUM')} />
                    <div className="buyerFormDels" onClick={this.Deladdress.bind(this,i)}>
                        -
                    </div>
                </div>
            )
        }
        return Address;
    },
    //库存所在地

    //产地
    AddOrigin(){
        if(this.state.ShowOrigin.length<10){
            this.state.ShowOrigin.push(['',0]);
            this.setState({ShowOrigin:this.state.ShowOrigin});
        }else if(this.state.ShowOrigin.length>=10){
            message.warning('产地最多可添加10条信息');
            return;
        }
    },
    DelOrigin(i){
        let ShowOriginInput = document.getElementsByClassName("ShowOriginInput");
        this.state.ShowOrigin.splice(i,1);
        this.setState({ShowOrigin:this.state.ShowOrigin});
        for(let j=0;j<this.state.ShowOrigin.length;j++){
            ShowOriginInput[j].value = this.state.ShowOrigin[j][0];
        }
    },
    addOriginInput(i){
        let ShowOriginInput = document.getElementsByClassName("ShowOriginInput");
        this.state.ShowOrigin[i][0] = ShowOriginInput[i].value;
        this.setState({ShowOrigin:this.state.ShowOrigin});
    },
    addOriginnum(i,value){
        this.state.ShowOrigin[i][1] = value;
        this.setState({ShowOrigin:this.state.ShowOrigin});
    },
    ShowOrigin(){
        let Origin=[];
        for(let i=0;i<this.state.ShowOrigin.length;i++){
            Origin[i]=(
                <div className="ShowTi" key={i}>
                    <input value={this.state.ShowOrigin[i][0]} text="text" onChange={this.addOriginInput.bind(this,i)} className="ShowOriginInput" style={{width:170,height:32,marginRight:5}} />
                    <span style={{width:10,height:1,background:'#aeaeae',display:'inline-block',marginRight:5,position:'relative',top:-3}}></span>
                    <InputNumber onChange={this.addOriginnum.bind(this,i)} value={this.state.ShowOrigin[i][1]}  style={{width:90,marginRight:0,height:32}} placeholder={I18n.t('buyerForm.NUM')} />
                    <div className="buyerFormDels" onClick={this.DelOrigin.bind(this,i)}>
                        -
                    </div>
                </div>
            )
        }
        return Origin;
    },
    //产地

    //批号
    AddBatch(){
        if(this.state.ShowBatch.length<10){
            this.state.ShowBatch.push(['',0]);
            this.setState({ShowBatch:this.state.ShowBatch});
        }else if(this.state.ShowBatch.length>=10){
            message.warning('批号最多可添加10条信息');
            return;
        }
    },
    DelBatch(i){
        let ShowBatchInput = document.getElementsByClassName("ShowBatchInput");
        this.state.ShowBatch.splice(i,1);
        this.setState({ShowBatch:this.state.ShowBatch});
        for(let j=0;j<this.state.ShowBatch.length;j++){
            ShowBatchInput[j].value = this.state.ShowBatch[j][0];
        }
    },
    addBatchInput(i){
        let ShowBatchInput = document.getElementsByClassName("ShowBatchInput");
        this.state.ShowBatch[i][0] = ShowBatchInput[i].value;
        this.setState({ShowBatch:this.state.ShowBatch});
    },
    addBathnum(i,value){
        this.state.ShowBatch[i][1] = value;
        this.setState({ShowBatch:this.state.ShowBatch});
        let ShowBatchInput = document.getElementsByClassName("ShowBatchInput");
    },
    ShowBatch(){
        let Batch=[];
        for(let i=0;i<this.state.ShowBatch.length;i++){
            Batch[i]=(
                <div className="ShowTi" key={i}>
                    <input text="text" value={this.state.ShowBatch[i][0]} className="ShowBatchInput" onChange={this.addBatchInput.bind(this,i)} style={{width:170,height:32,marginRight:5}} />
                    <span style={{width:10,height:1,background:'#aeaeae',display:'inline-block',marginRight:5,position:'relative',top:-3}}></span>
                    <InputNumber onChange={this.addBathnum.bind(this,i)} value={this.state.ShowBatch[i][1]} style={{width:90,marginRight:0,height:32}} placeholder={I18n.t('buyerForm.NUM')} />
                    <div className="buyerFormDels" onClick={this.DelBatch.bind(this,i)}>
                        -
                    </div>
                </div>
            )
        }
        return Batch;
    },
    //批号


    //包装方式
    onChangePackings(i,label) {
        let str1 = I18n.t('packing.'+label[0]);
        let str2 = I18n.t('packing.'+label[1]);
        this.state.PackingsArr[i][2] = str1+'/'+str2;
        this.state.PackingsArr[i][0] = label;
        console.log(label);
        this.setState({PackingsArr:this.state.PackingsArr});
    },
    AddPackings(){
        if(this.state.PackingsArr.length<10){
            this.state.PackingsArr.push(['',0]);
            this.setState({PackingsArr:this.state.PackingsArr});
        }else if(this.state.PackingsArr.length>=10){
            message.warning('包装方式最多可添加10条信息');
            return;
        }
    },
    DelPackings(i){
        this.state.PackingsArr.splice(i,1);
        this.setState({PackingsArr:this.state.PackingsArr});
    },
    Packingsnum(i,value){
        this.state.PackingsArr[i][1] = value;
        this.setState({PackingsArr:this.state.PackingsArr});
    },
    ShowPackings(){
        let Packings=[];
        for(let i=0;i<this.state.PackingsArr.length;i++){
            Packings[i]=(
                <div className="ShowTi Packings" key={i}>
                    <Packing value={this.state.PackingsArr[i][0]} onChangePackings={this.onChangePackings.bind(this,i)} placeholder={I18n.t('buyerForm.searchpacking')} style={{width:170,marginRight:5,display:'inline-block'}}/>
                    <span style={{width:10,height:1,background:'#aeaeae',display:'inline-block',marginRight:5,position:'relative',top:-3}}></span>
                    <InputNumber value={this.state.PackingsArr[i][1]} onChange={this.Packingsnum.bind(this,i)} style={{width:90,marginRight:0,height:32}} placeholder={I18n.t('buyerForm.NUM')}/>
                    <div className="buyerFormDels" onClick={this.DelPackings.bind(this,i)}>
                        -
                    </div>
                </div>
            )
        }
        return Packings;
    },
    //包装方式
    Changedaofu(e){
        this.setState({dest_pay: e.target.checked});
    },
    setSum(value){
        this.setState({ trans_expanse: value });
    },
    ShowBox(){
        if(this.state.answer==2){
            return(
                <div className="shoppingBox">
                    <p>
                        <span>{I18n.t('buyer.emailbuy')}</span>
                        <InputNumber type="text" step={0.01} value={this.state.trans_expanse} style={{width:80,height:26,border:'1px solid #d6d6d6'}} onChange={this.setSum} />
                    </p>
                    <p>
                        <Checkbox value="daofu" onChange={this.Changedaofu} checked={this.state.dest_pay} >{I18n.t('buyer.daofu')}</Checkbox>
                    </p>
                </div>
            )
        }else{

        }
    },
    getPacking(){
        let getPacking = document.getElementsByClassName("getPacking")[0];
        this.state.getPackings = getPacking.value;
        this.setState({getPackings:this.state.getPackings});
    },
    getAddnum(){
        let getAddnum = document.getElementsByClassName("getAddnum")[0];
        this.state.getAddnums = getAddnum.children[1].children[0].value
        this.setState({getAddnums:this.state.getAddnums});
    },
    getReserved(){
        let getReserved = document.getElementsByClassName("getReserved")[0];
        this.state.getReserveds = getReserved.children[1].children[0].value;
        this.setState({getReserveds:this.state.getReserveds});
    },
    clickCancel(){

    },
    handleSubmit(e) {
        e.preventDefault();
        let q = this.state.value.toUpperCase();
        let PackingsAllnum = 0;
        let ShowBatchAllnum = 0;
        let ShowOriginAllnum = 0;
        let ShowAddressAllnum = 0;
        for(let i=0;i<this.state.PackingsArr.length;i++){
            if(this.state.PackingsArr[i][0][0] == ''&&this.state.PackingsArr[i][1] == 0){
                this.state.PackingsArr.splice(i,1);
            }else{
                PackingsAllnum = PackingsAllnum + this.state.PackingsArr[i][1];
            }
            this.setState({PackingsArr:this.state.PackingsArr});
        }
        for(let i=0;i<this.state.ShowBatch.length;i++){
            if(this.state.ShowBatch[i][0] == ''&&this.state.ShowBatch[i][1] == 0){
                this.state.ShowBatch.splice(i,1);
            }else{
                ShowBatchAllnum = ShowBatchAllnum + this.state.ShowBatch[i][1];
            }
            this.setState({ShowBatch:this.state.ShowBatch});
        }
        for(let i=0;i<this.state.ShowOrigin.length;i++){
            if(this.state.ShowOrigin[i][0] == ''&&this.state.ShowOrigin[i][1] == 0){
                this.state.ShowOrigin.splice(i,1);
            }else{
                ShowOriginAllnum = ShowOriginAllnum + this.state.ShowOrigin[i][1];
            }
            this.setState({ShowOrigin:this.state.ShowOrigin});
        }
        for(let i=0;i<this.state.ShowAddress.length;i++){
            if(this.state.ShowAddress[i][0] == I18n.t('buyerForm.inputaddress') && this.state.ShowAddress[i][1] == 0){
                this.state.ShowAddress.splice(i,1);
            }else{
                ShowAddressAllnum = ShowAddressAllnum + this.state.ShowAddress[i][1];
            }
            this.setState({ShowAddress:this.state.ShowAddress});
        }
        if( this.state.value == '' || (q != '' && Product.findOne({"Manufacturer Part Number": q}) == null)|| this.state.industry == ""||this.state.PackingsArr.length == 0 ||this.state.ShowBatch.length == 0 ||this.state.ShowOrigin.length == 0 ||this.state.ShowAddress.length == 0){
            message.error('库存上传失败，请您仔细检查所填写的信息！');
            if(this.state.PackingsArr.length == 0){
                this.state.PackingsArr=[['',0]];
            }
            if(this.state.ShowBatch.length == 0){
                this.state.ShowBatch=[['',0]];
            }
            if(this.state.ShowAddress.length == 0){
                this.state.ShowAddress=[[I18n.t('buyerForm.inputaddress'),0]];
            }
            if(this.state.ShowOrigin.length == 0){
                this.state.ShowOrigin=[['',0]];
            }
            this.setState({PackingsArr:this.state.PackingsArr,ShowAddress:this.state.ShowAddress, ShowBatch:this.state.ShowBatch, ShowOrigin:this.state.ShowOrigin});
        }else{
            this.props.form.validateFields((errors, values) => {
                if (errors) {
                    message.error('库存上传失败，请您仔细检查所填写的信息！');
                    return;
                }else{
                    if(ShowAddressAllnum != ShowBatchAllnum){
                        if(ShowAddressAllnum != ShowOriginAllnum){
                            if(ShowAddressAllnum != PackingsAllnum){
                                message.error('您所输入的批号，产地，包装方式的库存总量与库存所在地的库存总量不符，请您重新检查！',5)
                            }else{
                                message.error('您所输入的批号，产地的库存总量与库存所在地的库存总量不符，请您重新检查！',5)
                            }
                        }else{
                            message.error('您所输入的批号的库存总量与库存所在地的库存总量不符，请您重新检查！',5)
                        }
                    }else{
                        if(ShowAddressAllnum != ShowOriginAllnum){
                            if(ShowAddressAllnum != PackingsAllnum){
                                message.error('您所输入的产地，包装方式的库存总量与库存所在地的库存总量不符，请您重新检查！',5)
                            }else{
                                message.error('您所输入的产地的库存总量与库存所在地的库存总量不符，请您重新检查！',5)
                            }
                        }else{
                            if(ShowAddressAllnum != PackingsAllnum){
                                message.error('您所输入的包装方式的库存总量与库存所在地的库存总量不符，请您重新检查！',5)
                            }else{
                                let PackingsArr = [];
                                let ShowAddress = [];
                                let ShowBatch = [];
                                let ShowOrigin = [];
                                for(let i=0;i<this.state.PackingsArr.length;i++){
                                    if(i ==0){
                                        PackingsArr.push(this.state.PackingsArr[0]);
                                    }else{
                                        let j = 0;
                                        for(; j < PackingsArr.length; j ++){
                                            if(PackingsArr[j][0][0] == this.state.PackingsArr[i][0][0] && PackingsArr[j][0][1] == this.state.PackingsArr[i][0][1]){
                                                PackingsArr[j][1] = PackingsArr[j][1]+this.state.PackingsArr[i][1];
                                                break;
                                            }
                                        }
                                        if (j == PackingsArr.length) {
                                            PackingsArr.push(this.state.PackingsArr[i]);
                                        }
                                    }
                                }
                                for(let i=0;i<this.state.ShowAddress.length;i++){
                                    if(i ==0){
                                        ShowAddress.push(this.state.ShowAddress[0]);
                                    }else{
                                        let j = 0;
                                        for(; j<ShowAddress.length; j++){
                                            if( this.state.ShowAddress[i][0] == ShowAddress[j][0]){
                                                ShowAddress[j][1] = ShowAddress[j][1]+this.state.ShowAddress[i][1];
                                                break;
                                            }
                                        }
                                        if (j == ShowAddress.length) {
                                            ShowAddress.push(this.state.ShowAddress[i]);
                                        }
                                    }
                                }
                                for(let i=0;i<this.state.ShowBatch.length;i++){
                                    if(i ==0){
                                        ShowBatch.push(this.state.ShowBatch[0]);
                                    }else{
                                        let j = 0;
                                        for(; j<ShowBatch.length; j++){
                                            if( this.state.ShowBatch[i][0] == ShowBatch[j][0]){
                                                ShowBatch[j][1] = ShowBatch[j][1]+this.state.ShowBatch[i][1];
                                                break;
                                            }
                                        }
                                        if (j == ShowBatch.length) {
                                            ShowBatch.push(this.state.ShowBatch[i]);
                                        }
                                    }
                                }
                                for(let i=0;i<this.state.ShowOrigin.length;i++){
                                    if(i ==0){
                                        ShowOrigin.push(this.state.ShowOrigin[0]);
                                    }else{
                                        let j = 0;
                                        for(; j<ShowOrigin.length; j++){
                                            if( this.state.ShowOrigin[i][0] == ShowOrigin[j][0]){
                                                ShowOrigin[j][1] = ShowOrigin[j][1]+this.state.ShowOrigin[i][1];
                                                break;
                                            }
                                        }
                                        if (j == ShowOrigin.length) {
                                            ShowOrigin.push(this.state.ShowOrigin[i]);
                                        }
                                    }
                                }
                                Meteor.call('stock.updateStock', this.props.stockInfo._id, this.state.value, this.state.industry, values.pack, values.add, values.min, this.state.values, this.state.ShowTi, ShowAddress, ShowBatch, ShowOrigin, PackingsArr, this.state.DPA, this.state.COC,this.state.datapacket, this.state.Warranty,this.state.ROHS,  this.state.answer, this.state.trans_expanse, this.state.dest_pay);
                                message.success('库存上传成功！');
                                this.props.Close();
                            }
                        }
                    }
                }
            });
        }
    },
    resetFields(e){
        e.preventDefault();
    },
    Again(e) {
        e.preventDefault();
        this.props.form.resetFields();
    },
    render(){

        const searchCls = classNames({
            'ant-search-input': true,
            'ant-search-input-focus': this.state.focus,
        });
        const options = this.state.data.map(d => <Option key={d.value}>{d.text}</Option>);
        const formItemLayout = {
            labelCol: { span: 7 },
        };
        const columns = [{
            title: '型号',
            dataIndex: 'xinghao',
            render: (text) => <span style={{color:'#ed7020'}}>{text}</span>,
        }, {
            title: '厂商',
            dataIndex: 'address',
        }, {
            title: '供方标准包装',
            dataIndex: 'baozhuang',
        }, {
            title: '递增数量',
            dataIndex: 'dizeng',
        }, {
            title: '最小订货量',
            dataIndex: 'dinghuoliang',
        }, {
            title: '交易货币',
            dataIndex: 'huobi',
        }, {
            title: '价格梯度',
            dataIndex: 'tidu',
            render: (text) => (
                text.map(p => <p style={{marginBottom:5}}> <span>{p[0]}+ :</span> <span>￥{p[2]}</span> </p> )
            ),
        }, {
            title: '库存总量',
            dataIndex: 'kucunshuliang',
        }, {
            title: '库存所在地',
            dataIndex: 'kucunsuozaidi',
            render: (text) => (
                text.map(p => <p style={{marginBottom:5}}> <span>{p[0]}</span>/<span>{p[1]}件</span> </p> )
            ),
        }, {
            title: '批号',
            dataIndex: 'pihao',
            render: (text) => (
                text.map(p => <p style={{marginBottom:5}}> <span>{p[0]}</span>/<span>{p[1]}件</span> </p> )
            ),
        }, {
            title: '产地',
            dataIndex: 'chandi',
            render: (text) => (
                text.map(p => <p style={{marginBottom:5}}> <span>{p[0]}</span>/<span>{p[1]}件</span> </p> )
            ),
        },{
            title:'是否包邮',
            dataIndex:'baoyou',
        }
        ];
        let huobi;
        if(this.state.values == 1){
            huobi = I18n.t('buyerForm.ren');
        }else if(this.state.values ==2){
            huobi = I18n.t('buyerForm.dollar');
        }
        let answer;
        if(this.state.answer ==1){
            answer = I18n.t('buyerForm.yes');
        }else if(this.state.answer ==2){
            answer = I18n.t('buyerForm.no');
        }
        let allnum=0;
        for(let i=0;i<this.state.ShowAddress.length;i++){
            allnum = allnum + this.state.ShowAddress[i][1];
        }
        const data = [{
            key: '1',
            xinghao:this.state.typevalue,
            address:this.state.industry,
            baozhuang:this.state.getPackings,
            dizeng:this.state.getAddnums,
            dinghuoliang:this.state.getReserveds,
            chandi:this.state.ShowOrigin,
            tidu:this.state.ShowTi,
            kucunshuliang:allnum,
            pihao:this.state.ShowBatch,
            baoyou:answer,
            huobi:huobi,
            kucunsuozaidi:this.state.ShowAddress,
        }];
        const { getFieldProps ,getFieldError, isFieldValidating } = this.props.form;
        const packProps = getFieldProps('pack', {
            initialValue:this.state.getPackings,
            rules: [
                { required: true,min:1,message:I18n.t('buyer.error')},
            ],
        });
        const addProps = getFieldProps('add', {
            initialValue:this.state.getAddnums,
            rules: [
                { required: true,min:1,message:I18n.t('buyer.error'),type:'number'},
            ],
        });
        const minProps = getFieldProps('min', {
            initialValue:this.state.getReserveds,
            rules: [
                { required: true,min:1,message:I18n.t('buyer.error'),type:'number'},
            ],
        });
        return(
            <div>
                <Form inline className="SellerCenterUploadStockForm">
                    <FormItem
                        label={I18n.t('buyerForm.type')}
                        required
                    >
                        <Input.Group className={searchCls} >
                            <Select
                                combobox
                                value={this.state.value}
                                notFoundContent=""
                                defaultActiveFirstOption={false}
                                showArrow={false}
                                filterOption={false}
                                onChange={this.handleChange}
                                onFocus={this.handleFocusBlur}
                                onBlur={this.handleFocusBlur}
                            >
                                {options}
                            </Select>
                        </Input.Group>
                        {this.checkNumberExist()}
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.com')}
                    >
                        <SelectShop  func={this.onChangeIndustry} style={{width:280,height:32}} value={this.state.industry}/>
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.Packing')}
                    >
                        <Input {...packProps}  type="text" className="getPacking"  />
                        <div className="UploadStockOrigin">
                            <img src={(config.theme_path + 'radius.png')} alt=""/>
                            <div className="UploadStockOriginBox">
                                {I18n.t('buyerForm.packinges')}
                            </div>
                        </div>
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.addnum')}
                    >
                        <InputNumber {...addProps} className="getAddnum"  style={{width:'100%'}}  placeholder={I18n.t('buyerForm.NUM')} defaultValue={''}  step={1} />
                        <div className="UploadStockOrigin">
                            <img src={(config.theme_path + 'radius.png')} alt=""/>
                            <div className="UploadStockOriginBox">
                                {I18n.t('buyerForm.addnums')}
                            </div>
                        </div>
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.reserved')}
                    >
                        <InputNumber {...minProps} className="getReserved"  style={{width:'100%'}}  placeholder={I18n.t('buyerForm.NUM')} defaultValue={''}  step={1} />
                        <div className="UploadStockOrigin">
                            <img src={(config.theme_path + 'radius.png')} alt=""/>
                            <div className="UploadStockOriginBox">
                                {I18n.t('buyerForm.reserveds')}
                            </div>
                        </div>
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.money')}
                    >
                        <RadioGroup onChange={this.onChange} value={this.state.values}>
                            <Radio key="a" value={1}>{I18n.t('buyerForm.ren')}</Radio>
                            <Radio key="b" value={2}>{I18n.t('buyerForm.dollar')}</Radio>
                        </RadioGroup>
                        <div className="UploadStockOrigin">
                            <img src={(config.theme_path + 'radius.png')} alt=""/>
                            <div className="UploadStockOriginBox">
                                {I18n.t('buyerForm.moneies')}
                            </div>
                        </div>
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.price')}
                        {...formItemLayout}
                        className="buyerFormDelItem"
                    >
                        <div className="buyerFormDel" onClick={this.Add}>
                            +
                            <div className="buyerFormDelMess">
                                {I18n.t('buyer.moreten')}
                            </div>
                        </div>
                        <div className="buyerFormDelsTI" onClick={this.Del} style={this.state.ShowTi.length<=1 ? {display:'none'} : {display:'block'}}>
                            -
                        </div>
                        {this. ShowTi()}
                    </FormItem>
                    <FormItem
                        required
                        {...formItemLayout}
                        label={I18n.t('buyerForm.address')}
                    >
                        <div className="buyerFormDel" onClick={this.Addaddress}>
                            +
                            <div className="buyerFormDelMess">
                                {I18n.t('buyer.ShowAddressten')}
                            </div>
                        </div>

                        {this.ShowAddress()}
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.allnum')}
                    >
                        <InputNumber style={{width:280}} readOnly="readonly" value={this.state.allnum} />
                        <span style={{position:'absolute',bottom:'-28px',color:'#999',fontSize:'12px',left:0}}>(库存总量由库存所在地数量总和决定)</span>
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.Batch')}
                        {...formItemLayout}
                    >
                        <div className="buyerFormDel" onClick={this.AddBatch}>
                            +
                            <div className="buyerFormDelMess">
                                {I18n.t('buyer.ShowBatchten')}
                            </div>
                        </div>

                        {this.ShowBatch()}
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.Origin')}
                        {...formItemLayout}
                    >
                        <div className="buyerFormDel" onClick={this.AddOrigin}>
                            +
                            <div className="buyerFormDelMess">
                                {I18n.t('buyer.moreten')}
                            </div>
                        </div>
                        {this.ShowOrigin()}
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.Packings')}
                        className="BorderRadius"
                        {...formItemLayout}
                    >
                        <div className="buyerFormDel" onClick={this.AddPackings}>
                            +
                            <div className="buyerFormDelMess">
                                {I18n.t('buyer.ShowPackingsten')}
                            </div>
                        </div>
                        {this.ShowPackings()}
                    </FormItem>
                    <FormItem
                        label
                        className="SellerCenterUploadStockFormItem"
                    >
                        <Checkbox onChange={this.Change} checked={this.state.DPA} value="DPA">DPA</Checkbox>
                        <Checkbox onChange={this.Change} checked={this.state.COC} value="COC">COC</Checkbox>
                        <Checkbox onChange={this.Change} checked={this.state.datapacket} value="datapacket">{I18n.t('buyerForm.data')}</Checkbox>
                        <Checkbox onChange={this.Change} checked={this.state.Warranty} value="Warranty">{I18n.t('buyerForm.Warranty')} <span style={{fontSize:12,color:'#999'}}>{I18n.t('buyerForm.warr')}</span> </Checkbox>
                        <Checkbox onChange={this.Change} checked={this.state.ROHS} value="ROHS">ROHS</Checkbox>
                    </FormItem>
                    <FormItem
                        required
                        label={I18n.t('buyerForm.shipping')}
                        {...formItemLayout}
                    >
                        <RadioGroup onChange={this.onChanges} value={this.state.answer}>
                            <Radio key="y" value={1}>{I18n.t('buyerForm.yes')}</Radio>
                            <Radio key="n" value={2}>{I18n.t('buyerForm.no')}</Radio>
                        </RadioGroup>
                        {this.ShowBox()}
                    </FormItem>
                    <FormItem style={{marginLeft:255}}>
                        <Button onClick={this.handleSubmit} style={{marginRight:50,marginLeft:10}} type="primary">{I18n.t('submit')}</Button>
                        <Button type="ghost" onClick={this.props.Close}>{I18n.t('PersonalCenter.manageStock.cancel')}</Button>
                    </FormItem>
                </Form>
            </div>
        )
    }
})
SellerCenterManageStockUpdate = createForm()(SellerCenterManageStockUpdate);
export default SellerCenterManageStockUpdate;
SellerCenterManageStockUpdate.propTypes = {
};
export default createContainer(() => {
    Meteor.subscribe('product.part_number');
    return {
    };
}, SellerCenterManageStockUpdate);
