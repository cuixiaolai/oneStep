import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal } from 'antd';
import config from '../../config.json';
import SellerCenterAgent from './SellerCenterAgent.jsx';
import SellerCenterManufacturer from './SellerCenterManufacturer.jsx';
import { createContainer } from 'meteor/react-meteor-data';
import { Seller } from '../../api/Seller.js';
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;

export default class SellerCenterCompanyRole extends Component{
    callback(key) {
        console.log(key);
    }
    render(){
        return(
            <Tabs defaultActiveKey="1" onChange={this.callback} className="CompanyRole">
                <TabPane tab={I18n.t('buyer.Retailer')} key="1">
                    <div className="CompanyRoleBox">
                        <span style={{color:'#ed7020'}}>{I18n.t('buyer.worm')}</span>{I18n.t('CertificationAudit.goon')}
                    </div>
                </TabPane>
                <TabPane tab={I18n.t('buyer.Agent')} key="2">
                    <SellerCenterAgent authInfo={this.props.authInfo}/>
                </TabPane>
                <TabPane tab={I18n.t('buyer.Manufacturer')} key="3">
                    <SellerCenterManufacturer authInfo={this.props.authInfo}/>
                </TabPane>
            </Tabs>
        )
    }
}
export default createContainer(() => {
    Meteor.subscribe('seller');
    return {
        authInfo: Seller.findOne({_id: Meteor.userId()}, { fields: {'authentication': 1}}),
    };
}, SellerCenterCompanyRole);