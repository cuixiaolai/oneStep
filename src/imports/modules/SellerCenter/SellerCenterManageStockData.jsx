import React, { Component, PropTypes } from 'react';
import SellerCenterManageStockTable from './SellerCenterManageStockTable.jsx';
import { I18n } from 'react-redux-i18n';

export default class SellerCenterManageStockData extends Component {
    constructor(props) {
        super(props);
        this.state =  ({
            data: [],
            filter: '',
            
        });
    }
    componentDidMount() {
        if (this.props.stockInfo !== null) {
            let data = [];
            for(let i = 0; i < this.props.stockInfo.length; i++) {
                data.push({
                    key: this.props.stockInfo[i]._id,
                    model: this.props.stockInfo[i].product_id,
                    manufacturer: this.props.stockInfo[i].manufacturer,
                    packing: this.props.stockInfo[i].package,
                    number: this.props.stockInfo[i].inc_number,
                    minNum: this.props.stockInfo[i].MOQ,
                    change: this.props.stockInfo[i].currency==1 ? '人民币' : '美元',
                    price: this.props.stockInfo[i].price,
                    totalInventory: this.props.stockInfo[i].stock_quantity,
                    stockLocation: this.props.stockInfo[i].stock_address,
                    num: this.props.stockInfo[i].batch,
                    place: this.props.stockInfo[i].origin_address,
                    yn: this.props.stockInfo[i].free_ship==true ? '是' : '否',
                    operation: '编辑',
                    putaway: this.props.stockInfo[i].putaway,
                });

            }
            this.setState({ data: data });
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.stockInfo != this.props.stockInfo) {
            let data = [];
            if (nextProps.stockInfo !== null) {
                for(let i = 0; i < nextProps.stockInfo.length; i++) {
                    data.push({
                        key: nextProps.stockInfo[i]._id,
                        model: nextProps.stockInfo[i].product_id,
                        manufacturer: nextProps.stockInfo[i].manufacturer,
                        packing: nextProps.stockInfo[i].package,
                        number: nextProps.stockInfo[i].inc_number,
                        minNum: nextProps.stockInfo[i].MOQ,
                        change: nextProps.stockInfo[i].currency==1 ? '人民币' : '美元',
                        price: nextProps.stockInfo[i].price,
                        totalInventory: nextProps.stockInfo[i].stock_quantity,
                        stockLocation: nextProps.stockInfo[i].stock_address,
                        num: nextProps.stockInfo[i].batch,
                        place: nextProps.stockInfo[i].origin_address,
                        yn: nextProps.stockInfo[i].free_ship==true ? '是' : '否',
                        operation: '编辑',
                        putaway: nextProps.stockInfo[i].putaway,
                    });
                }
            }
            this.setState({ data: data });
        }
    }
    package(arr) {
        if (this.props.packing === '') {
            return true;
        }
        let i = 0;
        for (;i < arr.length; ++ i) {
            if (arr[i].package === this.props.packing) {
                break;
            }
        }
        if (i === arr.length) {
            return false;
        }
        else {
            return true;
        }
    }
    location(arr) {
        if (this.props.ShowAddress === I18n.t('buyerForm.searchp')) {
            return true;
        }
        let i = 0;
        for (;i < arr.length; ++ i) {
            if (arr[i].address === this.props.ShowAddress) {
                break;
            }
        }
        if (i === arr.length) {
            return false;
        }
        else {
            return true;
        }
    }
    render() {
        return <SellerCenterManageStockTable changeflag={this.props.changeflag} 
                                             data={this.state.data.filter(item=>(item.model==this.props.model||this.props.model=='')&&(item.manufacturer==this.props.industry||this.props.industry==I18n.t('buyer.Manufacturer'))
                                             &&(item.yn==this.props.shipping||this.props.shipping=='')&&(this.package(item.packing))&&(this.location(item.stockLocation))&&
                                             ((this.props.total1==''||item.totalInventory>=this.props.total1)&&(this.props.total2==''||item.totalInventory<=this.props.total2)))}/>;
    }
}