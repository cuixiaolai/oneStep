import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal,Select,InputNumber,message } from 'antd';
import config from '../../config.json';
import { createContainer } from 'meteor/react-meteor-data';
import { Seller } from '../../api/Seller.js';
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;
import DoubleRule from './DoubleRule.jsx';
const Option = Select.Option;
import SellerPayment from './SellerPayment.jsx';

export default class SellerCenterDoubleIndemnityBox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            check:false,
            value:'rmb',
            money:null,
            num:3,
            flag:false,
            result:false,
        });
    }
    change(value){
        this.setState({money:value});
        console.log(value);
    }
    onChange(e) {
        this.setState({check:e.target.checked})
    }
    handleChange(value) {
        this.setState({value:value});
    }
    Click(num,nums){
        if(this.state.check == false){
            message.error('请查看，并勾选双倍赔付规则！')
        }else{
            if(this.state.money == null){
                message.error('请输入申请额度！');
            }else{
                this.setState({check:false,money:null})
                console.log(this.state.money,num,nums);
                this.props.oneclick(this.state.money);
            }
        }
    }
    onclick(){
        this.props.click();
    }
    show(){
        return(
            <div>
                <DoubleRule route="double" onChange={this.onChange.bind(this)} name={I18n.t('DoubleIndemnity.rule')} />
                <p className="DoubleIndemnityBoxP" style={{width:700,margin:'30px auto',marginBottom:70}}>
                    <span style={{fontSize:'18px',marginRight:10}}>{I18n.t('DoubleIndemnity.aply')}：</span>
                    <Select size="large" defaultValue="rmb" style={{ width: 117,height:36 }} onChange={this.handleChange.bind(this)}>
                        <Option value="rmb">{I18n.t('DoubleIndemnity.rmb')}</Option>
                    </Select>
                    <InputNumber min={0} onChange={this.change.bind(this)} placeholder={I18n.t('DoubleIndemnity.please')} style={{width:117,marginLeft:10,height:36}} size="large" />
                </p>
                <SellerPayment Click={this.Click.bind(this)} style={{width:700,margin:'0 auto'}} />
            </div>
        )
    }
    render(){
        if (this.props.num == 1) {
            return(
                <div className="SellerCenterDoubleIndemnityBox">
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>{I18n.t('DoubleIndemnity.resuccess')}</span>
                    </p>
                    <p style={{marginBottom:60}}>{I18n.t('DoubleIndemnity.wating')}</p>
                </div>
            )
        }
        else if (this.props.num == 2){
            return(
                <div className="SellerCenterDoubleIndemnityBox">
                    <p>
                        <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                        <span>{I18n.t('DoubleIndemnity.returnsuccess')}</span>
                    </p>
                    <p style={{marginBottom:60}}>{I18n.t('DoubleIndemnity.returnsuccesswords')}</p>
                </div>
            )
        }
        else {
            if(this.props.double == null || (this.props.double != null && (this.props.double.total == 0 || this.props.double.verify == true && this.props.double.success == true))){
                return(
                    this.show()
                )
            }
            else if(this.props.double != null && this.props.double.verify == false){
                return(
                    <div className="SellerCenterDoubleIndemnityBox">
                        <p>
                            <img src={(config.theme_path + 'bigwatch.png')} alt=""/>
                            <span>{I18n.t('PersonalCenterSecurity.authentication.wait')}</span>
                        </p>
                        <p style={{marginBottom:60}}>{I18n.t('DoubleIndemnity.wating')}</p>
                    </div>
                )
            }
            else {
                return <div className="SellerCenterDoubleIndemnityBox"></div>;
            }
        }
    }
}
