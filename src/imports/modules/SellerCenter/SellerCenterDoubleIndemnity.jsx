import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal } from 'antd';
import config from '../../config.json';
import { createContainer } from 'meteor/react-meteor-data';
import { Seller } from '../../api/Seller.js';
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;
import SellerCenterDoubleIndemnityBox from './SellerCenterDoubleIndemnityBox.jsx'
export default class SellerCenterDoubleIndemnity extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            flag:true,
            num:0,
            flags:false,
            money:null,
            showflag:false,
        });
    }
    callback(key) {
       
    }
    showbefore(){
        if(this.state.flags == true){
            return(
                <span>{I18n.t('DoubleIndemnity.did')} ￥32434</span>
            )
        }
    }
    Again(){
        Meteor.call('seller.againDouble');
        this.setState({num:0});
    }
    return(){
        this.setState({num: 2});
        Meteor.call('seller.paybackDouble');
    }
    oneclick(amount){
        this.setState({num: 1});
        Meteor.call('seller.addDouble', amount, function(err) {
            if (err) {
                console.log(err);
            }
            else {
                Meteor.call('authentication.addDoubleAmount', amount);
            }
        }.bind(this));
    }
    showflag(){
        this.setState({showflag:true});
    }
    showresult(){
        if(this.props.sellerInfo !=  null){
            if(this.props.sellerInfo.double != null && this.props.sellerInfo.double.total != 0 && this.props.sellerInfo.double.verify == true && this.props.sellerInfo.double.success == false){
                return(
                    <div className="SellerCenterDoubleIndemnityBox" style={{marginBottom:30}}>
                        <p>
                            <img src={(config.theme_path + 'error_03.png')} alt=""/>
                            <span>{I18n.t('DoubleIndemnity.error')}</span>
                        </p>
                        <p style={{marginBottom:6}}>{I18n.t('DoubleIndemnity.reason')} {this.props.sellerInfo.double.reason} </p>
                        <p style={{textAlign:'right'}}>
                            <Button type="ghost" onClick={this.Again.bind(this)}>{I18n.t('DoubleIndemnity.again')}</Button>
                        </p>
                    </div>
                )
            }
            else if(this.props.sellerInfo.double != null && this.props.sellerInfo.double.total != 0 && this.props.sellerInfo.double.verify == true && this.props.sellerInfo.double.success == true){
                return(
                    <div className="SellerCenterDoubleIndemnityBox"  style={{marginBottom:30}}>
                        <p>
                            <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                            <span>{I18n.t('DoubleIndemnity.success')}</span>
                        </p>
                        <p style={{marginBottom:6}}>{I18n.t('DoubleIndemnity.will')} ￥ {this.props.sellerInfo.double.total} {this.showbefore()} </p>
                        <p style={{textAlign:'right'}}>
                            <Button onClick={this.return.bind(this)} type="ghost">{I18n.t('DoubleIndemnity.return')}</Button>
                        </p>
                    </div>
                )
            }
        }
    }
    one(){
        this.setState({num:0});
    }
    showTab(){
        if(this.state.flag == true && this.props.sellerInfo != null){
            if(this.props.sellerInfo.double != null && this.props.sellerInfo.double.total != 0 && this.props.sellerInfo.double.verify == true && this.props.sellerInfo.double.success == false){
                
            }else{
                return(
                    <div className="SellerCenterMarginGuarantee">
                        <p>{I18n.t('DoubleIndemnity.name')}</p>
                        <SellerCenterDoubleIndemnityBox oneclick={this.oneclick.bind(this)} num={this.state.num} double={this.props.sellerInfo.double}/>
                    </div>
                )
            }

        }
    }
    render(){
        return(
            <div>
                {this.showresult()}
                {this.showTab()}
            </div>
        )
    }
}
export default createContainer(() => {
    Meteor.subscribe('seller');
    return {
        sellerInfo: Seller.findOne({_id: Meteor.userId()}, { fields: {'double': 1}}),
    };
}, SellerCenterDoubleIndemnity);
