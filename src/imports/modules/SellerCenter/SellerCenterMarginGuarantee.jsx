import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal,message,InputNumber,Select } from 'antd';
import config from '../../config.json';
import { createContainer } from 'meteor/react-meteor-data';
import { Seller } from '../../api/Seller.js';
import DoubleRule from './DoubleRule.jsx';
import SellerPayment from './SellerPayment.jsx';
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;
const Option = Select.Option;

export default class SellerCenterMarginGuarantee extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            check:false,
            value:'rmb',
            money:null,
            num:0,
            showflag:true,
            flag:true,
            flags:true,
        });
    }
    onChange(e) {
        this.setState({check:e.target.checked})
    }
    handleChange(value) {
        this.setState({value:value});
    }
    change(value){
        this.setState({money:value});
        console.log(value);
    }
    Click(num,nums){
        if(this.state.check == false){
            message.error('请查看，并勾选保证金担保规则！')
        }else{
            if(this.state.money == null){
                message.error('请输入申请额度！');
            }else{
                console.log(this.state.money,num,nums);
                Meteor.call('seller.addGuarantee', this.state.money, function(err) {
                    if (err) {
                        console.log(err);
                    }
                    else{
                        Meteor.call('authentication.addGuaranteeAmount', this.state.money);
                        this.setState({num: 1});
                    }
                }.bind(this));
            }
        }
    }
    Again(){
        Meteor.call('seller.againGuarantee');
        this.setState({num:0});
    }
    return(){
        this.setState({num:this.state.num+1});
    }
    returns(){
        this.setState({num: 2});
        Meteor.call('seller.paybackGuarantee');
    }
    showresult(){
        if(this.props.sellerInfo !=  null){
            if(this.props.sellerInfo.guarantee != null && this.props.sellerInfo.guarantee.total != 0 && this.props.sellerInfo.guarantee.verify == true && this.props.sellerInfo.guarantee.success == false){
                return(
                    <div className="SellerCenterDoubleIndemnityBox" style={{marginBottom:30}}>
                        <p>
                            <img src={(config.theme_path + 'rederror.png')} alt=""/>
                            <span>{I18n.t('Bond.error')}</span>
                        </p>
                        <p style={{marginBottom:6}}>{I18n.t('DoubleIndemnity.reason')} {this.props.sellerInfo.guarantee.reason} </p>
                        <p style={{textAlign:'right'}}>
                            <Button type="ghost" onClick={this.Again.bind(this)}>{I18n.t('DoubleIndemnity.again')}</Button>
                        </p>
                    </div>
                )
            }
            else if(this.props.sellerInfo.guarantee != null && this.props.sellerInfo.guarantee.total != 0 && this.props.sellerInfo.guarantee.verify == true && this.props.sellerInfo.guarantee.success == true){
                return(
                    <div className="SellerCenterDoubleIndemnityBox"  style={{marginBottom:30}}>
                        <p>
                            <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                            <span>{I18n.t('Bond.success')}</span>
                        </p>
                        <p style={{marginBottom:6}}>{I18n.t('Bond.will')} ￥ {this.props.sellerInfo.guarantee.total}
                            <span>{I18n.t('DoubleIndemnity.did')} ￥{this.props.sellerInfo.guarantee.last}</span>
                        </p>
                        <p style={{textAlign:'right'}}>
                            <Button onClick={this.returns.bind(this)} type="ghost">{I18n.t('DoubleIndemnity.return')}</Button>
                        </p>
                    </div>
                )
            }
        }
    }
    show(){
        console.log(this.state.num,this.props.sellerInfo);
        if (this.props.sellerInfo != null) {
            if(this.props.sellerInfo.guarantee != null && this.props.sellerInfo.guarantee.total != 0 && this.props.sellerInfo.guarantee.verify == true && this.props.sellerInfo.guarantee.success == false){

            }else{
                if (this.state.num == 1) {
                    return(
                        <div className="SellerCenterMarginGuarantee">
                            <p>{I18n.t('Bond.name')}</p>
                            <div className="SellerCenterDoubleIndemnityBox">
                                <p>
                                    <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                                    <span>{I18n.t('Bond.resuccess')}</span>
                                </p>
                                <p style={{marginBottom:60}}>{I18n.t('DoubleIndemnity.wating')}</p>
                            </div>
                        </div>
                    )
                }
                else if (this.state.num == 2){
                    return(
                        <div className="SellerCenterMarginGuarantee">
                            <p>{I18n.t('Bond.name')}</p>
                            <div className="SellerCenterDoubleIndemnityBox">
                                <p>
                                    <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                                    <span>{I18n.t('Bond.returnsuccess')}</span>
                                </p>
                                <p style={{marginBottom:60}}>{I18n.t('Bond.returnsuccesswords')}</p>
                            </div>
                        </div>
                    )
                }
                else {
                    if(this.props.sellerInfo.guarantee == null || (this.props.sellerInfo.guarantee != null &&
                        (this.props.sellerInfo.guarantee.total == 0 || this.props.sellerInfo.guarantee.verify == true && this.props.sellerInfo.guarantee.success == true))){
                        return(
                            <div>
                                <div className="SellerCenterMarginGuarantee">
                                    <p>{I18n.t('Bond.name')}</p>
                                    <DoubleRule route="bond" onChange={this.onChange.bind(this)} />
                                    <p className="DoubleIndemnityBoxP" style={{width:700,margin:'30px auto',marginBottom:70}}>
                                        <span style={{fontSize:'18px',marginRight:10}}>{I18n.t('DoubleIndemnity.aply')}：</span>
                                        <Select size="large" defaultValue="rmb" style={{ width: 117,height:36 }} onChange={this.handleChange.bind(this)}>
                                            <Option value="rmb">{I18n.t('DoubleIndemnity.rmb')}</Option>
                                        </Select>
                                        <InputNumber min={0} onChange={this.change.bind(this)} placeholder={I18n.t('DoubleIndemnity.please')} style={{width:117,marginLeft:10,height:36}} size="large" />
                                    </p>
                                    <SellerPayment Click={this.Click.bind(this)} style={{width:700,margin:'0 auto'}} />
                                </div>
                            </div>
                        )
                    }
                    else if(this.props.sellerInfo.guarantee != null && this.props.sellerInfo.guarantee.verify == false){
                        return(
                            <div className="SellerCenterMarginGuarantee">
                                <p>{I18n.t('Bond.name')}</p>
                                <div className="SellerCenterDoubleIndemnityBox">
                                    <p>
                                        <img src={(config.theme_path + 'bigwatch.png')} alt=""/>
                                        <span>{I18n.t('PersonalCenterSecurity.authentication.wait')}</span>
                                    </p>
                                    <p style={{marginBottom:60}}>{I18n.t('DoubleIndemnity.wating')}</p>
                                </div>
                            </div>
                        )
                    }
                }
            }
        }
    }
    one(){
        this.setState({num:0});
    }
    onclicks(){
        this.setState({showflag:true,num:this.state.num+1});
    }
    render(){
        return(
            <div>
                {this.showresult()}
                {this.show()}
            </div>
        )
    }
}
export default createContainer(() => {
    Meteor.subscribe('seller');
    return {
        sellerInfo: Seller.findOne({_id: Meteor.userId()}, { fields: {'guarantee': 1}}),
    };
}, SellerCenterMarginGuarantee);
