import React, { Component } from 'react';
import { Link } from 'react-router';
import { Button,Icon,Checkbox,message} from 'antd';
import { Translate, I18n  } from 'react-redux-i18n';
import config from '../../config.json';
import { Meteor } from 'meteor/meteor';

export default class NewTr extends Component{
    constructor(state) {
        super(state);
        this.state = ({
            price: false,
            stockLocation: false,
            num: false,
            place: false,
            packing: false,
        });
    }
    
    ShowPrice(){
        let price = [];
        let length = this.state.price == false&&this.props.date.price.length >= 3 ? 3 : this.props.date.price.length;
        if (this.props.date.change == '美元') {
            for(let i=0; i < length; i ++){
                price[i] = (
                    <p>{this.props.date.price[i].min}+ : ${this.props.date.price[i].price}</p>
                )
            }
        }
        else {
            for(let i=0; i < length; i ++){
                price[i] = (
                    <p>{this.props.date.price[i].min}+ : ￥{this.props.date.price[i].price}</p>
                )
            }
        }
        return price;
    }
    Showpacking(){
        let packing = [];
        let length = this.state.packing == false&&this.props.date.packing.length >= 3 ? 3 : this.props.date.packing.length;
        for(let i=0; i < length; i ++){
            packing[i] = (
                <p>{this.props.date.packing[i].package}/{this.props.date.packing[i].quantity}</p>
            )
        }
        return packing;
    }
    ShowstockLocation(){
        let stockLocation = [];
        let length = this.state.stockLocation == false&&this.props.date.stockLocation.length >= 3 ? 3 : this.props.date.stockLocation.length;
        for(let i=0; i < length; i ++){
            stockLocation[i] = (
                <p>{this.props.date.stockLocation[i].address}/{this.props.date.stockLocation[i].quantity}</p>
            )
        }
        return stockLocation;
    }
    Shownum(){
        let num = [];
        let length = this.state.num == false&&this.props.date.num.length >= 3 ? 3 : this.props.date.num.length;
        for(let i=0; i < length; i ++){
            num[i] = (
                <p>{this.props.date.num[i].num}/{this.props.date.num[i].quantity}</p>
            )
        }
        return num;
    }
    Showplace(){
        let place = [];
        let length = this.state.place == false&&this.props.date.place.length >= 3 ? 3 : this.props.date.place.length;
        for(let i=0; i < length; i ++){
            place[i] = (
                <p>{this.props.date.place[i].address}/{this.props.date.place[i].quantity}</p>
            )
        }
        return place;
    }
    ShowWords(word){
        if(this.props.date[word].length>3){
            if(this.state[word]==false){
                return(
                    <p onClick={this.ShowMore.bind(this,word)} style={{cursor:'pointer',color:'#ed7020'}}>
                        {I18n.t('PersonalCenter.manageStock.more')}
                        <img style={{marginLeft:2}} src={(config.theme_path +　'origindown_03.png')} alt=""/>
                    </p>
                )
            }else{
                return(
                    <p onClick={this.ShowMore.bind(this,word)}  style={{cursor:'pointer',color:'#ed7020'}}>
                        {I18n.t('CompantInvoicsThree.hidden')}
                        <img style={{marginLeft:2,transform: 'rotate(180deg)'}} src={(config.theme_path +　'origindown_03.png')} alt=""/>
                    </p>
                )
            }
        }
    }
    ShowMore(word){
        this.setState({[word]: !this.state[word]});
    }
    Onclick(value){
        this.props.changeflag(value);
    }
    ShowEdit(){
        if(this.props.date.putaway == false){
            return(
                <p onClick={this.Onclick.bind(this,this.props.date.key)}>{I18n.t('Addresspage.edit')}</p>
            )
        }
    }
    onShowClick(value) {
        console.log(value);
        Meteor.call('stock.showUpStock', value);
        message.success('成功上架！');
    }
    Showshelf(){
        if(this.props.date.putaway == true){
            return(
                <p onClick={this.onHideClick.bind(this,this.props.date.key)}>{I18n.t('PersonalCenter.manageStock.off')}</p>
            )
        }
    }
    onHideClick(value) {
        console.log(value);
        Meteor.call('stock.outOfStock', value);
        message.success('成功下架！');
    }
    Showshelves(){
        if(this.props.date.putaway == false){
            return(
                <p onClick={this.onShowClick.bind(this,this.props.date.key)}>{I18n.t('PersonalCenter.manageStock.on')}</p>
            )
        }
    }
    onDeleteClick(value) {
        console.log(value);
        Meteor.call('stock.deleteStock', value);
    }
    render(){
        return(
            <tr>
                <td>
                    <Checkbox checked={this.props.checked}  onClick={this.props.clickCheckbox} />
                </td>
                <td  style={{maxWidth:'100px',lineHeight:'1.5'}}>
                    <p>{this.props.date.model}</p>
                </td>
                <td  style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    <p>{this.props.date.manufacturer}</p>
                </td>
                <td style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    {this.Showpacking()}
                    {this.ShowWords('packing')}
                </td>
                <td style={{maxWidth:'50px',lineHeight:'1.5'}}>
                    <p>{this.props.date.number}</p>
                </td>
                <td style={{maxWidth:'20px',lineHeight:'1.5'}}>
                    <p>{this.props.date.minNum}</p>
                </td>
                <td style={{maxWidth:'50px',lineHeight:'1.5'}}>
                    <p>{this.props.date.change}</p>
                </td>
                <td style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    {this.ShowPrice()}
                    {this.ShowWords('price')}
                </td>
                <td style={{maxWidth:'80px',lineHeight:'1.5'}}>
                    <p>{this.props.date.totalInventory}</p>
                </td>
                <td style={{maxWidth:'200px',lineHeight:'1.5'}} >
                    {this.ShowstockLocation()}
                    {this.ShowWords('stockLocation')}
                </td>
                <td style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    {this.Shownum()}
                    {this.ShowWords('num')}
                </td>
                <td style={{maxWidth:'200px',lineHeight:'1.5'}}>
                    {this.Showplace()}
                    {this.ShowWords('place')}
                </td>
                <td>
                    {this.props.date.yn}
                </td>
                <td className="NewTrCao" style={{minWidth:'80px'}}>
                    {this.ShowEdit()}
                    {this.Showshelf()}
                    {this.Showshelves()}
                    <p onClick={this.onDeleteClick.bind(this,this.props.date.key)}>{I18n.t('Addresspage.del')}</p>
                </td>
            </tr>
        )
    }
}
