import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import { Table } from 'antd';

export default class Tablebox extends Component{
    render(){
        console.log(this.props);
        return(
            <div className="Table">
                <Table columns={this.props.columns} dataSource={this.props.dataSource} pagination={this.props.pagination}/>
            </div>
        )
    }
}
