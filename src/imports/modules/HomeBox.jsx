import React, { Component, PropTypes } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Link, browserHistory } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Input, Select, Button, Icon ,Cascader } from 'antd';
import classNames from 'classnames';
import config from '../config.json';
import Products from './Product.jsx';
import HomeBoxSelect from './HomeBoxSelect.jsx';
import { connect } from 'react-redux';

 class HomeBox extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            data: [],
            value: '',
            focus: false,
            selectedOptions:[],
        });
    }
    setValue(value) {
        this.setState({value: value});
    }
    handleSearch() {
        browserHistory.replace('/Search/'+encodeURIComponent(this.state.value.trim()));
        // $.ajax({
        //     type: 'POST',
        //     url: 'http://api.kdniao.cc/Ebusiness/EbusinessOrderHandle.aspx',
        //     RequestData: '',
        //     EBusinessID: '',
        //     RequestType: '1002',
        //     DataSign: '',
        //     data: {
        //         "OrderCode": "",
        //         "ShipperCode": "SF",
        //         "LogisticCode": "611612394313"
        //     },
        //     dataType: "json",
        //     success: function (data) {
        //         console.log(data);
        //     },
        //     error: function (data) {
        //         console.log(data);
        //     }
        // });
    }
    onChange(value,selectedOptions) {
        this.props.onChange(value,selectedOptions);
        this.setState({selectedOptions:value});
    }
    render() {
        const btnCls = classNames({
            'ant-search-btn': true,
            'ant-search-btn-noempty': !!this.state.value.trim(),
        });
        const searchCls = classNames({
            'ant-search-input': true,
            'ant-search-input-focus': this.state.focus,
        });
        return(
            <div className="homebox">
                <div className="homebox-img">
                    <img src={(config.theme_path + 'newlogo.png')} alt=""/>
                </div>
                <div className="ant-search-input-wrapper" style={this.props.style}>
                    <Input.Group className={searchCls}>
                        <HomeBoxSelect setValue={this.setValue.bind(this)}/>
                        <div className="ant-input-group-wrap">
                            <Button  className={btnCls} size={this.props.size} onClick={this.handleSearch.bind(this)} value="12344" ><Translate value="home.search" /></Button>
                        </div>
                    </Input.Group>
                    <Products value={this.state.selectedOptions} onChange={this.onChange.bind(this)} style={{width:120,height:38,borderRadius:'4px 0 0 4px',border:'0',position:'absolute',top:0,left:2}} />
                </div>
            </div>
        )
    }
}

export default connect(null, (dispatch) => {
    return {
        onChange: (value,selectedOptions) => {
            dispatch({
                type: 'CHANGE',
                new_category: value,
                new_options:selectedOptions,
                from:'homebox'
            });
        }
    }
})(HomeBox);
