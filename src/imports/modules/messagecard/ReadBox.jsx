import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal } from 'antd';
import config from '../../config.json';
import { createContainer } from 'meteor/react-meteor-data';
import { Seller } from '../../api/Seller.js';
import ReadBoxTab from './ReadBoxTab.jsx';
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;

export default class ReadBox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            current:1,
            key:1,
        });
    }
    callback(key) {
        console.log(key);
        this.setState({key:key});
    }
    clickAll(){
        confirm({
            title:I18n.t('messageCard.sure'),
            content: I18n.t('messageCard.words'),
            okText:I18n.t('buyerForm.yes'),
            cancelText:I18n.t('buyerForm.no'),
            onOk() {
                Meteor.call('message.deleteAllReadMessage');
            },
            onCancel() {},
        });
    }
    render(){
        return(
            <div style={{position:'position',overflow:'hidden'}}>
                <p style={{textAlign:'right'}}> <span onClick={this.clickAll} style={{cursor:'pointer',position:'absolute',top:'8px',right:'0',zIndex:555}}>{I18n.t('messageCard.allmessage')}</span> </p>
                <Tabs defaultActiveKey="1" onChange={this.callback.bind(this)} className="CompanyRole">
                    <TabPane tab={I18n.t('messageCard.order')} key="1">
                        <ReadBoxTab data={this.props.data.filter(item=>item.type<=20)}/>
                    </TabPane>
                    <TabPane tab={I18n.t('messageCard.examine')} key="2">
                        <ReadBoxTab data={this.props.data.filter(item=>(item.type>20&&item.type<=40))}/>
                    </TabPane>
                    <TabPane tab={I18n.t('messageCard.system')} key="3">
                        <ReadBoxTab data={this.props.data.filter(item=>item.type>40)}/>
                    </TabPane>
                </Tabs>

            </div>
        )
    }
}
