import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';
import { Message } from '../../api/Message.js';
import { Tabs,Modal } from 'antd';
import NoReadBox from './noReadBox.jsx';
import ReadBox from './ReadBox.jsx';
const TabPane = Tabs.TabPane;

class MessageCard extends Component{
    constructor(props) {
        super(props);
        this.state = ({

        });
    }
    callback(key) {
        console.log(key);
    }
    showTab() {
        if (this.props.message != null) {
            console.log(this.props.message);
            return (
                <Tabs onChange={this.callback} type="card">
                    <TabPane tab={I18n.t('messageCard.noread')} key="1">
                        <NoReadBox data={this.props.message.filter(item=>item.sign==1)} />
                    </TabPane>
                    <TabPane tab={I18n.t('messageCard.read')} key="2">
                        <ReadBox data={this.props.message.filter(item=>item.sign==0)} />
                    </TabPane>
                </Tabs>
            );
        }
    }
    render(){
        return(
            <div className="MessageCard">
                <p style={{width:'100%',paddingLeft:8,borderLeft:'4px solid #0c5aa2',fontSize:'18px'}}>{I18n.t('messageCard.name')}</p>
                <div className="SellerCenterUploadStock SellerCenterModelAudit">
                    {this.showTab()}
                </div>
            </div>
        )
    }
}
export default createContainer(() => {
    Meteor.subscribe('message');
    return {
        message: Message.find({to: Meteor.userId()}, {sort: {date: -1}}).fetch(),
    };
}, MessageCard);