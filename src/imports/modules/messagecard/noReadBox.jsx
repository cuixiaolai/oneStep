import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Meteor } from 'meteor/meteor';
import config from '../../config.json';
import { Tabs,Table,Button,Modal,Timeline,Pagination } from 'antd';
import MessageBox from './MessageBox.jsx';
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;

export default class NoReadBox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            arr:[],
            current: 1,
        });
    }
    callback(key) {
        console.log(key);
    }
    ShowTimeLine(){
        let arr = [];
        let data = this.props.data.slice((this.state.current-1)*10, this.state.current*10);
        for(let i = 0; i < data.length; i ++){
            arr[i] = (
                <Timeline.Item>
                    <MessageBox data={data[i]} />
                </Timeline.Item>
            )
        }
        return arr;
    }
    onChange(page) {
        console.log(page);
        this.setState({
            current: page,
        });
    }
    ShowPagination(){
        return(
            <Pagination onChange={this.onChange.bind(this)} showQuickJumper total={this.props.data.length}  />
        )
    }
    clickAll(){
        confirm({
            title:I18n.t('messageCard.sure'),
            content: I18n.t('messageCard.words'),
            okText:I18n.t('buyerForm.yes'),
            cancelText:I18n.t('buyerForm.no'),
            onOk() {
                Meteor.call('message.deleteAllnoReadMessage');
            },
            onCancel() {},
        });
    }
    render(){
        if(this.props.data.length > 0){
            return(
                <div className="noReadBoxInner">
                    <p style={{textAlign:'right'}}> <span onClick={this.clickAll}>{I18n.t('messageCard.allmessage')}</span> </p>
                    <Timeline>
                        {this.ShowTimeLine()}
                    </Timeline>
                    <div className="ShowPaginationBox" style={this.props.data.length > 10 ? {display:'block'} : {display:'none'}}>
                        {this.ShowPagination()}
                        <div className="clear"></div>
                    </div>
                </div>
            )
        }else{
            return(
                <div className="NoReadBox">
                    <p>
                        <img src={(config.theme_path + 'noaddress.png')} alt=""/>
                        <span>{I18n.t('messageCard.nomessage')}</span>
                    </p>

                </div>
            )
        }

    }
}
