import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Meteor } from 'meteor/meteor';

export default class MessageBox extends Component{
    constructor(props) {
        super(props);
        this.state={
            time: '',
            top: '',
            string: '',
            inner: '',
        };
    }
    initData(data) {
        let inner = '';
        let month = (data.date.getMonth()+1).toString();
        month = month.length == 2 ? month : '0'+month;
        let day = data.date.getDate().toString();
        day = day.length == 2 ? day : '0'+day;
        let hour = data.date.getHours().toString();
        hour = hour.length == 2 ? hour : '0'+hour;
        let minutes = data.date.getMinutes().toString();
        minutes = minutes.length == 2 ? minutes : '0'+minutes;
        let seconds = data.date.getSeconds().toString();
        seconds = seconds.length == 2 ? seconds : '0'+seconds;
        let date = data.date.getFullYear()+'-'+month+'-'+day+' '+hour +':'+minutes+':'+seconds;
        switch (data.type) {
            case 1:
                inner = I18n.t('messageCard.tips.log1') + data.string;
                break;
            case 2:
                inner = I18n.t('messageCard.tips.log2_1') + data.string + I18n.t('messageCard.tips.log2_2');
                break;
            case 3:
                inner = I18n.t('messageCard.tips.log3_1') + data.string + I18n.t('messageCard.tips.log3_2');
                break;
            case 4:
                inner = I18n.t('messageCard.tips.log4_1') + data.string + I18n.t('messageCard.tips.log4_2');
                break;
            case 5:
                inner = I18n.t('messageCard.tips.log5_1') + data.string + I18n.t('messageCard.tips.log5_2');
                break;
            case 6:
                inner = I18n.t('messageCard.tips.log6_1') + data.string + I18n.t('messageCard.tips.log6_2');
                break;
            case 7:
                inner = I18n.t('messageCard.tips.log7_1') + data.string + I18n.t('messageCard.tips.log7_2');
                break;
            case 8:
                inner = I18n.t('messageCard.tips.log8_1') + data.string + I18n.t('messageCard.tips.log8_2');
                break;
            case 9:
                inner = I18n.t('messageCard.tips.log9_1') + data.string + I18n.t('messageCard.tips.log9_2');
                break;
            case 10:
                inner = I18n.t('messageCard.tips.log10_1') + data.string + I18n.t('messageCard.tips.log10_2');
                break;
            case 11:
                inner = I18n.t('messageCard.tips.log11_1') + data.string + I18n.t('messageCard.tips.log11_2');
                break;
            case 12:
                inner = I18n.t('messageCard.tips.log12_1') + data.string + I18n.t('messageCard.tips.log12_2');
                break;
            case 13:
                inner = I18n.t('messageCard.tips.log13_1') + data.string + I18n.t('messageCard.tips.log13_2');
                break;
            case 21:
                inner = I18n.t('messageCard.tips.log21');
                break;
            case 22:
                inner = I18n.t('messageCard.tips.log22_1') + data.string + I18n.t('messageCard.tips.log22_2');
                break;
            case 23:
                inner = I18n.t('messageCard.tips.log23');
                break;
            case 24:
                inner = I18n.t('messageCard.tips.log24_1') + data.string + I18n.t('messageCard.tips.log24_2');
                break;
            case 25:
                inner = I18n.t('messageCard.tips.log25');
                break;
            case 26:
                inner = I18n.t('messageCard.tips.log26_1') + data.string + I18n.t('messageCard.tips.log26_2');
                break;
            case 27:
                inner = I18n.t('messageCard.tips.log27');
                break;
            case 28:
                inner = I18n.t('messageCard.tips.log28_1') + data.string + I18n.t('messageCard.tips.log28_2');
                break;
            case 29:
                inner = I18n.t('messageCard.tips.log29');
                break;
            case 30:
                inner = I18n.t('messageCard.tips.log30_1') + data.string + I18n.t('messageCard.tips.log30_2');
                break;
            case 31:
                inner = I18n.t('messageCard.tips.log31');
                break;
            case 32:
                inner = I18n.t('messageCard.tips.log32_1') + data.string + I18n.t('messageCard.tips.log32_2');
                break;
            case 33:
                inner = I18n.t('messageCard.tips.log33');
                break;
            case 34:
                inner = I18n.t('messageCard.tips.log34_1') + data.string + I18n.t('messageCard.tips.log34_2');
                break;
            case 41:
                inner = data.string;
                break;
            default:
                break;
        }
        if (data.type <= 20) {
            this.setState({time: date, top: '订单消息', string: data.string, inner: inner});
        }
        else if (data.type <= 40) {
            this.setState({time: date, top: '审核消息', string: '', inner: inner});
        }
        else {
            this.setState({time: date, top: '系统消息', string: '', inner: inner});
        }
    }
    componentWillMount() {
        if (this.props.data != null) {
            this.initData(this.props.data);
        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.data._id != nextProps.data._id) {
            this.initData(nextProps.data);
        }
    }
    click(){
        Meteor.call('message.deleteMessage', this.props.data._id);
    }
    renderId() {
        if (this.state.string != '') {
            return <p style={{marginBottom:'8px',paddingLeft:'15px'}}>{I18n.t('OrderConfirm.ordernumber')} {this.state.string} </p>;
        }
    }
    render(){
        return(
            <div>
                {this.state.time}
                <div className="noReadSmallBox">
                    <p style={{textAlign:'right'}}><span onClick={this.click.bind(this)} style={{cursor:'pointer',color:'#999'}}>×</span></p>
                    <div className="noreadSmallBoxInner">
                        <p className="noReadSmallBoxname">
                            {this.state.top}
                        </p>
                        <p className="xians"></p>
                        {this.renderId()}
                        <p style={{paddingLeft:'15px',lineHeight:1.5}}> {this.state.inner} </p>
                    </div>
                </div>
            </div>
        )
    }
}
