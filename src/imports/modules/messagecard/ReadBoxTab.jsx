import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Tabs,Table,Button,Modal } from 'antd';
import config from '../../config.json';
import { createContainer } from 'meteor/react-meteor-data';
import { Seller } from '../../api/Seller.js';
const confirm = Modal.confirm;
const TabPane = Tabs.TabPane;
import { Timeline,Pagination } from 'antd';
import MessageBox from './MessageBox.jsx';


export default class ReadBox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            current: 1,
        });
    }
    ShowList(){
        let arr = [];
        let data = this.props.data.slice((this.state.current-1)*10, this.state.current*10);
        for(let i = 0; i < data.length; i ++){
            arr[i] = (
                <Timeline.Item>
                    <MessageBox data={data[i]} />
                </Timeline.Item>
            )
        }
        return arr;
    }
    onChange(page) {
        console.log(page);
        this.setState({
            current: page,
        });
    }
    ShowPagination(num){
        return(
            <Pagination onChange={this.onChange.bind(this)} showQuickJumper total={num} />
        )
    }
    render(){
        if (this.props.data.length > 0) {
            return(
                <div className="noReadBoxInner">
                    <Timeline>
                        {this.ShowList()}
                    </Timeline>
                    <div className="ShowPaginationBox" style={this.props.data.length>10 ? {display:'block'} : {display:'none'}}>
                        {this.ShowPagination(this.props.data.length)}
                        <div className="clear"></div>
                    </div>
                </div>
            )
        }
        else{
            return(
                <div className="NoReadBox">
                    <p>
                        <img src={(config.theme_path + 'noaddress.png')} alt=""/>
                        <span>{I18n.t('messageCard.nomessage')}</span>
                    </p>

                </div>
            )
        }
    }
}