import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Icon} from 'antd';
import config from '../config.json';

import HelpDocs from '../ui/helpDocs.jsx';

export default class BottomBox extends Component{

        constructor(props) {
        super(props);
        this.state = ({
            checked:false,
            flag:false,
            path:11,
        });
    }
    onClick(a){

        console.log("111111111111")
        console.log("a"+a)
        this.setState({flag:!this.state.flag});
        this.setState({path:a})
        console.log("path="+this.state.path)
        console.log("falg="+JSON.stringify(this.state));
    }
    Cloce(){
        this.setState({flag:false,checked:'true'});
    }

    bottomfun(){
        if(this.props.path&&
            (this.props.path=='Login'||this.props.path=='Register')){
            return(
                <div className="bottomBox">
                    <div className="BOTTOM-INNER">
                        <p>Copyright &copy;2015-2016{I18n.t('bottom.company')}</p>
                    </div>
                </div>
            )
        }else{
            return(
                <div id="bottomBox" className="bottomBox">
                    <div className="bottomBoxTop">
                        <div className="bottomBoxTop-inner">
                            <div className="left">
                                <img src={(config.theme_path + 'bottom1_03.png')} alt=""/>
                                <p>{I18n.t('bottomtop.one')}</p>
                                <div className="line"></div>
                            </div>
                            <div className="left">
                                <img src={(config.theme_path + 'bottom2_05.png')} alt=""/>
                                <p>{I18n.t('bottomtop.two')}</p>
                                <div className="line"></div>
                            </div>
                            <div className="left">
                                <img src={(config.theme_path + 'bottom3_07.png')} alt=""/>
                                <p>{I18n.t('bottomtop.three')}</p>
                                <div className="line"></div>
                            </div>
                            <div className="left">
                                <img src={(config.theme_path + 'bottom4_09.png')} alt=""/>
                                <p>{I18n.t('bottomtop.four')}</p>
                                <div className="line"></div>
                            </div>
                            <div className="left">
                                <img src={(config.theme_path + 'bottom5_11.png')} alt=""/>
                                <p>{I18n.t('bottomtop.five')}</p>
                            </div>
                            <p className="clear"></p>
                        </div>
                    </div>
                    <div className="bottomBox-inner">
                        <div className="bottomBox-inner-top">
                            <ul>
                                <li className="bottomBox-lists">
                                    <p><Translate value="bottom.about.name" /><span><Icon type="down" /></span> </p>
                                    <ul>
                                        <li> <a href="http://www.alphabeta-group.com"><Translate value="bottom.about.one" /></a> </li>
                                        <li> <a href="http://www.alphabeta-group.com"><Translate value="bottom.about.two" /></a></li>
                                        <li> <a href="http://www.alphabeta-group.com"><Translate value="bottom.about.three" /></a></li>
                                        <li> <a href="http://www.alphabeta-group.com"><Translate value="bottom.about.four" /></a></li>
                                    </ul>
                                </li>
                                <li className="bottomBox-lists">
                                    <p> <Translate value="bottom.zhinan.name" /><span><Icon type="down" /></span> </p>
                                    <ul>
                                        <li onClick={this.onClick.bind(this,21)}><Translate value="bottom.zhinan.one" /></li>
                                        <li onClick={this.onClick.bind(this,22)}><Translate value="bottom.zhinan.two" /></li>
                                        <li onClick={this.onClick.bind(this,23)}><Translate value="bottom.zhinan.three" /></li>
                                        <li onClick={this.onClick.bind(this,24)}><Translate value="bottom.zhinan.fore" /></li>
                                    </ul>
                                </li>
                                <li className="bottomBox-lists">
                                    <p><Translate value="bottom.zhifu.name" /><span><Icon type="down" /></span> </p>
                                    <ul>
                                        <li onClick={this.onClick.bind(this,31)}><Translate value="bottom.zhifu.one" /></li>
                                        <li onClick={this.onClick.bind(this,32)}><Translate value="bottom.zhifu.two" /></li>
                                        <li onClick={this.onClick.bind(this,33)}><Translate value="bottom.zhifu.three" /></li>
                                        <li onClick={this.onClick.bind(this,34)}><Translate value="bottom.zhifu.four" /></li>
                                        <li onClick={this.onClick.bind(this,35)}><Translate value="bottom.zhifu.five" /></li>
                                    </ul>
                                </li>

                                <li className="bottomBox-lists">
                                    <p><Translate value="bottom.fuwu.name" /><span><Icon type="down" /></span> </p>
                                    <ul>
                                        <li onClick={this.onClick.bind(this,41)}><Translate value="bottom.fuwu.one" /></li>
                                        <li onClick={this.onClick.bind(this,42)}><Translate value="bottom.fuwu.two" /></li>
                                        <li onClick={this.onClick.bind(this,43)}><Translate value="bottom.fuwu.three" /></li>
                                        <li onClick={this.onClick.bind(this,44)}><Translate value="bottom.fuwu.four" /></li>
                                        <li onClick={this.onClick.bind(this,45)}><Translate value="bottom.fuwu.five" /></li>
                                    </ul>
                                </li>
                                <li className="bottomBox-lists">
                                    <p> <Translate value="bottom.fapiao.name" /><span><Icon type="down" /></span> </p>
                                    <ul>
                                        <li onClick={this.onClick.bind(this,51)}><Translate value="bottom.fapiao.one" /></li>
                                        <li onClick={this.onClick.bind(this,52)}><Translate value="bottom.fapiao.two" /></li>
                                        <li onClick={this.onClick.bind(this,53)}><Translate value="bottom.fapiao.three" /></li>
                                        <li onClick={this.onClick.bind(this,54)}><Translate value="bottom.fapiao.four" /></li>
                                        <li onClick={this.onClick.bind(this,55)}><Translate value="bottom.fapiao.five" /></li>
                                    </ul>
                                </li>
                                <li className="bottomBox-lists">
                                    <img src={(config.file_server+config.theme_path+'bottom1_03.jpg')} classname="iew-h" alt=""/>
                                    <p><Translate value="bottom.sao" /></p>
                                </li>
                                <div className="clear"></div>
                            </ul>
                        </div>
                        <div className="bottomBox-xian"></div>
                        <div className="bottomBox-inner-bottom">
                            <p><img src={(config.theme_path + 'phone_03.png')} alt=""/> {I18n.t('bottomtop.worktime')} </p>
                            <p> Copyright &copy; <Translate value="bottom.company" /></p>
                        </div>
                    </div>
                    <HelpDocs  click={this.Cloce.bind(this)}  flag={this.state.flag}   path={this.state.path}/>
                </div>
            )
        }
    }
    render(){
        return(
           this.bottomfun()
        )
    }
}
