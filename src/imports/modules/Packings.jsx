import React, { Component } from 'react';
import { Cascader } from 'antd';

const options = [{
    value: 'juandai',
    label: '卷带',
    children: [{
        value: 'weichai',
        label: '未拆原厂包装',
    },{
        value: 'bufen',
        label: '部分原厂包装',
    },{
        value: 'gongfang',
        label: '供方包装',
    }],
}, {
    value: 'guanzhuang',
    label: '管装',
    children: [{
        value: 'weichai',
        label: '未拆原厂包装',
    },{
        value: 'bufen',
        label: '部分原厂包装',
    },{
        value: 'gongfang',
        label: '供方包装',
    }],
}, {
    value: 'tuopan',
    label: '托盘',
    children: [{
        value: 'weichai',
        label: '未拆原厂包装',
    },{
        value: 'bufen',
        label: '部分原厂包装',
    },{
        value: 'gongfang',
        label: '供方包装',
    }],
}, {
    value: 'yuanbaozhuang',
    label: '原包装',
    children: [{
        value: 'weichai',
        label: '未拆原厂包装',
    },{
        value: 'bufen',
        label: '部分原厂包装',
    },{
        value: 'gongfang',
        label: '供方包装',
    }],
}, {
    value: 'lingsanbaozhuang',
    label: '零散包装',
    children: [{
        value: 'weichai',
        label: '未拆原厂包装',
    },{
        value: 'bufen',
        label: '部分原厂包装',
    },{
        value: 'gongfang',
        label: '供方包装',
    }],
}];

const Packing = React.createClass({
    render(){
        return(
            <div style={this.props.style}>
                <Cascader value={this.props.value} placeholder={this.props.placeholder} size="large"  options={options} onChange={this.props.onChangePackings} />
            </div>
        )
    }
})
export default Packing;