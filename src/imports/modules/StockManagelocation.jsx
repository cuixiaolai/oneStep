import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import {Input,Icon,Tabs} from 'antd';
const TabPane = Tabs.TabPane;

const provinceData = ['北京', '天津', '河北', '山西', '内蒙古', '辽宁', '吉林', '黑龙江', '上海', '江苏',
    '浙江', '安徽', '福建', '江西', '山东', '河南', '湖北', '湖南', '广东', '广西', '海南', '重庆',
    '四川', '贵州', '云南', '西藏', '陕西', '甘肃', '青海', '宁夏', '新疆', '台湾', '香港', '澳门'];
const cityData = {
    '北京': ['北京'],
    '天津': ['天津'],
    '河北': ['石家庄', '唐山', '秦皇岛', '邯郸', '邢台', '保定', '张家口', '承德', '沧州', '廊坊', '衡水'],
    '山西': ['太原', '大同', '阳泉', '长治', '晋城', '朔州', '忻州', '吕梁', '晋中', '临汾', '运城'],
    '内蒙古': ['呼和浩特', '包头', '乌海', '赤峰', '呼伦贝尔', '兴安盟', '通辽', '锡林郭勒盟', '乌兰察布盟', '伊克昭盟', '巴彦淖尔盟', '阿拉善盟'],
    '辽宁': ['沈阳', '大连', '鞍山', '抚顺', '本溪', '丹东', '锦州', '营口', '阜新', '辽阳', '盘锦', '铁岭', '朝阳', '葫芦岛'],
    '吉林': ['长春', '吉林', '四平', '辽源', '通化', '白山', '松原', '白城', '延边'],
    '黑龙江': ['哈尔滨', '齐齐哈尔', '鸡西', '鹤岗', '双鸭山', '大庆', '伊春', '七台河', '牡丹江', '黑河', '绥化', '大兴安岭'],
    '上海': ['上海'],
    '江苏': ['南京', '无锡', '徐州', '常州', '苏州', '南通', '连云港', '淮安', '盐城', '扬州', '镇江', '泰州', '宿迁'],
    '浙江': ['杭州', '宁波', '温州', '嘉兴', '湖州', '绍兴', '金华', '衢州', '舟山', '台州', '丽水'],
    '安徽': ['合肥', '芜湖', '蚌埠', '淮南', '马鞍山', '淮北', '铜陵', '安庆', '黄山', '滁州', '阜阳', '宿州', '六安', '宣城', '巢湖', '池州'],
    '福建': ['福州', '厦门', '宁德', '莆田', '泉州', '漳州', '龙岩', '三明', '南平'],
    '江西': ['南昌', '景德镇', '萍乡', '九江', '新余', '鹰潭', '赣州', '宜春', '上饶', '吉安', '抚州'],
    '山东': ['济南', '青岛', '淄博', '枣庄', '东营', '烟台', '潍坊', '济宁', '泰安', '威海', '日照', '莱芜', '临沂', '德州', '聊城', '滨州', '菏泽'],
    '河南': ['郑州', '开封', '洛阳', '平顶山', '安阳', '鹤壁', '新乡', '焦作', '濮阳', '许昌', '漯河', '三门峡', '南阳', '商丘', '信阳', '周口', '驻马店',],
    '湖北': ['武汉', '黄石', '十堰', '宜昌', '襄樊', '鄂州', '荆门', '孝感', '荆州', '黄冈', '咸宁', '随州', '恩施',],
    '湖南': ['长沙', '株洲', '湘潭', '衡阳', '邵阳', '岳阳', '常德', '张家界', '益阳', '郴州', '永州', '怀化', '娄底', '湘西'],
    '广东': ['广州', '韶关', '深圳', '珠海', '汕头', '佛山', '江门', '湛江', '茂名', '肇庆', '惠州', '梅州', '汕尾', '河源', '阳江', '清远', '东莞', '中山', '潮州', '揭阳', '云浮'],
    '广西': ['南宁', '柳州', '桂林', '梧州', '北海', '防城港', '钦州', '贵港', '玉林', '崇左', '来宾', '贺州', '百色', '河池'],
    '海南': [ '海口', '三亚'],
    '重庆': ['重庆'],
    '四川': ['成都', '自贡', '攀枝花', '泸州', '德阳', '绵阳', '广元', '遂宁', '内江', '乐山', '南充', '宜宾', '广安', '达川', '雅安', '阿坝', '甘孜', '凉山', '巴中', '眉山', '资阳'],
    '贵州': ['贵阳', '六盘水', '遵义', '铜仁',  '毕节', '安顺'],
    '云南': ['昆明', '曲靖', '玉溪', '昭通', '思茅', '西双版纳', '大理', '保山', '德宏', '丽江', '临沧'],
    '西藏': ['拉萨', '昌都', '山南', '日喀则', '那曲', '阿里', '林芝'],
    '陕西': ['西安', '铜川', '宝鸡', '咸阳', '渭南', '延安', '汉中', '安康', '商洛', '榆林'],
    '甘肃': ['兰州', '嘉峪关', '金昌', '白银', '天水', '酒泉', '张掖', '武威', '定西', '陇南', '平凉', '庆阳', '临夏', '甘南'],
    '青海': ['西宁', '海东', '海北', '黄南', '海南', '果洛', '玉树', '海西'],
    '宁夏': ['银川', '石嘴山', '吴忠', '固原', '中卫 '],
    '新疆': ['乌鲁木齐', '克拉玛依', '吐鲁番', '哈密', '昌吉', '博尔塔拉', '巴音郭楞', '阿克苏',  '喀什', '和田', '伊犁', '塔城','阿勒泰'],
    '台湾': ['台湾'],
    '香港': ['香港岛', '九龙区', '新界'],
    '澳门': ['澳门']
};

export default class StockManagelocation extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            flag: false,
            cities: cityData[provinceData[0]],
            province: provinceData[0],
            secondCity: cityData[provinceData[0]][0],
            key:1,
            closeflag:true,
        });
    }
    show(){
        console.log(this.state.province,this.state.secondCity);
    }
    click(){
        this.setState({flag:!this.state.flag});
    }
    callback(key) {
        console.log(key);
        this.setState({key:key});
    }
    handleProvinceChange(value) {
        this.setState({
            cities: cityData[value],
            province:value,
            secondCity: cityData[value][0],
            key:"2",
        });
    }
    onSecondCityChange(value) {
        this.setState({
            secondCity: value,
            flag:false,
        });
        let address = this.state.province + ' ' + value;
        this.props.func(address);
    }
    Close(){
        this.setState({flag:false});
    }
    locationClose(){
        this.setState({closeflag:true});

    }
    render(){
        const provinceOptions = provinceData.map(province => <span><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></span>);
        const cityOptions = this.state.cities.map(city => <span><span onClick={this.onSecondCityChange.bind(this,city)} key={city}>{city}</span></span>);
        return(
            <div style={this.props.style} className="Stocklocation">
                {
                    this.state.closeflag == true?(
                        <div>
                            <Input allowClear={true} type="text" readOnly="readonly" value={this.props.value} style={{height:30,width:100}} onClick={this.click.bind(this)}/>
                            {/*
                            <Icon type="plus" className="locationClose" onClick={this.locationClose.bind(this)} style={this.state.flag ? {transform:'scale(0.667) rotate(180deg)'} : {display:'none'}} onClick={this.locationClose.bind(this)} />
                             */}
                        </div>

                    ):(
                        <div>
                            <Input type="text" readOnly="readonly" value='请输入' style={{height:30,width:100}} onClick={this.click.bind(this)}/>

                        </div>
                    )
                }
                <Icon type="down" className="Stocklocation-down" style={this.state.flag ? {transform:'scale(0.667) rotate(180deg)'} : {transform:'scale(0.667) rotate(0deg)'}} />
                <div className="StocklocationBox" style={this.state.flag ? {display:'block'} : {display:'none'}}>
                    <Icon type="plus" className="StocklocationClose" onClick={this.Close.bind(this)} />
                    <Tabs onChange={this.callback.bind(this)} type="card" activeKey={this.state.key}>
                        <TabPane tab={<span>{this.state.province}<Icon type="down" /></span>} key="1">
                            <div className="StocklocationsmallBox" >
                                {provinceOptions}
                            </div>
                        </TabPane>
                        <TabPane tab={<span>{this.state.secondCity}<Icon type="down" /></span>} key="2">
                            <div className="StocklocationsmallBox">
                                {cityOptions}
                            </div>
                        </TabPane>
                    </Tabs>
                </div>
            </div>
        )
    }
}