import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Modal, Button } from 'antd';
const confirm = Modal.confirm;


export default class AlertBox extends Component{
    showConfirm() {
        confirm({
            title: <p>{this.props.words}</p> ,
            content: this.props.sure,
            onOk() {
                this.props.handleOk();
            },
            onCancel() {},
        });
    }
    render(){
        return(
            this.showConfirm()
        )
    }
}