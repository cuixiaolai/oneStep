import React, { Component } from 'react';
import { Cascader } from 'antd';

const options = [{
    value: 'beijing',
    label: '北京',
    children: [{
        value: 'beijing',
        label: '北京',
    }
    ],
}, {
    value: 'tianjin',
    label: '天津',
    children: [{
        value: 'tianjin',
        label: '天津',
    }],
}, {
    value: 'hebei',
    label: '河北',
    children: [{
        value: 'shijiahzuang',
        label: '石家庄',
    },{
        value: 'tangshan',
        label: '唐山',
    },{
        value: 'qinhuangdao',
        label: '秦皇岛',
    },{
        value: 'handan',
        label: '邯郸',
    },{
        value: 'qinhuangdao',
        label: '秦皇岛',
    },{
        value: 'xingtai',
        label: '邢台',
    },{
        value: 'baoding',
        label: '保定',
    },{
        value: 'zhangjiakou',
        label: '张家口',
    },{
        value: 'baoding',
        label: '保定',
    },{
        value: 'chengde',
        label: '承德',
    },{
        value: 'cangzhou',
        label: '沧州',
    },{
        value: 'langfang',
        label: '廊坊',
    },{
        value: 'hengshui',
        label: '衡水',
    }],
}];

const StockManage_location = React.createClass({
    render(){
        return(
            <div style={this.props.style}>
                <Cascader value={this.props.value} placeholder={this.props.placeholder} size="large"  options={options} onChange={this.props.onChangePackings()} />
            </div>
        )
    }
})
export default StockManage_location;