import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';

import { Button } from 'antd';


export default class Userregistration extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            flag:true,
        });
    }
    Close(){
        this.setState({flag:false});
    }
    render(){
        return(
            <div className="RegisterAgreement" style={this.state.flag ? {display:'block'} : {display:'none'}}>
                <p>{I18n.t('Agreement.Userregistration')}</p>
                <div className="RegisterAgreementinner">
                    <p>
                        {I18n.t('Agreement.Userregistrationname')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.Userregistrationnameone')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationnameoneth')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationnameoneed')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationnameonems')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationnameonetd')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.Userregistrationtwo')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationtwoth')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationtwotd')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.Userregistrationthree')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationthreees')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationthreeth')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationthreetd')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationthreeed')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationthrees')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationthreed')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.Userregistrationfour')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfours')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfoures')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfourty')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfourth')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfourtd')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfoured')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfouring')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfourcd')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfourad')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfourbd')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfourfd')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfourgd')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfourhd')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.Userregistrationfive')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfives')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationfived')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.Userregistrationsix')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationsixes')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationsixed')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationsixs')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.Userregistrationseven')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationsevens')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationsevenes')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationsevened')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationseventh')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationsevening')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationsevenad')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.Userregistrationeight')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationeightes')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationeights')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationeighted')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationeightth')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationeighting')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationeighttd')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.Userregistrationnine')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationnines')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationnineth')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationninetd')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationnineing')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.Userregistrationten')}
                    </p>
                    <p>
                        {I18n.t('Agreement.Userregistrationtenes')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.UserregistrationEleven')}
                    </p>
                    <p>
                        {I18n.t('Agreement.UserregistrationElevenes')}
                    </p>
                    <p className="name">
                        {I18n.t('Agreement.UserregistrationTwelve')}
                    </p>
                    <p>
                        {I18n.t('Agreement.UserregistrationTwelves')}
                    </p>
                    <p>
                        {I18n.t('Agreement.UserregistrationTwelveth')}
                    </p>
                    <p>
                        {I18n.t('Agreement.UserregistrationTwelvetd')}
                    </p>
                    <p>
                        {I18n.t('Agreement.UserregistrationTwelveing')}
                    </p>
                </div>
                <Button onClick={this.Close.bind(this)} type="primary">{I18n.t('Agreement.Useragree')}</Button>
            </div>
        )
    }
}