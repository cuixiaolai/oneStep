import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import ForgetFirst from './Forgetpasswd1.jsx';
import ForgetSecond from './ForgetPasswd2.jsx';
import ForgetThird from './ForgetPasswd3.jsx';
import ForgetFourth from './ForgetPasswd4.jsx';
import { Steps, Button } from 'antd';
import config from '../../config.json';


const Step = Steps.Step;
const array = Array.apply(null, [5,6,7,8,8]);
const steps = array.map((item,i) => ({
    title: '输入账户名'
}));

const ForgetPasswd = React.createClass({
    getInitialState() {
        return {
            current: 0,
            arr:this.arr,
            userId: '',
            username: '',
            email: '',
            phone: ''
        };
    },
    next() {
        let current = this.state.current + 1;
        if (current === steps.length-1) {
            current = 0;
        };
        this.setState({ current });
    },
    setUserInfo(user) {
        this.setState({
            userId: user._id,
            username: user.username,
        });
        if (user.emails != null) {
            this.setState({
                email: user.emails[0].address,
            });
        }
        if (user.phone != null) {
            this.setState({
                phone: user.phone
            });
        }
    },
    render(){
        const { current } = this.state;
        let forget1=this.refs.forget1;
        let forget2=this.refs.forget2;
        let forget3=this.refs.forget3;
        let forget4=this.refs.forget4;
        var arr=[forget1,forget2,forget3,forget4];
        var picarr=[135,345,555,765];
        let picShaw=this.refs.picShaw;
        if(forget1){
            for(var i=0;i<arr.length;i++){
                arr[i].style.display='none';
            };
            arr[current].style.display="block";
            picShaw.style.left=picarr[current]+'px';
        }

        return(
            <div className="Forgetpasswd">
                <Steps current={current}>
                    <Step title={I18n.t('forgetpassword.one.inter')}  key="1" />
                    <Step title={I18n.t('forgetpassword.one.Authentication')}  key="2" />
                    <Step title={I18n.t('forgetpassword.one.resetpassword')}  key="3" />
                    <Step title={I18n.t('forgetpassword.one.complete')}  key="4" />
                    <Step key="5" />
                    <div className="bigXian"></div>
                    <img src={(config.theme_path + '/center.png')} alt="" ref="picShaw" />
                </Steps>
                <div>
                    <div className="Forgetpasswd-box" ref="forget1">
                        <ForgetFirst forget={this.next} user={this.setUserInfo}/>
                    </div>
                    <div className="Forgetpasswd-box" ref="forget2">
                        <ForgetSecond forget={this.next} userId={this.state.userId} username={this.state.username} email={this.state.email} phone={this.state.phone}/>
                    </div>
                    <div className="Forgetpasswd-box" ref="forget3">
                        <ForgetThird forget={this.next}  userId={this.state.userId}/>
                    </div>
                    <div className="Forgetpasswd-box" ref="forget4">
                        <ForgetFourth forget={this.next}/>
                    </div>
                </div>
            </div>
        )
    }
});
export default ForgetPasswd;
