import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Input, Button, Checkbox,Radio } from 'antd';
import config from '../../config.json';

const RadioGroup = Radio.Group;
const FormItem = Form.Item
const createForm = Form.create;

let ForgetFourth = React.createClass({
    render(){
        return(
            <Form inline>
                <FormItem style={{marginBottom:'20px!important'}}>
                    <img src={(config.theme_path + '/bigright_03.png')} alt=""/>
                    <span>{I18n.t('forgetpassword.four.success')}</span>
                </FormItem>
                <FormItem>
                    <span>{I18n.t('forgetpassword.four.please')}</span>
                </FormItem>
                <FormItem>
                    <Link to="/login"><Button type="primary"><Translate value="forgetpassword.four.rightnow" /></Button> </Link>
                    <Link to="/"><Translate value="forgetpassword.four.index" /></Link>
                </FormItem>
            </Form>
        )
    }
})
ForgetFourth = createForm()(ForgetFourth);
export default ForgetFourth;
