import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
export default class Forget extends Component{
    render (){
        return (
            <div className="forget">
                    <Link to="/register" className="left" style={{color:"#0a66bc"}}><Translate value="Forget.soon"/></Link>
                    <Link to="/forget" className="right"><Translate value="Forget.forget"/></Link>
                <div className="clear"></div>
            </div>
        )
    }
}