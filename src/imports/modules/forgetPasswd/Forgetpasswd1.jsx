import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Input, Button, Checkbox } from 'antd';
import VerifyCode from '../VerifyCode.jsx';
const FormItem = Form.Item
const createForm = Form.create;
import config from '../../config.json';
let ForgetFirst = React.createClass({
    getInitialState() {
        return {
            verify_code: this.generateCode(),
            error:I18n.t('forgetpassword.one.error'),
            flag:false,
        };
    },
    HandleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }else {
                Meteor.call('user.checkUser', values.emailname, function(error, data) {
                    console.log(error);
                    if(error) {
                        this.setState({flag:true});
                    }
                    else {
                        this.props.user(data);
                        this.props.forget();
                    }
                }.bind(this));
            }
        });
    },
    generateCode() {
        let items = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPRSTUVWXYZ23456789'.split('');
        let randInt = function (start, end) {
            return Math.floor(Math.random() * (end - start)) + start;
        }
        let code = '';
        for (let i = 0; i < 4; ++i) {
            code += items[randInt(0, items.length)];
        }
        return code;
    },
    checkVerifyCode(rule, value, callback) {
        if (value && value.toLowerCase() != this.state.verify_code.toLowerCase()) {
            callback(I18n.t('Register.verifyCodeError'));
        } else {
            callback();
        }
    },
    checkUser(rule, value, callback) {
        if (!value) {
            callback();
        } else {
            setTimeout(() => {
                Meteor.call('user.checkUser', value, function(error) {
                    if (error) {
                        callback([new Error(I18n.t('Register.sorrys'))]);
                    } else {
                        callback();
                    }
                });
            }, 800);
        }
    },
    refresh() {
        this.props.form.resetFields(['emailyan']);
        this.setState({ verify_code: this.generateCode() });
    },
    render(){
        const { getFieldProps ,getFieldError, isFieldValidating } = this.props.form;
        const yanEmailProps = getFieldProps('emailyan', {
            rules: [
                { required: true, min: 4,max:4,message:I18n.t('Register.yanTishi')},
                { validator: this.checkVerifyCode }
            ],
        });
        const nameEmailProps = getFieldProps('emailname', {
            rules: [
                { required: true, min: 1, message: I18n.t('Register.user') },
                { validator: this.checkUser }
            ],
        });
        return(
                <Form inline onSubmit={this.handleSubmit}>
                    <FormItem
                        label={I18n.t('forgetpassword.one.user')}
                    >
                        <Input placeholder={I18n.t('forgetpassword.one.userPlaceholder')}
                            {...nameEmailProps}
                        />
                    </FormItem>
                    <FormItem
                        label={I18n.t('forgetpassword.one.Verification')}
                    >
                        <Input {...yanEmailProps} type="text" placeholder={I18n.t('Register.verification')} />
                        <VerifyCode code={this.state.verify_code} />
                        <div style={{position:'absolute',top:38,right:0}}>{I18n.t('kan')} <span style={{color:'#0a66bc',cursor:'pointer'}} onClick={this.refresh}>{I18n.t('shu')}</span> </div>
                    </FormItem>
                    <FormItem>
                        <Button type="primary" onClick={this.HandleSubmit}>{I18n.t('forgetpassword.next')}</Button>
                    </FormItem>
                    <div className="forgetError" style={this.state.flag ? {display:"block" }: {display:'none'}}>
                        <img src={(config.theme_path + '/error.png')} alt=""/>
                        <p>{this.state.error}</p>
                        <div className="clear"></div>
                    </div>
                </Form>
        )
    }
})
ForgetFirst = createForm()(ForgetFirst);
export default ForgetFirst;
