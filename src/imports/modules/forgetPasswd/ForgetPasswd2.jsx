import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import { Form,Input,Button,Checkbox,Radio,Cascader,message} from 'antd';
const RadioGroup = Radio.Group;
const FormItem = Form.Item
const createForm = Form.create;
import config from '../../config.json';


let ForgetSecond = React.createClass({
    getInitialState() {
        return {
            phone:'',
            flag:false,
            num:1,
            nums:60,
            sends:false,
            sendmessage:I18n.t('forgetpassword.two.messageVerification'),
            sendemail:I18n.t('forgetpassword.two.messageEmailVerification'),
            label:'',
            value:'',
        };
    },
    onChange(value) {
        console.log(value[0]);
        this.setState({value:value[0]});
        if(value[0]=='email'){
            let phone=this.state.sendemail;
            this.setState({phone: phone});
        }else{
            let phone=this.state.sendmessage;
            this.setState({phone: phone});
        }
    },
    HandleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }else{
                if (this.state.phone == I18n.t('forgetpassword.two.messageVerification') || (this.state.phone=='' && this.props.phone != "" && this.props.email=='')) {
                    console.log(I18n.t('forgetpassword.two.messageVerification'));
                    this.props.next();
                }
                else {
                    if (values.emailyan == null) {
                        this.setState({flag:true});
                        return;
                    }
                    // Meteor.call('user.verifyEmailResetPasswordCode', this.props.userId, values.emailyan, function(error) {
                    //     if (error) {
                    //         this.setState({flag:true});
                    //     }
                    //     else {
                    //         this.props.forget();
                    //     }
                    // }.bind(this));
                }
            }
        });
    },
    getOption() {
        let options = [];
        if (this.props.email != '') {
            let email = this.props.email;
            let arr = email.split('@');
            if (arr[0] <= 4) {
                let substr = arr[0].substring(1);
                substr = substr.replace(/[^\n\r]/g, '*');
                email = arr[0][0] + substr + '@' + arr[1];
            }
            else {
                let begin = (arr[0].length - 4)%2 === 0 ? (arr[0].length - 4)/2 : (arr[0].length - 3)/2
                console.log(begin);
                let substr = arr[0].substring(begin, begin+4);
                substr = substr.replace(/[^\n\r]/g, '*');
                email = arr[0].substring(0, begin) + substr + arr[0].substring(begin+4) + '@' + arr[1];
            }
            options.push({
                value: 'email',
                label: I18n.t('forgetpassword.two.email') + email,
            })
        }
        if (this.props.phone != '') {
            let phone = this.props.phone;
            let substr = phone.substring(4, 8);
            substr = substr.replace(/[^\n\r]/g, '*');
            phone = phone.substring(0, 4) +　substr + phone.substring(8);
            options.push({
                value: 'phone',
                label: I18n.t('forgetpassword.two.phone') + phone,
            })
        }
        if (options[0] != null){
            if(this.state.sends==false){
                return <Cascader size="large" options={options}  onChange={this.onChange} defaultValue={[options[0].value]}/>;
            }else if(this.state.sends==true) {
                return <Cascader size="large" options={options}    onChange={this.onChange} defaultValue={[options[0].value]}/>;
            }

        }
    },
    getTips() {
        if (this.state.label != '') {
            let phone = this.state.label;
            return <span>{phone}</span>;
        }
        if (this.state.phone == '') {
            if (this.props.email != ''){
                let phone=this.state.sendemail;
                return <span>{phone}</span>;
            }
            else if (this.props.phone != '') {
                let phone=this.state.sendmessage;
                return <span>{phone}</span>;
            }
        }
        else {
            if(this.state.sends==false){
                return <span>{this.state.phone}</span>;
            }else if(this.state.sends==true){
                let phone=(this.state.nums+I18n.t('sendagain'))
                return <span>{phone}</span>;
            }
        }
    },
    tick(){
        this.state.nums -= 1;
        this.setState({label: this.state.nums+I18n.t('sendagain')});
        if(this.state.nums == 0){
            clearInterval(this.interval);
            this.setState({sends:false,nums:60});
            this.setState({label: ''});
        }
    },
    SendMassage(e) {
        if(this.state.sends==false){
            if (this.state.phone == I18n.t('forgetpassword.two.messageVerification') || (this.state.phone=='' && this.props.phone != "" && this.props.email=='')) {
                this.setState({sends:true});
                this.interval = setInterval(this.tick, 1000);
                console.log(this.state.phone);
            }
            else{
                this.setState({sends:true});
                // Meteor.call('accounts.ResetPasswordEmail', this.props.userId, this.props.username, this.props.email);
                message.success(I18n.t('forgetpassword.two.success'),3);
                this.interval = setInterval(this.tick, 1000);
            }
        }
    },
    show(){
        if(this.state.num==1){
            const { getFieldProps } = this.props.form;
            const yanProps = getFieldProps('emailyan', {
                rules: [
                    { required: true, min: 4,max:4,message:I18n.t('Register.yanTishi')},
                    { validator: this.checkVerifyCode }
                ],
            });
            return(
                <Form inline form={this.props.form}>
                    <FormItem
                        label={I18n.t('forgetpassword.two.please')}
                    >
                        {this.getOption()}
                    </FormItem>
                    <FormItem
                        label={I18n.t('forgetpassword.two.user')}
                    >
                        <span>{this.props.username}</span>
                    </FormItem>
                    <FormItem
                        label={I18n.t('forgetpassword.two.Verification')}
                    >
                        <Input placeholder={I18n.t('forgetpassword.two.Verificationplaceholder')}
                            {...yanProps}
                        />
                        <div className="getyan" style={this.state.sends ? {  cursor: 'auto',color:'#999999'} : {  cursor:'pointer',color:'#5b5b5b'}} onClick={this.SendMassage}>
                            {this.getTips()}
                        </div>
                        <div className="forget-tishi" style={this.state.flag ? {display:"block" }: {display:'none'}}>
                            <img src={(config.theme_path + '/error.png')} alt=""/>
                            <p>{I18n.t('forgetpassword.two.error')}</p>
                            <div className="clear"></div>
                        </div>
                    </FormItem>
                    <FormItem>
                        <Button  type="primary" onClick={this.HandleSubmit}>{I18n.t('forgetpassword.next')}</Button>
                    </FormItem>
                </Form>
            )
        }else if(this.state.num==0){
            const success = function () {
                message.success('邮件发送成功！');
            };
            return(
                <div className="sendsuccess">
                    <p>
                        <span>{I18n.t('Register.perEmail.send')}</span>
                        <span style={{color:'#0a66bc'}}>3046***737@qq.com</span>
                    </p>
                    <p>
                        {I18n.t('Register.perEmail.please')}
                    </p>
                    <p>
                        <p>
                            {I18n.t('Register.perEmail.no')}
                            <div className="senderrorBox">
                                <p>
                                    <img src={config.theme_path + '/origin.png'} alt=""/>
                                    <span>{I18n.t('Register.perEmail.reasonone')}</span>
                                    <div className="clear"></div>
                                </p>
                                <p>
                                    <img src={config.theme_path + '/origin.png'} alt=""/>
                                    <span>{I18n.t('Register.perEmail.reasontwo')} <span onClick={success} style={{color:"#0a66bc",cursor:'pointer'}}>{I18n.t('Register.perEmail.again')}</span> </span>
                                    <div className="clear"></div>
                                </p>
                                <div className="jiao"></div>
                            </div>
                        </p>
                    </p>
                </div>
            )
        }
    },
    render(){
        return(
            this.show()
        )
    }
})
ForgetSecond = createForm()(ForgetSecond);
export default ForgetSecond;
