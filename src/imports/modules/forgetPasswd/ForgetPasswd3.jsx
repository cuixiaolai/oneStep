import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Input, Button, Checkbox,Radio } from 'antd';
import config from '../../config.json';

const RadioGroup = Radio.Group;
const FormItem = Form.Item
const createForm = Form.create;

let ForgetThird = React.createClass({
    getInitialState() {
        return {
           message:I18n.t('Register.passwordTishi')
        };
    },
    HandleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }
            else{
                Meteor.call('accounts.SetPassword', this.props.userId, values.emailpasswd, function(error) {
                    if(error) {

                    }
                    else {
                        this.props.forget();
                    }
                }.bind(this))
            }
        });
    },
    checkEmailPass(rule, value, callback) {
        const { validateFields } = this.props.form;
        let aa = value.indexOf('@');
        let bb = value.indexOf('!');
        let cc = value.indexOf('#');
        let dd = value.indexOf('$');
        let ee = value.indexOf('%');
        let ff = value.indexOf('&');
        let gg = value.indexOf('*');
        let hh = value.indexOf('(');
        let ii = value.indexOf(')');
        let jj = value.indexOf('[');
        let kk = value.indexOf(']');
        let ll = value.indexOf('{');
        let mm = value.indexOf('}');
        let nn = value.indexOf(';');
        let oo = value.indexOf('<');
        let pp = value.indexOf('>');
        let qq = value.indexOf(',');
        let rr = value.indexOf('/');
        let ss = value.indexOf('。');
        let tt = value.indexOf('《');
        let uu = value.indexOf('》');
        let vv = value.indexOf('|');
        let ww = value.indexOf('?');
        let xx = value.indexOf('~');
        let yy = value.indexOf('"');
        let zz = value.indexOf('^');
        let ab = value.indexOf("\ ");
        let ac = value.indexOf("'");
        if(aa<0&&bb<0&&cc<0&&dd<0&&ee<0&&ff<0&&gg<0&&hh<0&&ii<0&&jj<0&&kk<0&&ll<0&&mm<0&&nn<0&&oo<0&&pp<0&&qq<0&&rr<0&&ss<0&&tt<0&&uu<0&&vv<0&&ww<0&&xx<0&&yy<0&&zz<0&&ab<0&&ac<0){
            if (value) {
                validateFields(['emailrePasswd'], { force: true });
            }
            callback();
        }else{
            callback('不包含符号');
        }
    },
    checkEmailPass2(rule, value, callback) {
        const { getFieldValue } = this.props.form;
        if (value && value !== getFieldValue('emailpasswd')) {
            callback(I18n.t('Register.pwTishi'));
        } else {
            callback();
        }
    },
    render(){
        const { getFieldProps } = this.props.form;
        const passwdEmailProps = getFieldProps('emailpasswd', {
            rules: [
                { required: true, whitespace: true,min:6,max:20, message:I18n.t('Register.passwordTishi')},
                { validator: this.checkEmailPass },
            ],
        });
        const rePasswdEmailProps = getFieldProps('emailrePasswd', {
            rules: [{
                required: true,
                whitespace: true,
                message: I18n.t('Register.repasswordTishi'),
            }, {
                validator: this.checkEmailPass2,
            }],
        });
        return(
            <Form inline>
                <FormItem>
                    <img src={(config.theme_path + '/right_03.png')} alt=""/>
                    <span>{I18n.t('forgetpassword.three.success')}</span>
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('forgetpassword.three.password')}
                >
                    <Input {...passwdEmailProps} type="password" autoComplete="off" placeholder={I18n.t('forgetpassword.three.passwordplaceholder')}
                    />
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('forgetpassword.three.repassword')}
                >
                    <Input {...rePasswdEmailProps} type="password" autoComplete="off" placeholder={I18n.t('forgetpassword.three.repasswordplaceholder')}
                    />
                </FormItem>
                <FormItem>
                    <Button type="primary" onClick={this.HandleSubmit}>{I18n.t('forgetpassword.next')}</Button>
                </FormItem>
            </Form>
        )
    }
})
ForgetThird = createForm()(ForgetThird);
export default ForgetThird;
