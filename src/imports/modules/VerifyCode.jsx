import React, { Component, PropTypes } from 'react';
// import { Meteor } from 'meteor/meteor';
import { I18n } from 'react-redux-i18n';

export default class VerifyCode extends Component {
    constructor(props) {
        super(props);
        this.state = ({ code: '', img: '' });
    }

    componentWillMount() {
        this.setState({ code: this.props.code});
        // Meteor.call('generate.verify_code', this.props.code, function(error, data) {
        //     this.setState({ img: data });
        // }.bind(this));
        const url = "http://localhost:3001/api/v1/veifyCode/"+this.props.code;
                           var xhr_rate = new XMLHttpRequest();
                    xhr_rate.open('get', url, true);
                    xhr_rate.send();
                    xhr_rate.onload = function (event) {
                        const rate_ret = JSON.parse(xhr_rate.responseText)
                        if (rate_ret.code == 200) {
                            console.log(rate_ret.body)
                this.setState({ img: rate_ret.body });
                        }
                    }.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.code != this.state.code) {
            this.setState({ code: nextProps.code});
            // Meteor.call('generate.verify_code', nextProps.code, function(error, data) {
            //     this.setState({ img: data });
            // }.bind(this));
                    const url = "http://localhost:3001/api/v1/veifyCode/"+nextProps.code;
                           var xhr_rate = new XMLHttpRequest();
                    xhr_rate.open('get', url, true);
                    xhr_rate.send();
                    xhr_rate.onload = function (event) {
                        const rate_ret = JSON.parse(xhr_rate.responseText)
                        if (rate_ret.code == 200) {
                            console.log(rate_ret.body)
                 this.setState({ img: rate_ret.body });
                        }
                    }.bind(this)
        }
    }

    renderTest() {
        return <img src={this.state.img} alt=""/>;
    }

    render() {
        return (
            <div>
                {this.renderTest()}
            </div>
        );
    }
}
