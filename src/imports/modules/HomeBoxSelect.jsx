import React, { Component, PropTypes } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
// import { Product } from '../api/Product.js';
import { Select } from 'antd';
// import { createContainer } from 'meteor/react-meteor-data';
const Option = Select.Option;

export default class HomeBoxSelect extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            data: [],
            value: '',
            focus: false,
            selectedOptions:[],
        });
    }
    handleChange(e) {
        this.setState({value: e});
        this.props.setValue(e);
        if (e.trim() == '') {
            this.setState({data: []});
        }
        else {
            let q = '^' + e.trim().toUpperCase();
            let result = [];
            // let cursor = Product.find({"Manufacturer Part Number": {$regex: q}}, {fields: {'Manufacturer Part Number': 1, '_id': 0}, sort: ['Manufacturer Part Number','asc'], limit: 15});
            // console.log(cursor);
            // cursor.forEach(function(item) {
            //     result.push({value: item["Manufacturer Part Number"], text: item["Manufacturer Part Number"]});
            // });
            this.setState({data: result});
        }
    }
    handleFocusBlur(e) {
        this.setState({
            focus: e.target === document.activeElement,
        });
    }
    onChange(value,selectedOptions) {
        this.props.onChange(value,selectedOptions);
        this.setState({selectedOptions:value});
    }
    render() {
        const options = this.state.data.map(d => <Option key={d.value}>{d.text}</Option>);
        return(
            <Select
                combobox
                value={this.state.value}
                placeholder={this.props.placeholder}
                notFoundContent=""
                defaultActiveFirstOption={false}
                showArrow={false}
                filterOption={false}
                onChange={this.handleChange.bind(this)}
                onFocus={this.handleFocusBlur.bind(this)}
                onBlur={this.handleFocusBlur.bind(this)}
            >
                {options}
            </Select>
        )
    }
}

HomeBoxSelect.propTypes = {
};
// export default createContainer(() => {
//     Meteor.subscribe('product.part_number');
//     return {};
// }, HomeBoxSelect);
