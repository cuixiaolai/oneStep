import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Select,DatePicker } from 'antd';
const Option = Select.Option;

const provinceData = ['中国 +86','美国 +45','德国 +56','台湾 +93','港澳 +10'];
let SelectPhone = React.createClass({
    getInitialState() {
        return {
            job: provinceData[0],
        };
    },
    handleProvinceChange(value) {
        this.props.industry(value);
        this.setState({
            job: value,
        });
    },
    componentWillMount() {
        if (this.props.default != null) {
            this.setState({job: this.props.default});
            this.props.industry(this.props.default);
        }
        else {
            this.props.industry(provinceData[0]);
        }
    },
    render(){
        const provinceOptions = provinceData.map(province => <Option key={province}>{province}</Option>);
        return(
            <Select size="large" defaultValue={this.state.job} style={this.props.style} onChange={this.handleProvinceChange}>
                {provinceOptions}
            </Select>
        )
    }
})
export default SelectPhone;
