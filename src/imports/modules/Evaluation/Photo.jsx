import React, { Component } from 'react';
import { Link } from 'react-router'
import { Translate, I18n } from 'react-redux-i18n';
import { Upload, Icon, Modal,message } from 'antd';
import config from '../../config.json';

export default class Photo extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            previewVisible: false,
            previewImage: '',
            arr:[],
            arrname:[],
            ARR:[],
            number:'',
            fileList:'',
            red:false,
        });
    }
    handleCancel() {
        this.setState({
            previewVisible: false,
        });
    }
    handleChange(info){
        let i=this.state.arr.length;
        let arr=this.state.arr;
        let arrname=this.state.arrname;
        let ARR=[arr,arrname];
        info.fileList=[];
        if(this.state.arr.length<=5){
            if (info.file.status !== 'uploading'){

            }
            if (info.file.status === 'done'){
                i++;
                if(i<=10){
                    let path = info.file.response[0].path;
                    path=path.replace(/public/, '');
                    arr.push(path);
                    arrname.push(info.file.name);
                }else{
                    message.error(`最多为5张。`);
                }
                this.setState({arr:arr, arrname:arrname, ARR:ARR});
                this.props.setList(arr);
            } else if (info.file.status === 'error'){
                message.error(`${info.file.name} 上传失败。`);
            }
        }else{

        }
    }

    click(i){
        this.state.arr.splice(i,1);
        this.state.arrname.splice(i,1);
        this.state.ARR=[this.state.arr,this.state.arrname];
        this.setState({ARR:this.state.ARR,arr:this.state.arr,arrname:this.state.arrname});
        this.props.setList(this.state.arr);
    }

    Photo(){
        let PIC=[];
        if(this.props.close == false){
            for(let i=0; i<this.state.arr.length; i++){
                PIC[i] = (
                    <div className="PersonalCenterBuyer-Phone">
                        <div className="PersonalCenterBuyer-reddel" key={i} onClick={this.click.bind(this,i)} >
                            <img src={(config.theme_path + 'originerror.png')} alt=""/>
                        </div>
                        <div className="PersonalCenterBuyer-img">
                            <img src={(config.file_server + this.state.arr[i])}/>
                        </div>
                    </div>
                )
            }
        }else{
            for(let i=0;i<this.state.arr.length;i++){
                PIC[i] = (
                    <div className="PersonalCenterBuyer-Phone">
                        <div className="PersonalCenterBuyer-del">
                            <span key={i} onClick={this.click.bind(this,i)}  >+</span>
                        </div>
                        <div className="PersonalCenterBuyer-img">
                            <img src={(config.file_server + this.state.arr[i])}/>
                        </div>
                    </div>
                )
            }
        }
        return PIC;
    }
    ShowP(){
        if(this.props.flag == false){

        }else{
            return(
                <p style={{position:'absolute',bottom:'-18px',left:0}}>{I18n.t('management.upload')}{length}{I18n.t('zhang')}</p>
            )
        }
    }
    Upload(){
        let num=this.state.arr.length;
        let i=0;
        let numarr=[];
        let props = {
            action: config.file_server + 'seller',
            showUploadList: false,
            listType: 'picture-card',
            multiple:true,
            onChange: this.handleChange.bind(this),
            beforeUpload(file){
                num++;
                i++;
                numarr.push(num);
                console.log(num);
                let isJPG;
                if(num<=5){
                    if(file.size <= 4194304){
                        if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='image/pdf'){
                            isJPG=true;
                        }else{
                            isJPG=false;
                            message.error('you can  upload JPG，JPEG，PDF，BMP file~');
                        }
                    }else{
                        isJPG=false;
                        message.error('超过4m');
                    }
                }else{
                    num = (numarr[0]-1);
                    console.log(numarr,i ,num);
                    isJPG=false;
                    message.error(`最多为5张。`);
                }
                return isJPG;
            },
        };
        let length = 5-this.state.arr.length;
        console.log(length);
        if(this.state.arr.length>=5){
            return(
                <Upload disabled={true} {...props}>
                    <Icon type="plus-circle-o" />
                    <div className="ant-upload-text">{I18n.t('buyer.add')}</div>
                    {this.ShowP()}
                </Upload>
            )
        }else{
            return(
                <Upload {...props}>
                    <Icon type="plus-circle-o" />
                    <div className="ant-upload-text">{I18n.t('buyer.add')}</div>
                    {this.ShowP()}
                </Upload>
            )
        }
    }
    componentWillMount() {
        if (this.props.picList != null && this.props.picList.length > 0) {
            let arr = [];
            let arrname = [];
            let ARR=[arr,arrname];
            for (let i = 0; i < this.props.picList.length; ++ i) {
                arr.push(this.props.picList[i]);
                arrname.push(this.props.picList[i].replace('/uploads/seller/', ''));
            }
            this.setState({arr:arr, arrname:arrname, ARR:ARR});
        }
    }
    render(){
        return(
            <div>
                <div className="PersonalCenterBuyer-Picbox" style={this.props.style}>
                    {this.Photo()}
                    {this.Upload()}
                </div>
            </div>
        )
    }
}
