import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
import { Checkbox,Button,Input,Rate  } from 'antd';
import Photo from './Photo.jsx';

export default class Commodityevaluation extends Component{
    constructor(state) {
        super(state);
        this.state = ({
            comment: {arr:{'socle':0,'speed':0,'package':0,'service':0,'label':0}, goods:'', server:''}
        });
    }
    onChange(label, value){
        if(label == 'socle'){
            this.state.comment.arr.socle = value;
        }else if(label == 'speed'){
            this.state.comment.arr.speed = value;
        }else if(label == 'package'){
            this.state.comment.arr.package = value;
        }else if(label == 'service'){
            this.state.comment.arr.service = value;
        }else if(label == 'label'){
            this.state.comment.arr.label = value;
        }
        this.props.click(this.state.comment, this.props.keys);
    }
    setList(arr){
        console.log(arr);
    }
    Change(){
        let Commodityevaluationtextarea = document.getElementsByClassName('Commodityevaluationtextarea')[this.props.keys];
        let CommodityevaluationInput = document.getElementsByClassName('CommodityevaluationInput')[this.props.keys];
        this.state.comment.goods = CommodityevaluationInput.value;
        this.state.comment.server = Commodityevaluationtextarea.value;

        this.props.click(this.state.comment, this.props.keys);
    }
    render(){
        return(
            <div className="Commodityevaluation">
                <div className="left">
                    <img src={(config.theme_path + 'huo_03.jpg')} alt=""/>
                    <p style={{color:'#ed7020'}}>{this.props.name}</p>
                    <p>{I18n.t('evaluate.num')} {this.props.num} </p>
                </div>
                <div className="left CommodityevaluationText">
                    <div>
                        <div className="left">{I18n.t('management.goods')}</div>
                        <div className="left">
                            <Input onBlur={this.Change.bind(this)} type="textarea"  id="textarea" name="textarea" maxLength={500} placeholder={I18n.t('management.say')} className="CommodityevaluationInput" />
                        </div>
                        <div className="clear"></div>
                    </div>
                    <div>
                        <div className="left">{I18n.t('management.Evaluation')}</div>
                        <div className="left">
                            <Input onBlur={this.Change.bind(this)} className="Commodityevaluationtextarea" type="textarea"  id="textarea" name="textarea" />
                        </div>
                        <div className="clear"></div>
                    </div>
                    <p className="CommodityevaluationPhoto">
                        <Photo setList={this.setList.bind(this)} flag={true}/>
                    </p>
                    <p>
                        <span className="left">{I18n.t('management.Pin')}</span>
                        <Rate allowHalf defaultValue={0} onChange={this.onChange.bind(this,'socle')} />
                    </p>
                    <p>
                        <span>{I18n.t('management.speed')}</span>
                        <Rate allowHalf defaultValue={0}  onChange={this.onChange.bind(this,'speed')}/>
                    </p>
                    <p>
                        <span className="left">{I18n.t('management.package')}</span>
                        <Rate allowHalf defaultValue={0}  onChange={this.onChange.bind(this,'package')} />
                    </p>
                    <p>
                        <span className="left">{I18n.t('management.service')}</span>
                        <Rate allowHalf defaultValue={0}  onChange={this.onChange.bind(this,'service')}/>
                    </p>
                    <p>
                        <span className="left">{I18n.t('management.identification')}</span>
                        <Rate allowHalf defaultValue={0}  onChange={this.onChange.bind(this,'label')}/>
                    </p>
                </div>
                <div className="clear"></div>
            </div>
        )
    }
}
