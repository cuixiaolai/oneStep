import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { DatePicker, Form, Steps, Button,Input,Cascader , Col,Upload, Icon, Modal ,message} from 'antd';
import config from '../../config.json';
const FormItem = Form.Item;
const createForm = Form.create;
const Dragger = Upload.Dragger;

let PersonalCenterSecurityAuthenticationForm = React.createClass({
    getInitialState() {
        return {
            startValue: null,
            startString: null,
            endValue: null,
            endString: null,
            endOpen: false,
            priviewVisible: false,
            priviewImage: '',
            url: '',
            urls: '',
        };
    },
    HandleSubmit(e) {
        e.preventDefault();
        if(this.state.url == '' || this.state.urls == ''|| this.state.endValue == null || this.state.endValue == null ){
            message.error('请将信息填写完成！')
        }else{
            this.props.form.validateFields((errors, values) => {
                if (errors) {
                    console.log('Errors in form!!!');
                    return;
                }else{
                    $.ajax({
                        type: 'POST',
                        url: config.file_server + 'save_identity',
                        data: { path: this.state.url, path1: this.state.urls },
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                    // Meteor.call('buyer.addIdentityAuthentication', values.name, values.idcard, this.state.startString, this.state.endString, this.state.url.replace('tmp/', ''), this.state.urls.replace('tmp/', ''));
                    // Meteor.call('authentication.addIdentityAuthentication', Meteor.userId(), values.name, values.idcard, this.state.startString, this.state.endString, this.state.url.replace('tmp/', ''), this.state.urls.replace('tmp/', ''));
                }
            });
        }
    },
    disabledStartDate(startValue) {
        if (!startValue || !this.state.endValue) {
            return false;
        }
        return startValue.getTime() >= this.state.endValue.getTime();
    },
    disabledEndDate(endValue) {
        if (!endValue || !this.state.startValue) {
            return false;
        }
        return endValue.getTime() <= this.state.startValue.getTime();
    },
    onStartChange(date, dateString) {
        this.setState({
            startValue: date,
            startString: dateString,
        });
    },
    onEndChange(date, dateString) {
        this.setState({
            endValue: date,
            endString: dateString,
        });
    },
    handleStartToggle({ open }) {
        if (!open) {
            this.setState({ endOpen: true });
        }
    },
    handleEndToggle({ open }) {
        this.setState({ endOpen: open });
    },
    handleChange(info) {
        if (info.file.status !== 'uploading') {

        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace('public/', '');
            this.setState({url: path});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    },
    handleChanges(info) {
        if (info.file.status !== 'uploading') {
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace('public/', '');
            this.setState({urls: path});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    },
    render(){
        const { getFieldProps } = this.props.form;
        const nameProps = getFieldProps('name', {
            validate: [{
                rules: [
                    { required: true, min: 2,max:10,message:I18n.t('PersonalCenterSecurity.SecurityAuthentication.nameerror')},
                ],
                trigger: 'onBlur'
            }],
        });
        const idcardProps = getFieldProps('idcard', {
            validate: [{
                rules: [
                    { required: true, min: 18,max:18,message:I18n.t('PersonalCenterSecurity.SecurityAuthentication.idcard')},
                ],
                trigger: 'onBlur'
            }],
        });
        const formItemLayout = {
            labelCol: { span: 7 },
        };
        const props = {
            name: 'file',
            showUploadList: false,
            action: config.file_server + 'identity',
            onChange: this.handleChange,
            beforeUpload(file){
                let isJPG;
                if(file.size <= 4194304){
                    console.log(file.type);
                    if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='application/pdf'){
                        isJPG=true;
                    }else{
                        isJPG=false;
                        message.error('您只可以上传格式为 GIF, JPG, BMP, PNG 的图片！');
                    }
                }else{
                    isJPG=false;
                    message.error('超过4m');
                }
                return isJPG;
            }
        };
        const prop = {
            name: 'file',
            showUploadList: false,
            action: config.file_server + 'identity',
            onChange: this.handleChanges,
            beforeUpload(file){
                let isJPG;
                if(file.size <= 4194304){
                    console.log(file.type);
                    if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='application/pdf'){
                        isJPG=true;
                    }else{
                        isJPG=false;
                        message.error('您只可以上传格式为 GIF, JPG, BMP, PNG 的图片！');
                    }
                }else{
                    isJPG=false;
                    message.error('超过4m');
                }
                return isJPG;
            }
        };
        return(
            <Form form={this.props.form} inline className="Anthentication">
                <FormItem
                    hasFeedback
                    label={I18n.t('PersonalCenterSecurity.SecurityAuthentication.truename')}
                >
                    <Input  type="text" {...nameProps} />
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('PersonalCenterSecurity.SecurityAuthentication.IDcard')}
                >
                    <Input  type="text" {...idcardProps}/>
                </FormItem>
                <FormItem
                    label={I18n.t('PersonalCenterSecurity.SecurityAuthentication.data')}
                    hasFeedback
                >
                    <Col>
                        <FormItem>
                            <DatePicker
                                disabledDate={this.disabledStartDate}
                                value={this.state.startValue}
                                onChange={this.onStartChange}
                                toggleOpen={this.handleStartToggle}
                            />
                        </FormItem>
                    </Col>
                    <Col>
                        <p className="ant-form-split"></p>
                    </Col>
                    <Col>
                        <FormItem>
                            <DatePicker
                                disabledDate={this.disabledEndDate}
                                value={this.state.endValue}
                                onChange={this.onEndChange}
                                open={this.state.endOpen}
                                toggleOpen={this.handleEndToggle}
                            />
                        </FormItem>
                    </Col>
                </FormItem>
                <FormItem
                    label={I18n.t('PersonalCenterSecurity.SecurityAuthentication.up')}
                    {...formItemLayout}
                >
                    <div className="Anthentication-Idcard-left left">
                        <div className="Anthentication-upload left">
                            <Dragger {...props}>
                                <Icon type="plus-circle-o" />
                                <div className="ant-upload-text">{I18n.t('PersonalCenterSecurity.SecurityAuthentication.Positive')}</div>
                            </Dragger>
                            <div className="Anthentication-upload-img" style={this.state.url != '' ? {display:'block'} : {display:'none'}}>
                                <img src={(config.file_server + this.state.url)} alt=""  />
                                <Upload {...props}>
                                    {I18n.t('CompantInvoicsThree.change')}
                                </Upload>
                            </div>
                        </div>
                        <div className="Anthentication-upload left">
                            <Dragger {...prop}>
                                <Icon type="plus-circle-o" />
                                <div className="ant-upload-text">{I18n.t('PersonalCenterSecurity.SecurityAuthentication.other')}</div>
                            </Dragger>
                            <div className="Anthentication-upload-img" style={this.state.urls != '' ? {display:'block'}:{display:'none'}}>
                                <img src={(config.file_server + this.state.urls)} alt=""  />
                                <Upload {...prop}>
                                    {I18n.t('CompantInvoicsThree.change')}
                                </Upload>
                            </div>
                        </div>
                        <div className="clear"></div>
                    </div>
                    <div className="Anthentication-Idcard-right left">
                        <p>
                            {I18n.t('PersonalCenterSecurity.SecurityAuthentication.example')}
                        </p>
                        <div>
                            <img src={config.theme_path + 'exampler.png'} alt=""/>
                        </div>
                         <div className="clear"></div>
                        <p>
                            {I18n.t('PersonalCenterSecurity.SecurityAuthentication.example')}
                        </p>
                        <div>
                            <img src={config.theme_path + 'examplel.png'} alt=""/>
                        </div>
                        <div className="clear"></div>
                    </div>
                    <div className="clear"></div>
                </FormItem>
                <div className="Authentication-info">
                    <p>
                        <div className="left">
                            <img src={config.theme_path + 'hai.png'} alt=""/>
                        </div>
                        <p  className="left">{I18n.t('PersonalCenterSecurity.SecurityAuthentication.info')}</p>
                        <div className="clear"></div>
                    </p>
                    <ul>
                        <li>
                            <img src={config.theme_path + 'origin.png'} alt=""/>
                            <span >{I18n.t('PersonalCenterSecurity.SecurityAuthentication.infoone')}</span>
                        </li>
                        <li>
                            <img src={config.theme_path + 'origin.png'} alt=""/>
                            <span >{I18n.t('PersonalCenterSecurity.SecurityAuthentication.infotwo')}</span>
                        </li>
                        <li>
                            <img src={config.theme_path + 'origin.png'} alt=""/>
                            <span >{I18n.t('PersonalCenterSecurity.SecurityAuthentication.infothree')}</span>
                        </li>
                        <li>
                            <img src={config.theme_path + 'origin.png'} alt=""/>
                            <span >{I18n.t('PersonalCenterSecurity.SecurityAuthentication.infofour')}</span>
                        </li>
                    </ul>
                </div>
                <FormItem>
                    <Button type="primary" onClick={this.HandleSubmit}>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.Submit')}</Button>
                </FormItem>
            </Form>
        )
    }
})
PersonalCenterSecurityAuthenticationForm = createForm()(PersonalCenterSecurityAuthenticationForm);
export default PersonalCenterSecurityAuthenticationForm;
