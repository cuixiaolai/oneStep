import React, { Component, PropTypes } from 'react';
import { Button, Pagination, Select } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import AddressList from './AddressList.jsx';
import EditAddress from './EditAddress.jsx';
import AddAddress from './AddAddress.jsx';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Buyer } from '../../api/Buyer.js';
import config from '../../config.json';

export default class PersonalAddress extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            flags: true,
            address: null,
            current: 1,
            error:false,  
        });
    }
    click(){
        if(this.props.address.address == null || (this.props.address.address != null && this.props.address.address.length<20)){
            this.setState({address: null});
            this.setState({flags: false});
        }else if(this.props.address.address.length=20){
            this.setState({flags: true});
            this.setState({error:true});
        }
    }

    close(){
        this.setState({error:false});
    }

    clickOpen(item){
        this.setState({address: item});
        this.setState({flags: false});
    }

    clickClose(){
        this.setState({flags: true});
    }

    onChange(page){
        window.scrollTo(0,0);
        this.setState({current: page});
    }
    renderEdit() {
        if (this.state.flags == false) {
            if (this.state.address == null) {
                return (
                    <div>
                        <div style={{position: 'fixed',top: 0,bottom: 0,left: 0,right: 0,background: 'rgba(0,0,0,.1)',zIndex: 101}}/>
                        <AddAddress click={this.clickClose.bind(this)}/>
                    </div>
                );
            }
            else {
                return (
                <div>
                    <div style={{position: 'fixed',top: 0,bottom: 0,left: 0,right: 0,background: 'rgba(0,0,0,.1)',zIndex: 101}}/>
                    <EditAddress click={this.clickClose.bind(this)} item={this.state.address}/>
                </div>
                );
            }
        }
    }

    render() {
        let itemNumber = 0;
        if (this.props.address != null && this.props.address.address != null) {
            itemNumber = this.props.address.address.length;
        }
        if(itemNumber >= 6){
            this.state.flag = false;
        }
        return (
            <div className="personal-address-box">
                <Button onClick={this.click.bind(this)} name="1" className="addAddress" type="ghost">{I18n.t('Addresspage.add')}</Button>
                <p className="youhave">
                    {I18n.t('Addresspage.have')} <span style={{color:'#0685da'}}>{itemNumber}</span> {I18n.t('Addresspage.address')}
                </p>
                <AddressList address={this.props.address} click={this.clickOpen.bind(this)} page={this.state.current} num={itemNumber}/>
                <nav className="page" style={itemNumber < 6 ? {display:'none'} : {diplay:'block'}}>
                    <Pagination current={this.state.current} onChange={this.onChange.bind(this)} pageSize={5} total={itemNumber} />
                    <div className="clear"></div>
                </nav>
                {this.renderEdit()}
                <div className="AddaddressError" style={ this.state.error ? {display:'block'} : {display:'none'} }>
                    <img src={config.theme_path + 'close.png'} alt="" onClick={this.close.bind(this)}/>
                    <div>
                        <img src={config.theme_path + 'emailerror.png'} alt=""/>
                        <span>{I18n.t('Addresspage.error')}</span>
                    </div>
                    <p>{I18n.t('Addresspage.have')} <span style={{color:'#d43327'}}>{itemNumber}</span>{I18n.t('Addresspage.addresss')}  </p>
                    <Button type="primary" size="large" onClick={this.close.bind(this)}>{I18n.t('Addresspage.sure')}</Button>
                </div>
            </div>
        );
    }
}

// export default createContainer(() => {
//     Meteor.subscribe('buyer');
//     return {
//         address: Buyer.findOne({_id: Meteor.userId()}, { fields: {'address': 1, 'defaultAddress': 1}}),
//     };
// }, PersonalAddress);