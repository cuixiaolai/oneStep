import React, { Component } from 'react';
import { I18n } from 'react-redux-i18n';
// import { Meteor } from 'meteor/meteor';
import { Button, Form, Input,Select,Option ,Checkbox,Tabs,Icon,Radio,DatePicker } from 'antd';
import SelectJob from '../SelectJob.jsx';
import { Link } from 'react-router';
import AlreadyCertified from './AlreadyCertified.jsx';
import config from '../../config.json';
const RadioGroup = Radio.Group;
const createForm = Form.create;
const FormItem = Form.Item;

let PersonalCenterRight = React.createClass({
    HandleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }else {
                this.setState({sure:true});
                setTimeout(this.change,2000);
                let Personalname=this.refs.Personalname;
                let Personalcom=this.refs.Personalcom;
                let Personaljob=this.refs.Personaljob;
                // Meteor.call('buyer.updatePersonalInfo', Personalname.refs.input.value, this.state.sex, this.state.data, Personalcom.refs.input.value, Personaljob.refs.input.value, this.state.industry);
            }
        });
    },
    change(){
        this.setState({sure:false});
    },
    getInitialState() {
        return {
            sex: 1,
            data:'',
            industry:'',
            sure:false,
        };
    },
    onChangeSex(e) {
        this.setState({
            sex: e.target.value,
        });
    },
    onChangeDate(date, dateString) {
        this.setState({
            data: dateString,
        });
    },
    onChangeIndustry(value) {
        this.setState({
            industry: value,
        });
    },
    nameRender() {
        if (this.props.UserInfo && this.props.UserInfo.nickname) {
            return <Input maxLength={20}  ref="Personalname" defaultValue={this.props.UserInfo.nickname}/>;
        }
        else {
            return <Input maxLength={20}  ref="Personalname"/>;
        }
    },
    birthdayRender() {
        if (this.props.UserInfo && this.props.UserInfo.birthday) {
            return <DatePicker size="large" onChange={this.onChangeDate} defaultValue={this.props.UserInfo.birthday}/>;
        }
        else {
            return <DatePicker size="large" onChange={this.onChangeDate}/>;
        }
    },
    phoneRender() {
        if (this.props.User && this.props.User.phone) {
            let phone = this.props.User.phone;
            let substr = phone.substring(4, 8);
            substr = substr.replace(/[^\n\r]/g, '*');
            phone = phone.substring(0, 4) +　substr + phone.substring(8);
            return (
                <div>
                    <span>{phone}</span>
                    <div className="PerCenter-modify"><Link to="/personal_center/phone" style={{color: '#0ca232'}}>{I18n.t('PerCenterRight.modify')}</Link></div>
                </div>
            );
        }
        else {
            return (
                <div>
                    <span>{I18n.t('PerCenterRight.noBind')}</span>
                    <div className="PerCenter-modify"><Link to="/personal_center/new_phone" style={{color: '#0ca232'}}>{I18n.t('PerCenterRight.bind')}</Link></div>
                </div>
            );
        }
    },
    emailRender() {
        if (this.props.User && this.props.User.emails) {
            let email = this.props.User.emails[0].address;
            let arr = email.split('@');
            if (arr[0] <= 4) {
                let substr = arr[0].substring(1);
                substr = substr.replace(/[^\n\r]/g, '*');
                email = arr[0][0] + substr + '@' + arr[1];
            }
            else {
                let begin = (arr[0].length - 4)%2 === 0 ? (arr[0].length - 4)/2 : (arr[0].length - 3)/2
                let substr = arr[0].substring(begin, begin+4);
                substr = substr.replace(/[^\n\r]/g, '*');
                email = arr[0].substring(0, begin) + substr + arr[0].substring(begin+4) + '@' + arr[1];
            }
            return (
                <div>
                    <span>{email}</span>
                    <div className="PerCenter-modify"><Link to="/personal_center/email" style={{color: '#0ca232'}}>{I18n.t('PerCenterRight.modify')}</Link></div>
                </div>
            );
        }
        else {
            return (
                <div>
                    <span>{I18n.t('PerCenterRight.noBind')}</span>
                    <div className="PerCenter-modify"><Link to="/personal_center/new_email" style={{color: '#0ca232'}}>{I18n.t('PerCenterRight.bind')}</Link></div>
                </div>
            );
        }
    },
    companyRender() {
        if (this.props.UserInfo && this.props.UserInfo.company) {
            return <Input  ref="Personalcom" defaultValue={this.props.UserInfo.company}/>;
        }
        else {
            return <Input  ref="Personalcom"/>;
        }
    },
    careerRender() {
        if (this.props.UserInfo && this.props.UserInfo.career) {
            return <Input  ref="Personaljob" defaultValue={this.props.UserInfo.career}/>;
        }
        else {
            return <Input  ref="Personaljob"/>;
        }
    },
    industryRender() {
        if (this.props.UserInfo && this.props.UserInfo.industry) {
            return <SelectJob industry={this.onChangeIndustry} default={this.props.UserInfo.industry} style={{width:300,height:40}}/>;
        }
        else {
            return <SelectJob industry={this.onChangeIndustry} />;
        }
    },
    componentWillMount() {
        if (this.props.UserInfo && this.props.UserInfo.sex) {
            this.setState({ sex: this.props.UserInfo.sex });
        }
        if (this.props.UserInfo && this.props.UserInfo.birthday) {
            this.setState({ data: this.props.UserInfo.birthday });
        }
        if (this.props.UserInfo && this.props.UserInfo.industry) {
            this.setState({ industry: this.props.UserInfo.industry });
        }
    },
    Show(){
        if ( this.props.User != null) {
            // if (Roles.userIsInRole(Meteor.userId(), 'person')) {
                return (
                    <Form inline onSubmit={this.handleSubmit} form={this.props.form}>
                        <FormItem
                            hasFeedback
                            label={I18n.t('PerCenterRight.name')}
                        >
                            {this.nameRender()}
                        </FormItem>
                        <FormItem
                            hasFeedback
                            label={I18n.t('PerCenterRight.sex')}
                        >
                            <RadioGroup onChange={this.onChangeSex} value={this.state.sex}>
                                <Radio key="a" value={1}>{I18n.t('PerCenterRight.man')}</Radio>
                                <Radio key="b" value={2}>{I18n.t('PerCenterRight.woman')}</Radio>
                            </RadioGroup>
                        </FormItem>
                        <FormItem
                            hasFeedback
                            label={I18n.t('PerCenterRight.birthday')}
                        >
                            {this.birthdayRender()}
                        </FormItem>
                        <FormItem
                            hasFeedback
                            label={I18n.t('PerCenterRight.phone')}
                        >
                            {this.phoneRender()}
                        </FormItem>
                        <FormItem
                            hasFeedback
                            label={I18n.t('PerCenterRight.email')}
                        >
                            {this.emailRender()}
                        </FormItem>
                        <FormItem
                            hasFeedback
                            label={I18n.t('PerCenterRight.com')}
                        >
                            {this.companyRender()}
                        </FormItem>
                        <FormItem
                            hasFeedback
                            label={I18n.t('PerCenterRight.job')}
                        >
                            {this.careerRender()}
                        </FormItem>
                        <FormItem
                            hasFeedback
                            label={I18n.t('PerCenterRight.industry')}
                        >
                            {this.industryRender()}
                        </FormItem>
                        <FormItem>
                            <Button type="primary"
                                    onClick={this.HandleSubmit}>{I18n.t('PerCenterRight.save')}</Button>
                            <div  className="SURE" style={this.state.sure?{dislay:'inline-block'}:{display:'none'}}>
                                <div className="left" style={{height:14,marginLeft:15,marginRight:3}}>
                                    <img src={(config.theme_path + 'true.png')} alt="" style={{height:14}}/>
                                </div>
                                <p className="left" style={{fontSize:'14px'}}>{I18n.t('save')}</p>
                                <div className="clear"></div>
                            </div>
                        </FormItem>
                    </Form>
                )
            // } else if (Roles.userIsInRole(Meteor.userId(), 'company')) {
            //     /*个人中心-企业资料*/
            //     return (
            //         /*个人中心-企业资料-认证  */
            //         <AlreadyCertified User={this.props.User} authentication={this.props.UserInfo.authentication} company_url={this.props.UserInfo.company_url} company_detail={this.props.UserInfo.company_detail}/>

            //     )
            // }
        }
    },
    render() {
        return(
            this.Show()
        )
    }
});
PersonalCenterRight = createForm()(PersonalCenterRight);
export default PersonalCenterRight;