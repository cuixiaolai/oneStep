import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { DatePicker, Form, Steps, Button,Input,Cascader , Col,Upload, Icon, Modal ,message,Checkbox } from 'antd';
import config from '../../config.json';

let CompantInvoicsThree = React.createClass({
    getInitialState() {
        return {
            words:'',
            flag:false,
            word:'',
            flags:false,
        };
    },
    click(){
      this.setState({flag:!this.state.flag});
    },
    onclick(){
        this.setState({flags:!this.state.flags});
    },
    render(){
        console.log(this.props.invoice);
        if(this.state.flag==false){
            this.state.words=I18n.t('CompantInvoicsThree.look');
        }else if(this.state.flag==true){
            this.state.words=I18n.t('CompantInvoicsThree.hidden')
        }
        if(this.state.flags==false){
            this.state.word=I18n.t('CompantInvoicsThree.look');
        }else if(this.state.flags==true){
            this.state.word=I18n.t('CompantInvoicsThree.hidden')
        }
        return(
            <div className="CompantInvoicsThree">
                <p>
                    <img src={(config.theme_path +'bigright_03.png')} alt=""/>
                    {I18n.t('CompantInvoicsThree.success')}
                </p>
                <div>
                    <p className="CompantInvoicsThree-name">
                        <span>1</span>
                        <span>{I18n.t('CompantInvoicsThree.invoice')}</span>
                        <span onClick={this.click}>
                            {this.state.words}
                            <span><img src={(config.theme_path + 'down.png')} alt="" style={this.state.flag ? {transform:'rotate(180deg)'}:{}}/></span>
                        </span>
                    </p>
                    <div className="CompantInvoicsThree-box" style={this.state.flag ? {display:'block'} : {display:'none'}} >
                        <ul>
                            <li>
                                <span>{I18n.t('CompantInvoicsThree.comname')}</span>
                                {this.props.invoice.name}
                            </li>
                            <li>
                                <span>{I18n.t('CompantInvoicsThree.address')}</span>
                                {this.props.invoice.address}
                            </li>
                            <li>
                                <span>{I18n.t('CompantInvoicsThree.phone')}</span>
                                {this.props.invoice.tel}
                            </li>
                            <li>
                                <span>{I18n.t('CompantInvoicsThree.code')}</span>
                                {this.props.invoice.id}
                            </li>
                            <li>
                                <span>{I18n.t('CompantInvoicsThree.band')}</span>
                                {this.props.invoice.bank_name}
                            </li>
                            <li>
                                <span>{I18n.t('CompantInvoicsThree.bandnum')}</span>
                                {this.props.invoice.bank_account}
                            </li>
                        </ul>
                    </div>
                </div>
                <div>
                    <p className="CompantInvoicsThree-name">
                        <span>2</span>
                        <span>{I18n.t('CompantInvoicsThree.file')}</span>
                        <span onClick={this.onclick}>
                            {this.state.word}
                            <span><img src={(config.theme_path + 'down.png')} alt="" style={this.state.flags ? {transform:'rotate(180deg)'}:{}}/></span>
                        </span>
                    </p>
                    <div className="CompantInvoicsThree-box" style={this.state.flags ? {display:'block'} : {display:'none'}}>
                        <p>
                            <span className="left">{I18n.t('CompantInvoicsThree.Tax')}</span>
                            <img src={(config.file_server + this.props.invoice.tax)} alt="" className="left" />
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left">{I18n.t('CompantInvoicsThree.Taxpayer')}</span>
                            <img src={(config.file_server + this.props.invoice.tax_payer)} alt="" className="left" />
                            <div className="clear"></div>
                        </p>
                        <p>
                            <span className="left">{I18n.t('CompantInvoicsThree.attorney')}</span>
                            <img src={(config.file_server + this.props.invoice.attorney)} alt="" className="left" />
                            <div className="clear"></div>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
})
export default CompantInvoicsThree;