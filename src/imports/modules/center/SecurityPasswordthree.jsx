import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Steps, Button,Input,Cascader } from 'antd';
import config from '../../config.json';

import Securitylevel from './Securitylevel.jsx';

let SecurityPasswordthree = React.createClass({
    getInitialState(){
        return{
            safe:'',
        }
    },
    safe(){
        if (this.props.word != null) {
            let num = 4;
            let width = 100;
            num = this.props.word.phone != '' ? num - 1 : num;
            num = this.props.word.email != '' ? num - 1 : num;
            num = this.props.word.pay != null ? num - 1 : num;
            if (this.props.word.authentication != null && this.props.word.authentication.verify == true && this.props.word.authentication.success == true) {
                num = num - 1;
            }
            let tips;
            switch (num) {
                case 0:
                    tips = I18n.t('PersonalCenterSecurity.top.very');
                    break;
                case 1:
                    tips = I18n.t('PersonalCenterSecurity.top.top');
                    break;
                case 2:
                    tips = I18n.t('PersonalCenterSecurity.top.ordinary');
                    break;
                case 3:
                    tips = I18n.t('PersonalCenterSecurity.top.commonly');
                    break;
                case 4:
                    tips = I18n.t('PersonalCenterSecurity.top.Unsafe');
                    break;
                default:
                    break;
            }
            return (
                <p>
                    <span>{I18n.t('PersonalCenterSecurity.SecurityTopPassword.safe')}</span>
                    <span>
                        <Securitylevel num={num} width={width} />
                    </span>
                    <span>{tips}</span>
                </p>
            );
        }
    },
    render(){
        return(
            <div className="SecurityPasswordthree">
                <p>
                    <img src={config.theme_path + 'bigright_03.png'} alt=""/>
                    <span>{this.props.word.name}</span>
                </p>
                <p>
                    <Link to="/personal_center/security"><Button  type="primary" style={{marginLeft:0,width:115,height:32}}>{I18n.t('PersonalCenterSecurity.SecurityTopPassword.do')}</Button></Link>
                </p>
            </div>
        )
    }
})
export default SecurityPasswordthree;
