import React, { Component, PropTypes } from 'react';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
import { Button, Form, Input,Select ,Checkbox,Tabs,Icon,Cascader,DatePicker,message} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
import Selectcity from '../Selectcity.jsx';
const FormItem = Form.Item;
const createForm = Form.create;
const cityPhone = ['中国 +86','台湾 +886','香港 +852','美国 +1'];
const Option = Select.Option;
let EditAddress = React.createClass({
    getInitialState() {
        return {
            address: '',
            city: '',
        };
    },
    onChange(value){
        console.log(value);
    },
    handleSubmit(e) {
        e.preventDefault();
        if(this.state.address == '请选择 请选择 请选择'){
            message.error('请选择所在地址！');
        }else{
            this.props.form.validateFields((errors, values) => {
                if (errors) {
                    console.log('Errors in form!!!');
                    return;
                }
                else {
                    let phone = this.props.item.phone.split(' ');
                    let city = this.state.city == '' ? phone[0]+' '+phone[1] : this.state.city;
                    // Meteor.call('buyer.updateAddress', this.props.item.id, values.name, this.state.address, values.Detailedaddress, city+' '+values.phone, values.telephone, values.zipcode);
                    this.props.form.resetFields();
                    this.props.click();
                }
            });
        }
    },
    handleProvinceChange(value) {
        this.setState({
            city: value,
        });
    },
    handleChange(value) {
        console.log(`selected ${value}`);
    },
    setAddress(text) {
        this.setState({ address: text });
    },
    closeClick(e) {
        e.preventDefault();
        this.props.form.resetFields();
        this.props.click();
    },
    checkphone(rule,value,callback){
        if(value){
            let react = /^(\d{3,4}-)?\d{7,8}$/;
            if(!react.test(value)){
                callback(I18n.t('EditAddress.phone'));
            }else{
                callback();
            }
        }else {
            callback();
        }
    },
    userPhoneExists(rule, value, callback) {
        let react = /^1[34578]{1}\d{9}$/;
        if(value != ''){
            if(!react.test(value)){
                callback(I18n.t('Register.phonetishis'));
            }else{
                callback();
            }
        }else{
            callback(I18n.t('Register.nophone'));
        }
    },
    checkemail(rule, value, callback){
        let react =/^[1-9]\d{5}$/;
        if(value && value!=''){
            if(!react.test(value)){
                callback(I18n.t('EditAddress.zipcode'));
            }else{
                callback();
            }
        }else{
            callback();
        }
    },
    render(){
        console.log(this.props.item);
        const provinceOptions = cityPhone.map(province => <Option key={province}>{province}</Option>);
        const formItemLayout = {
            labelCol: { span: 7 },
        };
        const { getFieldProps ,getFieldError, isFieldValidating } = this.props.form;
        const nameEmailProps = getFieldProps('name', {
            initialValue: this.props.item.name,
            rules: [
                { required: true, min: 1,max:25, message: I18n.t('EditAddress.Addressee') },
            ],
        });
        const DetailedaddressProps=getFieldProps('Detailedaddress',{
            initialValue: this.props.item.detailAddress,
            rules: [
                { required: true, min: 1,max:120, message: I18n.t('EditAddress.EditAddress') },
            ],
        });
        let default_phone = this.props.item.phone.split(' ')[2] == null ? this.props.item.phone : this.props.item.phone.split(' ')[2];
        let default_city = this.props.item.phone.split(' ')[0] + ' ' + this.props.item.phone.split(' ')[1];
        let phoneProps;
        if (this.state.city == '') {
            switch (default_city) {
                case cityPhone[0]:
                    phoneProps  = getFieldProps('phone', {
                        initialValue: default_phone,
                        validate: [{
                            rules: [
                                { validator: this.userPhoneExists},
                            ],
                            trigger: 'onBlur'
                        }],
                    });
                    break;
                case cityPhone[1]:
                    phoneProps  = getFieldProps('phone', {
                        initialValue: default_phone,
                        validate: [{
                            rules: [
                                { required: true, min: 9, max: 9, message: I18n.t('EditAddress.phone') },
                            ],
                            trigger: 'onBlur'
                        }],
                    });
                    break;
                case cityPhone[2]:
                    phoneProps  = getFieldProps('phone', {
                        initialValue: default_phone,
                        validate: [{
                            rules: [
                                { required: true, min: 8, max: 8, message: I18n.t('EditAddress.phone') },
                            ],
                            trigger: 'onBlur'
                        }],
                    });
                    break;
                case cityPhone[3]:
                    phoneProps  = getFieldProps('phone', {
                        initialValue: default_phone,
                        validate: [{
                            rules: [
                                { required: true, min: 10, max: 10, message: I18n.t('EditAddress.phone') },
                            ],
                            trigger: 'onBlur'
                        }],
                    });
                    break;
                default:
                    break;
            }
        }
        else {
            switch (this.state.city) {
                case cityPhone[0]:
                    phoneProps  = getFieldProps('phone', {
                        initialValue: default_phone,
                        rules: [
                            { required: true, min: 11, max: 11, message: I18n.t('EditAddress.phone') },
                        ],
                    });
                    break;
                case cityPhone[1]:
                    phoneProps  = getFieldProps('phone', {
                        initialValue: default_phone,
                        rules: [
                            { required: true, min: 9, max: 9, message: I18n.t('EditAddress.phone') },
                        ],
                    });
                    break;
                case cityPhone[2]:
                    phoneProps  = getFieldProps('phone', {
                        initialValue: default_phone,
                        rules: [
                            { required: true, min: 8, max: 8, message: I18n.t('EditAddress.phone') },
                        ],
                    });
                    break;
                case cityPhone[3]:
                    phoneProps  = getFieldProps('phone', {
                        initialValue: default_phone,
                        rules: [
                            { required: true, min: 10, max:10, message: I18n.t('EditAddress.phone') },
                        ],
                    });
                    break;
                default:
                    break;
            }
        }
        const telephoneProps = getFieldProps('telephone', {
            initialValue: this.props.item.tel,
            validate: [{
                rules: [
                    { validator : this.checkphone},
                ],
                trigger: 'onBlur'
            }],
        });
        const ZipcodeProps= getFieldProps('zipcode', {
            initialValue: this.props.item.code,
            validate: [{
                rules: [
                    {validator : this.checkemail},
                ],
                trigger: 'onBlur'
            }],
        });
        return(
            <div className="Editaddress">
                <div className="Editaddress-inner">
                    <p className="Editaddress-name">
                        {I18n.t('EditAddress.edit')}
                        <img onClick={this.closeClick} src={(config.theme_path + '/close.png')} alt=""/>
                    </p>
                    <Form form={this.props.form}>
                        <FormItem
                            label={I18n.t('EditAddress.nameTitle')}
                            hasFeedback
                            required
                            {...formItemLayout}
                        >
                            <Input maxLength="25" {...nameEmailProps}/>
                        </FormItem>
                        <FormItem
                            label={I18n.t('EditAddress.addressTitle')}
                            required
                            {...formItemLayout}
                        >
                            <Selectcity  func={this.setAddress} default={this.props.item.address}/>
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label={I18n.t('EditAddress.detailAddressTitle')}
                            required
                            {...formItemLayout}
                        >
                            <Input className="area" type="textarea" maxLength="120" id="textarea" name="textarea" {...DetailedaddressProps} />
                        </FormItem>
                        <FormItem
                            label={I18n.t('EditAddress.phoneTitle')}
                            hasFeedback
                            {...formItemLayout}
                        >
                            <Select size="large" defaultValue={default_city} style={{ width:110,height:30,float: 'left',marginRight:10}} onChange={this.handleProvinceChange}>
                                {provinceOptions}
                            </Select>
                            <span className="left">
                                <Input {...phoneProps}/>
                            </span>
                            <div className="clear"></div>
                        </FormItem>
                        <FormItem
                            label={I18n.t('EditAddress.telTitle')}
                            {...formItemLayout}
                        >
                            <Input {...telephoneProps} />
                        </FormItem>
                        <FormItem
                            label={I18n.t('EditAddress.codeTitle')}
                            {...formItemLayout}
                        >
                            <Input {...ZipcodeProps}/>
                        </FormItem>
                        <FormItem>
                            <Button type="primary" htmlType="submit" onClick={this.handleSubmit}>{I18n.t('EditAddress.commit')}</Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
        )
    }
})
EditAddress = createForm()(EditAddress);
export default EditAddress;
