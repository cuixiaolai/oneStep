import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Button, Form, Input,Select,Option ,Checkbox} from 'antd';
import Rightnow from '../Rightnow.jsx';
export default class Alertsuccess extends Component{
    render (){
        return(
            <div className="alertBox">
                <Translate value="alertsuccess.happy" />
                <p> <span ref="time" >3</span> <Translate value="alertsuccess.time" /> </p>
                <p>
                    <Link to="/login"> <Button type="primary"><Translate value="alertsuccess.rightnow" /></Button> </Link>
                    <Link to="/"> <Button type="primary"><Translate value="alertsuccess.index" /></Button></Link>
                </p>
            </div>
        );
    }
}
