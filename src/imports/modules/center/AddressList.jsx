import React, { Component } from 'react';
import { Button,Pagination } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import Addressbox from './Addressbox.jsx';
import config from '../../config.json';

export default class AddressList extends Component{
    renderList() {
        if(this.props.num!=0){
            if (this.props.address != null && this.props.address.address != null){
                let address = [];
                let list = [];
                if (this.props.address.defaultAddress != null) {
                    let index = 0;
                    for (index = 0; index < this.props.address.address.length; ++ index) {
                        if (this.props.address.defaultAddress == this.props.address.address[index].id) {
                            address.push(this.props.address.address[index]);
                            break;
                        }
                    }
                    if (index != this.props.address.address.length) {
                        console.log(this.props.address.address);
                        for (let i = 0; i < this.props.address.address.length; ++ i) {
                            if (this.props.address.defaultAddress != this.props.address.address[i].id) {
                                address.push(this.props.address.address[i]);
                            }
                        }
                    }
                    else {
                        address = this.props.address.address;
                    }
                }
                else {
                    address = this.props.address.address;
                }
                for (let i = (this.props.page - 1) * 5; i < address.length && i < this.props.page * 5; ++ i) {
                    if (this.props.address.defaultAddress === address[i].id) {
                        list.push(<Addressbox key={address[i].id} click={this.props.click} item={address[i]} defaultAddress={true}/>);
                    }
                    else {
                        list.push(<Addressbox key={address[i].id} click={this.props.click} item={address[i]} defaultAddress={false}/>);
                    }
                }
                return list;
            }
        }else if(this.props.num==0){
            return(
                <div className="AddressListNone">
                    <p>
                        <img src={config.theme_path + 'noaddress.png'} alt=""/>
                        <span>{I18n.t('Addresspage.noadderss')}</span>
                    </p>
                </div>
            )
        }
    }
    render(){
        return (
            <div className="AddressBox">
                {this.renderList()}
            </div>
        )
    }
}
