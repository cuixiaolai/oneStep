import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import { Steps, Button,Form ,Input,message} from 'antd';
import SelectJob from '../SelectJob.jsx';
import config from '../../config.json';
const FormItem = Form.Item;
const createForm = Form.create;

let AnthenticationComone = React.createClass({
    getInitialState() {
        return {
            job: '',
        };
    },
    HandleSubmit(e) {
        e.preventDefault();
        console.log(this.state.job);
        if(this.state.job == '请选择'){
            message.error('请选择公司性质！');
        }else{
            this.props.form.validateFields((errors, values) => {
                if (errors) {
                    console.log('Errors in form!!!');
                    return;
                }else{
                    // Meteor.call('buyer.addCompanyAuthentication', values.comname, values.address, this.state.job, values.name, values.peoId, function(err) {
                    //     if (err) {
                    //         console.log(err);
                    //     }
                    //     else {
                    //         this.props.next();
                    //     }
                    // }.bind(this));
                }
            });
        }
    },
    comNameExists(rule, value, callback) {
        if (!value) {
            callback();
        } else {
            // setTimeout(() => {
            //     Meteor.call('user.checkComName', value, (err) => {
            //         if (err) {
            //             callback([new Error(I18n.t('Anthentication.comnameerror1'))]);
            //         } else {
            //             callback();
            //         }
            //     });
            // }, 800);
        }
    },
    industry(value) {
        this.setState({job: value});
    },
    userPhoneExists(rule, value, callback) {
        let react = /^\d{17}([0-9]|x|X)$/ ;
        if(!react.test(value)){
            callback(I18n.t('Anthentication.peoIderror'));
        }else{
            callback();
        }
    },
    render(){
        const { getFieldProps } = this.props.form;
        const comnameProps = getFieldProps('comname', {
            initialValue: this.props.company_name,
            validate: [{
                rules: [
                    { required: true, min: 4,max:20,message:I18n.t('Anthentication.comnameerror')},
                    { validator: this.comNameExists },
                ],
                trigger: 'onBlur'
            }],
        });
        const addressProps = getFieldProps('address', {
            validate: [{
                rules: [
                    { required: true , min:8 , message:I18n.t('Anthentication.addresserror')},
                ],
                trigger: 'onBlur'
            }],
        });
        const nameProps = getFieldProps('name', {
            validate: [{
                rules: [
                    { required: true, min: 2,max: 5,message:I18n.t('Anthentication.nameerror')},
                ],
                trigger: 'onBlur'
            }],
        });
        const peoIdProps = getFieldProps('peoId', {
            validate: [{
                rules: [
                    { validator: this.userPhoneExists },
                ],
                trigger: 'onBlur'
            }],
        });

        const formItemLayout = {
            labelCol: { span: 7 },
        };
        return(
            <Form form={this.props.form} inline className="AnthenticationComone">
                <FormItem
                    hasFeedback
                    label={I18n.t('Anthentication.comname')}
                >
                    <Input  type="text" {...comnameProps} />
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label={I18n.t('Anthentication.address')}
                >
                    <Input minLength="16" maxLength="50"  type="textarea" {...addressProps} />
                </FormItem>
                <FormItem
                    required
                    label={I18n.t('Anthentication.company')}
                >
                    <SelectJob style={{width:400,height:40}} industry={this.industry}/>
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('Anthentication.name')}
                >
                    <Input  type="text" {...nameProps} />
                </FormItem>
                <FormItem
                    hasFeedback
                    required
                    label={I18n.t('Anthentication.peoId')}
                >
                    <Input  type="text" {...peoIdProps} />
                </FormItem>
                <FormItem>
                    <Button type="primary" onClick={this.HandleSubmit}>{I18n.t('forgetpassword.next')}</Button>
                </FormItem>
            </Form>
        )
    }
})
AnthenticationComone = createForm()(AnthenticationComone);
export default AnthenticationComone;