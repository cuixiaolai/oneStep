import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
import {  Button } from 'antd';
import PersonalCenterSecurityAuthenticationForm from './PersonalCenterSecurityAuthenticationForm.jsx';

export default class PersonalAuthentication extends Component {
    click(){
        $.ajax({
            type: 'POST',
            url: config.file_server + 'delete_identity',
            data: { path: this.props.currentUserInfo.authentication.front, path1: this.props.currentUserInfo.authentication.back },
            dataType: "json",
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
        // Meteor.call('buyer.deleteIdentityAuthentication');
    }
    show() {
        // if(Meteor.userId() != null && this.props.currentUserInfo!=null){
        if( this.props.currentUserInfo!=null){
            if (this.props.currentUserInfo != null) {
                if(this.props.currentUserInfo.authentication == null) { //提交审核
                    return(
                        <div>
                            <p className="SecurityTopName" style={{marginBottom:32}}><span></span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.name')}
                            </p>
                            <p style={{fontSize:14,color:'#707070',marginLeft:42}}>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.words')}</p>
                            <PersonalCenterSecurityAuthenticationForm />
                        </div>
                    )
                }
                else if(this.props.currentUserInfo.authentication.verify == false) { //审核中
                    return(
                        <div>
                            <p className="SecurityTopName" style={{marginBottom:32}}><span></span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.name')}
                            </p>
                            <div className="Securitysuccess">
                                <div className="Securitysuccess-top">
                                    <p>
                                        <div className="left">
                                            <img src={config.theme_path + 'watch.png'} alt=""/>
                                        </div>
                                        {I18n.t('PersonalCenterSecurity.SecurityAuthentication.doing')}
                                        <div className="clear"></div>
                                    </p>
                                    <p>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.successwords')}</p>
                                </div>
                                <p>
                                    {I18n.t('PersonalCenterSecurity.SecurityAuthentication.message')}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.names')}</span>
                                    {this.props.currentUserInfo.authentication.name}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.id')}</span>
                                    {this.props.currentUserInfo.authentication.id}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.effective')}</span>
                                    {this.props.currentUserInfo.authentication.begin}——{this.props.currentUserInfo.authentication.end}
                                </p>
                            </div>
                        </div>
                    )
                }
                else if(this.props.currentUserInfo.authentication.success == true){ //审核成功
                    let date = this.props.currentUserInfo.authentication.date;
                    let month = date.getMonth() + 1;
                    let strDate = date.getDate();
                    let hour = date.getHours();
                    let minute = date.getMinutes();
                    let seconds = date.getSeconds();
                    return(
                        <div>
                            <p className="SecurityTopName" style={{marginBottom:32}}><span></span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.name')}
                            </p>
                            <div className="Securitysuccess">
                                <div className="Securitysuccess-top">
                                    <img src={config.theme_path + 'origin3.png'} alt=""/>
                                    <p>
                                        <div className="left">
                                            <img src={config.theme_path + 'true.png'} alt=""/>
                                        </div>
                                        {I18n.t('PersonalCenterSecurity.SecurityAuthentication.pass')}
                                        <div className="clear"></div>
                                    </p>
                                    <p>
                                        {I18n.t('PersonalCenterSecurity.SecurityAuthentication.Congratulations')}

                                    </p>
                                </div>
                                <p>
                                    {I18n.t('PersonalCenterSecurity.SecurityAuthentication.message')}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.time')}</span>
                                    {date.getFullYear()+'-'+month+'-'+strDate+' '+hour+':'+minute+':'+seconds}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.names')}</span>
                                    {this.props.currentUserInfo.authentication.name}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.id')}</span>
                                    {this.props.currentUserInfo.authentication.id}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.effective')}</span>
                                    {this.props.currentUserInfo.authentication.begin}——{this.props.currentUserInfo.authentication.end}
                                </p>
                            </div>
                        </div>
                    )
                }
                else{ //审核失败
                    let date = this.props.currentUserInfo.authentication.date;
                    let month = date.getMonth() + 1;
                    let strDate = date.getDate();
                    let hour = date.getHours();
                    let minute = date.getMinutes();
                    let seconds = date.getSeconds();
                    return(
                        <div>
                            <p className="SecurityTopName" style={{marginBottom:32}}><span></span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.name')}
                            </p>
                            <div className="Securitysuccess">
                                <div className="Securitysuccess-top">
                                    <p>
                                        <div className="left">
                                            <img src={config.theme_path + 'rederror.png'} alt="" style={{width:24}}/>
                                        </div>
                                        {I18n.t('PersonalCenterSecurity.SecurityAuthentication.nopass')}
                                        <div className="clear"></div>
                                    </p>
                                    <p>
                                        {I18n.t('CompantInvoics.careful')}
                                        <span onClick={this.click.bind(this)} style={{color:"#0c5aa2",curson:'pointer'}}>{I18n.t('CompantInvoics.again')}</span>
                                    </p>
                                </div>
                                <p>
                                    {I18n.t('PersonalCenterSecurity.SecurityAuthentication.message')}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.time')}</span>
                                    {date.getFullYear()+'-'+month+'-'+strDate+' '+hour+':'+minute+':'+seconds}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.names')}</span>
                                    {this.props.currentUserInfo.authentication.name}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.id')}</span>
                                    {this.props.currentUserInfo.authentication.id}
                                </p>
                                <p>
                                    <span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.effective')}</span>
                                    {this.props.currentUserInfo.authentication.begin}——{this.props.currentUserInfo.authentication.end}
                                </p>
                            </div>
                        </div>
                    )
                }
            }
        }
    }
    render() {
        return(
            <div>
                {this.show()}
            </div>
        )
    }
}