import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import { DatePicker, Form, Steps, Button, Input, Cascader, Upload, Icon, Modal ,message,Checkbox } from 'antd';
import config from '../../config.json';
import { Link } from 'react-router';
const FormItem = Form.Item;
const createForm = Form.create;

import Agreement from '../../ui/Agreement.jsx';

let CompantInvoicsOne = React.createClass({
    getInitialState() {
        return {
            checked:false,
            flag:false,
        };
    },
    onChange(e) {
        let checked=`${e.target.checked}`;
        console.log(`${e.target.checked}`);
        this.setState({checked:checked})
    },
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }else{
                console.log(this.state.checked);
                if(this.state.checked=='true'){
                    Meteor.call('buyer.addInvoice', values.comname, values.address, values.phone, values.code, values.bandname, values.bandnum, function(err, data){
                        if (err) {
                            console.log(err);
                        }
                        else {
                            this.props.next();
                        }
                    }.bind(this));
                }else{
                    message.warning('请查看并勾选发票须知');
                }
            }
        });
    },
    onClick(){
        this.setState({flag:!this.state.flag});  
    },
    Cloce(){
        this.setState({flag:false,checked:'true'});
    },
    userPhoneExists(rule, value, callback) {

        let react = /^1[34578]{1}\d{9}$/ ;
        if(!react.test(value)){
            callback(I18n.t('CompantInvoicsOne.phoneerror'));
        }else{
            callback();
        }
    },
    usercodeProps(rule, value, callback){
        let react = /^([a-zA-Z0-9]{15}|[a-zA-Z0-9]{18})$/ ;
        if(!react.test(value)){
            callback(I18n.t('CompantInvoicsOne.coderror'));
        }else{
            callback();
        }
    },
    userbandnumProps(rule, value, callback){
        let react = /^\d{12,18}$/ ;
        if(!react.test(value)){
            callback(I18n.t('CompantInvoicsOne.bandnumerror'));
        }else{
            callback();
        }
    },
    render(){
        const { getFieldProps ,getFieldError, isFieldValidating } = this.props.form;

        const comnameProps = getFieldProps('comname', {
            validate: [{
                rules: [
                    { required: true, min: 4,max:20, message: I18n.t('CompantInvoicsOne.comserror') },
                ],
                trigger: 'onBlur'
            }],
        });
        const addressProps = getFieldProps('address',{
            validate: [{
                rules:[
                    {required: true, min:4, message: I18n.t('CompantInvoicsOne.comerror')}
                ],
                trigger: 'onBlur'
            }],
        });
        const phoneProps = getFieldProps('phone',{
            validate: [{
                rules:[
                    { validator: this.userPhoneExists },
                ],
                trigger: 'onBlur'
            }],
        });
        const codeProps = getFieldProps('code',{
            validate: [{
                rules:[
                    { validator: this.usercodeProps }
                ],
                trigger: 'onBlur'
            }],
        });
        const bandnameProps = getFieldProps('bandname',{
            validate: [{
                rules:[
                    {required: true, min: 4,max:20, message: I18n.t('CompantInvoicsOne.bandnameerroe')}
                ],
                trigger: 'onBlur'
            }],
        });
        const bandnumProps = getFieldProps('bandnum',{
            validate: [{
                rules:[
                    { validator: this.userbandnumProps }
                ],
                trigger: 'onBlur'
            }],
        });
        const formItemLayout = {
            labelCol: { span: 7 },
        };
        return(
            <Form form={this.props.form} inline className="Compantinvoics-form" >
                <FormItem
                    hasFeedback
                    label={I18n.t('CompantInvoicsOne.comname')}
                >
                    <Input  maxLength={80} minLength={8}  type="text" {...comnameProps}  placeholder={I18n.t('CompantInvoicsOne.compl')}  />
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('CompantInvoicsOne.address')}
                    {...formItemLayout}
                >
                    <Input maxLength='40'  style={{resize:'none',width:310,height:100,lineHeight:1.4}}  type="textarea" {...addressProps} placeholder={I18n.t('CompantInvoicsOne.addpl')} />
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('CompantInvoicsOne.phone')}
                >
                    <Input  type="text" {...phoneProps} placeholder={I18n.t('CompantInvoicsOne.phonepl')} />
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('CompantInvoicsOne.code')}
                >
                    <Input  type="text" {...codeProps} placeholder={I18n.t('CompantInvoicsOne.codpl')} />
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('CompantInvoicsOne.bandname')}
                >
                    <Input  type="text" {...bandnameProps} placeholder={I18n.t('CompantInvoicsOne.bandnamepl')} />
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('CompantInvoicsOne.bandnum')}
                >
                    <Input  type="text" {...bandnumProps} placeholder={I18n.t('CompantInvoicsOne.bandnumpl')} />
                </FormItem>
                <div className="Compantinvoics-form-read">
                    <Checkbox onChange={this.onChange} checked={this.state.checked}></Checkbox>
                    <span>{I18n.t('CompantInvoicsOne.read')}</span>
                    <span style={{cursor:'pointer',color:'#0c5aa2'}} onClick={this.onClick}>{I18n.t('CompantInvoicsOne.message')}</span>
                </div>
                <FormItem>
                    <Button type="primary" onClick={this.handleSubmit}>{I18n.t('forgetpassword.next')}</Button>
                </FormItem>
                <Agreement  click={this.Cloce}  flag={this.state.flag}   path="invoice_notice"  />
            </Form>
        )
    }
})
CompantInvoicsOne = createForm()(CompantInvoicsOne);
export default CompantInvoicsOne;
