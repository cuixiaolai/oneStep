import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

import { Select, DatePicker, Modal, Button ,Input ,Menu, Dropdown, Icon,Form ,Tabs, Col, Table } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
/*卖家中心-库存管理-全部*/


const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const InputGroup = Input.Group;
const columns = [{
    title: '型号',
    dataIndex: 'model',
    key: 'model',

}, {
    title: '厂商',
    dataIndex: 'manufacturer',
    key: 'manufacturer',

}, {
    title: '供方标准包装',
    dataIndex: 'packing',
    key: 'packing',

}, {
    title: '递增数量',
    dataIndex: 'number',
    key: 'number',

}, {
    title: '最小订货量',
    dataIndex: 'minNum',
    key: 'minNum',

}, {
    title: '交易货币',
    dataIndex: 'change',
    key: 'change',

}, {
    title: '价格梯度',
    dataIndex: 'price',
    key: 'price',

}, {
    title: '库存总量',
    dataIndex: 'totalInventory',
    key: 'totalInventory',

}, {
    title: '库存所在地',
    dataIndex: 'stockLocation',
    key: 'stockLocation',

}, {
    title: '批号',
    dataIndex: 'num',
    key: 'num',

}, {
    title: '产地',
    dataIndex: 'place',
    key: 'place',

}, {
    title: '是否包邮',
    dataIndex: 'age8',
    key: 'age8',

}, {
    title: '操作',
    dataIndex: 'age9',
    key: 'age9',

}

];

const data = [];
for (let i = 0; i < 10; i++) {
    data.push({
        key: i,
        model: `Edward King ${i}`,
        manufacturer: 32,
        packing: `London, Park Lane no. ${i}`,
    });
}

const rowSelection = {
    onChange(selectedRowKeys, selectedRows) {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect(record, selected, selectedRows) {
        console.log(record, selected, selectedRows);
    },
    onSelectAll(selected, selectedRows, changeRows) {
        console.log(selected, selectedRows, changeRows);
    },
};

function callback(key) {
    console.log(key);
}

export default class ManageStock_show extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            stocktab: true,

        });
    }
    clickStockTab() {
        console.log(this.props.aria-selected);
    }
    render(){

        return (

           <div>
               <div className="filterCondition">
                   {/*全部*/}

                   <Form inline >
                       <p>{I18n.t('PersonalCenter.manageStock.filterCondition')}</p>
                       <FormItem
                           id="control-input"
                           label={I18n.t('PersonalCenter.manageStock.model')}
                           labelCol={{ span: 7 }}
                           wrapperCol={{ span: 14 }}
                       >

                           <Input id="control-input"  style={{ width: 100 }} />

                       </FormItem>
                       <FormItem
                           id="select"
                           label={I18n.t('PersonalCenter.manageStock.manufacturer')}
                           labelCol={{ span: 7 }}
                           wrapperCol={{ span: 14 }}
                       >
                           <Select id="select" size="large" defaultValue="lucy" style={{ width: 100 }}>
                               <Option value="jack">jack</Option>
                               <Option value="lucy">lucy</Option>
                               <Option value="disabled" disabled>disabled</Option>
                               <Option value="yiminghe">yiminghe</Option>
                           </Select>
                       </FormItem>
                       <FormItem
                           id="select"
                           label={I18n.t('PersonalCenter.manageStock.freeShipping')}
                           labelCol={{ span: 7 }}
                           wrapperCol={{ span: 14 }}
                       >
                           <Select id="select" size="large" defaultValue="lucy" style={{ width: 100 }} >
                               <Option value="jack">jack</Option>
                               <Option value="lucy">lucy</Option>
                               <Option value="disabled" disabled>disabled</Option>
                               <Option value="yiminghe">yiminghe</Option>
                           </Select>
                       </FormItem>
                       <FormItem
                           id="select"
                           label={I18n.t('PersonalCenter.manageStock.packingMethod')}
                           labelCol={{ span: 10 }}
                           wrapperCol={{ span: 14 }}
                       >
                           <Select id="select" size="large" defaultValue="lucy" style={{ width: 100 }} >
                               <Option value="jack">jack</Option>
                               <Option value="lucy">lucy</Option>
                               <Option value="disabled" disabled>disabled</Option>
                               <Option value="yiminghe">yiminghe</Option>
                           </Select>
                       </FormItem>

                   </Form>
               </div>
               <div className="stockFilter">
                   <Form inline >
                       <p>{I18n.t('PersonalCenter.manageStock.stockFilter')}</p>

                       <FormItem
                           label={I18n.t('PersonalCenter.manageStock.totalInventory')}
                           labelCol={{ span: 5 }}
                           wrapperCol={{ span: 16 }}
                       >
                           <InputGroup>
                               <Col span="6">
                                   <Input id=""  />
                               </Col>
                               <Col span="2">
                                   <p className="ant-form-split">—</p>
                               </Col>
                               <Col span="6">
                                   <Input id="" />
                               </Col>

                           </InputGroup>
                       </FormItem>
                       <FormItem
                           id="select"
                           label={I18n.t('PersonalCenter.manageStock.stockLocation')}
                           labelCol={{ span: 10 }}
                           wrapperCol={{ span: 6 }}
                       >
                           <Select id="select" size="large" defaultValue="lucy" style={{ width: 118 }} >
                               <Option value="jack">jack</Option>
                               <Option value="lucy">lucy</Option>
                               <Option value="disabled" disabled>disabled</Option>
                               <Option value="yiminghe">yiminghe</Option>
                           </Select>
                       </FormItem>

                   </Form>
               </div>
               <div className="stockTable">
                   <p>{I18n.t('PersonalCenter.manageStock.select')}</p>
                   <Button>{I18n.t('PersonalCenter.manageStock.on')}</Button>
                   <Button>{I18n.t('PersonalCenter.manageStock.off')}</Button>
                   <a className="a1" href="">{I18n.t('PersonalCenter.manageStock.deleteSelect')}</a>
                   <Table columns={columns} rowSelection={rowSelection} dataSource={data} scroll={{x: 1300  }}/>
               </div>
           </div>




);}}