import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
import {  Button } from 'antd';
import { Link, browserHistory } from 'react-router';

export default class CompanyAuthentication extends Component {
    componentWillMount() {
        if (this.props.currentUserInfo != null) {
            if (this.props.currentUserInfo.authentication == null || this.props.currentUserInfo.authentication.verify == null) {
                browserHistory.replace('/personal_center/new_authentication');
            }
        }
    }
    click() {
        $.ajax({
            type: 'POST',
            url: config.file_server + 'delete_com_identity',
            data: { path: this.props.currentUserInfo.authentication.front, path1: this.props.currentUserInfo.authentication.end, path2: this.props.currentUserInfo.authentication.license },
            dataType: "json",
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
        // Meteor.call('buyer.deleteIdentityAuthentication');
    }
    show() {
        // if(Meteor.userId() != null && this.props.currentUserInfo!=null){
        if( this.props.currentUserInfo!=null){
            if (this.props.currentUserInfo.authentication == null || this.props.currentUserInfo.authentication.verify == null) { //提交审核
                browserHistory.replace('/personal_center/new_authentication');
                return <p className="SecurityTopName" style={{marginBottom:32}}><span></span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.name')}</p>;
            }
            else if(this.props.currentUserInfo.authentication.verify == false){ //审核中
                return(
                    <div>
                        <p className="SecurityTopName" style={{marginBottom:32}}><span></span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.name')}
                        </p>
                        <div className="Securitysuccess">
                            <div className="Securitysuccess-top">
                                <p>
                                    <div className="left">
                                        <img src={config.theme_path + 'watch.png'} alt=""/>
                                    </div>
                                    {I18n.t('PersonalCenterSecurity.SecurityAuthentication.doing')}
                                    <div className="clear"></div>
                                </p>
                                <p>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.successwords')}</p>
                            </div>
                            <p>
                                {I18n.t('PersonalCenterSecurity.SecurityAuthentication.message')}
                            </p>
                            <p>
                                <span>{I18n.t('Anthentication.comname')}：</span>
                                {this.props.currentUserInfo.authentication.company}
                            </p>
                            <p>
                                <span className="left">{I18n.t('Anthentication.address')}：</span>
                                <span className="left" style={{maxWidth:464,lineHeight:'1.3',display:'inline-block'}}>{this.props.currentUserInfo.authentication.address}</span>
                                <p className="clear"></p>
                            </p>
                            <p>
                                <span>{I18n.t('Anthentication.company')}：</span>
                                {this.props.currentUserInfo.authentication.industry}
                            </p>
                            <p>
                                <span>{I18n.t('Anthentication.name')}：</span>
                                {this.props.currentUserInfo.authentication.name}
                            </p>
                        </div>
                    </div>
                )
            }
            else if(this.props.currentUserInfo.authentication.success == true){ //审核成功
                return(
                    <div>
                        <p className="SecurityTopName" style={{marginBottom:32}}><span></span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.name')}
                        </p>
                        <div className="Securitysuccess">
                            <div className="Securitysuccess-top">
                                <img src={config.theme_path + 'origin3.png'} alt=""/>
                                <p>
                                    <div className="left">
                                        <img src={config.theme_path + 'true.png'} alt=""/>
                                    </div>
                                    {I18n.t('PersonalCenterSecurity.SecurityAuthentication.pass')}
                                    <div className="clear"></div>
                                </p>
                                <p>
                                    {I18n.t('PersonalCenterSecurity.SecurityAuthentication.Congratulations')}

                                </p>
                            </div>
                            <p>
                                {I18n.t('PersonalCenterSecurity.SecurityAuthentication.message')}
                            </p>
                            <p>
                                <span>{I18n.t('Anthentication.comname')}：</span>
                                {this.props.currentUserInfo.authentication.company}
                            </p>
                            <p>
                                <span className="left">{I18n.t('Anthentication.address')}：</span>
                                <span className="left" style={{maxWidth:464,lineHeight:'1.3',display:'inline-block'}}>{this.props.currentUserInfo.authentication.address}</span>
                                <p className="clear"></p>
                            </p>
                            <p>
                                <span>{I18n.t('Anthentication.company')}：</span>
                                {this.props.currentUserInfo.authentication.industry}
                            </p>
                            <p>
                                <span>{I18n.t('Anthentication.name')}：</span>
                                {this.props.currentUserInfo.authentication.name}
                            </p>
                        </div>
                    </div>
                )
            }
            else{ //审核失败
                return(
                    <div>
                        <p className="SecurityTopName" style={{marginBottom:32}}><span></span>{I18n.t('PersonalCenterSecurity.SecurityAuthentication.name')}
                        </p>
                        <div className="Securitysuccess">
                            <div className="Securitysuccess-top">
                                <p>
                                    <div className="left">
                                        <img src={config.theme_path + 'rederror.png'} alt="" style={{width:24}}/>
                                    </div>
                                    {I18n.t('PersonalCenterSecurity.SecurityAuthentication.nopass')}
                                    <div className="clear"></div>
                                </p>
                                <p>
                                    {I18n.t('CompantInvoics.careful')}
                                    <span onClick={this.click.bind(this)} style={{color:"#0c5aa2",cursor:'pointer'}}>{I18n.t('CompantInvoics.again')}</span>
                                </p>
                            </div>
                            <p>
                                {I18n.t('PersonalCenterSecurity.SecurityAuthentication.message')}
                            </p>
                            <p>
                                <span>{I18n.t('Anthentication.comname')}：</span>
                                {this.props.currentUserInfo.authentication.company}
                            </p>
                            <p>
                                <span className="left">{I18n.t('Anthentication.address')}：</span>
                                <span className="left" style={{maxWidth:464,lineHeight:'1.3',display:'inline-block'}}>{this.props.currentUserInfo.authentication.address}</span>
                                <p className="clear"></p>
                            </p>
                            <p>
                                <span>{I18n.t('Anthentication.company')}：</span>
                                {this.props.currentUserInfo.authentication.industry}
                            </p>
                            <p>
                                <span>{I18n.t('Anthentication.name')}：</span>
                                {this.props.currentUserInfo.authentication.name}
                            </p>
                        </div>
                    </div>
                )
            }
        }
    }
    render() {
        return(
            <div>
                {this.show()}
            </div>
        )
    }
}
