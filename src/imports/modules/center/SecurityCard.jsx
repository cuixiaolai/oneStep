import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router';
// import { createContainer } from 'meteor/react-meteor-data';
import config from '../../config.json'
import { Translate, I18n } from 'react-redux-i18n';

export default class SecurityCard extends Component{
    constructor(props) {
        super(props);
    }
    safe(){
        if(this.props.size==0){
            return(
                I18n.t('PersonalCenterSecurity.top.very')
            )
        }else if(this.props.size==1){
            return(
                I18n.t('PersonalCenterSecurity.top.height')
            )
        }else if(this.props.size==2){
            return(
                I18n.t('PersonalCenterSecurity.top.middle')
            )
        }else if(this.props.size==3){
            return(
                I18n.t('PersonalCenterSecurity.top.low')
            )
        }
    }
    security(){
        if(this.props.size==0){
            return(
                <div className="PersonalCenterSecurity-colorinner" style={{width:'100%',background:'#4ab744'}}></div>
            )
        }
        else if(this.props.size==1){
            return(
                <div className="PersonalCenterSecurity-colorinner" style={{width:'80%',background:'#4ab744'}}></div>
            )
        }
        else if(this.props.size==2){
            return(
                <div className="PersonalCenterSecurity-colorinner" style={{width:'60%',background:'#fcb926'}}></div>
            )
        }
        else if(this.props.size==3){
            return(
                <div className="PersonalCenterSecurity-colorinner" style={{width:'40%',background:'#ed7020'}}></div>
            )
        }
    }
    words(){
        if(this.props.type=="security" && this.props.word.flag == true){
            return(
                <span>
                    {this.props.word.words}
                    <div className="security">
                        {this.security()}
                    </div>
                    {this.safe()}
                </span>
            )
        }else {
            return(
                <span>{this.props.word.words}</span>
            )
        }
    }
    render(){
        return(
            <div className="SecurityCard">
                    <span>
                        <img src={this.props.word.pic} alt="" style={{width:16}}/>
                        {this.props.word.picname}
                    </span>
                <span>{this.props.word.name}</span>
                {this.words()}
                <span className="right">
                        <Link to={this.props.word.link}>
                             {this.props.word.modify}
                        </Link>
                    </span>
                <div className="clear"></div>
            </div>
        );
    }
}
