import React, { Component } from 'react';
import { Link } from 'react-router';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import { Checkbox,Button,message } from 'antd';
import config from '../../config.json';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Seller } from '../../api/Seller.js';
import Phone from './Phone.jsx';
import Agreement from '../../ui/Agreement.jsx';

export default class PersonalCenterBuyerinner extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            Retailer:this.props.flaga,
            Agent:false,
            AgentList: [],
            Manufacturer:false,
            ManufacturerList: [],
            words:'',
            flag:false,
            word:'',
            flags:false,
            agree:'',
            show:false,
        });
    }
    setAgentList(arr) {
        let agent = [];
        if (arr.length > 10) {
            agent = arr.slice(0, 10);
        }
        else {
            agent = arr;
        }
        console.log(agent);
        this.setState({AgentList: agent});
    }
    setManufacturerList(arr) {
        let manufacturer = [];
        if (arr.length > 10) {
            manufacturer = arr.slice(0, 10);
        }
        else {
            manufacturer = arr;
        }
        console.log(manufacturer);
        this.setState({ManufacturerList: manufacturer});
    }
    click(){
        this.setState({flag:!this.state.flag});
    }
    onclick(){
        this.setState({flags:!this.state.flags});
    }
    Flag(){ //提交资质文件
        if(this.state.agree == true){
            if( (this.state.Agent == true && this.state.AgentList.length==0)||(this.state.Manufacturer == true && this.state.ManufacturerList.length == 0) ){
                message.error('请选择上传图片！');
            }else{
                let agent = [];
                let manufacturer = [];
                for (let i = 0; i < this.state.AgentList.length; ++ i) {
                    $.ajax({
                        type: 'POST',
                        url: config.file_server + 'save_seller',
                        data: { path: this.state.AgentList[i]},
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                    agent.push(this.state.AgentList[i].replace('tmp/', ''));
                }
                for (let i = 0; i < this.state.ManufacturerList.length; ++ i) {
                    $.ajax({
                        type: 'POST',
                        url: config.file_server + 'save_seller',
                        data: { path: this.state.ManufacturerList[i]},
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                    manufacturer.push(this.state.ManufacturerList[i].replace('tmp/', ''));
                }
                // Meteor.call('seller.addAuthentication', agent, manufacturer);
                // Meteor.call('authentication.addSellerAuthentication', Meteor.userId(), agent, manufacturer);
            }

        }else{
            message.warning('请查看并选择卖家开店规则须知！');
        }
    }
    Flags(){
        for (let i = 0; i < this.state.AgentList.length; ++ i) {
            $.ajax({
                type: 'POST',
                url: config.file_server + 'delete_seller',
                data: { path: this.state.AgentList[i] },

                dataType: "json",
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
        for (let i = 0; i < this.state.ManufacturerList.length; ++ i) {
            $.ajax({
                type: 'POST',
                url: config.file_server + 'delete_seller',
                data: { path: this.state.ManufacturerList[i] },

                dataType: "json",
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
        // Meteor.call('authentication.cancelSellerAuthentication', Meteor.userId());
        // Meteor.call('seller.deleteAuthentication');
    }
    Btn(){
        if((this.state.Agent==false && this.state.Manufacturer==false && this.state.Retailer==false) || this.props.flaga==false){
            return(
                <Button disabled type="primary" size="large" onClick={this.Flag.bind(this)}>{I18n.t('PersonalCenter.company.yes')}</Button>
            )
        }else{
            return(
                <Button type="primary" size="large" onClick={this.Flag.bind(this)}>{I18n.t('PersonalCenter.company.yes')}</Button>
            )
        }
    }
    onChange(e){
        console.log(`checked = ${e.target.checked}`,e.target.value);
        if('Retailer'==e.target.value){
            let value=e.target.checked;
            this.setState({ Retailer: value });
        }else if('Agent' == e.target.value){
            let value = e.target.checked;
            this.setState({ Agent: value });
            if (value == false) {
                this.setState({ AgentList: [] });
            }
        }else if('Manufacturer' == e.target.value){
            let value = e.target.checked;
            this.setState({ Manufacturer: value });
            if (value == false) {
                this.setState({ ManufacturerList: [] });
            }
        }else if('agree' == e.target.value){
            let value = e.target.checked;
            this.setState({ agree: value });
        }
    }
    ShowResult(){
        if(this.props.authInfo.authentication.verify == false){//等待审核
            return(
                <p className="PersonalCenterBuyerinner-success">
                    <p>
                        <div className="left">
                            <img src={(config.theme_path + 'bigwatch.png')} alt="" style={{marginRight:18}}/>
                        </div>
                        <p className="left">{I18n.t('buyer.waiting')}</p>
                        <div className="clear"></div>
                    </p>
                    <p>
                        {I18n.t('buyer.please')}
                    </p>
                </p>
            )
        }else if((this.props.authInfo.authentication.verify == true && this.props.authInfo.authentication.success == true)){//审核成功
            return(
                <p className="PersonalCenterBuyerinner-success">
                    <p>
                        <div className="left">
                            <img src={(config.theme_path + 'bigright_03.png')} alt="" style={{width:40,marginRight:18}}/>
                        </div>
                        <p className="left">{I18n.t('buyer.suceed')}</p>
                        <div className="clear"></div>
                    </p>
                    <p>{I18n.t('sellerAudit.youAre')}
                        {(()=>{
                            const seller = [];
                            // if(Roles.userIsInRole(Meteor.userId(), 'agent')) {
                            //     seller.push(<span style={{marginRight: 10}}>{I18n.t('sellerAudit.agent')}</span>);
                            // }
                            // if(Roles.userIsInRole(Meteor.userId(), 'manufacturer')) {
                            //     seller.push(<span style={{marginRight: 10}}>{I18n.t('sellerAudit.manufacturer')}</span>);
                            // }
                            // if(Roles.userIsInRole(Meteor.userId(), 'retailer')) {
                            //     seller.push(<span>{I18n.t('sellerAudit.retailer')}</span>);
                            // }
                            // if(!(Roles.userIsInRole(Meteor.userId(), 'agent') && Roles.userIsInRole(Meteor.userId(), 'manufacturer') && Roles.userIsInRole(Meteor.userId(), 'retailer'))) {
                            //     seller.push(<div style={{marginTop: 10}}><span style={{marginRight: 10}}>{I18n.t('sellerAudit.youCan')}</span><Link to="/seller_center/certification_audit">{I18n.t('sellerAudit.apply')}</Link></div>);
                            // }
                            return seller;
                        })()}
                    </p>
                </p>
            )
        }else if(this.props.authInfo.authentication.verify == true && this.props.authInfo.authentication.success == false){//审核失败
            return(
                <p className="PersonalCenterBuyerinner-success">
                    <p>
                        <div className="left">
                            <img src={(config.theme_path + 'rederror.png')} alt="" style={{width:40,marginRight:18}}/>
                        </div>
                        <p className="left">{I18n.t('buyer.fail')}</p>
                        <div className="clear"></div>
                    </p>
                    <p>
                        {I18n.t('buyer.lost')}
                        <span style={{color:'#0a66bc',cursor:'pointer'}} onClick={this.Flags.bind(this)}>{I18n.t('buyer.again')}</span>
                    </p>
                </p>
            )
        }
    }
    agentPicList() {
        let PIC=[];
        if(this.props.authInfo.authentication.agent.files.length != 0){
            for(let i = 0; i < this.props.authInfo.authentication.agent.files.length; ++ i){
                PIC[i] = (
                    <div>
                        <img src={(config.file_server + this.props.authInfo.authentication.agent.files[i])}/>
                    </div>
                )
            }
        }
        return PIC;
    }
    manufacturerPicList() {
        let PIC=[];
        if(this.props.authInfo.authentication.manufacturer.files.length != 0){
            for(let i = 0; i < this.props.authInfo.authentication.manufacturer.files.length; ++ i){
                PIC[i] = (
                    <div>
                        <img src={(config.file_server + this.props.authInfo.authentication.manufacturer.files[i])}/>
                    </div>
                )
            }
        }
        return PIC;
    }
    waitAgent(){
        if(this.props.authInfo.authentication.agent.verify == false){
            return(
                <span  style={{marginLeft:90}}>
                    <img style={{width:16,marginRight:8,verticalAlign:'middle'}}  src={(config.theme_path + 'watch.png')} alt=""/>
                    <span >{I18n.t('buyer.waiting')}</span>
                </span>
            )
        }
        else if(this.props.authInfo.authentication.agent.verify == true){
            return(
                <span style={{marginLeft:90}}>
                    <img style={{width:16,marginRight:8,verticalAlign:'middle'}}  src={(config.theme_path + 'error_03.png')} alt=""/>
                    <span>{I18n.t('buyer.fail')}</span>
                </span>
            )
        }
    }
    waitManufacturer(){
        console.log(this.props.authInfo.authentication.manufacturer.verify,this.props.authInfo.authentication.agent.verify,this.props.authInfo.authentication);
        if(this.props.authInfo.authentication.manufacturer.verify == false){
            return(
                <span style={{marginLeft:90}}>
                    <img style={{width:16,marginRight:8,verticalAlign:'middle'}}  src={(config.theme_path + 'watch.png')} alt=""/>
                    <span>{I18n.t('buyer.waiting')}</span>
                </span>
            )
        }
        else if(this.props.authInfo.authentication.manufacturer.verify == true){
            return(
                <span style={{marginLeft:90}}>
                    <img style={{width:16,marginRight:8,verticalAlign:'middle'}}  src={(config.theme_path + 'error_03.png')} alt=""/>
                    <span>{I18n.t('buyer.fail')}</span>
                </span>
            )
        }
    }
    ShowPIC(){//等待页面显示资质文件
        if(this.props.authInfo.authentication.agent.files.length != 0 && this.props.authInfo.authentication.manufacturer.files.length == 0){//选择代理商
            return(
                <div style={{marginLeft:140}}>
                    <div style={{marginBottom:20}}>
                        <p className="CompantInvoicsThree-name">
                            <span>1</span>
                            <span>{I18n.t('buyer.daili')}</span>
                                <span onClick={this.click.bind(this)}>
                                    {this.state.words}
                                    <span><img src={(config.theme_path + 'down.png')} alt="" style={this.state.flag ? {transform:'rotate(180deg)'}:{}}/></span>
                                </span>
                            {this.waitAgent()}
                        </p>
                        <div className="CompantInvoicsThree-box" style={this.state.flag ? {display:'block'} : {display:'none'}} >
                            {this.agentPicList()}
                        </div>
                    </div>
                </div>
            )
        }else if(this.props.authInfo.authentication.agent.files.length == 0 && this.props.authInfo.authentication.manufacturer.files.length != 0){//选择厂商
            return(
                <div style={{marginLeft:140}}>
                    <div style={{marginBottom:20}}>
                        <p className="CompantInvoicsThree-name">
                            <span>1</span>
                            <span>{I18n.t('buyer.chang')}</span>
                                    <span onClick={this.onclick.bind(this)}>{this.state.word}
                                        <span>
                                            <img src={(config.theme_path + 'down.png')} alt="" style={this.state.flags ? {transform:'rotate(180deg)'}:{}}/>
                                        </span>
                                    </span>
                            {this.waitManufacturer()}
                        </p>
                        <div className="CompantInvoicsThree-box" style={this.state.flags ? {display:'block'} : {display:'none'}}>
                            {this.manufacturerPicList()}
                        </div>
                    </div>
                </div>
            )
        }else if(this.props.authInfo.authentication.agent.files.length != 0 && this.props.authInfo.authentication.manufacturer.files.length != 0){//厂商与代理商
            return(
                <div style={{marginLeft:140}}>
                    <div style={{marginBottom:20}}>
                        <p className="CompantInvoicsThree-name">
                            <span>1</span>
                            <span>{I18n.t('buyer.daili')}</span>
                                <span onClick={this.click.bind(this)}>
                                    {this.state.words}
                                    <span><img src={(config.theme_path + 'down.png')} alt="" style={this.state.flag ? {transform:'rotate(180deg)'}:{}}/></span>
                                </span>
                            {this.waitAgent()}
                        </p>
                        <div className="CompantInvoicsThree-box" style={this.state.flag ? {display:'block'} : {display:'none'}} >
                            {this.agentPicList()}
                        </div>
                    </div>
                    <div>
                        <p className="CompantInvoicsThree-name">
                            <span>2</span>
                            <span>{I18n.t('buyer.chang')}</span>
                                    <span onClick={this.onclick.bind(this)}>{this.state.word}
                                        <span>
                                            <img src={(config.theme_path + 'down.png')} alt="" style={this.state.flags ? {transform:'rotate(180deg)'}:{}}/>
                                        </span>
                                    </span>
                            {this.waitManufacturer()}
                        </p>
                        <div className="CompantInvoicsThree-box" style={this.state.flags ? {display:'block'} : {display:'none'}}>
                            {this.manufacturerPicList()}
                        </div>
                    </div>
                </div>
            )
        }else{

        }

    }

    SearchPage(){
        console.log(this.props.authInfo);
        if(this.props.authInfo == null) {
            return <div className="PersonalCenterBuyerinner"></div>;
        }
        else if (this.props.authInfo.authentication == null) {
            return(
                <div className="PersonalCenterBuyerinner">
                    {this.Showworm()}
                    <div className="PersonalCenterBuyerinner-one">
                        <p className="left">{I18n.t('buyer.tab')}</p>
                        {this.ShowCheckbox()}
                        <div className="clear"></div>
                    </div>
                    {this.show()}
                    {this.showRetailer()}
                    {this.showAgent()}
                    {this.showManufacturer()}
                    {this.Showagree()}
                    <p style={{textAlign:'center'}} className="buyer-btn">
                        {this.Btn()}
                    </p>
                </div>
            );
        }else{
            return(
                <div>
                    {this.ShowResult()}
                    {this.ShowPIC()}
                </div>
            );
        }
    }
    ShowCheckbox(){
        if(this.props.flaga==false){ //判断是否实名认证
            return(
                <div className="left">
                    <Checkbox disabled onChange={this.onChange.bind(this)} value="Retailer">{I18n.t('buyer.Retailer')}</Checkbox>
                    <Checkbox disabled onChange={this.onChange.bind(this)} value="Agent">{I18n.t('buyer.Agent')}</Checkbox>
                    <Checkbox disabled onChange={this.onChange.bind(this)} value="Manufacturer">{I18n.t('buyer.Manufacturer')}</Checkbox>
                </div>
            )
        }else{
            return(
                <div className="left">
                    <Checkbox onChange={this.onChange.bind(this)} defaultChecked={true} value="Retailer">{I18n.t('buyer.Retailer')}</Checkbox>
                    <Checkbox onChange={this.onChange.bind(this)} value="Agent">{I18n.t('buyer.Agent')}</Checkbox>
                    <Checkbox onChange={this.onChange.bind(this)} value="Manufacturer">{I18n.t('buyer.Manufacturer')}</Checkbox>
                </div>
            )
        }
    }
    Showworm(){
        if(this.props.flaga==false){ //判断是否实名认证
            return(
                <div className="PersonalCenterBuyerinner-worm">
                    <p>
                        <div className="left">
                            <img src={(config.theme_path + 'bigorigin.png')} alt=""/>
                        </div>
                        <p className="left">{I18n.t('buyer.worm')}</p>
                        <div className="clear"></div>
                    </p>
                    <p>{I18n.t('buyer.want')}</p>
                    <p>{I18n.t('buyer.now')} <Link to="/personal_center/authentication">{I18n.t('buyer.true')}</Link> </p>
                </div>
            )
        }else{

        }
    }
    show(){
        if(this.state.Agent==false&&this.state.Manufacturer==false&&this.state.Retailer==false){
            return(
                <div style={{marginBottom:200}}>
                    <p className="left"></p>
                    <div className="left PersonalCenterBuyerinner-null" style={{height:320}}>
                        <p>{I18n.t('buyer.worm')}</p>
                        <p>{I18n.t('buyer.type')}</p>
                    </div>
                    <div className="clear"></div>
                </div>
            )
        }else{

        }
    }
    showAgent(){
        console.log(this.state.Agent);
        if(this.state.Agent==false){

        }else if(this.state.Agent==true){
            return(
                <div>
                    <p className="left">{I18n.t('buyer.Agent')}：</p>
                    <div className="left PersonalCenterBuyerinner-Agent">
                        <div className="left">
                            {I18n.t('buyer.file')}
                        </div>
                        <div className="left">
                            <Phone setList={this.setAgentList.bind(this)} flag={true} style={{width:426,height:'auto'}}/>
                        </div>
                        <div className="clear"></div>
                    </div>
                    <div className="clear"></div>
                </div>
            )
        }
    }
    showManufacturer(){
        if(this.state.Manufacturer==true){
            let photo=document.querySelector('.PersonalCenterBuyer-Picbox');
            console.log(photo);
            return(
                <div>
                    <p className="left">{I18n.t('buyer.Manufacturer')}：</p>
                    <div className="left PersonalCenterBuyerinner-Agent">
                        <div className="left">
                            {I18n.t('buyer.file')}
                        </div>
                        <div className="left">
                            <Phone flag={true} style={{width:426,height:'auto'}} setList={this.setManufacturerList.bind(this)} />
                        </div>
                        <div className="clear"></div>
                    </div>
                    <div className="clear"></div>
                </div>
            )
        }else{

        }
    }
    OnClick(){
        this.setState({show:!this.state.show});
    }
    CloseClick(){
        this.setState({show:false,agree:true});
    }
    Showagree(){
        if(this.state.Agent==true||this.state.Manufacturer==true||this.state.Retailer==true){
            return(
                <p className="PersonalCenterBuyerinner-agree">
                    <Checkbox onChange={this.onChange.bind(this)} checked={this.state.agree} value="agree"></Checkbox>
                    <span style={{marginLeft:10}}>{I18n.t('Register.read')}</span>
                    <span   onClick={this.OnClick.bind(this)}  style={{cursor:'pointer',color:'#0c5aa2'}} >{I18n.t('buyer.Shop')}</span>
                    <Agreement path="seller_shop" flag={this.state.show} click={this.CloseClick.bind(this)} />
                </p>
            )
        }else{

        }
    }
    showRetailer(){
        if(this.state.Retailer==true){
            return(
                <div >
                    <p className="left">{I18n.t('buyer.Retailer')}：</p>
                    <div className="left PersonalCenterBuyerinner-Retailer">
                        <p><span style={{color:'#ed7020'}}>{I18n.t('buyer.worm')}</span>{I18n.t('buyer.go')}</p>
                        <p>{I18n.t('buyer.noneed')}</p>
                    </div>
                    <div className="clear"></div>
                </div>
            )
        }else{

        }
    }
    render(){
        if(this.state.flag==false){
            this.state.words=I18n.t('CompantInvoicsThree.look');
        }else if(this.state.flag==true){
            this.state.words=I18n.t('CompantInvoicsThree.hidden')
        }
        if(this.state.flags==false){
            this.state.word=I18n.t('CompantInvoicsThree.look');
        }else if(this.state.flags==true){
            this.state.word=I18n.t('CompantInvoicsThree.hidden')
        }
        return(
            this.SearchPage()
        )
    }
}

// export default createContainer(() => {
//     Meteor.subscribe('seller');
//     return {
//         authInfo: Seller.findOne({_id: Meteor.userId()}, { fields: {'authentication': 1}}),
//     };
// }, PersonalCenterBuyerinner);