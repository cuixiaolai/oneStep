import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Steps, Button } from 'antd';
import config from '../../config.json';


export default class Securitylevel extends Component{
    show(){
            if(this.props.num==0){
                return(
                    <div className="PersonalCenterSecurity-colorinner" style={{width:'100%',background:'#4ab744'}}></div>
                )
            }
            else if(this.props.num==1){
                return(
                    <div className="PersonalCenterSecurity-colorinner" style={{width:'80%',background:'#4ab744'}}></div>
                )
            }
            else if(this.props.num==2){
                return(
                    <div className="PersonalCenterSecurity-colorinner" style={{width:'60%',background:'#fcb926'}}></div>
                )
            }
            else if(this.props.num==3){
                return(
                    <div className="PersonalCenterSecurity-colorinner" style={{width:'40%',background:'#ed7020'}}></div>
                )
            }
            else if(this.props.num==4){
                return(
                    <div className="PersonalCenterSecurity-colorinner" style={{width:'20%',background:'#d43327'}}></div>
                )
            }
    }
    render(){
        console.log(this.props);
        return(
            <div className="PersonalCenterSecurity-color" style={{width:this.props.width}}>
                {this.show()}
            </div>
        )
    }
}
