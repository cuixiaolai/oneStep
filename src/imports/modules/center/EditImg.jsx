import React, { Component, PropTypes } from 'react';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
import { Upload, message, Button } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';

let EditImg = React.createClass({
    closeClick() {
        this.props.click();
    },
    saveClick() {
        if (this.state.address.match('tmp')) {
            $.ajax({
                type: 'POST',
                url: config.file_server + 'save_icon',
                data: { path: this.state.address, old_path: this.props.icon },
                dataType: "json",
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
            this.props.img(this.state.address.replace('tmp/', ''));
        }
        this.props.click();
    },
    getInitialState() {
        return {
            address: '',
            flag: true,
        };
    },
    imgshow(){
        if (this.state.address != '') {
            return (
                <img src={(config.file_server + this.state.address)} alt="" className="myimg"
                     style={this.state.flag ? {width: "100%"} : {height: "100%"}}/>
            )
        }
        else {
            return (
                <img src={(config.theme_path + 'centering.png')} alt="" className="defaultimg"/>
            )
        }
    },
    handleChange(info) {
        if (info.file.status !== 'uploading') {
            
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace(/public/, '');
            this.setState({address: path});
            let imgsW=document.querySelector('.myimg').naturalWidth;
            let imgsH=document.querySelector('.myimg').naturalHeight;
            if(imgsW>imgsH){
                this.setState({flag:false});
            }else{
                this.setState({flag:true});
            }
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    },
    componentWillMount() {
        if (this.props.icon) {
            this.setState({ address: this.props.icon });
        }
    },
    render(){
        const props = {
            action: config.file_server + 'icon',
            showUploadList: false,
            onChange: this.handleChange,
            beforeUpload(file){
                let isJPG;
                if(file.size <= 4194304){
                    console.log(file.type);
                    if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='image/gif'){
                        isJPG=true;
                    }else{
                        isJPG=false;
                        message.error('您只可以上传格式为 GIF, JPG, BMP, PNG 的图片！');
                    }
                }else{
                    isJPG=false;
                    message.error('超过4m');
                }
                return isJPG;
            }
        };
        return(
            <div>
                <div style={{position: 'fixed',top: 0,left: 0,right: 0,bottom: 0,background: 'rgba(0,0,0,.1)',zIndex: 101}}/>
                <div className="Editaddress">
                    <div className="Editaddress-inner" style={{height:620}}>
                        <p className="Editaddress-name">
                            {I18n.t('EditImg.name')}
                            <img onClick={this.closeClick} src={(config.theme_path + '/close.png')} alt=""/>
                        </p>
                        <div className="UploadImg">
                            <div className="Upload-bigImg left">
                                {this.imgshow()}
                            </div>
                            <div className="Upload-smallImg left">
                                <p>{I18n.t('EditImg.smallimg')}</p>
                                <p>100*100{I18n.t('EditImg.px')}</p>
                                <div className="Upload-smallImg-inner" ref="pic">
                                    {this.imgshow()}
                                </div>
                            </div>
                            <div className="clear"></div>
                            <Upload {...props}>
                                <Button type="ghost">
                                    {I18n.t('EditImg.search')}
                                </Button>
                            </Upload>
                            <p style={{marginTop:12,fontSize:14,color:'#888'}}>{I18n.t('EditImg.friends')}</p>
                            <Button className="Upload-save" style={{marginTop:60}}  type="primary" onClick={this.saveClick}>{I18n.t('EditImg.save')}</Button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
})
export default EditImg;
