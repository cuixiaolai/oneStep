import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Steps, Button } from 'antd';
import config from '../../config.json';
import SecurityPasswordone from './SecurityPasswordone.jsx';
import SecurityPasswordtwo from './SecurityPasswordtwo.jsx';
import SecurityPasswordthree from './SecurityPasswordthree.jsx';
import SecurityPhoneone from './SecurityPhoneone.jsx';
import SecurityEmailone from './SecurityEmailone.jsx';
import CompantInvoicsOne from './CompantInvoicsOne.jsx';
import CompantInvoicsTwo from './CompantInvoicsTwo.jsx';
import CompantInvoicsThree from './CompantInvoicsThree.jsx';
import AnthenticationComone from './AnthenticationComone.jsx';
import AnthenticationComtwo from './AnthenticationComtwo.jsx';
import AnthenticationComthree from './AnthenticationComthree.jsx';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Users } from '../../api/User.js';
// import { Buyer } from '../../api/Buyer.js';
const Step = Steps.Step;
const array = Array.apply(null, [5,6,7]);
const steps = array.map((item,i) => ({
    title: I18n.t('Register.top.one')
}));

export default class SecurityTop extends Component{
    constructor(props) {
        super(props);
        if(this.props.flag==true){
            this.state=({
                current:1,
                style: 152,
                flag: true,
                email: '',
                route:'',
            })
        }else{
            this.state=({
                current:0,
                style: 152,
                flag: true,
                email: '',
                route:'',
            })
        };
    };
    componentWillMount() {
        if (this.props.callback == true) {
            let current = this.state.current + 1;
            let style=this.state.style + 210;
            if (current === steps.length) {
                current = 0;
                style = 152;
            }
            let flag = false;
            this.setState({ current, style, flag });
        }
    };
    next() {
        let current = this.state.current + 1;
        let style=this.state.style + 210;
        if (current === steps.length) {
            current = 0;
            style = 152;
        }
        this.setState({ current,style });
    };
    prev(){
        let current = this.state.current-1;
        let style = this.state.style-210;
        this.setState({current,style});
    };
    show() {
        if (this.props.currentUser != null && this.props.currentUserInfo != null) {
            let person_email = this.props.currentUser.emails == null ? '' : this.props.currentUser.emails[0].address;
            let person_phone = this.props.currentUser.phone == null ? '' : this.props.currentUser.phone;
            if (this.state.route == '/personal_center/password') {
                let info={
                    name: I18n.t("PersonalCenterSecurity.SecurityTopPassword.success"),
                    email: person_email,
                    phone: person_phone,
                    pay: this.props.currentUserInfo.pay,
                    authentication: this.props.currentUserInfo.authentication
                };
                if (this.state.current == 0) {
                    return (
                        <div>
                            <SecurityPasswordone  next={this.next.bind(this)} email={person_email} phone={person_phone} username={this.props.currentUser.username}/>
                        </div>
                    )
                } else if (this.state.current == 1) {
                    return (
                        <div>
                            <SecurityPasswordtwo  next={this.next.bind(this)} flag={this.props.flag} />
                        </div>
                    )
                } else if (this.state.current == 2) {
                    return (
                        <div>
                            <SecurityPasswordthree word={info}/>
                        </div>
                    )
                }
            }
            else if (this.state.route == '/personal_center/phone' || this.state.route == '/personal_center/new_phone') {
                let info={
                    name: I18n.t("PersonalCenterSecurity.SecurityTopPhone.success"),
                    email: person_email,
                    phone: person_phone,
                    pay: this.props.currentUserInfo.pay,
                    authentication: this.props.currentUserInfo.authentication
                };
                if (this.state.current == 0) {
                    return (
                        <div>
                            <SecurityPasswordone  next={this.next.bind(this)} email={person_email} phone={person_phone} username={this.props.currentUser.username}/>
                        </div>
                    )
                } else if (this.state.current == 1) {
                    return (
                        <div>
                            <SecurityPhoneone next={this.next.bind(this)} flag={this.props.flag} />
                        </div>
                    )
                } else if (this.state.current == 2) {
                    return (
                        <div>
                            <SecurityPasswordthree word={info}/>
                        </div>
                    )
                }
            }
            else if (this.state.route == '/personal_center/email' || this.state.route == '/personal_center/new_email') {
                let info={
                    name: I18n.t("PersonalCenterSecurity.SecurityTopEmail.success"),
                    email: person_email,
                    phone: person_phone,
                    pay: this.props.currentUserInfo.pay,
                    authentication: this.props.currentUserInfo.authentication
                };
                if (this.state.current == 0) {
                    return (
                        <div>
                            <SecurityPasswordone  next={this.next.bind(this)} email={person_email} phone={person_phone} username={this.props.currentUser.username}/>
                        </div>
                    )
                } else if (this.state.current == 1) {
                    return (
                        <div>
                            <SecurityEmailone next={this.next.bind(this)} flag={this.props.flag} username={this.props.currentUser.username}/>
                        </div>
                    )
                } else if (this.state.current == 2) {
                    return (
                        <div>
                            <SecurityPasswordthree word={info}/>
                        </div>
                    )
                }
            }
            else if (this.state.route == '/personal_center/pay') {
                if (this.state.current == 0) {
                    return (
                        <div>
                            pay one page
                            <Button type="primary" onClick={this.next.bind(this)}>{I18n.t('forgetpassword.next')}</Button>
                        </div>
                    )
                } else if (this.state.current == 1) {
                    return (
                        <div>
                            pay two page
                            <Button type="primary" onClick={this.next.bind(this)}>{I18n.t('forgetpassword.next')}</Button>
                        </div>
                    )
                } else if (this.state.current == 2) {
                    return (
                        <div>
                            pay three page
                            <Button type="primary" onClick={this.next.bind(this)}>{I18n.t('forgetpassword.next')}</Button>
                        </div>
                    )
                }
            }
            else if (this.state.route=='/personal_center/invoics') {
                if (this.state.current == 0) {
                    return (
                        <div>
                            <CompantInvoicsOne  next={this.next.bind(this)}/>
                        </div>
                    )
                }else if(this.state.current==1){
                    return (
                        <div>
                            <CompantInvoicsTwo next={this.next.bind(this)} prev={this.prev.bind(this)} />
                        </div>
                    )
                }else if(this.state.current==2){
                    return (
                        <div>
                            <CompantInvoicsThree invoice={this.props.invoice}/>
                        </div>
                    )
                }
            }else if (this.state.route=='/personal_center/new_authentication'){
                if (this.state.current == 0) {
                    return (
                        <div>
                            <AnthenticationComone  next={this.next.bind(this)} company_name={this.props.company_name}/>
                        </div>
                    )
                }else if(this.state.current==1){
                    return (
                        <div>
                            <AnthenticationComtwo next={this.next.bind(this)} />
                        </div>
                    )
                }else if(this.state.current==2){
                    return (
                        <div>
                            <AnthenticationComthree />
                        </div>
                    )
                }
            }
        }
    }
    cardshow(){
        if(this.state.route=='/personal_center/invoics'){
            return(
                <div className="Compantinvoics-card">
                    <p>
                        <div className="left">
                            <img src={(config.theme_path + 'origin03.png')} alt=""/>
                        </div>
                        <p className="left">{this.props.words.top}</p>
                        <div className="clear"></div>
                    </p>
                    <p>
                        {this.props.words.word}
                    </p>
                    <p>
                        {this.props.words.will}
                    </p>
                </div>
            )
        }else{

        }
    }
    render(){
        const { current } = this.state;
        let route=window.location.pathname;
        this.state.route=route;
        this.props.current=this.state.current;
        console.log(this.state.route,this.state.current);
        return(
            <div>
                <p className="SecurityTopName"><span></span>{this.props.words.name}</p>
                {this.cardshow()}
                <div className="SecurityTopBar">
                    <div className="perPhoneregister">
                        <Steps current={current} style={this.props.flag ? { display: 'none'}:{display:'block'}}>
                            <Step title={this.props.words.titleone}  key="1" />
                            <Step title={this.props.words.titletwo}  key="2" />
                            <Step title={this.props.words.titlethree}  key="3" />
                            <div className="bigXian"></div>
                            <img src={(config.theme_path + '/center.png')} alt="" ref="picShaw" style={{left:this.state.style+'px',width:54}}  />
                        </Steps>
                        {this.show()}
                    </div>
                </div>
            </div>
        )
    }
}

// export default createContainer(() => {
//     Meteor.subscribe('buyer');
//     Meteor.subscribe('users');
//     return {
//         currentUserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'pay': 1, 'authentication': 1}}),
//         currentUser: Users.findOne({_id: Meteor.userId()}, { fields: {'username': 1, 'emails': 1, 'phone': 1}}),
//     };
// }, SecurityTop);
