import React, { Component, PropTypes } from 'react';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
import { Button, Form, Input,Select,Checkbox,Tabs,Icon,Cascader,message} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
import Selectcity from '../Selectcity.jsx';
import Stocklocation from '../Stocklocation.jsx';
const FormItem = Form.Item;
const createForm = Form.create;
const cityPhone = ['中国 +86','台湾 +886','香港 +852','美国 +1','澳门 +853'];
const Option = Select.Option;

let AddAddress = React.createClass({
    getInitialState() {
        return {
            address: '',
            city:cityPhone[0],
        };
    },
    handleProvinceChange(value) {
        this.setState({
            city:value,
        });
    },
    onChange(value){
        console.log(value);
    },

    handleSubmit(e) {
        e.preventDefault();
        if(this.state.address == '请选择 请选择 请选择'){
            message.error('请选择所在地址！');
        }else{
            this.props.form.validateFields((errors, values) => {
                console.log(values);
                if (errors) {
                    console.log('Errors in form!!!');
                    return;
                }
                else {
                    // Meteor.call('buyer.addAddress', values.name, this.state.address, values.Detailedaddress, this.state.city+' '+values.phone, values.telephone, values.zipcode);
                    this.props.form.resetFields();
                    this.props.click();
                }
            });
        }
    },

    handleChange(value) {
        console.log(`selected ${value}`);
    },

    setAddress(text) {
        this.setState({ address: text });
    },

    closeClick(e) {
        e.preventDefault();
        this.props.form.resetFields();
        this.props.click();
    },
    checkphone(rule,value,callback){
        let react = /^(\d{3,4}-)?\d{7,8}$/;
        if(value != ''){
            if(!react.test(value)){
                callback(I18n.t('EditAddress.phone'));
            }else{
                callback();
            }
        }else{
            callback();
        }
    },
    userPhoneExists(rule, value, callback) {
        let This = this;
        let react = /^1[34578]{1}\d{9}$/;
        if (value != ''){
            if(!react.test(value)){
                callback(I18n.t('Register.phonetishis'));
            }else{
                callback();
            }
        }else{
            callback(I18n.t('Register.nophone'));
        }

    },
    checkemail(rule, value, callback){
        let react =/^[1-9]\d{5}$/;
        if(value != ''){
            if(!react.test(value)){
                callback(I18n.t('EditAddress.zipcode'));
            }else{
                callback();
            }
        }else{
            callback();
        }
    },
    render(){
        const provinceOptions = cityPhone.map(province => <Option key={province}>{province}</Option>);
        const formItemLayout = {
            labelCol: { span: 7 },
        };
        const { getFieldProps ,getFieldError, isFieldValidating } = this.props.form;
        const nameEmailProps = getFieldProps('name', {
            rules: [
                { required: true, min: 1,max:25, message: I18n.t('EditAddress.Addressee') },
            ],
        });
        const DetailedaddressProps=getFieldProps('Detailedaddress',{
            rules: [
                { required: true, min: 1,max:120, message: I18n.t('EditAddress.EditAddress') },
            ],
        })
        let phoneProps;
        switch (this.state.city) {
            case cityPhone[0]:
                phoneProps  = getFieldProps('phone', {
                    validate: [{
                        rules: [
                            { validator: this.userPhoneExists},
                        ],
                        trigger: 'onBlur'
                    }],
                });
                break;
            case cityPhone[1]:
                phoneProps  = getFieldProps('phone', {
                    validate: [{
                        rules: [
                            { required: true, min: 9, max: 9, message: I18n.t('EditAddress.phone') },
                            { validator: this.userPhoneExists},
                        ],
                        trigger: 'onBlur'
                    }],
                });
                break;
            case cityPhone[2]:
                phoneProps  = getFieldProps('phone', {
                    validate: [{
                        rules: [
                            { required: true, min: 8, max: 8, message: I18n.t('EditAddress.phone') },
                            { validator: this.userPhoneExists},
                        ],
                        trigger: 'onBlur'
                    }],
                });
                break;
            case cityPhone[3]:
                phoneProps  = getFieldProps('phone', {
                    validate: [{
                        rules: [
                            { required: true, min: 10, max:10, message: I18n.t('EditAddress.phone') },
                            { validator: this.userPhoneExists},
                        ],
                        trigger: 'onBlur'
                    }],
                });
                break;
            default:
                break;
        }
        const telephoneProps = getFieldProps('telephone', {
            initialValue:'',
            validate: [{
                rules: [
                    { required: false },
                    { validator : this.checkphone},
                ],
                trigger: 'onBlur'
            }],
        });
        const ZipcodeProps = getFieldProps('zipcode', {
            initialValue:'',
            validate: [{
                rules: [
                    { required: false},
                    { validator : this.checkemail},
                ],
                trigger: 'onBlur'
            }],
        });
        return(
            <div className="Editaddress">
                <div className="Editaddress-inner">
                    <p className="Editaddress-name">
                        {I18n.t('EditAddress.new')}
                        <img onClick={this.closeClick} src={(config.theme_path + '/close.png')} alt=""/>
                    </p>
                    <Form form={this.props.form}>
                        <FormItem
                            label={I18n.t('EditAddress.nameTitle')}
                            hasFeedback
                            required
                            {...formItemLayout}
                        >
                            <Input maxLength="25" {...nameEmailProps}/>
                        </FormItem>
                        <FormItem
                            label={I18n.t('EditAddress.addressTitle')}
                            required
                            {...formItemLayout}
                        >
                             <Selectcity func={this.setAddress}/>
                            
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label={I18n.t('EditAddress.detailAddressTitle')}
                            required
                            {...formItemLayout}
                        >
                            <Input className="area" type="textarea"  maxLength="120"  id="textarea" name="textarea" {...DetailedaddressProps} />
                        </FormItem>
                        <FormItem
                            required
                            label={I18n.t('EditAddress.phoneTitle')}
                            hasFeedback
                            {...formItemLayout}
                        >
                            <Select size="large" defaultValue={cityPhone[0]} style={{ width:110,height:30,marginRight:10}} onChange={this.handleProvinceChange}>
                                {provinceOptions}
                            </Select>
                            <Input {...phoneProps}/>
                            <div className="clear"></div>
                        </FormItem>
                        <FormItem
                            label={I18n.t('EditAddress.telTitle')}
                            {...formItemLayout}
                        >
                            <Input {...telephoneProps} />
                        </FormItem>
                        <FormItem
                            label={I18n.t('EditAddress.codeTitle')}
                            {...formItemLayout}
                        >
                            <Input {...ZipcodeProps}/>
                        </FormItem>
                        <FormItem>
                            <Button type="primary" htmlType="submit" onClick={this.handleSubmit}>{I18n.t('EditAddress.commit')}</Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
        )
    }
})
AddAddress = createForm()(AddAddress);
export default AddAddress;
