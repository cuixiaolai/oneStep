import { Modal, Button } from 'antd';
import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';

export default class DialogBox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            loading: false,
            visible: false,
        });
    }
    showModal() {
        this.setState({
            visible: true,
        });
    }

    handleCancel() {
        this.setState({ visible: false });
    }
    render() {
        return (
            <div className={this.props.className}>
                <span onClick={this.showModal.bind(this)} >
                    {this.props.content}
                </span>
                <Modal ref="modal"
                       visible={this.state.visible}
                       title="" onOk={this.props.handleOk} onCancel={this.handleCancel.bind(this)}
                       footer={[
            <Button  key="back" type="ghost" style={{height:32,width:72,marginRight:50,fontSize:14}} onClick={this.handleCancel.bind(this)}>{I18n.t('PersonalCenter.company.no')}</Button>,
            /*在此处加*/
            <Button className="yes" key="submit" type="primary" style={{height:32,width:72,marginRight:140,fontSize:14}} loading={this.state.loading} onClick={this.props.handleOk}>
                {I18n.t('PersonalCenter.company.yes1')}
            </Button>
          ]}
                >
                    <p className="prompt" style={{paddingRight:30,paddingLeft:30,paddingTop:30,fontSize:14}}>{this.props.words}</p>
                </Modal>
            </div>
        );
    }
}