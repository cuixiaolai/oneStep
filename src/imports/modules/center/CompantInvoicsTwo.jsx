import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { DatePicker, Form, Steps, Button,Input,Cascader , Col,Upload, Icon, Modal ,message,Checkbox } from 'antd';
import config from '../../config.json';
const FormItem = Form.Item;
const createForm = Form.create;
const Dragger = Upload.Dragger;

let CompantInvoicsTwo = React.createClass({
    getInitialState() {
        return {
            url:'',
            flag:false,
            urle:'',
            flage:false,
            urlt:'',
            flagt:false,
        };
    },
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }else{
                if(this.state.url!=''&&this.state.urle!=''&&this.state.urlt!=''){
                    $.ajax({
                        type: 'POST',
                        url: config.file_server + 'save_invoice',
                        data: { path: this.state.url, path1: this.state.urle, path2: this.state.urlt },
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                    Meteor.call('buyer.updateInvoice', this.state.url.replace('tmp/', ''), this.state.urle.replace('tmp/', ''), this.state.urlt.replace('tmp/', ''), function(err){
                        if (err) {
                            console.log(err);
                        }
                        else {
                            Meteor.call('authentication.addInvoiceAuthentication', Meteor.userId());
                            this.props.next();
                        }
                    }.bind(this));

                }else{
                }
            }
        });
    },
    handleChange(info) {
        if (info.file.status !== 'uploading') {

        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace('public/', '');
            console.log(path);
            this.setState({url: path});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    },
    handleChangee(info) {
        if (info.file.status !== 'uploading') {

        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace('public/', '');
            console.log(path);
            this.setState({urle: path});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    },
    handleChanget(info) {
        if (info.file.status !== 'uploading') {

        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace('public/', '');
            console.log(path);
            this.setState({urlt: path});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    },
    render(){
        const props = {
            name: 'file',
            showUploadList: false,
            action: config.file_server + 'invoice',
            onChange: this.handleChange,
            beforeUpload(file){

                let isJPG;
                if(file.size <= 4194304){
                    if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='application/pdf'){
                        isJPG=true;
                    }else{
                        isJPG=false;
                            message.error('you can  upload PDF,JPG,PNG,BMP file~');
                    }
                }else{
                    isJPG=false;
                    message.error('超过4m');
                }
                return isJPG;
            },
        };
        const propse = {
            name: 'file',
            showUploadList: false,
            action: config.file_server + 'invoice',
            onChange: this.handleChangee,
            beforeUpload(file){

                let isJPG;
                if(file.size <= 4194304){
                    if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='application/pdf'){
                        isJPG=true;
                    }else{
                        isJPG=false;
                        message.error('you can  upload PDF,JPG,PNG,BMP file~');
                    }
                }else{
                    isJPG=false;
                    message.error('超过4m');
                }
                return isJPG;
            },
        };
        const propst = {
            name: 'file',
            showUploadList: false,
            action: config.file_server + 'invoice',
            onChange: this.handleChanget,
            beforeUpload(file){

                let isJPG;
                if(file.size <= 4194304){
                    if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='application/pdf'){
                        isJPG=true;
                    }else{
                        isJPG=false;
                        message.error('you can  upload PDF,JPG,PNG,BMP file~');
                    }
                }else{
                    isJPG=false;
                    message.error('超过4m');
                }
                return isJPG;
            },
        };
        if(this.state.url==''){
            this.state.flag=false;
        }else if(this.state.url!=''){
            this.state.flag=true;
        }
        return(
            <div className="CompantInvoicsTwo">
                <div className="Anthentication" style={{marginLeft:170,marginTop:60}}>
                    <div style={{marginBottom:30}}>
                        <div className="left" style={{fontSize:14,color:'#707070',marginRight:10}}>{I18n.t('CompantInvoicsThree.Tax')}</div>
                        <div className="Anthentication-upload left">
                            <Dragger {...props}>
                                <Icon type="plus-circle-o" />
                                <div className="ant-upload-text" style={{marginTop:10,color:'#888'}}>{I18n.t('CompantInvoicsThree.upload')}</div>
                            </Dragger>
                            <div className="Anthentication-upload-img" style={this.state.url != '' ? {display:'block'} : {display:'none'}}>
                                <img src={(config.file_server + this.state.url)} alt=""  />
                                <Upload {...props}>
                                    {I18n.t('CompantInvoicsThree.change')}
                                </Upload>
                            </div>
                        </div>
                        <div className="clear"></div>
                    </div>

                    <div style={{marginBottom:30}}>
                        <div className="left" style={{fontSize:14,color:'#707070',marginRight:10}}>{I18n.t('CompantInvoicsThree.Taxpayer')}</div>
                        <div className="Anthentication-upload left">
                            <Dragger {...propse}>
                                <Icon type="plus-circle-o" />
                                <div className="ant-upload-text" style={{marginTop:10,color:'#888'}}>{I18n.t('CompantInvoicsThree.upload')}</div>
                            </Dragger>
                            <div className="Anthentication-upload-img" style={this.state.urle != '' ? {display:'block'} : {display:'none'}}>
                                <img src={(config.file_server + this.state.urle)} alt=""  />
                                <Upload {...propse}>
                                    {I18n.t('CompantInvoicsThree.change')}
                                </Upload>
                            </div>
                        </div>
                        <div className="clear"></div>
                    </div>

                    <div style={{marginBottom:30}}>
                        <div className="left" style={{fontSize:14,color:'#707070',marginRight:10}}>{I18n.t('CompantInvoicsThree.attorney')}</div>
                        <div className="Anthentication-upload left" style={{marginBottom:10}}>
                            <Dragger {...propst}>
                                <Icon type="plus-circle-o" />
                                <div className="ant-upload-text" style={{marginTop:10,color:'#888'}}>{I18n.t('CompantInvoicsThree.upload')}</div>
                            </Dragger>
                            <div className="Anthentication-upload-img" style={this.state.urlt != '' ? {display:'block'} : {display:'none'}}>
                                <img src={(config.file_server + this.state.urlt)} alt=""  />
                                <Upload {...propst}>
                                    {I18n.t('CompantInvoicsThree.change')}
                                </Upload>
                            </div>
                        </div>
                        <div className="clear"></div>
                    </div>
                </div>
                <div className="Compantinvoics-form">
                    <Button type="ghost" onClick={this.props.prev}>{I18n.t('forgetpassword.previous')}</Button>
                    <Button type="primary" onClick={this.handleSubmit}>{I18n.t('forgetpassword.next')}</Button>
                </div>
                <ul>
                    <li>
                        <div className="left">
                            <img src={(config.theme_path + 'blue.png')} alt=""/>
                        </div>
                        <p className="left">
                            <span>{I18n.t('CompantInvoicsTwo.ulone')}</span>
                            <div className="CompantInvoicsTwoSmall">
                                税务登记证，是从事生产、经营的纳税人向生产、经营地或者纳税义务发生地的主管税务机关申报办理税务登记时，所颁发的登记凭证。
                            </div>
                        </p>
                        <div className="clear"></div>
                    </li>
                    <li>
                        <div className="left">
                            <img src={(config.theme_path + 'blue.png')} alt=""/>
                        </div>
                        <p className="left">
                            <span>{I18n.t('CompantInvoicsTwo.ultwo')}</span>
                            <div className="CompantInvoicsTwoSmall">
                                增值税一般纳税人的认定标准(范围)工业企业年应税销售额在100万元以上，商业企业年应税销售额在180万元以上。
                            </div>
                        </p>
                        <div className="clear"></div>
                    </li>
                    <li>
                        <div className="left">
                            <img src={(config.theme_path + 'blue.png')} alt=""/>
                        </div>
                        <p className="left">
                            <span>{I18n.t('CompantInvoicsTwo.ulthree')}</span>
                            <div className="CompantInvoicsTwoSmall">
                                需要委托本平台进行发票开取
                            </div>
                        </p>
                        <div className="clear"></div>
                    </li>
                    <li>
                        <div className="left">
                            <img src={(config.theme_path + 'radius.png')} alt=""/>
                        </div>
                        <p className="left">
                            <p>{I18n.t('CompantInvoicsTwo.ulfour')}</p>
                            <p>{I18n.t('CompantInvoicsTwo.ulfive')}</p>
                        </p>
                        <div className="clear"></div>
                    </li>
                </ul>
            </div>
        )
    }
})
CompantInvoicsTwo = createForm()(CompantInvoicsTwo);
export default CompantInvoicsTwo;
