import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Input, Button, Checkbox, message } from 'antd';
import VerifyCode from '../VerifyCode.jsx';
// import { Meteor } from 'meteor/meteor';
const FormItem = Form.Item;
const createForm = Form.create;
import config from '../../config.json';

let SecurityEmailone = React.createClass({
    getInitialState() {
        return {
            verify_code: this.generateCode(),
            error:I18n.t('forgetpassword.one.error'),
            flag:false,
            phone:this.props.flag,
            send:false,
            sendverify:false,
            num:60,
            sendmessage:I18n.t('sendmessage'),
            sends:false,
            false:false,
        };
    },
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }else{
                if (this.props.flag == true) {
                    // Meteor.call('user.addEmail', values.phone);
                }
                else {
                    // Meteor.call('user.changeEmail', values.phone);
                }
                this.props.next();
            }
        });
    },
    userEmailExists(rule, value, callback) {
        if (!value) {
            callback();
        } else {
            // setTimeout(() => {
            //     Meteor.call('user.checkEmail', value, (err) => {
            //         if (err) {
            //             callback([new Error(I18n.t('Register.sorryEmail'))]);
            //         } else {
            //             callback();
            //         }
            //     });
            // }, 800);
        }
    },
    checkVerifyCode(rule, value, callback) {
        if (value && value.toLowerCase() != this.state.verify_code.toLowerCase()) {
            callback(I18n.t('Register.verifyCodeError'));
        } else {
            callback();
        }
    },
    generateCode() {
        let items = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPRSTUVWXYZ23456789'.split('');
        let randInt = function (start, end) {
            return Math.floor(Math.random() * (end - start)) + start;
        }
        let code = '';
        for (let i = 0; i < 4; ++i) {
            code += items[randInt(0, items.length)];
        }
        return code;
    },
    refresh() {
        this.props.form.resetFields(['yan']);
        this.setState({ verify_code: this.generateCode() });
    },
    click(){
        if(this.state.sends==false){
            this.props.form.validateFields((errors, values) => {
                if (errors) {
                    if (this.props.form.getFieldError('yan') == I18n.t('Register.verifyCodeError')) {
                        this.setState({ verify_code: this.generateCode() });
                    }
                    console.log('Errors in form!!!');
                    return;
                }else{
                    this.interval = setInterval(this.tick, 1000);
                    // Meteor.call('accounts.ResetPasswordEmail', Meteor.userId(), this.props.username, values.phone);
                    message.success(I18n.t('forgetpassword.two.success'),3);
                    this.state.sends=true;
                }
            });
        }
    },
    tick(){
        console.log(this.state.sends);
        if(this.state.sends==true) {
            this.state.num-=1;
            this.setState({sendmessage:this.state.num+I18n.t('sendagain')});
            console.log(this.state.num);
            console.log(this.state.num+I18n.t('sendagain'));
        }
        if(this.state.num==0){
            clearInterval(this.interval);
            this.setState({sendmessage:I18n.t('sendmessage'),num:60,sends:false});
            console.log(this.state.sends);
        }
    },
    render(){
        const { getFieldProps ,getFieldError, isFieldValidating } = this.props.form;
        const phoneProps = getFieldProps('phone', {
            validate: [{
                rules: [
                    { required: true, message: I18n.t('Register.emailTishi') },
                ],
                trigger: 'onBlur',
            }, {
                rules: [
                    { type: 'email', message:  I18n.t('Register.pleaseemailTishi') },
                    { validator: this.userEmailExists},
                ],
                trigger: 'onBlur',
            }],
        });
        const yanProps = getFieldProps('yan', {
            validate: [{
                rules: [
                    { required: true, min: 4,max:4,message:I18n.t('Register.yanTishi')},
                    { validator: this.checkVerifyCode }
                ],
                trigger: 'onBlur'
            }],
        });
        const formItemLayout = {
            labelCol: { span: 4 },
        };
        return(
            <Form  inline style={{marginLeft:106}} className="SecurityPassword" form={this.props.form} >
                <FormItem
                    hasFeedback
                    label={I18n.t('Register.perEmail.email')}
                >
                    <Input {...phoneProps} type="text" placeholder={I18n.t('Register.perEmail.emailPlear')} />
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('Register.perPhone.phonever')}
                    className="SecurutyPasswordone-verifycode"
                >
                    <Input {...yanProps} type="text" placeholder={I18n.t('Register.perPhone.verification')} />
                    <VerifyCode code={this.state.verify_code} />
                    <div style={{position:'absolute',top:38,right:0}}>{I18n.t('kan')} <span style={{color:'#0a66bc',cursor:'pointer'}} onClick={this.refresh}>{I18n.t('shu')}</span> </div>
                    <div className="clear"></div>
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('Register.perEmail.yanemail')}
                >
                    <Input type="text" placeholder={I18n.t('Register.perPhone.verificationmessage')} maxLength='6' ref='verificationmessage' />
                    <div className="send" style={this.state.sends ? {  cursor: 'auto',color:'#999999'} : {  cursor:'pointer',color:'#5b5b5b'}} onClick={this.click}>
                        {this.state.sendmessage}
                    </div>
                    <div className="clear"></div>
                </FormItem>
                <FormItem>
                    <Button type="primary" style={{marginLeft:142}} onClick={this.handleSubmit}>{I18n.t('forgetpassword.next')}</Button>
                </FormItem>
            </Form>
        )
    }
})
SecurityEmailone = createForm()(SecurityEmailone);
export default SecurityEmailone;
