import React, { Component } from 'react';
import { Link } from 'react-router'
import { Translate, I18n } from 'react-redux-i18n';
import { Upload, Icon, Modal,message } from 'antd';
import config from '../../config.json';
import PhotoBox from './PhotoBox.jsx';
import getFileItem from './getFileItem.jsx';

export default class Phone extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            previewVisible: false,
            previewImage: '',
            arr:[],
            arrname:[],
            ARR:[],
            number:'',
            fileList:'',
            red:false,
        });
    }
    handleCancel() {
        this.setState({
            previewVisible: false,
        });
    }
    handleChange(info){
        let i=this.state.arr.length;
        let arr=this.state.arr;
        let arrname=this.state.arrname;
        let ARR=[arr,arrname];
        info.fileList=[];
        if(this.state.arr.length<=10){
            if (info.file.status !== 'uploading'){

            }
            if (info.file.status === 'done'){
                i++;
                if(i<=10){
                    let path = info.file.response[0].path;
                    path=path.replace(/public/, '');
                    arr.push(path);
                    arrname.push(info.file.name);
                }else{
                    message.error(`最多为10张。`);
                }
                this.setState({arr:arr, arrname:arrname, ARR:ARR});
                this.props.setList(arr);
            } else if (info.file.status === 'error'){
                message.error(`${info.file.name} 上传失败。`);
            }
        }else{

        }
    }

    click(i){
        this.state.arr.splice(i,1);
        this.state.arrname.splice(i,1);
        this.state.ARR=[this.state.arr,this.state.arrname];
        this.setState({ARR:this.state.ARR,arr:this.state.arr,arrname:this.state.arrname});
        this.props.setList(this.state.arr);
    }

    Photo(){
        let PIC=[];
        if(this.props.close == false){
            for(let i=0; i<this.state.arr.length; i++){
                PIC[i] = (
                    <div className="PersonalCenterBuyer-Phone">
                        <div className="PersonalCenterBuyer-reddel" key={i} onClick={this.click.bind(this,i)} >
                            <img src={(config.theme_path + 'originerror.png')} alt=""/>
                        </div>
                        <div className="PersonalCenterBuyer-img">
                            <img src={(config.file_server + this.state.arr[i])}/>
                        </div>
                        <p>
                            {this.state.arrname[i]}
                        </p>
                    </div>
                )
            }
        }else{
            for(let i=0;i<this.state.arr.length;i++){
                PIC[i] = (
                    <div className="PersonalCenterBuyer-Phone">
                        <div className="PersonalCenterBuyer-del">
                            <span key={i} onClick={this.click.bind(this,i)}  >+</span>
                        </div>
                        <div className="PersonalCenterBuyer-img">
                            <img src={(config.file_server + this.state.arr[i])}/>
                        </div>
                        <p>
                            {this.state.arrname[i]}
                        </p>
                    </div>
                )
            }
        }
        return PIC;
    }
    ShowRed(){
        if(this.state.arr.length>=10){
            return(
                <span style={{color:'#ed7020'}}>{I18n.t('buyer.more')}</span>
            )

        }else{
            return(
                <span style={{color:'#999999'}}>{I18n.t('buyer.more')}</span>
            )
        }
    }
    Upload(){
        let num=this.state.arr.length;
        let i=0;
        let numarr=[];
        let props = {
            action: config.file_server + 'seller',
            showUploadList: false,
            listType: 'picture-card',
            multiple:true,
            onChange: this.handleChange.bind(this),
            beforeUpload(file){
                num++;
                i++;
                numarr.push(num);
                let isJPG;
                if(num<=10){
                    if(file.size <= 4194304){
                        if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='application/pdf'){
                            isJPG=true;
                        }else{
                            isJPG=false;
                            message.error('您只可以上传格式为 PDF, JPG, BMP, PNG 的图片');
                        }
                    }else{
                        isJPG=false;
                        message.error('超过4m');
                    }
                }else{
                    num = (numarr[0]-1);
                    isJPG=false;
                    message.error(`最多为10张。`);
                }
                return isJPG;
            },
        };
        if(this.state.arr.length>=10){
            return(
                <Upload disabled={true} {...props}>
                    <Icon type="plus-circle-o" />
                    <div className="ant-upload-text">{I18n.t('buyer.add')}</div>
                </Upload>
            )
        }else{
            return(
                <Upload {...props}>
                    <Icon type="plus-circle-o" />
                    <div className="ant-upload-text">{I18n.t('buyer.add')}</div>
                </Upload>
            )
        }
    }
    Showwords(){
        if(this.props.flag==true){
            return(
                <p style={{fontSize:'14px',color:'#999',marginTop:20}}>
                    <span style={{color:'#ed7020'}}>{I18n.t('buyer.zhu')}</span>
                    {this.ShowRed()}
                    <p style={{marginTop:5,marginLeft:28}}>{I18n.t('buyer.style')}</p>
                </p>
            )
        }else{

        }
    }
    componentWillMount() {
        if (this.props.picList != null && this.props.picList.length > 0) {
            let arr = [];
            let arrname = [];
            let ARR=[arr,arrname];
            for (let i = 0; i < this.props.picList.length; ++ i) {
                arr.push(this.props.picList[i]);
                arrname.push(this.props.picList[i].replace('/uploads/seller/', ''));
            }
            this.setState({arr:arr, arrname:arrname, ARR:ARR});
        }
    }
    render(){
        return(
            <div>
                <div className="PersonalCenterBuyer-Picbox" style={this.props.style}>
                    {this.Photo()}
                    {this.Upload()}
                </div>
                {this.Showwords()}
            </div>
        )
    }
}
