import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { DatePicker, Form, Steps, Button,Input,Cascader , Col,Upload, Icon, Modal ,message,Checkbox } from 'antd';
import config from '../../config.json';
const FormItem = Form.Item;
const createForm = Form.create;
const Dragger = Upload.Dragger;


let AnthenticationComtwo = React.createClass({
    getInitialState() {
        return {
            url: '',
            urls: '',
            urlt:'',
            startValue: null,
            startValueStr: '',
            endValue: null,
            endValueStr: '',
            endOpen: false,
            startValues: null,
            startValuesStr: '',
            endValues: null,
            endValuesStr: '',
            endOpens: false,
        };
    },
    click(){
        if(this.state.url!='' && this.state.urls!='' && this.state.urlt!='' && this.state.startValue!=null && this.state.endValue!=null
            && this.state.startValues!=null && this.state.endValues!=null){
            $.ajax({
                type: 'POST',
                url: config.file_server + 'save_com_identity',
                data: { path: this.state.url, path1: this.state.urls, path2: this.state.urlt },
                dataType: "json",
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
            Meteor.call('buyer.updateCompanyAuthentication', this.state.url.replace('tmp/', ''), this.state.urls.replace('tmp/', ''), this.state.startValueStr, this.state.endValueStr,
                this.state.urlt.replace('tmp/', ''), this.state.startValuesStr, this.state.endValuesStr, function(err) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        Meteor.call('authentication.addCompanyAuthentication', Meteor.userId());
                        this.props.next();
                    }
                }.bind(this));

        }else{
            message.error(I18n.t('CompantInvoicsThree.tips'));
        }
    },
    handleChange(info) {
        if (info.file.status !== 'uploading') {

        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace('public/', '');
            this.setState({url: path});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    },
    handleChanges(info) {
        if (info.file.status !== 'uploading') {
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace('public/', '');
            this.setState({urls: path});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    },
    handleChanget(info) {
        if (info.file.status !== 'uploading') {
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} 上传成功。`);
            message.success(`${info.file.name} 上传成功。`);
            let path = info.file.response[0].path;
            path = path.replace('public/', '');
            this.setState({urlt: path});
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} 上传失败。`);
        }
    },
    disabledStartDate(startValue) {
        if (!startValue || !this.state.endValue) {
            return false;
        }
        return startValue.getTime() >= this.state.endValue.getTime();
    },
    disabledEndDate(endValue) {
        if (!endValue || !this.state.startValue) {
            return false;
        }
        return endValue.getTime() <= this.state.startValue.getTime();
    },
    onStartChange(date, dateString) {
        this.setState({
            startValue: date,
            startValueStr: dateString,
        });
    },
    onEndChange(date, dateString) {
        this.setState({
            endValue: date,
            endValueStr: dateString,
        });
    },
    handleStartToggle({ open }) {
        if (!open) {
            this.setState({ endOpen: true });
        }
    },
    handleEndToggle({ open }) {
        this.setState({ endOpen: open });
    },
    disabledStartDates(startValues) {
        if (!startValues || !this.state.endValues) {
            return false;
        }
        return startValues.getTime() >= this.state.endValues.getTime();
    },
    disabledEndDates(endValues) {
        if (!endValues || !this.state.startValues) {
            return false;
        }
        return endValues.getTime() <= this.state.startValues.getTime();
    },
    onStartChanges(date, dateString) {
        this.setState({
            startValues: date,
            startValuesStr: dateString,
        });
    },
    onEndChanges(date, dateString) {
        this.setState({
            endValues: date,
            endValuesStr: dateString,
        });
    },
    handleStartToggles({ open }) {
        if (!open) {
            this.setState({ endOpens: true });
        }
    },
    handleEndToggles({ open }) {
        this.setState({ endOpens: open });
    },

    render(){
        const props = {
            name: 'file',
            showUploadList: false,
            action: config.file_server + 'identity',
            onChange: this.handleChange,
            beforeUpload(file){
                let isJPG;
                if(file.size <= 4194304){
                    console.log(file.type);
                    if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='image/gif'||file.type=='application/pdf'){
                        isJPG=true;
                    }else{
                        isJPG=false;
                        message.error('您只能上传 GIF, JPG, BMP, PNG ,PDF 格式的图片');
                    }
                }else{
                    isJPG=false;
                    message.error('超过4m');
                }
                return isJPG;
            }
        };
        const prop = {
            name: 'file',
            showUploadList: false,
            action: config.file_server + 'identity',
            onChange: this.handleChanges,
            beforeUpload(file){
                let isJPG;
                if(file.size <= 4194304){
                    console.log(file.type);
                    if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='image/gif'||file.type=='application/pdf'){
                        isJPG=true;
                    }else{
                        isJPG=false;
                        message.error('您只能上传 GIF, JPG, BMP, PNG, PDF 格式的图片');
                    }
                }else{
                    isJPG=false;
                    message.error('超过4m');
                }
                return isJPG;
            }
        };
        const propt = {
            name: 'file',
            showUploadList: false,
            action: config.file_server + 'identity',
            onChange: this.handleChanget,
            beforeUpload(file){
                let isJPG;
                if(file.size <= 4194304){
                    console.log(file.type);
                    if(file.type=='image/jpeg'||file.type=='image/png'||file.type=='image/bmp'||file.type=='image/gif'||file.type=='application/pdf'){
                        isJPG=true;
                    }else{
                        isJPG=false;
                        message.error('您只能上传 GIF, JPG, BMP, PNG, PDF 格式的图片');
                    }
                }else{
                    isJPG=false;
                    message.error('超过4m');
                }
                return isJPG;
            }
        };
        if(this.state.url==''){
            this.state.flag=false;
        }else if(this.state.url!=''){
            this.state.flag=true;
        }
        return(
            <div className="AnthenticationComtwo">
                <p className="AnthenticationP">
                    <div className="left">
                        1
                    </div>
                    <div className="left">
                        {I18n.t('Anthentication.uploadIp')}
                    </div>
                    <div className="clear"></div>
                </p>
                <div>
                    <div className=" Anthentication-Idcard-left left">
                        <div className="Anthentication-upload left">
                            <Dragger {...props}>
                                <Icon type="plus-circle-o" />
                                <div className="ant-upload-text">{I18n.t('PersonalCenterSecurity.SecurityAuthentication.Positive')}</div>
                            </Dragger>
                            <div className="Anthentication-upload-img" style={this.state.url != '' ? {display:'block'} : {display:'none'}}>
                                <img src={(config.file_server + this.state.url)} alt=""  />
                                <Upload {...props}>
                                    {I18n.t('CompantInvoicsThree.change')}
                                </Upload>
                            </div>
                        </div>
                        <div className="Anthentication-upload left">
                            <Dragger {...prop}>
                                <Icon type="plus-circle-o" />
                                <div className="ant-upload-text">{I18n.t('PersonalCenterSecurity.SecurityAuthentication.other')}</div>
                            </Dragger>
                            <div className="Anthentication-upload-img" style={this.state.urls != '' ? {display:'block'}:{display:'none'}}>
                                <img src={(config.file_server + this.state.urls)} alt=""  />
                                <Upload {...prop}>
                                    {I18n.t('CompantInvoicsThree.change')}
                                </Upload>
                            </div>
                        </div>
                        <div className="clear"></div>
                    </div>
                    <div className="Anthentication-Idcard-right left">
                        <p>
                            {I18n.t('PersonalCenterSecurity.SecurityAuthentication.example')}
                        </p>
                        <div style={{width:180,height:114,marginBottom:30}}>
                            <img src={config.theme_path + 'exampler.png'} alt=""/>
                        </div>
                        <div className="clear"></div>
                        <p>
                            {I18n.t('PersonalCenterSecurity.SecurityAuthentication.example')}
                        </p>
                        <div style={{width:180,height:114,marginBottom:30}}>
                            <img src={config.theme_path + 'examplel.png'} alt=""/>
                        </div>
                        <div className="clear"></div>
                    </div>
                    <div className="clear"></div>
                </div>
                <div style={{marginBottom:70}}>
                    <span style={{fontSize:14,color:'#707070',marginLeft:150}}>{I18n.t('Anthentication.data')}</span>
                    <DatePicker
                        disabledDate={this.disabledStartDate}
                        value={this.state.startValue}
                        placeholder="开始日期"
                        onChange={this.onStartChange}
                        toggleOpen={this.handleStartToggle}
                    />
                    <span style={{display:'inline-block',width:20,height:1,background:'#aeaeae',marginRight:10}}></span>
                    <DatePicker
                        disabledDate={this.disabledEndDate}
                        value={this.state.endValue}
                        placeholder="结束日期"
                        onChange={this.onEndChange}
                        open={this.state.endOpen}
                        toggleOpen={this.handleEndToggle}
                    />
                </div>
                <p className="AnthenticationP">
                    <div className="left">
                        2
                    </div>
                    <div className="left">
                        {I18n.t('Anthentication.uploadCom')}
                    </div>
                    <div className="clear"></div>
                </p>
                <div>
                    <div className=" Anthentication-Idcard-left left">
                        <div className="Anthentication-upload left">
                            <Dragger {...propt}>
                                <Icon type="plus-circle-o" />
                                <div className="ant-upload-text">{I18n.t('Anthentication.zhizhao')}</div>
                            </Dragger>
                            <div className="Anthentication-upload-img" style={this.state.urlt != '' ? {display:'block'} : {display:'none'}}>
                                <img src={(config.file_server + this.state.urlt)} alt=""  />
                                <Upload {...propt}>
                                    {I18n.t('CompantInvoicsThree.change')}
                                </Upload>
                            </div>
                        </div>
                        <div className="clear"></div>
                    </div>
                    <div className="Anthentication-Idcard-right left">
                        <p>
                            {I18n.t('PersonalCenterSecurity.SecurityAuthentication.example')}
                        </p>
                        <div style={{width:180,height:114,marginBottom:30}}>
                            <img src={config.theme_path + 'examples.png'} alt=""/>
                        </div>
                        <div className="clear"></div>
                    </div>
                    <div className="clear"></div>
                </div>
                <div style={{marginBottom:70}}>
                    <span style={{fontSize:14,color:'#707070',marginLeft:150}}>{I18n.t('Anthentication.datas')}</span>
                    <DatePicker
                        disabledDate={this.disabledStartDates}
                        value={this.state.startValues}
                        placeholder="开始日期"
                        onChange={this.onStartChanges}
                        toggleOpen={this.handleStartToggles}
                    />
                    <span style={{display:'inline-block',width:20,height:1,background:'#aeaeae',marginRight:10}}></span>
                    <DatePicker
                        disabledDate={this.disabledEndDates}
                        value={this.state.endValues}
                        placeholder="结束日期"
                        onChange={this.onEndChanges}
                        open={this.state.endOpens}
                        toggleOpen={this.handleEndToggles}
                    />
                </div>

                <div className="AnthenticationBox">
                        <p>
                            <div className="left">
                                <img src={(config.theme_path+'origin03.png')} alt=""/>
                            </div>
                            <p className="left">
                                {I18n.t('Anthentication.Cozy')}
                            </p>
                            <div className="clear"></div>
                        </p>
                        <ul>
                            <li>
                                <div className="left">
                                    <img src={(config.theme_path+'origin.png')} alt=""/>
                                </div>
                                <p className="left">
                                    {I18n.t('Anthentication.cozyone')}
                                </p>
                                <div className="clear"></div>
                            </li>
                            <li>
                                <div className="left">
                                    <img src={(config.theme_path+'origin.png')} alt=""/>
                                </div>
                                <p className="left">
                                    {I18n.t('Anthentication.cozytwo')}
                                </p>
                                <div className="clear"></div>
                            </li>
                            <li>
                                <div className="left">
                                    <img src={(config.theme_path+'origin.png')} alt=""/>
                                </div>
                                <p className="left">
                                    {I18n.t('Anthentication.cozythree')}
                                </p>
                                <div className="clear"></div>
                            </li>
                            <li>
                                <div className="left">
                                    <img src={(config.theme_path+'origin.png')} alt=""/>
                                </div>
                                <p className="left">
                                    {I18n.t('Anthentication.cozyfour')}
                                </p>
                                <div className="clear"></div>
                            </li>
                        </ul>
                </div>
                <Button style={{marginLeft:360,marginTop:60,width:120}} type="primary" onClick={this.click}>{I18n.t('forgetpassword.next')}</Button>
            </div>
        )
    }
})
export default AnthenticationComtwo;