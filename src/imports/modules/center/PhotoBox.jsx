import React, { Component } from 'react';
import { Link } from 'react-router'
import { Translate, I18n } from 'react-redux-i18n';
import { Upload, Icon, Modal,message } from 'antd';
import config from '../../config.json';


export default class PhotoBox extends Component{
    render(){
        return(
            <div className="PersonalCenterBuyer-Phone">
                <div className="PersonalCenterBuyer-del">
                    <span onClick={this.props.click}>+</span>
                </div>
                <div className="PersonalCenterBuyer-img">
                    <img src={this.props.src}/>
                </div>
                <p>
                    {this.props.name}
                </p>
            </div>
        )
    }
}