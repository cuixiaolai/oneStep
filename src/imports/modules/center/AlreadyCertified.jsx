import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
// import { createContainer } from 'meteor/react-meteor-data';
// import { Seller } from '../../api/Seller.js';
import { Link, browserHistory } from 'react-router';
import { Select, DatePicker, Modal, Button ,Input ,Menu, Dropdown, Icon,Form } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import DialogBox from './DialogBox.jsx';
import config from '../../config.json';
const createForm = Form.create;
const FormItem = Form.Item;
/*个人中心-企业资料*/
export default class AlreadyCertified extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            isyes1: true,
            isyes2: true,
            url: '',
            detail: '',
        });
    }
    clickYes1() {
        this.setState({isyes1: !this.state.isyes1, url: this.props.company_url?this.props.company_url:''});
    }
    clickYes2() {
        this.setState({isyes2: !this.state.isyes2, detail: this.props.company_detail?this.props.company_detail:''});
    }
    clickNo1() {
        // Meteor.call('buyer.updateCompanyUrl', this.state.url);
        this.setState({isyes1: !this.state.isyes1});
    }
    clickNo2() {
        // Meteor.call('buyer.updateCompanyDetail', this.state.detail);
        this.setState({isyes2: !this.state.isyes2});
    }
    getEmail() {
        let email = this.props.User.emails[0].address;
        let arr = email.split('@');
        if (arr[0] <= 4) {
            let substr = arr[0].substring(1);
            substr = substr.replace(/[^\n\r]/g, '*');
            email = arr[0][0] + substr + '@' + arr[1];
        }
        else {
            let begin = (arr[0].length - 4)%2 === 0 ? (arr[0].length - 4)/2 : (arr[0].length - 3)/2
            let substr = arr[0].substring(begin, begin+4);
            substr = substr.replace(/[^\n\r]/g, '*');
            email = arr[0].substring(0, begin) + substr + arr[0].substring(begin+4) + '@' + arr[1];
        }
        return email;
    }
    getPhone() {
        let phone = this.props.User.phone;
        if (phone != null) {
            let substr = phone.substring(4, 8);
            substr = substr.replace(/[^\n\r]/g, '*');
            phone = phone.substring(0, 4) +　substr + phone.substring(8);
        }
        return phone;
    }
    handleUrlChange(e) {
        this.setState({ url: e.target.value });
    }
    handleDetailChange(e) {
        this.setState({ detail: e.target.value });
    }
    handleOk() {
        $.ajax({
            type: 'POST',
            url: config.file_server + 'delete_com_identity',
            data: { path: this.props.authentication.front, path1: this.props.authentication.end, path2: this.props.authentication.license },
            dataType: "json",
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
        // Meteor.call('Roles.removeUsersToRoles', Meteor.userId(), 'retailer');
        // if (Roles.userIsInRole(Meteor.userId(), 'agent')) {
        //     Meteor.call('Roles.removeUsersToRoles', Meteor.userId(), 'agent');
        //     for (let i = 0; i < this.props.authInfo.authentication.agent.files.length; ++ i) {
        //         $.ajax({
        //             type: 'POST',
        //             url: config.file_server + 'delete_seller',
        //             data: { path: this.props.authInfo.authentication.agent.files[i] },

        //             dataType: "json",
        //             success: function (data) {
        //                 console.log(data);
        //             },
        //             error: function (data) {
        //                 console.log(data);
        //             }
        //         });
        //     }
        // }
        // if (Roles.userIsInRole(Meteor.userId(), 'manufacturer')) {
        //     Meteor.call('Roles.removeUsersToRoles', Meteor.userId(), 'manufacturer');
        //     for (let i = 0; i < this.props.authInfo.authentication.manufacturer.files.length; ++ i) {
        //         $.ajax({
        //             type: 'POST',
        //             url: config.file_server + 'delete_seller',
        //             data: { path: this.props.authInfo.authentication.manufacturer.files[i] },
        //             dataType: "json",
        //             success: function (data) {
        //                 console.log(data);
        //             },
        //             error: function (data) {
        //                 console.log(data);
        //             }
        //         });
        //     }
        // }
        // Meteor.call('Roles.removeUsersToRoles', Meteor.userId(), 'guarantee');
        // Meteor.call('Roles.removeUsersToRoles', Meteor.userId(), 'double');
        // Meteor.call('Roles.removeUsersToRoles', Meteor.userId(), 'stock_verify');
        // Meteor.call('stock.allOutOfStock');
        // Meteor.call('seller.deleteAuthentication');
        // Meteor.call('authentication.cancelSellerAuthentication', Meteor.userId());
        // Meteor.call('buyer.deleteIdentityAuthentication', function(err){
        //     if (err) {
        //         console.log(err);
        //     }
        //     else {
        //         Meteor.call('authentication.cancelCompanyAuthentication', Meteor.userId());
        //         browserHistory.replace('/personal_center/authentication');
        //     }
        // });
    }
    midRender() {
        console.log(this.props.authentication);
        let certified = 0;
        if (this.props.authentication == null || this.props.authentication.verify == false || this.props.authentication.success == false) {
            certified = 1;
        }
        return (
            <div className="aC_middle">
                <FormItem
                    id="control-input"
                    label={I18n.t('PersonalCenter.company.name')}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 14 }}
                >
                    {certified == 1 ? (<p className="p1">{this.props.User.company}<img src={(config.theme_path + 'unauthorized.png')} alt="" style={{width:51,height:14}}/></p>)
                    :( <p className="p1">{this.props.User.company}<img src={(config.theme_path + 'alreadyCertified.png')} alt="" style={{width:51,height:14}}/></p> )}
                </FormItem>
                <FormItem
                    id="control-input"
                    label={I18n.t('PersonalCenter.company.address')}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 14 }}
                >
                    {this.props.authentication == null ? (<p>{I18n.t('PersonalCenter.company.noWrite')}</p>) : (<p className="p1">{this.props.authentication.address}</p>)}
                </FormItem>
                <FormItem
                    id="control-input"
                    label={I18n.t('PersonalCenter.company.nature')}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 14 }}
                >
                    {this.props.authentication == null ? (<p>{I18n.t('PersonalCenter.company.noWrite')}</p>) : (<p className="p1">{this.props.authentication.industry}</p>)}
                </FormItem>
                <FormItem
                    id="control-input"
                    label={I18n.t('PersonalCenter.company.fName')}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 14 }}
                >
                    {this.props.authentication == null ? (<p>{I18n.t('PersonalCenter.company.noWrite')}</p>) : (<p className="p1">{this.props.authentication.name}</p>)}
                </FormItem>
                <FormItem
                    id="control-input"
                    label={I18n.t('PersonalCenter.company.fID')}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 14 }}
                >
                    {this.props.authentication == null ? (<p>{I18n.t('PersonalCenter.company.noWrite')}</p>) : (<p className="p1">{this.props.authentication.id}</p>)}
                </FormItem>
                { certified == 1 ? (<Link to="/personal_center/authentication"><span className="span_1">{I18n.t('PersonalCenter.company.perfect')}</span></Link>) : (<DialogBox handleOk={this.handleOk.bind(this)}  className="dialogBox " words={I18n.t('PersonalCenter.company.prompt')}  content={I18n.t('PersonalCenter.company.update')} authentication={this.props.authentication}/>)}
            </div>
        );
    }
    render(){
        if (this.props.User != null) {
            return (
                <div className="alreadyCertified">
                    <h2>{I18n.t('PersonalCenter.company.essentialInformation')} </h2>
                    <div className="aC_top">
                        <FormItem
                            id="control-input"
                            label={I18n.t('PersonalCenter.company.email')}
                            labelCol={{ span: 6 }}
                            wrapperCol={{ span: 14 }}
                        >
                            <p className="p1">{this.getEmail()}</p>
                            <Link to="/personal_center/email"><span className="span_1">{I18n.t('PersonalCenter.company.update')}</span></Link>
                        </FormItem>
                        <FormItem
                            id="control-input"
                            label={I18n.t('PersonalCenter.company.phone')}
                            labelCol={{ span: 6 }}
                            wrapperCol={{ span: 14 }}
                        >
                            <p className="p1">{this.getPhone()}</p>
                            <Link to="/personal_center/phone"><span className="span_1">{I18n.t('PersonalCenter.company.update')}</span></Link>
                        </FormItem>
                    </div>
                    <h2>{I18n.t('PersonalCenter.company.authenticationInformation')} </h2>
                    {this.midRender()}

                    <h2>{I18n.t('PersonalCenter.company.companyInformation')} </h2>

                    <div className="aC_bottom">
                        <FormItem
                            id="control-input"
                            label={I18n.t('PersonalCenter.company.website')}
                            labelCol={{ span: 6 }}
                            wrapperCol={{ span: 14 }}
                        >
                            {
                                /*根据isyes判断即认证未认证显示不同内容*/
                                this.state.isyes1 == true ? (
                                    <div className="isYes">
                                        {this.props.company_url == null ? (<p  className="left">{I18n.t('PersonalCenter.company.noWrite')}</p>) : (<p className="p1 left">{this.props.company_url}</p>)}
                                        <span className="right" onClick={this.clickYes1.bind(this)}>{this.props.company_url?I18n.t('PersonalCenter.company.fix'):I18n.t('PersonalCenter.company.towrite')}</span>
                                        <div className="clear"></div>
                                    </div>
                                ) : (
                                    <div className="isNo">
                                        <Input defaultValue={this.state.url} maxLength="36" minLength="10"  id="control-input" onChange={this.handleUrlChange.bind(this)} />
                                        <span className="span_1" onClick={this.clickNo1.bind(this)}>{I18n.t('PersonalCenter.company.yes')}</span>
                                    </div>
                                )

                            }
                        </FormItem>
                        <FormItem
                            id="control-input"
                            label={I18n.t('PersonalCenter.company.profile')}
                            labelCol={{ span: 6 }}
                            wrapperCol={{ span: 14 }}
                        >
                            {
                                /*根据isyes判断即认证未认证显示不同内容*/
                                this.state.isyes2 == true ? (
                                    <div className="isYes">
                                        {this.props.company_detail == null ? (<p className="left">{I18n.t('PersonalCenter.company.noWrite')}</p>) : (<p className="p1 left">{this.props.company_detail}</p>)}
                                        <span className="right" onClick={this.clickYes2.bind(this)}>{this.props.company_detail?I18n.t('PersonalCenter.company.fix'):I18n.t('PersonalCenter.company.towrite')}</span>
                                        <div className="clear"></div>
                                    </div>
                                ) : (
                                    <div className="isNo">
                                        <Input type='textarea' defaultValue={this.state.detail} className="left" style={{width:347,height:54,resize:'none',marginRight:10}} id="control-input" onChange={this.handleDetailChange.bind(this)} maxLength="80" />
                                        <span className="span_1 right" onClick={this.clickNo2.bind(this)}>{I18n.t('PersonalCenter.company.yes')}</span>
                                        <div className="clear"></div>
                                    </div>
                                )
                            }
                        </FormItem>
                    </div>
                </div>
            );
        }
        else {
            return <div className="alreadyCertified"></div>;
        }
    }
}

// export default createContainer(() => {
//     Meteor.subscribe('seller');
//     return {
//         authInfo: Seller.findOne({_id: Meteor.userId()}, { fields: {'authentication': 1}}),
//     };
// }, AlreadyCertified);