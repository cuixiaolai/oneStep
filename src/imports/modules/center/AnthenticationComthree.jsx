import React, { Component } from 'react';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Button} from 'antd';
import config from '../../config.json'

export default class AnthenticationComthree extends Component{
    render(){
        return(
            <div className="AnthenticationComthree">
                <p>
                    <img src={(config.theme_path + 'bigright_03.png')} alt=""/>
                    {I18n.t('Anthentication.Congratulations')}
                </p>
                <p>
                    {I18n.t('CompantInvoics.please')}
                </p>
                <Link to="/personal_center/security">
                    <Button style={{width:114,height:32,margin:0}} type="primary">{I18n.t('Addresspage.sure')}</Button>
                </Link>
            </div>
        )
    }
}