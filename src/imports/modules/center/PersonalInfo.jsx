import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import { Button, Form, Input,Select,Option ,Checkbox,Tabs,Icon,Radio} from 'antd';
// import { createContainer } from 'meteor/react-meteor-data';
import PersonalCenterRight from './PersonalCenterRight.jsx'
import EditImg from './EditImg.jsx';
// import { Buyer } from '../../api/Buyer.js';
// import { Users } from '../../api/User.js';
import config from '../../config.json';

export default class PersonalInfo extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            flags: true,
            img: null,
        });
    }
    clickOpen() {
        this.setState({flags: false});
    }
    clickClose() {
        this.setState({flags: true});
    }
    setImg(path) {
        this.setState({img: path});
        Meteor.call('buyer.updatePersonalIcon', path);
    }
    renderIcon() {
        if (this.state.img != null) {
            return (
                <div className="PerImg">
                    <img src={(config.file_server + this.state.img)} alt="" style={{width:140,height:140,display: 'inline-block'}}/>
                    <p onClick={this.clickOpen.bind(this)}>{I18n.t('PerCenterLeft.upload')}</p>
                </div>
            );
        }
        else if (this.props.UserInfo && this.props.UserInfo.icon) {
            return (
                <div className="PerImg">
                    <img src={(config.file_server + this.props.UserInfo.icon)} alt="" style={{width:140,height:140,display: 'inline-block'}}/>
                    <p onClick={this.clickOpen.bind(this)}>{I18n.t('PerCenterLeft.upload')}</p>
                </div>
            );
        }
        else {
            return (
                <div className="PerImg">
                    <img src={(config.theme_path + 'centering.png')} alt="" style={{width:140,height:140,display: 'inline-block'}}/>
                    <p onClick={this.clickOpen.bind(this)}>{I18n.t('PerCenterLeft.upload')}</p>
                </div>
            );
        }
    }
    renderInfo() {
        if (this.props.UserInfo && this.props.User) {
            return <PersonalCenterRight UserInfo={this.props.UserInfo} User={this.props.User}/>;
        }
    }
    renderEdit() {
        if (this.state.flags == false) {
            if (this.state.img != null) {
                return <EditImg click={this.clickClose.bind(this)} img={this.setImg.bind(this)} icon={this.state.img}/>;
            }
            else if (this.props.UserInfo && this.props.UserInfo.icon) {
                return <EditImg click={this.clickClose.bind(this)} img={this.setImg.bind(this)} icon={this.props.UserInfo.icon}/>;
            }
            else {
                return <EditImg click={this.clickClose.bind(this)} img={this.setImg.bind(this)}/>;
            }
        }
    }
    componentWillMount() {
        if (this.props.UserInfo && this.props.UserInfo.icon) {
            this.setState({ img: this.props.UserInfo.icon });
        }
    }
    render() {
        return (
            <div className="personal-info-box">
                <div className="PerCenterLeft right">
                    {this.renderIcon()}
                </div>
                <div className="PerCenterRight left">
                    {this.renderInfo()}
                </div>
                <div className="clear"></div>
                {this.renderEdit()}
            </div>
        );
    }
}

// export default createContainer(() => {
//     Meteor.subscribe('buyer');
//     Meteor.subscribe('users');
//     return {
//         UserInfo: Buyer.findOne({_id: Meteor.userId()}, { fields: {'nickname': 1, 'sex': 1, 'birthday': 1, 'company': 1, 'career': 1, 'industry': 1, 'icon': 1, 'authentication': 1, 'company_url': 1, 'company_detail': 1}}),
//         User: Users.findOne({_id: Meteor.userId()}, { fields: {'emails': 1, 'phone': 1, 'company': 1}}),
//     };
// }, PersonalInfo);