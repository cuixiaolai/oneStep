import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router';
import { Translate, I18n } from 'react-redux-i18n';
import { Form, Steps, Button, Input, Cascader, message } from 'antd';
import config from '../../config.json';
const FormItem = Form.Item;
const createForm = Form.create;
import VerifyCode from '../VerifyCode.jsx';

let SecurityPasswordone = React.createClass({
    getInitialState() {
        return {
            phone: '',
            label: '',
            flag:false,
            num:1,
            nums:60,
            sends:false,
            sendmessage:I18n.t('forgetpassword.two.messageVerification'),
            sendemail:I18n.t('forgetpassword.two.messageEmailVerification'),
            lebal: '',
            value: '',
            verify_code: this.generateCode(),
            showflag:false,
            errorflag:false,
        };
    },
    HandleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) {
                console.log('Errors in form!!!');
                return;
            }else{
                if (this.state.phone == I18n.t('forgetpassword.two.messageVerification') || (this.state.phone=='' && this.props.phone != "" && this.props.email=='')) {
                    console.log(I18n.t('forgetpassword.two.messageVerification'));
                    this.props.next();
                }
                else {
                    console.log(values.emailyan);
                    if (values.emailyan == null) {
                        this.setState({flag:true,errorflag:true});
                        return;
                    }
                    // Meteor.call('user.verifyEmailResetPasswordCode', Meteor.userId(), values.emailyan, function(error) {
                    //     if (error) {
                    //         this.setState({flag:true,showflag:true,errorflag:false});
                    //     }
                    //     else {
                    //         this.props.next();
                    //         this.setState({flag:false,showflag:false,errorflag:false});
                    //     }
                    // }.bind(this));
                }
            }
        });
    },
    generateCode() {
        let items = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPRSTUVWXYZ23456789'.split('');
        let randInt = function (start, end) {
            return Math.floor(Math.random() * (end - start)) + start;
        }
        let code = '';
        for (let i = 0; i < 4; ++i) {
            code += items[randInt(0, items.length)];
        }
        return code;
    },

    onChange(value) {
        console.log(value[0]);
        this.setState({value:value[0]});
        if(value[0]=='email'){
            let phone=this.state.sendemail;
            this.setState({phone: phone, lebal:I18n.t('Register.emailname')});
        }else{
            let phone=this.state.sendmessage;
            this.setState({phone: phone, lebal:I18n.t('Register.phonename')});
        }
    },
    getOption() {
        let options = [];
        if (this.props.email != '') {
            let email = this.props.email;
            let arr = email.split('@');
            if (arr[0] <= 4) {
                let substr = arr[0].substring(1);
                substr = substr.replace(/[^\n\r]/g, '*');
                email = arr[0][0] + substr + '@' + arr[1];
            }
            else {
                let begin = (arr[0].length - 4)%2 === 0 ? (arr[0].length - 4)/2 : (arr[0].length - 3)/2
                console.log(begin);
                let substr = arr[0].substring(begin, begin+4);
                substr = substr.replace(/[^\n\r]/g, '*');
                email = arr[0].substring(0, begin) + substr + arr[0].substring(begin+4) + '@' + arr[1];
            }
            options.push({
                value: 'email',
                label:  email,
            })
        }
        if (this.props.phone != '') {
            let phone = this.props.phone;
            let substr = phone.substring(4, 8);
            substr = substr.replace(/[^\n\r]/g, '*');
            phone = phone.substring(0, 4) +　substr + phone.substring(8);
            options.push({
                value: 'phone',
                label:  phone,
            })
        }
        if (options[0] != null){
            if(this.state.sends==false){
                return <Cascader size="large" options={options}  onChange={this.onChange} defaultValue={[options[0].value]}/>;
            }else if(this.state.sends==true) {
                return <Cascader size="large" options={options}    onChange={this.onChange} defaultValue={[options[0].value]}/>;
            }
        }
    },
    getTips() {
        if (this.state.label != '') {
            let phone=this.state.label;
            return <span>{phone}</span>;
        }
        if (this.state.phone == '') {
            if (this.props.email != ''){
                let phone=this.state.sendemail;
                return <span>{phone}</span>;
            }
            else if (this.props.phone != '') {
                let phone=this.state.sendmessage;
                return <span>{phone}</span>;
            }
        }
        else {
            if(this.state.sends==false){
                return <span>{this.state.phone}</span>;
            }else if(this.state.sends==true){
                let phone=(this.state.nums+I18n.t('sendagain'))
                return <span>{phone}</span>;
            }
        }
    },
    refresh(){
        if(this.state.sends==false) {
            this.props.form.resetFields(['securitycode']);
            this.setState({verify_code: this.generateCode()});
        }
    },
    checkPhoneVerifyCode(rule, value, callback){
        if (!value) {
            callback();
        } else {
            // setTimeout(() => {
            //     Meteor.call('phone.checkVerifyCode', this.props.phone, value, (err,result) => {
            //         if(result){
            //             callback();
            //         }else{
            //             callback([new Error('验证码错误')]);
            //         }
            //     });
            // },100)
        }

    },
    checkVerifyCode(rule, value, callback) {
        if (value && value.toLowerCase() != this.state.verify_code.toLowerCase()) {
            callback(I18n.t('Register.verifyCodeError'));
        } else {
            callback();
        }
    },
    SendMassage(){
        if (this.state.phone == I18n.t('forgetpassword.two.messageVerification') || (this.state.phone=='' && this.props.phone != "" && this.props.email=='')) {
            if(this.state.sends==false){
                this.props.form.validateFields(['securitycode'],(errors, values) => {
                    if (errors) {
                        if (this.props.form.getFieldError('securitycode') == I18n.t('Register.verifyCodeError')) {
                            this.setState({ verify_code: this.generateCode() });
                        }
                        console.log('Errors in form!!!');
                        return;
                    }else{
                        // Meteor.call('phone.sendMessage', this.props.phone, (err) => {
                        //     if (err) {
                        //         message.error('每天只能发送三次验证码'); //in fact three times for each phone_number
                        //         console.log(err);
                        //     } else {
                        //         // this.refresh();
                        //         this.interval = setInterval(this.tick, 1000);
                        //         this.state.sends=true;
                        //     }
                        // });
                        this.setState({sends:true});
                        this.interval = setInterval(this.tick, 1000);
                    }
                });
            }
        }else{
            if(this.state.sends==false){
                this.props.form.validateFields((errors, values) => {
                    if (errors) {
                        if (this.props.form.getFieldError('emailyan') == I18n.t('Register.verifyCodeError')) {
                            this.setState({ verify_code: this.generateCode() });
                        }
                        return;
                    }else{
                        this.setState({sends: true});
                        // Meteor.call('accounts.ResetPasswordEmail', Meteor.userId(), this.props.username, this.props.email);
                        message.success(I18n.t('forgetpassword.two.success'),3);
                        this.interval = setInterval(this.tick, 1000);
                    }
                });
            }
        }
    },
    tick(){
        this.state.nums -= 1;
        this.setState({label: this.state.nums+I18n.t('sendagain')});
        if(this.state.nums == 0){
            clearInterval(this.interval);
            this.setState({sends:false,nums:60});
            this.setState({label: ''});
        }
    },
    render(){
        const { getFieldProps } = this.props.form;
        const yanEmailProps = getFieldProps('securitycode', {
            validate: [{
                rules: [
                    { required: true, min: 4,max:4,message:I18n.t('Register.yanTishi')},
                    { validator: this.checkVerifyCode }
                ],
                trigger: 'onBlur'
            }],
        });
        const yanProps = getFieldProps('emailyan', {});
        let  phoneMessageProps;
        if (this.state.phone == I18n.t('forgetpassword.two.messageVerification') || (this.state.phone=='' && this.props.phone != "" && this.props.email=='')) {
            phoneMessageProps = getFieldProps('phone_message', {
                validate: [{
                    rules: [
                        { required: true, min: 1, message:'请输入验证码'},
                        { validator: this.checkPhoneVerifyCode }
                    ],
                    trigger: 'onBlur'
                }],
            });
        }

        if(this.state.phone==''){
            if(this.props.email!=''){
                this.state.lebal=I18n.t('Register.emailname');
            }else if(this.props.phone!=''){
                this.state.lebal=I18n.t('Register.phonename');
            }
        }
        return(
            <Form form={this.props.form} inline style={{marginLeft:124}} className="SecurityPassword">
                <FormItem
                    label={I18n.t('forgetpassword.two.please')}
                >
                    {this.getOption()}
                </FormItem>
                <FormItem
                    hasFeedback
                    label={I18n.t('forgetpassword.two.Verification')}
                    className="SecurutyPasswordone-verifycode"
                >
                    <Input  type="text"  placeholder={I18n.t('login.pic')} {...yanEmailProps} />
                    <VerifyCode code={this.state.verify_code} />
                    <div style={{position:'absolute',top:38,right:0,fontSize:12}}>{I18n.t('kan')}
                        <span onClick={this.refresh} style={this.state.sends ? {  cursor: 'auto',color:'#0a66bc'} : {  cursor:'pointer',color:'#0a66bc'}}>{I18n.t('shu')}</span>
                    </div>
                    <div className="clear"></div>
                </FormItem>
                <FormItem
                    label={this.state.lebal}
                    style={{position:'relative'}}
                >
                    {this.state.phone == I18n.t('forgetpassword.two.messageVerification') || (this.state.phone=='' && this.props.phone != "" && this.props.email=='')?
                    <Input placeholder={I18n.t('forgetpassword.two.Verificationplaceholder')} {...phoneMessageProps} />
                    :<Input placeholder={I18n.t('forgetpassword.two.Verificationplaceholder')} {...yanProps} />}

                    <div className="getyan" style={this.state.sends ? {  cursor: 'auto',color:'#999999'} : {  cursor:'pointer',color:'#5b5b5b'}} onClick={this.SendMassage}>
                        {this.getTips()}
                    </div>
                    <p style={this.state.showflag ? {position:'absolute',color:'#d43327',fontSize:'12px',lineHeight:'22px',left:10,display:'block'} : {display:'none'}} >{I18n.t('Register.verifyCodeError')}</p>
                    <p style={this.state.errorflag ? {position:'absolute',color:'#d43327',fontSize:'12px',lineHeight:'22px',left:10,display:'block'} : {display:'none'}} >{I18n.t('Register.noverifyCodeError')}</p>
                </FormItem>
                <FormItem>
                    <Button type="primary" onClick={this.HandleSubmit} style={{marginLeft:142}}>{I18n.t('forgetpassword.next')}</Button>
                </FormItem>
            </Form>
        )
    }
})
SecurityPasswordone = createForm()(SecurityPasswordone);
export default SecurityPasswordone;
