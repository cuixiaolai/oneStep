import React, { Component, PropTypes } from 'react';
// import { Accounts } from 'meteor/accounts-base';
// import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router';
// import { createContainer } from 'meteor/react-meteor-data';
import { Button, Form, Input,Select,Option ,Checkbox,Tabs,Icon,Modal} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
const confirm = Modal.confirm;

export default class Addressbox extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            flag: true,
            flags: this.props.style,
        });
    }

    click(){
        // Meteor.call('buyer.setDefaultAddress', this.props.item.id);
        window.scrollTo(0,0);
    }

    editClick() {
        this.props.click(this.props.item);
    }

    deleteClick() {
        let itemId=this.props.item.id;
        confirm({
            title: I18n.t('Addresspage.tips'),
            content: '',
            okText:'是',
            cancelText:'否',
            onOk(){
                // Meteor.call('buyer.deleteAddress', itemId);
            },
            onCancel() {},
        });
    }

    render(){
        let address = this.props.item.address.split(' ');
        return(
            <div className="Addressbox">
                <p>
                    <span>{this.props.item.name}</span>
                    <span>{address[0]+' '+address[1]}</span>
                    <span style={this.props.defaultAddress ? {display:"inline-block"} : {display: "none"}}>{I18n.t('Addresspage.default')}</span>
                </p>
                <ul>
                    <li>
                        <span>{I18n.t('Addresspage.Addressee')}</span>
                        <span>{this.props.item.name}</span>
                    </li>
                    <li>
                        <span>{I18n.t('Addresspage.Localarea')}</span>
                        <span>{this.props.item.address}</span>
                    </li>
                    <li>
                        <span>{I18n.t('Addresspage.Detailed')}</span>
                        <span style={{wordWrap:'break-word', wordBreak:'normal',lineHeight:'1.3' }}>{this.props.item.detailAddress}</span>
                    </li>
                    <li>
                        <span>{I18n.t('Addresspage.phonenum')}</span>
                        <span>{this.props.item.phone}</span>
                    </li>
                    <li>
                        <span>{I18n.t('Addresspage.Fixedtelephone')}</span>
                        <span>{this.props.item.tel}</span>
                    </li>
                    <li>
                        <span>{I18n.t('Addresspage.Zipcode')}</span>
                        <span>{this.props.item.code}</span>
                    </li>
                    <li>
                        <span onClick={this.editClick.bind(this)} >{I18n.t('Addresspage.edit')}</span>
                        <span onClick={this.deleteClick.bind(this)} >{I18n.t('Addresspage.del')}</span>
                        <span onClick={this.click.bind(this)} style={this.props.defaultAddress ? {display:"none"} : {display: "block"}} >{I18n.t('Addresspage.todefault')}</span>
                        <div className="clear"></div>
                    </li>
                </ul>
            </div>
        )
    }
}
