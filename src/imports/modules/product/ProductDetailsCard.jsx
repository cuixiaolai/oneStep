import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import { Link } from 'react-router'
import config from '../../config.json';
import { createContainer } from 'meteor/react-meteor-data';


export default class ProductDetailsCard extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            numone:true,
            numtwo: true,
            numthree:true,
            numfour:true,
        });

    }
    formatDate(date){
        let temp = '';
        if(date != undefined){
            let date_day = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
            let date_time = (date.getHours()<10?'0'+date.getHours():date.getHours())+':'+(date.getMinutes()<10?'0'+date.getMinutes():date.getMinutes())+':'+(date.getSeconds()<10?'0'+date.getSeconds():date.getSeconds());
            temp = date_day+' '+date_time;
        }
        return temp;
    }
    formatAnonymousCustomerName(customer_name, customer_anonymous){
        let temp ='';
        if(typeof customer_name != 'undefined'){
            if(customer_anonymous){
                temp += customer_name.substring(0,1);
                for(let i = 1 ; i < customer_name.length -1 ; i++){
                    temp += '*';
                }
                temp += customer_name.substring(customer_name.length -1,customer_name.length);
            }else{
                temp = customer_name;
            }
        }
        return temp;
    }
    render(){
        return(
            <div className="ProductDetailsCard">
                <div className="left">
                    <div>
                        <img src={this.props.comment.customer_portrait == ''?(config.theme_path + 'huo_03.jpg'):(config.file_server + this.props.comment.customer_portrait)} alt="" style={{width:116,height:116}} />
                    </div>
                    <p title={this.formatAnonymousCustomerName(this.props.comment.customer_name, this.props.comment.customer_anonymous)}>
                        {this.formatAnonymousCustomerName(this.props.comment.customer_name, this.props.comment.customer_anonymous)}
                    </p>
                    {this.props.comment.customer_anonymous?<p>{I18n.t('management.noname')}</p>:null}
                </div>
                <div className="left">
                    <p>{this.formatDate(this.props.comment.comment_createAt)}</p>
                    <p> <span style={{color:'#0c5aa2'}}>{I18n.t('management.goods')}</span> {this.props.comment.customer_comment_product == ''?'买家无评价':this.props.comment.customer_comment_product} </p>
                    <p> <span style={{color:'#0c5aa2'}}>{I18n.t('management.Evaluation')}</span> {this.props.comment.customer_comment_service == ''?'买家无评价':this.props.comment.customer_comment_product} </p>
                    <p style={this.props.comment.customer_has_images? {dislpaly:'block'} : {display:'none'}}>
                        <img src={(config.theme_path + 'examplel.png')} alt=""/>
                        <img src={(config.theme_path + 'examplel.png')} alt=""/>
                        <img src={(config.theme_path + 'examplel.png')} alt=""/>
                        <img src={(config.theme_path + 'examplel.png')} alt=""/>
                    </p>
                    <p  style={this.props.comment.customer_has_addition ? {dislpaly:'block'} : {display:'none'}}> <span style={{color:'#999'}}>{I18n.t('management.Additional')}：</span> {this.props.comment.customer_additional_comment}</p>
                    <p  style={this.props.comment.seller_product_comment != ''?{dislpaly:'block'} : {display:'none'}}> <span style={{color:'#0c5aa2'}}>{I18n.t('management.reply')}</span> {this.props.comment.seller_product_comment} </p>
                </div>
                <div className="clear"></div>
            </div>
        )
    }
}