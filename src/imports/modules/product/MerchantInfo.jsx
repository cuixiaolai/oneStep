import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';

export default class MerchantInfo extends Component {
    constructor(props) {
        super(props);
        this.state={company_info: null};
    }
    componentWillMount() {
        Meteor.call('seller.getSellerInfo', this.props.sellerId, function(err, data) {
            if (err) {
                console.log(err);
            }
            else {
                this.setState({company_info: data});
                console.log(data);
            }
        }.bind(this));
    }
    renderCompanyInfo() {
        if (this.state.company_info != null) {
            return (
                <div className="top">
                    <p> <span>{I18n.t('productdetails.corporatename')}</span>{this.state.company_info.company}</p>
                    <p> <span>{I18n.t('productdetails.companyaddress')}</span>{this.state.company_info.address}</p>
                    <p> <span>{I18n.t('productdetails.companywebsite')}</span>{this.state.company_info.company_url}</p>
                    <p> <span>{I18n.t('productdetails.mail')} </span>{this.state.company_info.company_email}</p>
                    <p> <span>{I18n.t('productdetails.companyprofile')}</span>{this.state.company_info.company_detail}</p>
                    <p> <span>{I18n.t('productdetails.ordercontact')}</span>{this.state.company_info.contact_name}</p>
                    <p> <span>{I18n.t('productdetails.ordercontactphone')}</span>{this.state.company_info.contact_phone}</p>
                    <p> <span>{I18n.t('productdetails.ordercontactemail')} </span>{this.state.company_info.contact_email}</p>
                </div>
            );
        }
    }
    showAgent() {
        if (this.state.company_info != null && this.state.company_info.agent.length != 0) {
            return (
                <div className="dai">
                    <img src={(config.theme_path+'dai.png')} alt="图片"/>
                    <div style={{width:'100%',marginLeft:'30px',height:1,borderTop:'1px solid #d6d6d6',marginBottom:10}}></div>
                    <ul>
                        {this.state.company_info.agent.map((text) => {
                            return <li>{text}</li>;
                        })}
                    </ul>
                    <div className="clearFloat"></div>
                </div>
            );
        }
    }
    showManufacturer() {
        if (this.state.company_info != null && this.state.company_info.manufacturer.length != 0) {
            return(
                <div className="changpai">
                    <img style={{marginBottom:5}} src={(config.theme_path+'changpai.png')} alt="图片"/>
                    <div style={{width:'100%',marginLeft:'30px',height:1,borderTop:'1px solid #d6d6d6',marginBottom:10}}></div>
                    <ul>
                        {this.state.company_info.manufacturer.map((text) => {
                            return <li>{text}</li>;
                        })}
                    </ul>
                    <div className="clearFloat"></div>
                </div>
            );
        }
    }
    render() {
        return (
            <div className="tab3">
                <span className="bluespan"></span>
                <h3>{I18n.t('productdetails.basicdata')}</h3>
                {this.renderCompanyInfo()}
                <span className="bluespan"></span>
                 <h3>{I18n.t('productdetails.platformtag')}</h3>
                 <div className="middle">
                     {
                         (Roles.userIsInRole(Meteor.userId(), 'double')) ? (<img  src={(config.theme_path+'pei_1.png')} alt="图片"/>) : ('')
                     }
                     {
                         (Roles.userIsInRole(Meteor.userId(), 'guarantee')) ? (<img  src={(config.theme_path+'bao_1.png')} alt="图片"/>) : ('')
                     }
                     {
                         (Roles.userIsInRole(Meteor.userId(), 'stock_verify')) ? (<img  src={(config.theme_path+'pei_1.png')} alt="图片"/>) : ('')
                     }
                 </div>
                <span className="bluespan"></span>
                <h3>{I18n.t('productdetails.selleridentity')}</h3>
                <div className="bottom" style={{marginTop:20}}>
                    {this.showManufacturer()}
                    {this.showAgent()}
                    <div className="lingshou">
                        <img src={(config.theme_path+'lingshou.png')} alt="图片"/>
                    </div>

                </div>
            </div>
        );
    }
}