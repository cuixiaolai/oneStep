import React, { Component } from 'react';
import { Select, Dropdown, DatePicker, Modal, Button ,Input ,Menu, Icon,Form ,Tabs, Col ,Checkbox ,Pagination,Cascader} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
import {Link} from 'react-router'
import SearchCommodity from '../order/SearchCommodity.jsx';
import SelectBox from './SelectBox.jsx';
{/*退换货单*/}

export default class ReturnNoteAll extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            downflag:false,
            current_page: 1,

            searchWord:'',  //搜索框关键字
            dropdownLable:I18n.t('returnnote.lastweek'), //下拉菜单显示
            dropdownkey:'0',
            totalnum:0,
        });
    }
    componentDidMount(){
    }
    //按商品名搜索
    searchcommodity(value){
        this.setState({
            searchWord:value.trim(),
        });
    }

    //搜索
    showSearch() {
        let temp = [];
        temp[0] = (
            <div>
                <div className="searchcommodity">
                    <SearchCommodity placeholder= {I18n.t('order.placeholder')}
                                     onSearch={this.searchcommodity.bind(this)} style={{ width: 240,height: 40 }}/>
                </div>
                <div className="selectbox">
                    <SelectBox />
                </div>
            </div>
        )
        return temp;
    }
    showPackage(package_arr){
        let packageStr = '';
        for (let i = 0; i < package_arr.length; ++ i) {
            if (packageStr == '') {
                packageStr = package_arr[i].package;
            }
            else {
                packageStr = packageStr + ', ' + package_arr[i].package;
            }
        }
        return packageStr;
    }
    //商品
    showDetails(product) {
        let temp = [];
        temp[0] = (
            <div className="clearFloat">
                <div className="productLeft">
                    <img src={(config.theme_path+'shop_03.jpg')} alt="图片"/>
                </div>
                <div className="productRight">
                    <h3>{product.product_id}</h3>
                    <p>厂家：{product.manufacturer} <span>{this.showPackage(product.package)}</span> </p>
                    <p className="p_">{product.describe}</p>
                </div>
            </div>
        )
        return temp;
    }
    //数量
    showAmount(quantity) {
        let temp = [];
        temp[0] = (
            <div >
                ×{quantity}
            </div>)
        return temp;
    }
    calculatePrice(price, quantity){
        let grad = 0;
        let flag = true;
        for (let j = 0; j < price.length && flag; j++) {
            if (quantity >= price[j].min) {
                grad = j;
            }else {
                flag = false;
            }
        }
        return price[grad].price
    }
    //交易金额
    showtransactionamount(price,quantity,currency) {
        let temp = [];
        temp[0] = (
            <div >{currency==1?'￥':'$'}{this.calculatePrice(price,quantity)}/个</div>
        )
        return temp;
    }
    //退款金额
    showrefundamount(product,currency,type,problem_detail) {
        let temp = [];
        if(type == 'return_goods'){
            temp.push(
                <div >{currency==1?'￥':'$'}<span className="total">{problem_detail.return_goods.amount}</span> </div>
            )
        }else if(type == 'return_fund'){
            temp.push(
                <div >{currency==1?'￥':'$'}<span className="total">{problem_detail.return_fund.amount}</span> </div>
            )
        }else if(type == 'quality_assurance'){

        }
        return temp;
    }
    //申请时间
    showapplicationtime(product,type,problem_detail) {
        let temp = [];
        let date = null;
        if(type == 'return_goods'){
            date = problem_detail.return_goods.createAt_problem;
        }else if(type == 'return_fund'){
            date = problem_detail.return_fund.createAt_problem;
        }else if(type == 'quality_assurance'){
            date = problem_detail.quality_assurance.createAt_problem;
        }
        let date_day='';
        let date_time='';
        if(date!=null){
            date_day = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
            date_time = (date.getHours()<10?'0'+date.getHours():date.getHours())+':'+(date.getMinutes()<10?'0'+date.getMinutes():date.getMinutes())+':'+(date.getSeconds()<10?'0'+date.getSeconds():date.getSeconds());
        }
        temp[0] = (
            <div className="applicationtime">
                <p>{date_day}</p>
                <p>{date_time}</p>
            </div>
        )
        return temp;
    }
    //服务类型
    showservicetype(type){
        let temp = [];
        if(type == 'return_fund'){
            temp[0] = (
                <div>
                    退款
                </div>      
            )
        }else if(type == 'return_goods'){
            temp[0] = (
                <div>
                    退货
                </div>
            )
        }else if(type == 'quality_assurance'){
            temp[0] = (
                <div>
                    质保
                </div>
            )
        }

        return temp;
    }
    //订单状态
    showorderstatus(product,_id,type,problem_detail){
        let temp = [];
        if(type == 'return_goods'){
            let text = [];
            if(problem_detail.return_goods_status == 4){
                if(problem_detail.return_goods.agreed){
                    text.push(<span className="hover-blue">退货成功</span>);
                }else{
                    text.push(<span className="hover-blue">退货失败</span>);
                }
            }else{
                text.push(<span className="hover-blue">退货中</span>);
            }
            temp.push(<Link to={"/return/"+this.props.user_type+"returngoods_"+_id} >{text}</Link>);
            temp.push(
                <Link to={"/goods_details/"+this.props.user_type+"returngoods_"+_id}>
                    <span className="orderdetails">{I18n.t('shoppingCart.paymentMoney.details')}</span>
                </Link>
            );
        }else if(type == 'return_fund'){
            let text = [];
            if(problem_detail.return_fund_status == 1){
                text.push(<span className="hover-blue">{I18n.t('returnnote.refund')}</span>);
            }else if(problem_detail.return_fund_status == 2){
                if(problem_detail.return_fund.agreed){
                    text.push(<span className="hover-blue">{I18n.t('returnnote.refundsuccess')}</span>);
                }else{
                    text.push(<span className="hover-blue">{I18n.t('returnnote.hasrefused')}</span>);
                }
            }
            temp.push(<Link to={"/return/"+this.props.user_type+"refund_"+_id} >{text}</Link>);
            temp.push(
                <Link to={"/goods_details/"+this.props.user_type+"refund_"+_id}>
                    <span  className="orderdetails">{I18n.t('shoppingCart.paymentMoney.details')} </span>
                </Link>
            );
        }else if(type == 'quality_assurance'){
            let text = [];
            if(problem_detail.quality_assurance_status == 5){
                text.push(<span className="hover-blue">质保完成</span>);
            }else{
                text.push(<span className="hover-blue">质保申请中</span>);
            }
            temp.push(<Link to={"/return/"+this.props.user_type+"warranty_"+_id} >{text}</Link>);
            temp.push(
                <Link to={"/goods_details/"+this.props.user_type+"warranty_"+_id}>
                    <span className="orderdetails">{I18n.t('shoppingCart.paymentMoney.details')}</span>
                </Link>
            );
        }
        return <div>{temp}</div>;
    }

    clickDown(obj) {
        if(obj.key == "0"){
            this.setState({
                dropdownLable:I18n.t('returnnote.lastweek'),
            });
        }
        if(obj.key == "1"){
            this.setState({
                dropdownLable:I18n.t('returnnote.lastmonth'),
            });
        }
        if(obj.key == "2"){
            this.setState({
                dropdownLable:I18n.t('returnnote.lastthreemonths'),
            });
        }
        if(obj.key == "3"){
            this.setState({
                dropdownLable:I18n.t('returnnote.recenthalfyear'),
            });
        }
        this.setState({
            dropdownkey:obj.key,
        });
        if(this.state.downflag == false){
            this.setState({downflag: true});
        }else{
            this.setState({downflag: false});
        }
    }
    showTop(){
        let temp = [];
        const menu = (
            <Menu onClick={this.clickDown.bind(this)}>
                <Menu.Item key="0">
                    <span>{I18n.t('returnnote.lastweek')}</span>
                </Menu.Item>
                <Menu.Item key="1">
                    <span>{I18n.t('returnnote.lastmonth')}</span>
                </Menu.Item>
                <Menu.Divider />
                <Menu.Item key="2">
                    <span>{I18n.t('returnnote.lastthreemonths')}</span>
                </Menu.Item>
                <Menu.Item key="3">
                    <span>{I18n.t('returnnote.recenthalfyear')}</span>
                </Menu.Item>
            </Menu>
        );

        temp[0] = (
            <div className="showTop">
                <ul className="top">
                    <li style={{width:80}} className="timeframe">
                        <Dropdown overlay={menu} trigger={['click']}>
                            <a className="ant-dropdown-link" href="#">
                                {this.state.dropdownLable} <Icon type="down" style={this.state.downflag ? {transform:'scale(0.667) rotate(180deg)'} : {transform:'scale(0.667) rotate(0deg)'}} />
                            </a>
                        </Dropdown>
                    </li>
                    <li style={{width:240}}>
                        {I18n.t('order.commodity')}
                    </li>
                    <li style={{width:100}}>
                        {I18n.t('order.amount')}
                    </li>
                    <li style={{width:110}}>
                        {I18n.t('returnnote.transactionamount')}
                    </li>
                    <li style={{width:110}}>
                        {I18n.t('returnnote.refundamount')}
                    </li>
                    <li style={{width:110}}>
                        {I18n.t('returnnote.applicationtime')}
                    </li>
                    <li style={{width:120}}>
                        {I18n.t('returnnote.servicetype')}
                    </li>
                    <li style={{width:115}}>
                        {I18n.t('returnnote.orderstatus')}
                    </li>
                </ul>
            </div>

        )
        return temp;
    }

    showTableTop(company_type, company_name, customer_name, id, problem_type,problem_detail){
        let temp = [];
        let sign = '';
        if (company_type == 'agent') {
            sign = '代理商';
        } else if (company_type == 'stock') {
            sign = '厂商'
        } else if (company_type == 'retailer') {
            sign = '零售商'
        }
        let date = null;
        if(problem_type == 'return_goods'){
            date = problem_detail.return_goods.update_time_problem;
        }else if(problem_type == 'return_fund'){
            date = problem_detail.return_fund.update_time_problem;
        }else if(problem_type == 'quality_assurance'){
            date = problem_detail.quality_assurance.update_time_problem;
        }
        let date_day='';
        if(date!=null){
            date_day = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
        }
        temp[0] = (
            <div className="tableTop">
                <span title={this.props.user_type == 'seller'?customer_name:company_name} style={{display: 'inline-block',width: 90,textOverflow: 'ellipsis',overflow: 'hidden',lineHeight: '20px'}}>
                    {this.props.user_type == 'seller'?customer_name:[company_name, <span className="sign">{sign}</span>]}
                </span>
                <img src={(config.theme_path+'talk.png')} className="sellertalkImg" alt="图片"/>
                <p className="ordernumber">{I18n.t('order.ordernumber')}<span>{id}</span></p>
                <p className="orderdate">{date_day}</p>
            </div>
        )

        return temp;
    }
    showOrder(order) {
        let temp = [];

        for(let i in order.product){
            let tr = [];
            tr.push(
                <td style={{width:350}}>
                    {this.showDetails(order.product[i])}
                </td>
            );
            tr.push(
                <td style={{width:100}}>
                    {this.showAmount(order.product[i].quantity)}
                </td>
            );
            tr.push(
                <td style={{width:110}}>
                    {this.showtransactionamount(order.product[i].price, order.product[i].quantity, order.currency)}
                </td>
            );
            if(order.product.length > 1){
                if(i == 0) {
                    tr.push(
                        <td style={{width: 115}} rowSpan={order.product.length}>
                            {this.showrefundamount(order.product[i], order.currency, order.type, order.problem_detail)}
                        </td>
                    );
                    tr.push(
                        <td style={{width: 110}} rowSpan={order.product.length}>
                            {this.showapplicationtime(order.product[i], order.type, order.problem_detail)}
                        </td>
                    );
                    tr.push(
                        <td style={{width: 120}} className="status" rowSpan={order.product.length}>
                            {this.showservicetype(order.type)}
                        </td>
                    );
                    tr.push(
                        <td style={{width: 115}} className="status" rowSpan={order.product.length}>
                            {this.showorderstatus(order.product, order._id, order.type, order.problem_detail)}
                        </td>
                    );
                }
            }else{
                tr.push(
                    <td style={{width:115}}>
                        {this.showrefundamount(order.product[i],order.currency,order.type, order.problem_detail)}
                    </td>
                );
                tr.push(
                    <td style={{width:110}}>
                        {this.showapplicationtime(order.product[i],order.type, order.problem_detail)}
                    </td>
                );
                tr.push(
                    <td style={{width:120}} className="status">
                        {this.showservicetype(order.type)}
                    </td>
                );
                tr.push(
                    <td style={{width:115}} className="status">
                        {this.showorderstatus(order.product,order._id,order.type,order.problem_detail)}
                    </td>
                );
            }
            temp.push(<tr>{tr}</tr>);
        }

        return temp;
    }

    //数据过滤-商品id或者订单号
    dataFilter(datas){
        let filtereddata = [];
        let now = new Date();
        let timeselect;
        //最近一周
        if(this.state.dropdownkey == "0"){
            timeselect = now.getTime() - 7*24*3600*1000;
        }
        if(this.state.dropdownkey == "1"){
            timeselect = now.getTime() - 30*24*3600*1000;
        }
        if(this.state.dropdownkey == "2"){
            timeselect = now.getTime() - 90*24*3600*1000;
        }
        if(this.state.dropdownkey == "3"){
            timeselect = now.getTime() - 183*24*3600*1000;
        }
        for(let i=0;i<datas.length;i++){
            if(datas[i].id == this.state.searchWord||datas[i].product.product_id == this.state.searchWord ||this.state.searchWord ==''){
                if(datas[i].update_time_problem.getTime() - timeselect>=0){
                    console.log("正常操作");
                    filtereddata.push(datas[i]);
                }
            }
        }

        return filtereddata;
    }
    showTable(showdata) {
        let slicedata = showdata.slice((this.state.current_page-1)*10, this.state.current_page*10);
        let temp = [];

        console.log(slicedata);
        for (let i = 0; i < slicedata.length; i++) {
            temp[i] = (
                <div className="table_">
                    {this.showTableTop(slicedata[i].company_type, slicedata[i].company_name, slicedata[i].customer_name, slicedata[i].id, slicedata[i].type,slicedata[i].problem_detail)}
                    <table  style={{border:1}}>
                        {this.showOrder(slicedata[i])}
                    </table>
                </div>
            )
        }
        return temp;
    }
    changePage(page){
        this.setState({current_page: page});
    }
    ShowPagination(data_length){
        return(
            <Pagination showQuickJumper  current={this.state.current_page} pageSize="10" onChange={this.changePage.bind(this)} total={data_length}/>
        )
    }
    render() {
        let showdata = this.dataFilter(this.props.data);
        return (
            <div className="returnnote">
                {this.showSearch()}
                {this.showTop()}
                <div className="clearFloat"></div>
                {this.showTable(showdata)}
                <div className="ShowPaginationBox">
                    {this.ShowPagination(showdata.length)}
                    <div className="clear"></div>
                </div>
            </div>
        );
    }
}
