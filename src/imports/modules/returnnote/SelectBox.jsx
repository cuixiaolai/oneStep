import React, { Component } from 'react';
import { Select } from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
const Option = Select.Option;

function handleChange(value) {
    console.log(`selected ${value}`);
}
const SelectBox = React.createClass({
    render(){
        return(
        <Select
            showSearch
            style={{ width: 130,height:30 }}
            placeholder="全部"
            optionFilterProp="children"
            onChange={handleChange}
            notFoundContent=""
        >
            <Option value="1">{I18n.t('productdetails.all')}</Option>
            <Option value="2">{I18n.t('productdetails.refundprocessed')}</Option>
            <Option value="3">{I18n.t('productdetails.waitebuyersship')}</Option>
            <Option value="4">{I18n.t('productdetails.receivegoodsmerchant')}</Option>
            <Option value="5">{I18n.t('productdetails.refundsuccess')}</Option>
            <Option value="6">{I18n.t('productdetails.hasrefused')}</Option>
            
        </Select>)
    }
})

export default SelectBox;