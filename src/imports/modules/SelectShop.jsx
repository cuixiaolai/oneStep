import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
import {Input,Icon,Tabs} from 'antd';
// import { Manufacturer } from '../api/Manufacturer.js';
// import { createContainer } from 'meteor/react-meteor-data';
import {get} from '../../utils/request.js';
const TabPane = Tabs.TabPane;

class SelectShop extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            flag: false,
            key:1,
            words:'',
            first:'',
            cityDataAll: null,
            cityData: null,
        });
    }
    initData(data) {
        let A = [];
        let B = [];
        let C = [];
        let D = [];
        let E = [];
        let F = [];
        let G = [];
        let H = [];
        let I = [];
        let J = [];
        let K = []; 
        let L = [];
        let M = [];
        let N = [];
        let O = [];
        let P = [];
        let Q = [];
        let R = [];
        let S = [];
        let T = [];
        let U = [];
        let V = [];
        let W = [];
        let X = [];
        let Y = [];
        let Z = [];
        let other = [];
        for (let i = 0; i < data.length; ++ i) {
            if (data[i].name == ''){
                continue;
            }
            if (data[i].name[0].toUpperCase() == 'A'){ A.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'B'){ B.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'C'){ C.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'D'){ D.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'E'){ E.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'F'){ F.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'G'){ G.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'H'){ H.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'I'){ I.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'J'){ J.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'K'){ K.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'L'){ L.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'M'){ M.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'N'){ N.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'O'){ O.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'P'){ P.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'Q'){ Q.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'R'){ R.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'S'){ S.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'T'){ T.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'U'){ U.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'V'){ V.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'W'){ W.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'X'){ X.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'Y'){ Y.push(data[i].name); }
            else if (data[i].name[0].toUpperCase() == 'Z'){ Z.push(data[i].name); }
            else { other.push(data[i].name); }
        }
        let cityData = {
            A: A, B: B, C: C, D: D, E: E, F: F, G: G,
            H: H, I: I, J: J, K: K, L: L, M: M, N: N,
            O: O, P: P, Q: Q, R: R, S: S, T: T, U: U,
            V: V, W: W, X: X, Y: Y, Z: Z, other: other,
        };
        this.state.cityDataAll = cityData;
        this.setState({cityDataAll: this.state.cityDataAll});
        let newarr={A:[],B:[],C:[],D:[],E:[],F:[],G:[],H:[],I:[],J:[],K:[],L:[],M:[],N:[],O:[],P:[],Q:[],R:[],S:[],T:[],U:[],V:[],W:[],X:[],Y:[],Z:[],other:[]};
        for(let i =0 ;i<this.state.cityDataAll.A.length;i++){
            newarr.A.push(this.state.cityDataAll.A[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.B.length;i++){
            newarr.B.push(this.state.cityDataAll.B[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.C.length;i++){
            newarr.C.push(this.state.cityDataAll.C[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.D.length;i++){
            newarr.D.push(this.state.cityDataAll.D[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.E.length;i++){
            newarr.E.push(this.state.cityDataAll.E[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.F.length;i++){
            newarr.F.push(this.state.cityDataAll.F[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.I.length;i++){
            newarr.I.push(this.state.cityDataAll.I[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.J.length;i++){
            newarr.J.push(this.state.cityDataAll.J[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.K.length;i++){
            newarr.K.push(this.state.cityDataAll.K[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.L.length;i++){
            newarr.L.push(this.state.cityDataAll.L[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.M.length;i++){
            newarr.M.push(this.state.cityDataAll.M[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.N.length;i++){
            newarr.N.push(this.state.cityDataAll.N[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.O.length;i++){
            newarr.O.push(this.state.cityDataAll.O[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.P.length;i++){
            newarr.P.push(this.state.cityDataAll.P[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.Q.length;i++){
            newarr.Q.push(this.state.cityDataAll.Q[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.R.length;i++){
            newarr.R.push(this.state.cityDataAll.R[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.S.length;i++){
            newarr.S.push(this.state.cityDataAll.S[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.T.length;i++){
            newarr.T.push(this.state.cityDataAll.T[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.U.length;i++){
            newarr.U.push(this.state.cityDataAll.U[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.V.length;i++){
            newarr.V.push(this.state.cityDataAll.V[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.W.length;i++){
            newarr.W.push(this.state.cityDataAll.W[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.X.length;i++){
            newarr.X.push(this.state.cityDataAll.X[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.Y.length;i++){
            newarr.Y.push(this.state.cityDataAll.Y[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.Z.length;i++){
            newarr.Z.push(this.state.cityDataAll.Z[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.G.length;i++){
            newarr.G.push(this.state.cityDataAll.G[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.H.length;i++){
            newarr.H.push(this.state.cityDataAll.H[i]);
        }
        for(let i =0 ;i<this.state.cityDataAll.other.length;i++){
            newarr.other.push(this.state.cityDataAll.other[i]);
        }
        this.setState({cityData:newarr});
    }
    componentDidMount(){
        if (this.props.manufacturer != null) {
            this.initData(this.props.manufacturer);
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.manufacturer != null) {
            if (this.props.manufacturer == null || (this.props.manufacturer != nextProps.manufacturer)) {
                this.initData(nextProps.manufacturer);
            }
        }
    }
    show(){
    }
    click(){
        this.setState({flag:!this.state.flag});
    }
    callback(key) {
        this.setState({key:key});
    }
    Close(){
        this.setState({flag:false});
    }
    onchange(){
        let StocklocationBoxInput = document.getElementsByClassName('StocklocationBoxInput')[0];
        let first = StocklocationBoxInput.value.slice(0,1);
        let aa = StocklocationBoxInput.value.toLowerCase();
        if (aa == ''){
            const newarr={A:[],B:[],C:[],D:[],E:[],F:[],G:[],H:[],I:[],J:[],K:[],L:[],M:[],N:[],O:[],P:[],Q:[],R:[],S:[],T:[],U:[],V:[],W:[],X:[],Y:[],Z:[],other:[]};
            for(let i =0 ;i<this.state.cityDataAll.A.length;i++){
                newarr.A.push(this.state.cityDataAll.A[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.B.length;i++){
                newarr.B.push(this.state.cityDataAll.B[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.C.length;i++){
                newarr.C.push(this.state.cityDataAll.C[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.D.length;i++){
                newarr.D.push(this.state.cityDataAll.D[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.E.length;i++){
                newarr.E.push(this.state.cityDataAll.E[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.F.length;i++){
                newarr.F.push(this.state.cityDataAll.F[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.I.length;i++){
                newarr.I.push(this.state.cityDataAll.I[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.J.length;i++){
                newarr.J.push(this.state.cityDataAll.J[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.K.length;i++){
                newarr.K.push(this.state.cityDataAll.K[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.L.length;i++){
                newarr.L.push(this.state.cityDataAll.L[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.M.length;i++){
                newarr.M.push(this.state.cityDataAll.M[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.N.length;i++){
                newarr.N.push(this.state.cityDataAll.N[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.O.length;i++){
                newarr.O.push(this.state.cityDataAll.O[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.P.length;i++){
                newarr.P.push(this.state.cityDataAll.P[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.Q.length;i++){
                newarr.Q.push(this.state.cityDataAll.Q[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.R.length;i++){
                newarr.R.push(this.state.cityDataAll.R[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.S.length;i++){
                newarr.S.push(this.state.cityDataAll.S[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.T.length;i++){
                newarr.T.push(this.state.cityDataAll.T[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.U.length;i++){
                newarr.U.push(this.state.cityDataAll.U[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.V.length;i++){
                newarr.V.push(this.state.cityDataAll.V[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.W.length;i++){
                newarr.W.push(this.state.cityDataAll.W[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.X.length;i++){
                newarr.X.push(this.state.cityDataAll.X[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.Y.length;i++){
                newarr.Y.push(this.state.cityDataAll.Y[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.Z.length;i++){
                newarr.Z.push(this.state.cityDataAll.Z[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.G.length;i++){
                newarr.G.push(this.state.cityDataAll.G[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.H.length;i++){
                newarr.H.push(this.state.cityDataAll.H[i]);
            }
            for(let i =0 ;i<this.state.cityDataAll.other.length;i++){
                newarr.other.push(this.state.cityDataAll.other[i]);
            }
            this.setState({cityData:newarr});
        }else {
            if(first == 'A'||first == 'a'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.A.length;i++){
                    let ss = this.state.cityDataAll.A[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.A[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.A = newarr;
                this.setState({key:'1',cityData:this.state.cityData});
            }else if(first == 'B'||first == 'b'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.B.length;i++){
                    let ss = this.state.cityDataAll.B[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.B[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.B = newarr;
                this.setState({key:'2',cityData:this.state.cityData});
            }else if(first == 'C'||first == 'c'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.C.length;i++){
                    let ss = this.state.cityDataAll.C[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.C[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.C = newarr;
                this.setState({key:'3',cityData:this.state.cityData});
            }else if(first == 'D'||first == 'd'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.D.length;i++){
                    let ss = this.state.cityDataAll.D[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.D[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.D = newarr;
                this.setState({key:'4',cityData:this.state.cityData});
            }else if(first == 'E'||first == 'e'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.E.length;i++){
                    let ss = this.state.cityDataAll.E[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.E[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.E = newarr;
                this.setState({key:'5',cityData:this.state.cityData});
            }else if(first == 'F'||first == 'f'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.F.length;i++){
                    let ss = this.state.cityDataAll.F[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.F[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.F = newarr;
                this.setState({key:'6',cityData:this.state.cityData});
            }else if(first == 'G'||first == 'g'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.G.length;i++){
                    let ss = this.state.cityDataAll.G[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.G[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.G = newarr;
                this.setState({key:'7',cityData:this.state.cityData});
            }else if(first == 'H'||first == 'h'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.H.length;i++){
                    let ss = this.state.cityDataAll.H[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.H[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.H = newarr;
                this.setState({key:'8',cityData:this.state.cityData});
            }else if(first == 'I'||first == 'i'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.I.length;i++){
                    let ss = this.state.cityDataAll.I[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.I[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.I = newarr;
                this.setState({key:'9',cityData:this.state.cityData});
            }else if(first == 'J'||first == 'j'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.J.length;i++){
                    let ss = this.state.cityDataAll.J[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.J[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.J = newarr;
                this.setState({key:'10',cityData:this.state.cityData});
            }else if(first == 'K'||first == 'k'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.K.length;i++){
                    let ss = this.state.cityDataAll.K[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.K[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.K = newarr;
                this.setState({key:'11',cityData:this.state.cityData});
            }else if(first == 'L'||first == 'l'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.L.length;i++){
                    let ss = this.state.cityDataAll.L[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.L[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.L = newarr;
                this.setState({key:'12',cityData:this.state.cityData});
            }else if(first == 'M'||first == 'm'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.M.length;i++){
                    let ss = this.state.cityDataAll.M[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.M[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.M = newarr;
                this.setState({key:'13',cityData:this.state.cityData});
            }else if(first == 'N'||first == 'n'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.N.length;i++){
                    let ss = this.state.cityDataAll.N[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.N[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.N = newarr;
                this.setState({key:'14',cityData:this.state.cityData});
            }else if(first == 'O'||first == 'o'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.O.length;i++){
                    let ss = this.state.cityDataAll.O[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.O[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.O = newarr;
                this.setState({key:'15',cityData:this.state.cityData});
            }else if(first == 'P'||first == 'p'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.P.length;i++){
                    let ss = this.state.cityDataAll.P[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.P[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.P = newarr;
                this.setState({key:'16',cityData:this.state.cityData});
            }else if(first == 'Q'||first == 'q'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.Q.length;i++){
                    let ss = this.state.cityDataAll.Q[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.Q[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.Q = newarr;
                this.setState({key:'17',cityData:this.state.cityData});
            }else if(first == 'R'||first == 'r'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.R.length;i++){
                    let ss = this.state.cityDataAll.R[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.R[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.R = newarr;
                this.setState({key:'18',cityData:this.state.cityData});
            }else if(first == 'S'||first == 's'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.S.length;i++){
                    let ss = this.state.cityDataAll.S[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.S[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.S = newarr;
                this.setState({key:'19',cityData:this.state.cityData});
            }else if(first == 'T'||first == 't'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.T.length;i++){
                    let ss = this.state.cityDataAll.T[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.T[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.T = newarr;
                this.setState({key:'20',cityData:this.state.cityData});
            }else if(first == 'U'||first == 'u'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.U.length;i++){
                    let ss = this.state.cityDataAll.U[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.U[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.U = newarr;
                this.setState({key:'21',cityData:this.state.cityData});
            }else if(first == 'V'||first == 'v'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.V.length;i++){
                    let ss = this.state.cityDataAll.V[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.V[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.V = newarr;
                this.setState({key:'22',cityData:this.state.cityData});
            }else if(first == 'W'||first == 'w'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.W.length;i++){
                    let ss = this.state.cityDataAll.W[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.W[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.W = newarr;
                this.setState({key:'23',cityData:this.state.cityData});
            }else if(first == 'X'||first == 'x'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.X.length;i++){
                    let ss = this.state.cityDataAll.X[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.X[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.X = newarr;
                this.setState({key:'24',cityData:this.state.cityData});
            }else if(first == 'Y'||first == 'y'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.Y.length;i++){
                    let ss = this.state.cityDataAll.Y[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.Y[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.Y = newarr;
                this.setState({key:'25',cityData:this.state.cityData});
            }else if(first == 'Z'||first == 'z'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.Z.length;i++){
                    let ss = this.state.cityDataAll.Z[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.Z[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.Z = newarr;
                this.setState({key:'26',cityData:this.state.cityData});
            }else if (first == '1' || first == '2' || first == '3' || first == '4' || first =='5' || first == '6' || first == '7' || first == '8' || first == '9' || first == '0'){
                let newarr = [];
                for(let i=0;i<this.state.cityDataAll.other.length;i++){
                    let ss = this.state.cityDataAll.other[i].slice(0,aa.length).toLowerCase();
                    if( ss== aa){
                        newarr.push(this.state.cityDataAll.other[i]);
                    }
                }
                if(newarr.length == 0){
                    newarr.push(I18n.t('nofind'));
                }
                this.state.cityData.other = newarr;
                this.setState({key:'27',cityData:this.state.cityData});
            }
        }

        this.state.first = first;
        this.setState({first:first});
    }
    handleProvinceChange(value){
        this.state.words = value;
        this.setState({words:this.state.words,flag:false});
        let address = this.state.words;
        this.props.func(address);
    }
    Showcircle(){
        if(this.props.value == I18n.t('PersonalCenter.manageStock.manufacturer') || this.props.value == ''){
        }else{
            return(
                <Icon type="plus-circle"  className="Stocklocation-circle" onClick={this.props.Closecircle} />
            )
        }
    }
    render(){
        if (this.state.cityData == null) {
            return <div></div>;
        }
        else {
            let StocklocationBoxInput = document.getElementsByClassName('StocklocationBoxInput')[0];
            let A = this.state.cityData.A.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let B = this.state.cityData.B.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let C = this.state.cityData.C.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let D = this.state.cityData.D.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let E = this.state.cityData.E.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let F = this.state.cityData.F.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let G = this.state.cityData.G.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let H = this.state.cityData.H.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let I = this.state.cityData.I.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let J = this.state.cityData.J.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let K = this.state.cityData.K.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let L = this.state.cityData.L.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let M = this.state.cityData.M.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let N = this.state.cityData.N.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let O = this.state.cityData.O.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let P = this.state.cityData.P.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let Q = this.state.cityData.Q.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let R = this.state.cityData.R.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let S = this.state.cityData.S.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let T = this.state.cityData.T.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let U = this.state.cityData.U.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let V = this.state.cityData.V.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let W = this.state.cityData.W.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let X = this.state.cityData.X.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let Y = this.state.cityData.Y.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let Z = this.state.cityData.Z.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            let other = this.state.cityData.other.map(province => <p><span onClick={this.handleProvinceChange.bind(this,province)} key={province}>{province}</span></p> );
            return(
                <div style={this.props.style} className="Stocklocation">
                    <div className="StocklocationShaw"  style={this.state.flag ? {display:'block'} : {display:'none'}} onClick={this.Close.bind(this)}></div>
                    <Input type="text" readOnly="readonly" value={this.props.value} style={{height:'100%',position:'relitive',zIndex:23}} onClick={this.click.bind(this)}/>
                    <Icon type="down" className="Stocklocation-down" style={this.state.flag ? {transform:'scale(0.667) rotate(180deg)'} : {transform:'scale(0.667) rotate(0deg)'}} />
                    {this.Showcircle()}
                    <div className="StocklocationBox" style={this.state.flag ? {display:'block'} : {display:'none'}}>
                        <Icon type="plus" className="StocklocationClose" onClick={this.Close.bind(this)} />
                        <Input type="text" className="StocklocationBoxInput" onChange={this.onchange.bind(this)} />
                        <Icon type="search" style={{marginLeft:-18,position:'relative',zIndex:348}}/>
                        <div className="StocklocationResultBox" style={{maxHeight:450,overflowY:'auto'}}>
                            <Tabs defaultActiveKey="1"  onChange={this.callback.bind(this)} activeKey={this.state.key}>
                                <TabPane tab="A" key="1">
                                    <div className="StocklocationsmallBox" >
                                        {A}
                                    </div>
                                </TabPane>
                                <TabPane tab="B" key="2">
                                    <div className="StocklocationsmallBox" >
                                        {B}
                                    </div>
                                </TabPane>
                                <TabPane tab="C" key="3">
                                    <div className="StocklocationsmallBox" >
                                        {C}
                                    </div>
                                </TabPane>
                                <TabPane tab="D" key="4">
                                    <div className="StocklocationsmallBox" >
                                        {D}
                                    </div>
                                </TabPane>
                                <TabPane tab="E" key="5">
                                    <div className="StocklocationsmallBox" >
                                        {E}
                                    </div>
                                </TabPane>
                                <TabPane tab="F" key="6">
                                    <div className="StocklocationsmallBox" >
                                        {F}
                                    </div>
                                </TabPane>
                                <TabPane tab="G" key="7">
                                    <div className="StocklocationsmallBox" >
                                        {G}
                                    </div>
                                </TabPane>
                                <TabPane tab="H" key="8">
                                    <div className="StocklocationsmallBox" >
                                        {H}
                                    </div>
                                </TabPane>
                                <TabPane tab="I" key="9">
                                    <div className="StocklocationsmallBox" >
                                        {I}
                                    </div>
                                </TabPane>
                                <TabPane tab="J" key="10">
                                    <div className="StocklocationsmallBox" >
                                        {J}
                                    </div>
                                </TabPane>
                                <TabPane tab="K" key="11">
                                    <div className="StocklocationsmallBox" >
                                        {K}
                                    </div>
                                </TabPane>
                                <TabPane tab="L" key="12">
                                    <div className="StocklocationsmallBox" >
                                        {L}
                                    </div>
                                </TabPane>
                                <TabPane tab="M" key="13">
                                    <div className="StocklocationsmallBox" >
                                        {M}
                                    </div>
                                </TabPane>
                                <TabPane tab="N" key="14">
                                    <div className="StocklocationsmallBox" >
                                        {N}
                                    </div>
                                </TabPane>
                                <TabPane tab="0" key="15">
                                    <div className="StocklocationsmallBox" >
                                        {O}
                                    </div>
                                </TabPane>
                                <TabPane tab="P" key="16">
                                    <div className="StocklocationsmallBox" >
                                        {P}
                                    </div>
                                </TabPane>
                                <TabPane tab="Q" key="17">
                                    <div className="StocklocationsmallBox" >
                                        {Q}
                                    </div>
                                </TabPane>
                                <TabPane tab="R" key="18">
                                    <div className="StocklocationsmallBox" >
                                        {R}
                                    </div>
                                </TabPane>
                                <TabPane tab="S" key="19">
                                    <div className="StocklocationsmallBox" >
                                        {S}
                                    </div>
                                </TabPane>
                                <TabPane tab="T" key="20">
                                    <div className="StocklocationsmallBox" >
                                        {T}
                                    </div>
                                </TabPane>
                                <TabPane tab="U" key="21">
                                    <div className="StocklocationsmallBox" >
                                        {U}
                                    </div>
                                </TabPane>
                                <TabPane tab="V" key="22">
                                    <div className="StocklocationsmallBox" >
                                        {V}
                                    </div>
                                </TabPane>
                                <TabPane tab="W" key="23">
                                    <div className="StocklocationsmallBox" >
                                        {W}
                                    </div>
                                </TabPane>
                                <TabPane tab="X" key="24">
                                    <div className="StocklocationsmallBox" >
                                        {X}
                                    </div>
                                </TabPane>
                                <TabPane tab="Y" key="25">
                                    <div className="StocklocationsmallBox" >
                                        {Y}
                                    </div>
                                </TabPane>
                                <TabPane tab="Z" key="26">
                                    <div className="StocklocationsmallBox" >
                                        {Z}
                                    </div>
                                </TabPane>
                                <TabPane tab="其它" key="27">
                                    <div className="StocklocationsmallBox" >
                                        {other}
                                    </div>
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default class SelectShop1 extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            manufacturer: null
        });
    }    
    componentWillReceiveProps(nextProps) {
        if (nextProps.manufacturer != null) {
            if (this.props.manufacturer == null || (this.props.manufacturer != nextProps.manufacturer)) {
                get("manufacturer").then(body=>{
                    if(body.code&&body.code == 200)
                        this.setState({manufacturer: body.body});
                })
            }
        }        
    }
    componentWillMount() {
        get("manufacturer").then(body=>{
                    if(body.code&&body.code == 200)
                        this.setState({manufacturer: body.body});
                })
    }
    render() {
       
            return <SelectShop style={this.props.style} manufacturer={this.state.manufacturer} value={this.props.value} Closecircle={this.props.Closecircle.bind(this)} func={this.props.func.bind(this)}></SelectShop>;
        }

}

// export default createContainer(() => {
//     Meteor.subscribe('manufacturer');
//     return {
//         manufacturer: Manufacturer.find({}).fetch(),
//     };
// }, SelectShop);