import React, { Component } from 'react';
import { Translate, I18n } from 'react-redux-i18n';
// import ProductDetailsCard from './ProductDetailsCard';
import { Rate } from 'antd';

class GoodsEvaluation extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            type:0
        });

    }
    onClick(word,j){
        console.log(word);
        let ProductDetailsBoxBottomname = document.getElementsByClassName('ProductDetailsBoxBottomname')[0];
        let num = ProductDetailsBoxBottomname.getElementsByTagName('span');
        for(let i=0;i<num.length;i++){
            if(i == j){
                num[j].style.color = '#0c5aa2';
                num[j].style.borderBottom = '2px solid #0c5aa2';
            }else{
                num[i].style.color = '#707070';
                num[i].style.borderBottom = '0';
            }
        }
        this.setState({
           type: j
        });
    }
    showComment(comment){
        let temp = [];
        // for(let i in comment){
        //     if(this.state.type == 0){
        //         temp.push(<ProductDetailsCard comment={comment[i]}/>);
        //     }else if(this.state.type == 1){
        //         if(comment[i].customer_has_images){
        //             temp.push(<ProductDetailsCard comment={comment[i]}/>);
        //         }
        //     }else if(this.state.type == 2){
        //         if(comment[i].customer_has_addition){
        //             temp.push(<ProductDetailsCard comment={comment[i]}/>);
        //         }
        //     }
        // }
        return temp;
    }
    render() {
        if(this.props.stockInfo != null && this.props.commentInfo != null && this.props.commentInfo[0] != null){
            return (
                <div className="ProductDetailsBox">
                    {typeof this.props.stockInfo.comment_five_stars != 'undefined'?
                        <div className="ProductDetailsBoxTop">
                                <span>
                                    <span className="left">{I18n.t('management.Pin')} </span> <Rate disabled  allowHalf defaultValue={Math.round(this.props.stockInfo.comment_five_stars.socle * 2)/2} />
                                    {this.props.stockInfo.comment_five_stars.socle.toFixed(2)}
                                </span>
                                <span>
                                    <span className="left">{I18n.t('management.speed')}</span> <Rate disabled  allowHalf defaultValue={Math.round(this.props.stockInfo.comment_five_stars.speed * 2)/2} />
                                    {this.props.stockInfo.comment_five_stars.speed.toFixed(2)}
                                </span>
                                <span>
                                     <span className="left">{I18n.t('management.service')}</span>  <Rate disabled  allowHalf defaultValue={Math.round(this.props.stockInfo.comment_five_stars.service * 2)/2} />
                                    {this.props.stockInfo.comment_five_stars.service.toFixed(2)}
                                </span>
                                <span>
                                    <span className="left">{I18n.t('management.identification')}</span> <Rate disabled  allowHalf defaultValue={Math.round(this.props.stockInfo.comment_five_stars.label * 2)/2} />
                                    {this.props.stockInfo.comment_five_stars.label.toFixed(2)}
                                </span>
                                <span>
                                    <span className="left">{I18n.t('management.package')}</span> <Rate disabled  allowHalf defaultValue={Math.round(this.props.stockInfo.comment_five_stars.package * 2)/2} />
                                    {this.props.stockInfo.comment_five_stars.package.toFixed(2)}
                                </span>
                        </div>
                        :null
                    }
                    <div className="ProductDetailsBoxBottom">
                        <div className="ProductDetailsBoxBottomname">
                            <span onClick={this.onClick.bind(this,'all',0)}>{I18n.t('order.all')}</span>
                            <span onClick={this.onClick.bind(this,'picture',1)}>{I18n.t('management.pic')}</span>
                            <span onClick={this.onClick.bind(this,'review',2)}>{I18n.t('management.zhui')}</span>
                            {/*<span onClick={this.onClick.bind(this,'back',3)}>{I18n.t('management.return')}</span>*/}
                        </div>
                        {this.showComment(this.props.commentInfo)}
                    </div>
                </div>
            );
        }else{
            return <div>该产品还没有评价</div>;
        }

    }
}

export default GoodsEvaluation;