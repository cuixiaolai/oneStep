import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
import goodsDetail from './goodsDetail.json';

export default class GoodsDetail extends Component {
    constructor(props) {
        super(props);
        this.state={product_info: null};
    }
    componentWillMount() {
        // Meteor.call('product.findProductInfo', this.props.stockInfo.product_id, function(err, data) {
        //     if (err) {
        //         console.log(err);
        //     }
        //     else {
                // this.setState({product_info: data});
                this.setState({product_info: goodsDetail});
        //     } 
        // }.bind(this));
    }
    randerInfo() {
        if (this.state.product_info != null) {
            let tmp = [];
            tmp[0] = <p> <span>{I18n.t('productdetails.technicalspecifications')} </span>{this.state.product_info.Description}</p>;
            tmp[1] = <p> <span>{I18n.t('productdetails.series')}</span>{this.state.product_info.Series}</p>;
            let i = 2;
            for (var item in this.state.product_info) {
                if (item == '_id' || item == 'Datasheets' || item == 'Image' || item == 'Manufacturer Part Number' || item == 'Manufacturer' || item == 'Description' || item == 'Series') {
                    continue;
                }
                tmp[i++] = <p> <span>{item+'：'}</span>{this.state.product_info[item]}</p>;
            }
            return tmp;
        }
    }
    render() {
        let package_str = '';
        for (let i = 0; i < this.props.stockInfo.package.length; ++ i) {
            if (package_str == '') {
                package_str = this.props.stockInfo.package[i].package;
            }
            else {
                package_str += ', ' + this.props.stockInfo.package[i].package;
            }
        }
        let stock_address = '';
        for (let i = 0; i < this.props.stockInfo.stock_address.length; ++ i) {
            if (stock_address == '') {
                stock_address = this.props.stockInfo.stock_address[i].address;
            }
            else {
                stock_address += ', ' + this.props.stockInfo.stock_address[i].address;
            }
        }
        let batch = '';
        for (let i = 0; i < this.props.stockInfo.batch.length; ++ i) {
            if (batch == '') {
                batch = this.props.stockInfo.batch[i].num;
            }
            else {
                batch += ', ' + this.props.stockInfo.batch[i].num;
            }
        }
        let origin_address = '';
        for (let i = 0; i < this.props.stockInfo.origin_address.length; ++ i) {
            if (origin_address == '') {
                origin_address = this.props.stockInfo.origin_address[i].address;
            }
            else {
                origin_address += ', ' + this.props.stockInfo.origin_address[i].address;
            }
        }
        return (
            <div className="tab1">
                <div className="top">
                    <p>{I18n.t('productdetails.supplierstandardpackaging')}{this.props.stockInfo.standard_packing}</p>
                    <p>{I18n.t('productdetails.increasingnumber')}{this.props.stockInfo.inc_number}</p>
                    <p>{I18n.t('productdetails.minimumorderquantity')}{this.props.stockInfo.MOQ}</p>
                    <p>{I18n.t('productdetails.packingmethod')}{package_str}</p>
                </div>
                <div className="middle">
                    <p>{I18n.t('productdetails.stocklocation')}{stock_address}</p>
                    <p>{I18n.t('productdetails.batchnumber')}{batch}</p>
                    <p>{I18n.t('productdetails.originplace')}{origin_address}</p>
                </div>
                <div className="bottom">
                    {this.randerInfo()}
                </div>
                <div className="pdf">
                    <img src={(config.theme_path+'pdf.png')} alt="图片"/>
                    <span>{I18n.t('productdetails.viewdatasheet')} </span>
                </div>
            </div>
        );
    }
}
