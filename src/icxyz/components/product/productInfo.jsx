import React, { Component } from 'react';
// import { Meteor } from 'meteor/meteor';
import { Select,Rate, Dropdown, DatePicker, Modal, Button ,Input ,Menu, Icon,Form ,Tabs, InputNumber ,Checkbox ,Pagination,Radio,message} from 'antd';
import { Translate, I18n } from 'react-redux-i18n';
import config from '../../config.json';
import { Link, browserHistory } from 'react-router';
const RadioGroup = Radio.Group;
// import SearchBox from '../search/SearchBox.jsx';

export default class ProductInfo extends Component{
    constructor(props) {
        super(props);
        this.state = ({
            value:1,
            number: 1,
            price: 0,
            totalPrice: 0,
            onSpan:0,
            data: null,
            headicon: '',
            stocktype:'',
            topayment:false,
            addtocart:false,
            showcard:false,
            num:5,
        });
    }
    Closeshowcard(){
        this.setState({showcard:false});
    }
    ShowCard(){
        return(
            <div className="SearchShowCard" style={this.state.showcard ? {display:'block',top:'-24px',right:'-84px'} : {display:'none'}}>
                <div className="SearchShowCardJiao">
                    <div className="SearchShowCardJiaoinner"></div>
                </div>
                <p style={{textAlign:'right'}}>
                    <span onClick={this.Closeshowcard.bind(this)} style={{cursor:'pointer'}}>×</span>
                </p>
                <p>
                    <div className="left">
                        <img src={(config.theme_path + 'right_03.png')} alt=""/>
                    </div>
                    <div style={{fontSize:'20px',color:'#5b5b5b',marginLeft:10,lineHeight:'24px'}} className="left">{I18n.t('Search.success')}</div>
                    <div className="clear"></div>
                </p>
                <p>
                    <div style={{marginLeft:34,marginTop:8}}><span>{I18n.t('sellerorderdetails.youcango')} </span> <Link to="/shopping_cart">{I18n.t('Search.payment')}</Link> </div>
                </p>
            </div>
        )
    }
    componentWillMount() {
        let i = 0;
        console.log("this.props.stockInfo.price")
        console.log(this.props.stockInfo)
        console.log(this.props.stockInfo.price)
        for (; i < this.props.stockInfo.price.length; ++ i) {
            if (this.props.stockInfo.price[i].min > this.props.stockInfo.MOQ) {
                break;
            }
        }
        let product_price = this.props.stockInfo.price[i-1].price;
        this.setState({number: this.props.stockInfo.MOQ, price: product_price, totalPrice: product_price*this.props.stockInfo.MOQ});
        let stock_address = [];
        for (let j in this.props.stockInfo.stock_address) {
            let item = [this.props.stockInfo.stock_address[j].address, this.props.stockInfo.stock_address[j].quantity];
            stock_address.push(item);
        }
        let origin_address = [];
        for (let j in this.props.stockInfo.origin_address) {
            let item = [this.props.stockInfo.origin_address[j].address, this.props.stockInfo.origin_address[j].quantity];
            origin_address.push(item);
        }
        let batch = [];
        for (let j in this.props.stockInfo.batch) {
            let item = [this.props.stockInfo.batch[j].num, this.props.stockInfo.batch[j].quantity];
            batch.push(item);
        }
        let package_way = [];
        for (let j in this.props.stockInfo.package) {
            let item = [this.props.stockInfo.package[j].package, this.props.stockInfo.package[j].quantity];
            package_way.push(item);
        }
        let price = [];
        let rmb = [];
        let dollar = [];
        for (let j in this.props.stockInfo.price) {
            let item = this.props.stockInfo.price[j].min;
            price.push(item);
            if (this.props.stockInfo.currency == 1) {
                rmb.push(this.props.stockInfo.price[j].price);
            }
            else if (this.props.stockInfo.currency == 2) {
                dollar.push(this.props.stockInfo.price[j].price);
            }
        }
        let item_data = {
            key: this.props.stockInfo._id,
            name: this.props.stockInfo.user_company,
            money: this.props.stockInfo.product_id,
            address: this.props.stockInfo.manufacturer,
            kucun: this.props.stockInfo.stock_quantity,
            img: this.props.stockInfo.img,
            suozaidi: stock_address,
            chandi: origin_address,
            pihao: batch,
            baozhuangfangshi: package_way,
            tidu: price,
            renmingbi: rmb,
            meiyuan: dollar,
            standard_packing: this.props.stockInfo.standard_packing,
            inc_number: this.props.stockInfo.inc_number,
            MOQ: this.props.stockInfo.MOQ,
            user_id: this.props.stockInfo.user_id,
            quantity: this.props.stockInfo.MOQ,
        };
        // Meteor.call('buyer.getIcon', this.props.stockInfo.user_id, function(err, data) {
        //     if (err) {
        //         console.log(err);
        //     }
        //     else {
        //         this.setState({headicon: data});
        //     }
        // }.bind(this));
        this.setState({data: item_data, stocktype: this.props.stockInfo.type});
    }
    // 立即购买
    showToPayment(){
        // if(this.state.topayment == true){
        //     return(
        //         <SearchBox word={I18n.t('productdetails.buyimmediately')} click={this.closeToPayment.bind(this)} flag={false} data={this.state.data} Card={this.ToPayment.bind(this)} stocktype={this.state.stocktype} />
        //     )
        // }
    }
    openToPayment(){
        // if(Meteor.userId() == this.state.data.user_id){
        //     message.warning('不能购买自己的产品');
        // } else {
        //     this.setState({topayment:true});
        // }
    };
    closeToPayment(){
        this.setState({topayment:false});
    }
    ToPayment(num,value){
        // if (Meteor.userId() == null) {
        //     message.warning('请先登录账号');
        // }
        // else {
        //     Meteor.call('quote.updateRecord', this.state.data.key, num, this.state.data.name, value, this.state.stocktype, (err) => {
        //         if (err) {

        //         } else {
        //             this.closeToPayment();
        //             browserHistory.replace('/Payment');
        //         }
        //     });
        // }
    }
    //加入购物车
    showAddShopCard(){
        // if(this.state.addtocart == true){
        //     return(
        //         <SearchBox word={I18n.t('top.shop')} click={this.closeAddShopCard.bind(this)} flag={false} data={this.state.data} Card={this.AddShopCard.bind(this)} stocktype={this.state.stocktype} />
        //     )
        // }
    }
    openAddShopCard(){
        // if(Meteor.userId() == this.state.data.user_id){
        //     message.warning('不能购买自己的产品');
        // } else {
        //     this.setState({addtocart:true});
        // }
    };
    closeAddShopCard(){
        this.setState({addtocart:false});
    }
    AddShopCard(num,value){
        // if (Meteor.userId() == null) {
        //     message.warning('请先登录账号');
        // }
        // else {
        //     Meteor.call('cart.updateRecord', this.state.data.key, num, this.state.data.name, value, this.state.stocktype, (err) => {
        //         if (err) {

        //         } else {
        //             this.closeAddShopCard();
        //             this.setState({showcard: true});
        //             this.interval = setInterval(this.tick.bind(this), 1000);
        //         }
        //     });
        // }
    }
    tick(){
        if (this.state.num == 0) {
            this.setState({showcard:false,num:5});
            clearInterval(this.interval);
        }
        else {
            this.setState({num:this.state.num - 1});
        }
    }
    ShowDelivery(){
        if(this.props.stockInfo.currency == 1){
            return(
                <RadioGroup onChange={this.changeradio.bind(this)} value={1}>
                    <Radio key="a" value={1}>{I18n.t('ShoppingCart.china')}</Radio>
                </RadioGroup>
            )
        }else if(this.props.stockInfo.currency == 2){
            return(
                <RadioGroup onChange={this.changeradio.bind(this)} value={this.state.value}>
                    <Radio key="a" value={1}>{I18n.t('productdetails.freetradezone')}</Radio>
                    <Radio key="b" value={2}>{I18n.t('productdetails.overseasdomestic')}</Radio>
                </RadioGroup>
            )
        }
    }
    showcontentRight(){
        let temp = [];
        temp[0] = (
            <div className="contentRight">
                <div className="top">
                    <img src={(config.file_server+this.state.headicon)} alt="图片" className="lagou"/>
                    <p><img className="productlogo" src={(config.theme_path+'productlogo.png')} alt="图片"/> {this.props.stockInfo.user_company.length > 8 ? this.props.stockInfo.user_company.substr(0,8)+'...' : this.props.stockInfo.user_company }
                    </p>
                    <img className="jin" src={(config.theme_path+'jin.png')} alt="图片"/>
                </div>
                <p>{I18n.t('productdetails.creditscore')}<span>45</span></p>
                {/*transactionordernumber is decided by seller deliver goods,need to change?*/}
                <p>{I18n.t('productdetails.transactionordernumber')}{this.props.stockInfo.order_count}单</p>
                {/*<p>{I18n.t('productdetails.cancelordernumber')}2单</p>*/}
                <div className="bottom">
                    <p>{I18n.t('productdetails.merchantlabel')}</p>
                   {/* {
                        (Roles.userIsInRole(Meteor.userId(), 'double')) ? (<p><img  src={(config.theme_path+'pei.png')} alt="图片"/></p>) : (<p></p>)
                    }
                    {
                        (Roles.userIsInRole(Meteor.userId(), 'guarantee')) ? (<p><img  src={(config.theme_path+'bao.png')} alt="图片"/></p>) : (<p></p>)
                    }
                    {
                        (Roles.userIsInRole(Meteor.userId(), 'stock_verify')) ? (<p><img  src={(config.theme_path+'pei.png')} alt="图片"/></p>) : (<p></p>)
                    }*/}
                </div>
            </div>
        )
        return temp;
    }
    changeradio(e) {
        console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });
    }
    clickPlus(){
        if (this.state.number < this.props.stockInfo.stock_quantity) {
            let num = this.state.number + this.props.stockInfo.inc_number;
            let i = 0;
            for (; i < this.props.stockInfo.price.length; ++ i) {
                if (this.props.stockInfo.price[i].min > num) {
                    break;
                }
            }
            let price = this.props.stockInfo.price[i-1].price;
            this.state.data.quantity = num;
            this.setState({number: num, price: price, totalPrice: price*num, data: this.state.data});
        }
    }
    clickMinus(){
        if (this.state.number > this.props.stockInfo.MOQ) {
            let num = this.state.number - this.props.stockInfo.inc_number;
            let i = 0;
            for (; i < this.props.stockInfo.price.length; ++ i) {
                if (this.props.stockInfo.price[i].min > num) {
                    break;
                }
            }
            let price = this.props.stockInfo.price[i-1].price;
            this.state.data.quantity = num;
            this.setState({number: num, price: price, totalPrice: price*num, data: this.state.data});
        }
    }
    changeNum(value) {
        let num = value;
        let tmp = num-this.props.stockInfo.MOQ;
        let result = Math.ceil(tmp / this.props.stockInfo.inc_number)*this.props.stockInfo.inc_number+this.props.stockInfo.MOQ;
        let i = 0;
        for (; i < this.props.stockInfo.price.length; ++ i) {
            if (this.props.stockInfo.price[i].min > result) {
                break;
            }
        }
        let price = this.props.stockInfo.price[i-1].price;
        this.state.data.MOQ = result;
        this.setState({number: result, price: price, totalPrice: price*result, data: this.state.data});
    }
    ShowMin(){
        let min = [];
        for(let i=0;i<this.props.stockInfo.price.length;i++){
            min[i] = (
                <span>{this.props.stockInfo.price[i].min}+</span>
            )
        }
        return min;
    }
    ShowPrice(){
        let price = [];
        if(this.props.stockInfo.currency == 1){
            for(let i=0;i<this.props.stockInfo.price.length;i++){
                price[i] = (
                    <span>￥{this.props.stockInfo.price[i].price}</span>
                )
            }

        }else if(this.props.stockInfo.currency == 2){
            for(let i=0;i<this.props.stockInfo.price.length;i++){
                price[i] = (
                    <span>${this.props.stockInfo.price[i].price}</span>
                )
            }
        }
        return price;
    }
    ShowName(){
        if(this.props.stockInfo.currency == 1){
            return(
                <span>{I18n.t('productdetails.rmb')}</span>
            )
        }else if(this.props.stockInfo.currency == 2){
            return(
                <span>{I18n.t('buyerForm.dollar')}</span>
            )
        }
    }
    render(){
        console.log(this.props);
        return (
            <div>
                {this.ShowCard()}
                <div className="contentLeft">
                    <img src={(config.file_server+this.props.stockInfo.img)} alt="图片"/>
                </div>
                <div className="contentMiddle">
                    <p>{I18n.t('productdetails.model')}<span className="model">{this.props.stockInfo.product_id}</span> <span className="totalinventory">{I18n.t('productdetails.totalinventory')}{this.props.stockInfo.stock_quantity}{I18n.t('productdetails.jian')}</span></p>
                    <p >{I18n.t('productdetails.manufacturer')}{this.props.stockInfo.manufacturer}</p>
                    <p>{I18n.t('productdetails.supplier')}{this.props.stockInfo.user_company}</p>
                    <p>{I18n.t('productdetails.shipment')}{this.props.stockInfo.sold_quantity}{I18n.t('productdetails.jian')}</p>
                    <p className="money">{this.props.stockInfo.currency == 1 ? '￥' : '$'}{this.state.price}</p>
                    <div className="pricegradient">
                        <p > <span> {I18n.t('productdetails.pricegradient')}</span>{this.ShowMin()}</p>
                        <p >{this.ShowName()} {this.ShowPrice()} </p>
                    </div>
                    <span className="deliveryplace">{I18n.t('productdetails.deliveryplace')}</span>
                    {this.ShowDelivery()}
                    <div className="info">
                        <div className="numInput">
                            <span className="num">{I18n.t('evaluate.num')}</span>
                            <span className="minus" onClick={this.clickMinus.bind(this)}><img src={(config.theme_path+'minus.png')} /></span>
                            <InputNumber className="input_" min={this.props.stockInfo.MOQ} max={this.props.stockInfo.stock_quantity} step={this.props.stockInfo.inc_number} onChange={this.changeNum.bind(this)} value={this.state.number} />
                            <span className="plus" onClick={this.clickPlus.bind(this)} ><img src={(config.theme_path+'plus.png') } /></span>
                        </div>
                        <p className="total">{I18n.t('productdetails.total')}<span>{this.props.stockInfo.currency == 1 ? '￥' : '$'}{Math.round((this.state.totalPrice)*100)/100}</span></p>
                        <p className="postage">{I18n.t('productdetails.postage')}<span>{this.props.stockInfo.currency == 1 ? '￥' : '$'}{this.props.stockInfo.free_ship == 1 ? 0 : this.props.stockInfo.trans_expanse}</span></p>
                    </div>
                    <div className="button">
                        <span className="buyimmediately" onClick={this.openToPayment.bind(this)} >{I18n.t('productdetails.buyimmediately')}</span>
                        <span className="joinshopcart"  onClick={this.openAddShopCard.bind(this)}>{I18n.t('productdetails.joinshopcart')}</span>
                    </div>
                </div>
                {this.showcontentRight()}
                {this.showToPayment()}
                {this.showAddShopCard()}
            </div>
        );
    }
}