import { hashHistory,browserHistory} from 'react-router';
import server from '../imports/config.json';
import fetch from 'isomorphic-fetch'

 function request (method, url, body) {
  method = method.toUpperCase();
  if (method === 'GET') {
    // fetch的GET不允许有body，参数只能放在url中
    body = undefined;
  } else {
    body = body && JSON.stringify(body);
  }

  return fetch(server.server+url, {
    credentials: 'include',
    method,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      // 'Access-Token': sessionStorage.getItem('access_token') || '' // 从sessionStorage中获取access token
    },
    body
  })
    .then((response) => {
      // if (500 === 400) {
      //   console.log("res.json().code400")
      //   hashHistory.push('/login');
      //   return Promise.reject('Unauthorized.');
      // } else {
      //   // const token = res.headers.get('access-token');
      //   // if (token) {
      //   //   sessionStorage.setItem('access_token', token);
      //   // }
        
      // }
      return response.json();
    }).then((body)=>{
      if(body.code==401){
        browserHistory.replace('/login'); 
        // hashHistory.push('/login');
        return 'Unauthorized.';
      }else{
        return body;
      }

    });
}

export const get = url => request('GET', url);
export const post = (url, body) => request('POST', url, body);
export const put = (url, body) => request('PUT', url, body);
export const del = (url, body) => request('DELETE', url, body);